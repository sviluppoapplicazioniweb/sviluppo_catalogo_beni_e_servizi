<?php

/*
  Uploadify v2.1.4
  Release Date: November 8, 2010

  Eseguo l'upload dei file e aggiorno la tabella T_File.
 */
include "progetto.inc";
include $PROGETTO . "/prepend.php3";
include "authpath.inc";

if (!empty($_FILES)) {
    $tempFile = '/tmp/' . basename($_FILES['Filedata']['tmp_name']);
    //$targetPath = $_SERVER['DOCUMENT_ROOT'] . '/multimedia/' . $_POST['Id_Multimedia'] . '/';
    $targetPath = $PATHDOCS . 'images/upload/strutture/' . $_POST["Id_Struttura"] . '/';
    $targetFile = str_replace('//', '/', $targetPath) . $_FILES['Filedata']['name'];

    // $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
    // $fileTypes  = str_replace(';','|',$fileTypes);
    // $typesArray = split('\|',$fileTypes);
    // $fileParts  = pathinfo($_FILES['Filedata']['name']);
    // if (in_array($fileParts['extension'],$typesArray)) {
    // Uncomment the following line if you want to make the directory if it doesn't exist
    //mkdir(str_replace('//', '/', $targetPath), 0755, true);
    mkdir($targetPath);
    
    $db_insimage = new DB_CedCamCMS;
    $SQL_numimg = "SELECT * FROM T_Images WHERE Id_Struttura = " . $_POST["Id_Struttura"];
    $db_insimage->query($SQL_numimg);
    
    if ($db_insimage->num_rows() < 5) {
        if (move_uploaded_file($tempFile, $targetFile)) {
            $SQL_Ins = "INSERT INTO T_Images (Id_Struttura, Image) VALUES (" . $_POST["Id_Struttura"] . ", '" . $_FILES['Filedata']['name'] . "')";
            $db_insimage->query($SQL_Ins);
        }
    }
    
    echo str_replace($_SERVER['DOCUMENT_ROOT'], '', $targetFile);
    // } else {
    // 	echo 'Invalid file type.';
    // }
}
?>