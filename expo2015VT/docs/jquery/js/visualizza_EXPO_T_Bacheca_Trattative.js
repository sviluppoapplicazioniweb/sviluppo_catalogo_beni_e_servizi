function salva(destinazione, stato) {
    var iform = document.form_moduli;
    if (valida(iform, stato)) {
        iform.method = "post";
        iform.setAttribute('action', destinazione);
        iform.submit();
    }

}

function valida(iform, stato) {

    if (stato == 0) {
        var formValore = iform.valore.value;
        if (formValore === "") {
            alert($("#_CAMPI_OB_").val());
            return false;
        }
    } else if (stato == 2) {
        var formTesto = iform.testo.value;
        if (formTesto === "") {
            alert($("#_CAMPI_OB_").val());
            return false;
        }

    }
    return true;
}

function visualizzaBOX(tipo, idTable) {
    var par = 'page=tr&lang=' + $("#Lang").val() + '&Id=' + idTable + "&IdLog=" + $("#IdLog").val();
    var url = '';

    if (tipo === "PRT") {
        url = "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc";
    } else {
        url = "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc";
    }


    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
            if (tipo === "PRT") {
                $("#box_popup2").html(response);
                document.getElementById("coprente2").style.display = 'block';

            } else {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
            }

        }});
}


function visualizza(idTrat) {
    var par = 'lang=' + $("#Lang").val() + '&Id=' + idTrat;
    var url = "ajaxlib/requester/visualizza_Trattativa.inc";
    
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
        }});
}


function chiudi() {
    document.getElementById("coprente").style.display = 'none';
}

function chiudi2() {
    document.getElementById("coprente2").style.display = 'none';

}

$(document).ready(function() {
    $("#valore").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });
    $("#valore").blur(function() {
        $(this).currency({region: '', thousands: '.', decimal: ',', decimals: 2});
    });

});

document.writeln("<script type='text/javascript' src='/jquery/jquery.currency.js'></script>");