var divOld = "";
var imgOld = "";
function mostraDivScorrevole(IdTag) {
    var divId = "#divScorrevole" + IdTag;
    var imgId = "#imgTag" + IdTag;
    $(divOld).hide('fast');
    $(imgOld).attr("src", "images/admin/piu.gif");
    if (divOld != divId) {
        $(divId).css({"text-align": "left"});
        $(divId).animate({"height": "toggle"}, {duration: 1000});
        $(imgId).attr("src", "images/admin/meno.gif");
        divOld = divId;
        imgOld = imgId;
    } else {
        divOld = "";
        imgOld = "";
    }

}

function salva(destinazione) {
    var iform = document.form_moduli;
    if (valida(iform)) {
        iform.method = "post";
        iform.setAttribute('action', destinazione);
        iform.submit();
    }

}

function valida(iform) {


    var formOggetto = iform.oggetto;
    var formDescrizione = iform.messaggio.value;

    if (formDescrizione == "" || (formOggetto && formOggetto.value == "")) {
        alert($("#_CAMPI_OB_").val());
        return false;
    }
    else {
        return true;
    }
}

function chiudiTrattativa(destinazione) {
    var iform = document.form_moduli;
    iform.method = "post";
    iform.setAttribute('action', destinazione);
    iform.submit();


}

function showMail(idTag){
        $(idTag).toggle();
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';
}

function chiudi2() {
    document.getElementById("coprente2").style.display = 'none';

}

function visualizzaBOX(tipo, idTable) {
    var par = 'page=tr&lang=' + $("#Lang").val() + '&Id=' + idTable + "&IdLog=" + $("#IdLog").val();
    var url = '';

    if (tipo === "PRT") {
        url = "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc";
    } else {
        url = "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc";
    }


    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
            if (tipo === "PRT") {
                $("#box_popup2").html(response);
                document.getElementById("coprente2").style.display = 'block';

            } else {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
            }

        }});
}
