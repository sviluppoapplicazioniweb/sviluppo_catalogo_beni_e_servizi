
function invia(destinazione) {
    var iform = document.form_moduli;
    if (valida(iform)) {
        iform.method = "post";
        iform.setAttribute('action', destinazione);
        iform.submit();
    }

}

function valida(iform) {


    var formOggetto = iform.oggetto;
    var formImpresa = iform.idImpresa.value;
    var formValore = iform.valore.value;
    var formDescrizione = iform.messaggio.value;

    if (formDescrizione == "" || formOggetto.value == ""
            || formImpresa.value == "" || formValore.value == "") {
        alert($("#msgError").val());
        return false;
    }
    else {
        return true;
    }
}

function viewProdotti(idInput) {
    var par = 'lang=' + $("#Lang").val() + '&idImp=' + idInput.value + '&idPar=' + document.getElementById('IdPar').value;
    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/select_prodotti.inc",
        data: par,
        success: function(response) {
            
            if(response == ""){
                $("#tdTextProdotto").hide();
                $("#prodottiTd").html("");
            } else {
                $("#tdTextProdotto").show();
                $("#prodottiTd").html(response);
            }
            
        }});
}

$(document).ready(function() {
    $("#tdTextProdotto").hide();
    $("#valore").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });
    $("#valore").blur(function() {
        $(this).currency({region: '', thousands: '.', decimal: ',', decimals: 2});
    });

});

document.writeln("<script type='text/javascript' src='/jquery/jquery.currency.js'></script>");


