
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse_cookie.js'></script>");

$(document).ready(function() {

    $('.collapsible').collapsible({
        defaultOpen: '',
        cookieName: 'nav',
        speed: 'slow'
    });

    $("a[name='p1']").addClass('pageSelect');
    
    var availableTags = $("#listaImprese").val().split('|');

    /*$("#impresa").autocomplete({
        source: availableTags
    });*/
    
    $( "#impresa" ).autocomplete({
    	appendTo: "#risultati",
        open: function() {
            var position = $("#risultati").position(),
                left = position.left, top = position.top;

            $("#risultati > ul").css({left: left + 20 + "px",
                                    top: top + 4 + "px" });

        },
    	source: function ( request, response ) {
    		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
    		response( 
    				$.grep( 
    						availableTags, function( item ){
    							return matcher.test( item );
    						}
    						)
    				);
    	}
    }); 
    
    if ($("#retiImpresa").is(":checked")){
    	$("#rete").attr("disabled", false);
    }else{
    	$("#rete").attr("disabled", true);
    }
    
    
    $('#retiImpresa').change(function() {
        if ($(this).is(":checked")) {
        	$("#rete").attr("disabled", false);
        	$("#rete").select();
        } else {
        	$("#rete").val("");
        	$("#rete").attr("disabled", true);
        }
    });
    

    $('#impresa').keypress(function(e) {
        if (e.which == 13) {
            cerca("index.phtml?Id_VMenu=304&azione=SRC");
        }
    });

    $('#all').change(function() {
        if ($(this).is(":checked")) {
            $("#risultoRicerca :checkbox").prop("checked", true);
        } else {
            $("#risultoRicerca :checkbox").attr('checked', false);
        }
    });
    
    $('#tooltipSelect li').hover(
            function () {
                //mostra sottomenu
                $('ul', this).stop(true, true).slideDown(0);
     
            }, 
            function () {
                //nascondi sottomenu
                $('ul', this).stop(true, true).slideUp(200);        
            }
        );
    
    
    var availableTagsReti = $("#listaReti").val().split('|');

    /*$("#impresa").autocomplete({
        source: availableTags
    });*/
    
    $( "#rete" ).autocomplete({
    	appendTo: "#risultatiRete",
        open: function() {
            var position = $("#risultatiRete").position(),
                left = position.left, top = position.top;

            $("#risultatiRete > ul").css({left: left + 20 + "px",
                                    top: top + 4 + "px" });

        },
    	source: function ( request, response ) {
    		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
    		response( 
    				$.grep( 
    						availableTagsReti, function( item ){
    							return matcher.test( item );
    						}
    						)
    				);
    	}
    }); 

});

function inviaSiexpo(iform, destinazione) {
    //alert('ci sono: '+ destinazione);
    
	var iform = $("#siexpoForm");
    iform.attr('method', 'post');
    iform.attr('action', destinazione);
    iform.submit();
}

function selezionaTutti(){
	
	$("input[name=richiestaImp]").prop("checked", true);
	$("input[name=richiestaProd]").prop("checked", true);
	$("#all").prop("checked", true);	
}  

function delesezionaTutti(){

	$("input[name=richiestaImp]").prop("checked", false);
	$("input[name=richiestaProd]").prop("checked", false);
	$("#all").prop("checked", false);	
}  

function invertiSelezione(){

	$("input[name=richiestaImp]").each(function(){
		$(this).prop("checked",!$(this).prop("checked"));
	})

}

function visualizzaBOX(tipo, idTable) {
    var par = 'page=rc&lang=' + $("#Lang").val() + '&Id=' + idTable + "&IdLog=" + $("#IdLog").val() + "&IdEsito=" + $("#esito"+tipo+idTable).val();
    var url = '';
    
    //alert('Tipo'+tipo);
    //exit;
    if (tipo === "PRT") {
        url = "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc";
    } else if (tipo === "RTI"){
    	url = "ajaxlib/requester/partecipante_EXPO_T_Reti.inc";
    } else {
        url = "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc";
    }


    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
            if (tipo === "PRT") {
                $("#box_popup2").html(response);
                document.getElementById("coprente2").style.display = 'block';
            } else if (tipo === "RTI"){
            	$("#box_popup2").html(response);
            	//alert('pork Mis');
            	document.getElementById("coprente2").style.display = 'block';
            } else {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
            }

        }});
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';

}
function chiudi2() {
    document.getElementById("coprente2").style.display = 'none';

}

function selectPreferiti(idA, idImp, idPar) {
    var par = 'idImp=' + idImp + '&idPar=' + idPar;
    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/select_preferiti.inc",
        data: par,
        success: function(response) {
            document.getElementById(idA).innerHTML = '<img draggable="false" src="images/expo2015/' + response + '" />';
        }});
}

function aggiungiParametri(idPar) {
    var e = document.getElementById(idPar).children;
    var addPar = "";
    for (var i = 0; i < e.length; i++) {
        addPar += "&" + e[i].id + "=" + e[i].value;
    }
    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/visualizza_ricerca_parametri.inc",
        data: 'lang=' + $("#Lang").val() + '&action=VIEW&parametro=' + idPar + addPar,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }
    });

}

function selezioneRichiestaProdotto(idProd) {

    var addPar = "&richiestaProd0=" + idProd;

    document.getElementById("coprente").style.display = 'none';
    document.getElementById("coprente2").style.display = 'none';

    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
        data: 'lang=' + $("#Lang").val() + addPar,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});

}

function selezioneRichiestaImpresa(idImp) {

    var addPar = "&richiestaImp0=" + idImp;

    document.getElementById("coprente").style.display = 'none';
    document.getElementById("coprente2").style.display = 'none';

    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
        data: 'lang=' + $("#Lang").val() + addPar,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});

}

function selezioneRichiesta() {

    var e = document.getElementsByName("richiestaImp");
    var addPar = "";

    var indImp = 0;
    if (!e.length)
        e = [e];
    for (var i = 0; i < e.length; i++) {
        if (e[i].checked) {
            addPar += "&richiestaImp" + indImp + "=" + e[i].value;
            indImp++;
        }

    }
    var indProd = 0;
    e = document.getElementsByName("richiestaProd");
    if (!e.length)
        e = [e];

    for (var i = 0; i < e.length; i++) {
        if (e[i].checked) {
            addPar += "&richiestaProd" + indProd + "=" + e[i].value;
            indProd++;
        }

    }

    if (indProd > 0 || indImp > 0) {
        $.ajax({
            type: "GET",
            async: false,
            url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
            data: 'lang=' + $("#Lang").val() + addPar,
            success: function(response) {
                $("#box_popup").html(response);

                document.getElementById("coprente").style.display = 'block';
            }});
    }
}

function cerca(destinazione) {
    var iform = document.formCerca;
    iform.method = "post";
    iform.setAttribute('action', destinazione);
    iform.submit();

}

function visualizzaProdotti(myId) {

    if ($('#tr' + myId).attr("class") == "subElencoHidden") {
        $('#tr' + myId).attr('class', 'subElencoView');
        $('#img' + myId).attr('src', '/images/expo2015/meno.png');
    } else {
        $('#tr' + myId).attr('class', 'subElencoHidden');
        $('#img' + myId).attr('src', '/images/expo2015/piu.png');
    }
}

function svcerca() {
    $("#datiRicerca :checkbox").removeAttr("checked");
    $("#datiRicerca :radio").removeAttr("checked");

    $(".linkRicercanumeroD").hide();
    $(".linkRicercafatturato").hide();
    
    document.getElementById("impresa").value = "";
    //document.getElementById("numeroD").value = "";
    //document.getElementById("fatturato").value = "";

    var parRicerca = '<span class="textAdd">0</span>';

    $("#categoria").html(parRicerca);
    $("#lingue").html(parRicerca);
    $("#certificazioni").html(parRicerca);
    $("#associazioni").html(parRicerca);


    $("#parametriRicerca").html("");
    $("#requesterTable").html("");


    $.cookie('nav', null);
    $('.collapsible').collapsible({
        defaultOpen: '',
        animateClose: function(elem, opts) {
            elem.next().slideDown(opts.speed);
        }
    });

    $(window).scrollTop(0);

}

function deseleziona(campo) {
    $("input[name=" + campo + "]:radio").removeAttr("checked");
    $(".linkRicerca" + campo).hide();
}

/*
 function svrichieste() {
 
 //alert()
 $("#all").attr("checked", status)
 if (status) {
 $("#risultoRicerca :checkbox").attr('checked', true);
 } else {
 $("#risultoRicerca :checkbox").attr('checked', false);
 }
 /*
 if($("#all").is(":checked")){
 $("#risultoRicerca :checkbox").attr('checked', 'checked');
 } else{
 $("#risultoRicerca :checkbox").removeAttr("checked");
 }
 
 }*/