function visualizzaBOX(tipo, idTable) {
    var par = 'page=rc&lang=' + $("#Lang").val() + '&Id=' + idTable + "&IdLog=" + $("#IdLog").val();
    var url = '';

    if (tipo === "PRT") {
        url = "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc";
    } else if (tipo === "RTI"){
    	url = "ajaxlib/requester/partecipante_EXPO_T_Reti.inc";
    } else {
        url = "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc";
    }


    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
            if (tipo === "PRT") {
                $("#box_popup2").html(response);
                document.getElementById("coprente2").style.display = 'block';
            } else if (tipo === "RTI"){
            	$("#box_popup2").html(response);
                document.getElementById("coprente2").style.display = 'block';
            } else {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
            }
        }});
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';

}
function chiudi2() {
    document.getElementById("coprente2").style.display = 'none';

}

function selectPreferiti(idA, idImp, idPar) {
    var par = 'idImp=' + idImp + '&idPar=' + idPar;
    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/select_preferiti.inc",
        data: par,
        success: function(response) {
            if (response != "preferiti_stella_accesa.png") {
                var element = document.getElementById(idA);
                var trDivLinea = document.getElementById("trDivLinea" + idImp);
                var trDiv = document.getElementById("trDiv" + idImp);

                var iTr = document.getElementById("tr" + idImp);

                element.parentNode.removeChild(element);
                trDiv.parentNode.removeChild(trDiv);
                trDivLinea.parentNode.removeChild(trDivLinea);

                if (iTr) {
                    iTr.parentNode.removeChild(iTr);
                }
            }

        }});
}

function visualizzaProdotti(myId) {
    if ($('#tr' + myId).attr("class") == "subElencoHidden") {
        $('#tr' + myId).attr('class', 'subElencoView');
        $('#img' + myId).attr('src', '/images/expo2015/meno.png');
    } else {
        $('#tr' + myId).attr('class', 'subElencoHidden');
        $('#img' + myId).attr('src', '/images/expo2015/piu.png');
    }
}

function selezioneRichiesta() {

    var e = document.getElementsByName("richiestaImp");
    var addPar = "";

    var indImp = 0;
    if (!e.length)
        e = [e];
    for (var i = 0; i < e.length; i++) {
        if (e[i].checked) {
            addPar += "&richiestaImp" + indImp + "=" + e[i].value;
            indImp++;
        }

    }
    var indProd = 0;
    e = document.getElementsByName("richiestaProd");
    if (!e.length)
        e = [e];

    for (var i = 0; i < e.length; i++) {
        if (e[i].checked) {
            addPar += "&richiestaProd" + indProd + "=" + e[i].value;
            indProd++;
        }

    }

    if (indProd > 0 || indImp > 0) {
        $.ajax({
            type: "GET",
            async: false,
            url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
            data: 'idu=' + $("#IdPartecipante").val() + '&lang=' + $("#Lang").val() + addPar,
            success: function(response) {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
            }});
    }
}

function selezioneRichiestaProdotto(idProd) {

    var addPar = "&richiestaProd0=" + idProd;

    document.getElementById("coprente").style.display = 'none';
    document.getElementById("coprente2").style.display = 'none';

    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
        data: 'lang=' + $("#Lang").val() + addPar,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});

}


$(document).ready(function() {
    $('#all').change(function() {
        if ($(this).is(":checked")) {
            $("#divPreferiti :checkbox").prop("checked", true);
        } else {
            $("#divPreferiti :checkbox").attr('checked', false);
        }
    });
    
    $('#tooltipSelect li').hover(
            function () {
                //mostra sottomenu
                $('ul', this).stop(true, true).slideDown(0);
     
            }, 
            function () {
                //nascondi sottomenu
                $('ul', this).stop(true, true).slideUp(200);        
            }
        );


    
});



function selezionaTutti(){
	
	$("input[name=richiestaImp]").prop("checked", true);
	$("input[name=richiestaProd]").prop("checked", true);
	$("#all").prop("checked", true);	
}  

function delesezionaTutti(){

	$("input[name=richiestaImp]").prop("checked", false);
	$("input[name=richiestaProd]").prop("checked", false);
	$("#all").prop("checked", false);	
}  

function invertiSelezione(){

	$("input[name=richiestaImp]").each(function(){
		$(this).prop("checked",!$(this).prop("checked"));
	});

}

function selezioneRichiestaImpresa(idImp) {

    var addPar = "&richiestaImp0=" + idImp;

    document.getElementById("coprente").style.display = 'none';
    document.getElementById("coprente2").style.display = 'none';

    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/visualizza_mail_richiesta.inc",
        data: 'lang=' + $("#Lang").val() + addPar,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});

}
