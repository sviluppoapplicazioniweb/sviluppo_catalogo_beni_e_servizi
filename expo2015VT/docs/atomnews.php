<?
include "progetto.inc";
include $PROGETTO."/prepend.php3";
include $PROGETTO."/uffici.inc";

header("Content-Type: text/xml");
header("Pragma: no-cache");

$db_news = new DB_CedCamCMS;

$tipoart_atom=3;

//$SQL="select T_News.*,DATE_FORMAT(Data,'%d') as Giorno,DATE_FORMAT(Data,'%m') as Mese,DATE_FORMAT(Data,'%Y') as Anno,Tlk_TipoArticolo1.Descrizione from T_News,Tlk_TipoArticolo1 where T_News.Id_TipoArticolo1=Tlk_TipoArticolo1.Id order by Data DESC";

$SQL="SELECT T_Articoli.* , DATE_FORMAT( T_Articoli.Data_Modifica, '%d' ) AS Giorno, DATE_FORMAT( T_Articoli.Data_Modifica, '%m' ) AS Mese, DATE_FORMAT( T_Articoli.Data_Modifica, '%Y' ) AS Anno,T_Articoli.Data_Modifica as DataMod, T_Menu.Titolo";
$SQL.=" FROM T_Articoli, T_Menu";
$SQL.=" WHERE T_Articoli.Id_TipoArticolo1 = T_Menu.Id and T_Articoli.Id_TipoArticolo3 = ".$tipoart_atom;
$SQL.=" ORDER BY DataMod DESC ";

if ($db_news->query($SQL))
{
	$i=1;
	$dataeora = "";
	$items = "";
	while ($db_news->next_record())
	{
		switch ($db_news->f('Mese'))
		{
			case "01":
				$mese="Gennaio";
				break;
			case "02":
				$mese="Febbraio";
				break;
			case "03":
				$mese="Marzo";
				break;
			case "04":
				$mese="Aprile";
				break;
			case "05":
				$mese="Maggio";
				break;
			case "06":
				$mese="Giugno";
				break;
			case "07":
				$mese="Luglio";
				break;
			case "08":
				$mese="Agosto";
				break;
			case "09":
				$mese="Settembre";
				break;
			case "10":
				$mese="Ottobre";
				break;
			case "11":
				$mese="Novembre";
				break;
			case "12":
				$mese="Dicembre";
				break;
		}

		$link=$db_news->f('Link');


$titolo_atom=$tipoart_atom==1?$db_news->f('Titolo_Html'):$db_news->f('Titolo_Abstract');
$testo_atom=$tipoart_atom==1?$db_news->f('Testo_Html'):$db_news->f('Testo_Abstract');

//$titolo_atom=str_replace('"','',str_replace("&","&amp;",$titolo_atom));
//$testo_atom=str_replace('"','',str_replace("&","&amp;",$testo_atom));

$titolo_atom=str_replace('"','',str_replace("&","&amp;",$titolo_atom));
$testo_atom=str_replace("&","&amp;",$testo_atom);

$testo_atom= preg_replace("/\{%(\w+)%\}/e", "\$\\1",$testo_atom);

		if (is_numeric($link))
			$link= "index.phtml?Id_VMenu=".$link;
		elseif ($link=="#")
			$link= "index.phtml?Id_VMenu=291#articolo".$db_news->f('Id_Articolo');
		elseif ($link=="")
			$link= "index.phtml?Id_VMenu=241&amp;daabstract=".$db_news->f('Id_Articolo');
		else
			$link= "vaialsito.php?sito=".base64_encode($link);

		$link="http://www.va.camcom.it/".$link;
		
		$dataeora=date('D M j G:i:s T Y');

		$items.="<entry>\n";
		$items.="    <title>".$db_news->f('Giorno')." ".$mese." ".$db_news->f('Anno')." - ".utf8_encode($titolo_atom)."</title>\n";
		$items.="    <link rel=\"alternate\" type=\"text/html\" href=\"".$link."\" />\n";
		$items.="    <link rel=\"service.edit\" type=\"application/atom+xml\" href=\"".$link."\" title=\"".utf8_encode($titolo_atom)."\" />\n";
		$items.="    <id>tag:www.va.camcom.it.it,2007://1.".$i."</id>\n";
		$items.="    \n";
		$items.="    <published>".$db_news->f('DataMod')."</published>\n";
		$items.="    <updated>".$db_news->f('DataMod')."</updated>\n";
		$items.="    \n";
		$items.="    <summary>".utf8_encode($titolo_atom)."</summary>\n";
		$items.="    <author>\n";
		$items.="        <name>La redazione</name>\n";
		$items.="        <uri>http://www.va.camcom.it</uri>\n";
		$items.="    </author>\n";
		$items.="            <category term=\"".utf8_encode($db_news->f('Titolo'))."\" />\n";
		$items.="    \n";
		$items.="    <content type=\"html\" xml:lang=\"it\" xml:base=\"http://www.va.camcom.it/\">\n";
		$items.="".utf8_encode($testo_atom)."\n";
		$items.="    </content>\n";
		$items.="</entry>\n";


		$i++;
	}
}
$codice_html=file_get_contents($PATHDOCS."tmpl/atomnews.xml");
echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);
?>