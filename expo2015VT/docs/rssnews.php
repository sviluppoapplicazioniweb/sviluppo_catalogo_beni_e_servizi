<?
include "progetto.inc";
include $PROGETTO."/prepend.php3";

header("Content-Type: text/xml");
header("Pragma: no-cache");

$db_news = new DB_CedCamCMS;

$SQL="select *,DATE_FORMAT(Data_Modifica,'%d') as Giorno,DATE_FORMAT(Data_Modifica,'%m') as Mese,DATE_FORMAT(Data_Modifica,'%Y') as Anno from T_Articoli where Id_TipoArticolo3=3 order by Data_Modifica DESC";
if ($db_news->query($SQL))
{
	$i=1;
	$dataeora = "";
	$items = "";
	while ($db_news->next_record())
	{
		switch ($db_news->f('Mese'))
		{
			case "01":
				$mese="Gennaio";
				break;
			case "02":
				$mese="Febbraio";
				break;
			case "03":
				$mese="Marzo";
				break;
			case "04":
				$mese="Aprile";
				break;
			case "05":
				$mese="Maggio";
				break;
			case "06":
				$mese="Giugno";
				break;
			case "07":
				$mese="Luglio";
				break;
			case "08":
				$mese="Agosto";
				break;
			case "09":
				$mese="Settembre";
				break;
			case "10":
				$mese="Ottobre";
				break;
			case "11":
				$mese="Novembre";
				break;
			case "12":
				$mese="Dicembre";
				break;
		}

		$link=$db_news->f('Link');


		if (is_numeric($link))
			$link= "index.phtml?Id_VMenu=".$link;
		elseif ($link=="#")
			$link= "index.phtml?Id_VMenu=291#articolo".$db_news->f('Id_Articolo');
		elseif ($link=="")
			$link= "index.phtml?Id_VMenu=241&amp;daabstract=".$db_news->f('Id_Articolo');
		else
			$link= "vaialsito.php?sito=".base64_encode($link);

		$link="http://www.va.camcom.it/".$link;

		$descrizione=strip_tags($db_news->f('Testo_Abstract'));
		
//		$dataeora=date('D M j G:i:s T Y');
		$dataeora=date('r');

		$items.="                  <item>\n";
		$items.="                          <title>\n";
		$items.="                                  ".$db_news->f('Giorno')." ".$mese." ".$db_news->f('Anno')." - ".utf8_encode(str_replace("&","&amp;",$db_news->f('Titolo_Abstract')))."\n";
		$items.="                          </title>\n";
		$items.="                          <guid>\n";
		$items.="                                  ".$link."\n";
		$items.="                          </guid>\n";
		$items.="                          <link>\n";
		$items.="                                  ".$link."\n";
		$items.="                          </link>\n";
		$items.="                          <description>\n";
		$items.="                                  ".utf8_encode(str_replace("&","&amp;",$descrizione))."\n";
		$items.="                          </description>\n";
		$items.="                          <pubDate>\n";
//		$items.="                                  ".$db_news->f('Data_Modifica')."\n";
		$items.="                                  ".date("r",strtotime($db_news->f('Data_Modifica')))."\n";
//<pubDate>Wed, 02 Oct 2002 15:00:00 +0200</pubDate>
		$items.="                          </pubDate>\n";
		$items.="                  </item>\n";

		$i++;
	}
}
$codice_html=file_get_contents($PATHDOCS."tmpl/rssnews.xml");
echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);
?>