<script language="JavaScript" type="text/javascript">  
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi(); }   // esc
  	});
</script>


<?php
if ($includeConfig != 1)
    include "configuration.inc";

//$idImpresaAllow = ($_POST['Id']);
$uidArrayAllow = array($_REQUEST['Id']);
$utypeArrayAllow = array(98);
//$idVMenuAllow = array(301);
//include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){



require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$Lang = settaVar($_REQUEST, 'lang', "IT");
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");

$db = new DataBase();
$Id = settaVar($_REQUEST, 'Id', '');
$query = "SELECT * FROM EXPO_T_Bacheca WHERE id = '$Id'";

$IdPartecipante = $db->GetRow($query, 'IdPartecipante');
$IdImpresa = $db->GetRow($query, 'IdImpresa');
$IdProdotto = $db->GetRow($query, 'IdProdotto');

$IdMessaggio = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$Id'", 'IdMessaggio');
?>
<div id="bottomBox">
    <a class="buttonsLista" draggable="false" href="#" onclick="chiudi()" ><?php echo _CHIUDI_; ?></a>
</div>
<h2><?php echo _VISUALIZZA_TRATTATIVA_; ?></h2>
<br>
<br>
<table  > 
    <tr>

        <td class="tdText"><?php echo _PARTECIPANTE_; ?>:</td>
        <td class="tdText2"><?php echo $db->GetRow('SELECT * FROM T_Anagrafica WHERE id = ' . $IdPartecipante, 'RagSoc'); ?></td>
        <td class="tdText"><?php echo _FORNITORE_; ?>:</td>
        <td class="tdText2"><?php echo $db->GetRow('SELECT * FROM EXPO_T_Imprese WHERE id = ' . $IdImpresa, 'RagioneSociale'); ?></td>
    </tr>
    <tr>
        <td class="tdTextLine2T" colspan="2">
            <?php echo _VALORE_; ?>:
            <?php echo number_format($db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$Id'", 'Valore'), 2, ',', '.'); ?> &euro;
        </td>
        <td colspan="2" class="tdTextLine2T">
            <?php
            $query = "SELECT DISTINCT Nome
                                  FROM EXPO_T_Prodotti AS T
                                  RIGHT JOIN EXPO_TJ_Bacheca_Prodotti AS TJ ON T.Id = TJ.IdProdotto
                                WHERE TJ.IdBacheca = '$Id'
                                ORDER BY Nome";
            if ($db->NumRows($query) > 0) {
                ?>
                <?php echo _PRODOTTO_; ?>:

                <ul>  
                    <?php
                    foreach ($db->GetRows($query) AS $rows) {
                        ?>
                        <li><?php echo $rows ['Nome']; ?></li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </td>
    </tr>

    <?php if (strcmp($IdMessaggio, "") != 0 && strcmp($IdMessaggio, "0") != 0) { ?>
        <tr>
            <td colspan="4"><br></td>        
        </tr>
        <tr>
            <td colspan="4" class="tdText" style="border-bottom: 1px solid #014D7F;"><?php echo _OSSERVAZIONI_; ?> </td>        
        </tr>
        <tr>
            <td colspan="4" style="border-bottom: 1px solid #014D7F;">
                <br>
                <?php echo $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Messaggi WHERE Id = '$IdMessaggio'", 'Testo'); ?>    
                <br><br>
            </td>
        </tr>


    <?php } ?>

</table>
<?php }
//}

else{
	?>

<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error"><?php echo _ACCESSO_NEGATO_?></p>
	</div>

<?php 
}
