<script language="JavaScript" type="text/javascript">
    $(function() {
        $("#products").tabs();
    });
    function invia(destinazione) {
        var iform = document.siexpoForm;
        iform.method = "post";
        iform.setAttribute('action', destinazione);
        iform.submit();
    }

    $(document).keyup(function(e) {
    	  
    	  if (e.keyCode == 27) { chiudi2(); }   // esc
    	});
    
</script>
<?
if ($includeConfig != 1)
    include "configuration.inc";



//$idImpresaAllow = ($_POST['Id']);

$utypeArrayAllow = array(1,98);
//$idVMenuAllow = array(301);
//include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$Lang = settaVar($_REQUEST, 'lang', "IT");
$page = settaVar($_REQUEST, 'page', "");
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");

$db = new DataBase();
$view = settaVar($_REQUEST, "bview", "");
$IdProdotto = $_REQUEST['Id'];
$IdPartecipante = settaVar($_REQUEST, 'IdLog', '');
$IdEsito = settaVar($_REQUEST, 'IdEsito', 'null');



$queryPrd = "SELECT * FROM EXPO_T_Prodotti AS P JOIN EXPO_TJ_Imprese_OrdiniProfessionali as TJ on P.IdImpresa = TJ.Id_Impresa WHERE P.Id = $IdProdotto";
$row = $db->GetRow($queryPrd,null,null, 'ajax anteprima prodotto vetrina Ln.29');

$ordine = $row['Id_OrdineProfessionale'];
$studio = $row['IsStudio'];

$IdImpresa = $row['IdImpresa'];

$quesySQL = "SELECT RagioneSociale,StatoRegistrazione FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
$query = $db->GetRow($quesySQL,null,null, "estrazione stato registrazione visualizza prodotto ln.58");
$statoImpresa = $query['StatoRegistrazione'];
$ragioneSociale = $query['RagioneSociale'];

if ($statoImpresa == 5){
//$idMax = $db->GetRow("SELECT MAX(Id) AS id FROM EXPO_Log_Visite_Vetrina", "id") + 1;
//print "INSERT INTO EXPO_Log_Visite_Vetrina VALUES($idMax,'$IdImpresa','$IdProdotto','$IdPartecipante',NOW())";
if ($IdPartecipante != ""){
	$sqlInsert = "INSERT INTO EXPO_Log_Visite_Vetrina (Id,Id_Impresa,Id_Prodotto,Id_Partecipante,DataVisita,IdEsitoRicerca) VALUES((SELECT COALESCE(MAX(L.Id),0)+1  FROM EXPO_Log_Visite_Vetrina as L),'$IdImpresa','$IdProdotto','$IdPartecipante',NOW(),$IdEsito)";
	$db->Query($sqlInsert);
	
	$db->Query("UPDATE EXPO_T_Prodotti SET VisiteTotali = (VisiteTotali)+1 WHERE Id = '$IdProdotto'",null, 'aggiornamento visite totali');
}


$IdTipo = "P" . $IdProdotto;
$isSiexpo = $row['IsSiexpo'];
$rootDoc = selectRootDoc($ragioneSociale) . "/";
$tag = "Nome";
$testoNome = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoNome, "") == 0) {
    $testoNome = $row['Nome'];
}

$tag = "descrizione";
$testoDescrizione = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoDescrizione, "") == 0) {
    $testoDescrizione = html_entity_decode($row['Descrizione']);
}

$tag = "certificazione";
$testoCertificazione = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoCertificazione, "") == 0) {
    $testoCertificazione = html_entity_decode($row['Certificazione']);
}

$tag = "aspetti";
$testoAspetti = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoAspetti, "") == 0) {
    $testoAspetti = html_entity_decode($row['Aspetti']);
}


$tag = "brevetti";
$testoBrevetti = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoBrevetti, "") == 0) {
    $testoBrevetti = html_entity_decode($row['Brevetti']);
}

$descrizione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/prodotto", _PRODOTTO_FILE_), getAllegati('link', $row['ProdottoLink']));
$certificazione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/certificazione", _CERTIFICAZIONE_FILE_), getAllegati('link', $row['CertificazioneLink']));
$aspetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/aspetti", _ASPETTI_FILE_), getAllegati('link', $row['AspettiLink']));
$brevetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/brevetti", _BREVETTI_FILE_), getAllegati('link', $row['BrevettiLink']));
?>
<div id="bottomBox">
    <a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi2()" >X</a>
    <?php if (strcmp($page, "rc") == 0) { ?>
        <a class="buttonsLista" draggable="false" href="#" onclick="selezioneRichiestaProdotto('<?php echo $IdProdotto; ?>')"><?php echo _RICHIESTA_; ?></a> 
        <br>
    <?php } ?>

</div>

<div class="listaParametri">
    <div id="products">
        <ul>
            <li><a draggable="false" href="#products-1"><?php echo _DATI_GENERALI_T_; ?></a></li>
            <li><a draggable="false" href="#products-2"><?php echo _C_MERC_T_; ?></a></li>
            <?php if (viewTabProd($testoCertificazione, $certificazione)) { ?><li><a href="#products-3"><?php echo _CERTIFICAZIONI_T_; ?></a></li><?php } ?>
            <?php if (viewTabProd($testoAspetti, $aspetti)) { ?><li><a draggable="false" href="#products-4"><?php echo _ASPETTI_T_; ?></a></li><?php } ?>
            <?php if (viewTabProd($testoBrevetti, $brevetti)) { ?><li><a draggable="false" href="#products-5"><?php echo _BREVETTI_T_; ?></a></li><?php } ?>
        </ul>
        <div id="products-1" style="width: 100%;color: #666">
            <h4><?php echo _NOME_; ?></h4> 
            <?php echo $testoNome; ?>

            <?php if (viewTabProd($testoDescrizione, $descrizione)) { ?>
                <h4><?php echo _DESCRIZIONE_; ?></h4>
                <?php echo $testoDescrizione; ?>

                <br><br>
                <?php echo $descrizione[0]; ?>
                <?php echo $descrizione[1]; ?>
            <?php } ?>
        </div>
        <div id="products-2" style="width: 100%;">
            <h4><?php echo _C_MERC_; ?></h4>
            <ul>
                <?php
                $query = "SELECT T.Id AS Id, T.DescEstesa AS DescEstesa,DescEstesa_AL as DescEstesa_AL,T.FlagNote,T.Note
            ,DescEstesa_AL2 as DescEstesa_AL2
                FROM EXPO_Tlk_Categorie AS T
                RIGHT JOIN EXPO_TJ_Prodotti_Categorie AS TJ ON T.Id = TJ.IdCategoria
                AND TJ.IsIscritta =  'Y'
                AND TJ.IdProdotto = '$IdProdotto'
                    WHERE T.Id IS NOT NULL
                    ORDER BY DescEstesa";
                foreach ($db->GetRows($query) AS $rows) {
				
				$infoNota = '';
				if (strcmp ($rows['FlagNote'],'Y') == 0) {
				
					$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
					/*if ((strcmp ($rows['Note'],'_DEFAULT_') != 0)){
						$nota = $rows['Note'];
					}
				*/
					$infoNota = " <a href=\"#\" class=\"tooltip\" draggable=\"false\" >
                                        <img src=\"tmpl/expo2015VT/vetrina/images/infoGrey.png\" />
                                        <span>
                                        ".$nota."    <br><br>
                                        </span>
                                    </a>";
					}

                    switch ($Lang) {
                        case 'IT':
                            ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa']); echo $infoNota;?></li><?php
                            break;
                        case 'EN':
                            ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa_AL']); echo $infoNota;?></li><?php
                                break;
                            case 'FR':
                                ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa_AL2']); echo $infoNota;?></li><?php
                                break;
                            default:
                                break;
                        }
                    }
                    ?>
            </ul>
        </div>
        <?php if (viewTabProd($testoCertificazione, $certificazione)) { ?>
            <div id="products-3" style="width: 100%;color: #666">
                <h4><?php echo _CERTIFICAZIONI_; ?></h4>
                <?php echo descrizioneDefoult($testoCertificazione, $certificazione[0], $certificazione[1]); ?>
                <br><br>
                <?php echo $certificazione[0]; ?>
                <?php echo $certificazione[1]; ?>

            </div>
        <?php } ?>
        <?php if (viewTabProd($testoAspetti, $aspetti)) { ?>
            <div id="products-4" style="width: 100%;color: #666">
                <h4><?php echo _ASPETTI_; ?></h4>
                <?php echo descrizioneDefoult($testoAspetti, $aspetti[0], $aspetti[1]); ?>
                <br><br>
                <?php echo $aspetti[0]; ?>
                <?php echo $aspetti[1]; ?>
            </div>
        <?php } ?>
        <?php 
        if ($ordine == 0){	
        	if (viewTabProd($testoBrevetti, $brevetti)) { ?>
            <div id="products-5" style="width: 100%;color: #666">
                <h4><?php echo _BREVETTI_; ?></h4>
                <?php echo descrizioneDefoult($testoBrevetti, $brevetti[0], $brevetti[1]); ?>
                <br><br>
                <?php echo $brevetti[0]; ?>
                <?php echo $brevetti[1]; ?>
            </div>
        <?php } 
        }?>

    </div>
</div>
<?php }else {
	?>
	
	<div class="contentHeader">
		<div id="bottomBox">
			<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi2()" >X</a>
		</div>
	</div>
	
		<div class="error" style="text-align: center; font-size: 18px;">
			<p class="error"><?php echo _IMPRESA_TEMPORANEAMENTE_OFFLINE_ ?></p>
		</div>
	
	<?php 
}
//}

} else{
	?>

<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi2()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error"><?php echo _ACCESSO_NEGATO_?></p>
	</div>

<?php 
}

?>


