<script language="JavaScript" type="text/javascript">
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi2(); }   // esc
  	});
</script>


<?php

//include_once "configuration.inc";



//$idImpresaAllow = ($_POST['Id']);

$utypeArrayAllow = array(1,98);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/prepend.php3");
$Lang = settaVar($_REQUEST, 'lang', "IT");
$page = settaVar($_REQUEST, 'page', "");
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");

$db = new DataBase();
$IdRete = settaVar($_REQUEST, 'Id', '');
$IdPartecipante = settaVar($_REQUEST, 'IdLog', '');
$IdEsito = settaVar($_REQUEST, 'IdEsito', 'null');


if ($IdPartecipante != ""){
	$sqlInsert = "INSERT INTO EXPO_Log_Visite_Vetrina (Id,Id_Impresa,Id_Prodotto,Id_Rete,Id_Partecipante,DataVisita,IdEsitoRicerca) VALUES((SELECT COALESCE(MAX(L.Id),0)+1  FROM EXPO_Log_Visite_Vetrina as L),0,0,'$IdRete','$IdPartecipante',NOW(),$IdEsito)";
	$db->Query($sqlInsert,null, "insert log ajax reti vetrina ln 19");

	$db->Query("UPDATE EXPO_T_RetiImpresa SET VisiteTotali = (VisiteTotali)+1 WHERE Id = '$IdRete'",null, 'aggiornamento visite totali');
}


//Cerca la rete nella sua anagrafica
$query = "SELECT * FROM expo_t_retiimpresa WHERE Id = $IdRete";
$row = $db->GetRow($query);
$NRete = $row['NomeRete'];

$rootDoc = selectRootDocReti($NRete) . "/";
//Recupera le imprese iscritte ad una Rete
$qryLsImp = "SELECT expo_tj_imprese_retiimpresa.Id_Impresa, expo_tj_imprese_retiimpresa.Id_Rete, expo_tj_imprese_retiimpresa.IsCapogruppo, expo_t_imprese.RagioneSociale
FROM expo_t_imprese INNER JOIN expo_tj_imprese_retiimpresa ON expo_t_imprese.Id = expo_tj_imprese_retiimpresa.Id_Impresa
WHERE Id_Rete = $IdRete";



//echo $qryLsImp;
//$rowsLsImp = $db->GetRows($qryLsImp);
//print_r($rowsLsImp);
//exit;

$divListaImp = '<tr><td id="ImpreseXRete" colspan="2">'._IMPRESE_RETE_;
foreach ($db->GetRows($qryLsImp) AS $rowsLsImp) {
	if ($rowsLsImp['IsCapogruppo']=='Y'){
		$divListaImp .= '<div class="divImpRetiCapog" title="'._ANT_IMP_CAPOG_.'">'.$rowsLsImp['RagioneSociale'].'</div>';
	} else {
		$divListaImp .= '<div class="divImpReti">'.$rowsLsImp['RagioneSociale'].'</div>';
	}
}
$divListaImp .= '</td></tr>';

?>
<div class="contentHeader">
    <div id="bottomBox">
        <a class="buttonsChiudi" draggable="false" href="javascript:chiudi2()" >X</a>   
    </div>
</div>

<div class="divBoxAnteprima">
	
    <div class="containerDataCheck">
    	<table width="100%">
    		<tr><td><h4><?php echo _ANT_NOME_RETE_ .$row['NomeRete'] ?></h4></td></tr>
    		<tr>
            	<td><strong><?php echo _DESCR_RETE_ ?></strong>
        		<?php                           
                switch ($Lang) {
                	case 'IT': echo html_entity_decode($row['Descrizione']);
                    	break;
                    case 'EN': echo html_entity_decode($row['Descrizione_AL']);
                    	break;
                    case 'FR': echo html_entity_decode($row['Descrizione_AL2']);
                    	break;
               	}
                ?>
               	</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><?php echo $divListaImp; ?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><strong><?php echo _ANT_CONTATTO_RETE ?> </strong> 
            	<?php 
            		echo html_entity_decode($row['Contatto']);
            	?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
            	<td><?php echo getAllegatiFiles('file', $rootDoc . $IdRete . "/descriz", _ALLEGA_DOC_); ?></td>
           </tr>
           <tr>
            	<td><?php echo getAllegati('link', $row['SitoWeb']); ?></td>
           </tr>
        </table>
    </div>
</div>

<?php }
//}

else{
	?>

<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi2()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error"><?php echo _ACCESSO_NEGATO_?></p>
	</div>

<?php 
}

?>
