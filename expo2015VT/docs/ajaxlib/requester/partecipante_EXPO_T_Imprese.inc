<script language="JavaScript" type="text/javascript">
    $(function() {
        $("#tabs").tabs();
    });

    
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi(); }   // esc
  	});
</script>

<?

if ($includeConfig != 1)
    include "configuration.inc";


//$idImpresaAllow = ($_POST['Id']);

$utypeArrayAllow = array(1,98);
//$idVMenuAllow = array(301);
//include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/prepend.php3");
$Lang = settaVar($_REQUEST, 'lang', "IT");
$page = settaVar($_REQUEST, 'page', "");
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");

$db = new DataBase();
page_open(array("sess" => "CedCamCMS_Session"));
$IdImpresa = settaVar($_REQUEST, 'Id', '');
$IdPartecipante = settaVar($_REQUEST, 'IdLog', '');
$IdEsito = settaVar($_REQUEST, 'IdEsito', 'null');

$query = "SELECT * FROM EXPO_T_Imprese as I JOIN EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa WHERE I.Id = '$IdImpresa'";
$row = $db->GetRow($query);

if ($row['StatoRegistrazione'] == 5){
//$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'";

if ($IdPartecipante != ""){	
	$sqlInsert = "INSERT INTO EXPO_Log_Visite_Vetrina (Id,Id_Impresa,Id_Prodotto,Id_Partecipante,DataVisita,IdEsitoRicerca) VALUES((SELECT COALESCE(MAX(L.Id),0)+1  FROM EXPO_Log_Visite_Vetrina as L),'$IdImpresa','0','$IdPartecipante',NOW(),$IdEsito)";
	$db->Query($sqlInsert,null,'visualizza scheda impresa ln.31');

	$db->Query("UPDATE EXPO_T_Imprese SET VisiteTotali = (VisiteTotali)+1 WHERE Id = '$IdImpresa'",null, 'aggiornamento visite totali');
}
$sess->register(IdImpresa);
page_close();
//$query = "SELECT * FROM EXPO_T_Imprese as I JOIN EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa WHERE I.Id = '$IdImpresa'";
//$row = $db->GetRow($query);
$numProd = $db->NumRows("SELECT * FROM EXPO_T_Prodotti WHERE IdImpresa = '$IdImpresa'");
$rootDoc = selectRootDoc($db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale")) . "/";
$query = "SELECT * FROM EXPO_T_Imprese as I JOIN EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa WHERE I.Id = '$IdImpresa'";
//$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'";
$row = $db->GetRow($query);

$ordine = $row['Id_OrdineProfessionale'];
$studio = $row['IsStudio'];

$rootDocIf = Config::ROOTFILES . $rootDoc . $row['Id'];
//DETTAGLI
$sediLink = FALSE;
if (strcmp($row['InternazionalizzazioneLink'], "") != 0 || is_file($rootDocIf . "/internazionalizzazione.pdf")) {
    $sediLink = TRUE;
}
$referenzeLink = FALSE;
if (strcmp($row['ReferenzeLink'], "") != 0 || is_file($rootDocIf . "/referenze.pdf")) {
    $referenzeLink = TRUE;
}
$lingue = FALSE;
$queryLingue = "SELECT Lingua,EXPO_TJ_Imprese_Lingue.Id AS IdL FROM EXPO_Tlk_Lingue,EXPO_TJ_Imprese_Lingue
          WHERE EXPO_TJ_Imprese_Lingue.IdImpresa = '$IdImpresa' AND
          EXPO_TJ_Imprese_Lingue.IdLingua = EXPO_Tlk_Lingue.Id ORDER BY Lingua";
if ($db->NumRows($queryLingue) > 0) {
    $lingue = TRUE;
}

//RETI
$reti = FALSE;
$queryRete = "SELECT expo_tj_imprese_retiimpresa.Id_Impresa, expo_t_retiimpresa.NomeRete, expo_t_retiimpresa.Id
FROM expo_t_retiimpresa INNER JOIN expo_tj_imprese_retiimpresa ON expo_t_retiimpresa.Id = expo_tj_imprese_retiimpresa.Id_Rete
WHERE expo_tj_imprese_retiimpresa.Id_Impresa = $IdImpresa ORDER BY expo_t_retiimpresa.NomeRete;
          		";
if ($db->NumRows($queryRete) > 0) {
$reti = TRUE;
}

//CARATTERISTICHE
$carateristicheLink = FALSE;
if (is_file($rootDocIf . "/master.pdf") == 0 || is_file($rootDocIf . "/pCertificazione.pdf")) {
    $carateristicheLink = TRUE;
}
$green = FALSE;
$IdTipo = "I" . $IdImpresa;
$testoGreen = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = 'qualita' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
if (strcmp($testoGreen, "") != 0 || strcmp($row['Qualita_Green'], "") != 0) {
    $green = TRUE;
}

$greenLink = FALSE;
if (strcmp($row['Qualita_GreenLink'], "") != 0 || is_file($rootDocIf . "/qualita.pdf")) {
    $greenLink = TRUE;
}

$certificazioniLink = FALSE;
if (is_file($rootDocIf . "/certificazioni.pdf")) {
    $certificazioniLink = TRUE;
}
//Associazioni/forme di aggregazione
$reteLink = FALSE;
if (strcmp($row['RetiImpresaLink'], "") != 0 || is_file($rootDocIf . "/retiImpresa.pdf")) {
    $reteLink = TRUE;
}
$associazioni = FALSE;
$queryAss = "SELECT EXPO_TJ_Imprese_Associazioni.Id AS Id,EXPO_Tlk_Associazioni.Denominazione AS Denominazione FROM EXPO_Tlk_Associazioni,EXPO_TJ_Imprese_Associazioni
                                    WHERE EXPO_TJ_Imprese_Associazioni.IdImpresa = '$IdImpresa' AND
                                      EXPO_TJ_Imprese_Associazioni.IdAssociazione = EXPO_Tlk_Associazioni.id ORDER BY EXPO_TJ_Imprese_Associazioni.Id";
if ($db->NumRows($queryAss) > 0) {
    $associazioni = TRUE;
}


$queryProdotti = "SELECT Id,Nome FROM EXPO_T_Prodotti WHERE IdImpresa = '$IdImpresa' AND StatoProdotto = 'A' ORDER BY Nome";
$prodotti = FALSE;
if ($db->NumRows($queryProdotti) > 0) {
    $prodotti = TRUE;
}
?>
<div id="bottomBox">
    <a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
    <?php if (strcmp($page, "rc") == 0) { ?>
        <a class="buttonsLista" draggable="false" href="#" onclick="selezioneRichiestaImpresa('<?php echo $IdImpresa; ?>')"><?php echo _RICHIESTA_; ?></a> 
    <?php } ?>
</div>
<input type="hidden" id="IdImpresa" name="IdImpresa" value="<?php echo $IdImpresa;?>"/>
<div class="listaParametri">
    <div id="tabs">
        <ul>
            <li><a draggable="false" href="#tabs-1"><?php echo _DATI_GENERALI_; ?></a></li>
            <li><a draggable="false" href="#tabs-2"><?php echo _C_MERC_; ?></a></li>
            <li><a draggable="false" href="#tabs-3"><?php echo _DETTAGLI_; ?></a></li>
            <li><a draggable="false" href="#tabs-4"><?php echo _QUALIT_T_; ?></a></li>
            <li><a draggable="false" href="#tabs-5"><?php echo _ASSOCIZIONI_RETI_; ?></a></li>
            <?php if ($prodotti) { ?><li><a draggable="false" href="#tabs-6"><?php echo _PRODOTTI_; ?></a></li><?php } ?>
        </ul>
        <div id="tabs-1" style="width: 100%;">

             <h4><?php 	if ( $ordine== 0){ 
						echo _TIT_1_;
					} else if (strcmp($studio,"Y")== 0){
						echo _TIT_1_STUDIO_;
					} else if (strcmp($studio,"N")== 0){
						echo _TIT_1_LP_;
					} 
			?></h4>
            <div class="containerData">
                <table width="95%">
                    <tr>
                        <td><b><?php if ($ordine > 0){echo _RAGSOC_PROF_;}else {echo _RAGSOC_;} ?>:</b> <span><?php echo $row['RagioneSociale']; ?></span></td>
                        <td rowspan="3"><?php echo viewLogo($IdImpresa,$row['WebSiteURL']); ?></td>
                    </tr>
                    <?php if (strcmp($row['CapitaleSociale'], "0") != 0 && strcmp($row['CapitaleSociale'], "") != 0) { ?>
                        <tr>
                            <td><b><?php echo _CAPISOC_; ?>:</b> <span><?php echo $row['CapitaleSociale']; ?> &euro;</span></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td ><b><?php echo _SEDE_LEG_; ?>:</b> <span><?php echo $row['Indirizzo']; ?> 
                                <?php echo $row['Cap']; ?> - <?php echo $row['Citta']; ?> ( <?php echo $row['Pr']; ?> )
                            </span>
                        </td>
                    </tr> 

                    <tr>
                        <td colspan="2"><b><?php if ($ordine > 0){echo _N_DIP_ORDINI_;}else {echo _N_DIP_;} ?>:</b> <span>
                                <?php echo $row['NumeroDipendenti']; ?></span>
                        </td>
                    </tr>
                    <tr>                   
                        <td>
                            <br><?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/descrizione", _DESCRIZIONE_FILE_); ?>
                            <?php echo getAllegati('link', $row['DescrizioneAttivitaLink']); ?>
                        </td>
                        <td><?php if (strcmp($row['IsSiexpo2015'], "Y") == 0){
									//Immagine presente nel catalgo Siexpo
									//echo '<a href="http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295" target="_blank"><label for="isSiexpo" >Presente nel Catalogo <img src="/images/expo2015/siexpo.jpg"  style="vertical-align: middle;margin-left: 5px;width: 100px"/></label></a>';
                        			$query="SELECT * FROM T_Anagrafica WHERE Id = '$uid'";
                        			echo '<form  name="siexpoForm" id="siexpoForm" >
            <input type="hidden" name="user" value="'. $db->GetRow($query,"Login").'">
            <input type="hidden" name="e-mail" value="'. $db->GetRow($query,"E_Mail").'">
            <input type="hidden" name="nomeOP" value="'. $db->GetRow($query,"Nome").'">
            <input type="hidden" name="cognomeOP" value="'. $db->GetRow($query,"Cognome").'">
            <input type="hidden" name="PaeseOP" value="'. $db->GetRow($query,"RagSoc").'">
            <input type="hidden" name="Lingua" value="'; if ($Lang == "FR") $Lang = "EN"; echo $Lang; 
            echo '">
            <input type="hidden" name="CFImpresa" value="">
            <input type="hidden" name="okurl" value="/index.phtml?Id_VMenu=293&ImpCF='.$row['CodiceFiscale'].'">
            <input type="hidden" name="cyph" value="'. md5($db->GetRow($query,"Login").  date("ymd") ."siexpoexpo2015@@@").'">';            
            echo _ANTEP_IMP_ .'</form>';
								} else {
									
									echo '&nbsp;';
								}?></td>
                    </tr>
                </table>
            </div>
            <h4><?php echo _TIT_2_; ?></h4>
            <div class="containerData">
                <table width="95%">
                    <?php if (strcasecmp($row['Telefono'], "") != 0) { ?>
                        <tr>
                            <td ><b><?php echo _TEL_; ?>:</b> <span><?php echo prefissoTelefono($row['Telefono']); ?></span></td>
                        </tr>
                    <?php } ?>
                    <?php if (strcasecmp($row['Fax'], "") != 0) { ?>
                        <tr>
                            <td ><b>Fax:</b> <span><?php echo prefissoTelefono($row['Fax']); ?></span></td>
                        </tr>
                    <?php } ?>
                    <?php if (strcasecmp($row['WebSiteURL'], "") != 0) { ?>
                        <tr>
                            <td ><b>Web Site:</b> 
                                <a href="<?php echo getLinkWeb($row['WebSiteURL']); ?>" target="_blank"><?php echo $row['WebSiteURL']; ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <h4><?php echo _TIT_3_; ?></h4>
            <div class="containerData" >
                <table  width="95%">
                    <tr>
                        <td ><b><?php echo _U_FATTURATO_; ?>:</b> <span><?php echo number_format(doubleval($row['UltimoFatturato']), 0, ',', '.'); ?>  &euro;</span></td>
                    </tr> 
                    <?php if (strcmp($row['FatturatoUltimoEsercizio'], "0") != 0 && strcmp($row['FatturatoUltimoEsercizio'], "") != 0) { ?>
                        <tr>
                            <td ><b><?php echo _U_FATTURATO_ESERC; ?>:</b> <span><?php echo number_format(doubleval($row['FatturatoUltimoEsercizio']), 0, ',', '.'); ?>  &euro;</span></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td >
                            <br><?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/bilanci", _BILANCI_FILE_); ?> 
                        </td>
                    </tr>
                  </table>
            </div> 
             <h4><?php echo _DESCRIZIONE_ATT_; ?></h4>
                    <!-- <tr>
                        <td ><b><?php //echo _DESCRIZIONE_ATT_; ?>:</b></td>
                    </tr> -->
                    <div class="containerData" >
                <table  width="95%">
                    <tr>                   
                        <td ><span>
                                <?php
                                $IdTipo = "I" . $IdImpresa;
                                $testo = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = 'descrizione' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
                                if (strcmp($testo, "") != 0)
                                    echo html_entity_decode($testo);
                                else
                                    echo html_entity_decode($row['DescrizioneAttivita']);
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>



        </div>
        <div id="tabs-2" style="width: 100%;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <h4><?php echo _C_MERC_; ?></h4>
                        <ul>
                            <?php
                            $query = "SELECT TJ.Id AS Id,DescEstesa,DescEstesa_AL,DescEstesa_AL2 ,FlagNote,Note
                                FROM EXPO_TJ_Imprese_Categorie AS TJ
                                RIGHT JOIN EXPO_Tlk_Categorie AS T ON TJ.IdCategoria = T.Id 
                                WHERE TJ.IdImpresa = '$IdImpresa'                               
                                 AND  TJ.IsIscritta = 'Y'
                                      ORDER BY DescEstesa";
                            foreach ($db->GetRows($query) AS $rows) {

								$infoNota = '';
								if (strcmp ($rows['FlagNote'],'Y') == 0) {
								
									$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
									/*if ((strcmp ($rows['Note'],'_DEFAULT_') != 0)){
										$nota = $rows['Note'];
									}*/
								
									$infoNota = " <a href=\"#\" class=\"tooltip\" draggable=\"false\" >
                                        <img src=\"tmpl/expo2015VT/vetrina/images/infoGrey.png\" />
                                        <span>                                           
                                        ".$nota."    <br><br>
                                        </span>
                                    </a>";
								}
								
                                switch ($Lang) {
                                    case 'IT':
                                        ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa']); echo $infoNota;?></li><?php
                                        break;
                                    case 'EN':
                                        ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa_AL']); echo $infoNota;?></li><?php
                                            break;
                                        case 'FR':
                                            ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa_AL2']); echo $infoNota;?></li><?php
                                            break;
                                        default:
                                            break;
                                    }

                                    
                                    
                                    
                                }
                                ?>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div id="tabs-3" style="width: 100%;">

        <?php if ($ordine == 0){?>
            <h4><?php echo _T_IMPRESA_; ?></h4>
            <div class="containerData">
                <table width="95%">

                    <tr>
                        <td >
                            <?php echo getCheckImg($row['IsGeneralContractor']); ?> <?php echo strikeText($row['IsGeneralContractor'],_GENER_CONT_); ?><br>
                            <?php echo getCheckImg($row['IsProduttore']); ?> <?php echo strikeText($row['IsProduttore'],_PRODUTTORE_); ?><br>
                            <?php echo getCheckImg($row['IsDistributore']); ?> <?php echo strikeText($row['IsDistributore'],_DISTRIBUTORE_); ?><br>
                            <?php echo getCheckImg($row['IsPrestatoreDiServizi']); ?> <?php echo strikeText($row['IsPrestatoreDiServizi'],_IsPrestatoreDiServizi_); ?>
                        </td>
                    </tr>
                </table>           
            </div>
		<?php }?>

            <h4><?php if ( $ordine== 0){echo _SEDI_;} else if (strcmp($studio,"Y")== 0){echo _SEDI_STUDIO_;} else if (strcmp($studio,"N")== 0){echo _SEDI_LP_;} ?></h4>
            <div class="containerData">
                <table width="95%">
                    <tr>
                        <td> 
                            <?php echo getCheckImg($row['IsSediEstere']); ?> <?php echo strikeText($row['IsSediEstere'],_S_ESTERA_); ?><br>
                            <?php echo getCheckImg($row['IsNetwork']); ?> <?php echo strikeText($row['IsNetwork'],_NET_INTER_); ?>
                        </td>
                    </tr>
                    <?php if ($sediLink) { ?>
                        <tr>
                            <td >
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/internazionalizzazione", _INTERNAZIONALIZZAZIONE_FILE_); ?>
                                <?php echo getAllegati('link', $row['InternazionalizzazioneLink']); ?>
                            </td>
                        </tr>  
                    <?php } ?>
                </table>
            </div>

            <h4><?php echo _REFERENZE_; ?></h4>
            <div class="containerData">
                <table width="95%">
                    <tr>

                        <td width="60%">
                            <?php echo getCheckImg($row['IsPartecipazioneExpo']); ?> <?php echo strikeText($row['IsPartecipazioneExpo'],_PARTECIPZIONI_EXPO_); ?><br>
                            <?php echo getCheckImg($row['IsPartecipazioniInternazionali']); ?> <?php echo strikeText($row['IsPartecipazioniInternazionali'],_PAR_INTERNAZIONALI_); ?><br>
                        </td>
                    </tr>
                    <?php if ($referenzeLink) { ?>
                        <tr>
                            <td >
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/referenze", _REFERENZE_FILE_); ?>
                                <?php echo getAllegati('link', $row['ReferenzeLink']); ?>
                            </td>  
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <h4><?php echo _CON_LINGUE_; ?></h4>
            <div class="containerData">

                <table width="95%">
                    <tr>
                        <td  >

                            <?php
                            if ($lingue) {
                                echo "<ul>";
                                foreach ($db->GetRows($queryLingue) AS $rows) {
                                    ?>
                            <li><?php echo $rows ['Lingua']; ?> </li>

                            <?php
                        }
                        echo "</ul>";
                    } else {
                        echo _NO_LINGUE_;
                    }
                    ?>

                    </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="tabs-4" style="width: 100%;">

            <h4><?php 	if ( $ordine == 0){ 
							echo _QUALIT_;
						} else if (strcmp($studio,"Y")== 0){
							echo _QUALIT_STUDIO_;
						} else if (strcmp($studio,"N")== 0){
							echo _QUALIT_LP_;
						} 
				?>
			</h4>
            <div class="containerData">
                <table width="95%">
                    <tr>
                        <td >
                            <?php if ($ordine == 0) { echo getCheckImg($row['IsModello']); ?> <?php echo strikeText($row['IsModello'], _A_MODELLO_);echo "<br>"; }?>
                            <?php echo getCheckImg($row['IsMasterSpecialistico']); ?> <?php echo strikeText($row['IsMasterSpecialistico'],_P_MASTER_);; ?><br>
                            <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/master", _P_MASTER_FILE_); ?>
                            <?php echo getCheckImg($row['IsCertificazione']); ?> <?php echo strikeText($row['IsCertificazione'], _P_CERTIFICAZIONE_); ?><br>
                            <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/pCertificazione", _P_CERTIFICAZIONE_FILE_); ?>
                        </td>
                    </tr>
                </table>
            </div>

            <?php //if ($certificazioni || $certificazioniLink) { ?>
            <h4><?php echo _CERTIFICAZIONI2_; ?></h4>
            <div class="containerData">
                <table width="95%">
                    <tr>
                        <td >

                            <?php
                            $LangTab = "Descrizione";
                            if (strcmp($Lang, "IT") != 0)
                                $LangTab .= "_" . $Lang;
                            $query = "SELECT * FROM EXPO_Tlk_Certificazioni AS C join EXPO_TJ_Certificazioni_OrdiniProfessionali AS TJ ON C.Id = TJ.Id_Certificazione WHERE TJ.Id_OrdineProfessionale = $ordine ORDER BY C.IDCert";
                            //$query = "SELECT * FROM EXPO_Tlk_Certificazioni ORDER BY IDCert";
                            foreach ($db->GetRows($query) AS $rows) {
                                $query_check = "SELECT *
                                          FROM EXPO_TJ_Imprese_Certificazione
                                          where IdImpresa='" . $IdImpresa . "'
                                              and IdCertificazione='" . $rows['Id'] . "'";

                                $checked = "N";
                                if ($db->NumRows($query_check) > 0) {
                                    $checked = 'Y';
                                }
                                ?>
                                <div class="divCertificazioni">
                                    <?php echo getCheckImg($checked); ?> <?php echo strikeText($checked,$rows ['IDCert'] );?> 
                                    <a href="#" class="tooltip" draggable="false" >
                                        <img src="tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                                        
                                        <?php ?>
                                        <span>                                           
                                            <?php echo $rows[$LangTab]; ?><br><br>
                                        </span>
                                    </a>
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php if ($certificazioniLink) { ?>
                        <tr>
                            <td>
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/certificazioni", _CERTIFICAZIONI_FILE_); ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            
            <?php if ($green || $greenLink) { ?>
                <h4><?php echo _GREEN_; ?></h4>
                <div class="containerData">
                    <table width="95%">
                        <?php if ($green) { ?>
                            <tr>
                                <td >
                                    <?php
                                    if (strcmp($testoGreen, "") != 0)
                                        echo html_entity_decode($testoGreen);
                                    else
                                        echo html_entity_decode($row['Qualita_Green']);
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($greenLink) { ?>
                            <tr>
                                <td >
                                    <br><?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/qualita", _QUALITA_FILE_); ?>
                                    <?php echo getAllegati('link', $row['Qualita_GreenLink']); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php } ?>


        </div>
        <div id="tabs-5" style="width: 100%;">
            <h4><?php if ( $ordine== 0){echo _APP_RETE_IMP_;} else if ( $ordine > 0){echo _APP_RETE_IMP_PROF_;}?></h4>
            <div class="containerData">
                <table width="95%">
                    <tr>
                        <td  >

                            <?php
                            if ($reti) {
                                echo "<ul>";
                                foreach ($db->GetRows($queryRete) AS $rows) {
                                    echo '<li><a draggable="false" href="javascript:visualizzaBOX(\'RTI\',\'' . $rows['Id'] . '\')" >' . $rows['NomeRete'] . '</a><input type="hidden" id="esitoRTI'.$rows['Id'].'" name="esitoRTI'.$rows['Id'].'" value="'.$IdEsito.'"/></li>';
                        }
                        echo "</ul>";
                    } else {
                        echo _NO_RETI_;
                    }
                    ?>

                    </td>
                    </tr>
                </table>
            </div>
            <!--
                    <h4><?php echo _APP_ASS_; ?></h4>
                    <div class="containerData">
                        <table width="95%">
                            <tr>
                                <td >
            <?php
            if ($associazioni) {
                foreach ($db->GetRows($queryAss) AS $rows) {
                    ?>
                                                                                                    <div class="divLingue"><?php echo $rows ['Denominazione']; ?> </div>
                                                                    
                    <?php
                }
            } else {
                echo _NO_ASSSOCIAZIONI_;
            }
            ?>
                                </td>
                            </tr>
                        </table>
                    </div>
            -->
        </div>

        <div id="tabs-6" style="width: 100%">
            <?php if ($prodotti) { ?>
                <h4><?php echo _PRODOTTI_; ?></h4>
                <ul>
                    <?php
                    foreach ($db->GetRows($queryProdotti) AS $row) {
                        echo '<li><a draggable="false" href="javascript:visualizzaBOX(\'PRT\',\'' . $row['Id'] . '\')" >' . $row['Nome'] . '</a><input type="hidden" id="esitoPRT'.$row['Id'].'" name="esitoPRT'.$row['Id'].'" value="'.$IdEsito.'"/></li>';
                    }
                    ?>
                </ul>
            <?php } ?>
        </div>

    </div>
</div>
<?php }else {
	?>
	
	<div class="contentHeader">
		<div id="bottomBox">
			<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
		</div>
	</div>
	
		<div class="error" style="text-align: center; font-size: 18px;">
			<p class="error"><?php echo _IMPRESA_TEMPORANEAMENTE_OFFLINE_ ?></p>
		</div>
	
	<?php 
}
//}

} else{
	?>

<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>