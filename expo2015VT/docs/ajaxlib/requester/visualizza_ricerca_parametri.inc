<script type="text/javascript">
$(document).ready(function() {

    $('#paramAll').change(function() {
         if ($(this).is(":checked")) {
             $("#form_parametri :checkbox").prop("checked", true);
         } else {
             $("#form_parametri :checkbox").attr('checked', false);
         }
     });   

    $('#tooltipSelect li').hover(
            function () {
                //mostra sottomenu
                $('ul', this).stop(true, true).slideDown(0);
     
            }, 
            function () {
                //nascondi sottomenu
                $('ul', this).stop(true, true).slideUp(200);        
            }
        );

});


$(document).keyup(function(e) {
	  
	  if (e.keyCode == 27) { chiudi(); }   // esc
	});

function selezionaTutti(){
    
	$("#form_parametri :checkbox").prop("checked", true);
	$("#paramAll").prop("checked", true);	
	}  

function delesezionaTutti(){
    
	$("#form_parametri :checkbox").prop("checked", false);
	$("#paramAll").prop("checked", false);	
	}  

function invertiSelezione(){
    
   $("#form_parametri :checkbox").each(function(){
    	$(this).prop("checked",!$(this).prop("checked"));
	});
  
}  

</script>
<?php
//include "configuration.inc";



//$idImpresaAllow = ($_POST['Id']);

$utypeArrayAllow = array(98);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){

$Lang = $_REQUEST['lang'];
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();
$azione = $_REQUEST['action'];
$parametro = $_REQUEST['parametro'];
$numCol = 3;

//VISUALIZZAZIONE
if (strcmp($azione, "VIEW") == 0) {

    $arrayTitoli = array("certificazioni" => _CERTIFICAZIONI2_, "associazioni" => _APP_ASS_, "lingue" => _CON_LINGUE_, "categoria" => _C_MERC_);
    ?>
    <script language="JavaScript" type="text/javascript">
        function inserisciParametri(idPar) {
            var addPar = "";
            var ff = document.getElementById("form_parametri");
            for (var i = 0; i < ff.elements.length; i++) {
                if (ff.elements[i].checked)
                    addPar += "&" + ff.elements[i].id + "=" + ff.elements[i].value;
            }

            $.ajax({
                type: "GET",
                async: false,
                url: "ajaxlib/requester/visualizza_ricerca_parametri.inc",
                data: 'lang=<?php echo $Lang; ?>&action=INS&parametro=' + idPar + addPar,
                success: function(response) {
                    $("#" + idPar).html(response);
                    document.getElementById("coprente").style.display = 'none';
                }});

        }
    </script>
    <div id="bottomBox">
        <a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
        <a class = "buttonsLista" href="javascript:inserisciParametri('<?php echo $parametro; ?>')" draggable = "false" ><?php echo _SALVA_; ?></a>
      <h2 style="text-align: center;"><?php echo $arrayTitoli[$parametro]; ?></h2> 
    </div>

    <div class="listaParametri">
	
	<ul id="tooltipSelect">
		<li><input type="checkbox" id="paramAll" name="paramAll"  value="Y"><img src="images/expo2015/freccia_giu_grigio.png"/>
			<ul class="selectBox">
				<li><input class="selectBox" type="button" value="<?php ECHO _TUTTE_?>" onclick="selezionaTutti()" /></li>
				<li><input class="selectBox" type="button" value="<?php ECHO _NESSUNA_?>" onclick="delesezionaTutti()" /></li>
				<li><input class="selectBox" type="button" value="<?php ECHO _INVERTI_?>" onclick="invertiSelezione()" /></li>
			</ul>
			<div class="clear"></div>
		</li>
	</ul>

       <form id="form_parametri" >
        
            <?php
            if (strcmp($parametro, "certificazioni") == 0) {
                ?>

                <?php
                $LangTab = "Descrizione";
                if (strcmp($Lang, "IT") != 0)
                    $LangTab .= "_" . $Lang;

                $arryCol = array();
                $query = "SELECT DISTINCT * FROM EXPO_Tlk_Certificazioni ORDER BY IDCert";

                foreach ($db->GetRows($query) AS $rows) {
                    $arryCol[] = '<input type="checkbox" id="certificazioni' . $rows['Id'] . '" name="certificazioni' . $rows['Id'] . '" value="Y"  ' . getCheck(settaVar($_GET, 'certificazioni' . $rows['Id'], '')) . ' >' . $rows ['IDCert'] . '   
                                    <a href="#" class="tooltip" draggable="false" >
                                        <img src="/tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                                        <span>                                            
                                            ' . $rows[$LangTab] . '<br><br>
                                        </span>
                                    </a><br>';
                }

                echo ceateTab($arryCol, 1);
            } else if (strcmp($parametro, "associazioni") == 0) {
                ?>

                <?php
                $arryCol = array();
                $query = "SELECT DISTINCT Denominazione, Id FROM EXPO_Tlk_Associazioni ORDER BY Denominazione";
                foreach ($db->GetRows($query) AS $rows) {
                    $arryCol[] = '<input type="checkbox" id="associazioni' . $rows['Id'] . '" name="associazioni' . $rows['Id'] . '" value="Y"  ' . getCheck(settaVar($_GET, 'associazioni' . $rows['Id'], '')) . ' >' . $rows['Denominazione'] . '<br>';
                }

                echo ceateTab($arryCol, $numCol);
            } else if (strcmp($parametro, "lingue") == 0) {
                ?>

                <?php
                $maxRighe = 25;
                $arryCol = array();
                $iRiga = 0;
                $iCol = 0;
                $tagLigue = "Lingua";
                if (strcmp($Lang, "IT") != 0) {
                    $tagLigue .= "_" . $Lang;
                }
                $in = "";
                $query = "SELECT $tagLigue, Id,SUBSTRING($tagLigue,1,1) AS Iniziale FROM EXPO_Tlk_Lingue  ORDER BY $tagLigue";
                foreach ($db->GetRows($query) AS $rows) {
                    if (strcmp($in, $rows['Iniziale']) != 0) {
                        if ($iRiga == $maxRighe) {
                            $iCol++;
                            $iRiga = 0;
                        }

                        $in = $rows['Iniziale'];
                        $arryCol[$iCol][$iRiga] = "<strong>" . strtoupper($in) . "</strong>";

                        $iRiga++;
                    }

                    if ($iRiga == $maxRighe) {
                        $iCol++;
                        $iRiga = 0;
                    }


                    $arryCol[$iCol][$iRiga] = '<input type="checkbox" id="lingue' . $rows['Id'] . '" name="lingue' . $rows['Id'] . '" value="Y" ' . getCheck(settaVar($_GET, 'lingue' . $rows['Id'], '')) . ' class="selezionabile" >' . $rows[$tagLigue] . '<br>';

                    $iRiga++;
                }

                echo ceateTabMatr($arryCol, $iCol, $maxRighe);
            } else if (strcmp($parametro, "categoria") == 0) {
                $descAl = array("IT" => "", "FR" => "_AL2", "EN" => "_AL");
                ?>

                <?php
                $arryCol = array();
                $query1 = "SELECT Id_Categoria,Descrizione,Descrizione_AL,Descrizione_AL2 FROM  `EXPO_Tlk_Categorie` WHERE LENGTH(Id_Categoria) = 2 ORDER BY Id_Categoria";
                foreach ($db->GetRows($query1) AS $rows1) {
                    echo '<h3>' . $rows1['Descrizione' . $descAl[$Lang]] . '</h3>';
                    $Id_Categoria = $rows1['Id_Categoria'];
                    $query = "SELECT Id,DescEstesa,DescEstesa_AL,DescEstesa_AL2,FlagNote,Note FROM  `EXPO_Tlk_Categorie` WHERE Id_Categoria LIKE '$Id_Categoria%' AND LENGTH(Id_Categoria) = 8 ORDER BY Id_Categoria";
                    foreach ($db->GetRows($query) AS $rows) {
					$infoNota = '';
					if (strcmp ($rows['FlagNote'],'Y') == 0) {
					
						$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
						/*if ((strcmp ($rows['Note'],'_DEFAULT_') != 0)){
						 $nota = $rows['Note'];
						}
						*/
						$infoNota = " <a href=\"#\" class=\"tooltip\" draggable=\"false\" >
                                        <img src=\"tmpl/expo2015VT/vetrina/images/infoGrey.png\" />
                                        <span>
                                        ".$nota."    <br><br>
                                        </span>
                                    </a>";
						}
                        echo '<input type="checkbox" id="categoria' . $rows['Id'] . '" name="categoria' . $rows['Id'] . '" value="Y" ' . getCheck(settaVar($_GET, 'categoria' . $rows['Id'], '')) . '  >' . $rows["DescEstesa" . $descAl[$Lang]] . $infoNota. '<br>';
                    }
                }
            }
            ?>

        </form>
    </div>

    <?php
    //INSERIMENTO
} elseif (strcmp($azione, "INS") == 0) {
    if (strcmp($parametro, "certificazioni") == 0) {
        $i = 0;

        $query = "SELECT DISTINCT Id,IDCert FROM EXPO_Tlk_Certificazioni ORDER BY IDCert";
        $viewParametri = _R_N_PARAMETRI_ . "<br>";
        foreach ($db->GetRows($query) AS $rows) {
            if (isset($_GET['certificazioni' . $rows['Id']])) {
                echo '<input type ="hidden" name="certificazioni' . $rows['Id'] . '" id = "certificazioni' . $rows['Id'] . '" value = "Y"/>';
                $viewParametri .= $rows['IDCert'] . "<br>";
                $i++;
            }
        }

        echo '<a draggable="false" href="javascript:aggiungiParametri(\'certificazioni\')">(' . $i . ')</a>';
        if ($i > 0) {
            echo '<a href="javascript:aggiungiParametri(\'certificazioni\')" class="tooltip" draggable="false" >
                        <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                        <span> 
                        ' . $viewParametri . '
                        </span>
                    </a>';
        }
    } elseif (strcmp($parametro, "associazioni") == 0) {
        $i = 0;
        $viewParametri = _R_N_PARAMETRI_ . "<br>";
        $query = "SELECT DISTINCT Denominazione, Id FROM EXPO_Tlk_Associazioni ORDER BY Denominazione";
        foreach ($db->GetRows($query) AS $rows) {
            if (isset($_GET['associazioni' . $rows['Id']])) {
                echo '<input type ="hidden" name="associazioni' . $rows['Id'] . '" id = "associazioni' . $rows['Id'] . '" value = "Y"/>';
                $i++;
            }
        }
    } elseif (strcmp($parametro, "lingue") == 0) {
        $i = 0;
        $viewParametri = _R_N_PARAMETRI_ . "<br>";
        $tagLigue = "Lingua";
        if (strcmp($Lang, "IT") != 0) {
            $tagLigue .= "_" . $Lang;
        }
        $query = "SELECT * FROM EXPO_Tlk_Lingue  ORDER BY $tagLigue";
        foreach ($db->GetRows($query) AS $rows) {
            if (isset($_GET['lingue' . $rows['Id']])) {
                echo '<input type ="hidden" name="lingue' . $rows['Id'] . '" id = "lingue' . $rows['Id'] . '" value = "Y"/>';
                $viewParametri .= $rows[$tagLigue] . "<br>";
                $i++;
            }
        }

        echo '<a draggable="false" href="javascript:aggiungiParametri(\'lingue\')">(' . $i . ')</a>';
        if ($i > 0) {
            echo '<a href="javascript:aggiungiParametri(\'lingue\')" class="tooltip" draggable="false" >
                        <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                        <span> 
                        ' . $viewParametri . '
                        </span>
                    </a>';
        }
    } elseif (strcmp($parametro, "categoria") == 0) {
        $i = 0;
        $viewParametri = _R_N_PARAMETRI_ . "<br>";
        $query = "SELECT Id,DescEstesa FROM  `EXPO_Tlk_Categorie` WHERE LENGTH(Id_Categoria) = 8 ORDER BY Descrizione";
        foreach ($db->GetRows($query) AS $rows) {
            if (isset($_GET['categoria' . $rows['Id']])) {
                echo '<input type ="hidden" name="categoria' . $rows['Id'] . '" id = "categoria' . $rows['Id'] . '" value = "Y"/>';
                $viewParametri .= $rows["DescEstesa" . $descAl[$Lang]] . "<br>";
                $i++;
            }
        }
        echo '<a draggable="false" href="javascript:aggiungiParametri(\'categoria\')">(' . $i . ')</a>';
        if ($i > 0) {
            echo '<a href="javascript:aggiungiParametri(\'categoria\')" class="tooltip" draggable="false" >
                        <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                        <span> 
                        ' . $viewParametri . '
                        </span>
                    </a>';
        }
    }
}


 }
//}

else{
	?>
<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error"><?php echo _ACCESSO_NEGATO_?></p>
	</div>

<?php 
}

?>

