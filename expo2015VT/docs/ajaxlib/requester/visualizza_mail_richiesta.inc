<?php
//include "configuration.inc";


$utypeArrayAllow = array(98);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){



require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/languages/" . $_REQUEST['lang'] . ".inc");
$db = new DataBase();

$prodottiId = array();
$prodottiNome = array();
$impreseId = array();
$impreseNome = array();
$ind = 0;
while (strcmp(settaVar($_REQUEST, "richiestaProd" . $ind, ""), "") != 0) {
    $idProd = settaVar($_REQUEST, "richiestaProd" . $ind, "");

    $query = "SELECT IdImpresa,Nome FROM EXPO_T_Prodotti WHERE Id = '$idProd' ";
    $idImpresa = $db->GetRow($query, "IdImpresa");
    if (!in_array($idImpresa, $impreseId)) {
        $impreseId[] = $idImpresa;
        $impreseNome[$db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", "RagioneSociale")] = $idImpresa;
    }
    $nomeProd = $db->GetRow($query, "Nome");
    $prodottiNome[$nomeProd] = $idImpresa;
    $prodottiId[] = $idProd;
    $ind++;
}

$ind = 0;
while (strcmp(settaVar($_REQUEST, "richiestaImp" . $ind, ""), "") != 0) {
    $idImp = settaVar($_REQUEST, "richiestaImp" . $ind, "");

    if (!in_array($idImp, $impreseId)) {
        $impreseId[] = $idImp;
        $impreseNome[$db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$idImp'", "RagioneSociale")] = $idImp;
    }
    $ind++;
}

if (count($impreseId) > 0 || count($prodottiId) > 0) {
    ?>
    <div id="bottomBox">
        <a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
        <a class="buttonsLista" draggable="false" href="#" onclick="javascript:invia('index.phtml?Id_VMenu=299&amp;azione=SEND&amp;option=MLT')"><?php echo _INVIA_; ?></a>
    </div>

    <h2><?php echo _RICHIESTA_; ?></h2>
    <form enctype="multipart/form-data" id="form_moduli" name="form_moduli"  >
        <?php
        $ind = 0;

        foreach ($impreseId AS $value) {
            ?>
            <input type = "hidden" id="richiestaImp<?php echo $ind; ?>" name="richiestaImp<?php echo $ind; ?>" value = "<?php echo $value; ?>"/>            
            <?php
            $ind++;
        }
        $ind = 0;
        foreach ($prodottiId AS $value) {
            ?>
            <input type = "hidden" id="richiestaProd<?php echo $ind; ?>" name="richiestaProd<?php echo $ind; ?>" value = "<?php echo $value; ?>"/>            
            <?php
            $ind++;
        }
        ?>  
        <strong><?php echo _OGGETTO_; ?>:*</strong> 
        <input type = "text" id = "oggetto" name = "oggetto" size = "50" maxlength = "250" value = ""/>
        <br><br>
        <table id="tabella_elenchi"  >

            <tr>
                <td colspan="2" style="width: 50%; margin-left: 0px;padding-left: 0px;"><strong><?php echo _MESSAGGIO_; ?>:*</strong></td>

            </tr>
            <tr >
                <td  style="width: 50%;padding-top: 10px;padding-bottom: 10px;"><textarea rows="12" cols="70" id="messaggio" name="messaggio"></textarea></td>
                <td style="vertical-align: top;padding-top: 10px;padding-bottom: 10px;">
                    <strong><?php echo _IMPRESE_ . " / " . _PRODOTTI_; ?></strong>
                    <ul style="margin-left: 15px">
                        <?php
                        ksort($impreseNome);
                        foreach ($impreseNome as $key => $value) {
                            ?>
                            <li><?php
                                echo $key;
                                if (in_array($value, $prodottiNome)) {
                                    ?>
                                    <ul>
                                        <?php
                                        foreach ($prodottiNome AS $key1 => $value1) {
                                            if (strcmp($value1, $value) == 0) {
                                                ?>
                                                <li><?php echo $key1; ?></li>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>  
                    </ul>
                </td> 
            </tr>

        </table> 
    </form>
<?php } ?>
<script language="JavaScript" type="text/javascript">
    function invia(destinazione) {
        var iform = document.form_moduli;
        if (validaCampi(iform)) {
            iform.method = "post";
            iform.action = destinazione;
            iform.submit();
        }

    }

    function validaCampi(iform) {
        var formOggetto = iform.oggetto.value;
        var formMessaggio = iform.messaggio.value;
        if (formOggetto === "" || formMessaggio === "") {
            alert("<?php echo _CAMPI_OB_; ?>");
            return false;
        }
        return true;
    }
</script>

<?php }
//}

else{
	?>

<div class="contentHeader">
	<div id="bottomBox">
		<a class="buttonsChiudi" draggable="false" href="#" onclick="chiudi()" >X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error"><?php echo _ACCESSO_NEGATO_?></p>
	</div>

<?php 
}

