<?php
class Config {

      const DB_SERVER = 'test.digicamere.it',
            DB_NAME = 'expo_main',
            DB_USERNAME = 'expo2015',
            DB_PASSWORD = 'expo12!!',
            ROOTFILES = "D:/www/expo2015VT/docs/files/",
            ROOTFILESIMAGE = "files/",
			ROOTFILESHTML = "files/",
            SIZEMAX = "102400";  //KB
}

$PROGETTO = "expo2015VT";
//MAIL SITO
$MAILADMIN = "alessandro.ferla@digicamere.it";
$MAILSITO = "noreply.catalogue@expo2015.org";

//CARTELLE SITO
$PATHHOME = "/var/www/vhosts/digicamere.it/subdomains/test/";
$PATHDOCS = $PATHHOME."/httpdocs/";
$PATHLOG = $PATHDOCS . "logs/";
$PATHINC = $PATHHOME . "/include/";
$PATHPDFS = $PATHHOME . "pdf_delibere/";
$TAB_ANAGRAFICA_EXT = "T_Ext_Anagrafica";
$AZIONI = $PROGETTO . '/azioni/';

//DATI SITO
$TITLESITE = "EXPO 2015";
$DESCSITE = "EXPO 2015";
$MAINSITE = "http://test.digicamere.it/";
$HOMEURL = "http://test.digicamere.it/";

$PDMS = "https://pdmstest.expo2015.org/";

//VARIABILI SITO
$arrayRootDoc = array(
    "aziende1" => array("A", "B", "C", "D", "E", "F"),
    "aziende2" => array("G", "H", "I", "J", "K", "L"),
    "aziende3" => array("M", "N", "O", "P", "Q", "R", "S"),
    "aziende4" => array("T", "U", "V", "W", "X", "Y", "Z")
);
$templateUse = "vetrina";
$includeConfig = 1;
$includePrepend = 0;
$keySito = "1234567890abcdefghilmnop";
$DEBUG = 0;
$SPEDISCIMAIL = 1;
$LOGMAIL = 0;
$RESFORPAGE = 20;

//$tmp_upload_path = "/tmp/"; //Versione ufficiale
$tmp_upload_path = "C:\\xampp\\tmp\\"; //Versione XAMPP
?>