function popup(pagina,titolo,width,height)
{
	window.open ('/popup.phtml?pagina='+pagina,''+titolo+'','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width='+width+',height='+height);
}

function popup_scroll(pagina,titolo,width,height)
{
	window.open ('/popup.phtml?pagina='+pagina,''+titolo+'','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,copyhistory=0,width='+width+',height='+height);
}

function popup_admin(pagina,titolo,width,height)
{
	window.open (pagina,''+titolo+'','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1,copyhistory=0,width='+width+',height='+height);
}

function finestra(str)
{
    var agt=navigator.userAgent.toLowerCase();

    // *** BROWSER VERSION ***
    // Note: On IE5, these return 4, so use is_ie5up to detect IE5.
    var is_major = parseInt(navigator.appVersion);
    var is_minor = parseFloat(navigator.appVersion);

    // Note: Opera and WebTV spoof Navigator.  We do strict client detection.
    // If you want to allow spoofing, take out the tests for opera and webtv.
    var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
    var is_nav2 = (is_nav && (is_major == 2));
    var is_nav3 = (is_nav && (is_major == 3));
    var is_nav4 = (is_nav && (is_major == 4));
    var is_nav4up = (is_nav && (is_major >= 4));
    var is_navonly      = (is_nav && ((agt.indexOf(";nav") != -1) ||
                          (agt.indexOf("; nav") != -1)) );
    var is_nav6 = (is_nav && (is_major == 5));
    var is_nav6up = (is_nav && (is_major >= 5));
    var is_gecko = (agt.indexOf('gecko') != -1);


    var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
    var is_ie3    = (is_ie && (is_major < 4));
    var is_ie4    = (is_ie && (is_major == 4) && (agt.indexOf("msie 4")!=-1) );
    var is_ie4up  = (is_ie && (is_major >= 4));
    var is_ie5    = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
    var is_ie5_5  = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.5") !=-1));
    var is_ie5up  = (is_ie && !is_ie3 && !is_ie4);
    var is_ie5_5up =(is_ie && !is_ie3 && !is_ie4 && !is_ie5);
    var is_ie6    = (is_ie && (is_major == 4) && (agt.indexOf("msie 6.")!=-1) );
    var is_ie6up  = (is_ie && !is_ie3 && !is_ie4 && !is_ie5 && !is_ie5_5);

	if (is_nav4up)
	{
		// JavaScript here for Navigator 4
		 searchWin = window.open(str, "<? echo ucfirst($PROGETTO); ?>","scrollbars=yes,resizable=yes,width=550,height=600,status=no,location=no,toolbar=no,menubar=no,directories=no,screenY=100,screenX=100");
	}
	else if (is_ie4up)
	{
		// JavaScript here for IE 4 and later
		 searchWin = window.open(str, "<? echo ucfirst($PROGETTO); ?>","scrollbars=yes,resizable=yes,width=550,height=600,status=no,location=no,toolbar=no,menubar=no,directories=no,top=100,left=100");
	}

	searchWin.focus()
}

function emailCheck (emailStr, obbl) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")

if(emailStr == "")
{
	if( obbl == true)
	{
		return false
	}
	else
		return true
}
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
		return false
}
var user=matchArray[1]
var domain=matchArray[2]

if (user.match(userPat)==null) {
    return false
}

var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
		return false
	    }
    }
    return true
}

var domainArray=domain.match(domainPat)
if (domainArray==null) {
    return false
}

var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 ||
    domArr[domArr.length-1].length>3) {
   return false
}

if (len<2) {
   var errStr="Nell'indirizzo non � specificato il nome host!"
   return false
}

return true;
}

function ControllaCF(cf)
{
    var validi, i, s, set1, set2, setpari, setdisp, pi;
    if( cf == '' )  return '';
    cf = cf.toUpperCase();
    if( cf.length == 16 )
    {
		 validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		 for( i = 0; i < 16; i++ ){
			  if( validi.indexOf( cf.charAt(i) ) == -1 )
					return "Il codice fiscale contiene un carattere non valido `" +
						 cf.charAt(i) +
						 "'.\nI caratteri validi sono le lettere e le cifre.\n";
		 }
		 set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		 set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
		 setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		 setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
		 s = 0;
		 for( i = 1; i <= 13; i += 2 )
			  s += setpari.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
		 for( i = 0; i <= 14; i += 2 )
			  s += setdisp.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
		 if( s%26 != cf.charCodeAt(15)-'A'.charCodeAt(0) )
			  return "Il codice fiscale non ? corretto:\n"+
					"il codice di controllo non corrisponde.\n";
		 return "OK";
	 }
	 else if( cf.length == 11 )
	 {
		 pi=cf;
		 validi = "0123456789";
		 for( i = 0; i < 11; i++ ){
			  if( validi.indexOf( pi.charAt(i) ) == -1 )
					return "La partita IVA contiene un carattere non valido `" +
						 pi.charAt(i) + "'.\nI caratteri validi sono le cifre.\n";
		 }
		 s = 0;
		 for( i = 0; i <= 9; i += 2 )
			  s += pi.charCodeAt(i) - '0'.charCodeAt(0);
		 for( i = 1; i <= 9; i += 2 ){
			  c = 2*( pi.charCodeAt(i) - '0'.charCodeAt(0) );
			  if( c > 9 )  c = c - 9;
			  s += c;
		 }
		 if( ( 10 - s%10 )%10 != pi.charCodeAt(10) - '0'.charCodeAt(0) )
			  return "La partita IVA non ? valida:\n" +
					"il codice di controllo non corrisponde.\n";
		 return 'OK';
	 }
 	
		 
}
