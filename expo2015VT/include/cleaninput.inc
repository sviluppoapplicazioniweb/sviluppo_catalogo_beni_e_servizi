<?

function cleanvariable($array_variabili, $tipo_filtro)
{
	$out_array= array(); 

	foreach($array_variabili as $key => $value)
	{
		if (is_array($value))
			$out_array[$key] = filter_var_array($value, $tipo_filtro);
		else
			$out_array[$key] = filter_var($value, $tipo_filtro);

	}

	return $out_array;
}

function registraLog($chiave, $tipoArray, $messaggio, $oldvalue, $newvalue)
{
	global $SEMID,$CLEANERLOGFILE,$_SERVER;
	
	$sem=sem_get($SEMID);
	if(sem_acquire($sem))
	{
		$fp=fopen($CLEANERLOGFILE,"a");
		if($fp)
		{
			fprintf($fp,"[%s];%s;%s;%s;%s;%s;%s;%s;%s\n",date("D, d M H:i:s Y "),$tipoArray,$messaggio,$chiave,$oldvalue,$newvalue, $_SERVER["HTTP_REFERER"],$_SERVER["REMOTE_ADDR"],$_SERVER['HTTP_USER_AGENT']);
			fclose($fp);
		}
		sem_release($sem);
	}
}

function remove_javascript($java){

	return preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}

function remove_noscript($java){

	return preg_replace('~<\s*\bnoscript\b[^>]*>(.*?)<\s*\/\s*noscript\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}


function urldecode_array_element(&$item1, $key)
{
	$item1 = urldecode($item1);
}

function cleanvariableglobal($array_variabili, $tabella, $tipo_array="")
{
	if(empty($array_variabili))
		return true;

	foreach($array_variabili as $key => $value)
	{
		if (!(empty($tabella)))
		{
			if (is_array($value))
				array_walk($value, 'urldecode_array_element');
			else
			$value=urldecode($value);
		}

		$tempvalue=$value;
		$cleaned=0;
		$erroregrave=0;
		
//		if (empty($tabella))
//		{
			if (!(empty($value)))
			{
				switch ($key)
				{
					// variabili del tipo "01.01.03"
					case "Id_Gruppo":
					case "explode":
					case "Id_Fascia":
						$pregret=preg_match('/^(([0-9][0-9])([.][0-9][0-9])*)$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					case "Id_Menu":
						$pregret=preg_match('/^(([0-9][0-9])([.][0-9][0-9])*)$/', $value);
						if ($pregret!==1)
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
					case "my_numproc":
						$pregret=preg_match('/^([0-9\/])+$/', $value);
						if ($pregret!==1)
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
					case "Id_GiornoSettimana":
						$clean_array = array('Fri','Mon','Sat','Sun','Thu','Tue','Wed');
						if (!in_array($value,$clean_array))				
							$tempvalue = 'Fri';
						$cleaned=1;
						break;
					case "categoria":
						$pregret=preg_match('/^(([0-9]+)([,][0-9]+)*)$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					case "azione":
						//$clean_array = array('SRC','INS','UPD','SEL','PAY','END','SAL','PAYMYBANK','UPR','ADR','UPDONE','DEL','RLN','UPDALL','VIEW','SEND','CLS');
						$clean_array = array('SRC','UPD','VIEW','SEND','CLS','C','D');
						if (!in_array($value,$clean_array))				
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "action":
						//$clean_array = array('logout','Entra','INT','INS','UPD','DEL','VIEW','mybank','add','delete','ADD');
						$clean_array = array('VIEW','INS','logout','Entra');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "option":
						//$clean_array = array('user','regfirmadig','firmadig','INS','M','P','UPD');
						$clean_array = array('MLT');
						
						if (!in_array($value,$clean_array)){
							$varDecode = urldecode(base64_decode($value));
							//registraLog($key,$tipo_array,"OPTION",$value,$varDecode);
							$pregret=preg_match("/^([a-zA-Z]{8})+$/",$varDecode );
							if ($pregret!==1){
								$erroregrave=1;
								$messaggioclean="Check-base64".$key." fallito";
								$tempvalue = '';
							}else {
								$tempvalue = $value;
							}
						}
							
						$cleaned=1;
						break;
						
					case "pulsante":
					case "Pulsante":
						$clean_array = array('Aggiorna');//,'Invia');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
						
					case "tipoOperazione":
						$clean_array = array('VisualizzaFaq');//,'Invia');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "tmpl":
						$pregret=preg_match("/^(vetrina)+$/", $value);
						if ($pregret!==1){
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
							if($tempvalue<0 || $tempvalue>10)
							{
								$erroregrave=1;
								$messaggioclean="Check ".$key." fallito";
								$tempvalue="";
							}
						}
						$cleaned=1;
						break;

					case "reg_Lang":
					case "Lang2":
					case "lang":
						$pregret=preg_match("/^([A-Za-z_]{2,6})$/", $value);
					
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
						
					case "PHPSESSID":
					case "CedCamCMS_Session":
						$pregret=preg_match('/^([a-zA-Z0-9])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						else
						{
							/*$db_clean=new DB_CedCamCMS;
							$db_clean->connect();
							$sSQL="select * from active_sessions where sid='$value'";
							$db_clean->query($sSQL);
							if($db_clean->num_rows()==0)
							{
								$erroregrave=1;
								$messaggioclean="Check A".$key." fallito";
								$tempvalue="";
							}*/
						}

						$cleaned=1;
						break;
						
					case "password":
					case "Password":
					case "Password_Input":
					case "Rep_Password":
					case "New_Password":
					case "Old_Password":
		/*
						$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);
						$pregret=preg_match('/^([a-zA-Z0-9_!])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
						}
		*/
						$cleaned=1;
						break;
						

					case "a_Domanda":
						//registraLog($key,$tipo_array,"CONTROLLARE",$value,$tempvalue);
						$tempvalue = strip_tags(remove_noscript(remove_javascript($value)));
						$cleaned=1;
						break;
						
						
					case "ordina":
						$pregret=preg_match('/^([a-zA-Z0-9_\s,])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;

						break;

					case "uid":
					case "uid_utente":
					case "utype":
					case "nh":
					case "p":
					case "daabstract":
					case "tribunale":
					case "msx":
					case "Id_VMenu":
					case "idRicerca":
					case "IdLog":
					case "IdEsito":
					case "idImp":
					case "idPar":
					case "Id_Sottocategoria_Faq":
					case "Id_Sottocategoria_Faq_old":
					case "Id_Faq":
					case "IdPar":
					case "idImpresa":
						$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
						
					case "messaggio":
					case "oggetto":
					case "testo":
						$tempvalue = strip_tags(remove_noscript(remove_javascript($value)));
						//$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
						$cleaned=1;
						break;
						
					case "okurl":
						$tempvalue = filter_var($value, FILTER_SANITIZE_URL);
						$cleaned=1;
						break;
					//case "Specializzazioni":
					//	$tempvalue = filter_var_array($value, FILTER_SANITIZE_NUMBER_INT);
					//	$cleaned=1;
					//	break;
					// variabili Id
					case "cqs":
							$cqs_string=base64_decode($value);
						parse_str($cqs_string, $cqs_array);
							$cqs_array2=array();
						foreach ($cqs_array as $cqs_key => $cqs_value)
							{
								if ($cqs_key=="my_where")
									$cqs_array2[$cqs_key]= $cqs_value;
								else
									$cqs_array2[$cqs_key]= filter_var($cqs_value, FILTER_SANITIZE_STRING);
							}
							$tempvalue=base64_encode(http_build_query($cqs_array2));
							registraLog($key,$tipo_array,"CHECK CQS",$value,base64_encode(http_build_query($cqs_array2)));
							//$tempvalue=$cqs;
						$cleaned=1;
						break;

						
						
					case "nome":
					case "tab_nome":
					case "pagina":
					case "tk":
						$pregret=preg_match('/^([a-zA-Z0-9_])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
							
						
						
					case "numeroD":
					case "fatturato":
						$pregret=preg_match("/^([0-9\|])+$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);

						if ($pregret!==1){
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;

					//Nome impresa nei paremetri di ricerca	
					case "impresa":
					case "rete":
					case "cercaHtdig":
					case "_CAMPI_OB_";						
						//registraLog($key,$tipo_array,"CONTROLLARE",$value,$tempvalue);
						$tempvalue = strip_tags(remove_noscript(remove_javascript($value)));
						$cleaned=1;
						break;
					
					case "gereralContractor":
					case "produttore":
					case "distributore":
					case "IsPrestatoreDiServizi":
					case "partecipazioneExpo":
					case "partecipazioniInternazionali":
					case "sedEstera":
					case "netetwork":						
					case "aModello":
					case "pMaster":
					case "IsCertificazione":
					case "IsIscrSiexpo":
					case "retiImpresa":
						
					/*	
					case "referenzeL":
					case "IsGeneralContractor":
					case "IsProduttore":
					case "IsDistributore":
					case "IsPrestatoreDiServizi":
					case "IsSediEstere":
					case "IsNetwork":
					case "partecipazioneExpo":
					case "IsStudio":
					case "IsMasterSpecialistico":
					case "isCapogruppo":
					*/
					
						$pregret=preg_match("/^(Y|N|undefined)$/", $value);
					
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}else {
							if (strcmp($value, 'undefined') == 0){
								$tempvalue = 'N';
							}
						}
						$cleaned=1;
						break;

						
					case "parametro":	
						$pregret=preg_match("/^(certificazioni|lingue|categoria)$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
							
					case "page":
						$pregret=preg_match("/^(rc|tr)$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
						
					case "Id":
						$pregret=preg_match("/^([0-9])+$/", $value);
					
						if ($pregret!==1){
							$varTemp = base64_decode($value);
							$pregret1=preg_match("/^([0-9A-Za-z])+$/", $varTemp);
							if ($pregret1!==1){
								$erroregrave=1;
								$messaggioclean="Check-base64".$key." fallito";
								$tempvalue = '';
							}else {
								$tempvalue = $value;
							}
						} else {
								
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						}
					
						$cleaned=1;
						break;
						
					case "valore":
						$pregret=preg_match("/^([0-9\.,])+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
				
					case "tipo":
						$pregret=preg_match("/^(Prodotti|Imprese|Reti)$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}else {
							if (strcmp($value, 'undefined') == 0){
								$tempvalue = 'N';
							}
						}
						$cleaned=1;
						break;
							
						
					case "Link":
						$pregret=preg_match("/^([a-zA-Z-_])+$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}else {
							if (strcmp($value, 'undefined') == 0){
								$tempvalue = 'N';
							}
						}
						$cleaned=1;
						break;
					
					case "path":
					case "path":
						$pregret=preg_match("/^([a-zA-z0-9\.\/-_])+$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						
					default:
						
						/*if((substr($key,0,2) == "Id"))
						 {
						$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						}*/
						
		
						if (preg_match("/^(esito(IMP|PRT|RTI)[0-9]+)|(richiesta(Prod|Imp)[0-9]+)|(prodotto[0-9]+)$/", $key)==1)
						{
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
							$cleaned=1;
							
						}else 
							if (preg_match("/^(categori[e|a][0-9]+|certificazion[e|i][0-9]+|lingu[e|a][0-9]+)$/", $key)==1){
								$pregret=preg_match("/^(Y|N|undefined)$/", $value);
								//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
									
								if ($pregret!==1)
								{
									$erroregrave=1;
									$messaggioclean="Check ".$key." fallito";
									$tempvalue="";
								}else {
									if (strcmp($value, 'undefined') == 0){
										$tempvalue = 'N';
									}
								}
								$cleaned=1;
							}else {
								if ($tipo_array != "COOKIE"){
									registraLog($key,$tipo_array,"DEFAULT",$value,$tempvalue);
								}
							//registraLog($key,$tipo_array,"DEFAULT",$value,$tempvalue);
						}
						break;
				}
			}
			else
			{	
				registraLog($key,$tipo_array,"VUOTE",$value,$tempvalue);
				$cleaned=1;
			}
//		}

		if(!$cleaned && !$erroregrave)
		{
			if (isset($tabella))
			{
				foreach ($tabella as $chiave => $valore)
				{
					$campo= explode("|",$valore);


					$needlePos=strpos($chiave,$key.'|');
					if((string)$needlePos === (string)0)
					{
						switch ($campo[7])
						{
							case "TEXT":
							case "PASSWORD":
							case "HIDDEN":
							case "FILE":
							case "LABEL":
							case "LABEL2":
							case "SELECTREFRESH":
							case "SELECTFILTRO":
							case "SELECTDISTINCT":
							case "SELECTJOIN":
							case "SELECT":
							case "SELECTHIDDEN":
							case "SELECTHIDDEN2":
							case "SELECTHIDDEN3":
							case "ELENCO":
							case "DATATEXT":
							case "DATAITA":
							case "WWW":
							case "MAIL":
							case "LINKS":
							case "FUNZIONE":
							case "HTMLCONDIZ":
							case "SEPARATORE":
							case "DATA":
							case "RADIO":
							case "BUTTON":
							case "TEXTAREA":
								$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
								break;
							case "CHECKBOX":
								$tempvalue = filter_var_array($value, FILTER_SANITIZE_STRING);
								break;
							case "HTMLAREA":
								$tempvalue = mysql_real_escape_string($value);
//								$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);
								break;

							default:
								$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
								break;
						}
						$cleaned=1;
						$GLOBALS[$key]=$tempvalue;
						break;
					}
				}
			}
		}

		if (!$cleaned)
		{
			if (is_array($value))
				$tempvalue = filter_var_array($value, FILTER_SANITIZE_ENCODED);
			else
				$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);

			$cleaned=1;
			$GLOBALS[$key]=$tempvalue;
		}


		if ($cleaned==1)
		{
		//	registraLog($key,$tipo_array,"variabile pulita1",$value,$tempvalue);
			$GLOBALS[$key]=$tempvalue;
			if ($tipo_array=="GET")
				$_GET[$key]=$tempvalue;
			if ($tipo_array=="POST")
				$_POST[$key]=$tempvalue;
			if ($tipo_array=="COOKIE")
				$_COOKIE[$key]=$tempvalue;
		//	registraLog($key,$tipo_array,"variabile pulita2",$value,$tempvalue);
		}


		if ($erroregrave==1)
			registraLog($key,$tipo_array,$messaggioclean,$tempvalue,$value);
		elseif (empty($value))
		{
			//registraLog($key,"variabile pulita",$value,$tempvalue);
		}
		elseif ($tempvalue==$value || $key=="__utma" || $key=="__utmb" || $key=="__utmc" || $key=="__utmz") // || $tempvalue==urldecode($value) 
		{
			//registraLog($key,$tipo_array,"VVAriabile pulita",$value,$tempvalue);
		}
		elseif ($tempvalue!=$value && ($key=="password" || $key=="Password" || $key=="Password_Input" || $key=="Rep_Password" || $key=="New_Password" || $key=="Old_Password"))
			registraLog($key,$tipo_array,"VARIABILE MODIFICATA","PASSWORD","PASSWORD");
		elseif ($tempvalue!=$value)
			registraLog($key,$tipo_array,"VARIABILE MODIFICATA",$value,$tempvalue);
		else
			registraLog($key,$tipo_array,"VARIABILE SCONOSCIUTA",$value,$tempvalue);

	}
//	registraLog($key,"CHECK EXIT",$cleaned,$erroregrave);

	return (!$erroregrave && $cleaned==1);
}
