<?
/*************************************************
* interfaccia PHP per agire su crediti, prenotazione, sprenotazione,
	ricevere info su utenti gi� loggati
* Using: uapi_netcall($function, $parameters);
* function: prenota, sprenota, verifica, controlla,info
* parameters: cod_utente, articolo/servizio, [importo]
* il valore di ritorno pu� essere 0=ok, < 0 errore in $uapi_out_msg viene messo il messaggio di errore, in $uapi_out_par i parametri da esplodere
***************************************************************/
include $_PHPLIB["libdir"] ."db_mysql.inc";
class DB_Utenti extends DB_Sql {
  var $Host     = "weblinux";
  var $Database = "utenti";
  var $User     = "root";
  var $Password = "";
  var $Halt_On_Error = "report";
  var $Debug = false;
}
$db_utenti=new DB_Utenti;
$uapi_out_msg="";
$uapi_out_par="";
function uapi_netcall($function, $parameters)
{
global $db_utenti, $uapi_out_msg, $uapi_out_par;

$debug=0;

if($debug)
    echo "function=$function<BR>parameters=$parameters<BR>";
    parse_str($parameters);
    $sSQL="select persona_riferimento, id_utente, rag_soc, e_mail, flag_speciale from utenti left join convenzioni ON utenti.convenzione=convenzioni.convenzione WHERE cod_utente='$cod_utente'";
if($debug)
    echo "$sSQL<BR>";
    $db_utenti->query($sSQL);
    if(!$db_utenti->next_record())
    {
        $uapi_out_msg="Utente non trovato";
        return -9;
    }
    $persona_riferimento=$db_utenti->f(persona_riferimento);
    $id_utente=$db_utenti->f(id_utente);
    $rag_soc=$db_utenti->f(rag_soc);
    $e_mail=$db_utenti->f(e_mail);
    $flag_speciale=$db_utenti->f(flag_speciale);
    if(!isset($articolo) && isset($servizio))
    {
        $sSQL="select ID from articoli where codice_articolo = '$servizio'";
if($debug)
    echo "$sSQL<BR>";
        $db_utenti->query($sSQL);
        if($db_utenti->next_record())
        {
            $articolo=$db_utenti->f(ID);
        }
    }
    if(!isset($articolo))
    {
        $uapi_out_msg="E' necessario specificare il servizio/articolo";
        return -9;
    }

    $sSQL="select tipo_articolo, id_carta, articoli.descrizione from articoli INNER JOIN offerte_articoli ON ID_articolo=articoli.ID INNER JOIN offerte ON ID_offerta=offerte.ID where articoli.ID=$articolo group by tipo_articolo, id_carta limit 1";
if($debug)
    echo "$sSQL<BR>";
    $db_utenti->query($sSQL);
    if($db_utenti->next_record())
    {
        $tipo_articolo=$db_utenti->f(tipo_articolo);
        $id_carta=$db_utenti->f(id_carta);
        $descrizione_articolo=$db_utenti->f(descrizione);
        if($tipo_articolo=='A' )
            $sSQL="select count(*) as conta from acquisti where data_scadenza > NOW() and articoli like '%|$articolo|%' and ID_utente='$cod_utente'";
        else    //scalare
            $sSQL="select count(*) as conta from acquisti where tipo_articolo='S' and id_carta='$id_carta' and ID_utente='$cod_utente'";
if($debug)
    echo "$sSQL<BR>";
        $db_utenti->query($sSQL);
        $db_utenti->next_record();
        if($db_utenti->f(conta) == 0)
        {
            if($tipo_articolo=="A")
            {
                $uapi_out_msg="L'abbonamento � scaduto. Provvedere al rinnovo!";
                return -2;
            }
            else
            {
                $uapi_out_msg="Sulla carta non ci sono fondi a sufficienti per questa operazione. Provvedere alla ricarica!";
                return -1;
            }
        }

        switch($function)
        {
        case "info":
            $sSQL="select acquisti.data_scadenza, (acquisti.credito-acquisti.usufruito) as residuo from acquisti where ID_utente='$cod_utente'  ";
            if($tipo_articolo=='A' )
                $sSQL.="and data_scadenza > NOW() and articoli like '%|$articolo|%'";
            else    //scalare
                $sSQL.="and id_carta='$id_carta'";
if($debug)
    echo "$sSQL<BR>";
            $db_utenti->query($sSQL);
            $db_utenti->next_record();
            $array_info=array(id_utente, cod_utente, nome_riferimento, flag_speciale);
            if($tipo_articolo=='A')
                $array_info[]='data_scadenza';
            else
                $array_info[]='residuo';
            for($i=0;$i<count($array_info); $i++)
            {
                if(isset(${$array_info[$i]}))
                    $param_arr[]=$array_info[$i]."=".urlencode(${$array_info[$i]});
                else
                    $param_arr[]=$array_info[$i]."=".urlencode($db_utenti->f($array_info[$i]));
            }
            $uapi_out_par=implode("&amp;", $param_arr);
            return 0;
            break;
        case "prenota":
            if($tipo_articolo!="S")
            {
                return 0;
                break;
            }
            if(isset($importo) && $importo != '')
            {
                $sSQL="select (credito-usufruito) as residuo, prenotato, t_prenotazione, NOW()+0 as ora from acquisti where ID_utente='$cod_utente' and id_carta=$id_carta and tipo_articolo='S'";
                $db_utenti->query($sSQL);
if($debug)
    echo "$sSQL<BR>";
                $db_utenti->next_record();
if($debug)
echo "prenotato: ".$db_utenti->f(prenotato)."| t_prenotazione: ".($db_utenti->f(t_prenotazione) + 900)."|ora: ".$db_utenti->f(ora)."<BR>";
                if($db_utenti->f(prenotato) > 0 && ($db_utenti->f(t_prenotazione) + 900) > $db_utenti->f(ora))
                {
                    $uapi_out_msg="La carta risulta gi� prenotata. Per sprenotare, andare alla propria my-page e sprenotare il servizio tramite l'apposito tasto";
                    return -3;
                    break;
                }

                if($db_utenti->f(residuo) < $importo)
                {
                    $uapi_out_msg="Sulla carta non ci sono fondi a sufficienti per questa operazione. Provvedere alla ricarica!";
                    return -1;
                }

                if(empty($flag_speciale))
                {
                    $sSQL="update acquisti set prenotato=$importo, t_prenotazione=NOW() where ID_utente='$cod_utente' and id_carta=$id_carta and tipo_articolo='S'";
if($debug)
    echo "$sSQL<BR>";
                    $db_utenti->query($sSQL);
                }
                return 0;
            }
            else
            {
                $uapi_out_msg="E' necessario specificare l'importo in euro";
                return -5;
            }
            break;
        case "sprenota":
            if($tipo_articolo!="S")
            {
                return 0;
                break;
            }
            if(isset($importo) && $importo != '')
            {
                if(empty($flag_speciale))
                {
                    $sSQL="update acquisti set prenotato=0, t_prenotazione=0, usufruito=usufruito+$importo where ID_utente='$cod_utente' and id_carta=$id_carta and tipo_articolo='S'";
if($debug)
    echo "$sSQL<BR>";
                    $db_utenti->query($sSQL);
                    if($importo)
                    {
                        $sSQL="insert into acquisti_movimenti (ID_utente, ID_articolo, tipo_articolo, data, dare_avere, importo, ID_ordine)
                       values ('$cod_utente','$articolo', 'S', NOW(), 'A','$importo','$num_ordine')";
                       $db_utenti->query($sSQL);
                    }
                }
                return 0;
            }
            else
            {
                $uapi_out_msg="E' necessario specificare l'importo in euro";
                return -5;
            }
            break;
        case "controlla":
            return 0;
            break;
        case "verifica":
            if(isset($importo) && $importo != '')
            {
                $sSQL="select (credito-usufruito) as residuo, prenotato, t_prenotazione, NOW()+0 as ora from acquisti where ID_utente='$cod_utente' and id_carta=$id_carta and tipo_articolo='S'";
if($debug)
    echo "$sSQL<BR>";
                $db_utenti->query($sSQL);
                $db_utenti->next_record();
                if($db_utenti->f(prenotato) > 0 && ($db_utenti->f(t_prenotazione) + 900) > $db_utenti->f(ora))
                {
                    $uapi_out_msg="La carta risulta gi� prenotata. Per sprenotare, andare alla propria my-page e sprenotare il servizio tramite l'apposito tasto";
                    return -3;
                    break;
                }
                if($db_utenti->f(residuo) < $importo)
                {
                    $uapi_out_msg="Sulla carta non ci sono fondi a sufficienti per questa operazione. Provvedere alla ricarica!";
                    return -1;
                }
                else
                    return 0;
            }
            else
            {
                $uapi_out_msg="E' necessario specificare l'importo in euro";
                return -5;
            }
            break;
        }
    }
	else
	{
        $uapi_out_msg="L'articolo specificato non ha offerte o non � valido";
        return -9;
	}

}
?>
