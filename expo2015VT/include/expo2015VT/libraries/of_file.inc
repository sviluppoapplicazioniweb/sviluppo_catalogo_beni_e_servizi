<?php
/* OOHForms: file
 *
 * Copyright (c) 1998 by Jay Bloodworth
 *
 * $Id: of_file.inc,v 1.3 1999/10/14 15:37:41 negro Exp $
 */

class of_file extends of_element {

  var $isfile = true;
  var $size;

  function of_file($a) {
    $this->setup_element($a);
  }

  function self_get($val,$which, &$count) {
    $str = "";
//    if(isset($GLOBALS["ly"]) && !empty($GLOBALS["ly"]->font_def['font']))
//    {
//	eval("\$font=\"".$GLOBALS["ly"]->font_def['font']."\";");
//      	$str.="<FONT $font>";
//    }
    
    $str .= "<input type='hidden' name='MAX_FILE_SIZE' value=$this->size>\n";
    $str .= "<input type='file' name='$this->name'";
    if ($this->extrahtml)
      $str .= " $this->extrahtml";
    $str .= ">";
//    if(isset($GLOBALS["ly"]) && !empty($GLOBALS["ly"]->font_def['font']))
//	$str.="</FONT>";
    
    $count = 2;
    return $str;
  }

} // end FILE
