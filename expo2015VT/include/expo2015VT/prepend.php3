<?
/*
 * Session Management for PHP3
 *
 * Copyright (c) 1998,1999 SH Online Dienst GmbH
 *                    Boris Erdmann, Kristian Koehntopp
 *
 * $Id: prepend.php3,v 1.9 1999/10/24 10:21:24 kk Exp $
 *
 */
/*
if (!is_array($_PHPLIB)) {
# Aren't we nice? We are prepending this everywhere
# we require or include something so you can fake
# include_path  when hosted at provider that sucks.
  $_PHPLIB["libdir"] = "siexpo2015/phplib/";
}
*/

$_PHPLIB["libdir"] = $PROGETTO."/libraries/";

require($_PHPLIB["libdir"]."db_mysql.inc");  /* Change this to match your database. */

require($_PHPLIB["libdir"]."ct_sql.inc");    /* Change this to match your data storage container */

require($_PHPLIB["libdir"]."session.inc");   /* Required for everything below.      */
require($_PHPLIB["libdir"]."auth.inc");      /* Disable this, if you are not using authentication. */

require($_PHPLIB["libdir"]."layout_html.inc");
require($_PHPLIB["libdir"]."template.inc");

require($_PHPLIB["libdir"]."page.inc");  /* Required, contains the page management functions. */

//require($_PHPLIB["libdir"]."setDataPage.inc");

require($PROGETTO."/local.inc");     /* Required, contains your local configuration. */

    

?>