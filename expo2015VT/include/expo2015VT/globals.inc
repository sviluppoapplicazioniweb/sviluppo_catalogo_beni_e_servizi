<?
session_start();
/*
$DEBUG=0;
$SPEDISCIMAIL=1;
$LOGMAIL=0;
$RESFORPAGE=20;
$EMAILADMIN="alessandro.ferla@digicamere.it";
$NOMESITO="Purchase EXPO 2015";
$FIRMAMAIL="Purchase EXPO 2015";
$MAILSITO="noreply@purchase.expo2015.org";
$MAILSITO_ATTI="servizi@mi.camcom.it";
$MAILSITO_CONCORSI="selezioni.personale@mi.camcom.it";
$MAILURP="urp@mi.camcom.it";
$MAILSUPERVISORE="fattorusso@mi.camcom.it";
*/
//*****************************


//STATI CONVEGNO
$CONVEGNONUOVO=1;
$CONVEGNOAPERTO=2;
$CONVEGNOCHIUSO=3;

//Parametri sui Files
$MAXFILESIZE=50000;

$TAB_ANAGRAFICA="T_Anagrafica";
$ANAGRAFICA_AVANZATA=0; //0 nel caso non c'� bisogno di dati aggiuntivi in anagrafica e 1 per il contrario
$CONTROLLOTIPOUTENTE=98; // nel caso di anagrafica avanzata � il tipo utente su cui richiedere i dati aggiuntivi
$TEMPLATE_LABEL=100;
//$GESTIONE_SITO="base";
$GESTIONE_SITO="avanzata";
$AREE_RISERVATE=0;
$UNO=1;
$ZERO=0;
$DUE=2;
$ELEPAGE=10;
$ATTIVA="A";
$ARGOMENTO_NEWS_GENERICO=1; //SERVE PER LE NEWSLETTER PROFILATE
$SIMBOLO="|";

$MODULO_CONVEGNO=1;
//ID Gruppi applicativi
$ATTIDIGIT=1;
$CONVEGNI=2;
$CONCORSI=3;
$FINANZIAMENTI=10;
$NOSALVATAGGIO="Chiudi";

$DELIBERE=25;

/*
 * Funzione per la verifica se la pagina � filtrata per IP
 */
function PaginaFitrata($Id_VMenu){
	$Filtrata = false;
	
	$db1 = new DB_CedCamCMS;
	$SQL = "select * from T_Menu WHERE Id=".$Id_VMenu." AND IPFilter=1";
	$db1->query($SQL);
	if ($db1->num_rows()>0)
		$Filtrata = true;
	
	return $Filtrata;
}

/*
 * Funzione per la verifica dell'IP che hanno i permessi
*/
function ValidIP($IP_Client){
	$Valido = true;

	$db1 = new DB_CedCamCMS;
	$SQL = "select * from T_IPValidi WHERE IP='".$IP_Client."' AND Attivo=1";
	$db1->query($SQL);
	if ($db1->num_rows()!=1)
		$Valido = false;

	return $Valido;
}

/*
 * Funzione per recupero del nuovo Id Prodotto nel caso di inserimento nuovo
 */
function getNewId($key, $tabella){
	$db1 = new DB_CedCamCMS;
	$SQL = "select max($key) as Id_Max from $tabella";
	//echo $SQL;
	//exit;
	$db1->query($SQL);
	$db1->next_record();
	return $db1->f("Id_Max") + 1;
}

/*
 * Funzione per l'eliminazione del folder
 */
function deleteDirectory($dir) {
	if (!file_exists($dir)) return true;
	if (!is_dir($dir) || is_link($dir)) return unlink($dir);
	foreach (scandir($dir) as $item) {
		if ($item == '.' || $item == '..') continue;
		if (!deleteDirectory($dir . "/" . $item)) {
			chmod($dir . "/" . $item, 0777);
			if (!deleteDirectory($dir . "/" . $item)) return false;
		};
	}
	return rmdir($dir);
}

/*
 * Funzione per l'eliminazione dei dati correlati al prodotto
 */
function delProdotto($IdProd){
	If ($IdProd>0){
		$db1 = new DB_CedCamCMS;
		
		//Cancella Tabella SIEXPO_T_Refer_X_Prod
		$SQLDel = "DELETE FROM SIEXPO_T_Refer_X_Prod WHERE Id_Prodotto = ".$IdProd;
		$db1->query($SQLDel);
		//Cancella Tabella SIEXPO_TJ_Cat_X_Prod
		$SQLDel = "DELETE FROM SIEXPO_TJ_Cat_X_Prod WHERE Id_Prodotto = ".$IdProd;
		$db1->query($SQLDel);
		//Cancella Tabella SIEXPO_TJ_Crit_X_Prod
		$SQLDel = "DELETE FROM SIEXPO_TJ_Crit_X_Prod WHERE IDProd = ".$IdProd;
		$db1->query($SQLDel);
		//Cancella Tabella SIEXPO_TJ_Mat_X_Prod
		$SQLDel = "DELETE FROM SIEXPO_TJ_Mat_X_Prod WHERE Id_Prodotto = ".$IdProd;
		$db1->query($SQLDel);
		
		//Cancella Cartella con i files del Prodotto
		deleteDirectory($_SERVER['DOCUMENT_ROOT']."/files/imprese/".$_SESSION['uid']."/Prod_".$IdProd);
	}
} 

/*
 * Funzione per recupero dell'attributo file dal nome
*/
function getAttribFile($filename){
	
	$path_parts = pathinfo($filename);
	
	return $path_parts['extension'];
}

/*
 * Lancia un Javascript per visualizzare dei messaggi
 * display = indica se dev'essere attivata l'apertura di un 
 */
function messaggi(){
	
	$display = $_SESSION['display'];
	$messaggio = $_SESSION['messaggio'];
	//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
	//echo "goURL - $goURL";
	//exit;
	if (isset($display)){
		//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
		//exit;
		echo "<script>\n";
		echo "alert('$messaggio');";
		
		$goURL = $_SESSION['goURL'];
		if (strlen($goURL)>0){
			if ($goURL == "-1"){
				echo "history.go(-1);";
			} else {
			//echo "history.go(".$goURL.");";
			echo "window.location.href='".$goURL."';";
			//echo "location.href = '".$goURL."'";
			}
		}
					
		//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
		//exit;
		unset($_SESSION['display']);
		unset($_SESSION['messaggio']);
		unset($_SESSION['goURL']);
		
		echo "</script>";
		
	}
}

function messaggi_new($display, $messaggio, $goURL, $type, $nlink){

	//$display = $_SESSION['display'];
	//$messaggio = $_SESSION['messaggio'];
	//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
	//echo "goURL - $goURL";
	//exit;
	if (isset($display)){
		if ("S" == $type){
			//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
			//exit;
			echo "<script>\n";
			echo "alert('$messaggio');";
	
			//$goURL = $_SESSION['goURL'];
			if (strlen($goURL)>0){
				if ($goURL == "-1"){
					echo "history.go(-1);";
				} else {
					//echo "history.go(".$goURL.");";
					echo "window.location.href='".$goURL."';";
					//echo "location.href = '".$goURL."'";
				}
			}
			
			//echo "Display - $display Messaggio - $messaggio GoURL - $goURL";
			//exit;
			unset($_SESSION['display']);
			unset($_SESSION['messaggio']);
			unset($_SESSION['goURL']);
			
			echo "</script>";
		} elseif ("F" == $type) {
			$_SESSION['display']= $display;
			$_SESSION['messaggio']=$messaggio;
			$_SESSION['goURL']=$goURL;
			$_SESSION['nlink']=$nlink;
			
			$URLPag = "http://".$_SERVER["SERVER_NAME"]."/index.phtml?Id_VMenu=266";
			echo "<script>";
			echo "document.location='".$URLPag."';";
			echo "</script>";
			exit;
			//echo $_SESSION['messaggio'];
			//echo "<a href=".$_SESSION['goURL']."> ".$_SESSION['nlink']."</a>";
			//header("Location: ".$URLPag);
		}

	}
}

/*
 Controlla se esiste in tabella un nome file della presentazione e lo rimuove fisicamente dal path
*/
function killOldFile($Id,$nomefilenew,$nometab, $nomecampo)
{
	global $PATHDOCS,$PATHFILEIMPRESE,$PATHFILES;
	
	$CampoChiave = "";
	if ($nometab == "T_Ext_Anagrafica" or $nometab == "T_Prodotti" or $nometab == "SIEXPO_TJ_Crit_X_Prod"
			or $nometab == "SIEXPO_Tlk_SchedeT" or $nometab == "SIEXPO_T_Refer_X_Prod"){
		$CampoChiave = "Id";
	}
	 
	$db_delfile = new DB_CedCamCMS;
	$SQL_DelFile = "SELECT * FROM ".$nometab." WHERE ".$CampoChiave." = ". $Id;
	$db_delfile->query($SQL_DelFile);
	$db_delfile->next_record();
	//echo 'Nome File DB- '.$db_delfile->f("FilePresNome"). "| NomeFile New - ".$nomefilenew;
	//exit;
	
	$pathfile = $PATHDOCS.$PATHFILEIMPRESE;
	if ($db_delfile->f($nomecampo) != $nomefilenew){
		if ($nometab == "T_Ext_Anagrafica")
			$pathfile .= $Id .'/'.$db_delfile->f($nomecampo);
		elseif ($nometab == "T_Prodotti")
			//$pathfile .= $db_delfile->f("Id_Utente") ."/Prod_".$Id."/".$db_delfile->f($nomecampo);
			$pathfile .= $_SESSION['uid'] ."/Prod_".$Id."/".$db_delfile->f($nomecampo);
		elseif ($nometab == "SIEXPO_TJ_Crit_X_Prod"){
		    $pathfile .= $_SESSION['uid'] ."/Prod_".$db_delfile->f("IDProd")."/".$db_delfile->f($nomecampo);
		} elseif ($nometab == "SIEXPO_Tlk_SchedeT"){
			$pathfile = $PATHDOCS.$PATHFILES."schede_tecniche/".$db_delfile->f($nomecampo);
		}
		//echo "Path File New - $pathfile";
		//exit;
		if ($db_delfile->f($nomecampo) != ""){
			if (rfile($pathfile)==0){
				//echo "Manca il file da cancellare";
				//exit;
			}
		}
	} else {
		//echo "nessuna operazione";
		//exit;
	}
}

function crea_directory($path)
{
	//echo "ci sono - $path";
	//exit();
	if(!is_dir($path)) { 
		if(mkdir($path)){ 
            //echo "something was wrong at : " . $path; 
            return 1; 
        } 
		return 0;
    }  
	return 0;
}

function rfile($path)
{
	if (is_file($path)){
		if (unlink($path)){
			return 1;
		}
		return 0;
	}
	return 0;
}

function crea_newsletter_header($Id)
{
	//modo=A per tutta la newsletter
	//modo=S per selezione

	global $HOMEURL,$PROGETTO;
	$db1 = new DB_CedCamCMS;
	$db2 = new DB_CedCamCMS;
	$db = new DB_CedCamCMS;

	$SQLtemplate="select * from T_NewsLetter_Html";
	if ($db->query($SQLtemplate))
	{
		$db->next_record();
		$Header_NewsLetter=$db->f('Header');
	}

	$SQL="select * from T_NewsLetter where Id=".$Id;
	if ($db->query($SQL))
	{
		$db->next_record();
		//$contenuto.="<!DOCTYPE html PUBLIC \"-//w3c//dtd html 3.2 final//en\">\n";
		//$contenuto="<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";

		$contenuto.="<html>\n";
		$contenuto.="<head>\n";
		$contenuto.="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
		$contenuto.="<title>".$PROGETTO."</title>\n";
		//$contenuto.="<link rel=\"stylesheet\" href=\"".$HOMEURL."/CSS/\" type=\"text/css\" />";
		//$contenuto.="<link rel=\"stylesheet\" href=\"".$HOMEURL."CSS/newsletter.css\" type=\"text/css\" />";
		$contenuto.="<style type=\"text/css\">img.left {float: left;margin: 0 5px 5px 0;}</style>";
		$contenuto.="</head>\n";
		//$contenuto.="<body>\n";
		$contenuto.="<body style=\"text-align:center;font:100% Arial, Verdana, Helvetica, sans-serif;margin:0; padding:0; color: #02326C;\">\n";

		$contenuto.="<p></p><div style=\"text-align:center;\">".$Header_NewsLetter."</div>\n";
		//$contenuto.="<div class=\"titolo_newsletter\">".$db->f("Titolo")."</div>\n";
		$contenuto.="<div style=\"text-align:center;color: #660000;font: bold 14px/16px Arial, Verdana, Helvetica, sans-serif;margin-bottom: 1.2em;\">".$db->f("Titolo")."</div>\n";
		//$SQL1="select T_NewsLetter_News.*, Tlk_NewsLetter_Argomenti.Ordine from T_NewsLetter_News, Tlk_NewsLetter_Argomenti where Id_NewsLetter=".$Id." and  Tlk_NewsLetter_Argomenti.Id=T_NewsLetter_News.Id_Argomento order by Ordine";

		//$contenuto.="<div class=\"left\">";
		$contenuto.="<div style=\"text-align:left;width:96%;margin-left:2%;\">";
	}
	else
		echo "Errore SQL1: ".$SQL1."\n";
	return $contenuto;
}

function crea_newsletter_corpo($Id)
{
	global $ARGOMENTO_NEWS_GENERICO;
	$db1 = new DB_CedCamCMS;
	$db2 = new DB_CedCamCMS;
	$db = new DB_CedCamCMS;
	//$array_corpo=array();
	$SQL1="select T_NewsLetter_News.*, Tlk_NewsLetter_Argomenti.Ordine ";
	$SQL1.="from T_NewsLetter_News, Tlk_NewsLetter_Argomenti where Id_NewsLetter=".$Id." ";
	$SQL1.="and  Tlk_NewsLetter_Argomenti.Id=T_NewsLetter_News.Id_Argomento ";
	$SQL1.="order by Ordine, Id_Argomento, Ordine_News";
//	echo "-1-".$SQL1."--<br>";

$stileargomento="float:left;width:98%;background-color: #660033;color: #ffffff;font: bold 1.2em/1.4em Arial, Verdana, Helvetica, sans-serif;margin-bottom: 1em;padding: 3px 5px;";
$stiletitolo="float:left;width:98%;font: bold 1.0em/1.2em Arial, Verdana, Helvetica,sans-serif;color: #000000;background-color: #f2e5d4;padding: 3px 5px;";
$stiletesto="font: normal 1.0em/1.2em Verdana, Arial, Helvetica,sans-serif;color: #000000; margin-top:10px;";

	if ($db1->query($SQL1))
	{
		while($db1->next_record())
		{	
			if($db1->f("Id_Argomento")!=$arg_old)
			{
				$SQL2="select * from Tlk_NewsLetter_Argomenti where Id=".$db1->f("Id_Argomento")." order by Ordine";
//				echo "-2-".$SQL2."--<br>";
				$db2->query($SQL2);
				$db2->next_record();
				$SQL2="select Id_TipoRegistrazione from Tlk_TipoRegistrazione";
//				echo "-3-".$SQL2."--<br>";
				$db->query($SQL2);
				while($db->next_record())
				{
					$array_corpo[$db2->f("Id")][$db->f("Id_TipoRegistrazione")]="";
				}
				$arg_old=$db2->f("Id");
			}
			if($db1->f("TipoRegistrazione")=="|")
			{
				$SQL2="select Id_TipoRegistrazione from Tlk_TipoRegistrazione";
//				echo "-4-".$SQL2."--<br>";
				$db->query($SQL2);
				$my_registrazione="|";
				while($db->next_record())
					$my_registrazione.=$db->f("Id_TipoRegistrazione")."|";
				$registrazione=explode("|",$my_registrazione);	
			}
			else
				$registrazione=explode("|",$db1->f("TipoRegistrazione"));

			for($i=0;$i<sizeof($registrazione);$i++)
			{
				if($registrazione[$i]!="")
				{
					$reg=$registrazione[$i];
/*
					if($array_corpo[$db2->f("Id")][$reg]=="" && $db2->f("Id")!=$ARGOMENTO_NEWS_GENERICO)
						$array_corpo[$db2->f("Id")][$reg]="<div class=\"argomento_newsletter\"><h2>".$db2->f("Descrizione")."</h2></div>\n";
					if ($db1->f("Titolo_News")!="")
						$array_corpo[$db2->f("Id")][$reg].="<div class=\"sottotitolo_newsletter\">".stripslashes($db1->f("Titolo_News"))."</div>\n";
					$array_corpo[$db2->f("Id")][$reg].="<div class=\"testo_newsletter\">";
*/
					if($array_corpo[$db2->f("Id")][$reg]=="" && $db2->f("Id")!=$ARGOMENTO_NEWS_GENERICO)
						$array_corpo[$db2->f("Id")][$reg]="<br /><div style=\"".$stileargomento."\">".$db2->f("Descrizione")."</div>\n";
					if ($db1->f("Titolo_News")!="")
						$array_corpo[$db2->f("Id")][$reg].="<br /><div style=\"".$stiletitolo."\">".stripslashes($db1->f("Titolo_News"))."</div>\n";
					$array_corpo[$db2->f("Id")][$reg].="<br /><div style=\"".$stiletesto."\">";

					$array_corpo[$db2->f("Id")][$reg].=stripslashes($db1->f("Testo_Html"))."\n";

					if ($db1->f("Link")!="")
						$array_corpo[$db2->f("Id")][$reg].="<a href=\"http://".$db1->f("Link")."\">".$db1->f("Link")."</a>\n";

					$array_corpo[$db2->f("Id")][$reg].="</div>\n";			
					
					//echo "<strong>Argomento=".$db2->f("Id")." - TipoRegistrazione=".$registrazione[$i]."</strong><br>".$array_corpo[$db2->f("Id")][$reg]."<br><br>";				
				}				
			}
		}
	}
	return $array_corpo;
}




function crea_newsletter_footer($Id)
{
	$db = new DB_CedCamCMS;
	$SQLtemplate="select * from T_NewsLetter_Html";
	if ($db->query($SQLtemplate))
	{
		$db->next_record();
		$Footer_NewsLetter=$db->f('Footer');
	}
	$contenuto="</div>";
	$contenuto.="<div>".$Footer_NewsLetter."</div>\n";

	$contenuto.="</body>\n";
	$contenuto.="</html>\n";
	return $contenuto;
}



function form_convegno($Id_Convegno,$display,$Id_Iscrizione=0)
{
	$db_corsi = new DB_CedCamCMS;
	$db2 = new DB_CedCamCMS;
	$sql="select CONVEGNI_T_Campi_Categoria.Label, CONVEGNI_T_Campi_Convegno.Obbligatorio, ";
	$sql.="CONVEGNI_T_Campi_Categoria.Lunghezza, CONVEGNI_T_Campi_Categoria.Max_Lunghezza, ";
	$sql.="CONVEGNI_T_Campi_Categoria.Tipo_Campo, CONVEGNI_T_Campi_Convegno.Id_CampoConvegni, ";
	$sql.="CONVEGNI_T_Campi_Categoria.Testo_Libero ";
	$sql.="from CONVEGNI_T_Campi_Convegno,CONVEGNI_T_Campi_Categoria ";
	$sql.="where CONVEGNI_T_Campi_Convegno.Id_Campo=CONVEGNI_T_Campi_Categoria.Id_Campo ";
	$sql.="and CONVEGNI_T_Campi_Convegno.Id_Convegno=".$Id_Convegno." and Attiva=1";
	$sql.=" order by OrdinaOL";
	//echo "1-- ".$sql."<br/>";
	$sql_tutti=$sql;
	$my_form="";
	$db_corsi->query($sql);
	while($db_corsi->next_record())
	{
		$db_nome="campo_".$db_corsi->f("Id_CampoConvegni");
		//if(!(isset($$db_nome)))
		//	$$db_nome="";
		if($Id_Iscrizione>0 && $db_corsi->f('Tipo_Campo')!=6)
		{
			$sql="select Valore from CONVEGNI_T_Valori_Iscrizione_Convegni ";
			$sql.="where Id_Iscrizione=".$Id_Iscrizione." and Id_Campo=".$db_corsi->f("Id_CampoConvegni");
			$db2->query($sql);
			$db2->next_record();
			$$db_nome=$db2->f("Valore");
		}
		$obbl="";
		if($db_corsi->f('Tipo_Campo')==6) 
		{
			$my_form.="<div class=\"box1form\">";
		}
		else
		{
			$my_form.="<div class=\"box2form\">\n";
			$my_form.="<div class=\"form_label\">\n";
			if ($db_corsi->f('Obbligatorio')==1 && !($display) && ${$db_nome}=="")
				$obbl="(*) ";
			if($db_corsi->f('Label')!="")
			{
				$my_form.="<label for=\"".$db_nome."\">".$db_corsi->f('Label').$obbl." </label>";
				$my_form.="</div>\n";
			}
		}
		$my_form.="<div>\n";
		$valore_campo=${$db_nome};
		if (($display) && ($valore_campo=="")) $valore_campo="...";
		switch ($db_corsi->f('Tipo_Campo')) 
		{

			case 1:
				if ($display)
					$my_form.="<span class=\"display\">".$valore_campo."</span>";
				else
					$my_form.="<input type=\"text\" id=\"".$db_nome."\" name=\"".$db_nome."\" value=\"".$valore_campo."\" size=\"".$db_corsi->f('Lunghezza')."\" maxlength=\"".$db_corsi->f('Max_Lunghezza')."\">\n";
				break;
			case 2:
				$sql="select CONVEGNI_T_Valori_Campi_Convegno.Id_ValoreCampo_Convegno, ";
				$sql.="CONVEGNI_T_Valori_Campi_Categoria.Valore ";
				$sql.="from CONVEGNI_T_Valori_Campi_Convegno, CONVEGNI_T_Valori_Campi_Categoria ";
				$sql.="where CONVEGNI_T_Valori_Campi_Convegno.Id_Valore_Campo=CONVEGNI_T_Valori_Campi_Categoria.Id_Valore_Campo ";
				$sql.="and Id_CampoConvegni=".$db_corsi->f("Id_CampoConvegni")." and CONVEGNI_T_Valori_Campi_Convegno.Attiva=1 ";
				$sql.="order by CONVEGNI_T_Valori_Campi_Categoria.Valore";
				//echo "2-- ".$sql."<br/>";
				$db2->query($sql);
				$y=1;
				while($db2->next_record())
				{
					$i=$db2->f("Id_ValoreCampo_Convegno");
					$valore=$db2->f("Valore");
					if ($y==1 && empty($valore_campo))
						$my_form.="&nbsp;&nbsp;<input type=\"radio\" name=\"".$db_nome."\" value=\"".$i."\" checked";
					elseif ($valore_campo==$i)
						$my_form.="&nbsp;&nbsp;<input type=\"radio\" name=\"".$db_nome."\" value=\"".$i."\" checked";
					else
						$my_form.="&nbsp;&nbsp;<input type=\"radio\" name=\"".$db_nome."\" value=\"".$i."\"";
					if ($display)
						$my_form.=" disabled";
					$my_form.=">&nbsp;".$valore."<br />\n";
					$y++;
				}
				break;
			case 3:
				$sql="select CONVEGNI_T_Valori_Campi_Convegno.Id_ValoreCampo_Convegno, ";
				$sql.="CONVEGNI_T_Valori_Campi_Categoria.Valore ";
				$sql.="from CONVEGNI_T_Valori_Campi_Convegno, CONVEGNI_T_Valori_Campi_Categoria ";
				$sql.="where CONVEGNI_T_Valori_Campi_Convegno.Id_Valore_Campo=CONVEGNI_T_Valori_Campi_Categoria.Id_Valore_Campo ";
				$sql.="and Id_CampoConvegni=".$db_corsi->f("Id_CampoConvegni")." and CONVEGNI_T_Valori_Campi_Convegno.Attiva=1 ";
				$sql.="order by CONVEGNI_T_Valori_Campi_Categoria.Valore";
				$db2->query($sql);
				$y=1;
				while($db2->next_record())
				{
					$i=$db2->f("Id_ValoreCampo_Convegno");				
					$valore=$db2->f("Valore");
					$my_form.="&nbsp;&nbsp;<input type=\"checkbox\" name=\"".$db_nome."[]\" value=\"".$i."\"";
					
					//CONTROLLARE ma dovrebbe essere cosi
					if (strlen(strstr($valore_campo, "|".$i."|"))>0)
						$my_form.=" checked";
					if ($display)
						$my_form.=" disabled";	
					$my_form.=">&nbsp;".$valore."<br />\n";
	//						if ($i==1) ${$db_corsi->f('Nome')."_c".$i}=$obbl.${$db_corsi->f('Nome')."_c".$valore};
					$y++;
				}
				break;

			case 4:
				$sql="select CONVEGNI_T_Valori_Campi_Convegno.Id_ValoreCampo_Convegno, ";
				$sql.="CONVEGNI_T_Valori_Campi_Categoria.Valore ";
				$sql.="from CONVEGNI_T_Valori_Campi_Convegno, CONVEGNI_T_Valori_Campi_Categoria ";
				$sql.="where CONVEGNI_T_Valori_Campi_Convegno.Id_Valore_Campo=CONVEGNI_T_Valori_Campi_Categoria.Id_Valore_Campo ";
				$sql.="and Id_CampoConvegni=".$db_corsi->f("Id_CampoConvegni")." and CONVEGNI_T_Valori_Campi_Convegno.Attiva=1 ";
				$sql.="order by CONVEGNI_T_Valori_Campi_Categoria.Valore";
				$db2->query($sql);
				$y=1;
				if (!($display))
				{
					$my_form.="<select name=\"".$db_nome."\" id=\"s_".$db_nome."\">\n";
					$my_form.="<option value=\"\">--</option> \n";
				}
				while($db2->next_record())
				{
					$i=$db2->f("Id_ValoreCampo_Convegno");									
					$valore=$db2->f("Valore");
					if($display)
					{
						if ($valore_campo==$i)
							$my_form.=$valore;
					}
					else
					{
						$my_form.="<option value=\"".$i."\"";
						if ($valore_campo==$i)
							$my_form.= "selected=\"selected\"";
						$my_form.="> ".$valore."</option>\n";
					}
					$y++;

				}
				if (!($display))
					$my_form.="</select>";
				break;

			case 5:
				if ($display)
					$my_form.="<span class=\"display\">".$valore_campo."</span>";
				else
					$my_form.="<textarea name=\"".$db_nome."\" rows=\"".$db_corsi->f('Lunghezza')."\" cols=\"".$db_corsi->f('Max_Lunghezza')."\">".$valore_campo."</textarea>";
				break;
			case 6:
				$my_form.="<span style='text-align:left;'>".$db_corsi->f('Testo_Libero')."</span>";
				break;
			
				
		}
		$my_form.="</div>\n";
		$my_form.="<br style=\"clear: left;\">\n";
		$my_form.="</div>\n";
	}
	return $my_form;
}

function myhtmlentities($str)
{
            
    $tbl=get_html_translation_table(HTML_ENTITIES);
            
    unset ($tbl["<"]);
    unset ($tbl[">"]);
    unset ($tbl["'"]);
    unset ($tbl['"']);
    unset ($tbl['&']);
    unset ($tbl['#']);

	$tbl[chr(145)] = '\''; 
	$tbl[chr(146)] = '\''; 
	$tbl[chr(147)] = '&quot;'; 
	$tbl[chr(148)] = '&quot;'; 
//	$tbl["?"]="...";
	$tbl[chr(150)]="-";
	$tbl[chr(187)]="&raquo;";
	$tbl[chr(171)]="&laquo;";
            
    return str_replace(array_keys($tbl),array_values($tbl),$str);
            
}

function formatbytes($val, $digits = 3, $mode = "SI", $bB = "B")
{ //$mode == "SI"|"IEC", $bB == "b"|"B"
       $si = array("", "k", "M", "G", "T", "P", "E", "Z", "Y");
       $iec = array("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi");
       switch(strtoupper($mode)) {
           case "SI" : $factor = 1000; $symbols = $si; break;
           case "IEC" : $factor = 1024; $symbols = $iec; break;
           default : $factor = 1000; $symbols = $si; break;
       }
       switch($bB) {
           case "b" : $val *= 8; break;
           default : $bB = "B"; break;
       }
       for($i=0;$i<count($symbols)-1 && $val>=$factor;$i++)
           $val /= $factor;
       $p = strpos($val, ".");
       if($p !== false && $p > $digits) $val = round($val);
       elseif($p !== false) $val = round($val, $digits-$p);
       return round($val, $digits) .  $symbols[$i] . $bB;
   }

function data_italiana($my_data)
{
	$aaaa=substr($my_data,0,4);
	$mm=substr($my_data,5,2);
	$gg=substr($my_data,8,2);
	$ret_data=$gg."/".$mm."/".$aaaa;
	return $ret_data;
}

function data_italiana_estesa($my_data)
{
	$monthNames = array("gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre");
	$aaaa=substr($my_data,0,4);
	$mm=substr($my_data,5,2);
	$mm=intval($mm);
	$gg=substr($my_data,8,2);
	$ret_data=$gg." ".$monthNames[$mm]." ".$aaaa;
	return $ret_data;
}

function data_inglese($my_data)
{
	$aaaa=substr($my_data,6,4);
	$mm=substr($my_data,3,2);
	$gg=substr($my_data,0,2);
	$ret_data=$aaaa."-".$mm."-".$gg;
	return $ret_data;	
}

function dataora_italiana($my_data)
{
	$aaaa=substr($my_data,0,4);
	$mm=substr($my_data,5,2);
	$gg=substr($my_data,8,2);
	$ora=substr($my_data,10);
	$ret_data=$gg."/".$mm."/".$aaaa.$ora;
	return $ret_data;
}

function writeGlobal() {
	global $PATHLOGS;
		$n = (rand(0,1000)* 1000+rand(0,1000))*1000+rand(0,1000);
		
		$file_name=$PATHLOGS;
		$file_name.="Errors/";
		$file_name.=date("Ymd")."/";
		$file_name.=date("Ymd_His")."_".str_pad($n, 15, "0", STR_PAD_LEFT).".txt";

		if(!(file_exists(dirname($file_name)))) {
			mkdir(dirname($file_name),0777,true);	
		}
		
		$msg=print_r($GLOBALS,true);
		
		
			$fp=fopen($file_name,"x");
			fwrite($fp, $msg);
			fclose($fp); 	

		return $file_name;
		
}

function sqlGetSingleValue($sql,$column) {
	$db = new DB_CedCamCMS;
	$db->query($sql);
	$db->next_record();
	return $db->f($column);
}

function sqlGetSingleRecord($sql) {
	$db = new DB_CedCamCMS;
	$db->query($sql);
	$db->next_record();
	return $db->Record;
}


function string_begins_with($string, $search)
{
    return (strncmp($string, $search, strlen($search)) == 0);
}

/*
 * Restituisce le immagini come elementi <li>
 * 
 * @param string $folder la cartella che contiene le immagini es.: cartella/sottocartella.
 * @param string $dbtable Nome tabella sul DB.
 */
function ImgForScrol($folder,$dbtable) {
    $dbImgScroll = new DB_CedCamCMS;
    $sqlImgScroll = "SELECT ";
    $sqlImgScroll .= "Img, ";
    $sqlImgScroll .= "Nome, ";
    $sqlImgScroll .= "Link ";
//    $sqlImgScroll .= "FROM $dbtable order by Nome";
    $sqlImgScroll .= "FROM $dbtable order by rand()";
    $dbImgScroll->query($sqlImgScroll);
    while ($dbImgScroll->next_record()) {
        if (substr($dbImgScroll->f('Link'), 0, 1) == '/') {
            $Link = $dbImgScroll->f('Link');
        } else {
            $Link = "http://".$dbImgScroll->f('Link')."\" target=\"_blank";
        }
        
            $img_partners .= "<li><a href=\"".$Link."\"><img src=\"/$folder/".$dbImgScroll->f('Img')."\" alt=\"" . $dbImgScroll->f('Nome') . "\" style=\"margin:0px; padding:0px;\" /></a></li>\n";
    }
    //Ogni logo � ripetuto $index volte.
    for ($index = 0; $index < 4; $index++) { 
        $img_partners .= $img_partners;
    }
        return $img_partners;
}

?>
