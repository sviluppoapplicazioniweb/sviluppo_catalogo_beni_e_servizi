<?php
include $PROGETTO . "/libraries/setDataPage.inc";
require_once($PROGETTO . "/view/lib/db.class.php");
$urlHome = '/index.phtml?Id_VMenu='.$Id_VMenu;
$widthMenu = "390px";

$langHTML = $Lang;
if ($Id_VMenu == 312 || $Id_VMenu == 313){
	$temp = trim($_REQUEST['lang']);
	if (strlen($temp) == 2){
		$langHTML = $temp;	
	}
}

?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print strtolower($langHTML) ?>">
    <head>
        <title><? echo $titoloPage; ?></title>   
		<!--<meta http-equiv="x-ua-compatible" content="IE=8;">-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="title" content="<? echo $db_titolo->f('Titolo'); ?>" />
        <meta name="Description" content="<? echo $description; ?>" />
        <meta name="Keywords" content="<? echo $keywords; ?>" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="date" content="<?php echo "$meta_date"; ?>" />

		<?php $db = new DataBase();
	$str = $db->GetRow('SELECT File FROM T_Documenti WHERE Id=1','File');
	echo '<!-- '.$str.' -->';
    ?>
        <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/generale.css" />
        <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/template.css" />

        <link rel="stylesheet" type="text/css" href="/jquery/themes/base/jquery-ui.css" /> 
        <link rel="stylesheet" type="text/css" href="/jquery/themes/base/jquery.ui.menu.css" />

        <script type="text/javascript" src="/jquery/jquery-1.9.1.js" ></script>
        <script type="text/javascript" src="/jquery/ui/jquery-ui.js"  ></script>

        <?php if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/jquery/js/visualizza_" . $nome . ".js")) { ?>

            <script type="text/javascript" src="/jquery/js/visualizza_<?php echo $nome; ?>.js" ></script>
        <?php } ?>      
    </head>

    <body>
        <input type="hidden" id="Lang" name="Lang" value="<?php echo $Lang; ?>" />
        
        <div id="maincontainer">
            <div class="container_top"></div>
            <div id="container_center">
                <div id="container_center_ce">
                    <div id="hp_header" class="hp_header2">
                        <div id="header_left">
                            <div id="block-block-29" class="clear-block block block-block">
                                <div class="content">
                                    <p>
                                        <a href="<?php echo $urlHome; ?>" draggable="false" title="PARTICIPAN DIGITAL MANAGEMENT SYSTEM" alt="PARTICIPAN DIGITAL MANAGEMENT SYSTEM" >
                                            <img draggable="false" alt="PARTICIPAN DIGITAL MANAGEMENT SYSTEM" src="tmpl/expo2015VT/vetrina/images/logosx2.png"/>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="header_center">
                            <div id="block-locale-0" class="clear-block block block-locale">
                                <div class="content">
                                    <ul>
                                        <?php include $PROGETTO . "/templates/" . $templateUse . "/linguages.inc"; ?>
                                    </ul>
                                </div>
                            </div>
                            <div id="block-block-45" class="clear-block block block-block">
                                <div class="content">
                                    <div id="hpbox01">
                                        <div class="top"></div>
                                        <div class="central">
                                            <p class="loggedBlock"></p>
                                            <?php if (strcmp($nominativo, "") != 0) { ?>
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top;">
                                                                <?php echo _BENVENUTO_; ?>
                                                                <br/>
                                                                <a draggable="false" href="/jslogin.phtml?action=logout&okurl=<?php echo $PDMS; ?>"><?php echo _DISCONNETTI_; ?></a>
                                                            </td>
                                                            <td style="vertical-align: top;" width="250px">
                                                                <b>
                                                                    <?php echo $nominativo; ?>
                                                                    <b>
                                                                        <br/>
                                                                    </b>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                        </div>
                                        <div class="bottom"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="header_right">
                            <div id="block-block-46" class="clear-block block block-block">
                                <div class="content">
                                    <p>
                                        <a href="http://expo2015.org/" target="_blank" draggable="false" title="<?php echo _EXPO2015_; ?>" alt="<?php echo _EXPO2015_; ?>" >
                                            <img draggable="false" alt="<?php echo _EXPO2015_; ?>" src="/tmpl/expo2015VT/vetrina/images/logodx.png"/>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="main">
                    <div id="menu" class="mybottom menuvisenpmi">
                        <div id="content" class="contentmenu" >
                            <div id="block-superfish-1" class="clear-block block block-superfish">
                                <div class="content" align="center">
                                    <?
                                    if ($auth->auth["utype"] == 98) {
                                        if ($template[$tmpl]["top_item"] != "") {

                                            $left_item = explode("|", $template[$tmpl]["top_item"]);

                                            foreach ($left_item as $item)
                                                include $PROGETTO . "/" . $item . ".inc";
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mainboxbg">
                        <div id="parametriRicerca"></div>
                        <div class="titlePage">
                            <?php
                            if (!(isset($nh)) && $Id_VMenu != 315 && $Id_VMenu != 304 && $auth->auth["utype"] == 98) {
                                $help = $db_titolo->f('Help_HTML');

                                if ($Lang != $LinguaPrincipale && $db_titolo->f('Titolo_AL') != "")
                                    $titolo_barra = $db_titolo->f('Titolo_AL');
                                else
                                    $titolo_barra = $db_titolo->f('Titolo');


                                echo "<h1>" . $titolo_barra . "</h1>";
                            }
                            ?>
                        </div>
                        <div id="nodetop"></div>
                        <div id="pagina_interna_content">
                            <div class="clear-block">
                                <div id="nodebot"></div>
                                <div id="block-views-vw_listapartecipanti-block_1" class="clear-block block block-views">
                                    <div class="content">
                                        <div class="view view-vw-listapartecipanti view-id-vw_listapartecipanti view-display-id-block_1 viewtable view-dom-id-1">
                                            <div class="view-header">
                                                <div class="view-content">
                                                    <?php
                                                    if ($auth->auth["utype"] == 98 || ($auth->auth["uid"] == 2 && ($Id_VMenu == 312 || $Id_VMenu == 313)) || $Id_VMenu == 314) {
                                                        if ($pagina != "") {

                                                            include $PROGETTO . "/" . $pagina . ".inc";
                                                        } else {
                                                            include $PROGETTO . "/corpo.inc";
                                                        }
                                                    } else {
                                                        echo _NON_AB_;
                                                    }
                                                    ?>
                                                </div>
                                                <div class="view-footer">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_bottom"></div>
            <div id="footer">
                <div class="content">
                    <div id="block-block-22" class="clear-block block block-block">
                        <div class="content">
                            <p>&copy; 2011 - Expo 2015 S.p.A. - C.F. e P.I. 06398130960</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>