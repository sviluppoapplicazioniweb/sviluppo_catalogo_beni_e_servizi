<?

if ($auth->auth["utype"]=="")  $utype2=99;
else $utype2=$auth->auth["utype"];

include $PROGETTO."/dynamic_menu.inc";

$db_menu = new DB_CedCamCMS;

/*if($auth->auth["utype"]!="" && $auth->auth["utype"]==$CONTROLLOTIPOUTENTE)
{
	$sql="select Stato_Record_Locale from T_Utente where Id=".$auth->auth["id_utente"];
	$db_menu->query($sql);
	$db_menu->next_record();
	if($db_menu->f("Stato_Record_Locale")=="D")
		$utype2=99;
}
*/

$SQLmenu="select Mtop from ".$tabsvil."T_Menu where Id_Menu='$explode' and ".$tabsvil."T_Menu.Tipo<>'L'";
$db_menu->query($SQLmenu);
$db_menu->next_record();
$mtop=$db_menu->f('Mtop');
if ($DEBUG) echo "<!-- SQLmenu ".$SQLmenu."-->\n";

//$SQLmenu="select ".$tabsvil."T_Menu.Id, ".$tabsvil."T_Menu.Id_Menu, ".$tabsvil."T_Menu.Id_Sottomenu, ".$tabsvil."T_Menu.Id_Priorita, ".$tabsvil."T_Menu.Id_TipoUtente, ".$tabsvil."T_Menu.Mtop, ".$tabsvil."T_Menu.Stato_Menu, ".$tabsvil."T_Menu.Tipo, ".$tabsvil."T_Menu.Esploso, ".$tabsvil."T_Menu.Titolo, ".$tabsvil."T_Menu.Descrizione, Id_Pagina, T_Pagine.Id, T_Pagine.Descrizione, T_Pagine.Link from ".$tabsvil."T_Menu, T_Pagine";
//$SQLmenu="select ".$tabsvil."T_Menu.Id, ".$tabsvil."T_Menu.Id_Menu, ".$tabsvil."T_Menu.Id_Sottomenu, ".$tabsvil."T_Menu.Id_Priorita, ".$tabsvil."T_Menu.TipiUtente, ".$tabsvil."T_Menu.Mtop, ".$tabsvil."T_Menu.Stato_Menu, ".$tabsvil."T_Menu.Tipo, ".$tabsvil."T_Menu.Esploso, ".$tabsvil."T_Menu.Titolo, ".$tabsvil."T_Menu.Descrizione, ".$tabsvil."T_Menu.LinkEsterno, Id_Pagina, T_Pagine.Descrizione, T_Pagine.Link from ".$tabsvil."T_Menu, T_Pagine";
// Semplificata
$SQLmenu="select ".$tabsvil."T_Menu.Id, ".$tabsvil."T_Menu.Id_Menu, ".$tabsvil."T_Menu.Gruppi, ".$tabsvil."T_Menu.Id_Sottomenu, ".$tabsvil."T_Menu.Mtop, ".$tabsvil."T_Menu.Stato_Menu, ".$tabsvil."T_Menu.Tipo, ".$tabsvil."T_Menu.Esploso, ".$tabsvil."T_Menu.Titolo, ".$tabsvil."T_Menu.LinkEsterno, T_Pagine.Link";
if ($Lang!=$LinguaPrincipale)
	$SQLmenu.=", ".$tabsvil."T_Menu_AL.Titolo_AL, ".$tabsvil."T_Menu_AL.LinkEsterno_AL";
$SQLmenu.=" from ".$tabsvil."T_Menu, T_Pagine";
if ($Lang!=$LinguaPrincipale)
	$SQLmenu.=", ".$tabsvil."T_Menu_AL";
$SQLmenu.=" where ".$tabsvil."T_Menu.Id_Pagina=T_Pagine.Id";
if ($Lang!=$LinguaPrincipale)
{
	$SQLmenu.=" and ".$tabsvil."T_Menu_AL.Id_AL=".$tabsvil."T_Menu.Id and ".$tabsvil."T_Menu_AL.Lang_AL='".$Lang."'";
	if ($noPredefinita) $SQLmenu.=" and (".$tabsvil."T_Menu_AL.Titolo_AL<>'' or ".$tabsvil."T_Menu.Id_Menu>='80')";
}
$SQLmenu.=" and (".$tabsvil."T_Menu.Tipo='M' or ".$tabsvil."T_Menu.Tipo='L')";
//if ($Priorita[$utype2]>8)
//	$SQLmenu.=" and (".$tabsvil."T_Menu.Stato_Menu='A' or ".$tabsvil."T_Menu.Stato_Menu='D')";
//else
$SQLmenu.=" and ".$tabsvil."T_Menu.Stato_Menu='A'";
$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Menu>='02'";
//$SQLmenu.=" and T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (T_Menu.Id_TipoUtente=".$utype2." or T_Menu.Id_TipoUtente=0)";
$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."T_Menu.TipiUtente='|' or ".$tabsvil."T_Menu.TipiUtente='|0|')";

// Aggiungere controllo accessi Gruppi

if ($mtop!="00")
	$SQLmenu.=" and MTop ='".$mtop."'";
else
	$SQLmenu.=" and (MTop ='".$mtop."' or Id_Menu=MTop)";

if($mtop==$mtop_spri)
	$SQLmenu.=" and ".$tabsvil."T_Menu.Id<>'".$id_spri."'";

$SQLmenu.=" order by Id_Menu, Tipo, Id_Sottomenu";

$gruppiout="";
if ($DEBUG) echo "<!-- SQLmenu ".$SQLmenu."-->\n";
if ($db_menu->query($SQLmenu))
{
	$pages=array();
	$i=1;
	while($db_menu->next_record())
	{
	  if (checkgroup($db_menu->f('Gruppi')) && ((substr($db_menu->f('Id_Menu'),0,strlen($gruppiout))!=$gruppiout) || $gruppiout==""))
	  {
			$target="";
			if ($db_menu->f('Link')=="Link esterno")
			{
				if ($Lang!=$LinguaPrincipale && $db_menu->f('LinkEsterno_AL')!="")
					$link=$db_menu->f('LinkEsterno_AL');
				else
					$link=$db_menu->f('LinkEsterno');
				$target="_blank";
			}
			elseif(!strncmp($db_menu->f('Link'),"http",4) || !strncmp($db_menu->f('Link'),"mailto",6)) // è un programma esterno
				$link= preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_menu->f('Link')); // PM041021
			else
				$link="/index.phtml?Id_VMenu=".$db_menu->f(Id);
			$esploso=$db_menu->f('Esploso');
			if ($Lang!=$LinguaPrincipale && $db_menu->f('Titolo_AL')!="")
				$titolo=$db_menu->f('Titolo_AL');
			else
				$titolo=$db_menu->f('Titolo');

			if ($i==1)
				$pages[$db_menu->f('Tipo').$db_menu->f('Id_Menu')]=array($link=>$titolo, target=>"", sub=>$db_menu->f('Id_Sottomenu'), stato=>$db_menu->f('Stato_Menu'), tipo=>$db_menu->f('Tipo'), esploso=>$esploso, mtop=>$db_menu->f('Mtop'), primo=>"S", Id_VMenu=>$db_menu->f('Id'));
			else
				$pages[$db_menu->f('Tipo').$db_menu->f('Id_Menu')]=array($link=>$titolo, target=>"", sub=>$db_menu->f('Id_Sottomenu'), stato=>$db_menu->f('Stato_Menu'), tipo=>$db_menu->f('Tipo'), esploso=>$esploso, mtop=>$db_menu->f('Mtop'), primo=>"N", Id_VMenu=>$db_menu->f('Id'));

			$i++;
			$gruppiout="";
		}
		else
		{
			if ($gruppiout=="")
				$gruppiout=$db_menu->f('Id_Menu');
		}
	}
}
else
	echo "Errore SQLmenu: ".$SQLmenu."\n";

$menustring="";
$leftmenu="S";
if(!empty($explode))
	show_nav($explode);
else
	show_nav("00");
$leftmenu="N";

$codice_html=file_get_contents($PATHDOCS."tmpl/menu.html");

echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);

?>
