<script type="text/javascript">
    <!--
    function CheckITDate(datavalue)
    {
        // Regular expression used to check if date is in correct format
        var pattern = /[0-3][0-9]\/(0|1)[0-9]\/(19|20)[0-9]{2}/;

        if(datavalue.match(pattern))
        {
            var date_array = datavalue.split('/');
            var day = date_array[0];

            // Attention! Javascript consider months in the range 0 - 11
            var month = date_array[1] - 1;
            var year = date_array[2];

            // This instruction will create a date object
            source_date = new Date(year,month,day);

            if(year != source_date.getFullYear())
            {
                return false;
            }

            if(month != source_date.getMonth())
            {
                return false;
            }

            if(day != source_date.getDate())
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }
    function validate_Validator(f) {
<?
if ($azione == "INS" && $nome == "T_Anagrafica") {
    ?>

                if (f.Check_Privacy21.checked == false){
                    alert("Attenzione! Per procedere, ? necessario il consenso per il trattamento dei dati personali.");
                    f.Check_Privacy21.focus();
                    return(false);
                }
//                if(f.Data_Nascita)
//                {
//                    if(CheckITDate(f.Data_Nascita.value)==false)
//                    {
//                        alert("Attenzione il formato della data di nascita non ? valido.");
//                        f.Data_Nascita.focus();
//                        return(false);
//                    }
//                }    
    <?
}
if ($azione == "INS" && $nome == "SALE_T_Strutture") {
    ?>
           if (f.Numero_Sale.value <= 0){
                    alert("Attenzione! Devi specificare il numero totale di sale.");
                    f.Numero_Sale.focus();
                    return(false);
                } 
           if (f.Capienza_Sala_Principale.value < 200){
                    alert("Attenzione! La capienza della sala principale deve essere di almeno 200 posti.");
                    f.Capienza_Sala_Principale.focus();
                    return(false);
                }
    <?        
}
//TOLTOLANG reset($tabella[$Lang]);
reset($tabella);
//TOLTOLANG while (list ($key, $val) = each ($tabella[$Lang]))
while (list ($key, $val) = each($tabella)) {
    $temp = explode("|", $key);
    $key = $temp[0];
    $utenti_abilitati = $temp[1];
    $azione2 = $temp[2];
    $id_tipoutente2 = $temp[3];
    $val = preg_replace("/\{%(\w+)%\}/e", "\$\\1", $val);
    $campo = explode("|", $val);

    $abilitato = strpos($utenti_abilitati, ";" . $auth->auth["utype"] . ";");
    $da_controllare = strpos($id_tipoutente2, ";" . $Id_TipoUtente . ";");
    if (sizeof($campo) > 2 && ($azione == $azione2 || $azione2 == "") && ( $utenti_abilitati == "" || !($abilitato === false)) && ( $id_tipoutente2 == "" || !($da_controllare === false))) {
        if ($campo[$Parametri["Display"]] != "") {
            $arCondiz = explode(";", $campo[$Parametri["Display"]]);
            $condiz = "";

            if (sizeof($arCondiz) == 2) {
                $condiz = $arCondiz[0];
                $dis = $arCondiz[1];
            } elseif ($arCondiz[0] == "DIS")
                $dis = $arCondiz[0];
            elseif ($arCondiz[0] == "N")
                $campo[$Parametri["TipoCampo"]] = "HIDDEN";

            else {
                $condiz = $arCondiz[0];
                $dis = "";
            }
        }
        $dis_check = $dis;

        if ($campo[$Parametri["TipoCampo"]] == "BUTTON") {
            if ($campo[$Parametri["Label"]] == "Chiudi")
                $conferma = 0;
            else
                $conferma = 1;
        }

        if (dis_check == "DIS")
            $campo[$Parametri["Display"]] = "DIS";
        if ((($campo[$Parametri["Obbligatorio"]] == "S" || $campo[$Parametri["Obbligatorio"]] == "SNM") && ($campo[$Parametri["Display"]] == "" || $campo[$Parametri["Display"]] == "DIS")) || (($campo[$Parametri["Obbligatorio"]] == "S" || $campo[$Parametri["Obbligatorio"]] == "SNM") && ($condiz != "") && (eval("if (" . $condiz . ") return 1; else return 0;")))) {

            if ($campo[$Parametri["TipoCampo"]] == "RADIO") {
                echo "if (f." . $key . "[" . $campo[$Parametri["Parametro3"]] . "].checked) {\n";
                echo "   alert(\"" . $campo[$Parametri["Label"]] . " non selezionato.\");\n";
                //echo "   f.".$key.".focus();\n";
                echo "   return(false);\n";
                echo "}\n";
            } elseif ($campo[$Parametri["TipoCampo"]] == "CHECKBOX") {
                echo "var controllo=false;\n";
                echo "with (f) \n";
                echo "{\n";
                echo "	for (var i=0; i < elements.length; i++)\n";
                echo "	{\n";
                echo "		if (elements[i].name=='" . $key . "[]')\n";
                echo "		{ \n";
                echo "			controllo=check_control(f['" . $key . "[]'])\n;";
                echo "		}\n";
                echo "	}\n";
                echo "}\n";
                echo "if (controllo==false) {\n";
                if ($campo[$Parametri["Label"]] != "")
                    echo "   alert(\"" . $campo[$Parametri["Label"]] . " non selezionato.\");\n";
                else
                    echo "   alert(\"E' necessario confermare la presa visione delle condizioni.\");\n";
                //echo "   f.".$key.".focus();\n";
                echo "   return(false);\n";
                echo "}\n";
            } elseif ($campo[$Parametri["TipoCampo"]] == "HTMLAREA") {
                echo "try {";
                echo "	if(typeof(document.getElementById('" . $key . "_ed').EscapeUnicode) == 'undefined') {";
                echo "		throw \"Error\"";
                echo "	} else {";
                echo "		document.getElementById('" . $key . "_ed').EscapeUnicode = true;";
                echo "		document.getElementById('" . $key . "').value = document.getElementById('" . $key . "_ed').value;";
                echo "	}";
                echo "}";
                echo "catch(er) {";
                echo "	document.getElementById('" . $key . "').value = document.getElementById('" . $key . "_alt').value;";
                echo "}";
            } elseif ($campo[$Parametri["TipoCampo"]] == "TEXT" || $campo[$Parametri["TipoCampo"]] == "PASSWORD") {
                if ($campo[$Parametri["Parametro4"]] != "" && is_numeric($campo[$Parametri["Parametro4"]])) {
                    echo "if (f." . $key . ".value.length <" . $campo[$Parametri["Parametro4"]] . " )";
                    $mymex = " deve essere lungo almeno " . $campo[$Parametri["Parametro4"]] . " caratteri";
                } else {
                    echo "if (f." . $key . ".value.length < 1)";
                    $mymex = " errato o mancante";
                }
                echo "{\n";
                echo "   alert(\"" . $campo[$Parametri["Label"]] . $mymex . ".\");\n";
                echo "   f." . $key . ".focus();\n";
                echo "   return(false);\n";
                echo "}\n";
            } else {
                echo "if (f." . $key . ".value.length < 1) {\n";
                echo "   alert(\"" . $campo[$Parametri["Label"]] . " errato o mancante.\");\n";
                echo "   f." . $key . ".focus();\n";
                echo "   return(false);\n";
                echo "}\n";
            }
        } elseif ($campo[$Parametri["TipoCampo"]] == "HTMLAREA") {
            echo "try {";
            echo "	if(typeof(document.getElementById('" . $key . "_ed').EscapeUnicode) == 'undefined') {";
            echo "		throw \"Error\"";
            echo "	} else {";
            echo "		document.getElementById('" . $key . "_ed').EscapeUnicode = true;";
            echo "		document.getElementById('" . $key . "').value = document.getElementById('" . $key . "_ed').value;";
            echo "	}";
            echo "}";
            echo "catch(er) {";
            echo "	document.getElementById('" . $key . "').value = document.getElementById('" . $key . "_alt').value;";
            echo "}";
        }

        if ($key == "E_Mail" && ($campo[$Parametri["Obbligatorio"]] == "S" || $campo[$Parametri["Obbligatorio"]] == "SNM")) {
            echo "if (emailCheck(f." . $key . ".value,1) == false) {\n";
            echo "   alert(\"L'indirizzo " . $campo[$Parametri["Label"]] . " ? errato.\");\n";
            echo "   f." . $key . ".focus();\n";
            echo "   return(false);\n";
            echo "}\n";
        }

        if (substr($key, 0, 14) == "Codice_Fiscale" && ($campo[$Parametri["Obbligatorio"]] == "S" || $campo[$Parametri["Obbligatorio"]] == "SNM") && ($condiz != "") && (eval("if (" . $condiz . ") return 1; else return 0;"))) {
            echo "if (ControllaCF(f." . $key . ".value) != 'OK') {\n";
            echo "   alert(\"Il " . $campo[$Parametri["Label"]] . " ? errato.\");\n";
            echo "   f." . $key . ".focus();\n";
            echo "   return(false);\n";
            echo "}\n";
        }
    }
}

if ($conferma == 1)
    echo "if (!confirm('Confermi l\'operazione?')) return (false);\n";
?>
        return true;
    }

    function check_control(field) {
        var prova=false;
        for (i = 0; i <= field.length; i++) {
            if (field[i].checked)
                return true;
        }
        if (i==0)
        {
            if (field.checked)
                return true;
        }

        return false;
    }

    function validate_Email(f) {
        if (emailCheck(f.E_Mail.value,1) == false) {
            alert("L'indirizzo E_Mail ? errato.");
            f.E_Mail.focus();
            return(false);
        }
        return true;
    }
<?
if (isset($Help[$nome]))
    include $PROGETTO . "/help1.inc";
?>	

   
</script>
