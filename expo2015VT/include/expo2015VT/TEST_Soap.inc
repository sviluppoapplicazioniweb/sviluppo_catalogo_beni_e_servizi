<?
	//echo "test SOAP";
	
	require_once("nusoap/nusoapmime.php");
	//$PATH_TO_CERTS = "certificates";
	
	//$cert = $PATH_TO_CERTS.'/cert.crt';
	//$PATHINC = $PATHHOME."include/";
	$sslcertfile = $PATHINC.'/siexpo2015/clientCert.pem';
	$sslkeyfile = $PATHINC.'/siexpo2015/privateKey.pem';
	$passphrase = "1234";
	//$sslcertfilepers = $PATHINC.'/siexpo2015/wstelemaco.p12';
	
	$certRequest = array('','sslcertfile'=>$sslcertfile,'sslkeyfile'=>$sslkeyfile,'passphrase'=>$passphrase);
	
	//$client = new nusoap_client('http://localhost/soap/server/hellowsdl.php?wsdl', true);
	//$client = new nusoap_client('http://webservices.dotnethell.it/codicefiscale.asmx?WSDL', true);
	$client = new nusoap_client_mime('https://portari.infocamere.it/riso/PortaApplicativa?wsdl', true);
	//$mynamespace = "http://cert.controller.portaApplicativa.ictechnology.it/";
	$client->setCredentials('', '', $authtype = 'certificate', $certRequest);
	
	$err = $client->getError();
	
	if ($err) echo '<h2>Constructor error</h2><pre>'.$err.'</pre>';
	
	$client->setHTTPEncoding($enc='gzip', 'deflate');
	$client->soap_defencoding = "UTF-8";
	$cid = $client->addAttachment('', $sslcertfile,'', false);
	$param1 = array('codiceFiscale' => '06561570968','certificato' => $cid);
	//$client->setHeaders('<LicenseHeader><LicenseKey></LicenseKey></LicenseHeader>');
	$result = $client->call('ricercaCF', array($param1));
	//$result = $client->call('ricercaCF', array('codiceFiscale' => '06561570968',
	//		'certificato' => $cid));
	
	if ($client->fault) {
		echo '<h2>Fault</h2><pre>'; print_r($result); echo '</pre>';
	} else {
		$err = $client->getError();
		if ($err) {
			echo '<h2>Error</h2>' . $err . '';
		} else {
			echo '<h2>Result</h2><pre>'; print_r($result); echo '</pre>';
			echo '<h2>Attachments</h2><pre>';
			$attachments = $client->getAttachments();
			foreach ($attachments as $a) {
				echo 'Filename: ' . $a['filename'] . "\r\n";
				echo 'Content-Type: ' . $a['contenttype'] . "\r\n";
				echo 'cid: ' . htmlspecialchars($a['cid'], ENT_QUOTES) . "\r\n";
				echo htmlspecialchars($a['data'], ENT_QUOTES);
				echo "\r\n";
			}
			echo '</pre>';
		}
	}
	/*if ($client->fault) {
	
		// Manage error
	
	}*/
	
	echo '<h2>Request</h2>';
	
	echo ''.htmlspecialchars($client->request, ENT_QUOTES).'';
	
	/*echo '<h2>Response</h2>';
	
	echo '<pre>'.htmlspecialchars($client->response, ENT_QUOTES).'</pre>';*/
	
	echo '<h2>Debug</h2>';
	
	echo ''.htmlspecialchars($client->debug_str, ENT_QUOTES).'';
	/*for ($i=0;$i<=count($result);$i++){
		echo "Risultato:".$result[0];
	}*/
	//print_r($result);
	
?>