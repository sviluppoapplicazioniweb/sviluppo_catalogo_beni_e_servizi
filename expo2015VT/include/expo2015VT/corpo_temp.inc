<?
$db_menu = new DB_CedCamCMS;
$db_html = new DB_CedCamCMS;

if ($auth->auth["utype"]=="")
	$utype2=99;
else
	$utype2=$auth->auth["utype"];

$tipovis="table";

if(!(isset($daabstract)))
{
	$SQLmenu="select * from ".$tabsvil."T_Menu, ".$tabsvil."T_Box";
	if ($Lang!=$LinguaPrincipale)
		$SQLmenu.=", ".$tabsvil."T_Menu_AL";
	$SQLmenu.=" where ".$tabsvil."T_Menu.Id=".$tabsvil."T_Box.Id_Menu2 and ".$tabsvil."T_Menu.Id_Menu='".$explode."' and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."T_Menu.TipiUtente='|' or ".$tabsvil."T_Menu.TipiUtente='|0|')";
	if ($Lang!=$LinguaPrincipale)
	{
		$SQLmenu.=" and ".$tabsvil."T_Menu_AL.Id_AL=".$tabsvil."T_Menu.Id and ".$tabsvil."T_Menu_AL.Lang_AL='".$Lang."'";
		if ($noPredefinita) $SQLmenu.=" and ".$tabsvil."T_Menu_AL.Titolo_AL<>''";
	}
	if ($preview!=1)
		$SQLmenu.=" and Stato_Box='A'";
	else
		$SQLmenu.=" and (Stato_Box='A' or Stato_Box='S')";

	$SQLmenu.=" order by Ordine";

	if ($DEBUG) echo "<!-- SQLmenu $SQLmenu -->\n";
	$box=0;
	if ($db_menu->query($SQLmenu))
	{
		while($db_menu->next_record())
		{
			$box++;
			$SQLhtml="select * from ".$tabsvil."TJ_Articoli_X_Box_X_Menu,".$tabsvil."T_Articoli,Tlk_TipiHtml";
			if ($Lang!=$LinguaPrincipale)
				$SQLhtml.=", ".$tabsvil."T_Articoli_AL";
			$SQLhtml.=" where ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Box=".$db_menu->f('Id_Box')." and ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Menu=".$db_menu->f('Id_Menu2')." and ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Articolo=".$tabsvil."T_Articoli.Id_Articolo and Tlk_TipiHtml.Id=".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_TipoHtml";
			if ($Lang!=$LinguaPrincipale)
			{
				$SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Id_AL=".$tabsvil."T_Articoli.Id_Articolo and ".$tabsvil."T_Articoli_AL.Lang_AL='".$Lang."'";
				if ($noPredefinita) $SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Titolo_Html_AL<>''";
			}
			if ($preview!=1)
				$SQLhtml.=" and Stato_Articolo='A'";
			else
				$SQLhtml.=" and (Stato_Articolo='A' or Stato_Articolo='S')";
			$SQLhtml.=" and ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."TJ_Articoli_X_Box_X_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."TJ_Articoli_X_Box_X_Menu.TipiUtente='|' or ".$tabsvil."TJ_Articoli_X_Box_X_Menu.TipiUtente='|0|')";
//$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."T_Menu.TipiUtente='|' or ".$tabsvil."T_Menu.TipiUtente='|0|')";

			$SQLhtml.=" order by Ordine2";
			if ($DEBUG) echo "<!-- SQLhtml $SQLhtml -->\n";

			if ($db_html->query($SQLhtml))
			{
				$colonne=$db_html->num_rows($SQLhtml);
				if ($colonne!=0)
				{

if ($tipovis=="div")
{
					if ($colonne>1)
						echo "<div class=\"split".$colonne."\">\n";
}
else
{
					echo "<table class=\"tmpl\">\n";
					echo "<tr>\n";
/*
					switch ($colonne)
					{
					case 1:
						$perc_width="100%";
						break;
					case 2:
						$perc_width="48%";
						break;
					case 3:
						$perc_width="33%";
						break;
					case 4:
						$perc_width="25%";
						break;
					}
*/
}

					$colbox=1;
					while($db_html->next_record())
					{

if ($tipovis=="div")
{
						if ($colonne>1)
							echo "<div class=\"box".$colbox."\">\n";
}
else
{
							echo "\n<td class=\"split".$colonne." ".$db_html->f('css')."\">\n";
}

						if ($utype2<=2 && $preview!=1)
							include $PROGETTO."/admintool.inc";

						$TipoTesto=$db_html->f('TipoTesto');

						if ($preview==1)
							$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_Svil');
						elseif ($Lang!=$LinguaPrincipale)
							$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_AL');
						else
							$titolo_barra=$db_html->f('Titolo_'.$TipoTesto);

						if ($preview==1)
							$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_Svil'));
						elseif ($Lang!=$LinguaPrincipale)
							$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_AL'));
						else
							$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto));

						if ($preview==1)
							$link=$db_html->f('Link_'.$TipoTesto.'_Svil');
						elseif ($Lang!=$LinguaPrincipale)
							$link=$db_html->f('Link_'.$TipoTesto.'_AL');
						else
							$link=$db_html->f('Link_'.$TipoTesto);

						if (is_numeric($link))
							$link= "index.phtml?Id_VMenu=".$link;
						elseif ($link=="")
							$link= "index.phtml?Id_VMenu=".$Id_VMenu."&daabstract=".$db_html->f('Id_Articolo');
						
						echo "<a id=\"articolo".$db_html->f('Id_Articolo')."\"></a>";
						echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_TipiHtml'));

if ($tipovis=="div")
{
						if ($colonne>1)
							echo "\n</div> <!-- box -->\n";
}
else
{
						echo "\n</td>";
}

						$colbox++;
					}
if ($tipovis=="div")
{
					if ($colonne>1)
						echo "</div>\n";
					echo "<br style=\"clear:both;\" />";
}
else
{
					echo "\n</tr>";
					echo "</table>\n";
}
				}
				else
				{
					if ($utype2<=2 && $preview!=1)
						include $PROGETTO."/admintool.inc";
				}
			}
		}
//		if ($box==0)
//			echo "<script> document.location='/'; </script>";		
	}
	else
		echo "SQL Error: $SQLmenu";

	if ($box==0 && $utype2<=2 && $preview!=1)
	{
		$SQLmenu="select * from ".$tabsvil."T_Menu";
		$SQLmenu.=" where ".$tabsvil."T_Menu.Id_Menu='".$explode."' and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."T_Menu.TipiUtente='|' or ".$tabsvil."T_Menu.TipiUtente='|0|')";

		if ($db_menu->query($SQLmenu))
		{
			$db_menu->next_record();
			include $PROGETTO."/admintool.inc";
		}
	}
}
else
{
	$colonne=1;

	$SQLhtml="select ".$tabsvil."T_Articoli.*,Tlk_TipiHtml.Testo_TipiHtml";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=", ".$tabsvil."T_Articoli_AL.*";
	$SQLhtml.=" from ".$tabsvil."T_Articoli,Tlk_TipiHtml";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=",".$tabsvil."T_Articoli_AL";
	$SQLhtml.=" where Id_Articolo=".$daabstract." and Tlk_TipiHtml.Id=2";
	if ($Lang!=$LinguaPrincipale)
	{
		$SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Id_AL=".$tabsvil."T_Articoli.Id_Articolo and ".$tabsvil."T_Articoli_AL.Lang_AL='".$Lang."'";
		if ($noPredefinita) $SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Titolo_Html_AL<>''";
	}

//	if ($Lang!=$LinguaPrincipale)
	if ($db_html->query($SQLhtml))
	{
		$db_html->next_record();
		$TipoTesto="Html";
		if ($utype2<=2 && $preview!=1)
			include $PROGETTO."/admintool.inc";

		if ($preview==1)
			$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_Svil');
		elseif ($Lang!=$LinguaPrincipale)
			$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_AL');
		else
			$titolo_barra=$db_html->f('Titolo_'.$TipoTesto);

		if ($preview==1)
			$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_Svil'));
		elseif ($Lang!=$LinguaPrincipale)
			$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_AL'));
		else
			$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto));

		if ($preview==1)
			$link=$db_html->f('Link_'.$TipoTesto.'_Svil');
		elseif ($Lang!=$LinguaPrincipale)
			$link=$db_html->f('Link_'.$TipoTesto.'_AL');
		else
			$link=$db_html->f('Link_'.$TipoTesto);

		if (is_numeric($link))
			$link= "index.phtml?Id_VMenu=".$link;

		echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_TipiHtml'));
	}
}
		echo "<br />";
?>