<?

class clsDigiObject {
	
	static function friendly_debug_print($debug_backtrace,$kstep) {
		$s="<p>";
		$i=0;
		
		
		foreach($debug_backtrace as $line) {
						
			if ($i>=$kstep) {
				$s.="<div># ";
				$s.=str_replace("/nas_int/internet","",$line["file"]);
				$s.=" (" . $line["line"] . ") ";
        $s.=" @" . $line["class"].":";
				$s.= $line["function"];
				
				$s.="</div>";
				
				
				foreach($line["args"] as $arg) {
					$s.="<div><font color='#999999'><i>---> " .$arg. "</i></font></div>";
				}
				
				
			}
			$i++;			
		}
		$s.="</p>";
		return $s;
	}
	
	private function logError($msg) {
		global $MAILADMIN, $MAILERR;
		$trace = debug_backtrace();
		

	$s="";
	$s.="<br/>Msg: " . $msg;	            
	$s.="<br/>";	            
	$s.="<br/>" . clsDigiObject::friendly_debug_print($trace,0);
    
    $t = print_r($trace,true);
    $s.="<br/>Error: " . $trace[0]['file'];
    $s.="<br/>Error: " . $trace[0]['line'];
    //$s.="<br/>Trace:<br/><pre>" . $t . "</pre>";

	/*
    $t = print_r($GLOBALS,true);
    $s.="<br/>";
    $s.="<br/>Global:";
    $s.="<br/><pre>" . $t . "</pre>";
    */
    
	
    send_mail_html($MAILERR, $MAILADMIN, "Object Error", $s);            

        $s = $msg .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'];
    
        trigger_error(
            $s,
            E_ERROR);
	
	
	}
	
    public function __get($name) {
        $this->logError('Undefined property via __get(): ' . $name);
        return null;
    }
    public function __call($name, $arguments) {
        $this->logError('Undefined method via __call(): ' . $name);
        return null;
    	
        //echo "Calling object method '$name' " . implode(', ', $arguments). "\n";
    }

    /**  As of PHP 5.3.0  
    public static function __callStatic($name, $arguments) {
        logError('Undefined method via __callStatic(): ' . $name);
        return null;
    }
    */
}

?>