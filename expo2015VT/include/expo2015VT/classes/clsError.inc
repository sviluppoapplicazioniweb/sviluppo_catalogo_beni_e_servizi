<?

require_once($PROGETTO."/classes/clsDigiObject.inc");
require_once($PROGETTO."/classes/clsLog.inc");
require_once($PROGETTO."/classes/clsCqsHash.inc");

class clsError extends clsDigiObject  {

	static function sendWarning($warning_msg) {
		global $MAILADMIN, $MAILERR;

		global $MAILADMIN, $MAILERR;
		
		$trace = debug_backtrace();
		
		$msg="";
		$msg.=clsError::getRequestInfo();
		$msg.="<hr>";
		$msg.=clsError::addShowGlobalsLink();
		$msg.=clsError::addGenericLink("Trace", print_r($trace,true));
		$msg.="<hr>";
		$msg.=clsError::getUserInfo();
		$msg.="<hr>";
		
		$s="";
	    
		$s.="<p><div><font color='#999999'>Warning:</font></div>";
		$s.="<div>Message: " . $warning_msg ."</div>";
		$s.="</p>";;

		$msg.=$s;			
			
		$msg.=clsError::getTrace($trace,3);
		
		send_mail_html($MAILERR, $MAILADMIN, "Warning", $msg);		
	}

	static function sendWarningAndExit($msg) {
		clsError::sendWarning($msg);
		exit;
	}	
	
	static function getRequestInfo() {
		$msg="<p><i><font color='#999999'>Questo messaggio è stato generato automaticamente in seguito ad un errore.</font></i></p>";
		
		$msg.="<p><div><font color='#999999'>L'indirizzo richiesto che ha generato l'errore è:</font></div>" ;
		$msg.="<div>REQUEST_URI=".$GLOBALS['REQUEST_URI'];
		$msg.="</div></p>";
		
		$msg.="<p><div><font color='#999999'>L'indirizzo IP da cui proviene l'errore è: </font></div>";
		$msg.="<div>IP=".$_SERVER['REMOTE_ADDR'];
		$msg.="</div></p>";
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			
			$msg.="<p><div><font color='#999999'>L'errore arriva da una navigazione con proxy.</font></div>";
			$msg.="<div>REAL IP=".$_SERVER['HTTP_CLIENT_IP'];
			$msg.="</div></p>";
		}
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$msg.="<p><div><font color='#999999'>L'errore arriva da una navigazione con proxy.</font><div>";
			$msg.="<div>FROM PROXY IP=".$_SERVER['HTTP_X_FORWARDED_FOR'];
			$msg.="</div></p>";
		}
				
		return $msg;
	}
	
	static function writeGlobal() {
		$n = (rand(0,1000)* 1000+rand(0,1000))*1000+rand(0,1000);
		
		$file_name=$GLOBALS["env"]->path->log_path;
		$file_name.="Errors/";
		$file_name.=date("Ymd")."/";
		$file_name.=date("Ymd_His")."_".str_pad($n, 15, "0", STR_PAD_LEFT).".txt";

		if(!(file_exists(dirname($file_name)))) {
			mkdir(dirname($file_name),0777,true);	
		}

		/*
		   $file = basename($FileName);
		   $file = dirname($FileName);
		*/
		
		$msg=print_r($GLOBALS,true);
		
		
			$fp=fopen($file_name,"x");
			fwrite($fp, $msg);
			fclose($fp); 	

		return $file_name;
		
	}
	
	static function writeLinkedFile($content) {
		$n = (rand(0,1000)* 1000+rand(0,1000))*1000+rand(0,1000);
		
		$file_name=$GLOBALS["env"]->path->log_path;
		$file_name.="Errors/";
		$file_name.=date("Ymd")."/";
		$file_name.=date("Ymd_His")."_".str_pad($n, 15, "0", STR_PAD_LEFT).".txt";

		if(!(file_exists(dirname($file_name)))) {
			mkdir(dirname($file_name),0777,true);	
		}		

		/*
		   $file = basename($FileName);
		      $file = dirname($FileName);
		*/
		
				$msg=$content;
		
		
			$fp=fopen($file_name,"x");
			fwrite($fp, $msg);
			fclose($fp); 	

		return $file_name;
		
	}	
	
	static function addGenericLink($link_name, $content) {
		global $HOMEURL;
		
		$file_name=clsError::writeLinkedFile($content);
		
		$u=new clsCqsHash();
		$u->addParam("task", "download_file");
		$u->addParam("file_name_base64", base64_encode($file_name));
		
		
		$msg="<p><div><font color='#999999'>Al messaggio di errore è stato allegato il seguente file:</font></div>";
		$msg.="<div><a href='${HOMEURL}admin/doThings.phtml?" . $u->getUrl() . "'>" . $link_name . "</a></div></p>";		
		
		return $msg;
	}	

	
	static function addShowGlobalsLink() {
		global $HOMEURL;
		
		$file_name=clsError::writeGlobal();
		
		$u=new clsCqsHash();
		$u->addParam("task", "download_file");
		$u->addParam("file_name_base64", base64_encode($file_name));
		
		$msg="";
		$msg.="<p><div><font color='#999999'>Dal link seguente è possibile vedere il global della pagina:</font></div>";
		$msg.="<div><a href='${HOMEURL}admin/doThings.phtml?" . $u->getUrl() . "'>Show Globals</a></div></p>";		
		
		return $msg;
	}

/*	
	if(isset($HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR']))
  {
  $real_ip=$HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR'];
  if(ereg('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$',$real_ip)) $ip=$real_ip; // Se l'IP � valido � l'IP reale dell'utente
  }
*/	
  
	static function checkValidIP($ip) {
		return (ereg('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$',$ip));
	}  
	
	static function sendAccessoNonAutorizzato() {
		global $MAILADMIN, $MAILERR, $auth;
		
		// VG:20101004 - invio la mail solo se l'utente è loggato
		
		if(is_object($auth)) {

			$trace = debug_backtrace();

			$msg="";
			$msg.=clsError::getRequestInfo();
			$msg.="<hr>";
			$msg.=clsError::addShowGlobalsLink();
			$msg.=clsError::addGenericLink("Trace", print_r($trace,true));
			$msg.="<hr>";
			$msg.=clsError::getUserInfo();
			$msg.="<hr>";
			$msg.=clsError::getTrace($trace,1);
			
			$msg.="<hr>";
			
			send_mail_html($MAILERR, $MAILADMIN, "Accesso Non Autorizzato", $msg);
		
		}
	}	
	
	static function sendDataBaseError($dbmsg,$Errno,$Error) {
		global $MAILADMIN, $MAILERR;
		
		$trace = debug_backtrace();
		
		$msg="";
		$msg.=clsError::getRequestInfo();
		$msg.="<hr>";
		$msg.=clsError::addShowGlobalsLink();
		$msg.=clsError::addGenericLink("Trace", print_r($trace,true));
		$msg.="<hr>";
		$msg.=clsError::getUserInfo();
		$msg.="<hr>";
		
		$s="";
	    
	    $n=1000;
	    if (strlen($dbmsg)>$n) {
			$s.="<p><div><font color='#999999'>Dettagli dell'errore DB generato:</font></div>";
			$s.="<div>ErrNo: " . $Errno ."</div>";
		    $s.="<div>Error: " . $Error ."</div>";
		    $s.=clsError::addGenericLink("Download Complete SQL Error", $dbmsg);
		    $s.="<div>Database Error: " . substr($dbmsg,0,$n) . "...";
	    } else {
			$s.="<p><div><font color='#999999'>Dettagli dell'errore DB generato:</font></div>";
		    $s.="<div>ErrNo: " . $Errno ."</div>";
		    $s.="<div>Error: " . $Error ."</div>";
		    $s.="<div>Database Error: " . $dbmsg ."</div>";;
		}

		$msg.=$s;			
			
		$msg.=clsError::getTrace($trace,3);
		
		
		send_mail_html($MAILERR, $MAILADMIN, "Database Error", $msg);
	}

	
	static function sendException($ex_desc,Exception $ex) {
		global $MAILADMIN, $MAILERR;
		
		$trace = debug_backtrace();
		
		$msg="";
		$msg.=clsError::getRequestInfo();
		$msg.="<hr>";
		$msg.=clsError::addShowGlobalsLink();
		$msg.=clsError::addGenericLink("Trace", print_r($trace,true));
		$msg.="<hr>";
		$msg.=clsError::getUserInfo();
		$msg.="<hr>";
		
		$s="";
	    
		$exmsg="";
		$exmsg.="<br/>getCode: " . $ex->getCode() . "<br/>";
		$exmsg.="<br/>getFile: " . $ex->getFile() . "<br/>";
		$exmsg.="<br/>getLine: " . $ex->getLine(). "<br/>";
		$exmsg.="<br/>getMessage: " . $ex->getMessage() . "<br/>";
		$exmsg.="<br/>getTraceAsString: <pre>" . $ex->getTraceAsString() . "</pre><br/>";
		$exmsg.="<br/>getTrace: <pre>" . print_r($ex->getTrace(),true) . "</pre><br/>";
		
	    $n=1000;
	    if (strlen($exmsg)>$n) {
			$s.="<p><div><font color='#999999'>Dettagli dell'eccezione generata:</font></div></p>";
		    $s.=clsError::addGenericLink("Download Complete Exception", $exmsg);
		    $s.="<div>Database Error: " . substr($exmsg,0,$n) . "...";
	    } else {
			$s.="<p><div><font color='#999999'>Dettagli dell'eccezione generata:</font></div></p>";
		    $s.="<div>Database Error: " . $exmsg ."</div>";;
		}

		$msg.=$s;			
			
		$msg.=clsError::getTrace($trace,1);
		
		
		send_mail_html($MAILERR, $MAILADMIN, "Exception - " . $ex_desc, $msg);
	}
 	
	
	static function getTrace($trace,$kstep) {
		$n=1000;
		$msg="<p><div><font color='#999999'>L'errore è stato generato la seguente traccia</font></div>";
		$msg.="<div># " . str_replace("/nas_int/internet","",$trace[0]['file']) . " (" . $trace[0]['line'] . ") @" . $trace[0]['class'] . ":" . $trace[0]['function'] ."</div></p>";
							    
		$msg.="<div>" . substr(clsError::friendly_debug_print($trace,$kstep),0,$n) ."</div></p>";	

		return $msg;
	}	
	
	static function getUserInfo() {
		global $auth;
		$msg="";
		
		if(is_object($auth)) {
			$msg.="<p><div><font color='#999999'>L'errore è stato generato da:</font></div>";
			$msg.="<div>Utente registrato</div></p>";
			$msg.="<p><div><font color='#999999'>Informazioni sull'utente:</font></div>";
			$msg.="<div>utype=" . $auth->auth[utype]."</div>";
			$msg.="<div>uid=" . $auth->auth[uid]."</div>";
			$msg.="<div>uname=" . $auth->auth[uname]."</div></p>";
			
			//$msg.="<br/>auth=" . "<pre>" . print_r($auth,true) . "</pre>";
			
		} else {
			$msg.="<p><div><font color='#999999'>L'errore è stato generato da:</font></div>";
			$msg.="<div>Utente non registrato</div></p>";
			
		}		

		return $msg;
	}
	
}
?>