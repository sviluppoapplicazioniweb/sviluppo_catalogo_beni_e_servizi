<?
include $PROGETTO."/view/languages/EN.inc";
//es.
//include $PROGETTO."/lang_EOL.inc";

//-----------------------------------------------------------------
//									Gestione applicazioni
//-----------------------------------------------------------------

$Chiave["T_Applicazioni"]="Id";
$Permessi["T_Applicazioni"]=";1;2;";
$Location["T_Applicazioni"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=elenchi&amp;nome=T_Applicazioni";
$T_Applicazioni["Id|||"] = "Id||I";
$T_Applicazioni["Applicazione|||"] = "Applicazione||S|N||||TEXT|30|250";
$T_Applicazioni["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tj_Applicazioni_X_Utente"]="Id_Applicazione;Id_Utente";
$Permessi["Tj_Applicazioni_X_Utente"]=";1;2;";
$Azioni["Tj_Applicazioni_X_Utente"]["1"]="Id_Applicazione,Id_Applicazione;Id_Utente,Id|Elimina";
$Location["Tj_Applicazioni_X_Utente"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Anagrafica&amp;azione=UPD&amp;Id=$Id_Utente&Id_TipoUtente=98";
$Tj_Applicazioni_X_Utente["Id_Applicazione||||"] = "Utente||H|";
$Tj_Applicazioni_X_Utente["Id_Utente|||"] = "Utente||N|S||||HIDDEN|Id_Utente";
$Tj_Applicazioni_X_Utente["Id_Applicazione|||"] = "Applicazione||S|S||||SELECTFILTRO|T_Applicazioni;Id;Applicazione";
$Tj_Applicazioni_X_Utente["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";

//-----------------------------------------------------------------
//									Gestione Eventi
//-----------------------------------------------------------------

$Chiave["T_Eventi"]="Id_Evento";
$Permessi["T_Eventi"]=";1;2;";
$Azioni["T_Eventi"]["1"]="|Elimina";
$Azioni["T_Eventi"]["2"]="|Elimina";
$Location["T_Eventi"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=elenchi&amp;nome=T_Eventi";
$T_Eventi["Menu|||"]="Gestione Eventi||N|N||||LABEL|CENTER";
$T_Eventi["Id_Evento|||"] = "Id||I";
$T_Eventi["Nome_Evento|||"] = "Nome||S|S||||TEXT|40|250";
$T_Eventi["Descrizione_Evento|||"] = "Descrizione||N|N||||HTMLAREA|10|50";
$T_Eventi["Link_Evento|||"] = "Link||N|N||||TEXT|40|250";
$T_Eventi["Data_Inizio|||"] = "Data Inizio (aaaa-mm-dd)||S|S||||TEXT|10|10";
$T_Eventi["Data_Fine|||"] = "Data Fine (aaaa-mm-dd)||S|S||||TEXT|10|10";
$T_Eventi["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

//-----------------------------------------------------------------
//									Gestione Blog
//-----------------------------------------------------------------

$Chiave["T_Blog"]="Id_Blog";
$Permessi["T_Blog"]=";1;2;";
$Azioni["T_Blog"]["1"]="|Elimina";
$Azioni["T_Blog"]["2"]="|Elimina";
$Azioni["T_Blog"]["4"]="|Elimina";
$Filtri["T_Blog"]["Categoria"]="||||Tlk_Blog_Categorie|Categoria|Id|Descrizione|Id";
$Filtri["T_Blog"]["Commenti"]="||||T_Blog|Id_Post|Id_Post|Titolo|Id_Blog|Id_Post=0|Id_Blog|||||S";
$Filtri["T_Blog"]["Stato"]="||||Tlk_StatoRecord|Stato|Id|Descrizione|Id";
$Location["T_Blog"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=blog&Categoria=$Categoria&Id_Blog=$Id_Blog&post=$Id_Post";
$T_Blog["Id_Blog|||"] = "Id||I";
$T_Blog["Id_Owner||UPD|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|Id_Owner|";
$T_Blog["Id_Owner||INS||"] = "Proprietario||N|N||||HIDDEN|uid|";
$T_Blog["Id_Post||||"] = "Proprietario||F|N||||HIDDEN||";

$T_Blog["Categoria||INS|"] = "Categoria||F|N||||HIDDEN|Categoria|";
$T_Blog["Categoria||UPD|"] = "Categoria||S|S||DIS||SELECTFILTRO|Tlk_Blog_Categorie;Id;Descrizione|1|Id|Categoria";
$T_Blog["Titolo|||"] = "Titolo||S|S||||TEXT|40|100";
$T_Blog["Testo|||"] = "Testo||N|S||||TEXTAREA|10|40";

$GRUPPO_MODERATORE=2;
$NONATTIVA="D";
$group_appl=strpos($mygroup,"|".$GRUPPO_MODERATORE."|");
if(!($group_appl===false))
	$T_Blog["Stato||UPD|"] = "Stato||S|S||||SELECTFILTRO|Tlk_StatoRecord;Id;Descrizione|1";
else
	$T_Blog["Stato||INS|"] = "Stato||S|S||||HIDDEN||D|";

$T_Blog["Data_Inserimento||INS|"] = "||N|N||||DATA|";
$T_Blog["Data_Inserimento||UPD|"] = "Data Inserimento||N|N||DIS||TEXT|10|10";
$T_Blog["Data_Modifica||UPD|"] = "||N|N||||DATA|";
$T_Blog["Id_Modifica||UPD||"] = "||N|N||||HIDDEN|uid|";
$T_Blog["Data_Modifica||UPD||"] = "Data Ultima Modifica||N|N||DIS||TEXT|10|10";
$T_Blog["Id_Modifica||UPD|"] = "Modificato da||N|N||DIS||SELECTFILTRO|T_Anagrafica;Id;Login|1|Id|Id_Modifica|";
$T_Blog["Pulsante|||"] = "Inserisci||N|N||||BUTTON|CENTER";

//-----------------------------------------------------------------
//									Gestione FAQ
//-----------------------------------------------------------------

$GESTIONE_FAQ=21;

$Chiave["FAQ_T_Faq"]="Id_Faq";
$Permessi["FAQ_T_Faq"]=";1;2;3;15;97;98;99;";
$Azioni["FAQ_T_Faq"]["1"]="|"._FAQ_VISUALIZZA_."|"._FAQ_MODIFICA_."|"._FAQ_ELIMINA_."|"._FAQ_PUBBLICA_."|"._FAQ_OFFLINE_."";
$Azioni["FAQ_T_Faq"]["98"]="|"._FAQ_VISUALIZZA_."";
$Modifica["FAQ_T_Faq"]="H";
$Ordina["FAQ_T_Faq"]="Stato_Record";
$Inserimento["FAQ_T_Faq"]["95"]="Y";
$Inserimento["FAQ_T_Faq"]["96"]="Y";
$Inserimento["FAQ_T_Faq"]["97"]="Y";
$Inserimento["FAQ_T_Faq"]["98"]="Y";
$Inserimento["FAQ_T_Faq"]["99"]="N";

$Location["FAQ_T_Faq"]["INS"]["default"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["UPD"]["default"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Pubblica Faq"]["1"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Metti Offline"]["1"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Elimina"]["1"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["ModificaFaq"]["1"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Pubblica Faq"]["95"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Metti Offline"]["95"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["Elimina"]["95"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";
$Location["FAQ_T_Faq"]["ModificaFaq"]["95"] = "/index.phtml?pagina=elenchi&nome=FAQ_T_Faq&Id_VMenu=288";

$Filtri["FAQ_T_Faq"]["Id_Categoria_Faq2"]="||N|Id_Categoria_Faq2||||||Id_Categoria_Faq2=2||||||";
$Filtri["FAQ_T_Faq"][""._FAQ_ARGOMENTO_.""]="||||FAQ_Tlk_Sottocategorie_Faq|Id_Sottocategoria_Faq|Id_Sottocategoria_Faq|Descrizione_AL||Id_Categoria_Faq=2|Id_Sottocategoria_Faq|";
$FiltriText["FAQ_T_Faq"][""._FAQ_RICERCA_LIBERA_.""]="||Domanda|30|100|TEXT";
$FAQ_T_Faq["Id_Faq|||"] = "Id||H";

$FAQ_T_Faq["Id_Categoria_Faq2|;1;2;3;15;96;97;98;|INS|"] = "<strong>Category</strong>||F|S||||SELECTREFRESH|FAQ_Tlk_Categorie_Faq;Id_Categoria_Faq;Descrizione_AL";
$FAQ_T_Faq["Id_Sottocategoria_Faq|;1;2;3;15;96;97;98;|INS|"] = "<strong>Subcategory</strong>||F|S||||SELECTFILTRO|FAQ_Tlk_Sottocategorie_Faq;Id_Sottocategoria_Faq;Descrizione_AL|1|Id_Categoria_Faq|Id_Categoria_Faq2";
$FAQ_T_Faq["Domanda_AL|;1;2;3;15;96;97;98;|INS|"] = "<strong>Question</strong>||N|S||||TEXTAREA|4|50";
$FAQ_T_Faq["Risposta_AL|;1;2;3;15;|INS|"] = "<strong>Reply</strong>||N|S||||TEXTAREA|10|50";
$FAQ_T_Faq["Pulsante|;1;2;3;15;96;97;98;|INS|"] = "Run||N|N||||BUTTON|CENTER";

$FAQ_T_Faq["Id_Categoria_Faq2|;1;2;3;15;|UPD|"] = "<strong>Category</strong>||F|S||||SELECTREFRESH|FAQ_Tlk_Categorie_Faq;Id_Categoria_Faq;Descrizione_AL";
$FAQ_T_Faq["Id_Sottocategoria_Faq|;1;2;3;15;|UPD|"] = "<strong>Subcategory</strong>||F|S||||SELECTFILTRO|FAQ_Tlk_Sottocategorie_Faq;Id_Sottocategoria_Faq;Descrizione_AL|1|Id_Categoria_Faq|Id_Categoria_Faq2";
$FAQ_T_Faq["Domanda_AL|;1;2;3;15;|UPD|"] = "<strong>Question</strong>||S|S||||TEXTAREA|4|50";
$FAQ_T_Faq["Risposta_AL|;1;2;3;15;|UPD|"] = "<strong>Reply</strong>||N|S||||TEXTAREA|10|50";
$FAQ_T_Faq["Pulsante|;1;2;3;15;|UPD|"] = "Run||N|N||||BUTTON|CENTER";

$FAQ_T_Faq["Id_Categoria_Faq2|;98;|UPD|"] = "Category||F|N||DIS||SELECTREFRESH|FAQ_Tlk_Categorie_Faq;Id_Categoria_Faq;Descrizione_AL|1||";
$FAQ_T_Faq["Id_Sottocategoria_Faq|;98;|UPD|"] = "Subcategory||F|N||DIS||SELECTFILTRO|FAQ_Tlk_Sottocategorie_Faq;Id_Sottocategoria_Faq;Descrizione_AL|1|Id_Categoria_Faq|Id_Categoria_Faq2";
$FAQ_T_Faq["Domanda_AL|;98;|UPD|"] = "Question||S|N||DIS||TEXT|4|50";
$FAQ_T_Faq["Risposta_AL|;98;|UPD|"] = "Answer||N|N||DIS|SPAN|TEXTAREA|10|50";
$FAQ_T_Faq["indietro|;98;|UPD|"] = "<a class='buttonsLista' href=\"javascript:history.go(-1);\" alt=\"\">"._FAQ_INDIETRO_."</a>||N|N||||LABEL|4|50";

$Chiave["FAQ_Tlk_Categorie_Faq"]="Id_Categoria_Faq";
$Permessi["FAQ_Tlk_Categorie_Faq"]=";1;2;3;15;";
$Azioni["FAQ_Tlk_Categorie_Faq"]["1"]="|Delete";
$Azioni["FAQ_Tlk_Categorie_Faq"]["2"]="|Delete";
$Azioni["FAQ_Tlk_Categorie_Faq"]["3"]="|Delete";
$Azioni["FAQ_Tlk_Categorie_Faq"]["15"]="|Delete";
	
$Location["FAQ_Tlk_Categorie_Faq"]["default"]["default"] = "/index.phtml?Id_VMenu=42";
$FAQ_Tlk_Categorie_Faq["Id_Categoria_Faq|||"] = "Id||I";
$FAQ_Tlk_Categorie_Faq["Descrizione_AL|||"] = "Description||S|S||||TEXT|50|100";
$FAQ_Tlk_Categorie_Faq["Pulsante|||"] = "Run||N|N||||BUTTON|CENTER";

$Chiave["FAQ_Tlk_Sottocategorie_Faq"]="Id_Sottocategoria_Faq";
$Permessi["FAQ_Tlk_Sottocategorie_Faq"]=";1;2;3;15;";
$Azioni["FAQ_Tlk_Sottocategorie_Faq"]["1"]="|Delete";
$Azioni["FAQ_Tlk_Sottocategorie_Faq"]["2"]="|Delete";
$Azioni["FAQ_Tlk_Sottocategorie_Faq"]["3"]="|Delete";
$Azioni["FAQ_Tlk_Sottocategorie_Faq"]["15"]="|Delete";

$Location["FAQ_Tlk_Sottocategorie_Faq"]["default"]["default"] = "/index.phtml?Id_VMenu=43";
$Filtri["FAQ_Tlk_Sottocategorie_Faq"]["Categoria"]="||||FAQ_Tlk_Categorie_Faq|Id_Categoria_Faq|Id_Categoria_Faq|Descrizione|Id_Categoria_Faq||Id_Categoria_Faq";
$FAQ_Tlk_Sottocategorie_Faq["Id_Sottocategoria_Faq|||"] = "Id||I";
$FAQ_Tlk_Sottocategorie_Faq["Id_Categoria_Faq|||"] = "Category||S|S||||SELECTFILTRO|FAQ_Tlk_Categorie_Faq;Id_Categoria_Faq;Descrizione|1|";
$FAQ_Tlk_Sottocategorie_Faq["Descrizione_AL|||"] = "Description||S|S||||TEXT|60|100";
$FAQ_Tlk_Sottocategorie_Faq["Pulsante|||"] = "Run||N|N||||BUTTON|CENTER";

?>