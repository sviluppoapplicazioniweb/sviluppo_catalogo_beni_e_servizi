<?php
/* size dei font */
$size='-2';
$oldsize='-2';


/* parte riguardante l`accesso al database */
class DB_CedCamCMS extends DB_Sql {
/*
  var $Host     = "localhost";
  var $Database = "expo_main";
  var $User     = "root";
  var $Password = "";
  */
  var $Host     = Config::DB_SERVER;
  var $Database = Config::DB_NAME;
  var $User     = Config::DB_USERNAME;
  var $Password = Config::DB_PASSWORD;
  
  var $Halt_On_Error = "yes";
  var $Debug = false;
  var $Auto_Free = 0;
	
  function haltmsg($msg) {
    printf("<br /><strong>Database error:</strong> %s<br />\n", $msg);
    printf("<br /><strong>MySQL Error</strong>: %s (%s)<br />\n",
      $this->Errno, $this->Error);
    printf("<br />Contattare alessandro.ferla@digicamere.it e riportare ");
    printf("l'esatto messaggio di errore.<br />\n");
  }

  function connect($Database = "", $Host = "", $User = "", $Password = "") {
    if ("" == $Database)
      $Database = $this->Database;
    if ("" == $Host)
      $Host     = $this->Host;
    if ("" == $User)
      $User     = $this->User;
    if ("" == $Password)
      $Password = $this->Password;
      
    if ( 0 == $this->Link_ID ) {
    
      $this->Link_ID=mysql_pconnect($Host, $User, $Password);
      if (!$this->Link_ID) {
        $this->halt("pconnect($Host, $User, \$Password) failed.");
        return 0;
      }

		mysql_query("SET character_set_client='utf8'");
		mysql_query("SET character_set_results='utf8'");
		mysql_query("SET character_set_connection='utf8'");
//		mysql_query("SET NAMES 'utf8'");

      if (!@mysql_select_db($Database,$this->Link_ID)) {
        $this->halt("cannot use database ".$this->Database);
        return 0;
      }
    }
    
    return $this->Link_ID;
  }  
}

class CedCamCMS_CT_Sql extends CT_Sql {
  var $database_class = "DB_CedCamCMS";          ## Which database to connect...
  var $database_table = "active_sessions"; ## and find our session data in this table.
}



class CedCamCMS_Session extends Session {
  var $classname = "CedCamCMS_Session";

  var $cookiename     = "";                ## defaults to classname
  var $magic          = "CedCamera!!";      ## ID seed
  var $mode           = "cookie";          ## We propagate session IDs with cookies
  var $fallback_mode  = "";
  var $lifetime       = 0;                 ## 0 = do session cookies, else minutes
  var $that_class     = "CedCamCMS_CT_Sql"; ## name of data storage container
  var $gc_probability = 5;
  var $allowcache = "no";
}

class CedCamCMS_Auth extends Auth {
  var $classname      = "CedCamCMS_Auth";
  var $lifetime       =  200;

  var $database_class = "DB_CedCamCMS";
  var $database_table = "T_Anagrafica,T_Utente";

  function auth_loginform() {
    global $sess,$PROGETTO;

    include $PROGETTO."/loginform.phtml" ;
  }

  function auth_validatelogin() {
    global $Login, $Password;

    if(isset($Login))
	  $this->auth["uname"] = $Login;
    if(isset($Password))
	  $this->auth["pwd"] = $Password;

    $uid = false;
    $this->db->query(sprintf("select T_Anagrafica.*,T_Utente.Id_TipoUtente ".
                             "        from %s ".
                             "       where Login = '%s' ".
                             "         and Password = '%s'".
                             "         and Stato_Record = 'A'".
                             "         and T_Anagrafica.Id = T_Utente.Id",
                          $this->database_table,
                          addslashes($Login),
                          addslashes($Password)));
    while($this->db->next_record()) {
      $uid = $this->db->f("Id");
      $this->auth["utype"] = $this->db->f("Id_TipoUtente");
      $this->auth["prov"] = $this->db->f("Id_Provincia");
      $this->auth["id_utente"] = $this->db->f("Id");
    }
    return $uid;
  }
  function auth_validateuser() {
    global $Login, $Password;

    if(isset($Login))
	  $this->auth["uname"] = $Login;
    if(isset($Password))
	  $this->auth["pwd"] = $Password;

    $uid = false;
    $this->db->query(sprintf("select T_Anagrafica.*,T_Utente.Id_TipoUtente ".
                             "        from %s ".
                             "       where Login = '%s' ".
                             "         and Password = '%s'".
                             "         and Stato_Record = 'D'".
                             "         and T_Anagrafica.Id = T_Utente.Id",
                          $this->database_table,
                          addslashes($Login),
                          addslashes($Password)));
    while($this->db->next_record()) {
      $uid = $this->db->f("Id");
      $this->auth["utype"] = $this->db->f("Id_TipoUtente");
      $this->auth["prov"] = $this->db->f("Id_Provincia");
      $this->auth["id_utente"] = $this->db->f("Id");
    }
    return $uid;
  }
}

/* funzione per verifica_email */

function valid_email($email)
{
   if (eregi("^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}$", $email, $check)) {
      $elem = explode("@",$check[0]);
      $elem1 = explode(".",$elem[1]);
      $elem2 = $elem1[sizeof($elem1)-2].".".$elem1[sizeof($elem1)-1];
      if (getmxrr($elem2, $validate_email_temp) ) {
         return TRUE;
      }
      else
      {
         return FALSE;
      }
   }
   else
   {
      return FALSE;
   }
}

function creacod($codice)
{
	if ($codice=="")
	{
		for ($i = 1; $i <= 3; $i++) {
			srand((double)microtime()*1000000);
			$cifra=rand(1,26);
			switch ($cifra) {
				case '1': $l='A'; break;
				case '2': $l='B'; break;
				case '3': $l='C'; break;
				case '4': $l='D'; break;
				case '5': $l='E'; break;
				case '6': $l='F'; break;
				case '7': $l='G'; break;
				case '8': $l='H'; break;
				case '9': $l='K'; break;
				case '10': $l='I'; break;
				case '11': $l='J'; break;
				case '12': $l='L'; break;
				case '13': $l='M'; break;
				case '14': $l='N'; break;
				case '15': $l='O'; break;
				case '16': $l='P'; break;
				case '17': $l='Q'; break;
				case '18': $l='R'; break;
				case '19': $l='S'; break;
				case '20': $l='U'; break;
				case '22': $l='V'; break;
				case '23': $l='W'; break;
				case '24': $l='X'; break;
				case '25': $l='Y'; break;
				case '26': $l='Z'; break;
			}
			$codice .= $l;
	   	}
	}

   srand((double)microtime()*1000000);
   $cifra=rand(0,999);
   $codice .= sprintf("%03d",$cifra);
return($codice);
}

/*
 * Autore: Alessandro Ferla - 23/01/2013
 * Funzione modificata per poter gestire anche i TIPI file di cui fare
 * l'upload.
 */
function do_upload($FileName, $RealName, $Cartella, $TipoFile = "T", $fileKey, $newName = NULL)
{
    global $PATHDOCS,$PATHHOME,$tmp_upload_path;

    $file = basename($FileName);
	
    if ($newName != NULL){
    	$RealName = $newName;
    }
    
	if (string_begins_with($Cartella,"privatedocs/")) {
		$new_file_name = $PATHHOME.$Cartella."/".$RealName;
	} else {
		$new_file_name = $PATHDOCS.$Cartella."/".$RealName;	    
	}
	
	//echo "Path - $new_file_name";
	//exit;
	
	if(!(file_exists(dirname($new_file_name)))) {
		mkdir(dirname($new_file_name),0777,true);	
	}		

	$error_msg = "";
	
	if ( $FileName_size > $GLOBALS["MAXFILESIZE"] ) {
		if ($Lang == $LinguaPrincipale)
			$error_msg = "Spiacente, file troppo grande.";
		else 
			$error_msg = "Sorry, file too large.";
//		clsError::sendWarningAndExit($error_msg);
		return $error_msg;
	}
	
	if ($TipoFile != "T") {
		$ar_tipofile=explode(";", $TipoFile);
		$trovato = false;
		for($count=0;$count<sizeof($ar_tipofile);$count++) {
			if($ar_tipofile[$count]==$_FILES[$fileKey]['type']){
				$trovato = true;
				break;
			}
		}
		if (!$trovato) {
			/*echo "Tipo File: " . $TipoFile . "\n";
			echo "Tipo File: " . $_FILES[$fileKey]['type'];
			exit();*/
			if ($Lang == $LinguaPrincipale)
				$error_msg = "Invio [$RealName] non riuscito. Spiacente, il tipo file non � coretto.";
			else 
				$error_msg = "Enter [$RealName] failed. Sorry, the file type is not correct.";
			echo "<script>\n";
			if ($Lang == $LinguaPrincipale)
				echo "alert('Attenzione, $error_msg');";
			else 
				echo "alert('Caution, $error_msg');";
			echo "history.go(-1);";
			echo "</script>";
			exit();
			//$error_msg = "Spiacente, il tipo file non � corretto.";
			//return $error_msg;
		}
	}
	
	if (!copy($tmp_upload_path.$file, $new_file_name)) {
		if ($Lang == $LinguaPrincipale)
			$error_msg = "Invio [$RealName] non riuscito.";
		else 
			$error_msg = "Enter [$RealName] failed.";
		echo "<script>\n";
		if ($Lang == $LinguaPrincipale)
			echo "alert('Attenzione, $error_msg');";
		else 
			echo "alert('Caution, $error_msg');";
		echo "history.go(-1);";
		echo "</script>";		
//		clsError::sendWarningAndExit($error_msg);
		return $error_msg;
	}

	if (filesize($new_file_name)<=0) {
		echo "<script>\n";
		if ($Lang == $LinguaPrincipale)
			echo "alert('Attenzione, si sta cercando di caricare un file vuoto [$RealName].');";
		else 
			echo "alert('Attention, you're trying to load an empty file [$RealName].');";
		echo "history.go(-1);";
		echo "</script>";		
		$error_msg = "Errore, il $file � vuoto.";
//		clsError::sendWarningAndExit($error_msg);
		return $error_msg;
	}

	return $error_msg;

}

if (!function_exists('file_get_contents')) {
  function file_get_contents($filename, $use_include_path = 0) {
   $data = '';
   $file = @fopen($filename, "rb", $use_include_path);
   if ($file) {
     while (!feof($file)) $data .= fread($file, 1024);
     fclose($file);
   } else {
     /* There was a problem opening the file. */
     return FALSE;
   }
   return $data;
  }
}

function elencopagine($Id_Articolo)
{
   global $tabsvil;
	$db_corsi = new DB_CedCamCMS;
	$SQLcorsi= "SELECT ".$tabsvil."T_Menu.Id, ".$tabsvil."T_Menu.Titolo, ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Stato_Articolo, ".$tabsvil."TJ_Articoli_X_Box_X_Menu.TipoTesto";
	$SQLcorsi.= " FROM ".$tabsvil."TJ_Articoli_X_Box_X_Menu, ".$tabsvil."T_Menu";
	$SQLcorsi.= " WHERE ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Articolo =".$Id_Articolo;
	$SQLcorsi.= " AND ".$tabsvil."TJ_Articoli_X_Box_X_Menu.Id_Menu = ".$tabsvil."T_Menu.Id";
	$db_corsi->query($SQLcorsi);

	$dati="";
	while($db_corsi->next_record())
	{
		if ($db_corsi->f('Stato_Articolo')=="A") $stato="attivo"; else $stato="sviluppo";
		$dati.= "<a href=\"/index.phtml?Id_VMenu=".$db_corsi->f('Id')."\">".$db_corsi->f('Titolo')."</a> (".$db_corsi->f('TipoTesto')." - ".$stato.")<br />";
	}
		
	return($dati);
}

function checkgroup2($gruppi,$explode)
{
   global $tabsvil;
	$db_gruppi = new DB_CedCamCMS;
	while (strlen($explode)>2 && $gruppi=="|")
	{
		$explode=substr($explode,0,-3);
		$SQLgruppi= "SELECT Gruppi from ".$tabsvil."T_Menu where Id_Menu='".$explode."'";
		$db_gruppi->query($SQLgruppi);
		$db_gruppi->next_record();
		$gruppi=$db_gruppi->f('Gruppi');
   }
   if ($gruppi=="|")
		return(1);
   else
	   return(checkgroup($gruppi));
}

// controlla se utente con permessi 'mygroup' ha diritto a vedere pagine con permessi 'gruppi'
function checkgroup($gruppi)
{
   global $mygroup,$utype,$mastereditor;

//   if ($gruppi=="|" || $utype==1 || $utype==98)
	if ($gruppi=="|" || $utype==1 ||  $mastereditor==1)
		return(1);
	elseif ($mygroup=="|")
		return (0);
	else
	{
		$array_mygroup=explode("|",substr($mygroup,1,-1));
		$array_gruppi=explode("|",substr($gruppi,1,-1));
		$str_gruppi="";
		foreach ($array_gruppi as $key => $value)
			$str_gruppi.="'".$value."',";
		$str_gruppi=substr($str_gruppi,0,-1);
		
		$db_gruppi = new DB_CedCamCMS;
		$SQLgruppi= "SELECT Id,Id_Gruppo from T_Gruppi where Id in (".$str_gruppi.")";
		$db_gruppi->query($SQLgruppi);
		while($db_gruppi->next_record())
		{
			if (!(isset($array_gruppicat[substr($db_gruppi->f('Id_Gruppo'),0,2)])) or $array_gruppicat[substr($db_gruppi->f('Id_Gruppo'),0,2)]=="")
				$array_gruppicat[substr($db_gruppi->f('Id_Gruppo'),0,2)]="|";
			$array_gruppicat[substr($db_gruppi->f('Id_Gruppo'),0,2)].= $db_gruppi->f('Id')."|";
		}

		foreach ($array_gruppicat as $key => $value) {
			
			$array_gruppi2=explode("|",substr($value,1,-1));
			$mygroupabil[$key]=0;
			foreach ($array_gruppi2 as $key2 => $value2)
			{
				if(in_array($value2,$array_mygroup))
					$mygroupabil[$key]=1;
			}
		}
		
		$mygroupabilgen=1;
		foreach ($mygroupabil as $key => $value) {
			if ($value==0)
				$mygroupabilgen=0;
		}
		return($mygroupabilgen);
	}
}


$LinguaPrincipale="IT";
//$AltreLingue=array('EN', 'FR');
$AltreLingue=array('EN','FR');
$LinguaDefault="IT";
$noPredefinita=1;

if (isset($Lang2))
{
	$reg_Lang=$Lang2;
	if(!is_object($sess))
	{
		setcookie("reg_Lang",$reg_Lang,0,"/");
	}
	else
	{
		$sess->register("reg_Lang");
		page_close();
	}
}
if (!(isset($reg_Lang)))
	$Lang=$LinguaDefault;
else
	$Lang=$reg_Lang;


include $PROGETTO."/globals.inc";


?>