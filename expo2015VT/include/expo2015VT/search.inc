<?
function prova($words, $HTTP_POST_VARS, $QueryString)   {
# $Id: search.php,v 1.2 2001/10/12 17:51:28 junkmale Exp $
   #
   # This is a php wrapper for use with htdig (http://www.htdig.org/)
   #
   # Copyright (c) 2001 DVL Software Limited
   # http://www.dvl-software.com/
   #

   # This is basically a BSD License.  I got this from 
   # http://www.FreeBSD.org/copyright/freebsd-license.html
   #
   # Redistribution and use in source and binary forms, with or without 
   # modification, are permitted provided that the following conditions are met: 
   #
   #   1.Redistributions of source code must retain the above copyright notice, 
   #     this list of conditions and the following disclaimer. 
   #   2.Redistributions in binary form must reproduce the above copyright notice, 
   #     this list of conditions and the following disclaimer in the documentation 
   #     and/or other materials provided with the distribution. 

   # Items which are often configurable
   #
   $Debug = 0;  # set to non-zero to display debugging messages

   # change this to the location of the shell script call to htsearch
   #
   $HTSEARCH_PROG = "/usr/lib/cgi-bin/htsearch";
	setlocale(LC_ALL, 'it_IT');

   #
   # end of items which are often configurable

   #
   # these are the variables from the form. populated only
   # if the user clicked on Search
   #
   $Parameters = $HTTP_POST_VARS;
//$Parameters = $ParametriPost;
   #
   # these are the variables within the URL.  These are populated
   # on pages after the first page.  we perform this step to ensure
   # the form is populated with the appropriate values
   #
   //$QueryString = $HTTP_SERVER_VARS["QUERY_STRING"];
   //echo "QueryString - ".$QueryString;
   //exit;
   /*if ($QueryString) {
      $ArrayParm  = ConvertQueryStringToArray($QueryString, ";");*/

      #
      # these are the fields which the user fills in
      #
      //$method   = $ArrayParm["method"];
      $method = 'and';
      //$format   = $ArrayParm["format"];
      $format   = "short";
      //$sort     = $ArrayParm["sort"];
      $sort     = "Id_Prod";
      //$words    = urldecode($ArrayParm["words"]);
      //$pagina     = $ArrayParm["pagina"];
	  $pagina	= "search";
      # these fields are hidden and therefore don't need to be populated
      # but are provided for completeness

      //$config   = $ArrayParm["config"];
      $config   = 'siexpo2015';
      #$restrict = $ArrayParm["restrict"];
      #$exclude  = $ArrayParm["exclude"];
      #$submit   = $ArrayParm["submit"];
      #$page     = $ArrayParm["page"];
  /* } */ 

   #
   # don't let the bastards include HTML/PHP in their search strings
   # as a test case, include something like this "<img src=http://199.125.85.46/time.jpg>"
   # in the search field.  If you see the image in the results, the test has failed
   # The function strip_tags will strip all HTML and PHP tags.  With the above test case
   # you wind up with an empty string.  No big deal.  But a better solution might
   # return the original value to the search screen.  The current solution does not.
   #
   # Dan Langille 2001.10.13
   //echo "Parameters - ".$Parameters;
   //echo "words - ".$words;
   $words = strip_tags($words);

?>
<?
/*
if (!$Parameters && !strlen($HTTP_SERVER_VARS["QUERY_STRING"])) {
?>
<pre>
#
# htdig php wrapper 1.0
#
# Copyright (c) 2001 DVL Software Limited
# http://www.dvl-software.com/
#
# this source code can be obtained from:
#      http://freebsddiary.org/samples/htdig-php-wrapper.tar.gz
#
# If you have any trouble with htdig, try the htdig mailing lists at
# http://www.htdig.org/
#
# If you have any suggestions for improvments, bug reports, etc,
# please send patches to me.  Thanks.
#
# I'd also like to hear about where it's being used.  That's just me
# being curious, that's all.
#
# dan@langille.org
#
</pre>
<?
}
*/
//   require("search-form.php");

function ConvertQueryStringToArray($QueryString, $Delimiter) {
   # this function takes a string which contains parameters.  The parameters are delimited
   # by $Delimiter.  It splits these parameters up into an array.  It then takes the key/value
   # pairs of this array and puts it into an associative array.
   #
   # for example, if the input is: $QueryString = "size=10;colour=L;fabric=cotton"
   #                               $Delimiter   = ";"
   #
   # then the output will be this array:
   #
   #   array (
   #       "size"   => "10",
   #       "colour" => "L",
   #       "fabric" => "cotton"
   #   );
   #
   # It is assumed that the string is of the format: "key1=value1<*>key2=value2<*>key3=value3..."
   # where <*> is $Delimiter.
   #
   # This function returns an empty variable if no parameters are found.

#   echo "ConvertParametersToArray: QueryString = '$QueryString' with length " . strlen($QueryString) . "<br>\n";

   #
   # if there's nothing to do, do nothing.
   #
   if (strlen($QueryString)) {

      #
      # split the query string into an array.
      #
      $SimpleArray = explode($Delimiter, $QueryString);

      #
      # taken each element of the array, which will have
      # elements 0..n where each element is of the form
      # "keyn"="valuen" and split them into "keyn" and "valuen"
      #
      while (list($key, $value) = each($SimpleArray)) {
         list($KeyN, $ValueN) = split("=", $value);
//         echo "key=$KeyN"."---".$ValueN;
         #
         # put that key/value pair into the result we are going to pass back
         #
         $Result[$KeyN] = $ValueN;
      }
   } else {
      echo "nothing found<br>\n";
   }

   return $Result;
}

function CompileQuery($words) {

   //$query = '';

   /*while (list($name, $value) = each($pippo)) {
      $query = $query . "$name=$value;";
   }*/
   //$query = "-matchesperpage=1000;words=prova;pagina=search;method=and;format=short;sort=score;config=lodi;exclude=;";
   //$query = "pagina=search;method=and;format=short;sort=score;config=siexpo2015;exclude=;submit=;matchesperpage=1000;words=".$words;
   $query = "words=".$words.";pagina=search;method=or;format=short;sort=score;config=siexpo2015;exclude=;submit=;matchesperpage=1000";
   //echo $query;
   //exit;
   # remove the trailing ;
   $query = substr($query, 0, strlen($query) - 1);
  
   return $query;
}

#
# if the user clicked on Search or we have a query string
#
//if ($Parameters || strlen($HTTP_SERVER_VARS["QUERY_STRING"])) {
if ($words) {
	//echo "Query -".$HTTP_SERVER_VARS["QUERY_STRING"];
   //if (count($Parameters)) {
      //echo "Num Param - ".count($Parameters);
		//$query = CompileQuery($Parameters);
	$query = CompileQuery($words);
   //} else {
      //$query = $HTTP_SERVER_VARS["QUERY_STRING"];
   //}
   #
   # this code courtesy of an article by Colin Viebrock [colin@easyDNS.com]
   # at http://www.devshed.com/Server_Side/PHP/search/
   # which formed the basis of this work
   #
   #
   # execute the htsearch code
   #
   $command="$HTSEARCH_PROG  -c /internet/htdig/conf/siexpo2015.conf \"$query\"";
   exec($command,$result);
   //echo "Command -".$command;
   # debug: look at the output.  useful for seeing what is where
   if ($Debug) {
      while (list($k,$v) = each($result)) {
         echo "$k -> $v \n<br \>";
      }
   }

   # how many rows do we have?
   $rc = count($result);
	$db_htdig = new DB_CedCamCMS;

   # all these magic numbers have got to go
   //echo "N Risultati".$rc;
   
   if ($rc < 3) {
      //echo "There was an error executing this query.  Please try later.\n";
   } else {
      if ($result[2]=="NOMATCH") {
         //echo "There were no matches for <b>$words</b> found on the website.<p>\n";
      } else {
         if ($result[2]=="SYNTAXERROR") {
            //echo "There is a syntax error in your search for <b>$search</b>:<br \>";
            //echo "<pre>" . $result[3] . "</pre>\n";
         } else {
            #
            # display the headers, this includes the forum, the search
            # parameters, etc.
            #

            /*$ResultSetStart = 1;
            for ($i = $ResultSetStart; $i < $rc; $i++) {
            	$result[$i]=str_replace("* * ", "", $result[$i]);
            	if (substr($result[$i],0,30)=="Camera di Commercio di Lodi - ")
	               echo substr($result[$i],30);
					else
	               echo $result[$i];
            }*/
            
            $ResultSetStart = 1;
            $y=0;
            for ($i = $ResultSetStart; $i < $rc; $i++) {
				//echo $result[$i];
				$isLink = strpos($result[$i],"http://www.siexpo2015.it/ShowProd.phtml?Id_Prod=");
				//echo "isLink - ".$isLink." Len: ".strlen($isLink);
				//echo $result[$i];
				if (strlen($isLink) > 0){
					//echo $result[$i];
					//Elabora i result e ricava l'ID Prodotto.
					$posIDProd = strpos($result[$i],"Id_Prod=");
					$posAnd = strpos($result[$i],"&");
					//echo "posIDProd: ".$posIDProd." posAnd: ".$posAnd;
					$IDProd=substr($result[$i],$posIDProd+8,$posAnd-($posIDProd+8));
					//echo "IDProd -".$IDProd;
					$y++;
					$risultati[$y]=$IDProd;
					//echo "ris -".$risultati[$y];

				}
            	//$result[$i]=str_replace("* * ", "", $result[$i]);
            	/*if (substr($result[$i],0,30)=="Camera di Commercio di Lodi - ")
            		echo substr($result[$i],30);
            	else
            		echo $result[$i];
            	*/
            }
         }
      }
   }
}
/*
if ($HTTP_SERVER_VARS["QUERY_STRING"]!="")
	$urlsearch=$HTTP_SERVER_VARS["QUERY_STRING"];
else
	$urlsearch='exclude=;config=crotone;method=and;format=long;sort=score;matchesperpage=10;words='.$words;

readfile ("http://testconsumatori.cedcamera.com/search.php3?".$urlsearch);

*/
//exit;
return $risultati;
}
?>