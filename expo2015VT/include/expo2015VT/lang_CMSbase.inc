<?
$Chiave["T_Menu"]="Id";
$Permessi["T_Menu"]=";1;2;";
$Azioni["T_Menu"]["1"]="|Elimina";
$Azioni["T_Menu"]["2"]="|Elimina";
$Filtri["T_Menu"]["Requisito minimo"]="||||Tlk_Priorita|Id_Priorita|Id|Descrizione|Id";
$FiltriText["T_Menu"]["Menu"]="||Id_Menu|20|100|TEXT";
$FiltriText["T_Menu"]["Titolo"]="||Titolo|20|100|TEXT";
$Elimina["T_Menu"]="T_Box;Id_Menu2|T_Menu_AL;Id_AL";
$Location["T_Menu"]["ELM"]["default"] = "/index.phtml?Id_VMenu=40";
$Location["T_Menu"]["default"]["default"] = "/index.phtml?Id_VMenu=31";
$T_Menu["Id|||"] = "Id||I";
$T_Menu["Titolo|||"] = "Titolo||S|S||||TEXT|20|100";
$T_Menu["Id_Menu|;1;||"] = "Menu||S|S||||TEXT|10|50";
$T_Menu["Id_Menu|;2;||"] = "Menu||S|S||DIS||TEXT|10|50";
$T_Menu["Tipo|||"] = "Tipo||S|S||||SELECTREFRESH|Tlk_TipoPagina;Id;Descrizione|1";
$T_Menu["Id_Sottomenu|||"]="Ha Sottomen�?||N|N||\$Tipo=='M'||RADIO|Si;No|1;0|0|0";
$T_Menu["Esploso|||"]="Esploso?||N|N||\$Tipo=='M'||RADIO|Si;No|S;N|N|N";
$T_Menu["Id_Priorita|||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
$T_Menu["TipiUtente|||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
$T_Menu["Stato_Menu|||"] = "Stato||S|S||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$T_Menu["Mtop|||"] = "Menu iniziale||S|S||\$Tipo!='L'||TEXT|10|50";
$T_Menu["Descrizione|||"] = "Descrizione||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$T_Menu["Keywords|||"] = "Keywords||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$T_Menu["Popup|||"] = "Pop-up||N|N||\$Tipo!='L'||HTMLAREA|3|40";
$T_Menu["Id_Pagina|;1;||"] = "Template||S|N||\$Tipo!='L'||SELECTFILTRO|T_Pagine;Id;Descrizione";
$T_Menu["Id_Pagina|;2;3;||"] = "Template||S|N||\$Tipo!='L'||SELECTFILTRO|T_Pagine;Id;Descrizione||Flag_Pubblico||1";
$T_Menu["Id_Pagina||||"] = "Template||N|N||\$Tipo=='L'||HIDDEN|TEMPLATE_LABEL";
$T_Menu["MenuTemplate|||"] = "Template Menu||N|N||||TEXT|10|20";
$T_Menu["LinkEsterno|||"] = "Link esterno||N|N||\$Tipo!='L'||TEXT|30|100";
$T_Menu["Testo_Html||UPD|"] = "Box di contenuti||N|N||\$Tipo!='L'||ELENCO|T_Box|Id_Menu2|Id|T_Box.Ordine|Id_Box";
$T_Menu["IPFilter|||"]="Filtro IP?||N|N||||RADIO|Si;No|1;0|0|0";
$T_Menu["Id_Ufficio|||"] = "Ufficio||S|S||||SELECTFILTRO|T_Uffici;Id_Ufficio;Ufficio|Id|";

$T_Menu["Data_Modifica|||"] = "||N|N||||DATA|";
$T_Menu["Data_Modifica||UPD|"] = "Data Modifica||N|N||DIS||TEXT|10|10";
$T_Menu["Id_Modifica||UPD|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|Id_Modifica|";
$T_Menu["Id_Modifica||UPD||"] = "Proprietario||N|N||||HIDDEN|uid|";
$T_Menu["Id_Modifica||INS|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|uid|";
$T_Menu["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Menu_AL"]="Id_AL";
$Permessi["T_Menu_AL"]=";1;2;";
$Inserimento["T_Menu_AL"]["1"]="N";
$Inserimento["T_Menu_AL"]["2"]="N";
$Azioni["T_Menu_AL"]["1"]="|Modifica";
$Azioni["T_Menu_AL"]["2"]="|Modifica";
$Location["T_Menu_AL"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Menu&amp;azione=UPD&amp;Id=$Id_AL";
$T_Menu_AL["Id_AL|||"] = "Id||H";
$T_Menu_AL["Lang_AL|||"] = "Lingua||S|S||DIS||TEXT|2|2";
$T_Menu_AL["Titolo_AL|||"] = "Titolo||S|S||||TEXT|20|100";
$T_Menu_AL["Descrizione_AL|||"] = "Descrizione||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$T_Menu_AL["Keywords_AL|||"] = "Keywords||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$T_Menu_AL["Popup_AL|||"] = "Pop-up||N|N||\$Tipo!='L'||HTMLAREA|3|40";
$T_Menu_AL["LinkEsterno_AL|||"] = "Link esterno||N|N||\$Tipo!='L'||TEXT|30|100";
$T_Menu_AL["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Box"]="Id_Box";
$Permessi["T_Box"]=";1;2;";
$Azioni["T_Box"]["1"]="Id_Menu2,Id_Menu2;Id_Box,Id_Box|Modifica|box_del|box_up|box_down";
$Azioni["T_Box"]["2"]="Id_Menu2,Id_Menu2;Id_Box,Id_Box|Modifica|box_del|box_up|box_down";
$Location["T_Box"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Menu&amp;azione=UPD&amp;Id=$Id";
$T_Box["Id_Box|||"] = "Id_Box||H";
$T_Box["Id_Menu2|||"] = "Titolo Menu||N|N||||SELECTHIDDEN|T_Menu;Id;Titolo|Id|Id_Menu2|";
$T_Box["Id_Box||||"] = "Id Box||N|N||DIS||TEXT|5|5|";
$T_Box["Ordine|||"] = "Ordine||S|N||DIS||TEXT|4|4";
$T_Box["Stato_Box|||"] = "Stato||S|N||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$T_Box["Pagine||UPD|"] = "Articoli||N|N|TJ_Articoli_X_Box_X_Menu|||ELENCO|TJ_Articoli_X_Box_X_Menu|Id_Box;Id_Menu|Id_Box;Id_Menu2|TJ_Articoli_X_Box_X_Menu.Ordine2|Id_Articolo";
$T_Box["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["TJ_Articoli_X_Box_X_Menu"]="Id_Articolo;Id_Box;Id_Menu";
$Permessi["TJ_Articoli_X_Box_X_Menu"]=";1;2;";
$Azioni["TJ_Articoli_X_Box_X_Menu"]["1"]="|Modifica|Articolo|Elimina";
$Azioni["TJ_Articoli_X_Box_X_Menu"]["2"]="|Modifica|Articolo|Elimina";
$Location["TJ_Articoli_X_Box_X_Menu"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Box&amp;azione=UPD&amp;Id_Box=$Id_Box&amp;Id_Menu2=$Id_Menu";
$TJ_Articoli_X_Box_X_Menu["Label1|||"]="Gestione Articolo||N|N||||LABEL|CENTER";
$TJ_Articoli_X_Box_X_Menu["Id_Menu|||"] = "Titolo Menu||N|N||||SELECTHIDDEN|T_Menu;Id;Titolo|Id|Id_Menu|";
$TJ_Articoli_X_Box_X_Menu["Id_Box|||"] = "Id_Box||N|N||||HIDDEN";
$TJ_Articoli_X_Box_X_Menu["Id_Box||||"] = "Id Box||N|N||DIS||TEXT|5|5|";

$TJ_Articoli_X_Box_X_Menu["Id_Articolo||UPD|"] = "Descrizione||S|N||||SELECTHIDDEN|T_Articoli;Id_Articolo;Descrizione|Id_Articolo|Id_Articolo";
$TJ_Articoli_X_Box_X_Menu["Id_TipoArticolo||INS|"] = "Categoria||N|S||||SELECTREFRESH|Tlk_TipoArticolo1;Id;Descrizione|1||||N";
$TJ_Articoli_X_Box_X_Menu["Id_Articolo||INS|"] = "Descrizione||N|S||||SELECTFILTRO|T_Articoli;Id_Articolo;Descrizione|1|Id_TipoArticolo1|Id_TipoArticolo";

$TJ_Articoli_X_Box_X_Menu["TipoTesto|||"]="Abstract||S|N||||RADIO|Si;No|Abstract;Html|0|Html";
$TJ_Articoli_X_Box_X_Menu["Ordine2|||"] = "Ordine||S|S||||TEXT|4|4";
$TJ_Articoli_X_Box_X_Menu["Stato_Articolo|||"] = "Stato||S|N||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$TJ_Articoli_X_Box_X_Menu["Id_TipoHtml|||"] = "Tipo||N|S||||SELECTFILTRO|Tlk_TipiHtml;Id;Descrizione|1";
$TJ_Articoli_X_Box_X_Menu["Id_Priorita|||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
$TJ_Articoli_X_Box_X_Menu["TipiUtente|||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
$TJ_Articoli_X_Box_X_Menu["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Articoli"]="Id_Articolo";
$Permessi["T_Articoli"]=";1;2;3;";
$Azioni["T_Articoli"]["1"]="|Elimina";
$Azioni["T_Articoli"]["2"]="|Elimina";
$Azioni["T_Articoli"]["3"]="|";
$Filtri["T_Articoli"]["Categoria"]="||||Tlk_TipoArticolo1|Id_TipoArticolo1|Id|Descrizione|Id";
$Filtri["T_Articoli"]["Tipo Articolo"]="||||Tlk_TipoArticolo3|Id_TipoArticolo3|Id|Descrizione|Id";
$Location["T_Articoli"]["default"]["default"] = "/index.phtml?tmpl=2&Id_VMenu=32&pagina=elenchi&nome=T_Articoli";
$T_Articoli["Id_Articolo|||"] = "Id||I";
$T_Articoli["Id_TipoArticolo1|||"] = "Categoria pricipale||S|S||||SELECTFILTRO|Tlk_TipoArticolo1;Id;Descrizione|1";
$T_Articoli["Id_TipoArticolo3|||"] = "Tipo articolo||S|S||||SELECTFILTRO|Tlk_TipoArticolo3;Id;Descrizione|1";
$T_Articoli["Descrizione|||"] = "Descrizione||S|S||||TEXT|40|100";
$T_Articoli["Titolo_Html|||"] = "Titolo||N|S||||TEXT|40|100";
$T_Articoli["Testo_Html|||"] = "Html||N|S||||HTMLAREA|10|60";
$T_Articoli["Link_Html|||"] = "Link||N|N||||TEXT|40|100";
$T_Articoli["Titolo_Abstract|||"] = "Titolo Abstract||N|N||||TEXT|40|100";
$T_Articoli["Testo_Abstract|||"] = "Html Abstract||N|N||||HTMLAREA|10|60";
$T_Articoli["Link_Abstract|||"] = "Link Abstract||N|N||||TEXT|40|100";
$T_Articoli["Data_Scadenza|||"] = "Data Scadenza (aaaa-mm-gg)||N|N||||TEXT|20|20";
$T_Articoli["Data_Modifica|||"] = "||N|N||||DATA|";
$T_Articoli["Data_Modifica||UPD|"] = "Data Modifica||N|N||DIS||TEXT|20|20";
$T_Articoli["Id_Modifica||UPD|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|Id_Modifica|";
$T_Articoli["Id_Modifica||UPD||"] = "Proprietario||N|N||||HIDDEN|uid|";
$T_Articoli["Id_Modifica||INS|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|uid|";
$T_Articoli["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Articoli_Svil"]="Id_Svil";
$Permessi["T_Articoli_Svil"]=";1;2;";
$Inserimento["T_Articoli_Svil"]["1"]="N";
$Inserimento["T_Articoli_Svil"]["2"]="N";
$Azioni["T_Articoli_Svil"]["1"]="|Modifica";
$Azioni["T_Articoli_Svil"]["2"]="|Modifica";
$Location["T_Articoli_Svil"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Articoli&amp;azione=UPD&amp;Id_Articolo=$Id_Articolo_Svil";
$T_Articoli_Svil["Id_Svil|||"] = "Id||H";
$T_Articoli_Svil["Id_Articolo_Svil|||"] = "Id Articolo||S|S||DIS||TEXT|2|2";
$T_Articoli_Svil["Titolo_Html|||"] = "Titolo||S|S||||TEXT|40|100";
$T_Articoli_Svil["Testo_Html|||"] = "Html||N|N||||HTMLAREA|10|60";
$T_Articoli_Svil["Link_Html|||"] = "Link||N|N||||TEXT|40|100";
$T_Articoli_Svil["Titolo_Abstract|||"] = "Titolo Abstract||N|N||||TEXT|40|100";
$T_Articoli_Svil["Testo_Abstract|||"] = "Html Abstract||N|N||||HTMLAREA|10|60";
$T_Articoli_Svil["Link_Abstract|||"] = "Link Abstract||N|N||||TEXT|40|100";
$T_Articoli_Svil["Data_Modifica|||"] = "||N|N||||DATA|";
$T_Articoli_Svil["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Articoli_AL"]="Id_AL";
$Permessi["T_Articoli_AL"]=";1;2;";
$Inserimento["T_Articoli_AL"]["1"]="N";
$Inserimento["T_Articoli_AL"]["2"]="N";
$Azioni["T_Articoli_AL"]["1"]="|Modifica";
$Azioni["T_Articoli_AL"]["2"]="|Modifica";
$Location["T_Articoli_AL"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=T_Articoli&amp;azione=UPD&amp;Id_Articolo=$Id_AL";
$T_Articoli_AL["Id_AL|||"] = "Id||H";
$T_Articoli_AL["Lang_AL|||"] = "Lingua||S|S||DIS||TEXT|2|2";
$T_Articoli_AL["Titolo_Html_AL|||"] = "Titolo||S|S||||TEXT|40|100";
$T_Articoli_AL["Testo_Html_AL|||"] = "Html||N|N||||HTMLAREA|10|60";
$T_Articoli_AL["Link_Html_AL|||"] = "Link||N|N||||TEXT|40|100";
$T_Articoli_AL["Titolo_Abstract_AL|||"] = "Titolo Abstract||N|N||||TEXT|40|100";
$T_Articoli_AL["Testo_Abstract_AL|||"] = "Html Abstract||N|N||||HTMLAREA|10|60";
$T_Articoli_AL["Link_Abstract_AL|||"] = "Link Abstract||N|N||||TEXT|40|100";
$T_Articoli_AL["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

?>