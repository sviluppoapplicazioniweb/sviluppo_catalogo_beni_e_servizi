<?
function print_row_gen($maxlevs,$ID,$key,  $values, $equal, $levels, $actlevel, $actlevel_prec)
{
	global $PHP_SELF, $PATHDOCS, $anchorstring, $menustring, $force_menu, $tipomappa, $Id_VMenu, $db_menu, $chiave_tab, $tab_nome, $mastereditor, $utype, $area;
	reset($force_menu);
	while(list($chiave, $valore)=each($force_menu))
	{
		switch($chiave)
		{
			case 'folder-chiuso':
				$gif_tree[0]="$valore";
				break;
			case 'folder-aperto':
				$gif_tree[1]="$valore";
				break;
			case 'documento-chiuso':
				$gif_page[0]="$valore";
				break;
			case 'documento-aperto':
				$gif_page[1]="$valore";
				break;
			case 'folder-mappa-chiuso':
				$gif_tree1[0]="$valore";
				break;
			case 'folder-mappa-aperto':
				$gif_tree1[1]="$valore";
				break;
			case 'documento-mappa-chiuso':
				$gif_page1[0]="$valore";
				break;
			case 'documento-mappa-aperto':
				$gif_page1[1]="$valore";
				break;
			case 'folder-mappa-admin-chiuso':
				$gif_tree2[0]="$valore";
				break;
			case 'folder-mappa-admin-aperto':
				$gif_tree2[1]="$valore";
				break;
			case 'documento-mappa-admin-chiuso':
				$gif_page2[0]="$valore";
				break;
			case 'documento-mappa-admin-aperto':
				$gif_page2[1]="$valore";
				break;
			case 'gif-path':
				$gif_path="$valore";
				break;
			case 'gif-size':
				$gif_size="$valore";
				break;
			case 'gif-size2':
				$gif_size2="$valore";
				break;
			case 'gif-size-right':
				$gif_size_right="$valore";
				break;
			case 'gif-size-mappa':
				$gif_size_mappa="$valore";
				break;
			case 'shim-size':
				$shim_size="$valore";
				break;
			case 'table-size':
				$table_size="$valore";
				break;
			case 'label-color':
				$label_color="$valore";
				break;
			case 'label-bgcolor':
				$label_bgcolor="$valore";
				break;
			case 'tipo-menu':
				$tipo_menu="$valore";
				break;
		}
	}

	list($pagina, $vocedimenu)=each($values);
	$target=$values[target];
	$sub=$values[sub];
	$stato=$values[stato];
	$flagmod=$values[flagmod];
	$flag_struct=$values[flag_struct];
	$idmod=$values[idmod];
	
/*	
	if ($tipo_menu=="mappa" && $tipomappa=="admin")
	{
		$SQL1="select Login from T_Anagrafica where Id=".$idmod;
		$db_menu->query($SQL1);
		$db_menu->next_record();
		$codmod=$db_menu->f('Login');
	}

*/
	$tipo=$values[tipo];
	$esploso=$values[esploso];
	$primo=$values[primo];
	$mtop=$values[mtop];
//	if ($tipomappa=="admin")
		$idadmin=$values[Id_VMenu];
	
	$keyspazio=$key;
	$albero=explode(".",$key);
	$ramo=$albero[0];

	$classe=$tipo_menu.$actlevel;
	if ($tipo_menu!="L")
	{
		switch ($stato)
		{
			case "A":
				if ($equal)
					$classe.="sel";
				break;
			case "D":
			case "S":
				$classe.="dis";
				break;
		}
	}

	$linkstruct="";
/*	if($tipo_menu=="mappa" && $tipomappa=="admin" && strlen($key)==2)
	{
		$SQL1="select Flag_struttura from ".$tabsvil."T_Menu where Id_Menu like '".$key."%' and Flag_Struttura=3";
		$db_menu->query($SQL1);
		$db_menu->next_record();
		if ($db_menu->num_rows()>0)
			$linkstruct="&nbsp;(<a href=\"/index.phtml?Id_VMenu=68&amp;struct=".$key."\" alt=\"Approva struttura\">Approva struttura</a>)";
	}

	$SQL1="select MenuTemplate from ".$tabsvil."T_Menu where Id=".$idadmin;
	$db_menu->query($SQL1);
	$db_menu->next_record();
	$template2=$db_menu->f('MenuTemplate').".html";
*/	
//	if (!(file_exists($PATHDOCS."/tmpl/".$tipo_menu."/".$template2)))
		$template2=".html";


	if ($tipo=='L')
	{
		if ($template2==".html")
			if (file_exists($PATHDOCS."/tmpl/".$tipo_menu."/".$tipo."_".$actlevel.".html"))
				$template2=$tipo."_".$actlevel.".html";
			else
				$template2=$tipo."_"."default.html";
//		$colonne=$maxlevs+1;
	}
	else
	{
		if ($template2==".html")
			if (file_exists($PATHDOCS."/tmpl/".$tipo_menu."/".$tipo."_".$actlevel."_".$equal.".html"))
				$template2=$tipo."_".$actlevel."_".$equal.".html";
			elseif (file_exists($PATHDOCS."/tmpl/".$tipo_menu."/".$tipo."_".$actlevel."_"."default".".html"))
				$template2=$tipo."_".$actlevel."_"."default".".html";
			elseif (file_exists($PATHDOCS."/tmpl/".$tipo_menu."/".$tipo."_"."default"."_".$equal.".html"))
				$template2=$tipo."_"."default"."_".$equal.".html";
			else
				$template2=$tipo."_"."default"."_"."default".".html";

		if ($tipo_menu=="menu")
		{
			if($sub && $actlevel==1) //folder
				$gif=$gif_path.$gif_tree[$equal];
			else	// pagina
				$gif=$gif_path.$gif_page[$equal];
		}
		elseif ($tipomappa!="admin")
		{
			if($sub) //folder
				$gif=$gif_path.$gif_tree1[$equal];
			else	// pagina
				$gif=$gif_path.$gif_page1[$equal];
		}
		else
		{
/*
			if($sub) //folder
				$gif=$gif_path.$gif_tree2[$equal];
			else	// pagina
				$gif=$gif_path.$gif_page2[$equal];
*/
			if($tipo=="M") //folder
				$gif=$gif_path.$gif_tree2[$equal];
			else	// pagina
				$gif=$gif_path.$gif_page2[$equal];
		}
	}

	$htmlent=htmlentities($vocedimenu);
	if ($tipo=="L")
		$vocedimenu=$htmlent;
	else
	{
		//$vocedimenu="<a href=\"$pagina\" class=\"".$classe."\" target=".$target.">".htmlentities($vocedimenu)."</a>";
//		if ($key=="01")
//			$vocedimenu=$htmlent;
//		else
//			$vocedimenu="<a href=\"$pagina\">".$htmlent."</a>";
			
		$vocedimenu.=$linkstruct;
		if($tipo_menu=="mappa" && $tipomappa=="admin")
			$vocedimenu="<a id=\"idmenu".$idadmin."\"></a>".$vocedimenu;
//		if (strlen($key)==2 && $key!="01")
//			$anchorstring.="<a href=\"#idmenu".$idadmin."\">".$htmlent."</a><br />\n";
		if (strlen($key)==2)
			$anchorstring.="<a href=\"/index.phtml?Id_VMenu=".$Id_VMenu."&amp;area=".$idadmin."\"><!--htdig_noindex-->".$htmlent."<!--/htdig_noindex--></a><br />\n";
	}

	if($tipo_menu=="mappa" && $tipomappa!="admin")
		$vocedimenu="&nbsp;&nbsp;&nbsp;".$vocedimenu;
	
//27102005
//	if ($gif_size==0 && $tipo_menu=="menu") $colonne=1;

if ($tipomappa=="admin" && ($area!="" || strlen($key)==2))
{

	$vocediadmin="";
	$vocediadmin2="";
	$shim_admin="<img src=\"/images/shim.gif\" width=\"16\" height=\"16\" alt=''/>&nbsp;";

	//Inserisci prima
	if ($key!="00" && $key!="01")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=prima&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Inserisci prima\"><img src=\"/images/admin/up-ins.gif\" alt='Inserisci prima' /></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Inserisci dopo
	if ($key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=dopo&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Inserisci dopo\"><img src=\"/images/admin/down-ins.gif\" alt='Inserisci dopo' /></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Inserisci sottopagina
	if ($key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=sotto&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Inserisci sottopagina\"><img src=\"/images/admin/r-down.gif\" alt='Inserisci sottopagina' /></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Porta dentro
	if (substr($key,-2)>"01" && $key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=declassa&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Porta dentro\"><img src=\"/images/admin/up-dec.gif\" alt='Porta dentro' /></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Porta fuori
	if (strlen($key)>2 && $key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=promuovi&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Porta fuori\"><img src=\"/images/admin/l-pro.gif\" alt='Porta fuori'/></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Sposta in alto
	if (substr($key,-2)>"01" && $key!="01" && $key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=alto&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',0,0)\" title=\"Sposta in alto\"><img src=\"/images/admin/up.gif\" alt='Sposta in alto'/></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	//Sposta in basso
	if (substr($key,-2)<"79" && $key!="01" && $key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=insert_mappagen&amp;tipoins=basso&amp;Id_VMenu=".$Id_VMenu."&amp;par1=".$key."&amp;".$chiave_tab."=".$idadmin."','CMS',0,0)\" title=\"Sposta in basso\"><img src=\"/images/admin/down.gif\" alt='Sposta in basso'/></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	// Modifica pagina
	if ($key!="00")
		$vocediadmin.="<a href=\"javascript:popup_admin('/popup_admin.phtml?tmpl=2&amp;pagina=form&amp;nome=".$tab_nome."&amp;azione=UPD&amp;".$chiave_tab."=".$idadmin."','CMS',640,480)\" title=\"Modifica questa pagina\"><img src=\"/images/admin/pagina.gif\" alt='Modifica questa pagina'/></a>&nbsp;";
	else
		$vocediadmin.=$shim_admin;

	// Elimina pagina
	if ($key!="01" && $key!="00")
		//Personalizzazione per SIEXPO
		
		if ($tab_nome=="SIEXPO_Tlk_Categorie"){
			if ($utype<2){
				$vocediadmin.="<a href=\"/admin/azioni.phtml?nome=".$tab_nome."&amp;Id_VMenu=".$Id_VMenu."&amp;ordina=".$chiave_tab."=&amp;".$chiave_tab."=".$idadmin."&amp;op_esegui=Elimina&amp;azione=ELM&amp;par1=".$key."\" title=\"Elimina questa pagina\" onclick=\"if (!confirm('Verranno cancellate anche tutte le sottopagine. Confermi l\'operazione?')) return false\"><img src=\"/images/admin/trash.gif\" alt='Elimina questa pagina'/></a>&nbsp;";
			}
		} else {
			$vocediadmin.="<a href=\"/admin/azioni.phtml?nome=".$tab_nome."&amp;Id_VMenu=".$Id_VMenu."&amp;ordina=".$chiave_tab."=&amp;".$chiave_tab."=".$idadmin."&amp;op_esegui=Elimina&amp;azione=ELM&amp;par1=".$key."\" title=\"Elimina questa pagina\" onclick=\"if (!confirm('Verranno cancellate anche tutte le sottopagine. Confermi l\'operazione?')) return false\"><img src=\"/images/admin/trash.gif\" alt='Elimina questa pagina'/></a>&nbsp;";
		}
	else
		$vocediadmin.=$shim_admin;
}

if (($tipomappa=="admin" && ($area!="" || strlen($key)==2)) || $tipo_menu=="menu" || ($tipo_menu=="mappa" && $tipomappa!="admin"))
{
	$codice_html=file_get_contents($PATHDOCS."/tmpl/".$tipo_menu."/".$template2);
//$menustring.="<!-- $SQL1 template2 $template2 -->\n";
	$menustring.= preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);
}
}

function show_nav_gen($ID)
{
	global $pages, $PHP_SELF, $tipomappa, $leftmenu, $tab_nome, $chiave_tab;
	$sections = explode(".", $ID);
	$levels=count($sections);
	reset($pages);
	$maxlevs=$levels;
	$maxlevs1==0;
	while(list($key, $values)=each($pages))
	{
		$key=substr($key,1);
		if("$key"=="$ID")
		{
			if($values[sub])
				$maxlevs++;
		}
		if ($values[esploso])
		{
			$maxlevs2= count(explode(".",$key))+1;
			if ($maxlevs2>$maxlevs1)
				$maxlevs1=$maxlevs2;
		}
	}
	if ($maxlevs1>$maxlevs)
		$maxlevs=$maxlevs1;

	$levpg_prec=1;
	reset($pages);
	while(list($key, $values)=each($pages))
	{
		$key=substr($key,1);
		$pgsects=explode(".", $key);
		$levpg=count($pgsects);
		$maxeq=row_is_equal_gen($sections, $pgsects, $levpg);
		if($levpg==1)	//primo livello sempre
		{
//			if($maxeq) //per albero del ramo selezionato non cliccabile
			if($maxeq==count($sections) || $maxeq==1)
			{
				print_row_gen($maxlevs,$ID,$key, $values, 1, $levels, $levpg, $levpg_prec);
				$levpg_prec=$levpg;
			}
			else
			{
				print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
				$levpg_prec=$levpg;
			}
		}
		else
		{
			if($levpg > $levels)
			{
				if($maxeq == ($levpg-1))	// ramo da esplodere
				{
					print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
				elseif ($pages["M".substr($key,0,-3)][esploso] =="S")
				{
					print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
//Inzio modifica
				else if ($tipomappa=="admin" && $leftmenu!="S")
				{
					print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
// Fine Modifica per far stampare sottopagine homepage.
// echo "<!-- -1-".$key."-".$levpg."-".$levels."-".$maxeq."-->";
// ritorna -3-00.02-2-1-0
			}
			else
			{
				if($maxeq == $levpg)
				{
					print_row_gen($maxlevs,$ID,$key, $values, 1, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
				elseif(($levpg -1) == $maxeq)
				{
					print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
				elseif ($pages["M".substr($key,0,-3)][esploso] =="S")
				{
					print_row_gen($maxlevs,$ID,$key, $values, 0, $levels, $levpg, $levpg_prec);
					$levpg_prec=$levpg;
				}
			}
		}
	}
}

function row_is_equal_gen($sections, $pagessections, $actuallevel)
{
	for($i=0; $i<$actuallevel && $i < count($sections); $i++)
	{
		if($sections[$i] != $pagessections[$i])
			break;
	}
	return($i);
}
?>