<p class="center"><b>Informativa ai sensi dell’art. 13 del DLGS 196/03</b></p>
<div id="label"><p><em>
			
I dati personali da Lei forniti a questa Camera saranno oggetto di trattamento a mezzo di sistemi informatici nel pieno 
rispetto delle norme in materia di protezione dei dati personali (Dlgs 196/03).<br />
La finalità del trattamento dei Suoi dati è quella di permetterLe di esercitare il diritto di accesso agli atti 
attraverso la compilazione della domanda on-line e la ricezione, per mezzo di posta elettronica, dei documenti richiesti 
e visionabili ai sensi della legge 241/90 e sue modificazioni e del Regolamento per la disciplina del diritto di accesso 
agli atti della <a href="http://www.mi.camcom.it/upload/file/1449/724664/FILENAME/Reg_diritto_accesso_ultimo.pdf" title="consulta documento">Camera di Commercio di Milano</a><br />
Il conferimento è facoltativo, ma in caso di rifiuto non sarà possibile erogare il servizio. <br />
I dati forniti non saranno comunicati e diffusi a terzi ma possono essere conosciuti dai dipendenti camerali dell’Ufficio competente della gestione del procedimento.
Potrà aggiornare, modificare, cancellare i dati conferiti esercitando i diritti di cui all’art. 7 del dlgs 196/03. 
Titolare dei dati è la CCIAA di Milano. L’elenco completo dei Responsabili del trattamento è consultabile presso il sito 
<a href="http://www.mi.camcom.it" title="Vai al sito della CCIAA di Milano">www.mi.camcom.it</a>.			
</em></p>
<p><br /></p>
</div>
