<?
$timestamp=date("YmdGis");
$prefisso_file=$PROGETTO."_".$timestamp."_".$uname."_";
//$prefisso_file="varese_20060921164959_admin_";

$report="<strong>Oggetti aggiornati/pubblicati:</strong><br /><br />";

// ARTICOLI
$esegui=1;

$db_pubbl = new DB_CedCamCMS;
$db_pubbl2 = new DB_CedCamCMS;

if (isset($pubbl_art))
{

	$SQL="update SVIL_T_Articoli set PubblicatoDa='".$uid."', Data_Pubblicazione=NOW()";
	$SQL.=" where SVIL_T_Articoli.Id_Articolo=".$pubbl_art."";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-AGGIORNA ARTICOLO-<br />".$SQL."<br /><br />\n";

	$SQL="select * from SVIL_T_Articoli";
	$SQL.=" where SVIL_T_Articoli.Id_Articolo=".$pubbl_art."";
	$SQL.=" into outfile '".$prefisso_file."T_Articoli.txt' fields terminated by '|finecampo|'";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-EXPORT ARTICOLO-<br />".$SQL."<br /><br />\n";

	$SQL="load data infile '".$prefisso_file."T_Articoli.txt' replace into table T_Articoli character set utf8 fields terminated by '|finecampo|'";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-PUBBLICA ARTICOLO-<br />".$SQL."<br /><br />\n";

	//ALTRA LINGUA
	if(is_array($AltreLingue) && sizeof($AltreLingue)>0)
	{
		$SQL="select * from SVIL_T_Articoli_AL";
		$SQL.=" where SVIL_T_Articoli_AL.Id_AL=".$pubbl_art."";
		$SQL.=" into outfile '".$prefisso_file."T_Articoli_AL.txt' fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-EXPORT ARTICOLO_AL-<br />".$SQL."<br /><br />\n";

		$SQL="load data infile '".$prefisso_file."T_Articoli_AL.txt' replace into table T_Articoli_AL character set utf8 fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-PUBBLICA ARTICOLO_AL-<br />".$SQL."<br /><br />\n";
	}


	$SQL="update SVIL_T_Articoli set Flag_Modifica=1";
	$SQL.=" where SVIL_T_Articoli.Id_Articolo=".$pubbl_art."";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-CONFERMA PUBBLICAZIONE ARTICOLO SVIL-<br />".$SQL."<br /><br />\n";

	$SQL="update T_Articoli set Flag_Modifica=1";
	$SQL.=" where T_Articoli.Id_Articolo=".$pubbl_art."";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-CONFERMA PUBBLICAZIONE ARTICOLO-<br />".$SQL."<br /><br />\n";

	echo "<br /><br />Articolo pubblicato.<br /><br />\n";
}
elseif (isset($struct))
{
	$SQL="update SVIL_T_Menu set Flag_Struttura=2";
	$SQL.=" where Flag_Struttura=3 and Id_Menu like '".$struct."%'";
	if ($esegui)
		$db_pubbl->query($SQL);
	else
		echo "-APPROVA STRUTTURA-<br />".$SQL."<br /><br />\n";
		
	echo "<br /><br />Modifiche di struttura approvate.<br /><br />\n";
}
else
{
	$righe3=0;

	if (isset($area))
	{
		$SQL="select Id from SVIL_T_Menu where Id_Menu ='".$area."' or Id_Menu like '".$area.".__'";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-SQL categoriearea -<br />".$SQL."<br /><br />\n";
		$categoriearea="";
		while($db_pubbl->next_record())
		{
			if ($categoriearea=="")
				$categoriearea.="('".$db_pubbl->f('Id')."'";
			else
				$categoriearea.=", '".$db_pubbl->f('Id')."'";
		}
		$categoriearea.=")";
		if (!($esegui))
			echo "- categoriearea -<br />".$categoriearea."<br /><br />\n";

		$SQL="select Id from SVIL_T_Menu where Id_Menu like '".$area."%'";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-SQL paginearea -<br />".$SQL."<br /><br />\n";
		$paginearea="";
		while($db_pubbl->next_record())
		{
			if ($paginearea=="")
				$paginearea.="('".$db_pubbl->f('Id')."'";
			else
				$paginearea.=", '".$db_pubbl->f('Id')."'";
		}
		$paginearea.=")";
		if (!($esegui))
			echo "- paginearea -<br />".$paginearea."<br /><br />\n";
	}

	$SQL="select SVIL_T_Menu.Titolo, SVIL_T_Articoli.Descrizione from SVIL_TJ_Articoli_X_Box_X_Menu,SVIL_T_Menu,SVIL_T_Articoli";
	$SQL.=" where SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu=SVIL_T_Menu.Id";
	$SQL.=" and SVIL_T_Menu.Flag_Modifica=2 ";
	$SQL.=" and SVIL_T_Menu.Id_Modifica=".$uid;
	$SQL.=" and SVIL_TJ_Articoli_X_Box_X_Menu.Id_Articolo=SVIL_T_Articoli.Id_Articolo";
	$SQL.=" and SVIL_TJ_Articoli_X_Box_X_Menu.Stato_Articolo='A'";
	$SQL.=" and SVIL_T_Articoli.Flag_Modifica>2 and SVIL_T_Menu.Id_Menu<'80'";
	if (isset($area))
		$SQL.=" and SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu in ".$paginearea."";
	$db_pubbl->query($SQL);
	$righe1=$db_pubbl->num_rows();

	if (!($esegui))
		echo "-SQL righe 1-<br />".$SQL."<br /><br />\n";

	$SQL="select SVIL_T_Menu.Titolo from SVIL_T_Menu";
	if ($utype==1 || $mastereditor==1)
		$SQL.=" where (Flag_Struttura=3 or (Flag_Struttura=2 and Flag_Modifica>2))";
	else
		$SQL.=" where (((Flag_Struttura=3 or (Flag_Struttura=2 and Flag_Modifica>2)) and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|')))))";
	if (isset($area))
		$SQL.=" and SVIL_T_Menu.Id in ".$paginearea."";
		$SQL.=" and SVIL_T_Menu.Id_Menu<'80'";
	$db_pubbl2->query($SQL);
	$righe2=$db_pubbl2->num_rows();

	if (!($esegui))
		echo "-SQL righe 2-<br />".$SQL."<br /><br />\n";

	$righe3=$righe1+$righe2;
	
	if ($righe3==0)
	{
		$SQL="select count(*) as Articoli from SVIL_T_Articoli";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-COUNT ARTICOLI-<br />".$SQL."<br /><br />\n";
		$db_pubbl->next_record();
		$report.="Articoli:".$db_pubbl->f('Articoli')."<br />\n";

		$SQL="update SVIL_T_Articoli set PubblicatoDa='".$uid."', Data_Pubblicazione=NOW()";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-AGGIORNA ARTICOLI-<br />".$SQL."<br /><br />\n";

		$SQL="select * from SVIL_T_Articoli";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		$SQL.=" into outfile '".$prefisso_file."T_Articoli.txt' fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-EXPORT ARTICOLI-<br />".$SQL."<br /><br />\n";
		//ALTRA LINGUA
		if(is_array($AltreLingue) && sizeof($AltreLingue)>0)
		{
			$SQL="select Id_Articolo from SVIL_T_Articoli";
			$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
			if (isset($area))
				$SQL.=" and SVIL_T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
			$db_pubbl->query($SQL);
			$my_idart="|";
			while($db_pubbl->next_record())
				$my_idart.=$db_pubbl->f("Id_Articolo")."|";
			$SQL="select * from SVIL_T_Articoli_AL";
			$SQL.=" where instr('$my_idart',concat('|',Id_AL,'|'))";

			$SQL.=" into outfile '".$prefisso_file."T_Articoli_AL.txt' fields terminated by '|finecampo|'";
			if ($esegui)
				$db_pubbl->query($SQL);
			else
				echo "-EXPORT ARTICOLI_AL-<br />".$SQL."<br /><br />\n";

		}
		$SQL="select count(*) as Menu from SVIL_T_Menu";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-COUNT PAGINE-<br />".$SQL."<br /><br />\n";
		$db_pubbl->next_record();
		$report.="Pagine:".$db_pubbl->f('Menu')."<br />\n";

		$SQL="update SVIL_T_Menu set PubblicatoDa='".$uid."', Data_Pubblicazione=NOW()";
		$SQL.=" where ((Flag_Modifica=2 and Id_Modifica='".$uid."')";
		if ($utype==1 || $mastereditor==1)
			$SQL.=" or Flag_Struttura=2)";
		else
			$SQL.=" or (Flag_Struttura=2 and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|')))))";
		if (isset($area))
			$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-AGGIORNA MENU-<br />".$SQL."<br /><br />\n";

		$SQL="select * from SVIL_T_Menu";
		$SQL.=" where ((Flag_Modifica=2 and Id_Modifica='".$uid."')";
		if ($utype==1 || $mastereditor==1)
			$SQL.=" or Flag_Struttura=2)";
		else
			$SQL.=" or (Flag_Struttura=2 and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|')))))";
		if (isset($area))
			$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
		$SQL.=" into outfile '".$prefisso_file."T_Menu.txt' fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-EXPORT MENU-<br />".$SQL."<br /><br />\n";
		//ALTRA LINGUA

		if(is_array($AltreLingue) && sizeof($AltreLingue)>0)
		{
			$SQL="select Id from SVIL_T_Menu";
			$SQL.=" where ((Flag_Modifica=2 and Id_Modifica='".$uid."')";
			if ($utype==1 || $mastereditor==1)
				$SQL.=" or Flag_Struttura=2)";
			else
				$SQL.=" or (Flag_Struttura=2 and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|')))))";
			if (isset($area))
				$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
			$db_pubbl->query($SQL);
			$my_idmenu="|";
			while($db_pubbl->next_record())
				$my_idmenu.=$db_pubbl->f("Id")."|";
			$SQL="select * from SVIL_T_Menu_AL";
			$SQL.=" where instr('$my_idmenu',concat('|',Id_AL,'|'))";
			$SQL.=" into outfile '".$prefisso_file."T_Menu_AL.txt' fields terminated by '|finecampo|'";
			if ($esegui)
				$db_pubbl->query($SQL);
			else
				echo "-EXPORT MENU_AL-<br />".$SQL."<br /><br />\n";
			
			
		}



		$SQL="select count(*) as Box from SVIL_T_Box,SVIL_T_Menu";
		$SQL.=" where SVIL_T_Box.Id_Menu2=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Box.Id_Menu2 in ".$paginearea."";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-COUNT BOX-<br />".$SQL."<br /><br />\n";
		$db_pubbl->next_record();
		$report.="Box:".$db_pubbl->f('Box')."<br />\n";

		$SQL="select SVIL_T_Box.* from SVIL_T_Box,SVIL_T_Menu";
		$SQL.=" where SVIL_T_Box.Id_Menu2=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Box.Id_Menu2 in ".$paginearea."";
		$SQL.=" into outfile '".$prefisso_file."T_Box.txt' fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-EXPORT BOX-<br />".$SQL."<br /><br />\n";

		$SQL="select count(*) as ArtXBox from SVIL_TJ_Articoli_X_Box_X_Menu,SVIL_T_Menu";
		$SQL.=" where SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu in ".$paginearea."";
		$db_pubbl->query($SQL);
		if (!($esegui))
			echo "-COUNT Articoli in pagine-<br />".$SQL."<br /><br />\n";
		$db_pubbl->next_record();
		$report.="Articoli in pagine:".$db_pubbl->f('ArtXBox')."<br />\n";

		$SQL="select SVIL_TJ_Articoli_X_Box_X_Menu.* from SVIL_TJ_Articoli_X_Box_X_Menu,SVIL_T_Menu";
		$SQL.=" where SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_TJ_Articoli_X_Box_X_Menu.Id_Menu in ".$paginearea."";
		$SQL.=" into outfile '".$prefisso_file."TJ_Articoli_X_Box_X_Menu.txt' fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-EXPORT ARTICOLI X BOX-<br />".$SQL."<br /><br />\n";

		$SQL="delete T_Box.* from T_Box,SVIL_T_Menu";
		$SQL.=" where T_Box.Id_Menu2=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and T_Box.Id_Menu2 in ".$paginearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-ELIMINA BOX VECCHIE-<br />".$SQL."<br /><br />\n";

		$SQL="delete TJ_Articoli_X_Box_X_Menu.* from TJ_Articoli_X_Box_X_Menu,SVIL_T_Menu";
		$SQL.=" where TJ_Articoli_X_Box_X_Menu.Id_Menu=SVIL_T_Menu.Id and SVIL_T_Menu.Flag_Modifica=2 and SVIL_T_Menu.Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and TJ_Articoli_X_Box_X_Menu.Id_Menu in ".$paginearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-ELIMINA ARTICOLI X BOX VECCHIE-<br />".$SQL."<br /><br />\n";

		$SQL="delete T_Articoli.* from T_Articoli,SVIL_T_Eliminati";
		$SQL.=" where T_Articoli.Id_Articolo=SVIL_T_Eliminati.Id and SVIL_T_Eliminati.Tipo='A' and SVIL_T_Eliminati.Id_Utente='".$uid."' and SVIL_T_Eliminati.Fatto=0";
		if (isset($area))
			$SQL.=" and T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-ELIMINA ARTICOLI VECCHI-<br />".$SQL."<br /><br />\n";

		$SQL="delete T_Menu.* from T_Menu,SVIL_T_Eliminati";
		$SQL.=" where T_Menu.Id=SVIL_T_Eliminati.Id and SVIL_T_Eliminati.Tipo='M' and SVIL_T_Eliminati.Id_Utente='".$uid."' and SVIL_T_Eliminati.Fatto=0";
		if (isset($area))
			$SQL.=" and T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-ELIMINA MENU VECCHI-<br />".$SQL."<br /><br />\n";

		$SQL="update SVIL_T_Eliminati left join T_Menu  on (T_Menu.Id=SVIL_T_Eliminati.Id) set Fatto=1";
		$SQL.=" where T_Menu.Id is null and SVIL_T_Eliminati.Tipo='M' and Fatto=0";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA CANCELLAZIONE MENU-<br />".$SQL."<br /><br />\n";

		$SQL="update SVIL_T_Eliminati left join T_Articoli  on (T_Articoli.Id_Articolo=SVIL_T_Eliminati.Id) set Fatto=1";
		$SQL.=" where T_Articoli.Id_Articolo is null and SVIL_T_Eliminati.Tipo='A' and Fatto=0";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA CANCELLAZIONE ARTICOLI-<br />".$SQL."<br /><br />\n";

		$SQL="load data infile '".$prefisso_file."T_Articoli.txt' replace into table T_Articoli character set utf8 fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-PUBBLICA ARTICOLI-<br />".$SQL."<br /><br />\n";

		$SQL="load data infile '".$prefisso_file."T_Box.txt' replace into table T_Box character set utf8 fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-PUBBLICA BOX-<br />".$SQL."<br /><br />\n";

		$SQL="load data infile '".$prefisso_file."TJ_Articoli_X_Box_X_Menu.txt' replace into table TJ_Articoli_X_Box_X_Menu character set utf8 fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-PUBBLICA ARTICOLI X BOX-<br />".$SQL."<br /><br />\n";

		$SQL="load data infile '".$prefisso_file."T_Menu.txt' replace into table T_Menu character set utf8 fields terminated by '|finecampo|'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-PUBBLICA MENU-<br />".$SQL."<br /><br />\n";

		if(is_array($AltreLingue) && sizeof($AltreLingue)>0)
		{
			$SQL="load data infile '".$prefisso_file."T_Articoli_AL.txt' replace into table T_Articoli_AL character set utf8 fields terminated by '|finecampo|'";
			if ($esegui)
				$db_pubbl->query($SQL);
			else
				echo "-PUBBLICA ARTICOLI-<br />".$SQL."<br /><br />\n";
			$SQL="load data infile '".$prefisso_file."T_Menu_AL.txt' replace into table T_Menu_AL character set utf8 fields terminated by '|finecampo|'";
			if ($esegui)
				$db_pubbl->query($SQL);
			else
				echo "-PUBBLICA MENU-<br />".$SQL."<br /><br />\n";
		}


		$SQL="update SVIL_T_Articoli set Flag_Modifica=1";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE ARTICOLI SVIL-<br />".$SQL."<br /><br />\n";

		$SQL="update SVIL_T_Menu set Flag_Modifica=1";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE MENU SVIL-<br />".$SQL."<br /><br />\n";

		$SQL="update SVIL_T_Menu set Flag_Struttura=1";
		if ($utype==1 || $mastereditor==1)
			$SQL.=" where Flag_Struttura=2";
		else
			$SQL.=" where (Flag_Struttura=2 and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|'))))";
		if (isset($area))
			$SQL.=" and SVIL_T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE STRUTTURA MENU SVIL-<br />".$SQL."<br /><br />\n";

		$SQL="update T_Articoli set Flag_Modifica=1";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and T_Articoli.Id_TipoArticolo1 in ".$categoriearea."";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE ARTICOLI-<br />".$SQL."<br /><br />\n";

		$SQL="update T_Menu set Flag_Modifica=1";
		$SQL.=" where Flag_Modifica=2 and Id_Modifica='".$uid."'";
		if (isset($area))
			$SQL.=" and T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE MENU-<br />".$SQL."<br /><br />\n";

		$SQL="update T_Menu set Flag_Struttura=1";
		if ($utype==1 || $mastereditor==1)
			$SQL.=" where Flag_Struttura=2";
		else
			$SQL.=" where (Flag_Struttura=2 and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|'))))";
		if (isset($area))
			$SQL.=" and T_Menu.Id_Menu like '".$area."%'";
		if ($esegui)
			$db_pubbl->query($SQL);
		else
			echo "-CONFERMA PUBBLICAZIONE STRUTTURA MENU-<br />".$SQL."<br /><br />\n";

		echo $report;
	}
	else
	{
		if ($righe1>0)
		{
			echo "<br /><br />Attenzione: nelle pagine che stai cercando di pubblicare ci sono articoli non approvati.<br /><br />\n";
			while ($db_pubbl->next_record())
				echo "<strong>Pagina</strong>: ".$db_pubbl->f('Titolo')." - <strong>Articolo</strong>: ".$db_pubbl->f('Descrizione')."<br />\n";
			echo "<br />Procedere con l'approvazione degli articoli prima di pubblicare le pagine.<br /> Nel caso non sia possibile, modificare lo stato della pagina in 'Da approvare' per poter pubblicare le altre.<br />\n";
		}
		if ($righe2>0)
		{
			echo "<br /><br />Attenzione: prima di pubblicare le pagine � necessario aver approvato tutte le modifiche di struttura e che tutte le pagine che hanno subito modifiche di struttura non siano ancora in modifica o da approvare.<br /><br />\n";
			while ($db_pubbl2->next_record())
				echo "<strong>Pagina</strong>: ".$db_pubbl2->f('Titolo')."<br />\n";
		}
	}
}
echo "<br /><br /><a href=\"/index.phtml?pagina=mappa&amp;tipomappa=admin\">Torna alla mappa</a>.<br /><br />\n";

/*
// --- UPDATE per test ---
update `SVIL_T_Articoli` set Flag_Modifica=2 where Flag_Modifica>2;
update `SVIL_T_Menu` set Flag_Modifica=2 where Flag_Modifica>2;

update `SVIL_T_Articoli` set Id_Modifica=1 where Flag_Modifica=2;
update `SVIL_T_Menu` set Id_Modifica=1 where Flag_Modifica=2;
*/

?>