<div id="divPreferiti">
<?php
$db = new DataBase();
$IdPartecipante = $auth->auth["uid"];
?>
<input type="hidden" id="IdLog" name="IdLog" value="<?php echo $IdPartecipante; ?>"/>
<?php

$arrayRighe = array();
/*
$rTitolo = '<tr>
                                <th  width="40%" scope="col" class="tdHeaderAll">
<input type="checkbox" id="all" name="all" value="Y">
                                </th>
                                <th  scope="col" class="tdHeaderRic">' . _ADDETTI_ . '</th>
                                <th width="20%" scope="col" class="tdHeaderRicLeft" >' . _SEDE_LEG_ . '</th>
                                <th width="25%" scope="col" class="tdHeaderRicRight" >' . _FATTURATO_5_ . '</th>
                            </tr>';
*/
$rTitolo = '<tr>
                                <th colspan="2" scope="col" class="tdHeaderAll">
								
								<ul id="tooltipSelect">
								<li><input type="checkbox" id="all" name="all"  value="Y"><img src="images/expo2015/freccia_giu_grigio.png"/>
									<ul class="selectBox">
										<li><input class="selectBox" type="button" value="'._TUTTE_.'" onclick="selezionaTutti()" /></li>
										<li><input class="selectBox" type="button" value="'._NESSUNA_.'" onclick="delesezionaTutti()" /></li>
										<li><input class="selectBox" type="button" value="'._INVERTI_.'" onclick="invertiSelezione()" /></li>
									</ul>
									<div class="clear"></div>
								</li>
								</ul>
								
								</th>
								<th colspan="2" scope="col" class="tdHeaderRicLeft">' . _RAGIONESOCILE_ . '</th>
								<th width="10%" scope="col" class="tdHeaderRic">' . _ADDETTI_ . '</th>
                               	<th width="20%" scope="col" class="tdHeaderRicLeft" >' . _SEDE_LEG_ . '</th>
                                <th width="17%" scope="col" class="tdHeaderRicRight" >' . _FATTURATO_5_ . '<a href="#" class="tooltip" draggable="false" >
                    <img src="/tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                    <span>'. _TEXT_INFO_P_ .'</span>
                </a><br />' . _R_U_FATTURATO_2_ .'
               </th>
                            </tr>';

$query = "SELECT DISTINCT T2.id AS IdImpresa, RagioneSociale,Citta,Pr,UltimoFatturato,NumeroDipendenti
        FROM EXPO_T_Imprese AS T2
        INNER JOIN EXPO_T_Imprese_Preferiti AS T1 ON T2.Id = T1.IdImpresa
        AND T1.IdPartecipante = '$IdPartecipante'
        ORDER BY RagioneSociale";

$isColor = 0;
$idR = 0;
if ($db->NumRows($query) == 0) {
    ?>
    <h2><?php echo _NO_PREFERITI_; ?></h2>
    <?php
} else {
    foreach ($db->GetRows($query) AS $rows) {

        $idImp = $rows['IdImpresa'];
        $qProd = "SELECT Id,Nome FROM EXPO_T_Prodotti WHERE IdImpresa = '$idImp' ORDER BY Nome";

        $tabImpresa = "";
        $idPref = $db->GetRow("SELECT * FROM EXPO_T_Imprese_Preferiti WHERE IdPartecipante = '$IdPartecipante' AND IdImpresa = '$idImp'", 'Id');
        $tag = "preferiti_stella_accesa";
        if (strcmp($idPref, "") == 0) {
            $tag = "preferiti_stella_spenta";
        }
        $tabImpresa .= '<tr ' . getColor("lineTable", $isColor) . ' id="del' . $idImp . '">
        
<td width="1%">
<input type="checkbox" id="richiestaImp" name="richiestaImp" value="' . $idImp . '">
</td><td width="1%">
                                        <a draggable="false" href="javascript:selectPreferiti(\'del' . $idImp . '\',\'' . $idImp . '\',\'' . $IdPartecipante . '\')" id="preferito' . $idImp . '"><img draggable="false" src="images/expo2015/' . $tag . '.png"></a>
</td><td >';
        
        $tabImpresa .= '<a draggable="false" href="javascript:visualizzaBOX(\'IMP\',\'' . $idImp . '\')" >' . $rows['RagioneSociale'] . '</a></td><td width="1%">';
        
        if ($db->NumRows($qProd) > 0)
        	$tabImpresa .= '<a draggable="false" href="javascript:visualizzaProdotti(\'' . $idImp . '\')" ><img draggable="false" id="img' . $idImp . '" src="/images/expo2015/piu.png" width="11px" /></a>';
        
        
        
        $tabImpresa .='</td>
                                </td>
                                <td style="text-align: center;" >
                                ' . $rows['NumeroDipendenti'] . '
                            </td>
                             <td style="text-align: left;" >
                               ' . $rows['Citta'] . ' (' . $rows['Pr'] . ')
                            </td>
                            <td style="text-align: right" >
                                ' . number_format(doubleval($rows['UltimoFatturato']/1000), 0, ',', '.') . '
                            </td>
                        </tr>';
                        
                        
                        /*
        
        $tabImpresa .= '<tr ' . getColor("lineTable", $isColor) . ' id="del' . $idImp . '">
                            <td>
<table width="100%" >
<tr><td width="10%" style="border:0px;">
<input type="checkbox" id="richiestaImp" name="richiestaImp" value="' . $idImp . '">
</td><td width="10%" style="border:0px;">
<a draggable="false" href="javascript:selectPreferiti(\'del' . $idImp . '\',\'' . $idImp . '\',\'' . $IdPartecipante . '\')" id="preferito' . $idImp . '"><img draggable="false" src="images/expo2015/' . $tag . '.png"></a>
</td><td width="10%" style="border:0px;">';
        if ($db->NumRows($qProd) > 0)
            $tabImpresa .= '<a draggable="false" href="javascript:visualizzaProdotti(\'' . $idImp . '\')" ><img draggable="false" id="img' . $idImp . '" src="/images/expo2015/piu.png" width="16px" /></a>';

        $tabImpresa .= '</td><td style="border:0px;"><a draggable="false" href="javascript:visualizzaBOX(\'IMP\',\'' . $idImp . '\')" >' . $rows['RagioneSociale'] . '</a>
</td></tr></table>                                    
                                </td>
                                <td style="text-align: center;" class="tdBorderLeft">   
                                ' . $rows['NumeroDipendenti'] . '
                            </td>
                            <td style="text-align: left;" class="tdBorderLeft">
                               ' . $rows['Citta'] . ' ( ' . $rows['Pr'] . ' )
                            </td>
                            <td style="text-align: right" class="tdBorderLeft">
                                ' . number_format(doubleval($rows['UltimoFatturato']), 2, ',', '.') . ' &euro;
                            </td>
                        </tr>
                        <tr class="trBorder" id="trDivLinea'.$idImp.'"><td colspan="4"></td></tr>
                        <tr class="trBorderBottomNo" id="trDiv'.$idImp.'"><td colspan="4"></td></tr>';
*/
        $arrayRighe[] = $tabImpresa;

        if ($db->NumRows($qProd) > 0) {
            $tabProdotti = '<tr class="subElencoHidden" id="tr' . $idImp . '">
                                    <td colspan="6">
                            <table  width="100%">';
            $numProd = 0;
            $isColorSub = 0;

            foreach ($db->GetRows($qProd) AS $rowsProd) {
                $idPro = $rowsProd['Id'];
                $nomeProdotto = $rowsProd['Nome'];
                if (strcmp($Lang, "IT") != 0) {
                	$nomeProdotto = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = 'nome' AND Lang = '$Lang' AND  IdTipo = '" . ("P" . $idPro) . "'", "Testo");
                }
                $tabProdotti .= '<tr ' . getColor("lineTable", $isColorSub) . '>
                        <td width ="1%">
                        <input type="checkbox" id="richiestaProd" name="richiestaProd" value="' . $idPro . '">
                        </td><td>
						<a draggable="false" href="javascript:visualizzaBOX(\'PRT\',\'' . $idPro . '\')" >' . $nomeProdotto . '</a>
                            </td>
                                
                        </tr>';
                $numProd++;
                $isColorSub++;
            }
            $tabProdotti .='</table></td></tr>';

            $arrayRighe[] = $tabProdotti;
        }
        $idR++;
        $isColor++;
    }
    $evento = "javascript:cerca('index.phtml?Id_VMenu=" . $Id_VMenu . "&amp;azione=SRC')";
    $nElementi = settaVar($_POST, "elementiN", "25");
    $html = multiPage($rTitolo, $arrayRighe, $nElementi, $evento);
}

if (strcmp($html, "") != 0) {
    echo $html;
    echo '<a class="buttonsLista" draggable="false" href="javascript:selezioneRichiesta()">' . _RICHIESTA_R_ . '</a>';
}
?>
</div>   
<input type="hidden" id="IdPartecipante" name="IdPartecipante" value="<?php echo $IdPartecipante; ?>"/>
<div id="coprente3" >
    <div id="box_popup3"></div>
</div>
<div id="coprente" >
    <div id="box_popup"></div>
</div>  
<div id="coprente2" >
    <div id="box_popup2"></div>
</div>