<?php
require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();


if (strcmp($azione, "SEND") == 0) {

    $oggetto = settaVar($_POST, "oggetto", "");
    $testo = settaVar($_POST, "messaggio", "");
    $valore = str_replace(",00", "", str_replace(".", "", settaVar($_POST, "valore", "0")));

    $idUtente = $auth->auth["uid"];
    $idImpresa = settaVar($_POST, "idImpresa", "");

    
    $db->Query("INSERT INTO EXPO_T_Bacheca VALUES((SELECT @idBacheca:= COALESCE(MAX(T.Id),0)+1 AS Id FROM EXPO_T_Bacheca AS T),'$idUtente','$idImpresa','$oggetto')");
    $idBacheca = $db->GetRow("SELECT @idBacheca As idBacheca", "idBacheca");
    $query = "SELECT Id FROM EXPO_T_Prodotti WHERE IdImpresa = '$idImpresa'";
    foreach ($db->GetRows($query) AS $rows) {
        $idProdotto = settaVar($_POST, "prodotto" . $rows['Id'], "");
        if (strcmp($idProdotto, "") != 0) {
            $db->Query("INSERT INTO  EXPO_TJ_Bacheca_Prodotti (IdBacheca ,IdProdotto) VALUES ('$idBacheca', '$idProdotto')");
        }
    }

    
    
    $idMittente = $idUtente;
    $tipoMittente = 2;
    $idDestinatario = $idImpresa;
    $tipoDestinatario = 1;
    
    //$idMsg = $db->GetRow("SELECT MAX(Id) AS Id FROM EXPO_T_Bacheca_Messaggi", "Id") + 1;
    
    $db->Query("INSERT INTO EXPO_T_Bacheca_Messaggi VALUES((SELECT @idMsg:= COALESCE(MAX(BM.Id),0)+1 AS Id FROM EXPO_T_Bacheca_Messaggi AS BM),'$idBacheca','$idMittente',
                    '$tipoMittente','$idDestinatario','$tipoDestinatario','".mysql_escape_string($testo)."',NOW())");
    $idMsg = $db->GetRow("SELECT @idMsg As idMsg", "idMsg");
    
    $db->Query("INSERT INTO EXPO_T_Bacheca_Trattative VALUES((SELECT @idTrattativa:= COALESCE(MAX(BT.Id),0)+1 AS Id FROM EXPO_T_Bacheca_Trattative AS BT),'$idBacheca','$idUtente','$valore',NOW(),'A','$idMsg')");
    $idTrattativa = $db->GetRow("SELECT @idTrattativa As idTrattativa", "idTrattativa");
    $mail = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", "Email");
    $Impresa = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", "RagioneSociale");
    $mittente = $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$idUtente'", "Nome") . " " . $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$idUtente'", "Cognome");

    include_once "send_mail.inc";

    send_mail($mail, $MAILSITO, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _OGG_NOT_C_TRA_P_), preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NOT_C_TRA_P_), true);

    echo _SEND_OK_;
} else {
    ?>
    <form enctype="multipart/form-data" id="form_moduli" name="form_moduli"  >
        <input type = "hidden" name = "IdPar" id = "IdPar" value = "<?php echo $auth->auth["uid"]; ?>"/>
        <div class="boxMessaggi">
            <table>
                <tr>
                    <td class="tdText"><?php echo _OGGETTO_; ?>:*</td>
                    <td class="tdField">
                        <input type = "text" id = "oggetto" name = "oggetto" maxlength = "250" value = ""/>
                    </td>
                    <td class="tdText"><?php echo _IMPRESA_; ?>:*</td>
                    <td class="tdField">
                        <select id="idImpresa" name="idImpresa" onchange="viewProdotti(this)" >
                            <option value=""><?php echo _SELEZIONA_; ?></option>
                            <?php
                            $query = "SELECT DISTINCT Id,RagioneSociale FROM EXPO_T_Imprese WHERE StatoRegistrazione > 4 ORDER BY RagioneSociale";
                            foreach ($db->GetRows($query) AS $rows) {
                                ?>
                                <option value="<?php echo $rows['Id']; ?>"><?php echo $rows['RagioneSociale']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdText"><?php echo _VALORE_; ?>:*</td>
                    <td>
                        <input type = "text" id = "valore" name = "valore" size="10" maxlength="15" value = ""/> &euro;
                    </td>
                    <td class="tdText" id="tdTextProdotto"><?php echo _PRODOTTI_ ; ?>:</td>
                    <td id="prodottiTd"></td>  
                </tr>
               <tr>
                   <td colspan="4"><br></td>  
                </tr>
                <tr>
                    <td colspan="4"><?php echo _MESSAGGIO_; ?>:*</td>
                </tr>
                <tr >
                    <td colspan="4" ><textarea style="border: 1px solid #" rows="12" cols="70" id="messaggio" name="messaggio"></textarea></td>
                </tr>
                <tr>
                   <td colspan="4" id="tdButton"><a draggable="false" class="buttonsLista" href="javascript:invia('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=SEND')"><?php echo _INVIA_; ?></a></td>  
                </tr>
            </table>
            
        </div>
    </form>
    <input type="hidden" id="msgError" name="msgError" value="<?php echo _CAMPI_OB_; ?>"/>
<?php } ?>