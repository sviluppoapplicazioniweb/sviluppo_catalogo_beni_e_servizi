<style>
.inputCheck{
valign="top"
}
</style>

<?php
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/AlgoritmoOrdinamento.php");

$db = new DataBase();
$nomeImp = "";
$nomeRete = "";
$arrayListImprese = array();
foreach ($db->GetRows("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE StatoRegistrazione = '5' ORDER BY RagioneSociale") AS $rows) {
    $arrayListImprese[] = $rows['RagioneSociale'];
}

$arrayListReti = array();
foreach ($db->GetRows("SELECT NomeRete FROM EXPO_T_RetiImpresa ORDER BY NomeRete") AS $rows) {
	$arrayListReti[] = $rows['NomeRete'];
}
//$nomeImp = substr($nomeImp, 0, -1);
$nomeImp = implode("|", $arrayListImprese);
$nomeRete = implode("|", $arrayListReti);

$IdPartecipante = $auth->auth["uid"];

$SQLSelect = "SELECT DISTINCT RagioneSociale,Citta,Pr,UltimoFatturato,NumeroDipendenti,T1.id AS IdImpresa,PresenzaTotaleRicerche,PresenzaPrimePosizioni
        FROM EXPO_T_Imprese AS T1 ";

$SQLWhere = " WHERE RagioneSociale IS NOT NULL AND StatoRegistrazione = '5' ";


$impresa = settaVar($_POST, 'impresa', '');
$numeroD = settaVar($_POST, 'numeroD', '');
$fatturato = settaVar($_POST, 'fatturato', '');
$IsSediEstere = settaVar($_POST, 'sedEstera', 'N');
$IsNetwork = settaVar($_POST, 'netetwork', 'N');
$IsGeneralContractor = settaVar($_POST, 'gereralContractor', 'N');
$IsProduttore = settaVar($_POST, 'produttore', 'N');
$IsDistributore = settaVar($_POST, 'distributore', 'N');
$IsPrestatoreDiServizi = settaVar($_POST, 'IsPrestatoreDiServizi', 'N');
$IsPartecipazioneExpo = settaVar($_POST, 'partecipazioneExpo', 'N');
$IsPartecipazioniInternazionali = settaVar($_POST, 'partecipazioniInternazionali', 'N');
$IsSindacale = settaVar($_POST, 'sindacale', 'N');
$IsModello = settaVar($_POST, 'aModello', 'N');
$IsRetingLegalita = settaVar($_POST, 'pLegale', 'N');
$IsMasterSpecialistico = settaVar($_POST, 'pMaster', 'N');
$IsCertificazione = settaVar($_POST, 'IsCertificazione', 'N');
$IsRetiImpresa = settaVar($_POST, 'retiImpresa', 'N');
$rete = settaVar($_POST, 'rete', '');
$IsSiexpo2015 = settaVar($_POST, 'IsIscrSiexpo', 'N');
$cercaHtdig = settaVar($_POST, 'cercaHtdig', '');

$SQLWhereCondition = "";
$SQLJoinProdotti = "";
$SQLJoinCategorie = "";
$SQLCategorie = "";
$SQLJoinLingue = "";
$SQLLingue = "";
$SQLJoinAssociazioni = "";
$SQLAssociazioni = "";
$SQLJoinCertificazione = "";
$SQLCertificazione = "";
$SQLSiexpo = "";
$SQLForId = "";

if (strcmp($cercaHtdig, "") != 0) {
    require_once($PROGETTO . "/view/lib/htDig.class.php");

    $htDig = new HtDig();
    $returnSql = $htDig->searchWhitHtDig($cercaHtdig, array("Imprese" => "T1", "Prodotti" => "T2","Reti" => "T9"));
    if (strcmp($returnSql, "") != 0) {
        $SQLForId = $returnSql;
        //print $SQLForId;
    }

    if (strcmp($SQLForId, "") == 0) {
        $SQLWhereCondition .= "AND T1.Id = '0000' ";
    } else {
        $SQLJoinProdotti = " LEFT JOIN EXPO_T_Prodotti AS T2 ON T1.Id = T2.IdImpresa AND T2.StatoProdotto = 'A'";
        $SQLJoinReti = " JOIN EXPO_TJ_Imprese_RetiImpresa as T9 ON T1.Id = T9.Id_Impresa ";
        $SQLWhereCondition .= "AND ($SQLForId) ";
    }
}


$ricercheCategorie = array();
//CATEGORIA
$query = "SELECT Id FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = 8 ";
foreach ($db->GetRows($query) AS $rows) {
    if (isset($_POST['categoria' . $rows['Id']])) {
        $IdCat = $rows['Id'];
        $ricercheCategorie[] = $IdCat; 
        $SQLCategorie .= " IdCategoria = '$IdCat' OR";
    }
}
if (strcmp($SQLCategorie, "") != 0) {
    $SQLCategorie = substr($SQLCategorie, 0, -2);
    $SQLJoinCategorie = " RIGHT JOIN EXPO_TJ_Imprese_Categorie AS T3 ON T1.Id = T3.IdImpresa AND T3.IsIscritta = 'Y' AND ($SQLCategorie) ";
}


$ricercheLingue = array();
//LINGUE
$query = "SELECT Id FROM EXPO_Tlk_Lingue ";
foreach ($db->GetRows($query) AS $rows) {
    if (isset($_POST['lingue' . $rows['Id']])) {
        $IdLingue = $rows['Id'];
        $ricercheLingue[] = $IdLingue;
        $SQLLingue .= " IdLingua = '$IdLingue' OR";
    }
}
if (strcmp($SQLLingue, "") != 0) {
    $SQLLingue = substr($SQLLingue, 0, -2);
    $SQLJoinLingue = "RIGHT JOIN EXPO_TJ_Imprese_Lingue AS T4 ON T1.Id = T4.IdImpresa AND ($SQLLingue) ";
}

//ASSOCIZONI
$query = "SELECT DISTINCT Denominazione, Id FROM EXPO_Tlk_Associazioni";
foreach ($db->GetRows($query) AS $rows) {
    if (isset($_POST['associazioni' . $rows['Id']])) {
        $IdAssociazioni = $rows['Id'];
        $SQLAssociazioni .= " IdAssociazione = '$IdAssociazioni' OR";
    }
}
if (strcmp($SQLAssociazioni, "") != 0) {
    $SQLAssociazioni = substr($SQLAssociazioni, 0, -2);
    $SQLJoinAssociazioni = "RIGHT JOIN EXPO_TJ_Imprese_Associazioni AS T5 ON T1.Id = T5.IdImpresa AND ($SQLAssociazioni) ";
}

$ricercheCertificazioni = array();
//CERTIFICAZIONI
$query = "SELECT DISTINCT Descrizione,Id
            FROM EXPO_TJ_Imprese_Certificazione";
foreach ($db->GetRows($query) AS $rows) {
    if (isset($_POST['certificazioni' . $rows['Id']])) {
        $IdCertificazione = $rows['Id'];
        $ricercheCertificazioni[] = $IdCertificazione;
        $SQLCertificazione .= " IdCertificazione = '$IdCertificazione' OR";
    }
}
if (strcmp($SQLCertificazione, "") != 0) {
    $SQLCertificazione = substr($SQLCertificazione, 0, -2);
    $SQLJoinCertificazione = "RIGHT JOIN EXPO_TJ_Imprese_Certificazione AS T6 ON T1.Id = T6.IdImpresa AND ($SQLCertificazione) ";
}

//RetiImpresa
if ($IsRetiImpresa == "Y") {
	$SQLJoinReti = "INNER JOIN EXPO_TJ_Imprese_RetiImpresa AS T9 ON T1.Id = T9.Id_Impresa";
	if (strlen($rete)>0){
		//Ricava l'id per inserirlo nella query
		$qryRete = "SELECT * FROM EXPO_T_RetiImpresa WHERE NomeRete LIKE '".$rete."%'";
		$idRete = $db->GetRow($qryRete,'Id',null,'visualizza_Ricerca Ln.158');
		$SQLReti = "Id_Rete = $idRete";
		if ($idRete > 0){
			$SQLJoinReti .= " AND ($SQLReti)"; 
		}
		//$SQLJoinReti = "INNER JOIN EXPO_TJ_Imprese_RetiImpresa AS T9 ON T1.Id = T9.Id_Impresa AND ($SQLReti)";
	} /*else {
		$SQLJoinReti = "INNER JOIN EXPO_TJ_Imprese_RetiImpresa AS T9 ON T1.Id = T9.Id_Impresa";
	}*/
}

//echo 'Nome Rete: '.$rete.'<br/>';
//LISTA PARAMETRI RICERCA

$parametriRicerca = "";
$parametriRicerca .= getParametriRicerca(array($impresa, $numeroD, $fatturato), "", _IMPRESA_);
$parametriRicerca .= getParametriRicerca(array($SQLCategorie), "", _C_MERC_);
$parametriRicerca .= getParametriRicerca(array($IsGeneralContractor, $IsProduttore, $IsDistributore), "N", _T_IMPRESA_);
$parametriRicerca .= getParametriRicerca(array($IsSediEstere, $IsNetwork), "N", _SEDI_);
$parametriRicerca .= getParametriRicerca(array($IsPartecipazioneExpo, $IsPartecipazioniInternazionali), "N", _REFERENZE_);
$parametriRicerca .= getParametriRicerca(array($SQLLingue), "", _CON_LINGUE_);
$parametriRicerca .= getParametriRicerca(array($IsSindacale, $IsModello, $IsRetingLegalita, $IsMasterSpecialistico, $IsSiexpo2015), "N", _QUALIT_);
$parametriRicerca .= getParametriRicerca(array($SQLCertificazione), "", _CERTIFICAZIONI2_);
$parametriRicerca .= getParametriRicerca(array($IsRetiImpresa), "N", _R_RETE_IMP_);
//DA Aggiungere parametro Nome Rete

$SQLWhereCondition .= setTextSQLW('T1', "RagioneSociale", $impresa);
$SQLWhereCondition .= setNumSQLW('T1', "NumeroDipendenti", $numeroD);
$SQLWhereCondition .= setNumFattSQLW('T1', "UltimoFatturato", $fatturato);
$SQLWhereCondition .= setSQLW('T1', "IsSediEstere", $IsSediEstere);
$SQLWhereCondition .= setSQLW('T1', "IsNetwork", $IsNetwork);
$SQLWhereCondition .= setSQLW('T1', "IsGeneralContractor", $IsGeneralContractor);
$SQLWhereCondition .= setSQLW('T1', "IsProduttore", $IsProduttore);
$SQLWhereCondition .= setSQLW('T1', "IsDistributore", $IsDistributore);
$SQLWhereCondition .= setSQLW('T1', "IsPrestatoreDiServizi", $IsPrestatoreDiServizi);
$SQLWhereCondition .= setSQLW('T1', "IsPartecipazioneExpo", $IsPartecipazioneExpo);
$SQLWhereCondition .= setSQLW('T1', "IsPartecipazioniInternazionali", $IsPartecipazioniInternazionali);
$SQLWhereCondition .= setSQLW('T1', "IsSindacale", $IsSindacale);
$SQLWhereCondition .= setSQLW('T1', "IsModello", $IsModello);
$SQLWhereCondition .= setSQLW('T1', "IsRetingLegalita", $IsRetingLegalita);
$SQLWhereCondition .= setSQLW('T1', "IsMasterSpecialistico", $IsMasterSpecialistico);
$SQLWhereCondition .= setSQLW('T1', "IsCertificazione", $IsCertificazione);
$SQLWhereCondition .= setSQLW('T1', "IsSiexpo2015", $IsSiexpo2015);




$SQLWhere .= $SQLWhereCondition;



$SQL = $SQLSelect . $SQLJoinCategorie . $SQLJoinLingue . $SQLJoinAssociazioni . $SQLJoinCertificazione . $SQLJoinReti . $SQLJoinProdotti . $SQLWhere; // . " ORDER BY RAND()";


if (strcmp($parametriRicerca, "") != 0) {
    ?>
    <script type="text/javascript">
        $("#parametriRicerca").html("<br><?php echo _PARAMETRI_RICERCA_ . $parametriRicerca; ?>");
    </script>    
<?php } ?>

<input type="hidden" id="listaImprese" name="listaImprese" value="<?php echo $nomeImp; ?>"/>
<input type="hidden" id="listaReti" name="listaReti" value="<?php echo $nomeRete; ?>"/>
<input type="hidden" id="IdLog" name="IdLog" value="<?php echo $IdPartecipante; ?>"/>
<input type="hidden" id="numParametri" name="numParametri" value="<?php echo _R_N_PARAMETRI_; ?>"/>
<form id="formCerca" name="formCerca">
    <div id="ricerca">

        <div id="datiRicerca" >

            <div id="bottoneCercaClass">
                <a class="buttonCerca" href="javascript:cerca('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=SRC')" draggable="false"><?php echo _RICERCA_; ?></a>
            </div>

            <div class="boxParametro">
                <a draggable="false" href="javascript:aggiungiParametri('categoria')" class="tooltip">+ <?php echo _C_MERC_; ?><!-- <span><?php //echo _TEXT_INFO_C_MERC_;?></span>--></a>
                <span class="textAdd" id="categoria">
                    <?php
                    $parametri = _R_N_PARAMETRI_ . "<br>";
                    $i = 0;
                    $descAl = array("IT" => "", "FR" => "_AL2", "EN" => "_AL");
                    $query = "SELECT Id,DescEstesa,DescEstesa_AL,DescEstesa_AL2 FROM  `EXPO_Tlk_Categorie` WHERE LENGTH(Id_Categoria) = 8 ORDER BY Descrizione";
                    foreach ($db->GetRows($query) AS $rows) {
                        if (isset($_POST['categoria' . $rows['Id']])) {
                            echo '<input type ="hidden" name="categoria' . $rows['Id'] . '" id = "categoria' . $rows['Id'] . '" value = "Y"/>';
                            $parametri .= $rows["DescEstesa" . $descAl[$Lang]] . "<br>";
                            $i++;
                        }
                    }

                    echo '<a draggable="false" href="javascript:aggiungiParametri(\'categoria\')">(' . $i . ')</a>';
                    if ($i > 0) {
                        ?>

                        <a href="javascript:aggiungiParametri('categoria')" class="tooltip" draggable="false" >
                            <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                            <span> <?php echo $parametri; ?></span>
                        </a>
                    <?php } ?>
                </span>
            </div>

             <script type="text/javascript">
                $(document).ready(function() {
               		$("[name=numeroD]").change(function(){
                		$(".linkRicercanumeroD").show();
                	})
                });

                $(document).ready(function() {
                    $("[name=fatturato]").change(function(){
                    	$(".linkRicercafatturato").show();
                    })
                });
            </script>
            
            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _IMPRESA_; ?></span><br>
                <input type="text" id = "impresa" name = "impresa" maxlength = "255" value = "<?php echo $impresa; ?>"/><br />
                <label for="numeroD"><?php echo _N_DIP_; ?></label> 
                <span style="display:none" class="linkRicercanumeroD" onclick="javascript:deseleziona('numeroD')"><img src="/images/expo2015/b_drop.png" ></span><br />
                <table>
                <?php
                $LangTab = "Descrizione";
                if (strcmp($Lang, "IT") != 0)
                    $LangTab .= "_" . $Lang;

                $query = "SELECT * FROM EXPO_T_Tipo_Impresa AS T"
                        . " JOIN EXPO_TJ_Tipo_Impresa_Dipendenti AS TJ"
                        . " ON T.Id = TJ.IdTipoImpresa ORDER BY T.Id";
                foreach ($db->GetRows($query) AS $rows) {
                    $selectD = "";
                    if (strcmp($rows['Valore'], $numeroD) == 0) {
                        $selectD = "checked";
                        print ' <script type="text/javascript">$(".linkRicercanumeroD").show();</script>';
                    }
                    ?>
                    <tr><td valign="top"><input type="radio" id = "numeroD" name = "numeroD" value="<?php echo $rows['Valore']; ?>" <?php echo $selectD; ?> ></td><td><?php echo str_replace("|", " > ", $rows['Valore']); ?> </td></tr>
                <?php } ?>
				</table>
               
                <label for="fatturato"><?php echo _R_U_FATTURATO_; ?></label>
                <span style="display:none" class="linkRicercafatturato" onclick="javascript:deseleziona('fatturato')"><img src="/images/expo2015/b_drop.png" ></span>
                  
                <a href="#" class="tooltip" draggable="false" >
                    <img src="/tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                    <span><?php echo _TEXT_INFO_P_; ?></span>
                </a>
                <br />
				<label for="fatturato"><?php echo _R_U_FATTURATO_1_; ?></label>
				 <br /><table>
                <?php
                $LangTab = "Descrizione";
                if (strcmp($Lang, "IT") != 0)
                    $LangTab .= "_" . $Lang;

                $query = "SELECT * FROM EXPO_T_Tipo_Impresa AS T"
                        . " JOIN EXPO_TJ_Tipo_Impresa_Fatturato AS TJ"
                        . " ON T.Id = TJ.IdTipoImpresa ORDER BY T.Id";
                foreach ($db->GetRows($query) AS $rows) {
                    $selectF = "";
                    if (strcmp($rows['Valore'], $fatturato) == 0) {
                        $selectF = "checked";
                        print ' <script type="text/javascript">$(".linkRicercafatturato").show();</script>';
                    }
                    ?>
                    <tr><td valign="top"><input type="radio" id = "fatturato" name = "fatturato" value="<?php echo $rows['Valore']; ?>" <?php echo $selectF; ?>></td><td><?php echo str_replace("|", " > ", $rows['Valore']); ?> </td></tr>
                <?php } ?>
				</table>

            </div>

            
            
            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _T_IMPRESA_; ?></span><br>
                <table><tr><td valign="top"><input type="checkbox" id="gereralContractor" name="gereralContractor" value="Y" <?php echo getCheck($IsGeneralContractor); ?> ></td><td><?php echo _GENER_CONT_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="produttore" name="produttore" value="Y" <?php echo getCheck($IsProduttore); ?> ></td><td><?php echo _PRODUTTORE_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="distributore" name="distributore" value="Y" <?php echo getCheck($IsDistributore); ?> ></td><td><?php echo _DISTRIBUTORE_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="IsPrestatoreDiServizi" name="IsPrestatoreDiServizi" value="Y" <?php echo getCheck($IsPrestatoreDiServizi); ?> ></td><td><?php echo _IsPrestatoreDiServizi_; ?></td></tr></table>
            </div>

            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _R_RETE_IMP_R_; ?></span><br>
                <table><tr><td valign="top"><input type="checkbox" id="retiImpresa" name="retiImpresa" value="Y" <?php echo getCheck($IsRetiImpresa); ?> ></td><td><?php echo _APP_RETE_IMP_; ?></td></tr>
                </table>
                <input type="text" id = "rete" name = "rete" maxlength = "255" value = "<?php echo $rete; ?>"/><br />
            </div>

            <div class="boxParametro">
                <a draggable="false" href="javascript:aggiungiParametri('lingue')" class="tooltip">+ <?php echo _CON_LINGUE_; ?><!-- <span><?php //echo _TEXT_INFO_CON_LINGUE_;?></span> --></a>
                <span class="textAdd" id="lingue">

                    <?php
                    $parametri = _R_N_PARAMETRI_ . "<br>";
                    $i = 0;
                    $tagLigue = "Lingua";
                    if (strcmp($Lang, "IT") != 0) {
                        $tagLigue .= "_" . $Lang;
                    }
                    $query = "SELECT $tagLigue, Id FROM EXPO_Tlk_Lingue  ORDER BY $tagLigue";
                    foreach ($db->GetRows($query) AS $rows) {
                        if (isset($_POST['lingue' . $rows['Id']])) {
                            echo '<input type ="hidden" name="lingue' . $rows['Id'] . '" id = "lingue' . $rows['Id'] . '" value = "Y"/>';
                            $parametri .= $rows[$tagLigue] . "<br>";
                            $i++;
                        }
                    }

                    echo '<a draggable="false" href="javascript:aggiungiParametri(\'lingue\')">(' . $i . ')</a>';
                    if ($i > 0) {
                        ?>

                        <a href="javascript:aggiungiParametri('lingue')" class="tooltip" draggable="false" >
                            <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                            <span> <?php echo $parametri; ?></span>
                        </a>
                    <?php } ?>
                </span>
            </div>

            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _REFERENZE_; ?></span><br>
                <table><tr><td valign="top"><input type="checkbox" id="partecipazioneExpo" name="partecipazioneExpo" value="Y" <?php echo getCheck($IsPartecipazioneExpo); ?> ></td><td><?php echo _PARTECIPZIONI_EXPO_; ?></td></tr>
                <tr><td class="inputCheck"><input type="checkbox" id="partecipazioniInternazionali" name="partecipazioniInternazionali" value="Y" <?php echo getCheck($IsPartecipazioniInternazionali); ?> ></td><td><?php echo _PAR_INTERNAZIONALI_; ?></td></tr></table>
            </div>

            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _INTERNAZIONALIZZAZIONE_; ?></span><br>
                <table><tr><td valign="top"><input type="checkbox" id="sedEstera" name="sedEstera" value="Y" <?php echo getCheck($IsSediEstere); ?> ></td><td><?php echo _S_ESTERA_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="netetwork" name="netetwork" value="Y" <?php echo getCheck($IsNetwork); ?> ></td><td><?php echo _NET_INTER_; ?></td></tr></table>
            </div>


            <div class="boxParametro">
                <span class="titoloParametro"><?php echo _CARATTERISTICHE_; ?></span><br>
                <table><tr><td valign="top"><input type="checkbox" id="aModello" name="aModello" value="Y" <?php echo getCheck($IsModello); ?> ></td><td><?php echo _A_MODELLO_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="pMasetr" name="pMaster" value="Y" <?php echo getCheck($IsMasterSpecialistico); ?> ></td><td><?php echo _P_MASTER_R_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="IsCertificazione" name="IsCertificazione" value="Y" <?php echo getCheck($IsCertificazione); ?> ></td><td><?php echo _P_CERTIFICAZIONE_; ?></td></tr>
                <tr><td valign="top"><input type="checkbox" id="IsIscrSiexpo" name="IsIscrSiexpo" value="Y" <?php echo getCheck($IsSiexpo2015); ?> ></td><td><?php echo _P_SIEXPO_; ?></td></tr>
                </table>
            </div>

            <div class="boxParametro">
                <a draggable="false" href="javascript:aggiungiParametri('certificazioni')" class="tooltip">+ <?php echo _CERTIFICAZIONI2_; ?><!-- <span><?php //echo _TEXT_INFO_CERTIFICAZIONI2_;?></span> --></a>
                <span class="textAdd" id="certificazioni">
                    <?php
                    $i = 0;
                    $parametri = _R_N_PARAMETRI_ . "<br>";

                    $query = "SELECT DISTINCT Id,IDCert FROM EXPO_Tlk_Certificazioni ORDER BY IDCert";
                    foreach ($db->GetRows($query) AS $rows) {
                        if (isset($_POST['certificazioni' . $rows['Id']])) {
                            echo '<input type ="hidden" name="certificazioni' . $rows['Id'] . '" id = "certificazioni' . $rows['Id'] . '" value = "Y"/>';
                            $parametri .= $rows['IDCert'] . "<br>";
                            $i++;
                        }
                    }

                    echo '<a draggable="false" href="javascript:aggiungiParametri(\'certificazioni\')">(' . $i . ')</a>';
                    if ($i > 0) {
                        ?>

                        <a href="javascript:aggiungiParametri('certificazioni')" class="tooltip" draggable="false" >
                            <img src="/tmpl/expo2015VT/vetrina/images/bullets.png" />
                            <span> <?php echo $parametri; ?></span>
                        </a>
                    <?php } ?>
                </span>
            </div>


        </div>
		

        <div id="risultoRicerca">

            <div id="requesterTable">
                <?php
                if (strcmp($azione, "SRC") == 0) {
                    if (strcmp($SQLJoinCategorie, "") == 0 && strcmp($SQLJoinLingue, "") == 0 && strcmp($SQLJoinAssociazioni, "") == 0 && strcmp($SQLJoinCertificazione, "") == 0 && strcmp($SQLJoinReti, "") == 0 && strcmp($SQLWhereCondition, "") == 0 && strcmp($cercaHtdig, "") == 0) {
                        ?>
                        <h2><?php echo _NO_PAR_RICERCA_; ?></h2>
                        <?php
                    } else {

						$idRicercaPadre = settaVar($_REQUEST, 'idRicerca', 0);						

						$ragioneSociale = mysql_escape_string($impresa);
						
						$siglaNazioneSQL ="Select Sigla_Nazione from T_Anagrafica where Id=$IdPartecipante";
						$siglaNazione = $db->GetRow($siglaNazioneSQL,'Sigla_Nazione',null, 'estrazione sigla nazione utente visualizza vetrina ln 442');

						
						if (strcmp($cercaHtdig,"")!=0){
							$keywords = "'$cercaHtdig'";
						} else {
							$keywords = 'null';
						}	
						
						
						
						$ricercaSQL = "INSERT INTO EXPO_T_Ricerche
						(IdRicerca,IdPartecipante,SiglaNazione,IdRicercaPadre,DataRicerca,Keywords,
						RagioneSociale,NumeroDipendenti,UltimoFatturato,IsSediEstere,IsNetwork,
						IsGeneralContractor,IsProduttore,IsDistributore,IsPrestatoreDiServizi,
						IsPartecipazioneExpo,IsPartecipazioniInternazionali,IsSindacale,IsModello,
						IsRetingLegalita,IsMasterSpecialistico,IsCertificazione,IsSiexpo2015,IsRetiImpresa,NomeRete)
						
						VALUES
						((SELECT @Id := COALESCE(MAX(R.IdRicerca),0)+1 AS MAX FROM EXPO_T_Ricerche as R),$IdPartecipante,'$siglaNazione',$idRicercaPadre,NOW(), $keywords,
						'$ragioneSociale','$numeroD','$fatturato','$IsSediEstere','$IsNetwork',
						'$IsGeneralContractor','$IsProduttore','$IsDistributore','$IsPrestatoreDiServizi',
						'$IsPartecipazioneExpo','$IsPartecipazioniInternazionali','$IsSindacale','$IsModello',
						'$IsRetingLegalita','$IsMasterSpecialistico','$IsCertificazione','$IsSiexpo2015','$IsRetiImpresa','$rete'
						)";
						
						$db->Query($ricercaSQL,null,'log ricerca visualizza ricerca Ln.455');
						
						$IdRicerca = $db->GetRow('Select @Id as Id','Id');
						
						//$IdRicercaSQL = "SELECT COALESCE(MAX(R.IdRicerca),0) AS IdRicerca FROM EXPO_T_Ricerche as R WHERE IdPartecipante = $uid";
						//$IdRicerca = $db->GetRow($IdRicercaSQL, 'IdRicerca', null,'estrazione max id ricerca ln. 436');
						
						print '<input type="hidden" id="idRicerca" name="idRicerca" value="'.$IdRicerca.'"/>';
				
						foreach ($ricercheCategorie as $keyCat => $ricercaCategoria){
							$ricercaCategorieSQL = "INSERT INTO EXPO_TJ_Ricerche_Categorie
													VALUES ((SELECT COALESCE(MAX(R.Id),0)+1 FROM EXPO_TJ_Ricerche_Categorie as R),$IdRicerca,$ricercaCategoria)";
	
							$db->Query($ricercaCategorieSQL,null,'log ricerca visualizza ricerca Ln.464');
						}
						
						foreach ($ricercheLingue as $keyLin => $ricercaLingua){
							$ricercaLingueSQL = "INSERT INTO EXPO_TJ_Ricerche_Lingue
							VALUES ((SELECT COALESCE(MAX(R.Id),0)+1 FROM EXPO_TJ_Ricerche_Lingue as R),$IdRicerca,$ricercaLingua)";
						
							$db->Query($ricercaLingueSQL,null,'log ricerca visualizza ricerca Ln.472');
							
						}
						
						
						foreach ($ricercheCertificazioni as $keyCert => $ricercaCertificazione){
							$ricercaCertificazioniSQL = "INSERT INTO EXPO_TJ_Ricerche_Certificazioni
							VALUES ((SELECT COALESCE(MAX(R.Id),0)+1 FROM EXPO_TJ_Ricerche_Certificazioni as R),$IdRicerca,$ricercaCertificazione)";
						
							$db->Query($ricercaCertificazioniSQL,null,'log ricerca visualizza ricerca Ln.481');
						}
						
                        if ($db->NumRows($SQL) == 0) {
                            ?>
                            <div id="contenitoreTesta">
                                <div id="pCercaGenerale">
                                </div>
                                <div id="botSvuta">
                                    <a  href="javascript:svcerca()" draggable="false"><?php echo _SVUTA_RICERCA_; ?></a>
                                </div>
                            </div>
                            <h2><?php echo _NO_RICERCA_; ?></h2>
                        <?php } else { ?>
                            <div id="contenitoreTesta">
                                <div id="pCercaGenerale">
                                <script language="JavaScript" type="text/javascript">
								
									$(document).ready(function() {
								
										$('#cercaHtdig').keypress(function(e) {
									        if (e.which == 13) {
									        	cerca('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&azione=SRC');
									        }
									    });
								
									});
								    
							    </script>
                                    <!-- <span id="cercaGenerale"><?php echo strtoupper(_RICERCA_TEXT_); ?> </span>-->
                                    <div id="form-ricerca">
                                    <input type="text" id="cercaHtdig" name="cercaHtdig" size="35" maxlength="255" placeholder="<?php echo strtoupper(_RICERCA_TEXT_); ?>" value="<?php echo $cercaHtdig; ?>"/>
                                    <a href="javascript:cerca('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=SRC')" draggable="false"><img src="images/expo2015/lente.png" height ="16px" width="16px" draggable="false" style=" margin: auto; vertical-align: middle"/></a>
                                    </div>
                                </div>
                                <div id="botSvuta">
                                    <a  href="javascript:svcerca()" draggable="false"><?php echo _SVUTA_RICERCA_; ?></a>
                                </div>
                            </div>
                            <?php
                            
                            $arrayRighe = array();
							
                            //<input type="checkbox" id="all" name="all"  value="Y">
                            $rTitolo = '<tr>
                                <th colspan="2" scope="col" class="tdHeaderAll">

								<ul id="tooltipSelect">
								<li><input type="checkbox" id="all" name="all"  value="Y"><img src="images/expo2015/freccia_giu_grigio.png"/>
									<ul class="selectBox">
										<li><input class="selectBox" type="button" value="'._TUTTE_.'" onclick="selezionaTutti()" /></li>
										<li><input class="selectBox" type="button" value="'._NESSUNA_.'" onclick="delesezionaTutti()" /></li>
										<li><input class="selectBox" type="button" value="'._INVERTI_.'" onclick="invertiSelezione()" /></li>
									</ul>
									<div class="clear"></div>
								</li>
								</ul>
								
								</th>
								<th colspan="2" scope="col" class="tdHeaderRicLeft">' . _RAGIONESOCILE_ . '</th>
								<th width="10%" scope="col" class="tdHeaderRic">' . _ADDETTI_ . '</th>
                               	<th width="20%" scope="col" class="tdHeaderRicLeft" >' . _SEDE_LEG_ . '</th>
                                <th width="18%" scope="col" class="tdHeaderRicRight" >' . _FATTURATO_5_ . '<a href="#" class="tooltip" draggable="false" >
                    <img src="/tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                    <span>'. _TEXT_INFO_P_ .'</span>
                </a><br />' . _R_U_FATTURATO_2_ .' 
               </th>
                            </tr>';




                            $isColor = 0;
                            $idR = 0;
                            $arrayQuery = $db->GetRows($SQL);
							
                            //ORDINAMENTO $db->NumRows($SQL)
                            $algoritmoOrdinamento = new Ordinamento();
                            $arrayIndici = $algoritmoOrdinamento->main(count($arrayQuery),$Abilitazione_Ordinamento);
                            $elementiVisualizzati = 0;
                            print "<div id=\"risultatiTrovati\">". count($arrayIndici)." "._RISULTATI_TROVATI_."</div>";
                            foreach ($arrayIndici AS $ind) {
                                $rows = $arrayQuery[$ind - 1];
                                /*print "+++++++++++++++++++++++++++<pre>";
                                print_r($rows);
                                print "</pre>";*/
                                $idImp = $rows['IdImpresa'];
                                
                                $db->Query("UPDATE EXPO_T_Imprese SET PresenzaTotaleRicerche = PresenzaTotaleRicerche+1 WHERE Id = '$idImp'");
                                
                                $PrimePosizioni='N';
                                if($elementiVisualizzati < 40) {
                                
                                    $db->Query("UPDATE EXPO_T_Imprese SET PresenzaPrimePosizioni = PresenzaPrimePosizioni+1 WHERE Id = '$idImp'");
                                    $PrimePosizioni='Y';
                                }
                                
                                $esitiSQl="INSERT INTO EXPO_T_Esiti (IdEsito,IdRicerca,IdImpresa,PrimePosizioni) VALUES
											((SELECT @IdEsito := COALESCE(MAX(E.IdEsito),0)+1 FROM EXPO_T_Esiti as E),$IdRicerca,$idImp,'$PrimePosizioni')";
                                $db->Query($esitiSQl,null, 'inserimento esiti ricerca ln 596');
                               
                               	$idEsito = $db->GetRow("SELECT @IdEsito",'@IdEsito');
                                
                                $elementiVisualizzati ++;
                                        
                                $qProd = "SELECT Id,Nome FROM EXPO_T_Prodotti WHERE IdImpresa = '$idImp' AND StatoProdotto = 'A' ORDER BY Nome";

                                $tabImpresa = "";
                                $idPref = $db->GetRow("SELECT * FROM EXPO_T_Imprese_Preferiti WHERE IdPartecipante = '$IdPartecipante' AND IdImpresa = '$idImp'", 'Id');
                                $tag = "preferiti_stella_accesa";
                                if (strcmp($idPref, "") == 0) {
                                    $tag = "preferiti_stella_spenta";
                                }
                                $tabImpresa .= '<tr ' . getColor("lineTable", $isColor) . '>
                            
<td width="1%">
<input type="checkbox" id="richiestaImp" name="richiestaImp" value="' . $idImp . '"><input type="hidden" id="esitoIMP'.$idImp.'" name="esitoIMP'.$idImp.'" value="'.$idEsito.'"/>
</td><td width="1%">
                                        <a draggable="false" href="javascript:selectPreferiti(\'preferito' . $idImp . '\',\'' . $idImp . '\',\'' . $IdPartecipante . '\')" id="preferito' . $idImp . '"><img draggable="false" src="images/expo2015/' . $tag . '.png"></a>
</td><td >';
                                
                                $tabImpresa .= '<a draggable="false" href="javascript:visualizzaBOX(\'IMP\',\'' . $idImp . '\')" >' . $rows['RagioneSociale'] . '</a></td><td width="1%">';
                                
                                if ($db->NumRows($qProd) > 0)
                                    $tabImpresa .= '<a draggable="false" href="javascript:visualizzaProdotti(\'' . $idImp . '\')" ><img draggable="false" id="img' . $idImp . '" src="/images/expo2015/piu.png" width="11px" /></a>';

                                
                                
$tabImpresa .='</td>                                    
                                </td>
                                <td style="text-align: center;" >   
                                ' . $rows['NumeroDipendenti'] . '
                            </td>
                             <td style="text-align: left;" >
                               ' . $rows['Citta'] . ' (' . $rows['Pr'] . ')
                            </td>
                            <td style="text-align: right" >
                                ' . number_format(doubleval($rows['UltimoFatturato']/1000), 0, ',', '.') . '
                            </td>
                        </tr>';
                                
                                $tabProdotti = "";
                                if ($db->NumRows($qProd) > 0) {
                                    $tabProdotti .= '<tr class="subElencoHidden" id="tr' . $idImp . '">
                                    <td colspan="6">
                            <table  width="100%">';
                                    $numProd = 0;
                                    $isColorSub = 0;

                                    foreach ($db->GetRows($qProd) AS $rowsProd) {
                                        $idPro = $rowsProd['Id'];
                                        $nomeProdotto = $rowsProd['Nome'];
                                        if (strcmp($Lang, "IT") != 0) {
                                            $nomeProdotto = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = 'nome' AND Lang = '$Lang' AND  IdTipo = '" . ("P" . $idPro) . "'", "Testo");
                                        }

                                        $tabProdotti .= '<tr ' . getColor("lineTable", $isColorSub) . '>
                        <td width ="1%">
                        <input type="checkbox" id="richiestaProd" name="richiestaProd" value="' . $idPro . '"><input type="hidden" id="esitoPRT'.$idPro.'" name="esitoPRT'.$idPro.'" value="'.$idEsito.'"/>
						</td><td>
                        <a draggable="false" href="javascript:visualizzaBOX(\'PRT\',\'' . $idPro . '\')" >' . $nomeProdotto . '</a>
                            </td>
                                
                        </tr>';
                                        $numProd++;
                                        $isColorSub++;
                                    }
                                    $tabProdotti .='</table></td></tr>';

                                   
                                }
                                $arrayRighe[] = $tabImpresa . $tabProdotti;
                                $idR++;
                                $isColor++;
                            }
                            $evento = "javascript:cerca('index.phtml?Id_VMenu=" . $Id_VMenu . "&amp;azione=SRC')";
                            $nElementi = 20;
                            $html = multiPage($rTitolo, $arrayRighe, $nElementi, $evento, false);
                        }

                        if (strcmp($html, "") != 0) {
                            echo $html;
                            echo '<a class="buttonsLista" draggable="false" href="javascript:selezioneRichiesta()">' . _RICHIESTA_R_ . '</a>';
                        }
                    }
                }
                ?>
            </div>
        </div>
        

    </div>
</form>

<div id="coprente3" >
    <div id="box_popup3"></div>
</div>

<div id="coprente" >
    <div id="box_popup"></div>
</div>

<div id="coprente2" >
    <div id="box_popup2"></div>
</div>

