<?
if ($utype<=2)
	include $PROGETTO."/visualizza.inc";
else
{
	$db_PAVIA = new DB_CedCamCMS;

	$SQL="SET lc_time_names = 'it_IT'";
	$db_PAVIA->query($SQL);

	if ($edi_cate_codice=="") $edi_cate_codice=1;
	echo "<ul style=\"list-style-type:none;\">\n";
	$SQL="select *,date_format(edi_data,'%M') as mese,date_format(edi_data,'%Y') as anno from editoria, categoria_editoria where editoria.edi_cate_codice=categoria_editoria.cate_codice and editoria.edi_cate_codice=".$edi_cate_codice." order by edi_data desc";
	$db_PAVIA->query($SQL);
	$estratti=$db_PAVIA->num_rows();

	$page_tot=ceil($estratti/$RESFORPAGE);
	if($p>$page_tot)
	{
		$p=0;
		$page_elenchi=$p;
		$sess->register(page_elenchi);		
	}
	if($p==0)
	{
		$p=1;
		$lim_inf=0;
	}
	else
		$lim_inf=$RESFORPAGE*($p-1);

	$SQL.=" limit ".$lim_inf.",".$RESFORPAGE;

	include $PROGETTO."/paginazione.inc";

	$db_PAVIA->query($SQL);
	if($db_PAVIA->num_rows()>0)
		while($db_PAVIA->next_record())
		{
				$cate_nome=$db_PAVIA->f("cate_nome");
				$edi_titolo=$db_PAVIA->f("edi_titolo");


				$edi_file=$db_PAVIA->f("edi_file");

				$punto=strrpos($edi_file,".");
				if($punto===false)
					$formato="sconosciuto";
				else
					$formato=substr($edi_file,$punto+1);
				$numbyte=formatbytes(filesize($PATHDOCS."/files/Editoria/".$edi_file)); 

				if ($edi_file!="")
					$edi_file2="<a href=\"/files/Editoria/".$edi_file."\" alt=\"Scarica il file in formato ".$formato." ".$numbyte."\" title=\"Scarica il file in formato ".$formato." ".$numbyte."\">".$edi_file."</a>";
				else
					$edi_file2="";

				$edi_data=$db_PAVIA->f("edi_data");
				$edi_formato_data=$db_PAVIA->f("edi_formato_data");
				
				$edi_data2="";
				if ($edi_formato_data==1)
					$edi_data2=ucfirst(substr($db_PAVIA->f("mese"),0,3))." ";
					
				$edi_data2.=$db_PAVIA->f("anno");
				
				echo "<li>[".$edi_data2."] ".$edi_titolo." ".$edi_file2."</li>\n";
		}
	echo "</ul>\n";

}
?>