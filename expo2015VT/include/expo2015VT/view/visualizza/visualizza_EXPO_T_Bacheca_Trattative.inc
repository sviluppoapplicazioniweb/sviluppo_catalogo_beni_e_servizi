<form name="form_moduli"  method ="post" id="form_moduli" >
    <input type="hidden" id="_CAMPI_OB_" name="_CAMPI_OB_" value="<?php echo _CAMPI_OB_; ?>" />
    <input type="hidden" id="IdLog" name="IdLog" value="<?php echo $auth->auth["uid"]; ?>" />
    <?php
    //require_once($PROGETTO . "/view/lib/db.class.php");
    include_once "send_mail.inc";
    $db = new DataBase();

    $IdPartecipante = $auth->auth["uid"];
    
    if (strcmp($azione, "CLS") == 0 || strcmp($azione, "VIEW") == 0) {

        $Id = str_replace("Id", "", base64_decode(settaVar($_GET, 'Id', '')));
        $query = "SELECT * FROM EXPO_T_Bacheca WHERE Id = '$Id' AND IdPartecipante = '$IdPartecipante'";
        if (strcmp($db->GetRow($query, "Id",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.14"), "") == 0) {
            redirect("index.phtml?Id_VMenu=319" );
            exit;
        }

        $query = "SELECT * FROM EXPO_T_Bacheca WHERE id = '$Id'";
		
        $val = $db->GetRow($query,null,null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.21");
        
        $IdPartecipante = $val["IdPartecipante"];
        $IdImpresa = $val["IdImpresa"];
        $IdProdotto = $val["IdProdotto"];

        
        ?>

        <input type="hidden" id="Id" name="Id" value="<?php echo $Id; ?>" />
       
        <table  > 
            <tr>

                <td class="tdText"> <?php echo _PARTECIPANTE_; ?>:</td>
                <td class="tdText2"><?php echo $db->GetRow('SELECT * FROM T_Anagrafica WHERE Id = ' . $IdPartecipante, 'RagSoc',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.34"); ?></td>
                <td class="tdText"> <?php echo _FORNITORE_; ?>:</td>
                <td class="tdText2"><?php echo $db->GetRow('SELECT * FROM EXPO_T_Imprese WHERE Id = ' . $IdImpresa, 'RagioneSociale',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.36"); ?></td>
            </tr>
            <tr>
                <td class="tdTextLine2T" colspan="2">
                    <?php echo _VALORE_; ?>:
                    <?php
                    if (strcmp($azione, "CLS") == 0) {
                        ?>
                        *<input type = "text" id = "valore" name = "valore" size="15" maxlength ="15" value = ""/>
                    <?php } else { ?>
                        <?php echo number_format($db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$Id'", 'Valore',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.46"), 2, ',', '.'); ?>
                    <?php } ?>
                    &euro;
                </td>
                <td colspan="2" class="tdTextLine2T">
                    <?php
                    $query = "SELECT DISTINCT Nome
                                  FROM EXPO_T_Prodotti AS T
                                  RIGHT JOIN EXPO_TJ_Bacheca_Prodotti AS TJ ON T.Id = TJ.IdProdotto
                                WHERE TJ.IdBacheca = '$Id'
                                ORDER BY Nome";
                    if ($db->NumRows($query,null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.57") > 0) {
                        ?>
                        <?php echo _PRODOTTO_; ?>:

                        <ul>  
                            <?php
                            foreach ($db->GetRows($query,null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.63") AS $rows) {
                                ?>
                                <li><?php echo $rows ['Nome']; ?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </td>
            </tr>
            <?php
            if (strcmp($azione, "CLS") != 0) {
                ?>
                <tr>
                    <td colspan="4"><br></td>        
                </tr>
                <tr>
                    <td colspan="4" class="tdText">
                        <?php echo _OSSERVAZIONI_; ?>
                        <a href="#" class="tooltip">
                            <img src="tmpl/expo2015VT/vetrina/images/infoGrey.png" />
                            <span>
                                <img class="callout" src="tmpl/expo2015VT/vetrina/images/callout.gif" />
                                <?php echo _NOTE_5_; ?>
                            </span>
                        </a>
                    </td>        
                </tr>
                <tr>
                    <td colspan="4" id="tdTexArea"><textarea rows="12" cols="45" id="testo" name="testo"></textarea></td>        
                </tr>


            <?php } ?>
            <tr>
                <td colspan="4"><br></td>        
            </tr>
            <tr>
                <td colspan="4" id="tdButton">
                    <?php if (strcmp($azione, "VIEW") == 0) { ?>
                        <a class="buttonsLista" draggable="false" href="javascript:salva('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=C','1')"><?php echo _CONFERMA_; ?></a>
                        <a class="buttonsLista" draggable="false" href="javascript:salva('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=D','2')"><?php echo _DINIEGA_; ?></a>
                    <?php } else { ?>
                        <a class="buttonsLista" draggable="false" href="javascript:salva('index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&amp;azione=SEND','0')"><?php echo _INVIA_; ?></a>
                    <?php } ?>
                </td>        
            </tr>
        </table>


        <?php
    } else {
        if (strcmp($azione, "SEND") == 0) {
            $idBacheca = settaVar($_POST, "Id", "");
            $valore = str_replace(",00", "", str_replace(".", "", settaVar($_POST, "valore", "0")));


            $id = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$idBacheca'", "Id",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.118");
            if (strcmp($id, "") == 0) {
                
                $idUtente = $auth->auth["uid"];
                $db->Query("INSERT INTO EXPO_T_Bacheca_Trattative VALUES((SELECT @id:= COALESCE(TBT.MAX(Id),0)+1 AS Id FROM EXPO_T_Bacheca_Trattative AS TBT),'$idBacheca','$idUtente','$valore',NOW(),'A','0')",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.122");
                $id = $db->GetRow("SELECT @id AS id", "id");
            } else {
                //$id = settaVar($_GET, "Id", "");
                $idUtente = $auth->auth["uid"];
                $db->Query("UPDATE EXPO_T_Bacheca_Trattative SET IdChiusura = $idUtente,Valore = '$valore',Stato = 'A' WHERE IdBacheca = $idBacheca",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.126");
            }
            
            //INVIO MAIL APERTURA
            $idTrattativa = $id . "." . $idBacheca;
            $query = "SELECT * FROM EXPO_T_Bacheca WHERE Id = '$idBacheca'";
            $oggetto = $db->GetRow($query, "Oggetto",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.132");
            //destintario
            $idImp = $db->GetRow($query, "IdImpresa",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.134");
            $mail = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImp'", "Email",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.135");

            $Prodotto = "";
            $Impresa = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImp'", "RagioneSociale",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.138");

            $mittente = $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$uid'", "Nome",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.140") . " " . $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$uid'", "Cognome",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.140");
            send_mail($mail, $MAILSITO, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _OGG_NOT_C_TRA_P_), preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NOT_C_TRA_P_), true);
        } elseif (strcmp($azione, "D") == 0 || strcmp($azione, "C") == 0) {

            $idBacheca = settaVar($_POST, "Id", "");
			
            include_once "send_mail.inc";
            $idTrattativa = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$id'", "Id",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.147") . "." . $id;
           	$query = "SELECT * FROM EXPO_T_Bacheca WHERE Id = '$idBacheca'";
            $oggetto = $db->GetRow($query, "Oggetto",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.149");
            //destintario
            $idImp = $db->GetRow($query, "IdImpresa",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.151");
            $mail = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImp'", "Email",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.152");
            $idProd = $db->GetRow($query, "IdProdotto",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.153");
            $Prodotto = $db->GetRow("SELECT * FROM EXPO_T_Prodotti WHERE Id = '$idProd'", "Nome",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.154");
            $Impresa = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImp'", "RagioneSociale",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.155");
            $mittente = $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$uid'", "Nome",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.156") . " " . $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id = '$uid'", "Cognome",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.157");

            $idMsg = 0;
            
            $testo = settaVar($_POST, "testo", "");
            if (strcmp($testo, "")!=0){
	            
	            $db->Query("INSERT INTO EXPO_T_Bacheca_Messaggi VALUES((SELECT @idMsg:= COALESCE(MAX(BM.Id),0)+1 AS Id FROM EXPO_T_Bacheca_Messaggi AS BM),'$idBacheca','" . $uid . "',
	                    '2','" . $idImp . "','1','" . mysql_escape_string($testo) . "',NOW())",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.163");
	            $idMsg = $db->GetRow("SELECT @idMsg as idMsg","idMsg");
				//$db->Query("UPDATE EXPO_T_Bacheca_Trattative SET Stato = '$azione', IdMessaggio = '$idMsg', Data = NOW() WHERE IdBacheca = $idBacheca",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.166");
            }
            if (strcmp($azione, "D") == 0) {
                
                send_mail($mail, $MAILSITO, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _OGG_NOT_CD_TRA_P_), preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NOT_CD_TRA_P_), true);
            } else {

                send_mail($mail, $MAILSITO, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _OGG_NOT_CC_TRA_P_), preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NOT_CC_TRA_P_), true);
            }
            
            	 //print "UPDATE EXPO_T_Bacheca_Trattative SET Stato = '$azione', IdMessaggio = '$idMsg', Data = NOW() WHERE IdBacheca = $idBacheca";
            $db->Query("UPDATE EXPO_T_Bacheca_Trattative SET Stato = '$azione', IdMessaggio = '$idMsg', Data = NOW() WHERE IdBacheca = $idBacheca",null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.170");

         
        }
        ?>
        <a class="buttonsLista" draggable="false" href="/index.phtml?Id_VMenu=311"><?php echo _EXTRA_B_; ?></a>
        <br>
        <?php
        $arrayRighe = array();
        $rTitolo = '<tr>
                <th width="30%" scope="col" class="tdHeaderRicLeft">' . _IMPRESA_ . '</th>
                <th width="20%" scope="col" class="tdHeaderRicLeft">' . _DATA_CHIUSURA_ . '</th>
                <th width="15%" scope="col" class="tdHeaderRicLeft">' . _VALORE_ . ' ( &euro; )</th>
                <th scope="col" width="15%" class="tdHeaderRicLeft">' . _STATO_ . '</th>
                    <th scope="col" class="tdHeaderRic">' . _AZIONE_ . '</th>
            </tr>';


        $isColor = 0;
        $query = "SELECT IdImpresa,IdBacheca ,TBT.Id AS IdT,Stato ,IdChiusura,
            CASE
            WHEN Stato = 'A' THEN 1
            WHEN Stato = 'D' THEN 2
            WHEN Stato = 'C' THEN 3
            END AS Ordinamento
        FROM EXPO_T_Bacheca AS T RIGHT JOIN EXPO_T_Bacheca_Trattative AS TBT
        ON T.Id = TBT.IdBacheca
        WHERE  IdPartecipante = '$IdPartecipante' 
                ORDER BY Ordinamento,TBT.Data DESC";
        if ($db->NumRows($query,null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.197") == 0) {
            ?>
            <h2><?php echo _NO_TRATTATIVA_; ?></h2>
            <?php
        } else {

            foreach ($db->GetRows($query,null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.203") AS $rows) {

                $IdImpresa = $rows['IdImpresa'];
                $IdBacheca = $rows['IdBacheca'];
                $html = '<tr ' . getColor("lineTable", $isColor) . '>';

                $html .= '<td><a draggable="false" href="javascript:visualizzaBOX(\'IMP\',\'' . $IdImpresa . '\')" >' . $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", 'RagioneSociale',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.209") . '</a></td>
                        <td>' . $db->GetRow("SELECT DATE_FORMAT( Data,  '%d-%m-%Y %H:%i:%s' ) AS Data FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$IdBacheca'", 'Data',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.210") . '</td>
                        <td>' . number_format($db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$IdBacheca'", 'Valore',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.211"), 2, ',', '.') . '</td>
                        <td >' . $statoTrattative[$db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$IdBacheca'", 'Stato',null,"visualizza_EXPO_T_Bacheca_Trattative.inc Ln.212")] . '</td>
                        <td align="center">';
                if (strcmp($rows['Stato'], "A") == 0 && $rows['IdChiusura'] != $auth->auth["uid"]) {
                    $html .= '<a class="buttonsLista" draggable="false" href="index.phtml?Id_VMenu=' . $Id_VMenu . '&amp;azione=VIEW&Id=' . base64_encode('Id' . $IdBacheca) . '">' . _RISPONDI_ . '</a>';
                } else {
                    $html .= '<a class="buttonsLista" draggable="false" href="javascript:visualizza(\'' . $IdBacheca . '\')">' . _VISUALIZZA_ . '</a>';
                }
                $html .= '</td>
                    </tr>';
                $arrayRighe[] = $html;
                $isColor++;
            }
            $evento = "javascript:visualizza('index.phtml?Id_VMenu=" . $Id_VMenu . "')";
            $nElementi = settaVar($_POST, "elementiN", "25");
            echo multiPage($rTitolo, $arrayRighe, $nElementi, $evento);
        }
    }
    ?>
</form>

<div id="coprente" >
    <div id="box_popup"></div>
</div>

<div id="coprente2" >
    <div id="box_popup2"></div>
</div>