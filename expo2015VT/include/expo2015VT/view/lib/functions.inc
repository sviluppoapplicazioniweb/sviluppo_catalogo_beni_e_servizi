<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//SETTA LE VARIANBILI DEL FROM
function settaVar($type, $nome, $defoult) {
    if (isset($type[$nome])) {
        return $type[$nome];
    } else {
        return $defoult;
    }
}

//GESTIONE CHECKED
function getCheck($value) {
    if (strcmp($value, "Y") == 0) {
        return ' checked="checked" ';
    }
    return '';
}

//METTE LE IMMAGINI SUL I CHECK SELEZINATI O NO
function getCheckImg($value) {
    $img = '<img src="images/expo2015/';
    if (strcmp($value, "Y") == 0) {
        $img .= 'ok.png"';//'ico_check.jpg"';
    } else {
        $img .= 'ko.png"';//'ico_delete.jpg"';
    }
    $img .= ' class="checkImg" draggable="false" /> ';
	
    return $img;
}

function getLinkWeb($value) {
    if (strpos($value, "http://") === false) {
        $value = "http://" . $value;
    }
    return $value;
}

//VISUALIZZA LE IMMAGINI E LINK A FILE E VIDEO
function getAllegati($tipo, $value) {
    $tag = "";
    if (strcmp($tipo, 'file') == 0) {
        //return 'files/' . $value . '.pdf';
        if (is_file(Config::ROOTFILES . $value . '.pdf')) {
            $tag = '<b>' . _ALLEGA_DOC_ . ':</b> <a draggable="false" href="' . Config::ROOTFILESHTML . $value . '.pdf" target="_blank" >' . substr($value, strrpos($value, '/') + 1) . '.pdf</a>';
        }
    } elseif (strcmp($tipo, 'link') == 0 && strcmp($value, '') != 0) {
    	$value = urldecode($value);
        if (strpos($value, "http://") === false) {
            $value = "http://" . $value;
        }
        $tag = '<b>' . _ALLEGA_LINK_ . ':</b> <a draggable="false" href="' . $value . '" target="_blank" >' . $value . '</a>';
    }
    return $tag;
}

function getAllegatiFiles($tipo, $value, $text) {
    $tag = "";
    if (strcmp($tipo, 'file') == 0) {
        //return 'files/' . $value . '.pdf';
        if (is_file(Config::ROOTFILES . $value . '.pdf')) {
            $tag = '<b>' . _ALLEGA_DOC_ . ':</b> <a draggable="false" href="' . Config::ROOTFILESHTML . $value . '.pdf" target="_blank" >' . $text . '.pdf</a><br>';
        }
    } elseif (strcmp($tipo, 'link') == 0 && strcmp($value, '') != 0) {
        if (strpos($value, "http://") === false) {
            $value = "http://" . $value;
        }
        $tag = '<b>' . _ALLEGA_LINK_ . ':</b> <a draggable="false" href="' . $value . '" target="_blank" >' . $value . '</a><br>';
    }

    return $tag;
}

//RITORNA SE SELECT
function getSelected($value, $r) {
    if (strcmp($value, $r) == 0) {
        return ' selected ';
    }
    return '';
}

//BLOCCO DEI CAMPI TELEMACO
function getCheckHiddenTelemaco($value) {
    if (strcmp($value, "Y") == 0) {
        return ' disabled="disabled" ';
    }
    return '';
}

//BLOCCO DEI CAMPI
function getCheckHidden($value) {
    if (strcmp($value, "N") == 0) {
        return ' disabled="disabled" ';
    }
    return '';
}

//COLORE PER LE RIGHE ALTERNATE IN TAB
function getColor($classe, $i) {

    if ($i % 2 == 1){
        return ' class="' . $classe . '" ';
    }
    else{
        return '';
    }
}

//VISALIZZA IMG LOGO
function viewLogo($getIdImpresa,$website) {
    $img = "";

    $img = '<img id="logoImgSrc" draggable="false" src="getImageLogo.php?getIdImpresa=' . $getIdImpresa . '" />';

    if (strcasecmp($website, "") != 0){
    	$img = '<a href="'.getLinkWeb($website).'" target="_blank">'.$img.'</a>';
    }
    
    
    return $img;
}





//SETTA LA QUERY DI RICERCA
function setSQLW($tab, $campo, $value) {
    if (strcmp($value, 'Y') == 0) {
        return "AND $tab.$campo = '$value' ";
    }
    return '';
}

//SETTA LA QUERY DI RICERCA PER CAMPI TESTO
function setTextSQLW($tab, $campo, $value) {
    if (strcmp($value, '') != 0) {
        return "AND $tab.$campo LIKE '" . mysql_escape_string(htmlspecialchars($value)) . "%' ";
    }
    return '';
}

//SETTA LA QUERY DI RICERCA PER CAMPI NUMERO
function setNumSQLW($tab, $campo, $value) {
    if (strcmp($value, "") != 0) {
        list($v1, $v2 ) = explode("|", $value);
        if (strcmp($v2, '') != 0) {
            return " AND $tab.$campo >= $v1 AND $tab.$campo <= $v2 ";
        } else {
            return " AND $tab.$campo >= $v1 ";
        }
    }
    return '';
}

//SETTA LA QUERY DI RICERCA PER CAMPI NUMERO
function setNumFattSQLW($tab, $campo, $value) {
    if (strcmp($value, "") != 0) {
        list($v1, $v2 ) = explode("|", $value);
        $v1 = $v1 . "000";
        $v2 = $v2 . "000";
        if (strcmp($v2, '000') != 0) {
            return " AND $tab.$campo >= $v1 AND $tab.$campo <= $v2 ";
        } else {
            return " AND $tab.$campo >= $v1 ";
        }
    }
    return '';
}

//VISUALIZZAZIONE TAB PRODOTTI
function viewTabProd($testo, $allegati) {


    if (strcmp($testo, "") != 0 || strcmp($allegati[0], "") != 0 || strcmp($allegati[1], "") != 0)
        return true;

    return false;
}

//TABLE MATR
function ceateTabMatr($arryTab, $maxCol, $maxRighe) {


    $wCol = floor(100 / ($maxCol + 1));

    $html = '<table width="100%">';
    $iRiga = 0;

    while ($iRiga < $maxRighe) {
        $html .= "<tr>";
        $iCol = 0;
        while ($iCol <= $maxCol) {
            $html .= '<td width="' . $wCol . '%">' . $arryTab[$iCol][$iRiga] . '</td>';
            $iCol ++;
        }
        $iRiga++;
        $html .= "</tr>";
    }
    $html.="</table>";

    return $html;
}

//CREATE TAB
function ceateTab($arryCol, $numCol) {
    $widthTd = floor(100 / ($numCol + 1));
    $html = '<table width="100%">';
    $iCol = 0;
    foreach ($arryCol AS $value) {
        if ($iCol == 0)
            $html .= "<tr>";
        elseif ($iCol == $numCol) {
            $html .= "</tr>";
            $iCol = 0;
        }

        $html .= '<td width="' . $widthTd . '%">' . $value . '</td>';
        $iCol++;
    }
    if ($iCol < $numCol) {
        for ($i = $iCol; $i < $numCol; $i++)
            $html .= "<td></td>";
        $html .= "</tr>";
    }


    $html.="</table>";
    return $html;
}

//DIVIDE IN PAGE IL RITORNO DEL QUERY
function multiPage($rTitolo, $arrayRighe, $numElementPag, $evento, $selezionaNumElementi = true) {
    $html = "";
    $numElemeti = count($arrayRighe);
    $multiPage = false;
    if (strcmp($numElementPag, _ALL_) != 0) {
        $numPage = ceil($numElemeti / $numElementPag);
        if ($numPage > 1) {
            $multiPage = true;
            $html .= '<script type="text/javascript" src="/jquery/js/virtualpaginate.js"></script>';
            $html .= '<div id="contenetPages" style="width: 100%;">';
        }
    }
      
    $i = 0;
    for ($p = 1; $p <= $numPage; $p++) {
        if ($multiPage) {
            if ($p > 1) {
                $html .= '<div id="pag' . $p . '" style="display:none">';
            } else {
                $html .= '<div id="pag' . $p . '" >';
            }
        }
        $html .= '<table id="tabella_elenchi" >';
        $html .= $rTitolo;
        for ($r = 0; $r < $numElementPag; $r++) {
            if ($i < $numElemeti) {
                $html .= $arrayRighe[$i];
                $i++;
            } else {
                $r = $numElementPag;
            }
        }
        $html .= "</table>";

        if ($multiPage) {
            $html .= "</div>";
        }
    }


    if ($multiPage) {
    	$html .= '</div>';
    }
    
    if ($multiPage) {
        $numElemet = array(25, 50, 75, 100, 150, 200, _ALL_);
        $htmlNav .= '<div class="navPage">
            
        <div>';
        $htmlNav .= '<a draggable="false" href="javascript:virtualpaginateP(\'' . $numPage . '\')" ><img src="images/expo2015/freccia_sinistra_bianco.png"></a>&nbsp;';
        for ($p = 1; $p <= $numPage; $p++) {
            $htmlNav .= '<a draggable="false" name="p'.$p.'" href="javascript:virtualpaginate(\'' . $p . '\',\'' . $numPage . '\')">' . $p . '</a> | ';
        }
        $htmlNav .= '&nbsp;<a draggable="false" href="javascript:virtualpaginateN(\'' . $numPage . '\')" ><img src="images/expo2015/freccia_destra_bianco.png"></a>';
        $htmlNav .= '&nbsp;&nbsp;N.';
        if ($selezionaNumElementi) {
            $htmlNav .= '<select id="elementiN"  name="elementiN" onchange="' . $evento . '">';
            foreach ($numElemet AS $value) {
                $select = "";
                if ($numElementPag == $value)
                    $select = "selected";
                $htmlNav .= '<option value="' . $value . '" ' . $select . '>' . $value . '</option>';
            }

            $htmlNav .= '</select>';
        } else {
            $htmlNav .= $numElementPag;
        }
        $htmlNav .= '</div>
            </div>';
    }

    $html = $htmlNav.$html.$htmlNav;
    return $html;
}

//VISUALIZZA E GESTISCE FILE
function getFileInDir($nomeFile, $nomeElemento) {
    $tag = "";
    if (is_file(Config::ROOTFILES . $nomeFile . '.pdf')) {
        $tag .= '<div id="' . $nomeElemento . 'Delete">';
        $tag .= _FILE_PRESENTE_ . '<a draggable="false" href="' . Config::ROOTFILESHTML . $nomeFile . '.pdf" target="_blank" >' . _VISUALIZZA_ . '</a> 
            <a draggable="false" href="javascript:deleteFile(\'' . Config::ROOTFILESHTML . $nomeFile . '.pdf\',\'' . $nomeElemento . 'Delete\')" >' . _ELIMINA_ . '</a>';
        $tag .= '<br></div > ';
    }

    return $tag;
}

//VISUALIZZA DESCRIZIONE DI DEFOULT
function descrizioneDefoult($descrizione, $file, $link) {
    if (strcmp($descrizione, "") != 0) {
        return $descrizione;
    } else {
        $testo = "";

        if (strcmp($file, "") != 0 && strcmp($link, "") != 0) {
            $testo.= _FILELINK_V_;
        } else {
            if (strcmp($file, "") != 0)
                $testo .= _FILE_V_;
            if (strcmp($link, "") != 0)
                $testo .= _LINK_V_;
        }

        return html_entity_decode($testo);
    }
}

//RITORNA ROOTDOC
function selectRootDoc($nomeImpresa) {
    global $arrayRootDoc;
    $inizialeImpresa = strtoupper(substr($nomeImpresa, 0, 1));

    foreach ($arrayRootDoc as $key => $value) {
        if (in_array($inizialeImpresa, $value)) {
            return $key;
        }
    }
}

function selectRootDocReti($nomeRete) {
	global $arrayRootDocReti;
	global $elseRootDocReti;

	$inizialeRete = strtoupper(substr($nomeRete, 0, 1));

	foreach ($arrayRootDocReti as $key => $value) {
		if (in_array($inizialeRete, $value)) {
			return $key;
		}

	}
	return $elseRootDocReti;
}

function getParametriRicerca($par, $value, $text) {
    foreach ($par AS $v) {
        if (strcmp($v, $value) != 0) {
            return "<span>" . $text . "</span>";
        }
    }
    return '';
}

$statoTrattative = array("A" => _ATTESA_, "C" => _CONFERMATO_, "D" => _DINIEGA_, "S" => _SOSPESO_);
$widthImgLogo = 120;
$heightImgLogo = 120;


//Testo da barrare in caso di value == N
function strikeText($value,$text) {
    if (strcmp($value, "N") == 0) {
    	$text = "<font style=\" opacity:0.20; filter:alpha(opacity=20); /* For IE8 and earlier */\">$text</font>";
    } 
	
    return $text;
}

function prefissoTelefono($value){
	$pre = substr($value, 0 ,4);
	if ((strpos($pre, "+39") === false) || (strpos($pre, "0039") === false) ){
		$value = "+39" . $value;
	}
	return $value;
}
?>
