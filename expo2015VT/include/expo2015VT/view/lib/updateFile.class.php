<?php

include_once("configuration.inc");

class UpdateFile {

    private $allowedExts = array("pdf"); // ESTENZIONE FILE
    private $rootFile = Config::ROOTFILES;
    private $sizeMax = Config::SIZEMAX; //PESO PASSIMO DEL FILE
    private $extension;

//$_FILES["file"]["size"] < 20000
    //CONTROLLA IL SIZE DELLA CARTELLA IMPRESA
    private function du($dir) {
        $res = `du -sk $dir`;             // Unix command
        preg_match('/\d+/', $res, $KB); // Parse result
        $MB = round($KB[0] / 1024, 1);  // From kilobytes to megabytes
        return $MB;
    }

    //CONTROLA IL FILE
    private function controlloFile($key, $dir) {

        $temp = explode(".", $_FILES[$key]["name"]);
        $this->extension = end($temp);
        $spazioLibero = round($_FILES[$key]["size"] / 1024, 1) + $this->du($dir);
        if (in_array($this->extension, $this->allowedExts) && $spazioLibero < $this->sizeMax) {
            if ($_FILES[$key]["error"] == 0)
                return TRUE;
        }

        return FALSE;
    }

    public function updateFiles($listFiles, $dir, $subDir) {



        $pathDir = $this->rootFile . $dir . $subDir;

        if (!file_exists($pathDir)) {
            mkdir($pathDir, 0700);
        }


        foreach ($listFiles as $key => $value) {
            $nameFile = substr($key, 0, strlen($key) - 1);

            if ($this->controlloFile($key, $dir))
                move_uploaded_file($_FILES[$key]["tmp_name"], $pathDir . $nameFile . "." . $this->extension);
        }
    }

    public function getInfoFile($file, $sizeMax) {
        $iFile = array();

        if ($file['size'] <= $sizeMax) {

            $fp = fopen($file['tmp_name'], 'r');
            $content = fread($fp, filesize($file['tmp_name']));
            $content = addslashes($content);
            fclose($fp);
            $iFile['tmp_name'] = $file['tmp_name'];
            $iFile['name'] = $fileName = addslashes($file['name']);
            $iFile['type'] = $file['type'];
            $iFile['size'] = $file['size'];
            $iFile['content'] = $content;
            return $iFile;
        }

        return NULL;
    }

}

?>
