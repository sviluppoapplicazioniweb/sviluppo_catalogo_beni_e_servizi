<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of htDig
 *
 * @author Francesco Spagnoli
 */
class HtDig {

    public function searchWhitHtDig($words, $nameTab) {

        $arraySql = "";
        $arrayResult = $this->getIdSearch($words);
        
        
        if ($arrayResult != NULL) {
        	
        	/*
        	print "<pre>Array RESULT:<br> ";
        	print_r($arrayResult);
        	print "</pre>";
        	*/
        	
            foreach ($arrayResult AS $value) {
            	/*
            	print "<pre>Array RESULT value:<br> ";
            	print_r($value);
            	print "</pre>";
                */
            	$tab = $nameTab[$value['tipo']];
                $id = $value['id'];
                if (strcmp($value['tipo'], "Reti")==0){
                	$arraySql .= " $tab.Id_Rete = '$id' OR";
                }else{
                	$arraySql .= " $tab.Id = '$id' OR";
                }
                
                
            }
            if (strcmp($arraySql, "") != 0) {
            	$arraySql = substr($arraySql, 0, -2);
            }
        }
        
        /*
        print "<pre><h2>RESULT ID:</h2> ";
        print_r($arraySql);
        print "</pre>";
        */
        
        return $arraySql;
    }

    private function getIdSearch($words) {

    	$words = utf8_encode($words);
        $risultati = array();

        $HTSEARCH_PROG = "/var/www/cgi-bin/htsearch";
        setlocale(LC_ALL, 'it_IT');
        $method = 'and';
        $format = "short";
        $sort = "score";
        $pagina = "search";
        $config = 'expo';
        $words = strip_tags($words);
        $urlPage = "index.phtml?Id_VMenu=312&";
        $urlFile = "download_file.php?path=";
		$urlLink = "&Link=";

        $query = "words=" . $words . ";pagina=" . $pagina . ";method=" . $method . ";format=" . $format . ";sort=" . $sort . ";config=" . $config . ";exclude=;submit=;matchesperpage=1000";
        //$query = substr($query, 0, strlen($query) - 1);


        if ($words) {

            $command = "$HTSEARCH_PROG  -c /nas_int/internet/htdig_13/conf/expo.conf \"$query\"";
            //print "command: <br>$command";
            
            exec($command, $result);
			
			/*
            print "<pre><h2>RESULT HDIG:</h2> ";
			print_r ($result);
			print "</pre>";
			*/
			
            # how many rows do we have?
            $rc = count($result);
            
            if ($rc < 3) {
                //echo "There was an error executing this query.  Please try later.\n";
                return NULL;
            } else {
                if ($result[2] == "NOMATCH") {
                    //echo "There were no matches for <b>$words</b> found on the website.<p>\n";
                    return NULL;
                } else {
                    if ($result[2] == "SYNTAXERROR") {
                        //echo "There is a syntax error in your search for <b>$search</b>:<br \>";
                        return NULL;
                    } else {

                        $ResultSetStart = 1;

                        foreach ($result AS $row) {
							
                        	
                        	//https://expovtrsv.mi.cciaa.net/index.phtml?Id_VMenu=312&lang=IT&tipo=Imprese&Link=ReferenzeLink385&Id=385
                        	
                        	if (strpos($row, $urlLink) > 0) {
                        		/*
                        		print "<pre><h2>RESULT ROW URL PAGE:</h2> ";
                        		print_r($row);
                        		print "</pre>";
                        		*/
                        		$row = trim($row);
                        		$numcarPosTipo = strpos($row, "&Link=") - (strpos($row, "tipo=") + 5);
                        		$tipoContenuto = substr($row, strpos($row, "tipo=") + 5, $numcarPosTipo);
                        		$numcarPosId = strpos($row, '">') - (strpos($row, "&Id=") + 4);
                        		$idContenuto = substr($row, strpos($row, "&Id=") + 4, $numcarPosId);
                        		$risultati[] = array("tipo" => $tipoContenuto, "id" => $idContenuto);
                        		/*
                        		print "<pre>";
                        		print_r ($risultati);
                        		print "</pre>";
                        		*/
                        	} else if (strpos($row, $urlFile) > 0) {
                        		/* 
                        		print "<pre><h2>RESULT ROW URL FILE:</h2> ";
                        		print_r($row);
                        		print "</pre>";
                        		*/
                        		$row = trim($row);
                        		                        	
                        		$positionTagClose = strpos($row, '">');
                        		$positionAzienda = strpos($row, "=aziende")+10;
                        		$subString = substr($row,$positionAzienda,$positionTagClose-$positionAzienda);

                        		$rowExpolode = explode("/", $subString);
                        		/*
                        		print "<pre><h2>RESULT ROW explode:</h2> ";
                        		print_r($rowExpolode);
                        		print "</pre>";
                        		*/
                        		if (strpos($string, "prodotti")){
                        			$tipoContenuto = "Prodotti";
                        			$idContenuto = $rowExpolode[2];
                        		}else{
                        		//$numcarPosTipo = strpos($row, "&Id=") - (strpos($row, "tipo=") + 5);
                        		$tipoContenuto = "Imprese";//substr($row, strpos($row, "tipo=") + 5, $numcarPosTipo);
                        		//$numcarPosId = strpos($row, '">') - (strpos($row, "&Id=") + 4);
                        		$idContenuto = $rowExpolode[0];//substr($row, strpos($row, "&Id=") + 4, $numcarPosId);
                        		}
                        		$risultati[] = array("tipo" => $tipoContenuto, "id" => $idContenuto);
                        	//print $numcarPosTipo;
                        		
                        	} else if (strpos($row, $urlPage) > 0) {
                            	/*
                            	print "<pre><h2>RESULT ROW URL PAGE:</h2> ";
                            	print_r($row);
                            	print "</pre>";
                            	*/
                                $row = trim($row);
                                $numcarPosTipo = strpos($row, "&Id=") - (strpos($row, "tipo=") + 5);
                                $tipoContenuto = substr($row, strpos($row, "tipo=") + 5, $numcarPosTipo);
                                $numcarPosId = strpos($row, '">') - (strpos($row, "&Id=") + 4);
                                $idContenuto = substr($row, strpos($row, "&Id=") + 4, $numcarPosId);
                                $risultati[] = array("tipo" => $tipoContenuto, "id" => $idContenuto);
                            }
                            
                                                        
                        }
                    }
                }
            }
        }

        return $risultati;
    }

}

?>
