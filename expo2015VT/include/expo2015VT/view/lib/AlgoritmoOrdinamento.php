<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ordinamento
 *
 * @author Francesco Spagnoli
 */
class Ordinamento {

    //put your code here
    public function __construct() {
        
    }

    public function main($numeroIndici, $action) {
        $num = array();

        if ($action) {
            $schrage1 = 0;
            $risultato;
            $risultato2;
            $min = 1;
            $max = $n = ($numeroIndici);
            $posizione = 0;
            $i = 0;
            $seed = time();
            $M = 2147483647;
            $A = 1103515245;

            $seed = $this->schrage3($A, $seed, $M);

            /* Iterazioni successive, il valore del seme cambia, n viene decrementato man mano che l'array viene popolato */
            while ($n > 0) {

                $seed = $this->schrage3($A, $seed, $M);
                $risultato = floatval($seed / $M);
                $risultato2 = (int) (($risultato) * ($max - $min) + 0.5);
                $risultato2 = $min + $risultato2;

                /* Si cerca il numero estratto nell'array, se non presente viene inserito */
                $posizione = $this->getPosizione($risultato2, $num);

                if ($posizione == -1) {

                    $num[$i] = (int) $risultato2;
                    $i++;
                    $n--;
                }
            }
        } else {
            for ($i = 1; $i <= $numeroIndici; $i++) {
                $num[$i] = $i;
            }
        }

        return $num;
    }

    /**
     * Calculates (a * b) % m without overflow given certain conditions
     */
    public function addMod($a, $b, $m) {
        $result = 0;

        if ($b <= PHP_INT_MAX - $a) {
            $result = ($a + $b) % $m;
        } else {
            $result = $this->addMod($a, $b / 2, $m);
            $result = $this->addMod($result, $b / 2, $m);
            if ($b % 2 == 1) {
                $result = $this->addMod($result, 1, $m);
            }
        }

        return $result;
    }

    public function schrage3($a, $b, $m) {
        if ($a == 0) {
            return 0;
        }
        if ($b >= $m - 1) {
            $result = $this->schrage3($a, $b / 2, $m);
            $result = $this->addMod($result, $result, $m);
            if ($b % 2 == 1) {
                $result = $this->addMod($result, $a, $m);
            }
            return $result;
        }
        $quot = $m / $a;
        $rem = $m % $a;
        if ($rem >= $quot) {
            $result = $this->schrage3($a / 2, $b, $m);
            $result = $this->addMod($result, $result, $m);
            if ($a % 2 == 1) {
                $result = $this->addMod($result, $b, $m);
            }
            return $result;
        }

        // Schrage's algorithm
        $result = $a * ($b % $quot) - $rem * ($b / $quot);
        if ($result < 0) {
            $result += $m;
        }

        return $result;
    }

    public function getPosizione($valoreDaCercare, $arrayListe) {
        $posizione = -1;
        for ($i = 0; $i < count($arrayListe); $i++) {
            if ($arrayListe[$i] == $valoreDaCercare) {
                $posizione = $i;
            }
        }
        return $posizione;
    }

}

?>