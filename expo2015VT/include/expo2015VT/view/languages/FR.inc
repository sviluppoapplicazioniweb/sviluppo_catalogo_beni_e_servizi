<?php

//TITOLI TAB
define(_AZIONE_, "Action");
define(_EMAIL_, "E-mail");
define(_NOME_, "Nom");
define(_COGNOME_, "Pr&eacute;nom");
define(_TELEFONO_, "Phone");
define(_PRODOTTI_, "Produits");
define(_PRODOTTO_, "Produit");
define(_RAGIONESOCILE_, "Company name");
define(_PARAMETRI_, "Param&egrave;tres ");
define(_BENVENUTO_, "Welcome");
define(_DISCONNETTI_, "Disconnect");
define(_CERTIFICAZIONI_, "Certifications et Reconnaissances");
define(_ASPETTI_, "Aspects d'innovation et durabilit&eacute;");
define(_BREVETTI_, "Brevets");
define(_C_MERC_, "Cat&eacute;gorie de marchandises");

define(_C_MERC_T_, "Cat&eacute;gories<br>de Produits");
define(_DATI_GENERALI_T_, "Donn&eacute;es<br>G&eacute;n&eacute;rales");
define(_CERTIFICAZIONI_T_, "Certifications<br>Reconnaissances");
define(_ASPETTI_T_, "Aspects d'innovation <br>durabilit&eacute;");
define(_BREVETTI_T_, "Brevets<br><br>");


define(_C_LOGO_, "Change Logo");
define(_TIT_1_, "Donn&eacute;es provenant du Registre du Commerce");
define(_TIT_2_, "Contacts");
define(_TIT_3_, "Information material");

define(_DATI_GENERALI_, "General data companies");
define(_DETTAGLI_, "D&eacute;tails");
define(_QUALIT_T_, "Caract&eacute;ristiques");
define(_QUALIT_, "Characteristics of the business/professional");
define(_ASSOCIZIONI_, "Associations");
define(_ASSOCIZIONI_RETI_, "Formes d&rsquo;agr&eacute;gation");
define(_RAGSOC_, "Raison sociale");
define(_CAPISOC_, "Capital social");
define(_U_FATTURATO_, "Somme des cinq derniers chiffres d'affaires");
define(_R_U_FATTURATO_, "Somme des 5 derniers chiffres d'affaires");
define(_R_U_FATTURATO_1_, "(&euro; Milions)");
define(_R_U_FATTURATO_2_, "(&euro;x1000)");

define(_ALLEGA_DOC_, "<strong>Document d'approfondissement</strong>");
define(_ALLEGA_LINK_, "<strong>Lien d'approfondissement</strong>");
define(_ALLEGA_ELENCO_, "<strong>Liste des r&eacute;f&eacute;rences les plus qualifiantes</strong>");
define(_SEDE_LEG_, "Si&egrave;ge l&eacute;gal");
define(_TEL_, "Pho");
define(_DESCRIZIONE_, "Description");
define(_DESCRIZIONE_ATT_, "Description des activit&eacute;s");
define(_N_DIP_, "Nombre de salari&eacute;s");
define(_SEDI_, "Degree of enterprise internationalization");
define(_S_ESTERA_, "Pr&eacute;sence de si&egrave;ge &agrave; l'&eacute;tranger");
define(_NET_INTER_, "Membership in international Network");
define(_T_IMPRESA_, "Type d'Entreprise");
define(_GENER_CONT_, "Entrepreneur G&eacute;n&eacute;ral");
define(_PRODUTTORE_, "Producteur");
define(_DISTRIBUTORE_, "Distributeur");
define(_REFERENZE_, "R&eacute;f&eacute;rences");
define(_PARTECIPZIONI_EXPO_, "Participation in EXPO");
define(_PAR_INTERNAZIONALI_, "R&eacute;f&eacute;rences internationales");
define(_ESPERIENZA_IN_, "Fournisseur d'autres Expositions ou d'autres grands &eacute;v&eacute;nements internationaux");
define(_CON_LINGUE_, "Connaissance des langues");
define(_LINGUE_, "langues");
define(_SELEZIONA_, "S&eacute;lectionnez...");
define(_GREEN_, "Attention au th&eacute;me de la durabilit&eacute; &Eacute;cologie et Social");
define(_CERTIFICAZIONI2_, "Certifications");
define(_APP_ASS_, "Appartenance &agrave; des Associations");
define(_APP_RETE_IMP_, "Appartenance &agrave; des agr&eacute;gations d'Entreprises (r&eacute;seaux, consortiums, associations)");
define(_R_RETE_IMP_, "Forme d'agr&eacute;gation");
define(_R_N_PARAMETRI_, "Param&egrave;tres s&eacute;lectionn&eacute;s: ");
define(_R_SINDACALE_, "Pr&eacute;sence du Coll&egrave;ge des Commissaires aux Comptes");
define(_R_P_CERTIFICAZIONE_, "SOA Certification");

define(_SINDACALE_, "Pr&eacute;sence du Coll&egrave;ge des Commissaires aux Comptes");
define(_A_MODELLO_, "Mod&egrave;le Organisationnel 231/01");
define(_P_LEGALITA_, "Note de l&eacute;galit&eacute;");
define(_P_MASTER_R_, "Professionnel poss&eacute;dant un Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_P_MASTER_, "Masters/PhD in its business");
define(_P_CERTIFICAZIONE_, "Certification SOA");
define(_IMPRESA_, "Entreprise");
define(_PREFERITI_, "Favoris");
define(_OSSERVAZIONI_, "Reason:");
define(_PARTECIPANTE_, "Participant");
define(_FORNITORE_, "Fournisseur");
define(_VALORE_, "Valeur");
define(_DATA_CHIUSURA_, "Date de fermeture");
define(_STATO_, "&Eacute;tat");
define(_TESTO_MOD_IMG_, "<h2> Modifier Logo </ h2>
S&eacute;lectionner un nouveau Logo &agrave; t&eacute;l&eacute;charger<br>
Si besoin est, votre logo sera automatiquement r&eacute;duit &agrave; 120 pixels de largeur x 120 de hauteur.");
define(_PASSWORD_, "Password");
define(_PASS_, "Password (min. 8 char.)*");
define(_R_PASS_, "Re-enter Password*");
define(_SEND_OK_, "<h2>Message envoy&eacute; avec succ&eacute;s!</h2>");
define(_FILE_PRESENTE_, "A file ");
define(_FILE_V_, "Voir ci-dessous pour plus de d&eacute;tails fichier<br>");
define(_LINK_V_, "Voir les liens Web ci-dessous pour plus de d&eacute;tails<br>");
define(_NOTE_1_, "Mod&egrave;le d'organisation et de gestion visant &agrave; pr&eacute;venir la responsabilit&eacute; objective des entreprises au niveau p&eacute;nal; d&eacute;coulant du D&eacute;cret L&eacute;gislatif n� 231 du 8 juin 2001.");
define(_NOTE_2_, "Rating ethical managed by the Authority for Competition and Market Authority (AGCM) resulting from the Decree Law of 24 March 2012, n. 29 in order to promote ethical principles in Italy in corporate behavior.");
define(_NOTE_3_, "Products that have opened negotiations will not be displayed.");
define(_NOTE_4_, "<br>* Mandatory in case of refusal<br><br>");

//BOTTONI
define(_SALVA_, "Enregistrer");
define(_ELIMINA_, "Eliminer");
define(_AGGIUNGI_, "Ajouter");
define(_VISUALIZZA_, "Visualiser");
define(_INVIA_, "Envoyer");
define(_CHIUSURA_, "Cl&ocirc;ture de la n&eacute;gociation");
define(_INDIETRO_, "< En arri&egrave;re");
define(_CONFERMA_, "Confirmer");
define(_DINIEGA_, "Refuser");
define(_SOSPESO_, "Suspendre");
define(_CHIUDI_, "Fermer");
define(_RICHIESTA_, "Envoyer Demande");
define(_RICHIESTE_, "Demande");
define(_MODIFICA_, "Modifier");
define(_ATTESA_, "Attente");
define(_CONFERMATO_, "Confirm&eacute;");
define(_ANTEPRIMA_, "Preview");
define(_G_LINK_, "Regenerate Links");
define(_ACCEDI_, "Login");
define(_REGISTRATI_, "Register");
define(_CAMBIA_, "Modifier ");
define(_EXTRA_B_, "N&eacute;gociations hors tableau");
define(_ALL_, "All");
define(_SELEZIONA_B_, "S&eacute;lectionnez");

//MSG RISPOSTA
define(_PRODOTTO_CATEGORIA_, "The category you are clearing is associated with some products.");
define(_CAMPI_OB_CAT_, "Select at least one category.");
define(_EMAIL_ER_, "E-Mail field not valid");
define(_SMART_CARD_, "Smart Card and press enter login.");
define(_OK_INVIO_LINK_, "<h2>You have been sent an email with a confirmation link recording</h2>");
define(_PASSWORD_ERR_, "<h2>Password is incorrect</h2>");
define(_TRA_INCORSO_, "For these products are already in the negotiations:");
define(_CAMPI_OB_, "Les param&egrave;tres signal&eacute;s par un ast&eacute;risque sont obligatoires.");
define(_CAMPI_OB_TR_, "Enter a text for all languages?.");
define(_CAMPI_PAS_NO_, "Invalid Password");
define(_CAMPI_PAS_UG_, "Password does not match confirmation");

define(_OK_ATTIVAZINE_, "Activation was successful");

define(_MITTENTE_, "Exp&eacute;diteur");
define(_DESTINATARIO_, "Destinataire");
define(_OGGETTO_, "Objet");
define(_DATA_, "Date");
define(_OGGETTO_MAIL_, "Demande de devis pour ");
define(_MESSAGGIO_, "Message");
define(_ALLEGATO_, "Pi&egrave;ce jointe");

//MAIL DI RISPOSTA
define(_N_RISP_, "<br /><br />Automatically generated message please do not respond.");
define(_OGG_DATI_UTENTE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Data recording");
define(_MSG_DATI_UTENTE_, "<p>Dear user,<br /> 
we confirm successful registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
Here are your Username and Password: <br>
<br />
USER: {%user%} <br />
<br />
PASSWORD: {%Password%} <br />
<br /> <br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_AGG_I_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_AGG_I_, "<p>Dear {%utente%},<br /> 
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has assigned the role of Managing Director of the Company in the Catalogue Goods and Services Suppliers Expo 2015. 
Can access the catalog with the logon credentials that are already in its possession 
and then select the / Enterprise which is already written for you accederne to content. <br /><br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_REGISTRAZIONE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_NEW_REG_, "<p>Dear {%utente%},
���� <br /> we confirm the registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
By clicking on the link below which will activate your registration. <br />
���� {%link%} <br /><br />
���� Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_DELETE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Cancellation Personal Profile");
define(_MSG_DEL_REG_DEF_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
carried out the cancellation of his role as Chief Executive Officer of the Company. <br> 
Since it is not included on other companies in the Catalog its user has been deleted and can no 
longer use the login credentials in his possession. <br> For any information to contact regarding the 
direction of EXPO. 
<br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_MSG_DEL_REG_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>,as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has reset the memory of his role as Chief Executive Officer of the Company. <br />
Can still access the catalog with the logon credentials that are already in its possession and then select the / Enterprise which is already written for you accederne to content.
    <br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_BACHECA_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - negotiation ID #{%idBacheca%}");
define(_MSG_NOTIFICA_TRA_, "<p> Dear {%user%}  <br />
���� the user <strong>{%mittente%}</ strong> has sent you a message. <br />
���� You can find it in the Message section of your personal area of ??the Goods and Services Catalog Suppliers Expo 2015. <br />
���� <br />
���� Regards <br /> </ p>{%TITLESITE%}
    <br />-----------------------------------<br />
    <p>Gentile {%utente%} ,<br />
    l&#39;utente  <strong>{%mittente%}</strong> ti ha inviato un messaggio. <br />
    Lo trovi nella sezione Bacheca della tua area personale del Catalogo Fornitori Beni e Servizi Expo 2015.<br />
    <br />
    Distinti Saluti <br /> </p>{%TITLESITE%}"._N_RISP_);

define(_OGG_NOT_CD_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi � Bacheca Messaggi]  Trattativa ID #{%idTrattativa%}  ({%oggetto%})");
define(_MSG_NOT_CD_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the Enterprise {%Impresa%}
<strong>refused</strong> the closing of the deal. And &#39;possible to read the <strong>reasons</strong> for the refusal and send
message logging Showcase Area Board of Participants.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l�Impresa {%Impresa%} 
ha <strong>rifiutato</strong> la chiusura della trattativa. E� possibile leggere le motivazioni del rifiuto ed inviare 
un messaggio accedendo nell�Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CD_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi � Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_P_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation with Company {%Impresa%}, we inform you that the Participant {%mittente%} 
<strong>refused</strong> the closure of the deal. <br>
You can read the reasons for the refusal and send a message logging Area Board
Suppliers of Goods & Services Catalog.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha <strong>rifiutato</strong> la chiusura della trattativa.<br> 
E� possibile leggere le motivazioni del rifiuto ed inviare un messaggio accedendo nell�Area Bacheca 
del Catalogo Fornitori Beni & Servizi.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation,
we inform you that the company has <strong>confirmed</strong> {%Impresa%}
the closure of the negotiation<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto,
la informiamo che l�Impresa  {%Impresa%} ha <strong>confermato</strong> 
la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation,
we inform you that Participant {%mittente%} has confirmed the closing of the deal.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha confermato la chiusura della trattativa<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_I_, "<p>Good morning / Good evening , <br>
with reference to the aforementioned negotiation,
we inform you that company {%Impresa%} responsible for sealing the deal.
It requires you to confirm whether or not the outcome of the Showcase Participants accessing Area Board.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera ,<br>
con riferimento alla Trattativa Commerciale in oggetto, 
la informiamo che l�Impresa {%Impresa%} ha proceduto alla chiusura della trattativa. 
Si richiede di confermarne o meno l�esito accedendo nell�Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that Participant {%mittente%} responsible for sealing the deal. <br>
It requires you to confirm whether or not the outcome accessing Area Wall Suppliers Catalog Goods & Services<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%},  
la informiamo che il partecipante {%mittente%} ha proceduto alla chiusura della trattativa.<br>
Si richiede di confermarne o meno l�esito accedendo nell�Area Bacheca del Catalogo Fornitori Beni & Servizi<br />
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);

//GESTIONE LINGUE
define(_ITALIANO_, "Italian");
define(_INGLESE_, "English");
define(_FRANCESE_, "French");

//MODIFICA 25/09/2013
define(_FILE_NO_UP_, "<h2>Unable to upload.</h2>");
define(_FILE_MAX_, "<h2>File is too big.</h2>");
define(_ENTRA_SC_, "Sign in using Smart Card");
define(_ENTRA_LOG_, "Sign in using credentials recorded");
define(_DIM_PAS_, "Forgot your password?");
define(_REG_, "Registering");

//MODIFICA 26/09/2013
define(_RICERCA_, "Feuilleter");
define(_IMPRESE_, "Entreprises");
define(_AGGIUNGI_, "Ajouter des param&egrave;tres");
define(_CERCA_PAR_, "RECHERCHE DES PARAM&Egrave;TRES");
define(_ANNULLA_, "Cancels");

//MODIFICA 30/09/2013
define(_UTENTE_RLN_, "Lien transmis");
define (_NUMERO_OGGETTI_BACHECA_,"Il ya %n% des messages");

//MODIFICA 01/10/2013
define(_NO_PRODOTTI_,"To be able to enter new Products/Services, you must subscribe to at least one Product Category");

define (_AGGIUNGI_CATEGORIA_,"Add Product Category");
define (_GESTISCI_CATEGORIA_,"Manage Product Category");
define (_NESSUNA_CATEGORIA_,"None Product Category registered");

// MODIFICA DEL 02/10/2013
define (_NUMERO_OGGETTI_BACHECA_,"Il y a %n% messages");
define(_DESCRIZIONE_FILE_,"Description");
define(_BILANCI_FILE_,"Budget");
define(_INTERNAZIONALIZZAZIONE_FILE_,"Internationalization");
define(_REFERENZE_FILE_,"R&eacute;f&eacute;rences");
define(_QUALITA_FILE_,"Qualit&eacute;");
define(_RETI_FILE_,"Formes d&rsquo;agr&eacute;gation");

define(_PRODOTTO_FILE_,"Produit/Service");
define(_CERTIFICAZIONE_FILE_,"Certification");
define(_ASPETTI_FILE_,"Aspects");
define(_BREVETTI_FILE_,"Brevets");

define(_FILELINK_V_, "See File/Web link below for details<br>");

// MODIFICA DEL 04/10/2013
define(_NO_TRATTATIVA_,"Aucune N&eacute;gociation pr&eacute;sente");
define(_NO_BACHECA_,"Aucun Message sur le Tableau");
define(_NO_PREFERITI_,"Aucun favori pr&eacute;sent");
define(_NO_RICERCA_,"La recherche n'a pas donn&eacute; de r&eacute;sultats");

//MODIFICA DEL 07/10/2013
define(_NO_PAR_RICERCA_,"S&eacute;lectionner au moins un param&eacute;tre de recherche");
define(_SVUTA_RICERCA_,"Vider la recherche");
define(_NO_RICHIESTA_,"Annuler les demandes");
//MODIFICA DEL 16/10/2013
define(_NON_AB_,"<h2>VOUS N'AVEZ PAS L'AUTORISATION DE VISITER CE SITE.</h2>");
//MODIFICA 21/10/2013
define(_TIMEOUT_LOGIN_,"TIME OUT LOG-IN");
define(_TEXT_TIMEOUT_LOGIN_,"Trop de temps a pass&eacute; depuis votre derni&eacute;re identification.<br>Pour des raisons de s&eacute;curit&eacute;, nous vous prions de vous identifier de nouveau sur le portail");
define(_LOGIN_,"Log-in");

//MODIFICA 22/10/2013
define(_TEXT_HOME_1_,"<span>Bienvenue <br>dans le Catalogue des Fournisseurs <br>pour les Participants &agrave; Expo 2015</span><br>
        L&rsquo;environnement virtuel qui vous met en contact avec les entreprises et les professionnels qui pourront vous aider &agrave; concevoir, 
		construire et am&eacute;nager votre pavillon, gr&acirc;ce &agrave; la fourniture de services et de produits.");
define(_TEXT_HOME_2_," <span><br>Que peut-on faire avec le catalogue?</span>");
define(_TEXT_HOME_3_,"Pour recevoir des renseignements et de l&rsquo;aide, &eacute;crire &agrave; : <br>info-COcatalogue@expo2015.org");
define(_TEXT_HOME_4_,"<div>Rechercher l'entreprise ou le professionnel <br>int&eacute;ressant  par cat&eacute;gories commerciales.</div>");
define(_TEXT_HOME_5_,"<div>Affiner la recherche sur la base <br>de leurs r&eacute;f&eacute;rences, de leurs certifications ou d'autres <br>caract&eacute;ristiques distinctives des entreprises.</div>");
define(_TEXT_HOME_6_,"<div>Si vous connaissez l'entreprise,<br>vous pouvez la rechercher directement.</div>");
define(_TEXT_HOME_7_,"<div>Apr&egrave;s avoir trouv&eacute; une ou plusieurs entreprises <br>qui vous int&eacute;ressent, contactez-les en envoyant un demande &agrave; travers le syst&egrave;me de messagerie.</div>");
define(_TEXT_HOME_8_,"<div>G&eacute;rez les &eacute;ventuelles n&eacute;gociations <br>dans la zone d&eacute;di&eacute;e et s&ucirc;re !</div>");

//MODIFICA 28/10/2013
define(_EXPO2015_,"EXPO 2015 Milano");
//MODIFICA 31/10/2013
define(_PARAMETRI_RICERCA_,"<strong>Param&egrave;tres de recherche:</strong> ");

//MODIFICA 04/11/2013
define(_U_FATTURATO_ESERC,"Chiffre d'affaires du dernier exercice");
define(_IsPrestatoreDiServizi_,"Fournisseur de services");

//MODIFICA 05/11/2013
define(_TEXT_HOME_9_,"<div>Recherche produits durables et innovants sur SIEXPO</div>");
define(_P_MASTER_FILE_, "Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_P_CERTIFICAZIONE_FILE_, "Certification SOA");
define(_CERTIFICAZIONI_FILE_, "Certification"); 

//MODIFICA 06/11/2013
define(_RISPONDI_, "R&eacute;pondre");
define(_VISUALIZZA_TRATTATIVA_, "N&Eacute;GOTIATIONS");
//MODIFICA 15/11/2013
define(_ALLEGA_DOC_, "Document d&rsquo;approfondissement:");
define(_ALLEGA_LINK_, "Liens pour plus d'informations");

//MODIFICA 20/11/2013
define(_NO_ASSSOCIAZIONI_, "Pas d&rsquo;association s&eacute;lectionn&eacute;e");
define(_NO_LINGUE_, "Aucune langue s&eacute;lectionn&eacute;e");
define(_ADDETTI_, "Employ&eacute;s");
define(_FATTURATO_5_, "Chiffre d&rsquo;affaires");
define(_A_MODELLO_, "Mod&egrave;le Organisationnel 231/01");
define(_P_MASTER_R_, "Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_R_P_CERTIFICAZIONE_, "Certification SOA");
define(_CARATTERISTICHE_, "CARACT&Eacute;RISTIQUES");

//MODIFICA 22/11/2013
define(_TEXT_HOME_9_,'<div>Ricerca materiali e prodotti<br>sostenibili ed innovativi sul <a href="javascript:invia(\'http://www.siexpo2015.it/index.phtml?Id_VMenu=295\')">SIEXPO</a></div>');
define(_TEXT_INFO_P_,'Si l&rsquo;entreprise existe depuis moins de 5 ans, la somme des chiffres d&rsquo;affaires conclus depuis la naissance de l&rsquo;entreprise est indiqu&eacute;e ');
define(_N_DIP_, "Nombre d&rsquo;employ&eacute;s");
define(_R_RETE_IMP_R_, "Regroupement d'entreprises");
define(_INTERNAZIONALIZZAZIONE_, "Internationalisation");
define(_RICERCA_TEXT_, "Rechercher dans les r&#201;sultats");
define(_RICERCA_, "Rechercher");
define(_SVUTA_RICERCA_,"Nouvelle recherche");
define(_RICHIESTA_R_, "Contacter l&rsquo;entreprise s&eacute;lectionn&eacute;e");
define(_R_N_PARAMETRI_, "<strong>Les param&egrave;tres s&eacute;lectionns:</strong>");
//MODIFICA 06/12/2013
define(_IS_SIEXPO_, "Disponible dans le Catalogue Siexpo");

define(_FAQ_DOMANDA_,"Domanda");
define(_FAQ_PUBBLICATA_,"Pubblicata");
define(_FAQ_AZIONE_,"Action");
define(_FAQ_MODIFICA_,"Modifica");
define(_FAQ_ELIMINA_,"Elimina");
define(_FAQ_PUBBLICA_,"Pubblica Faq");
define(_FAQ_VISUALIZZA_,"Voir");
define(_FAQ_OFFLINE_,"Metti Offline");
define(_FAQ_OP_MODIFICA_,"ModificaFaq");
define(_FAQ_OP_VISUALIZZA_,"VisualizzaFaq");
define(_FAQ_OP_INSERIMENTO_,"InserimentoFaq");
define(_FAQ_MSG_ELENCO_,"N.B. L'elenco si riferisce alle sole Faq pubblicate");
define(_FAQ_MSG_ELENCO_2_,"N.B. La domanda verrà esaminata da EXPO prima della pubblicazione");
define(_FAQ_TOT_QUERY_,"Total FAQ: ");
define(_FAQ_INDIETRO_,"Arrière");
define(_FAQ_ARGOMENTO_,"Sujet");
define(_FAQ_RICERCA_LIBERA_,"Recherche libre");
define(_FAQ_NO_RISULTATI_,"Pas cette Faq / correspondant aux filtres sélectionnés");
define(_FAQ_MSG_OFFLINE_,"Vuoi mettere offline questa Faq?");
define(_FAQ_MSG_ONLINE_,"Vuoi pubblicare questa Faq?");
define(_FAQ_MSG_ELIMINA_,"Vuoi eliminare questa Faq?");
define(_FAQ_MSG_OFFLINE_OK_,"La Faq è offline");
define(_FAQ_MSG_ONLINE_OK_,"La Faq è pubblicata");
define(_FAQ_MSG_MODIFICA_OK_,"La Faq è stata modificata");
define(_FAQ_MSG_ELIMINA_OK_,"La Faq è stata cancellata");
define(_FAQ_MSG_INSERIMENTO_OK_,"La Faq è stata inserita correttamente");
define(_FAQ_ALT_OFFLINE_,"Metti Offline");
define(_FAQ_ALT_ONLINE_,"Pubblica Faq");
define(_FAQ_ALT_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_VISUALIZZA_,"Voir FAQ");
define(_FAQ_TITLE_MODIFICA_,"Modifica Faq");
define(_FAQ_TITLE_ONLINE_,"Pubblica Faq");
define(_FAQ_TITLE_OFFLINE_,"Metti Offline");
define(_FAQ_TITLE_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_INDIETRO_,"Torna alla pagina della Faq");

define(_FILTRI_RICERCA_LABEL_,"Filtres de recherche");
define(_FAQ_PULSANTE_AGGIORNA_,"Actualiser");

define(_RISULTATI_TROVATI_,"r&eacute;sultats");
define(_RISULTATI_PER_PAGINA_,"r&eacute;sultats par page");


define(_TUTTE_,"Toutes");
define(_NESSUNA_,"Aucune");
define(_INVERTI_,"Invert");


//MODIFICA  18/04/2014 PROFESSIONISTI

define(_TIT_1_LP_, "Donn&eacute;es du Professionnel");
define(_TIT_1_STUDIO_, "Donn&eacute;es du Cabinet");
define(_N_DIP_ORDINI_, "Nombre de pr&eacute;pos&eacute;s ou de collaborateurs");

define(_QUALIT_LP_, "Caract&eacute;ristiques du professionnel");
define(_QUALIT_STUDIO_, "Caract&eacute;ristiques du cabinet");
define(_SEDI_LP_, "Degr&eacute; d'internationalisation du professionnel");
define(_SEDI_STUDIO_, "Degr&eacute; d'internationalisation du cabinet");
define(_RAGSOC_PROF_, "Nom/D&eacute;nomination");
define(_APP_RETE_IMP_PROF_, "Appartenance &agrave; un regroupement (ex. : r&eacute;seaux, consortiums, associations)");

define(_IMPRESA_OFFLINE_PRODOTTI_, "L'impresa &egrave; offline. I prodotti/servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_LP_, "Il Professionista &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_STUDIO_, "Lo studio &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");

define(_PROFESSIONISTA_, "Professionnel");
define(_STUDIO_, "Cabinet");
define(_PUBBLICA_ALL_PROF_, "Services publics");

define(_SERVIZI_, "Liste des services");
define(_SERVIZIO_, "Service");

//MODIFICA  15/05/2014
define(_P_SIEXPO_, "Produits durables v&eacute;rifi&eacute;es par SIEXPO");
define(_ANTEP_IMP_,'<a draggable="false" href="javascript:inviaSiexpo(document.siexpoForm,\'http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295\')"><label for="isSiexpo" >Pr&eacute;sent dans le catalogue <img src="/images/expo2015/siexpo.jpg"  style="vertical-align: middle;margin-left: 5px;width: 100px"/></label></a>');
define(_NO_RETI_,'Pas de r&eacute;seau s&eacute;lectionn&eacute;');
define(_ANT_NOME_RETE_, "R&eacute;seau: ");
define(_DESCR_RETE_, "Description: ");
define(_ANT_CONTATTO_RETE, "Contacter le R&eacute;seau: ");
define(_IMPRESE_RETE_,"<strong>D'autres entreprises et membres du R&eacute;seau au catalogue</strong><br/>");
define(_ANT_IMP_CAPOG_,"Chef de groupe de l'entreprise");

define(_DISCLAIMER_CATEGORIE_DEFAULT_ ,"Les produits/services inclus dans cette cat&eacute;gorie commerciale sont fournis par le Partenaire/Sponsor d&#039;Expo 2015, dont l&#039;offre est d&eacute;taill&eacute;e dans le <b>Catalogue Des Partenaires Officiels</b> (http://catalogue.expo2015.org). D&#039;autres entreprises peuvent toutefois &ecirc;tre consult&eacute;es en poursuivant la recherche.");


define(_ACCESSO_NEGATO_,"NON SEI ABILITATO ALLA VISUALIZZAZIONE");

define(_IMPRESA_TEMPORANEAMENTE_OFFLINE_,"IMPRESA TEMPORANAMENTE OFF LINE");
?>

