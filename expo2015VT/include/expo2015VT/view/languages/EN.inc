<?php

//TITOLI TAB
define(_AZIONE_, "Action");
define(_EMAIL_, "E-mail");
define(_NOME_, "Name");
define(_COGNOME_, "Surname");
define(_TELEFONO_, "Phone");
define(_PRODOTTI_, "Products");
define(_PRODOTTO_, "Product");
define(_RAGIONESOCILE_, "Company name");
define(_PARAMETRI_, "Parameters");
define(_BENVENUTO_, "Welcome");
define(_DISCONNETTI_, "Disconnect");
define(_CERTIFICAZIONI_, "Certifications/Awards");
define(_ASPETTI_, "Innovation and sustainability");
define(_BREVETTI_, "Patents");
define(_C_MERC_, "Goods & services Categories");

define(_C_MERC_T_, "Goods & services <br>Categories");
define(_DATI_GENERALI_T_, "General<br>product info");
define(_CERTIFICAZIONI_T_, "Certifications/Awards");
define(_ASPETTI_T_, "Distinction in Innovation<br>Sustainability");
define(_BREVETTI_T_, "Patents<br><br>");


define(_C_LOGO_, "Change Logo");
define(_TIT_1_, "Information from Business Registry");
define(_TIT_2_, "Contacts");
define(_TIT_3_, "Information material");

define(_DATI_GENERALI_, "General company information");
define(_DETTAGLI_, "Details");
define(_QUALIT_T_, "Characteristics");
define(_QUALIT_, "Characteristics of the business/professional");
define(_ASSOCIZIONI_, "Associations");
define(_ASSOCIZIONI_RETI_, "Business network contracts");
define(_RAGSOC_, "Company name");
define(_CAPISOC_, "Share capital");
define(_U_FATTURATO_, "Revenue for the last 5 years");
define(_R_U_FATTURATO_, "Revenue for the last 5 years");
define(_R_U_FATTURATO_1_, "(&euro; Milions)");
define(_R_U_FATTURATO_2_, "(&euro;x1000)");

define(_ALLEGA_DOC_, "<strong>Explanatory document</strong>");
define(_ALLEGA_LINK_, "<strong>Links for further information</strong>");
define(_ALLEGA_ELENCO_, "<strong>List of references most qualified</strong>");
define(_SEDE_LEG_, "Registered headquarters");
define(_TEL_, "Phone");
define(_DESCRIZIONE_, "Description");
define(_DESCRIZIONE_ATT_, "Description of activity");
define(_N_DIP_, "Number of employees");
define(_SEDI_, "Degree of company internationalization");
define(_S_ESTERA_, "Existence of foreign branches");
define(_NET_INTER_, "Membership in international network");
define(_T_IMPRESA_, "Company Type");
define(_GENER_CONT_, "General contractor");
define(_PRODUTTORE_, "Producer");
define(_DISTRIBUTORE_, "Distributor");
define(_REFERENZE_, "References");
define(_PARTECIPZIONI_EXPO_, "Supplier for other expos or major international events");
define(_PAR_INTERNAZIONALI_, "International references");
define(_ESPERIENZA_IN_, "International Experiences");
define(_CON_LINGUE_, "Knowledge of languages");
define(_LINGUE_, "languages");
define(_SELEZIONA_, "Select...");
define(_GREEN_, "Concern for environmental and social sustainability");
define(_CERTIFICAZIONI2_, "Certifications");
define(_APP_ASS_, "Membership in associations");
define(_APP_RETE_IMP_, "Membership in company networks, consortia or other forms of aggregation (networks, consortia, associations)");
define(_R_RETE_IMP_, "Network of companies");
define(_R_N_PARAMETRI_, "Selected parameters: ");
define(_R_SINDACALE_, "Presence Board of Statutory Auditors");
define(_R_P_CERTIFICAZIONE_, "SOA Certification");

define(_SINDACALE_, "Presence of a Board of Statutory Auditors");
define(_A_MODELLO_, "Organizational Model 231/01");
define(_P_LEGALITA_, "Rating of legality");
define(_P_MASTER_R_, "Professional holding Masters / PhD");
define(_P_MASTER_, "Masters/PhD in its business");
define(_P_CERTIFICAZIONE_, "SOA Certification");
define(_IMPRESA_, "Company");
define(_PREFERITI_, "Favorites");
define(_OSSERVAZIONI_, "Reason:");
define(_PARTECIPANTE_, "Participant");
define(_FORNITORE_, "Supplier");
define(_VALORE_, "Value");
define(_DATA_CHIUSURA_, "Closing date");
define(_STATO_, "Status");
define(_TESTO_MOD_IMG_, "<h2> Logo Change </ h2>
Select a New Logo to load <br>
Your logo will be resized if needed to a maximum dimension of 120 pixels width x 120 height automatically.");
define(_PASSWORD_, "Password");
define(_PASS_, "Password (min. 8 char.)*");
define(_R_PASS_, "Re-enter Password*");
define(_SEND_OK_, "<h2>Message sent successfully!</h2>");
define(_FILE_PRESENTE_, "A file ");
define(_FILE_V_, "See File below for details<br>");
define(_LINK_V_, "See Web Link below for details<br>");
define(_NOTE_1_, "Model of organization and management aimed at preventing the liability of the undertakings in criminal liability arising from the Legislative Decree of 8 June 2001. 231.");
define(_NOTE_2_, "Rating ethical managed by the Authority for Competition and Market Authority (AGCM) resulting from the Decree Law of 24 March 2012, n. 29 in order to promote ethical principles in Italy in corporate behavior.");
define(_NOTE_3_, "Products that have opened negotiations will not be displayed.");
define(_NOTE_4_, "<br>* Mandatory in case of refusal<br><br>");

//BOTTONI
define(_SALVA_, "Save");
define(_ELIMINA_, "Delete");
define(_AGGIUNGI_, "Add");
define(_VISUALIZZA_, "View");
define(_INVIA_, "Send");
define(_CHIUSURA_, "Closed negotiation");
define(_INDIETRO_, "< Back");
define(_CONFERMA_, "Confirmed");
define(_DINIEGA_, "Denied");
define(_SOSPESO_, "Suspended");
define(_CHIUDI_, "CLOSE");
define(_RICHIESTA_, "Send Request");
define(_RICHIESTE_, "Request");
define(_MODIFICA_, "Edit");
define(_ATTESA_, "Wait");
define(_CONFERMATO_, "Confirmed");
define(_ANTEPRIMA_, "Preview");
define(_G_LINK_, "Regenerate Links");
define(_ACCEDI_, "Login");
define(_REGISTRATI_, "Register");
define(_CAMBIA_, "Change");
define(_EXTRA_B_, "Collateral negotiations");
define(_ALL_, "All");
define(_SELEZIONA_B_, "Select");

//MSG RISPOSTA
define(_PRODOTTO_CATEGORIA_, "The category you are clearing is associated with some products.");
define(_CAMPI_OB_CAT_, "Select at least one category.");
define(_EMAIL_ER_, "E-Mail field not valid");
define(_SMART_CARD_, "Smart Card and press enter login.");
define(_OK_INVIO_LINK_, "<h2>You have been sent an email with a confirmation link recording</h2>");
define(_PASSWORD_ERR_, "<h2>Password is incorrect</h2>");
define(_TRA_INCORSO_, "For these products are already in the negotiations:");
define(_CAMPI_OB_, "Parameters marked with an asterisk are required.");
define(_CAMPI_OB_TR_, "Enter the text for all languages.");
define(_CAMPI_PAS_NO_, "Invalid Password");
define(_CAMPI_PAS_UG_, "Password does not match confirmation");

define(_OK_ATTIVAZINE_, "Activation was successful");

define(_MITTENTE_, "From");
define(_DESTINATARIO_, "Address");
define(_OGGETTO_, "Subject");
define(_DATA_, "Date");
define(_OGGETTO_MAIL_, "Request a quote for ");
define(_MESSAGGIO_, "Message");
define(_ALLEGATO_, "Enclosure");

//MAIL DI RISPOSTA
define(_N_RISP_, "<br /><br />Automatically generated message please do not respond.");
define(_OGG_DATI_UTENTE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Data recording");
define(_MSG_DATI_UTENTE_, "<p>Dear user,<br /> 
we confirm successful registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
Here are your Username and Password: <br>
<br />
USER: {%user%} <br />
<br />
PASSWORD: {%Password%} <br />
<br /> <br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_AGG_I_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_AGG_I_, "<p>Dear {%utente%},<br /> 
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has assigned the role of Managing Director of the Company in the Catalogue Goods and Services Suppliers Expo 2015. 
Can access the catalog with the logon credentials that are already in its possession 
and then select the / Enterprise which is already written for you accederne to content. <br /><br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_REGISTRAZIONE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_NEW_REG_, "<p>Dear {%utente%},
���� <br /> we confirm the registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
By clicking on the link below which will activate your registration. <br />
���� {%link%} <br /><br />
���� Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_DELETE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Cancellation Personal Profile");
define(_MSG_DEL_REG_DEF_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
carried out the cancellation of his role as Chief Executive Officer of the Company. <br> 
Since it is not included on other companies in the Catalog its user has been deleted and can no 
longer use the login credentials in his possession. <br> For any information to contact regarding the 
direction of EXPO. 
<br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_MSG_DEL_REG_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>,as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has reset the memory of his role as Chief Executive Officer of the Company. <br />
Can still access the catalog with the logon credentials that are already in its possession and then select the / Enterprise which is already written for you accederne to content.
    <br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_BACHECA_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - negotiation ID #{%idBacheca%}");
define(_MSG_NOTIFICA_TRA_, "<p> Dear {%user%}  <br />
���� the user <strong>{%mittente%}</ strong> has sent you a message. <br />
���� You can find it in the Message section of your personal area of the Goods and Services Catalog Suppliers Expo 2015. <br />
���� <br />
���� Regards <br /> </ p>{%TITLESITE%}
    <br />-----------------------------------<br />
    <p>Gentile {%utente%} ,<br />
    l&#39;utente  <strong>{%mittente%}</strong> ti ha inviato un messaggio. <br />
    Lo trovi nella sezione Bacheca della tua area personale del Catalogo Fornitori Beni e Servizi Expo 2015.<br />
    <br />
    Distinti Saluti <br /> </p>{%TITLESITE%}"._N_RISP_);

define(_OGG_NOT_CD_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi � Bacheca Messaggi]  Trattativa ID #{%idTrattativa%}  ({%oggetto%})");
define(_MSG_NOT_CD_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the Enterprise {%Impresa%}
<strong>refused</strong> the closing of the deal. And &#39;possible to read the <strong>reasons</strong> for the refusal and send
message logging Showcase Area Board of Participants.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l'Impresa {%Impresa%} 
ha <strong>rifiutato</strong> la chiusura della trattativa. E' possibile leggere le motivazioni del rifiuto ed inviare 
un messaggio accedendo nell'Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CD_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi � Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_P_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation with Company {%Impresa%}, we inform you that the Participant {%mittente%} 
<strong>refused</strong> the closure of the deal. <br>
You can read the reasons for the refusal and send a message logging Area Board
Suppliers of Goods & Services Catalog.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha <strong>rifiutato</strong> la chiusura della trattativa.<br> 
E' possibile leggere le motivazioni del rifiuto ed inviare un messaggio accedendo nell'Area Bacheca 
del Catalogo Fornitori Beni & Servizi.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation,
we inform you that the company has <strong>confirmed</strong> {%Impresa%}
the closure of the negotiation<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto,
la informiamo che l'Impresa  {%Impresa%} ha <strong>confermato</strong> 
la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation,
we inform you that Participant {%mittente%} has confirmed the closing of the deal.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha confermato la chiusura della trattativa<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_I_, "<p>Good morning / Good evening , <br>
with reference to the aforementioned negotiation,
we inform you that company {%Impresa%} responsible for sealing the deal.
It requires you to confirm whether or not the outcome of the Showcase Participants accessing Area Board.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera ,<br>
con riferimento alla Trattativa Commerciale in oggetto, 
la informiamo che l'Impresa {%Impresa%} ha proceduto alla chiusura della trattativa. 
Si richiede di confermarne o meno l'esito accedendo nell'Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that Participant {%mittente%} responsible for sealing the deal. <br>
It requires you to confirm whether or not the outcome accessing Area Wall Suppliers Catalog Goods & Services<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%},  
la informiamo che il partecipante {%mittente%} ha proceduto alla chiusura della trattativa.<br>
Si richiede di confermarne o meno l'esito accedendo nell'Area Bacheca del Catalogo Fornitori Beni & Servizi<br />
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);

//GESTIONE LINGUE
define(_ITALIANO_, "Italian");
define(_INGLESE_, "English");
define(_FRANCESE_, "French");

//MODIFICA 25/09/2013
define(_FILE_NO_UP_, "<h2>Unable to upload.</h2>");
define(_FILE_MAX_, "<h2>File is too big.</h2>");
define(_ENTRA_SC_, "Sign in using Smart Card");
define(_ENTRA_LOG_, "Sign in using credentials recorded");
define(_DIM_PAS_, "Forgot your password?");
define(_REG_, "Registering");

//MODIFICA 26/09/2013
define(_RICERCA_, "Find");
define(_IMPRESE_, "Companies");
define(_AGGIUNGI_, "Add parameter");
define(_CERCA_PAR_, "SEARCH FOR PARAMETERS");
define(_ANNULLA_, "Cancel");

//MODIFICA 30/09/2013
define(_UTENTE_RLN_, "Link forwarded.");
define (_NUMERO_OGGETTI_BACHECA_,"There are %n% messages");

//MODIFICA 01/10/2013
define(_NO_PRODOTTI_,"To be able to enter new Products/Services, you must subscribe to at least one Product Category");

define (_AGGIUNGI_CATEGORIA_,"Add Product Category");
define (_GESTISCI_CATEGORIA_,"Manage Product Category");
define (_NESSUNA_CATEGORIA_,"None Product Category registered");

// MODIFICA DEL 02/10/2013
define (_NUMERO_OGGETTI_BACHECA_,"You have %n% messages");
define(_DESCRIZIONE_FILE_,"Description");
define(_BILANCI_FILE_,"Budget");
define(_INTERNAZIONALIZZAZIONE_FILE_,"Internationalization");
define(_REFERENZE_FILE_,"References");
define(_QUALITA_FILE_,"Quality");
define(_RETI_FILE_,"BusinessNetworks");

define(_PRODOTTO_FILE_,"Product/Service");
define(_CERTIFICAZIONE_FILE_,"Certification");
define(_ASPETTI_FILE_,"Aspects");
define(_BREVETTI_FILE_,"Patents");

define(_FILELINK_V_, "See File/Web link below for details<br>");

// MODIFICA DEL 04/10/2013
define(_NO_TRATTATIVA_,"No negotiations found");
define(_NO_BACHECA_,"No message on message board");
define(_NO_PREFERITI_,"No favourite present");
define(_NO_RICERCA_,"The search has produced no results");
//MODIFICA DEL 07/10/2013
define(_NO_PAR_RICERCA_,"Select at least one search criterion");
define(_SVUTA_RICERCA_,"Refresh search");
define(_NO_RICHIESTA_,"Reset requests");
//MODIFICA DEL 16/10/2013
define(_NON_AB_,"<h2>YOU ARE NOT AUTHORISED TO ACCESS THIS WEBSITE</h2>");

//MODIFICA 21/10/2013
define(_TIMEOUT_LOGIN_,"TIME OUT LOG-IN");
define(_TEXT_TIMEOUT_LOGIN_,"Your log-in has timed out. <br>For security reasons, please log in again.");
define(_LOGIN_,"Log-in");

//MODIFICA 22/10/2013
define(_TEXT_HOME_1_,"<span>Welcome <br>to the Companies Catalogue<br>for Expo 2015 Participants</span><br>
        A virtual environment that puts you in contact with companies and professionals who can provide
		services or products to help you in the design, construction and furnishing of your pavilion.");
define(_TEXT_HOME_2_," <span><br>What can you do with the catalogue?</span>");
define(_TEXT_HOME_3_,"For info and support, write to: <br>info-COcatalogue@expo2015.org");
define(_TEXT_HOME_4_,"<div>Search for companies or professionals <br>by category</div>");
define(_TEXT_HOME_5_,"<div>Refine your search on the basis of <br>references, certification or other <br>distinctive features of the companies</div>");
define(_TEXT_HOME_6_,"<div>If you know the name of the company, <br>you may search for it directly</div>");
define(_TEXT_HOME_7_,"<div>Once you have found a company or companies <br>of interest, contact them directly <br>by sending a request via the messaging system</div>");
define(_TEXT_HOME_8_,"<div>Carry out negotiations <br>in the dedicated secure area</div>");

//MODIFICA 28/10/2013
define(_EXPO2015_,"EXPO 2015 Milano");
//MODIFICA 31/10/2013
define(_PARAMETRI_RICERCA_,"<strong>Search parameters:</strong> ");
//MODIFICA 04/11/2013
define(_U_FATTURATO_ESERC,"Revenue last financial year");
define(_IsPrestatoreDiServizi_,"Services provider");

//MODIFICA 05/11/2013
define(_TEXT_HOME_9_,"<div>Search eco-friendly and innovative products on SIEXPO</div>");
define(_P_MASTER_FILE_, "Masters/PhD in its business");
define(_P_CERTIFICAZIONE_FILE_, "SOA Certification");
define(_CERTIFICAZIONI_FILE_, "Certification");

//MODIFICA 06/11/2013
define(_RISPONDI_, "Reply");
define(_VISUALIZZA_TRATTATIVA_, "NEGOTATIONS");
//MODIFICA 15/11/2013
define(_ALLEGA_DOC_, "Depth document:");
define(_ALLEGA_LINK_, "Links for further information:");

//MODIFICA 20/11/2013
define(_NO_ASSSOCIAZIONI_, "No Association selected");
define(_NO_LINGUE_, "No language selected");
define(_ADDETTI_, "Employees");
define(_FATTURATO_5_, "Revenue");
define(_A_MODELLO_, "Organizational Model 231/01");
define(_P_MASTER_R_, "Masters/PhD in its business");
define(_R_P_CERTIFICAZIONE_, "SOA Certification");
define(_CARATTERISTICHE_, "FEATURES");

//MODIFICA 22/11/2013
define(_TEXT_HOME_9_,'<div>Ricerca materiali e prodotti<br>sostenibili ed innovativi sul <a href="javascript:invia(\'http://www.siexpo2015.it/index.phtml?Id_VMenu=295\')">SIEXPO</a></div>');
define(_TEXT_INFO_P_,'If the company has existed for less than 5 years, the sum of revenues for completed years is provided since the founding of the company');
define(_N_DIP_, "Number of employees");
define(_R_RETE_IMP_R_, "Membership in associations");
define(_INTERNAZIONALIZZAZIONE_, "Degree of company internationalization");
define(_RICERCA_TEXT_, "Search in results");
define(_RICERCA_, "Search");
define(_SVUTA_RICERCA_,"New search");
define(_RICHIESTA_R_, "Contact the company selected");
define(_R_N_PARAMETRI_, "<strong>Selected parameters:</strong>");


//MODIFICA 06/12/2013
define(_IS_SIEXPO_, "Available in Siexpo Catalogue");

define(_FAQ_DOMANDA_,"Domanda");
define(_FAQ_PUBBLICATA_,"Pubblicata");
define(_FAQ_AZIONE_,"Action");
define(_FAQ_MODIFICA_,"Modifica");
define(_FAQ_ELIMINA_,"Elimina");
define(_FAQ_PUBBLICA_,"Pubblica Faq");
define(_FAQ_VISUALIZZA_,"View");
define(_FAQ_OFFLINE_,"Metti Offline");
define(_FAQ_OP_MODIFICA_,"ModificaFaq");
define(_FAQ_OP_VISUALIZZA_,"VisualizzaFaq");
define(_FAQ_OP_INSERIMENTO_,"InserimentoFaq");
define(_FAQ_MSG_ELENCO_,"N.B. The list refers only to published Faq");
define(_FAQ_MSG_ELENCO_2_,"N.B. The question will be reviewed prior to publication by EXPO");
define(_FAQ_TOT_QUERY_,"Total FAQ: ");
define(_FAQ_INDIETRO_,"Back");
define(_FAQ_ARGOMENTO_,"Topic");
define(_FAQ_RICERCA_LIBERA_,"Search");
define(_FAQ_NO_RISULTATI_,"No Faq corresponding to selected filters");
define(_FAQ_MSG_OFFLINE_,"Vuoi mettere offline questa Faq?");
define(_FAQ_MSG_ONLINE_,"Vuoi pubblicare questa Faq?");
define(_FAQ_MSG_ELIMINA_,"Vuoi eliminare questa Faq?");
define(_FAQ_MSG_OFFLINE_OK_,"La Faq è offline");
define(_FAQ_MSG_ONLINE_OK_,"La Faq è pubblicata");
define(_FAQ_MSG_MODIFICA_OK_,"La Faq è stata modificata");
define(_FAQ_MSG_ELIMINA_OK_,"La Faq è stata cancellata");
define(_FAQ_MSG_INSERIMENTO_OK_,"La Faq è stata inserita correttamente");
define(_FAQ_ALT_OFFLINE_,"Metti Offline");
define(_FAQ_ALT_ONLINE_,"Pubblica Faq");
define(_FAQ_ALT_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_VISUALIZZA_,"View Faq");
define(_FAQ_TITLE_MODIFICA_,"Modifica Faq");
define(_FAQ_TITLE_ONLINE_,"Pubblica Faq");
define(_FAQ_TITLE_OFFLINE_,"Metti Offline");
define(_FAQ_TITLE_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_INDIETRO_,"Torna alla pagina della Faq");

define(_FILTRI_RICERCA_LABEL_,"Search Filters");
define(_FAQ_PULSANTE_AGGIORNA_,"Update");

define(_RISULTATI_TROVATI_,"results");
define(_RISULTATI_PER_PAGINA_,"results per page");

define(_TUTTE_,"All");
define(_NESSUNA_,"None");
define(_INVERTI_,"Invert");

//MODIFICA  18/04/2014 PROFESSIONISTI

define(_TIT_1_LP_, "Professional information");
define(_TIT_1_STUDIO_, "Studio information");
define(_N_DIP_ORDINI_, "Number of employees or external personnel");

define(_QUALIT_LP_, "Characteristics of the professional");
define(_QUALIT_STUDIO_, "Characteristics of the studio");
define(_SEDI_LP_, "Degree of internationalization of the professional");
define(_SEDI_STUDIO_, "Degree of internationalization of the studio");
define(_RAGSOC_PROF_, "Name");
define(_APP_RETE_IMP_PROF_, "Membership in collective bodies (e.g. networks, consortia, associations)");

define(_IMPRESA_OFFLINE_PRODOTTI_, "L'impresa &egrave; offline. I prodotti/servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_LP_, "Il Professionista &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_STUDIO_, "Lo studio &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");

define(_PROFESSIONISTA_, "Professional");
define(_STUDIO_, "Studio");
define(_PUBBLICA_ALL_PROF_, "Public services");

define(_SERVIZI_, "List of services");
define(_SERVIZIO_, "Service");

//MODIFICA  15/05/2014
define(_P_SIEXPO_, "Sustainable products verified by SIEXPO");
define(_ANTEP_IMP_,'<a draggable="false" href="javascript:inviaSiexpo(document.siexpoForm,\'http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295\')"><label for="isSiexpo" >Present in the Catalogue <img src="/images/expo2015/siexpo.jpg"  style="vertical-align: middle;margin-left: 5px;width: 100px"/></label></a>');
define(_NO_RETI_,'No network selected');
define(_ANT_NOME_RETE_, "Network: ");
define(_DESCR_RETE_, "Description: ");
define(_ANT_CONTATTO_RETE, "Contact of Network: ");
define(_IMPRESE_RETE_,"<strong>Other companies and members of the Network to Catalogue</strong><br/>");
define(_ANT_IMP_CAPOG_,"Enterprise group leader");

define(_DISCLAIMER_CATEGORIE_DEFAULT_ ,"The products and services in this category are provided by Expo 2015 Partners or Sponsors, whose product/service portfolios are detailed in the <b>Official Partners Catalogue</b> (http://catalogue.expo2015.org), however other companies may be found by continuing the search.");

define(_ACCESSO_NEGATO_,"NON SEI ABILITATO ALLA VISUALIZZAZIONE");

define(_IMPRESA_TEMPORANEAMENTE_OFFLINE_,"IMPRESA TEMPORANAMENTE OFF LINE");
?>