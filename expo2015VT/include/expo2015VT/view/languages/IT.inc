<?php
//TITOLI TAB
define(_AZIONE_, "Azione");
define(_EMAIL_, "E-mail");
define(_NOME_, "Nome");
define(_COGNOME_, "Cognome");
define(_TELEFONO_, "Telefono");
define(_PRODOTTI_, "Prodotti");
define(_PRODOTTO_, "Prodotto");
define(_RAGIONESOCILE_, "Ragione Sociale");
define(_PARAMETRI_, "Parametri");
define(_BENVENUTO_, "Benvenuto");
define(_DISCONNETTI_, "Disconnetti");
define(_CERTIFICAZIONI_, "Certificazioni e Riconoscimenti");
define(_ASPETTI_, "Innovazione e sostenibilit&agrave;");
define(_BREVETTI_, "Brevetti");
define(_C_MERC_, "Categorie merceologiche");

define(_C_MERC_T_, "Categorie<br>Merceologiche");
define(_DATI_GENERALI_T_, "Dati<br>Generali");
define(_CERTIFICAZIONI_T_, "Certificazioni e<br>Riconoscimenti");
define(_ASPETTI_T_, "Innovazione<br>Sostenibilit&agrave;");
define(_BREVETTI_T_, "Brevetti<br><br>");


define(_C_LOGO_, "Cambia Logo");
define(_TIT_1_, "Dati provenienti dal Registro Imprese");
define(_TIT_2_, "Contatti");
define(_TIT_3_, "Fatturati");

define(_DATI_GENERALI_, "Dati Generali Imprese");
define(_DETTAGLI_, "Dettagli");
define(_QUALIT_T_, "Caratteristiche");
define(_QUALIT_, "Caratteristiche dell&#39;impresa/professionista");
define(_ASSOCIZIONI_, "Associazioni");
define(_ASSOCIZIONI_RETI_, "Forme di aggregazione");
define(_RAGSOC_, "Ragione Sociale");
define(_CAPISOC_, "Capitale Sociale");
define(_U_FATTURATO_, "Somma ultimi 5 fatturati *");

define(_R_U_FATTURATO_, "Somma ultimi 5 fatturati");
define(_R_U_FATTURATO_1_, "(&euro; Milioni)");
define(_R_U_FATTURATO_2_, "(&euro;x1000)");

define(_ALLEGA_ELENCO_, "<strong>Elenco referenze più qualificanti</strong>");
define(_SEDE_LEG_, "Sede Legale");
define(_TEL_, "Tel");
define(_DESCRIZIONE_, "Descrizione");
define(_DESCRIZIONE_ATT_, "Descrizione attivit&agrave;");
define(_N_DIP_, "Numero addetti");

define(_SEDI_, "Grado di internazionalizzazione dell&#39;Impresa");
define(_S_ESTERA_, "Presenza sede all&#39;estero");
define(_NET_INTER_, "Appartenenza a Network internazionale");
define(_T_IMPRESA_, "Tipo Impresa");
define(_GENER_CONT_, "General contractor");
define(_PRODUTTORE_, "Produttore");
define(_DISTRIBUTORE_, "Distributore");
define(_REFERENZE_, "Referenze");
define(_PARTECIPZIONI_EXPO_, "Fornitore di altre Expo o di altri grandi eventi internazionali");
define(_PAR_INTERNAZIONALI_, "Referenze Internazionali");
define(_ESPERIENZA_IN_, "Esperienze Internazionali");
define(_CON_LINGUE_, "Conoscenze lingue");
define(_LINGUE_, "lingue");
define(_SELEZIONA_, "Seleziona...");
define(_GREEN_, "Attenzione al tema della sostenibilit&agrave; Green e Sociali");
define(_CERTIFICAZIONI2_, "Certificazioni");
define(_APP_ASS_, "Appartenenza ad Associazioni di Categoria");
define(_APP_RETE_IMP_, "Appartenenza a reti di imprese, consorzi o altre forme di aggregazione");
define(_R_RETE_IMP_, "Rete di Imprese");
define(_R_N_PARAMETRI_, "Parametri selezionati: ");
define(_R_SINDACALE_, "Presenza Collegio Sindacale");
define(_R_P_CERTIFICAZIONE_, "Certificazione SOA");

define( _SINDACALE_, "Presenza del Collegio Sindacale");
define(_A_MODELLO_, "Modello Organizzativo 231/01");
define(_P_LEGALITA_, "Rating di legalit&agrave;");
define(_P_MASTER_R_, "Master Specialistico");
define(_P_MASTER_, "Professionista in possesso di Master Specialistico/Dottorato di ricerca");
define(_P_CERTIFICAZIONE_, "Certificazione SOA");

define(_OSSERVAZIONI_, "Motivazione:");
define(_PARTECIPANTE_, "Partecipante");
define(_FORNITORE_, "Fornitore");
define(_VALORE_, "Valore");
define(_DATA_CHIUSURA_, "Data chiusura");
define(_STATO_, "Stato");
define(_IMPRESA_, "Impresa");
define(_PREFERITI_, "Preferiti");
define(_TESTO_MOD_IMG_, "<h2>Modifica Logo</h2>
Seleziona una nuovo Logo da caricare<br>
Il tuo logo sar&agrave; ridotto, se necessario, automaticamente fino alle dimensioni di 120 x 120.");
define(_PASS_, "Password (min. 8 caratt.)*");
define(_PASSWORD_, "Password");
define(_R_PASS_, "Reinserisci Password*");
define(_SEND_OK_, "<h2>Messaggio inviato con successo!</h2>");
define(_FILE_PRESENTE_, "Un file presente ");
define(_FILE_V_, "Vedere File sottostante per i dettagli<br>");
define(_LINK_V_, "Vedere Link Web sottostante per i dettagli<br>");
define(_NOTE_1_, "Modello di organizzazione e gestione volto a prevenire la responsabilit&agrave; oggettiva delle imprese in sede penale derivante dal Decreto Legislativo 8 giugno 2001 n. 231.");
define(_NOTE_2_, "Rating etico gestito dalla Autorit&agrave; Garante della Concorrenza e del Mercato (AGCM) derivante dal Decreto legge 24 marzo 2012, n. 29 allo scopo di promuovere in Italia principi etici nei comportamenti aziendali.");
define(_NOTE_3_, "I prodotti che hanno delle trattative aperte non verranno visualizzati.");
define(_NOTE_4_, "<br>Allegare file in PDF<br><br>");
define(_NOTE_5_, "<br>* Obbligatorio in caso di rifiuto<br><br>");
define(_NOTE_6_, "Allegare un eventuale documento di presentazione del prodotto. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_7_, "Allegare un eventuale documento di descrizione/attestazione delle certificazioni relative al prodotto. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_8_, "Allegare un eventuale documento di descrizione degli aspetti di innovazione e sostenibilit&agrave; che il prodotto presenta. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_9_, "Allegare un eventuale documento di descrizione/attestazione del brevetto relativo al prodotto. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_10_, "Allegare un eventuale documento in cui siano approfonditi gli aspetti contabili riferiti ai fatturati degli ultimi 5 anni. Il documento deve quindi arricchire l'informazione sul fatturato. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i paesi partecipanti non vedranno alcun contenuto");
define(_NOTE_11_, "Allegare un eventuale documento in cui si dichiara come è composta la rete di imprese, i consorzi o le altre forme di aggregazione che si vanno a dichiarare. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_12_, "Allegare un eventuale documento in cui si approfondisce il grado di internazionalizzazione dell'impresa. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
define(_NOTE_13_, "Allegare un eventuale documento in cui si descrivono le referenze dell'impresa. Se inserito, il documento deve preferibilmente contenere le informazioni nelle tre lingue previste. In caso di mancato inserimento i Paesi Partecipanti non vedranno alcun contenuto");
//define(_NOTE_4_, "<br>Allegare file in PDF<br><br>");


//BOTTONI
define(_SALVA_, "Salva");
define(_ELIMINA_, "Elimina");
define(_AGGIUNGI_, "Aggiungi");
define(_VISUALIZZA_, "Visualizza");
define(_INVIA_, "Invia");
define(_CHIUSURA_, "Chiusura trattativa");
define(_INDIETRO_, "< Indietro");
define(_CONFERMA_, "Conferma");
define(_DINIEGA_, "Diniega");
define(_SOSPESO_, "Sospesa");
define(_CHIUDI_, "CHIUDI");
define(_RICHIESTA_, "Contatta l'azienda");
define(_RICHIESTE_, "Richieste");
define(_MODIFICA_, "Modifica");
define(_ATTESA_, "Attesa");
define(_CONFERMATO_, "Confermato");
define(_ANTEPRIMA_, "Anteprima");
define(_G_LINK_, "Rigenera Link");
define(_ACCEDI_, "Accedi");
define(_REGISTRATI_, "Registrati");
define(_CAMBIA_, "Cambia Impresa");
define(_EXTRA_B_, "Trattativa extra bacheca");
define(_ALL_, "Tutti");
define(_SELEZIONA_B_, "Seleziona");
define(_NEW_PROD_, "Nuovo prodotto");


//MSG RISPOSTA
define(_PRODOTTO_CATEGORIA_, "La categoria che stai deselezionando risulta associata ad alcuni prodotti.");
define(_CAMPI_OB_CAT_, "Selezionare almeno una categoria.");
define(_SMART_CARD_, "Inserisci Smart Card e premi accedi.");
define(_EMAIL_ER_, "Campo E-Mail non valido");
define(_OK_INVIO_LINK_, "<h2>Ti è stato inviata una E-Mail con un link di conferma registrazione</h2>");
define(_PASSWORD_ERR_, "<h2>La password non &egrave; corretta</h2>");
define(_TRA_INCORSO_, "Per questi prodotti sono gi&agrave; presenti delle trattative:");
define(_CAMPI_OB_, "I parametri contrassegnati con l'asterisco sono obbligatori.");
define(_CAMPI_OB_TR_, "Inserire il testo per tutte le lingue.");
define(_CAMPI_PAS_NO_, "Password non valida");
define(_CAMPI_PAS_UG_, "Password non corrisponde a quella di conferma");
define(_OK_ATTIVAZINE_,"Attivazione avvenuta con successo");

define(_UTENTE_PRESENTE_,"Delegato gi&agrave registrato!");
define(_UTENTE_INS_,"Delegato aggiunto con successo");
define(_UTENTE_UPD_,"I dati del delegato sono stati modificati con successo");
define(_UTENTE_DEL_,"Delegato eliminato con successo");

define(_MITTENTE_, "Mittente");
define(_DESTINATARIO_, "Destinatario");
define(_OGGETTO_, "Oggetto");
define(_DATA_, "Data");
define(_OGGETTO_MAIL_, "Richiesta preventivo per ");
define(_MESSAGGIO_, "Messaggio");
define(_ALLEGATO_, "Allegato");

//MAIL DI RISPOSTA
define(_N_RISP_, "<br /><br />Messaggio generato automaticamente si prega di non rispondere.");
define(_OGG_DATI_UTENTE_, "[EXPO2015 Catalogo Fornitori Beni & Servizi  Amministratore Sito]  Dati Utenza");
define(_MSG_DATI_UTENTE_, "<p>Gentile utente,<br>
le confermiamo l&#39;avvenuta registrazione al Catalogo Fornitori Beni e Servizi Expo 2015  <br />
Ecco le tue User e Password:<br>
<br />
USER: {%user%}<br />
<br />
PASSWORD: {%Password%}<br />
<br /><br />
Distinti Saluti <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_REGISTRAZIONE_, "[EXPO2015 Catalogo Fornitori Beni & Servizi  Amministratore Sito]  Notifica Attivazione Profilo Personale");
define(_MSG_NEW_REG_, "<p>Gentile {%utente%} ,
    <br /> le confermiamo l&#39;avvenuta registrazione al Catalogo Fornitori Beni e Servizi Expo 2015  <br />
    Cliccando sul link sottostante potr&agrave; attivare la sua registrazione. <br /> 
    {%link%}
    <br /><br />
    Distinti Saluti <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_AGG_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi  Amministratore Sito]  Notifica Attivazione Profilo Personale");
define(_MSG_AGG_I_, "<p>Gentile {%utente%} ,<br /> 
    le confermiamo che <strong>{%lg%}</strong>,in qualit&agrave; di Legale Rappresentante dell&#39;Impresa <strong>{%RagSoc%}</strong>,
 le ha assegnato il ruolo di Delegato di tale Impresa nel Catalogo Fornitori Beni e Servizi Expo 2015. Potr&agrave; accedere al Catalogo con le credenziali di accesso che sono gi&agrave; in suo possesso e successivamente selezionare la/le Imprese cui Lei risulta gi&agrave; iscritto per accederne ai contenuti. <br />
    <br />
    Distinti Saluti <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_DELETE_, "[EXPO2015 Catalogo Fornitori Beni & Servizi  Amministratore Sito]  Notifica Cancellazione Profilo Personale");
define(_MSG_DEL_REG_DEF_, "<p>Gentile utente ,
    <br /> le confermiamo che <strong>{%lg%}</strong>, 
    in qualit&agrave; di Legale Rappresentante dell&#39;Impresa <strong>{%RagSoc%}</strong>,
    ha effettuato la cancellazione del suo ruolo di Delegato di tale Impresa.<br>
    Dal momento che non risulta iscritto ad altre Imprese presenti nel Catalogo la sua utenza è stata cancellata e non potr&agrave; più utilizzare le credenziali di accesso in suo possesso.<br>
Per qualsiasi informazione al riguardo contatti la direzione di EXPO. <br />
    <br />
    Distinti Saluti <br /></p>{%TITLESITE%}"._N_RISP_);
define(_MSG_DEL_REG_, "<p>Gentile utente ,
    <br /> le confermiamo che <strong>{%lg%}</strong>, 
    in qualit&agrave; di Legale Rappresentante dell&#39;Impresa <strong>{%RagSoc%}</strong>,
    ha effettuato la cancellazione del suo ruolo di Delegato di tale Impresa. <br />
    Potr&agrave; comunque accedere al Catalogo con le credenziali di accesso che sono gi&agrave; in suo possesso e successivamente selezionare la/le Imprese cui Lei risulta gi&agrave; iscritto per accederne ai contenuti. <br />
    <br />
    Distinti Saluti <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_BACHECA_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idBacheca%}");
define(_MSG_NOTIFICA_TRA_, "<p>Gentile {%utente%} ,<br />
    l&#39;utente  <strong>{%mittente%}</strong> ti ha inviato un messaggio. <br />
    Lo trovi nella sezione Bacheca della tua area personale del Catalogo Fornitori Beni e Servizi Expo 2015.<br />
    <br />
    Distinti Saluti <br /> </p>{%TITLESITE%}<br />
    <br />-----------------------------------<br />
    <p>Dear {%utente%} ,<br />
    the user <strong>{%mittente%}</strong> has sent you a message.<br />
    You can find it in the Message section of your personal area of ​​the Goods and Services Catalog Suppliers Expo 2015. <br /><br />
    Regards <br /></p>{%TITLESITE%}"._N_RISP_);




define(_OGG_NOT_A_BACHECA_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Nuova Trattativa ID #{%idBacheca%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_, "<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l&#039;Impresa {%Impresa%}, 
la informiamo che {%mittente%} ha inviato una richiesta di offerta.<br>
&Egrave; possibile leggerne i dettagli accedendo nell&#039;Area Bacheca del Catalogo.<br>
<br />
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening , <br>
with reference to the aforementioned negotiation with Company {%Impresa%}
we inform you that {%mittente%} sent a request for quotation. <br>
E &#39;can read the details by logging in Area Wall catalog. <br>
<br />
Best Regards <br /> </ p>{%TITLESITE%}"._N_RISP_);

define(_OGG_NOT_CD_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_I_, "<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l&#039;Impresa {%Impresa%} 
ha <strong>rifiutato</strong> la chiusura della trattativa. E&#039; possibile leggere le motivazioni del rifiuto ed inviare 
un messaggio accedendo nell&#039;Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the Enterprise {%Impresa%}
<strong>refused</strong> the closing of the deal. And &#39;possible to read the <strong>reasons</strong> for the refusal and send
message logging Showcase Area Board of Participants.<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CD_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_P_, "<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l&#039;Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha <strong>rifiutato</strong> la chiusura della trattativa.<br> 
&Egrave; possibile leggere le motivazioni del rifiuto ed inviare un messaggio accedendo nell&#039;Area Bacheca 
del Catalogo Fornitori Beni & Servizi.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that the Participant {%mittente%} <strong>refused</strong> the closure of the deal. <br>
And &#39;possible to read the reasons for the refusal and send a message logging Area Board
Suppliers of Goods & Services Catalog.<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_I_, "<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l&#039;Impresa {%Impresa%} ha <strong>confermato</strong> 
la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the company has <strong>confirmed</strong> {%Impresa%}
the closure of the negotiation<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_P_, "<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l&#039;Impresa {%Impresa%}, 
la informiamo che {%mittente%} ha confermato la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that {%mittente%} has confirmed the closing of the deal..<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_C_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_C_TRA_I_, "<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l&#039;Impresa {%Impresa%} 
ha proceduto alla chiusura della trattativa. 
Si richiede di confermarne o meno l&#039;esito accedendo nell&#039;Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation,
we inform you that the company {%Impresa%} has proceeded to close the deal. <br>
It requires you to confirm whether or not the outcome of the Showcase Participants accessing Area Board.<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_C_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_C_TRA_P_, "<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l&#039;Impresa {%Impresa%},  
la informiamo che {%mittente%} ha proceduto alla chiusura della trattativa.<br>
Si richiede di confermarne o meno l&#039;esito accedendo nell&#039;Area Bacheca del Catalogo Fornitori Beni & Servizi<br />
Cordiali Saluti<br /></p>{%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that {%mittente%} has proceeded to close the deal. <br>
It requires you to confirm whether or not the outcome accessing Area Wall Suppliers Catalog Goods & Services<br />
Best Regards <br /> </ p> {%TITLESITE%}"._N_RISP_);
//GESTIONE LINGUE
define(_ITALIANO_, "Italiano");
define(_INGLESE_, "Inglese");
define(_FRANCESE_, "Francese");
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//MODIFICA 25/09/2013

define(_FILE_NO_UP_, "<h2>Impossibile eseguire l'upload.</h2>");
define(_FILE_MAX_, "<h2>Il file è troppo grande.</h2>");
define(_ENTRA_SC_, "Entra con Smart Card");
define(_ENTRA_LOG_, "Entra con credenziali registrate");
define(_DIM_PAS_, "Dimenticato la password?");
define(_REG_, "Registrazione");

//MODIFICA 26/09/2013

define(_RICERCA_, "Cerca");
define(_IMPRESE_, "Imprese");
define(_AGGIUNGI_, "Aggiungi parametro");
define(_CERCA_PAR_, "CERCA PER PARAMETRI");
define(_ANNULLA_, "Annulla");

//MODIFICA 30/09/2013
define(_UTENTE_RLN_, "Link inoltrato.");

//MODIFICA 01/10/2013
define(_NO_PRODOTTI_,"Per poter inserire nuovi prodotti/servizi è necessario iscrivere l'Impresa ad almeno una Categoria Merceologica");

define (_AGGIUNGI_CATEGORIA_,"Aggiungi categoria");
define (_GESTISCI_CATEGORIA_,"Gestisci categoria");

define (_NESSUNA_CATEGORIA_,"Nessuna categoria iscritta");

// MODIFICA DEL 02/10/2013
define (_NUMERO_OGGETTI_BACHECA_,"Ci sono %n% messaggi");

define(_DESCRIZIONE_FILE_,"Descrizione");
define(_BILANCI_FILE_,"Bilanci");
define(_INTERNAZIONALIZZAZIONE_FILE_,"Internazionalizzazione");
define(_REFERENZE_FILE_,"Referenze");
define(_QUALITA_FILE_,"Qualita");
define(_RETI_FILE_,"RetiImpresa");

define(_PRODOTTO_FILE_,"Prodotto/Servizio");
define(_CERTIFICAZIONE_FILE_,"Certificazione");
define(_ASPETTI_FILE_,"Aspetti");
define(_BREVETTI_FILE_,"Brevetti");

define(_FILELINK_V_, "Vedere File/Link Web sottostanti per i dettagli<br>");

// MODIFICA DEL 04/10/2013
define(_NO_TRATTATIVA_,"Nessuna Trattativa presente");
define(_NO_BACHECA_,"Nessun Messaggio in Bacheca");
define(_NO_PREFERITI_,"Nessun Preferito Presente");
define(_NO_RICERCA_,"La ricerca non ha prodotto risultati");

//MODIFICA DEL 07/10/2013
define(_NO_PAR_RICERCA_,"Selezionare almeno un parametro di ricerca.");
define(_SVUTA_RICERCA_,"Nuova ricerca");
define(_NO_RICHIESTA_,"Azzera richieste");
//MODIFICA DEL 16/10/2013
define(_NON_AB_,"<h2>NON SEI AUTORIZZATO A VISUALIZZARE QUESTO SITO.</h2>");

//MODIFICA 21/10/2013
define(_TIMEOUT_LOGIN_,"TIME OUT LOG-IN");
define(_TEXT_TIMEOUT_LOGIN_,"&Egrave; passato troppo tempo dall&#039;ultimo log-in.<br>Per motivi di sicurezza le chiediamo di rieseguire un log-in nel portale");
define(_LOGIN_,"Log-in");

//MODIFICA 22/10/2013
define(_TEXT_HOME_1_,"<span>Benvenuto<br>nel Catalogo<br>per i Partecipanti ad Expo 2015</span><br>
        L&rsquo;ambiente virtuale che ti mette in contatto con le imprese ed i professionisti,
        che potranno supportarti nella progettazione, costruzione ed allestimento
        del tuo padiglione, attraverso l&#039;erogazione di servizi o la fornitura di prodotti.");
define(_TEXT_HOME_2_," <span><br>Cosa puoi fare con il catalogo?</span>");
define(_TEXT_HOME_3_,"Per info e supporto scrivere a:<br>info-COcatalogue@expo2015.org");
define(_TEXT_HOME_4_,"<div>Ricercare l&#039;impresa o il professionista<br>d&#039;interesse per categoria merceologica</div>");
define(_TEXT_HOME_5_,"<div>Raffinare la ricerca in base alle loro<br>referenze, certificazioni o altre<br>caratteristiche distintive delle imprese</div>");
define(_TEXT_HOME_6_,"<div>Se conosci l&#039;impresa<br>puoi ricercarla direttamente</div>");
define(_TEXT_HOME_7_,"<div>Dopo aver trovato una o più imprese di tuo<br>interesse contattale inviando una richiesta<br>tramite il sistema di messaggistica</div>");
define(_TEXT_HOME_8_,"<div>Gestisci eventuali trattative<br>nell&#039;area dedicata e sicura</div>");

//MODIFICA 28/10/2013
define(_EXPO2015_,"EXPO 2015 Milano");

//MODIFICA 31/10/2013
define(_PARAMETRI_RICERCA_,"<strong>Parametri di ricerca:</strong> ");

//MODIFICA 04/11/2013
define(_U_FATTURATO_ESERC,"Fatturato ultimo esercizio");
define(_IsPrestatoreDiServizi_,"Prestatore di servizi");

//MODIFICA 05/11/2013
define(_P_MASTER_FILE_, "Master Specialistico/Dottorato");
define(_P_CERTIFICAZIONE_FILE_, "Certificazione SOA");
define(_CERTIFICAZIONI_FILE_, "Certificazioni");

//MODIFICA 06/11/2013
define(_RISPONDI_, "Rispondi");
define(_VISUALIZZA_TRATTATIVA_, "TRATTATIVA");

//MODIFICA 15/11/2013
define(_ALLEGA_DOC_, "Documento di approfondimento");
define(_ALLEGA_LINK_, "Link di approfondimento");

//MODIFICA 20/11/2013
define(_NO_ASSSOCIAZIONI_, "Nessuna Associazione selezionata");
define(_NO_LINGUE_, "Nessuna lingua selezionata");
define(_ADDETTI_, "Addetti");
define(_FATTURATO_5_, "Fatturato");
define(_A_MODELLO_, "Modello Organizzativo 231/01");
define(_P_MASTER_R_, "Master Specialistico");
define(_R_P_CERTIFICAZIONE_, "Certificazione SOA");
define(_CARATTERISTICHE_, "CARATTERISTICHE");

//MODIFICA 22/11/2013
//define(_TEXT_HOME_9_,'<div>Ricerca materiali e prodotti<br>sostenibili ed innovativi sul <a href="javascript:invia(\'http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295\')">SIEXPO</a></div>');
define(_TEXT_HOME_9_,"<div>Ricerca materiali e prodotti<br>sostenibili ed innovativi sul <a draggable=\"false\" href=\"javascript:inviaSiexpo(document.siexpoForm,'http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295')\">Catalogo SIEXPO</a></div>");
define(_TEXT_INFO_P_,'Se l&#039;impresa esiste da meno di 5 anni è riportata la somma dei fatturati degli esercizi conclusi dalla nascita dell’impresa');
define(_N_DIP_, "Numero addetti");
define(_R_RETE_IMP_R_, "Aggregazioni di Imprese");
define(_INTERNAZIONALIZZAZIONE_, "Internazionalizzazione");
define(_RICERCA_TEXT_, "Cerca nei risultati");
define(_RICERCA_, "Cerca");
define(_SVUTA_RICERCA_,"Nuova ricerca");
define(_RICHIESTA_R_, "Contatta azienda/e selezionata/e");
define(_R_N_PARAMETRI_, "<strong>Parametri selezionati:</strong>");

//MODIFICA 06/12/2013
define(_IS_SIEXPO_, "Presente nel Catalogo");

//MODIFICA DEL 27/12/2013
define(_FAQ_DOMANDA_,"Domanda");
define(_FAQ_PUBBLICATA_,"Pubblicata");
define(_FAQ_AZIONE_,"Azione");
define(_FAQ_MODIFICA_,"Modifica");
define(_FAQ_ELIMINA_,"Elimina");
define(_FAQ_PUBBLICA_,"Pubblica Faq");
define(_FAQ_VISUALIZZA_,"Visualizza");
define(_FAQ_OFFLINE_,"Metti Offline");
define(_FAQ_OP_MODIFICA_,"ModificaFaq");
define(_FAQ_OP_VISUALIZZA_,"VisualizzaFaq");
define(_FAQ_OP_INSERIMENTO_,"InserimentoFaq");
define(_FAQ_MSG_ELENCO_,"N.B. L'elenco si riferisce alle sole Faq pubblicate");
define(_FAQ_MSG_ELENCO_2_,"N.B. La domanda verrà esaminata da EXPO prima della pubblicazione");
define(_FAQ_TOT_QUERY_,"Totale FAQ: ");
define(_FAQ_INDIETRO_,"Indietro");
define(_FAQ_ARGOMENTO_,"Argomento");
define(_FAQ_RICERCA_LIBERA_,"Ricerca Libera");
define(_FAQ_NO_RISULTATI_,"Nessuna Faq presente/corrispondente ai filtri selezionati");
define(_FAQ_MSG_OFFLINE_,"Vuoi mettere offline questa Faq?");
define(_FAQ_MSG_ONLINE_,"Vuoi pubblicare questa Faq?");
define(_FAQ_MSG_ELIMINA_,"Vuoi eliminare questa Faq?");
define(_FAQ_MSG_OFFLINE_OK_,"La Faq è offline");
define(_FAQ_MSG_ONLINE_OK_,"La Faq è pubblicata");
define(_FAQ_MSG_MODIFICA_OK_,"La Faq è stata modificata");
define(_FAQ_MSG_ELIMINA_OK_,"La Faq è stata cancellata");
define(_FAQ_MSG_INSERIMENTO_OK_,"La Faq è stata inserita correttamente");
define(_FAQ_ALT_OFFLINE_,"Metti Offline");
define(_FAQ_ALT_ONLINE_,"Pubblica Faq");
define(_FAQ_ALT_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_VISUALIZZA_,"Visualizza Faq");
define(_FAQ_TITLE_MODIFICA_,"Modifica Faq");
define(_FAQ_TITLE_ONLINE_,"Pubblica Faq");
define(_FAQ_TITLE_OFFLINE_,"Metti Offline");
define(_FAQ_TITLE_ELIMINA_,"Elimina Faq");
define(_FAQ_TITLE_INDIETRO_,"Torna alla pagina della Faq");

define(_FILTRI_RICERCA_LABEL_,"Filtri di Ricerca");
define(_FAQ_PULSANTE_AGGIORNA_,"Aggiorna");

define(_RISULTATI_TROVATI_,"risultati");

define(_RISULTATI_PER_PAGINA_,"risultati per pagina");

define(_TUTTE_,"Tutte");
define(_NESSUNA_,"Nessuna");
define(_INVERTI_,"Inverti");


//MODIFICA  18/04/2014 PROFESSIONISTI

define(_TIT_1_LP_, "Dati del Professionista");
define(_TIT_1_STUDIO_, "Dati dello Studio");
define(_N_DIP_ORDINI_, "Numero addetti o collaboratori");

define(_QUALIT_LP_, "Caratteristiche del professionista");
define(_QUALIT_STUDIO_, "Caratteristiche dello studio");
define(_SEDI_LP_, "Grado di internazionalizzazione del professionista");
define(_SEDI_STUDIO_, "Grado di internazionalizzazione dello studio");
define(_RAGSOC_PROF_, "Nome/Denominazione");
define(_APP_RETE_IMP_PROF_, "Appartenenza a forme di aggregazione (ad es. reti, consorzi, associazioni)");

define(_IMPRESA_OFFLINE_PRODOTTI_, "L'impresa &egrave; offline. I prodotti/servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_LP_, "Il Professionista &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_STUDIO_, "Lo studio &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");

define(_PROFESSIONISTA_, "Professionista");
define(_STUDIO_, "Studio");
define(_PUBBLICA_ALL_PROF_, "Pubblica servizi");

define(_SERVIZI_, "Elenco servizi");
define(_SERVIZIO_, "Servizio");

//MODIFICA  15/05/2014
define(_P_SIEXPO_, "Prodotti sostenibili verificati da SIEXPO");
define(_ANTEP_IMP_,'<a draggable="false" href="javascript:inviaSiexpo(document.siexpoForm,\'http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=295\')"><label for="isSiexpo" >Presente nel Catalogo <img src="/images/expo2015/siexpo.jpg"  style="vertical-align: middle;margin-left: 5px;width: 100px"/></label></a>');
define(_NO_RETI_,'Nessuna rete selezionata');
define(_IMPRESE_RETE_,"<strong>Altre Imprese della Rete e iscritte al Catalogo</strong><br/>");
define(_ANT_IMP_CAPOG_,"Impresa capogruppo");
define(_ANT_NOME_RETE_, "Rete: ");
define(_DESCR_RETE_, "Descrizione: ");
define(_ANT_CONTATTO_RETE, "Contatto della Rete: ");


define(_DISCLAIMER_CATEGORIE_DEFAULT_ ,"I prodotti/servizi rientranti in questa merceologia sono forniti dal Partner/Sponsor di Expo 2015, la cui offerta &egrave; dettagliata nella <b>Official Partners Catalogue</b> (http://catalogue.expo2015.org), altre imprese sono comunque consultabili proseguendo la ricerca.");

define(_ACCESSO_NEGATO_,"NON SEI ABILITATO ALLA VISUALIZZAZIONE");
define(_IMPRESA_TEMPORANEAMENTE_OFFLINE_,"IMPRESA TEMPORANAMENTE OFF LINE");

?>
