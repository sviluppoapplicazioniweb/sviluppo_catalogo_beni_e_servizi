<?
$db_titolo = new DB_CedCamCMS;
$db_titolo2 = new DB_CedCamCMS;

$SQLtit = "select Id, Titolo, Help_HTML, MTop, Descrizione, Keywords, Popup, Data_modifica, Data_Pubblicazione, LinkEsterno";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=", Titolo_AL, Descrizione_AL, Keywords_AL, Popup_AL, LinkEsterno_AL";
$SQLtit.=" from " . $tabsvil . "T_Menu";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=", " . $tabsvil . "T_Menu_AL";
$SQLtit.=" where Id_Menu='" . $explode . "' and Tipo<>'L'";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=" and " . $tabsvil . "T_Menu_AL.Id_AL=" . $tabsvil . "T_Menu.Id and " . $tabsvil . "T_Menu_AL.Lang_AL='" . $Lang . "'";
if ($Priorita[$utype] > 8)
    $SQLtit.=" and (Stato_Menu='A' or Stato_Menu='D')";
else
    $SQLtit.=" and Stato_Menu='A'";

if ($db_titolo->query($SQLtit))
    $db_titolo->next_record();

$titolo_abs = "";

if (isset($daabstract) && (!(is_numeric($daabstract))))
    unset($daabstract);

if (isset($daabstract)) {
    $SQLtit2 = "select Titolo_Html";
    if ($Lang != $LinguaPrincipale)
        $SQLtit2.=", Titolo_Html_AL";
    $SQLtit2.=" from " . $tabsvil . "T_Articoli ";
    if ($Lang != $LinguaPrincipale)
        $SQLtit2.=", " . $tabsvil . "T_Articoli_AL";
    $SQLtit2.=" where Id_Articolo='" . $daabstract . "'";

    if ($db_titolo2->query($SQLtit2))
        $db_titolo2->next_record();
    if ($Lang != $LinguaPrincipale)
        $titolo_abs = $db_titolo2->f('Titolo_Html_AL');
    else
        $titolo_abs = $db_titolo2->f('Titolo_Html');
}

$myabil_page = 1;
if (($utype == 2 || $utype == 3) && $mastereditor == 0) {
    $db_admintool = new DB_CedCamCMS;
    $SQLadmintool = "select Id_Menu as myabil2 from " . $tabsvil . "T_Menu where instr('$myabil',concat('|',Id,'|'))";
    $db_admintool->query($SQLadmintool);
    $myabil2 = "|";
    $myabil_page = 0;
    while ($db_admintool->next_record()) {
        $myabil2.=$db_admintool->f('myabil2') . "|";
        if ($db_admintool->f('myabil2') == substr($explode, 0, strlen($db_admintool->f('myabil2'))))
            $myabil_page = 1;
    }
}

$al = $AltreLingue;
array_unshift($al, $LinguaPrincipale);
$scelta_lingua = "";
if ($QUERY_STRING != "")
    $scelta_lingua2 = "?" . $QUERY_STRING . "&amp;";
else
    $scelta_lingua2 = "?";
for ($i = 0; $i < sizeof($al); $i++)
    $scelta_lingua.="<a href=\"" . $PHP_SELF . $scelta_lingua2 . "Lang2=" . $al[$i] . "\">" . $al[$i] . "</a>&nbsp;";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="it">
    <head>
        <title>
            <?
//	echo ucfirst($PROGETTO)." - ".$db_titolo->f('Titolo');
            include "browser.inc";
            if ($db_titolo->f('Data_Pubblicazione') > $db_titolo->f('Data_modifica'))
                $meta_date = substr($db_titolo->f('Data_Pubblicazione'), 0, 10);
            else
                $meta_date = substr($db_titolo->f('Data_modifica'), 0, 10);
            if ($Lang != $LinguaPrincipale && $db_titolo->f('Descrizione_AL') != "")
                $description = $db_titolo->f('Descrizione_AL');
            else
                $description = $db_titolo->f('Descrizione');
            if ($Lang != $LinguaPrincipale && $db_titolo->f('Keywords_AL') != "")
                $keywords = $db_titolo->f('Keywords_AL');
            else
                $keywords = $db_titolo->f('Keywords');
            if ($Lang != $LinguaPrincipale && $db_titolo->f('Popup_AL') != "")
                $Popup = $db_titolo->f('Popup_AL');
            else
                $Popup = $db_titolo->f('Popup');
            if ($Lang != $LinguaPrincipale && $db_titolo->f('LinkEsterno_AL') != "")
                $LinkEsterno2 = $db_titolo->f('LinkEsterno_AL');
            else
                $LinkEsterno2 = $db_titolo->f('LinkEsterno');

            $mtop = $db_titolo->f('MTop');

            if ($titolo_abs != "")
                $titolo_pagina = $titolo_abs;
            else {
                if ($Lang != $LinguaPrincipale) {
                    if ($db_titolo->f('Popup_AL') != "")
                        $titolo_pagina = $db_titolo->f('Popup_AL');
                    else
                        $titolo_pagina = $db_titolo->f('Titolo_AL');
                }
                else {
                    if ($db_titolo->f('Popup') != "")
                        $titolo_pagina = $db_titolo->f('Popup');
                    else
                        $titolo_pagina = $db_titolo->f('Titolo');
                }
            }

            echo $titolo_pagina . " - " . $TITLESITE;

//charset=iso-8859-1
            ?>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="title" content="<? echo $db_titolo->f('Titolo'); ?>" />
        <meta name="Description" content="<? echo $description; ?>" />
        <meta name="Keywords" content="<? echo $keywords; ?>" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="date" content="<?php echo "$meta_date"; ?>" />
        <link href="/CSS/style.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css' />

        <link rel="stylesheet" href="/jquery/themes/base/jquery-ui.css" />
        <link rel="stylesheet" href="/jquery/themes/base/jquery.ui.menu.css" />

        <script type="text/javascript" src="/jquery/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/jquery/ui/jquery-ui.js"></script>

        <!--       
               <script src="/jquery-ui-1.10.2/jquery-1.9.1.js"></script>
               <script src="/jquery-ui-1.10.2/ui/jquery-ui.js"></script>
               <link rel="stylesheet" href="/jquery-ui-1.10.2/themes/base/jquery-ui.css" />
               <link rel="stylesheet" href="/jquery-ui-1.10.2/themes/base/jquery.ui.menu.css" />
        -->   

 <!--<script src="/jquery-ui-1.10.2/ui/minified/jquery-ui.min.js" type="text/javascript"></script>-->

 <!--       <script src="/js/responsiveslides.min.js"></script>
        <script>
            $(function() {

                // Slideshow 1
                $("#slider1").responsiveSlides({
                    maxwidth: 600,
                    speed: 800
                });

            });
        </script>-->
        <script>
            $(function() {
                $("#menu").menu();
            });
        </script>
        <script type="text/javascript">
            function calcChk()
            {
                var elements = document.getElementsByName('CritS');
                var Id_Crit = '';
                var numChk = 0;
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].checked) {
                        numChk++;
                        /*alert(elements[i].value);
                         alert('Chk'+ i +' - '+element[i].value);*/
                        if (numChk == 1) {
                            Id_Crit = elements[i].value;
                        } else {
                            Id_Crit = Id_Crit + '|' + elements[i].value;
                        }
                    }
                }
                var campoIdCrit = document.getElementsByName('Id_Crit');
                /*alert(campoIdCrit[0].value);
                 alert(campoIdCrit.length);*/
                campoIdCrit[0].value = Id_Crit;
                /*alert(campoIdCrit[0].value);*/
                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = 1;

                document.forms["filtri"].submit();
            }
        </script>
        <script type="text/javascript">
            function calcAlbCat(var1)
            {
                var campoIdCat = document.getElementsByName('Id_Cat');
                campoIdCat[0].value = var1;
                //alert("Welcome " + campoIdCat[0].value);
                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = 1;

                document.forms["filtri"].submit();
            }

            function calcPagin(Id_Cat, pag, ordina) {
                var campoTemp = document.getElementsByName('Id_Cat');
                campoTemp[0].value = Id_Cat;

                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = pag;
                //alert(campoTemp[0].value);

                var campoTemp = document.getElementsByName('ordina');
                campoTemp[0].value = ordina;

                document.forms["filtri"].submit();

            }

            function calcImpresa(var1) {
                var campoTemp = document.getElementsByName('ImpSel');
                campoTemp[0].value = var1.value;
                //alert("Id_Utente " + campoTemp[0].value);
                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = 1;

                document.forms["filtri"].submit();
            }

            function calcMat(var1) {
                var campoTemp = document.getElementsByName('MatSel');
                campoTemp[0].value = var1.value;
                //alert("Id_Utente " + campoTemp[0].value);
                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = 1;

                document.forms["filtri"].submit();
            }

            function delCat() {
                var campoTemp = document.getElementsByName('Id_Cat');
                campoTemp[0].value = "";

                var campoTemp = document.getElementsByName('pcor');
                campoTemp[0].value = 1;

                document.forms["filtri"].submit();
            }
        </script>    

        <?
        if ($Id_VMenu == 102 || $Id_VMenu == 1) {
            $StileCorpo = "corpo_home.css";
        } else {
            $StileCorpo = "corpo.css";
        }
        if ($mtop == "00")
            echo "<link href=\"CSS/menu1admin.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
        ?>
        <link href="CSS/<? echo $StileCorpo; ?>" rel="stylesheet" type="text/css" />
        <?
        if ($BROWSER [has_dhtml])
            echo "<link rel=\"stylesheet\" href=\"/CSS/style-new.css\" type=\"text/css\">";
        ?>
        <link rel="stylesheet" type="text/css" href="/CSS/print.css" media="print" />
        <?
        if ($popup_azioni != "N") {
            ?>
            <script type="text/javascript">

                //imposta altezza template
                function setTall() {
                    if (document.getElementById) {
    <?
    if ($template[$tmpl]["right_item"] == "" && $template[$tmpl]["left_item"] == "") {
        echo "var divs = new Array(document.getElementById('centro'));";
    } elseif ($template[$tmpl]["right_item"] != "" && $template[$tmpl]["left_item"] == "") {
        echo "var divs = new Array(document.getElementById('centro3'), document.getElementById('barra_dx'));";
    } elseif ($template[$tmpl]["right_item"] == "" && $template[$tmpl]["left_item"] != "") {
        echo "var divs = new Array(document.getElementById('centro2'), document.getElementById('barra_sx'));";
    } else {
        echo "var divs = new Array(document.getElementById('centro'), document.getElementById('barra_dx'), document.getElementById('barra_sx'));";
    }
    ?>

                        var maxHeight = 300;
                        for (var i = 0; i < divs.length; i++) {
                            //alert (divs[i].offsetHeight);
                            if (divs[i].offsetHeight > maxHeight) {
                                maxHeight = divs[i].offsetHeight;

                                //calcolo altezza centro
                                if (i == 0)
                                {
                                    var maxTotal = 0;
                                    for (var r = 1; 1; r++)
                                    {
                                        if (document.getElementById('riga' + r))
                                        {
                                            var maxRow = 0;
                                            for (var a = 1; 1; a++)
                                            {
                                                if (document.getElementById('articolo' + r + '.' + a))
                                                {
                                                    if (document.getElementById('articolo' + r + '.' + a).offsetHeight > maxRow)
                                                        maxRow = document.getElementById('articolo' + r + '.' + a).offsetHeight;
                                                }
                                                else
                                                    break;
                                            }
                                            maxTotal += maxRow;
                                            //alert (maxTotal);
                                        }
                                        else
                                            break;
                                    }
                                    if (maxTotal > maxHeight)
                                        maxHeight = maxTotal + 30;

                                }
                            }
                        }
                        maxHeight = maxHeight + 2;
                        //alert (maxHeight);

                        for (var i = 0; i < divs.length; i++) {
                            divs[i].style.height = maxHeight + 'px';

                            if (divs[i].offsetHeight > maxHeight) {
                                divs[i].style.height = (maxHeight - (divs[i].offsetHeight - maxHeight)) + 'px';
                            }
                        }
                    }
                }


    <?
// FREDDO MODIFICA
    include "jsfunction.inc";
    ?>
            </script>

            <script type="text/javascript">
                // Popup window code
                function newPopup(url, width, height) {
                    popupWindow = window.open(
                            url, 'popUpWindow', 'height=' + height + ',width=' + width + ',left=20,top=20,resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no')
                }
            </script>

            <?
        }
        /* if ($utype=="" || $utype>3)
          {
          define(__PHP_STATS_PATH__,$PATHDOCS.'stats/');
          include(__PHP_STATS_PATH__.'php-stats.redir.php');
          }
         */
        ?>
    </head>

    <body >

        <div id="container">
            <?
//if ($messaggio!="")
//	echo " <script type=\"text/javascript\">alert(\"".$messaggio."\");</script>";
            /*
              if ($Popup!="")
              {
              $reg_explode1="popup_".$explode;
              if (!isset($$reg_explode1))
              {scr
              $$reg_explode1="1";
              $sess->register("popup_".$explode);
              echo " <script type=\"text/javascript\">";
              echo "window.open('popup2.phtml?Id_VMenu=".$db_titolo->f(Id)."','nome','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=420,height=360')";
              echo " </script>";
              }
              }
             */
            include $PROGETTO . "/toolbar.inc";

            if (!isset($auth->auth["uid"]) || $auth->auth["uid"] == "") {
                if ($Lang == $LinguaPrincipale) {
                    if ($auth->auth["utype"] != 98) {
                        $login_head = '&nbsp;&nbsp;<a title="Accedi" href="/index.phtml?Id_VMenu=308"><u>Accedi</u></a>&nbsp;';
                    } else {
                        $login_head = '&nbsp;&nbsp;<a title="Accedi" href="/index.phtml?Id_VMenu=51"><u>Accedi</u></a>&nbsp;';
                    }


                    $login_head .= '&nbsp; o &nbsp;<a title="Registrazione" href="/index.phtml?Id_VMenu=264"><u>Registrati</u></a><br />';
                } else {

                    $login_head = '&nbsp;&nbsp;<a title="Login" href="/index.phtml?Id_VMenu=51"><u>Login</u></a>&nbsp;';

                    $login_head .= '&nbsp; or &nbsp;<a title="Register" href="/index.phtml?Id_VMenu=264"><u>Register</u></a><br />';
                }
            } else {
                //Ricava nome dell'utente per metterlo nel saluto
                $dbuser = new DB_CedCamCMS;
                $sql_user = "SELECT * FROM T_Anagrafica WHERE Id = " . $auth->auth["uid"];
                $dbuser->query($sql_user);
                if ($dbuser->num_rows() > 0) {
                    $dbuser->next_record();
                    $usrName = $dbuser->f('Nome');
                    $usrName.= " " . $dbuser->f('Cognome');
                }
                if ($Lang == $LinguaPrincipale) {
                    //$login_head = '<img src="/images/login.jpg" alt="Logout" title="Logout" />&nbsp;<a title="Logout" href="/jslogin.phtml?action=logout&okurl='.$HOMEURL.'" style="color:#B0B1B3; font-size:1.0em;padding-top:5px;">Logout</a><br />\n';
                    $login_head = '&nbsp;&nbsp;<img src="/images/login.jpg" alt="Profilo" title="Profilo" />&nbsp;<a title="Profilo" href="/index.phtml?Id_VMenu=51" style="color:#B0B1B3;">Profilo</a>&nbsp;&nbsp;<a title="disconnetti" href="/jslogin.phtml?action=logout&okurl=' . $MAINSITE . '">Disconnetti</a><br />';
                } else {
                    //$login_head = '<img src="/images/login.jpg" alt="Logout" title="Logout" />&nbsp;<a title="Logout" href="/jslogin.phtml?action=logout&okurl='.$HOMEURL.'" style="color:#B0B1B3; font-size:1.0em;padding-top:5px;">Logout</a><br />\n';
                    $login_head = '&nbsp;&nbsp;<img src="/images/login.jpg" alt="Profile" title="Profile" />&nbsp;<a title="Profile" href="/index.phtml?Id_VMenu=51" style="color:#B0B1B3;">Profile</a>&nbsp;&nbsp;<a title="disconnetti" href="/jslogin.phtml?action=logout&okurl=' . $MAINSITE . '">Disconnect</a><br />';
                }
            }

            if ($popup_azioni == "S")
                $codice_html = file_get_contents($PATHDOCS . "tmpl/header_popup.html");
            elseif ($popup_azioni != "N") {


                if ($Lang == $LinguaPrincipale)
                //$codice_html = file_get_contents($PATHDOCS . "tmpl/header.html");
                    $codice_html = file_get_contents($PATHDOCS . "tmpl/header.html");
                else
                    $codice_html = file_get_contents($PATHDOCS . "tmpl/header_" . $Lang . ".html");
            }
            if ($popup_azioni != "N")
                echo preg_replace("/\{%(\w+)%\}/e", "\$\\1", $codice_html);
            ?>
