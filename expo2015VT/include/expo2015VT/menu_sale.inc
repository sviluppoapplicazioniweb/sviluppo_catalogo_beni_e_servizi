<?php

$db_menu_sale = new DB_CedCamCMS;
if ($uid != "") {
    $menustring = "<li class=\"label\">Gestione Dati</li>";
    switch ($mygroup) {
        case "|4|":
            $SQLmenu_sale = "SELECT Id, Nome FROM SALE_T_Strutture WHERE Id_Utente = " . $uid;
            $db_menu_sale->query($SQLmenu_sale);
            $db_menu_sale->next_record();
            if ($db_menu_sale->num_rows() > 0 && $db_menu_sale->f('Nome')!="") {
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?tmpl=4&pagina=form&nome=SALE_T_Strutture&azione=UPD&Id=" . $db_menu_sale->f('Id') . "\">INFO STRUTTURA</a></li>";
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?Id_VMenu=122\">FOTO STRUTTURA</a></li>";
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?Id_VMenu=109\">GESTIONE SALE</a></li>";
            } else {
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?tmpl=4&pagina=form&nome=SALE_T_Strutture&azione=UPD&Id=" . $db_menu_sale->f('Id') . "\">COMPLETA INFO STRUTTURA</a></li>";
            }
            break;

        case "|5|":
        case "|9|":
            $SQLmenu_sale = "SELECT Id, Telefono FROM SALE_T_Agenzie WHERE Id_Utente = " . $uid;
            $db_menu_sale->query($SQLmenu_sale);
            $db_menu_sale->next_record();
            if ($db_menu_sale->num_rows() > 0 && $db_menu_sale->f('Telefono')!="") {
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?tmpl=4&pagina=form&nome=SALE_T_Agenzie&azione=UPD&Id=" . $db_menu_sale->f('Id') . "\">INFO AGENZIA</a></li>";
            } else {
                $menustring .= "<li class=\"menu1\"><a href=\"index.phtml?tmpl=4&pagina=form&nome=SALE_T_Agenzie&azione=UPD&Id=" . $db_menu_sale->f('Id') . "\">COMPLETA INFO AGENZIA</a></li>";
            }
            break;
    }

    $codice_html = file_get_contents($PATHDOCS . "tmpl/menu_sale.html");
    echo preg_replace("/\{%(\w+)%\}/e", "\$\\1", $codice_html);
}
?>
