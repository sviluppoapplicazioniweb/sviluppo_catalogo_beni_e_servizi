<?

class ReportClass {

    public $TotaleImp = "";
    public $TotaleErog = "";
    public $TotaleRic = "";

    public function CalcolaTot($TipoCal) {
        $dbTot = new DB_CedCamCMS;
        if ($TipoCal == "impegnato") {
            $sqlTot = "SELECT SUM(Importo) AS totale FROM FWA_T_Pratiche WHERE Id_Stato>=90 AND Id_Stato!=120";
        } elseif ($TipoCal == "erogato") {
            $sqlTot = "SELECT SUM(importo_accordato) AS totale FROM FWA_T_Pratiche";
        } elseif ($TipoCal == "richiesto") {
            $sqlTot = "SELECT SUM(Importo) AS totale FROM FWA_T_Pratiche WHERE Id_Stato>=30 AND Id_Stato!=40 AND Id_Stato!=80 AND Id_Stato!=120";
        }
        $TotalRow = $dbTot->query($sqlTot);
        $row = mysql_fetch_array($TotalRow);
        if ($TipoCal == "impegnato") {
            $this->TotaleImp = $row['totale'];
        } elseif ($TipoCal == "erogato") {
            $this->TotaleErog = $row['totale'];
        } elseif ($TipoCal == "richiesto") {
            $this->TotaleRic = $row['totale'];
        }
    }

    public function StampaReport() {
        $this->CalcolaTot("impegnato");
        $this->CalcolaTot("erogato");
        $this->CalcolaTot("richiesto");
        $tot1 = $this->TotaleImp;
        $tot2 = $this->TotaleErog;
        $tot3 = $this->TotaleRic;
        $codice_html = file_get_contents($PATHDOCS . "tmpl/report.html");
        echo preg_replace("/\{%(\w+)%\}/e", "\$\\1", $codice_html);
    }
}

$report1 = new ReportClass();
$report1->StampaReport();
?>