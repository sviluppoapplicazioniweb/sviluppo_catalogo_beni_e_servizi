<?php

/**
 * Per la formattazione di testo per le url semplificate.
 *
 * @author marco.zanaboni
 * MZ280211
 */
class URLClass {

    // metodi
    public function furl($str_url) {
        $stringaURL = $str_url;
        $stringaURL = str_replace(","," ",$stringaURL);
        $stringaURL = str_replace(" ","_",$stringaURL);
        $stringaURL = str_replace("'","_",$stringaURL);
        $stringaURL = str_replace("&","_",$stringaURL);
        $stringaURL = str_replace("!","_",$stringaURL);
        $stringaURL = str_replace("£","_",$stringaURL);
        $stringaURL = str_replace("?","_",$stringaURL);
        $stringaURL = str_replace("%","_",$stringaURL);
        $stringaURL = str_replace("-","_",$stringaURL);
        $stringaURL = str_replace("__","_",$stringaURL);
        $stringaURL = str_replace("è","e",$stringaURL);
        $stringaURL = str_replace("é","e",$stringaURL);
        $stringaURL = str_replace("à","a",$stringaURL);
        $stringaURL = str_replace("ò","o",$stringaURL);
        $stringaURL = str_replace("ù","u",$stringaURL);
        $stringaURL = str_replace("ì","i",$stringaURL);
        $stringaURL = str_replace("À","A",$stringaURL);
        $stringaURL = str_replace("È","E",$stringaURL);
        $stringaURL = str_replace("Ì","I",$stringaURL);
        $stringaURL = str_replace("Ò","O",$stringaURL);
        $stringaURL = str_replace("Ù","U",$stringaURL);
        return $stringaURL;
    }

}

?>
