ALTER TABLE EXPO_Tlk_OrdiniProfessionali ADD Autocertificazione varchar(1) NULL DEFAULT  'N';

CREATE TABLE EXPO_Tlk_Questionari (
  Id int(11) NOT NULL PRIMARY KEY UNIQUE,
  NQuestionario int(11) DEFAULT NULL,
  Tipo varchar(1) DEFAULT NULL,
  OrdineRisposta int(11) DEFAULT NULL,
  Testo varchar(255) DEFAULT NULL,
  Testo_EN varchar(255) DEFAULT NULL,
  Testo_FR varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE EXPO_TJ_Imprese_Questionari (
  Id int(11) NOT NULL PRIMARY KEY UNIQUE,
  IdImpresa int(11) DEFAULT NULL,
  IdQuestionario int(11) DEFAULT NULL,
  IdUtente int(11) DEFAULT NULL,
  Note text
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE EXPO_Log_Visite_Vetrina ADD IdEsitoRicerca int(11) NULL;


CREATE TABLE EXPO_TJ_Ricerche_Categorie
(
Id int(11) PRIMARY KEY UNIQUE,
IdRicerca int(11),
IdCategoria int(11)
)ENGINE=InnoDB;

CREATE TABLE EXPO_TJ_Ricerche_Lingue
(
Id int(11) PRIMARY KEY UNIQUE,
IdRicerca int(11),
IdLingua int(11)
)ENGINE=InnoDB;

CREATE TABLE EXPO_TJ_Ricerche_Certificazioni
(
Id int(11) PRIMARY KEY UNIQUE,
IdRicerca int(11),
IdCertificazione int(11)
)ENGINE=InnoDB;

CREATE TABLE EXPO_T_Ricerche
(
IdRicerca 						int(11) PRIMARY KEY UNIQUE,
IdPartecipante 					int(11),
SiglaNazione 					varchar (3),
IdRicercaPadre 					int(11),
DataRicerca 					datetime,
Keywords 						varchar(255) NULL,

RagioneSociale 					varchar(255) NULL,
NumeroDipendenti 				varchar(10) NULL,
UltimoFatturato 				varchar(10) NULL,
IsSediEstere  					varchar(1) NULL DEFAULT  'N',
IsNetwork  						varchar(1) NULL DEFAULT  'N',
IsGeneralContractor 			varchar(1) NULL DEFAULT  'N',
IsProduttore 					varchar(1) NULL DEFAULT  'N',
IsDistributore 					varchar(1) NULL DEFAULT  'N',
IsPrestatoreDiServizi 			varchar(1) NULL DEFAULT  'N',
IsPartecipazioneExpo 			varchar(1) NULL DEFAULT  'N',
IsPartecipazioniInternazionali 	varchar(1) NULL DEFAULT  'N',
IsSindacale						varchar(1) NULL DEFAULT  'N',
IsModello 						varchar(1) NULL DEFAULT  'N',
IsRetingLegalita 				varchar(1) NULL DEFAULT  'N',
IsMasterSpecialistico 			varchar(1) NULL DEFAULT  'N',
IsCertificazione 				varchar(1) NULL DEFAULT  'N',
IsSiexpo2015 					varchar(1) NULL DEFAULT  'N',
IsRetiImpresa 					varchar(1) NULL DEFAULT  'N',
NomeRete	 					varchar(255) NULL
)ENGINE=InnoDB;



ALTER TABLE EXPO_Tlk_Categorie ADD FlagNote Varchar(1) DEFAULT 'N';
ALTER TABLE EXPO_Tlk_Categorie ADD Note text NULL;
ALTER TABLE EXPO_Tlk_Categorie ADD Note_AL text NULL;
ALTER TABLE EXPO_Tlk_Categorie ADD Note_AL2 text NULL;

ALTER TABLE EXPO_Tlk_Categorie ENGINE=InnoDB;


CREATE INDEX Id_Categoria
ON EXPO_Tlk_Categorie (Id_Categoria);
