-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Dic 02, 2013 alle 12:33
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_FasciaDownload`
--

CREATE TABLE IF NOT EXISTS `EXPO_Tlk_FasciaDownload` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(100) NOT NULL,
  `Dimensione` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_Tlk_FasciaDownload`
--

INSERT INTO `EXPO_Tlk_FasciaDownload` (`Id`, `Nome`, `Dimensione`) VALUES
(1, 'Fascia 1', '2097152'),
(2, 'Fascia 2', '5242880'),
(3, 'Fascia 3', '10485760');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
