-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 22, 2013 alle 09:24
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_Pagamenti`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Pagamenti` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Result` varchar(20) NOT NULL,
  `MerchantOrderId` varchar(50) NOT NULL,
  `PaymentId` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Authorizationcode` varchar(6) NOT NULL,
  `DataRichiesta` datetime NOT NULL,
  `Amount` text NOT NULL,
  `IdTransazione` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Imprese_Pagamenti`
--

INSERT INTO `EXPO_TJ_Imprese_Pagamenti` (`Id`, `IdImpresa`, `Result`, `MerchantOrderId`, `PaymentId`, `Description`, `Authorizationcode`, `DataRichiesta`, `Amount`, `IdTransazione`) VALUES
(3, 1, 'GW00159', '', 0, 'Card Number Missing.', '', '2013-11-05 15:17:00', '', ''),
(4, 1, 'GW00166', '', 0, 'Invalid Card Number data.', '', '2013-11-05 15:18:00', '', ''),
(5, 1, 'GW00858', '', 0, 'Missing required data - CVV', '', '2013-11-05 15:18:00', '', ''),
(7, 9, 'OK', '', 0, '', '', '2013-11-06 12:31:00', '', ''),
(8, 10, 'OK', '', 0, '', '', '2013-11-06 12:36:00', '', ''),
(9, 10, 'OK', '', 0, '', '', '2013-11-06 12:39:00', '', ''),
(10, 10, 'OK', 'Order-527a28dd28e2a', 0, '', '', '2013-11-06 12:39:00', '', ''),
(11, 10, 'OK', 'Order-527a28dd28e2a', 0, '', '', '2013-11-06 12:43:00', '', ''),
(12, 2, 'OK', 'Order-527a67508fa80', 0, '', '', '2013-11-06 17:00:00', '', ''),
(13, 2, 'OK', 'Order-527a703a5184a', 0, '', '', '2013-11-06 17:53:00', '', ''),
(14, 2, 'OK', 'Order-527a74f001d4a', 0, '', '', '2013-11-06 17:58:00', '', ''),
(15, 11, 'OK', 'Order-527a75c08d0cd', 0, '', '', '2013-11-06 18:02:00', '', ''),
(16, 11, 'OK', 'Order-527a769c34a18', 0, '', '', '2013-11-06 18:04:00', '', ''),
(17, 13, 'OK', 'Order-527b617dd9a57', 0, '', '', '2013-11-07 10:48:00', '', ''),
(34, 7, 'OK', '528cc921663a7', 0, '', '', '2013-11-20 15:38:00', '100', '754293747923533249'),
(35, 7, 'APPROVED', '528ccd49ac576', 0, '', '', '2013-11-20 15:57:00', '100', '275245517141233249'),
(36, 7, 'APPROVED', '528de162a1e7c', 0, '', '', '2013-11-21 11:33:00', '100', '719780748498833259');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
