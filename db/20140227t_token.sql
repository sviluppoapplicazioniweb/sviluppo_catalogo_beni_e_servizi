﻿# Host: vlximidb13.mi.cciaa.net  (Version: 5.1.48-log)
# Date: 2014-02-27 12:21:20
# Generator: MySQL-Front 5.3  (Build 4.68)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "t_token"
#

CREATE TABLE "t_token" (
  "Id_Token" bigint(22) NOT NULL AUTO_INCREMENT,
  "Token" varchar(300) NOT NULL,
  "Id_Utente" bigint(22) NOT NULL,
  "IP1" varchar(20) NOT NULL,
  "IP2" varchar(20) DEFAULT NULL,
  "IP3" varchar(20) DEFAULT NULL,
  "Data_Scadenza" datetime NOT NULL,
  "Data_Utilizzo" datetime DEFAULT NULL,
  PRIMARY KEY ("Id_Token"),
  UNIQUE KEY "IX_Auth_Token_Unique" ("Token")
);

#
# Data for table "t_token"
#

INSERT INTO "t_token" VALUES (1,'110b2f8e25dda26da585aa1035abfe8456424d1f72ccf51267cdd37a2ac93dff',167,'172.24.5.106','','','2014-02-26 15:44:10','2014-02-25 15:47:20');
