-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 22, 2013 alle 09:24
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_OrdiniNoFine`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_OrdiniNoFine` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Result` varchar(20) NOT NULL,
  `MerchantOrderId` varchar(50) NOT NULL,
  `PaymentId` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Authorizationcode` varchar(6) NOT NULL,
  `DataRichiesta` datetime NOT NULL,
  `Amount` text NOT NULL,
  `IdTransazione` varchar(200) NOT NULL,
  `Stato` int(11) NOT NULL COMMENT '2 = completato; 1 in attesa; ',
  `SecurityToken` varchar(50) NOT NULL,
  `Hostedpageurl` varchar(300) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Imprese_OrdiniNoFine`
--

INSERT INTO `EXPO_TJ_Imprese_OrdiniNoFine` (`Id`, `IdImpresa`, `Result`, `MerchantOrderId`, `PaymentId`, `Description`, `Authorizationcode`, `DataRichiesta`, `Amount`, `IdTransazione`, `Stato`, `SecurityToken`, `Hostedpageurl`) VALUES
(2, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-20 15:54:00', '', '918692824083133249', 0, 'a3d77ad6b27e4b5498fabc3856f29b7f', ''),
(3, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:20:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(4, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:20:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(5, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:21:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(6, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:21:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(7, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:21:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(8, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:23:00', '', '365558292770333259', 0, '305185ac3a614d798cc7c8e3e2a052a8', ''),
(9, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:24:00', '', '891799791261033259', 0, '159bba2c289d41e087b41d91309205e3', ''),
(10, 7, 'NOT AUTHENTICATED', '', 0, '', '', '2013-11-21 11:32:00', '', '405707730170433259', 0, '44b907a06dea4725ac11910a378d1445', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
