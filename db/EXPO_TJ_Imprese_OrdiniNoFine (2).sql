-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 29, 2013 alle 09:31
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_OrdiniNoFine`
--

DROP TABLE IF EXISTS `EXPO_TJ_Imprese_OrdiniNoFine`;
CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_OrdiniNoFine` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Result` varchar(20) NOT NULL DEFAULT ' ',
  `MerchantOrderId` varchar(50) NOT NULL DEFAULT ' ',
  `PaymentId` varchar(50) NOT NULL,
  `Description` varchar(255) NOT NULL DEFAULT ' ',
  `Authorizationcode` varchar(6) NOT NULL DEFAULT ' ',
  `DataRichiesta` datetime NOT NULL,
  `Amount` varchar(50) NOT NULL DEFAULT ' ',
  `IdTransazione` varchar(200) NOT NULL DEFAULT ' ',
  `Stato` int(11) NOT NULL DEFAULT '0' COMMENT '2 = completato; 1 in attesa; ',
  `SecurityToken` varchar(50) NOT NULL DEFAULT ' ',
  `Hostedpageurl` varchar(300) NOT NULL DEFAULT ' ',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
