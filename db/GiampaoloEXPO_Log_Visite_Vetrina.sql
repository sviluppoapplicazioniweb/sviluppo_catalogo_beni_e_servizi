-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Feb 17, 2014 alle 15:05
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Log_Visite_Vetrina`
--

CREATE TABLE IF NOT EXISTS `EXPO_Log_Visite_Vetrina` (
  `Id` int(11) NOT NULL,
  `Id_Impresa` int(11) NOT NULL,
  `Id_Prodotto` int(11) NOT NULL,
  `Id_Partecipante` int(11) NOT NULL,
  `DataVisita` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_Log_Visite_Vetrina`
--

INSERT INTO `EXPO_Log_Visite_Vetrina` (`Id`, `Id_Impresa`, `Id_Prodotto`, `Id_Partecipante`, `DataVisita`) VALUES
(1, 5, 0, 77, '2014-02-07'),
(2, 4, 0, 77, '2014-02-07'),
(3, 2, 0, 77, '2014-02-12'),
(4, 2, 0, 77, '2014-02-12'),
(5, 3, 0, 77, '2014-02-12'),
(6, 2, 0, 77, '2014-02-12'),
(7, 3, 0, 77, '2014-02-13'),
(8, 1, 0, 77, '2014-02-13'),
(9, 1, 3, 77, '2014-02-13'),
(10, 1, 3, 77, '2014-02-13'),
(11, 6, 0, 0, '2014-02-14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
