﻿# Host: vlximidb08.mi.cciaa.net  (Version: 5.5.30-enterprise-commercial-advanced-log)
# Date: 2014-02-13 10:05:01
# Generator: MySQL-Front 5.3  (Build 4.68)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "expo_tj_fasciaupload_campi"
#

CREATE TABLE `expo_tj_fasciaupload_campi` (
  `Id` int(11) NOT NULL,
  `IdFascia` int(11) NOT NULL,
  `Campo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "expo_tj_fasciaupload_campi"
#

INSERT INTO `expo_tj_fasciaupload_campi` VALUES (1,1,'retiImpresaF'),(2,1,'certificazioneF'),(3,1,'certificazioniF'),(4,3,'descrizioneF'),(4,2,'bilanciF'),(5,2,'internazionalizzazioneF'),(6,2,'referenzeF'),(7,1,'brevettiF'),(8,2,'aspettiF'),(9,3,'prodottoF'),(10,1,'default'),(11,1,'pMaster'),(12,1,'pCertificazione');
