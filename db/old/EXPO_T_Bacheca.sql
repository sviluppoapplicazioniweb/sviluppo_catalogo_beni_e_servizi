﻿# Host: localhost  (Version: 5.5.16)
# Date: 2013-10-03 18:00:46
# Generator: MySQL-Front 5.3  (Build 4.4)

/*!40101 SET NAMES utf8 */;

#
# Source for table "EXPO_T_Bacheca"
#

DROP TABLE IF EXISTS `EXPO_T_Bacheca`;
CREATE TABLE `EXPO_T_Bacheca` (
  `Id` int(11) NOT NULL,
  `IdPartecipante` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Oggetto` varchar(250) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
