-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ott 25, 2013 alle 16:04
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Accreditamenti`
--

CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Accreditamenti` (
  `Id` int(11) NOT NULL,
  `Sigla` varchar(5) NOT NULL,
  `Descrizione` varchar(500) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_Tlk_Accreditamenti`
--

INSERT INTO `EXPO_Tlk_Accreditamenti` (`Id`, `Sigla`, `Descrizione`) VALUES
(1, 'SGQ ', 'Certificazioni di sistemi di gestione per la qualità'),
(2, 'SGA', 'Certificazioni di sistemi di gestione ambientale'),
(3, 'SGE', 'Certificazioni di sistemi di gestione dell''energia'),
(4, 'PRD', 'Certificazioni di prodotti / servizi'),
(5, 'PRS', 'Certificazioni di personale'),
(6, 'ISP', 'Ispezione'),
(7, 'SCR', 'Certificazioni di sistemi di gestione per la salute e sicurezza sul lavoro'),
(8, 'SSI', 'Certificazioni di sistemi di gestione per la sicurezza delle informazioni'),
(9, 'ITX', 'Certificazioni di sistemi di gestione Servizi Informatici'),
(10, 'DAP', 'Verifica e Convalida delle dichiarazioni ambientali di prodotto'),
(11, 'FSM', 'Certificazioni di sistemi di gestione per la sicurezza alimentare'),
(12, 'GHG', 'Verifica dei gas ad effetto serra'),
(14, 'EMAS', 'Certificazione per l''attività di Verifica Ambientale EMAS');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
