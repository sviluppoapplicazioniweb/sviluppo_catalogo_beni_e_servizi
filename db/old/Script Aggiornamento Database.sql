﻿DROP TABLE IF EXISTS `EXPO_TJ_Bacheca_Prodotti`;
DROP TABLE IF EXISTS `EXPO_T_Bacheca_Trattative`;
DROP TABLE IF EXISTS `EXPO_T_Prodotti_Preferiti`;
DROP TABLE IF EXISTS `EXPO_T_Imprese_Preferiti`;

CREATE TABLE `EXPO_TJ_Bacheca_Prodotti` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdBacheca` int(11) NOT NULL,
  `IdProdotto` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `EXPO_T_Bacheca_Trattative` (
  `Id` int(11) NOT NULL,
  `IdBacheca` int(11) NOT NULL,
  `IdChiusura` int(11) NOT NULL,
  `Valore` varchar(10) NOT NULL,
  `Data` datetime NOT NULL,
  `Stato` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `EXPO_T_Imprese_Preferiti` (
  `Id` int(11) NOT NULL,
  `IdPartecipante` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `EXPO_T_Bacheca_Trattative` VALUES (1,2,77,'100','2013-09-19 12:33:04','A'),(2,1,62,'50000','2013-09-19 12:59:37','A'),(3,4,77,'50000','2013-09-26 15:15:17','A');
INSERT INTO `EXPO_T_Imprese_Preferiti` VALUES (1,77,6),(2,77,5),(3,77,1);
