--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Prodotti`
--

DROP TABLE IF EXISTS `EXPO_T_Prodotti`;
CREATE TABLE IF NOT EXISTS `EXPO_T_Prodotti` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Nome` varchar(100) NOT NULL,
  `Descrizione` varchar(250) NOT NULL,
  `ProdottoLink` varchar(500) NOT NULL,
  `Certificazione` varchar(250) NOT NULL,
  `CertificazioneLink` varchar(500) NOT NULL,
  `Aspetti` varchar(250) NOT NULL,
  `AspettiLink` varchar(500) NOT NULL,
  `Brevetti` varchar(250) NOT NULL,
  `BrevettiLink` varchar(500) NOT NULL,
  `StatoProdotto` varchar(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



UPDATE `EXPO_T_Prodotti` SET StatoProdotto = 'A' WHERE StatoProdotto = "";

ALTER TABLE  `EXPO_T_Imprese` CHANGE  `CapitaleSociale`  `CapitaleSociale` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  '0'