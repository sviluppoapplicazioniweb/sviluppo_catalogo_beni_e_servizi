ALTER TABLE  `EXPO_T_Bacheca_Messaggi` ADD  `IdMittente` INT NOT NULL AFTER  `IdBacheca`
ALTER TABLE  `EXPO_T_Bacheca_Messaggi` ADD  `TipoMittente` INT NOT NULL COMMENT  'tipo=1 impresa; tipo=2 utente' AFTER  `IdMittente`

ALTER TABLE  `EXPO_T_Bacheca_Messaggi` ADD  `IdDestinatario` INT NOT NULL AFTER  `TipoMittente`
ALTER TABLE  `EXPO_T_Bacheca_Messaggi` ADD  `TipoDestinatario` INT NOT NULL COMMENT  'tipo=1 impresa; tipo=2 utente' AFTER  `IdDestinatario`