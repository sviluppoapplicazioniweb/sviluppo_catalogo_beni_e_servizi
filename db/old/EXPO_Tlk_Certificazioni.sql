-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ott 24, 2013 alle 15:50
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Certificazioni`
--

DROP TABLE IF EXISTS `EXPO_Tlk_Certificazioni`;
CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Certificazioni` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCert` varchar(25) DEFAULT NULL,
  `Descrizione` varchar(255) DEFAULT NULL,
  `Descrizione_EN` varchar(50) NOT NULL,
  `Descrizione_FR` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `EXPO_Tlk_Certificazioni`
--

INSERT INTO `EXPO_Tlk_Certificazioni` (`Id`, `IDCert`, `Descrizione`, `Descrizione_EN`, `Descrizione_FR`) VALUES
(1, 'LEED', 'Certificazione di edifici ecosostenibili', 'Green building certification ', 'Certification des bâtiments verts'),
(2, 'ISO 9001', 'Certificazione sistemi di gestione per la qualità', 'Quality management systems certification', 'Certification de systèmes de management de la qual'),
(3, 'ISO 14001', 'Certificazione sistemi di gestione ambientale', 'Corporate Environmental management systems certifi', 'Systèmes de management environnemental '),
(4, 'SA 8000', 'Certificazione della responsabilità sociale ed etica', 'Corporate Social Responsibility certification', 'Certification de la responsabilité sociale de l’en'),
(5, 'OHSAS 18001', 'Certificazione di sistema di gestione della salute e sicurezza dei lavoratori ', 'Occupational health and safety Management system c', 'Systèmes de management de la santé et la sécurité '),
(6, 'ISO 14064', 'Certificazione sistemi di gestione ambientale', 'Corporate Environmental management systems certifi', 'Systèmes de management environnemental '),
(7, 'ISO 27001', 'Certificazione del sistema di gestione delle informazioni ', 'Information security management certification ', 'Certification  de système de gestion de la sécurit');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
