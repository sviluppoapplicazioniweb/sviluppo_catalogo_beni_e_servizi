-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ott 25, 2013 alle 16:04
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_Accreditamenti`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Accreditamenti` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `IdAccreditamento` int(11) NOT NULL,
  `NCertificato` varchar(10) NOT NULL,
  `DataEmissione` varchar(10) NOT NULL,
  `DataScadenza` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Imprese_Accreditamenti`
--

INSERT INTO `EXPO_TJ_Imprese_Accreditamenti` (`Id`, `IdImpresa`, `IdAccreditamento`, `NCertificato`, `DataEmissione`, `DataScadenza`) VALUES
(1, 1, 0, '123456', '14/01/2001', '14/10/2015');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
