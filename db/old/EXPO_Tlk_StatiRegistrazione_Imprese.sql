﻿# Host: localhost  (Version: 5.5.16)
# Date: 2013-10-09 15:43:19
# Generator: MySQL-Front 5.3  (Build 4.4)

/*!40101 SET NAMES utf8 */;

#
# Source for table "EXPO_Tlk_StatiRegistrazione_Imprese"
#

DROP TABLE IF EXISTS `EXPO_Tlk_StatiRegistrazione_Imprese`;
CREATE TABLE `EXPO_Tlk_StatiRegistrazione_Imprese` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Descrizione` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "EXPO_Tlk_StatiRegistrazione_Imprese"
#

/*!40000 ALTER TABLE `EXPO_Tlk_StatiRegistrazione_Imprese` DISABLE KEYS */;
INSERT INTO `EXPO_Tlk_StatiRegistrazione_Imprese` VALUES (1,'Accettazione Visura'),(2,'Accettazione Regolamento'),(3,'Pagamento'),(4,'Impresa Offline'),(5,'Impresa Online');
/*!40000 ALTER TABLE `EXPO_Tlk_StatiRegistrazione_Imprese` ENABLE KEYS */;
