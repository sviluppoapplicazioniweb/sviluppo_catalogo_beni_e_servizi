﻿# Host: localhost  (Version: 5.5.16)
# Date: 2013-10-07 08:20:59
# Generator: MySQL-Front 5.3  (Build 4.4)

/*!40101 SET NAMES utf8 */;

#
# Source for table "EXPO_Tlk_Lingue"
#

DROP TABLE IF EXISTS `EXPO_Tlk_Lingue`;
CREATE TABLE `EXPO_Tlk_Lingue` (
  `Id` int(11) NOT NULL,
  `Lingua` varchar(50) NOT NULL,
  `Lingua_EN` varchar(255) NOT NULL DEFAULT '',
  `Lingua_FR` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
