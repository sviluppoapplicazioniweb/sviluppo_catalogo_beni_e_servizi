--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Ateco`
--


CREATE TABLE IF NOT EXISTS `EXPO_T_Ateco` (
  `Id` int(11) NOT NULL,
  `CodiceAteco` varchar(10) NOT NULL,
  `Descrizione` varchar(255) NOT NULL,
  `IsPresente` varchar(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Bacheca`
--

 
--

-- --------------------------------------------------------
 
--
-- Struttura della tabella `EXPO_T_Bacheca_Messaggi`
--


-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Imprese`
--

ALTER TABLE  `EXPO_T_Imprese` ADD  `FatturatoUltimoEsercizio` VARCHAR( 50 ) NOT NULL AFTER  `UltimoFatturato`;
ALTER TABLE  `EXPO_T_Imprese` ADD  `IsPrestatoreDiServizi` VARCHAR( 1 ) NOT NULL DEFAULT  'N' AFTER  `IsDistributore`;
ALTER TABLE  `EXPO_T_Imprese` CHANGE  `CapitaleSociale`  `CapitaleSociale` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  '0';

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Prodotti`
--
 
UPDATE `EXPO_T_Prodotti` SET StatoProdotto = 'D' WHERE StatoProdotto = "";


--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_Accreditamenti`
--
DROP TABLE IF  EXISTS `EXPO_TJ_Imprese_Accreditamenti`;
CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Accreditamenti` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `IdAccreditamento` int(11) NOT NULL,
  `NCertificato` varchar(10) NOT NULL,
  `DataEmissione` varchar(10) NOT NULL,
  `DataScadenza` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Imprese_Accreditamenti`
--

INSERT INTO `EXPO_TJ_Imprese_Accreditamenti` (`Id`, `IdImpresa`, `IdAccreditamento`, `NCertificato`, `DataEmissione`, `DataScadenza`) VALUES
(1, 1, 0, '123456', '14/01/2001', '14/10/2015');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_Ateco`
--
DROP TABLE IF  EXISTS `EXPO_TJ_Imprese_Ateco`;
CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Ateco` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `IdAteco` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
DROP TABLE IF  EXISTS `EXPO_Tlk_Accreditamenti`;
CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Accreditamenti` (
  `Id` int(11) NOT NULL,
  `Sigla` varchar(5) NOT NULL,
  `Descrizione` varchar(500) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_Tlk_Accreditamenti`
--

INSERT INTO `EXPO_Tlk_Accreditamenti` (`Id`, `Sigla`, `Descrizione`) VALUES
(1, 'SGQ ', 'Certificazioni di sistemi di gestione per la qualit�'),
(2, 'SGA', 'Certificazioni di sistemi di gestione ambientale'),
(3, 'SGE', 'Certificazioni di sistemi di gestione dell''energia'),
(4, 'PRD', 'Certificazioni di prodotti / servizi'),
(5, 'PRS', 'Certificazioni di personale'),
(6, 'ISP', 'Ispezione'),
(7, 'SCR', 'Certificazioni di sistemi di gestione per la salute e sicurezza sul lavoro'),
(8, 'SSI', 'Certificazioni di sistemi di gestione per la sicurezza delle informazioni'),
(9, 'ITX', 'Certificazioni di sistemi di gestione Servizi Informatici'),
(10, 'DAP', 'Verifica e Convalida delle dichiarazioni ambientali di prodotto'),
(11, 'FSM', 'Certificazioni di sistemi di gestione per la sicurezza alimentare'),
(12, 'GHG', 'Verifica dei gas ad effetto serra'),
(14, 'EMAS', 'Certificazione per l''attivit� di Verifica Ambientale EMAS');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Cariche_Rappresentanti`
--
DROP TABLE IF  EXISTS `EXPO_Tlk_Cariche_Rappresentanti`;
CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Cariche_Rappresentanti` (
  `Id` int(11) NOT NULL,
  `Carica` varchar(255) NOT NULL,
  `Sigla` varchar(5) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_Tlk_Cariche_Rappresentanti`
--

INSERT INTO `EXPO_Tlk_Cariche_Rappresentanti` (`Id`, `Carica`, `Sigla`) VALUES
(1, 'AMMINISTRATORE CON POSTILLA', 'ACP'),
(2, 'AMMINISTRATORE CON REQUISITI', 'ACR'),
(3, 'AMMINISTRATORE DELEGATO E PREPOSTO', 'ADP'),
(4, 'AMMINISTRATORE DELEGATO', 'AMD'),
(5, 'AMMINISTRATORE GIUDIZIARIO', 'AMG'),
(6, 'AMMINISTRATORE', 'AMM'),
(7, 'AMMINISTRATORE PROVVISORIO', 'AMP'),
(8, 'AMMINISTRATORE STRAORDINARIO', 'AMS'),
(9, 'AMMINISTRATORE E PREPOSTO', 'APR'),
(10, 'AMMINISTRATORE E RESPONSABILE TECNICO', 'ART'),
(11, 'TITOLARE DELL''IMPRESA ARTIGIANA', 'ATI'),
(12, 'AMMINISTRATORE UNICO', 'AUN'),
(13, 'AMMINISTRATORE UNICO E PREPOSTO', 'AUP'),
(14, 'NOMINA AD AMMINISTRATORE DELEGATO', 'C01'),
(15, 'NOMINA AD AMMINISTRATORE GIUDIZIARIO', 'C02'),
(16, 'NOMINA AD AMMINISTRATORE', 'C03'),
(17, 'NOMINA AD AMMINISTRATORE UNICO', 'C07'),
(18, 'NOMINA A CONSIGLIERE DELEGATO', 'C08'),
(19, 'NOMINA A COMMISSARIO GIUDIZIARIO', 'C09'),
(20, 'NOMINA A COMMISSARIO LIQUIDATORE', 'C11'),
(21, 'NOMINA A LEGALE RAPPRESENTANTE DI SOCIETA''', 'C13'),
(22, 'NOMINA A LIQUIDATORE', 'C14'),
(23, 'NOMINA A LIQUIDATORE GIUDIZIARIO', 'C15'),
(24, 'NOMINA A PRESIDENTE CONSIGLIO AMMINISTRAZIONE', 'C19'),
(25, 'NOMINA A PRESIDENTE COMITATO DIRETTIVO', 'C20'),
(26, 'NOMINA A PRESIDENTE COMITATO ESECUTIVO', 'C21'),
(27, 'NOMINA A RAPPRESENTANTE LEGALE DELLE SEDI SECONDARIE', 'C26'),
(28, 'NOMINA A VICE AMMINISTRATORE', 'C32'),
(29, 'NOMINA A VICE PRESIDENTE CONSIGLIO AMMINISTRAZIONE', 'C34'),
(30, 'CONSIGLIERE E DIRETTORE GENERALE', 'CDG'),
(31, 'CONSIGLIERE DELEGATO E PREPOSTO', 'CDP'),
(32, 'CONSIGLIERE DELEGATO E RESPONSABILE TECNICO', 'CDT'),
(33, 'CONDIRETTORE COMMERCIALE', 'CE'),
(34, 'CONSIGLIERE E PREPOSTO', 'CEP'),
(35, 'CONSIGLIERE E LEGALE RAPPRESENTANTE', 'CLR'),
(36, 'COMMISSARIO MINISTERIALE', 'CM'),
(37, 'COMMISSARIO STRAORDINARIO', 'CMS'),
(38, 'COAMMINISTRATORE', 'COA'),
(39, 'CONSIGLIERE DELEGATO', 'COD'),
(40, 'COMMISSARIO GIUDIZIARIO', 'COG'),
(41, 'COMMISSARIO LIQUIDATORE', 'COL'),
(42, 'COMMISSARIO PREFETTIZIO', 'COP'),
(43, 'CONSIGLIERE SEGRETARIO', 'COS'),
(44, 'COMMISSARIO GOVERNATIVO', 'COV'),
(45, 'COMMISSARIO GIUDIZIALE', 'COZ'),
(46, 'CURATORE', 'CRT'),
(47, 'CONDIRETTORE DI STABILIMENTO', 'CS'),
(48, 'CUSTODE SEQUESTRO GIUDIZIARIO', 'CSG'),
(49, 'COMMISSARIO STRAORDINARIO', 'CST'),
(50, 'CURATORE DELLO EMANCIPATO', 'CUE'),
(51, 'CURATORE FALLIMENTARE', 'CUF'),
(52, 'CURATORE SPECIALE DI MINORE', 'CUM'),
(53, 'DIRETTORE AMMINISTRATIVO', 'DA'),
(54, 'DIRIGENTE CON POTERE', 'DCP'),
(55, 'DIRETTORE COMMERCIALE', 'DE'),
(56, 'DELEGATO ALLA SOMMINISTRAZIONE', 'DES'),
(57, 'DIRETTORE DI FILIALE', 'DF'),
(58, 'DIRETTORE FINANZE', 'DFI'),
(59, 'DIRETTORE GENERALE', 'DG'),
(60, 'DELEGATO DI CUI ART. 2 LEGGE 25/8/91 N.287', 'DL2'),
(61, 'DELEGATO ALLA FIRMA', 'DLF'),
(62, 'DIP. DI IMPRESA AUTORIZ. AL SERV. NOLEG. CON CONDUCENTE', 'DNC'),
(63, 'DIRETTORE', 'DR'),
(64, 'DELEGATO ISCRIZIONE REC', 'DRE'),
(65, 'DIRETTORE RESPONSABILE', 'DRR'),
(66, 'DELEGATO DI CUI ALL''ART. 2 DELLA LEGGE 287 DEL 25.8.1991', 'DSA'),
(67, 'DIRETTORE TECNICO', 'DT'),
(68, 'GERENTE', 'GER'),
(69, 'ISPETTORE GENERALE', 'IG'),
(70, 'INSTITORE', 'IN'),
(71, 'LEGALE RAPPRESENTANTE', 'LER'),
(72, 'LEGALE RAPPRESENTANTE DI SOCIETA''', 'LGR'),
(73, 'LIQUIDATORE', 'LI'),
(74, 'LIQUIDATORE GIUDIZIARIO', 'LIG'),
(75, 'LEGALE RAPPRESENTANTE ART.2 L. 25/8/91 N.287', 'LR2'),
(76, 'LEGALE RAPPRESENTANTE / FIRMATARIO', 'LRF'),
(77, 'LEGALE RAPPRESENTANTE E RESPONSABILE TECNICO', 'LRT'),
(78, 'LEGALE RAPPRESENTANTE DI CUI ALL''ART. 2 DELLA LEGGE 287 DEL 25.8.1991', 'LSA'),
(79, 'LIQUIDATORE DI UNITA'' LOCALE', 'LUL'),
(80, 'MADRE ESERCENTE LA PATRIA POTESTA''', 'MPP'),
(81, 'MANDATO SPECIALE', 'MS'),
(82, 'ACCOMANDATARIO DI SAPA', 'OAS'),
(83, 'PERSONA OPERANTE PER CONTO DELLA SOCIETA''', 'OPC'),
(84, 'PRESIDENTE DI CONSORZIO', 'OPN'),
(85, 'PROCURATORE AMMINISTRATIVO', 'PA'),
(86, 'PRESIDENTE E AMMINISTRATORE DELEGATO', 'PAD'),
(87, 'PERSONA AUTORIZZATA ALLA FIRMA', 'PAF'),
(88, 'PROCURATORE', 'PC'),
(89, 'PRESIDENTE CONSIGLIO AMMINISTRAZIONE', 'PCA'),
(90, 'PRESIDENTE COMITATO DIRETTIVO', 'PCD'),
(91, 'PRESIDENTE COMITATO ESECUTIVO', 'PCE'),
(92, 'PRESIDENTE DEL COMITATO DI GESTIONE', 'PCG'),
(93, 'PRESIDENTE COMMISS. AMMINISTRATIVA', 'PCM'),
(94, 'PRESIDENTE CONSORZIO', 'PCO'),
(95, 'PROCURATORE CON POSTILLA', 'PCP'),
(96, 'PRESIDENTE DEL COMITATO DI CONTROLLO SULLA GESTIONE', 'PCT'),
(97, 'PRESIDENTE DEL CONSIGLIO DI SORVEGLIANZA', 'PCV'),
(98, 'PRESIDENTE E CONSIGLIERE DELEGATO', 'PDC'),
(99, 'RAPPRESENTANTE PREPOSTO ALLA DIPENDENZA IN ITALIA', 'PDI'),
(100, 'PRESIDENTE EFFETTIVO CONSIGLIO DIRETTIVO', 'PED'),
(101, 'PRESIDENTE E PREPOSTO', 'PEO'),
(102, 'PROCURATORE DI FILIALE', 'PF'),
(103, 'PROCURATORE GENERALE', 'PG'),
(104, 'PRESIDENTE CONSIGLIO DIRETTIVO', 'PGD'),
(105, 'PRESIDENTE GIUNTA ESECUTIVA', 'PGE'),
(106, 'PRESIDENTE DEL CONSIGLIO DI GESTIONE', 'PGS'),
(107, 'PADRE O MADRE ESERCENTE LA PATRIA POTESTA''', 'PM'),
(108, 'PROCURATORE AD NEGOTIA', 'PN'),
(109, 'PROCURATORE SPECIALE', 'PP'),
(110, 'PADRE ESERCENTE LA PATRIA POTESTA''', 'PPP'),
(111, 'PRESIDENTE', 'PRE'),
(112, 'PRESIDENTE ONORARIO', 'PRO'),
(113, 'RAPPRESENTANTE PREPOSTO A SEDE SECONDARIA IN ITALIA', 'PSE'),
(114, 'PREPOSTO DELLA SEDE SECONDARIA', 'PSS'),
(115, 'PRESIDENTE E RESPONSABILE TECNICO', 'PTE'),
(116, 'RAPPRESENTATE LEGALE DI CUI ALL''ART. 93 DEL R.D. 18/6/1931 N. 773', 'RAF'),
(117, 'RAPPRESENTANTE LEGALE DI CUI ALL''ART. 93 DEL R.D. 18/6/1931 N. 773', 'RAP'),
(118, 'RAPPRESENTANTE SOCIETA'' ESTERA', 'RES'),
(119, 'LEGALE RAPPRESENTANTE FIGLIO MINORE', 'RFM'),
(120, 'LEGALE RAPPRESENTANTE DI INCAPACE', 'RIN'),
(121, 'RAPPRESENTANTE IN ITALIA', 'RIT'),
(122, 'RAPPRESENTANTE LEGGE P.S.', 'RPS'),
(123, 'RAPPRESENTANTE LEGALE DELLE SEDI SECONDARIE', 'RSS'),
(124, 'SOCIO ACCOMANDATARIO D''OPERA', 'SA'),
(125, 'SOCIO ACCOMANDATARIO D''OPERA', 'SAO'),
(126, 'SOCIO ACCOMANDATARIO E PREPOSTO', 'SAP'),
(127, 'SOCIO RAPPRESENTANTE', 'SCR'),
(128, 'SEGRETARIO GENERALE', 'SGE'),
(129, 'SOCIO E LEGALE RAPPRESENTANTE', 'SLR'),
(130, 'SOCIO AMMINISTRATORE', 'SOA'),
(131, 'SOCIO DI SOCIETA'' DI FATTO', 'SOF'),
(132, 'SOCIO ACCOMANDATARIO E RAPPRESENTANTE LEGALE', 'SOL'),
(133, 'SOCIO DI SOCIETA'' IN NOME COLLETTIVO', 'SON'),
(134, 'SOCIO ACCOMANDATARIO', 'SOR'),
(135, 'SOCIO DI SOCIETA'' DI PERSONE RAPPRES.', 'SPR'),
(136, 'TITOLARE', 'TI'),
(137, 'TITOLARE DI CUI ART. 2 LEGGE 25/8/91 N.287', 'TI2'),
(138, 'TITOLARE FIRMATARIO', 'TIT'),
(139, 'TITOLARE E RESPONSABILE TECNICO', 'TTE'),
(140, 'TUTORE', 'TU'),
(141, 'RAPPRESENTANTE LEGALE DI CUI ALL''ART. 2 LEGGE REG. N. 37 DEL 30/8/1988', 'UM1');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Certificazioni`
--

DROP TABLE IF EXISTS `EXPO_Tlk_Certificazioni`;
CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Certificazioni` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCert` varchar(25) DEFAULT NULL,
  `Descrizione` varchar(255) DEFAULT NULL,
  `Descrizione_EN` varchar(50) NOT NULL,
  `Descrizione_FR` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `EXPO_Tlk_Certificazioni`
--

INSERT INTO `EXPO_Tlk_Certificazioni` (`Id`, `IDCert`, `Descrizione`, `Descrizione_EN`, `Descrizione_FR`) VALUES
(1, 'LEED', 'Certificazione di edifici ecosostenibili', 'Green building certification ', 'Certification des b�timents verts'),
(2, 'ISO 9001', 'Certificazione sistemi di gestione per la qualit�', 'Quality management systems certification', 'Certification de syst�mes de management de la qual'),
(3, 'ISO 14001', 'Certificazione sistemi di gestione ambientale', 'Corporate Environmental management systems certifi', 'Syst�mes de management environnemental '),
(4, 'SA 8000', 'Certificazione della responsabilit� sociale ed etica', 'Corporate Social Responsibility certification', 'Certification de la responsabilit� sociale de l�en'),
(5, 'OHSAS 18001', 'Certificazione di sistema di gestione della salute e sicurezza dei lavoratori ', 'Occupational health and safety Management system c', 'Syst�mes de management de la sant� et la s�curit� '),
(6, 'ISO 14064', 'Certificazione sistemi di gestione ambientale', 'Corporate Environmental management systems certifi', 'Syst�mes de management environnemental '),
(7, 'ISO 27001', 'Certificazione del sistema di gestione delle informazioni ', 'Information security management certification ', 'Certification  de syst�me de gestion de la s�curit');


--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Forme_Giuridiche`
--

DROP TABLE IF EXISTS `EXPO_Tlk_Forme_Giuridiche`;
CREATE TABLE `EXPO_Tlk_Forme_Giuridiche` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Codice` varchar(255) NOT NULL DEFAULT '',
  `Descrizione` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "EXPO_Tlk_Forme_Giuridiche"
#

INSERT INTO `EXPO_Tlk_Forme_Giuridiche` VALUES (1,'AA','SOCIETA\' IN ACCOMANDITA PER AZIONI'),(2,'AE','SOCIETA\' CONSORTILE IN ACCOMANDITA SEMPLICE'),(3,'AI','ASSOCIAZIONE IMPRESA'),(4,'AL','AZIENDA SPECIALE'),(5,'AN','SOCIETA\' CONSORTILE IN NOME COLLETTIVO'),(6,'AS','SOCIETA\' IN ACCOMANDITA SEMPLICE'),(7,'AT','AZIENDA AUTONOMA STATALE'),(8,'AU','SOCIETA\' PER AZIONI CON SOCIO UNICO'),(9,'CF','CONSORZIO FIDI'),(10,'CM','CONSORZIO MUNICIPALE'),(11,'CO','CONSORZIO'),(12,'CZ','CONSORZIO DI CUI ALLA DLGS 267/2000'),(13,'ED','ENTE DIRITTO PUBBLICO'),(14,'EI','ENTE IMPRESA'),(15,'EL','ENTE SOCIALE'),(16,'EP','ENTE PUBBLICO ECONOMICO'),(17,'FI','FONDAZIONE IMPRESA'),(18,'GE','GRUPPO EUROPEO DI INTERESSE ECONOMICO'),(19,'LL','AZIENDA SPECIALE DI CUI AL DLGS 267/2000'),(20,'MA','MUTUA ASSICURAZIONE'),(21,'OC','SOCIETA\' CONSORTILE COOPERATIVA'),(22,'SC','SOCIETA\' COOPERATIVA'),(23,'SD','SOCIETA\' EUROPEA'),(24,'SE','SOCIETA\' SEMPLICE'),(25,'SG','SOCIETA\' COOPERATIVA EUROPEA'),(26,'SL','SOCIETA\' CONSORTILE A RESPONSABILITA\' LIMITATA'),(27,'SM','SOCIETA\' DI MUTUO SOCCORSO'),(28,'SN','SOCIETA\' IN NOME COLLETTIVO'),(29,'SO','SOCIETA\' CONSORTILE PER AZIONI'),(30,'SP','SOCIETA\' PER AZIONI'),(31,'SR','SOCIETA\' A RESPONSABILITA\' LIMITATA'),(32,'SS','SOCIETA\' COSTITUITA IN BASE A LEGGI DI ALTRO STATO'),(33,'SU','SOCIETA\' A RESPONSABILITA\' LIMITATA CON UNICO SOCIO'),(34,'SV','SOCIETA\' TRA PROFESSIONISTI');
--

-- --------------------------------------------------------

 
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_StatiRegistrazione_Imprese`
--
DROP TABLE IF EXISTS `EXPO_Tlk_StatiRegistrazione_Imprese`;
CREATE TABLE `EXPO_Tlk_StatiRegistrazione_Imprese` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Descrizione` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "EXPO_Tlk_StatiRegistrazione_Imprese"
#

INSERT INTO `EXPO_Tlk_StatiRegistrazione_Imprese` VALUES (1,'Accettazione Visura'),(2,'Accettazione Regolamento'),(3,'Pagamento'),(4,'Impresa Offline'),(5,'Impresa Online');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Tipo_Contestazioni`
--
DROP TABLE IF EXISTS `EXPO_Tlk_Tipo_Contestazioni`;
CREATE TABLE IF NOT EXISTS `EXPO_Tlk_Tipo_Contestazioni` (
  `Id` int(11) NOT NULL,
  `Sigla` varchar(5) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `isPermessa` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--

-- --------------------------------------------------------

--
-- Struttura della tabella `SVIL_T_Menu`
--

DROP TABLE IF EXISTS `SVIL_T_Menu`;
CREATE TABLE IF NOT EXISTS `SVIL_T_Menu` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Id_Menu` varchar(50) NOT NULL DEFAULT '00',
  `Id_Sottomenu` tinyint(4) NOT NULL DEFAULT '0',
  `Id_Priorita` tinyint(4) NOT NULL DEFAULT '0',
  `Id_TipoUtente` tinyint(4) NOT NULL DEFAULT '0',
  `TipiUtente` varchar(100) NOT NULL DEFAULT '|',
  `Gruppi` varchar(250) NOT NULL DEFAULT '|',
  `MasterEditor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Stato_Menu` char(1) NOT NULL DEFAULT 'A',
  `Tipo` char(1) NOT NULL DEFAULT 'M',
  `Esploso` char(1) NOT NULL DEFAULT 'N',
  `Mtop` varchar(50) NOT NULL DEFAULT '00',
  `Titolo` varchar(100) NOT NULL,
  `Descrizione` varchar(250) DEFAULT NULL,
  `Keywords` varchar(250) DEFAULT NULL,
  `Popup` mediumtext,
  `Id_Pagina` int(10) unsigned DEFAULT NULL,
  `Id_Modifica` int(11) NOT NULL DEFAULT '0',
  `Data_Modifica` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Flag_Modifica` tinyint(1) unsigned NOT NULL DEFAULT '4',
  `Flag_Struttura` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `PubblicatoDa` varchar(20) NOT NULL DEFAULT '',
  `Data_Pubblicazione` datetime DEFAULT NULL,
  `Id_Articolo` int(11) NOT NULL DEFAULT '0',
  `LinkEsterno` varchar(100) DEFAULT '',
  `MenuTemplate` varchar(20) DEFAULT NULL,
  `Id_Ufficio` int(11) NOT NULL DEFAULT '0',
  `IPFilter` tinyint(1) NOT NULL DEFAULT '0',
  `Help_HTML` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_Menu` (`Id_Menu`,`Id_Sottomenu`,`Id_Priorita`,`Id_TipoUtente`,`Stato_Menu`,`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=316 ;

--
-- Dump dei dati per la tabella `SVIL_T_Menu`
--

INSERT INTO `SVIL_T_Menu` (`Id`, `Id_Menu`, `Id_Sottomenu`, `Id_Priorita`, `Id_TipoUtente`, `TipiUtente`, `Gruppi`, `MasterEditor`, `Stato_Menu`, `Tipo`, `Esploso`, `Mtop`, `Titolo`, `Descrizione`, `Keywords`, `Popup`, `Id_Pagina`, `Id_Modifica`, `Data_Modifica`, `Flag_Modifica`, `Flag_Struttura`, `PubblicatoDa`, `Data_Pubblicazione`, `Id_Articolo`, `LinkEsterno`, `MenuTemplate`, `Id_Ufficio`, `IPFilter`, `Help_HTML`) VALUES
(1, '00', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'HomePage', 'Home Page', 'Home Page', '', 0, 1, '2013-10-22 11:30:06', 1, 1, '1', '2013-10-22 11:30:17', 0, '709417', '', 0, 0, NULL),
(2, '00.01', 0, 0, 99, '|', '|', 0, 'A', 'P', '', '00', 'Registrazione Azienda', 'Registrati', 'Registrati', '', 231, 1, '2013-07-01 12:21:51', 1, 1, '1', '2013-07-01 12:22:06', 0, '', NULL, 0, 0, NULL),
(3, '00.11', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Comunicazioni', 'Comunicazioni', 'Comunicazioni', NULL, 105, 1, '2007-12-19 19:16:49', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(4, '00.04', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Archivio News', 'Archivio News', 'Archivio News', '', 57, 1, '2006-01-26 08:59:51', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(5, '00.05', 1, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Eventi', 'Eventi', 'Eventi', '', 60, 1, '2006-05-15 10:47:01', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(6, '00.06', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Invia ad un amico', 'Invia ad un amico', 'Invia ad un amico', '', 58, 1, '2006-03-01 10:12:08', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(7, '00.08', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pagina dinamica', 'Pagina dinamica', 'Pagina dinamica', '', 47, 1, '2005-10-11 14:11:40', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(8, '00.02', 0, 0, 0, '|', '|', 0, 'A', 'P', '', '02', 'Site map', 'Site map', 'Site map', '', 8, 1, '2012-06-27 10:19:38', 1, 1, '1', '2012-06-27 10:21:21', 0, '', NULL, 0, 0, NULL),
(9, '00.09', 0, 2, 0, '|', '|', 0, 'A', 'P', '', '00', 'Modifica i tuoi dati', 'Modifica i tuoi dati', 'Modifica i tuoi dati', '', 9, 1, '2013-04-11 16:08:00', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(10, '80', 0, 7, 0, '|', '|', 0, 'A', 'L', '', '00', 'AREA RISERVATA', 'Area Riservata', 'Area Riservata', '', 100, 1, '2013-06-10 16:40:47', 1, 1, '1', '2013-06-10 16:41:16', 0, '', '', 0, 0, NULL),
(11, '80', 1, 10, 0, '|', '|', 0, 'A', 'M', 'S', '00', 'Anagrafiche', 'Anagrafiche', 'Anagrafiche', '', 11, 1, '2013-08-02 09:49:09', 1, 1, '1', '2013-08-02 09:49:25', 0, '', '', 0, 0, NULL),
(12, '80.10', 0, 7, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Editor', 'Editor', 'Editor', '', 11, 1, '2013-04-11 16:13:15', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(13, '80.15', 0, 10, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Editor', 'Inserimento Editor', 'Inserimento Editor', '', 21, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(14, '80.17', 0, 10, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Editor', 'Modifica Editor', 'Modifica Editor', '', 22, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(15, '80.30', 0, 9, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Redattori', 'Redattori', 'Redattori', '', 78, 1, '2008-03-12 15:23:46', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(16, '80.35', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Redattore', 'Inserimento Redattore', 'Inserimento Redattore', '', 70, 1, '2006-08-07 16:40:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(17, '80.37', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Redattore', 'Modifica Redattore', 'Modifica Redattore', '', 71, 1, '2006-08-07 17:57:13', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(18, '80.40', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Partner', 'Partner', 'Partner', '', 79, 1, '2006-08-08 09:47:42', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(19, '80.45', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Tipo Utente 6', 'Inserimento Tipo Utente 6', 'Inserimento Tipo Utente 6', '', 72, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(20, '80.47', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Tipo Utente 6', 'Modifica Tipo Utente 6', 'Modifica Tipo Utente 6', '', 73, 1, '2004-11-17 16:47:11', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(21, '80.90', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Utenti', 'Utenti', 'Utenti', '', 12, 1, '2008-03-12 15:24:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(22, '80.91', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Password Dimenticata', 'Password Dimenticata', 'Password Dimenticata', '', 15, 1, '2006-04-28 13:52:30', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(23, '80.93', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Inserimento Utente', 'Inserimento Utente', 'Inserimento Utente', '', 20, 1, '2006-04-28 11:27:30', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(24, '80.94', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Benvenuto', 'Benvenuto', 'Benvenuto', '', 14, 1, '2006-04-28 12:34:48', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(25, '80.95', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Utente', 'Modifica Utente', 'Modifica Utente', '', 23, 1, '2004-11-17 18:17:50', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(26, '00.10', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Ricerca', 'Ricerca', 'Ricerca', '', 59, 1, '2006-02-07 14:01:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(27, '81', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Gruppi', 'Gruppi', 'Gruppi', NULL, 27, 1, '2010-12-15 19:51:26', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(28, '81.10', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione Gruppi', 'Gestione Gruppi', 'Gestione Gruppi', NULL, 28, 1, '2010-12-15 19:52:02', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(29, '84.40', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Invio Newsletter', 'Invio Newsletter', 'Invio Newsletter', NULL, 109, 1, '2008-12-16 10:35:13', 1, 3, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(30, '84', 1, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Newsletter', 'Newsletter', 'Newsletter', '', 51, 1, '2010-04-28 10:43:26', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(31, '84.05', 1, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Newsletter News', 'Newsletter News', 'Newsletter News', '', 56, 1, '2010-04-28 10:43:35', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(32, '84.10', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Template', 'Template', 'Template', '', 53, 1, '2008-12-16 10:34:35', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(33, '84.20', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Email', 'Email', 'Email', '', 52, 1, '2008-12-16 10:34:47', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(34, '84.30', 0, 8, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Argomenti', 'Argomenti', 'Argomenti', '', 54, 1, '2008-12-16 10:35:02', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(35, '92', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Mappa Admin', 'Mappa Admin', 'Mappa Admin', '', 38, 1, '2013-06-12 16:34:16', 1, 1, '1', '2013-06-12 16:34:20', 0, '', '', 0, 0, NULL),
(37, '94', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Eventi', 'Eventi', 'Eventi', '', 61, 1, '2012-06-05 15:08:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(38, '95', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'News', 'News', 'News', '', 50, 1, '2006-01-23 18:19:25', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(40, '97', 0, 9, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Editoria', 'Editoria', 'Editoria', '', 138, 1, '2010-04-27 16:51:40', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(50, '90', 0, 8, 0, '|', '|', 0, 'A', 'L', '', '00', 'Area CMS', 'Area CMS', 'Area CMS', '', 100, 1, '2006-08-07 19:11:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(51, '90.01', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Amministrazione', 'Amministrazione', 'Amministrazione', '', 4, 1, '2006-03-09 11:28:06', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(52, '91', 1, 10, 0, '|', '|', 0, 'A', 'M', 'S', '00', 'Menu', 'Menu', 'Menu', '', 62, 1, '2013-01-16 14:35:19', 1, 1, '1', '2013-01-16 14:35:54', 0, '', NULL, 0, 0, NULL),
(53, '91', 1, 8, 0, '|2|3|', '|', 0, 'A', 'M', 'S', '00', 'Menu', 'Menu', 'Menu', '', 38, 1, '2006-08-07 19:06:34', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(54, '91', 1, 8, 1, '|1|2|3|', '|', 0, 'A', 'P', 'S', '00', 'Menu', 'Menu', 'Menu', '', 62, 1, '2006-08-07 19:07:10', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(55, '91.01', 0, 8, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Menu_AL', 'Gestione T_Menu_AL', 'Gestione T_Menu_AL', '', 67, 1, '2006-08-07 19:12:44', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(56, '91.05', 0, 8, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Articoli', 'Articoli', 'Articoli', '', 64, 1, '2013-06-12 16:21:29', 1, 1, '1', '2013-06-12 16:21:34', 0, '', '', 0, 0, NULL),
(57, '91.05.01', 0, 8, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Articoli_AL', 'Gestione T_Articoli_AL', 'Gestione T_Articoli_AL', '', 66, 1, '2006-08-07 19:13:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(58, '91.10', 0, 10, 1, '|1|', '|', 0, 'A', 'M', 'N', '00', 'Templates', 'Templates', 'Templates', '', 33, 1, '2004-10-13 09:26:08', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(59, '91.10.01', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Pagine_AL', 'Gestione T_Pagine_AL', 'Gestione T_Pagine_AL', '', 44, 1, '2005-09-06 16:39:24', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(60, '91.15', 0, 8, 0, '|', '|', 0, 'A', 'P', '', '00', 'Gestione Box', 'Gestione Box', 'Gestione Box', '', 63, 1, '2006-08-07 19:12:20', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(61, '91.16', 0, 8, 0, '|', '|', 0, 'A', 'P', '', '00', 'Gestione Articoli_X_Box', 'Gestione Articoli_X_Box', 'Gestione Articoli_X_Box', '', 65, 1, '2006-08-07 19:12:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(62, '91.20', 0, 10, 1, '|', '|', 0, 'A', 'M', 'N', '00', 'Tipi Html', 'Tipi Html', 'Tipi Html', '', 34, 1, '2004-11-15 17:49:20', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(63, '91.32', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Tipi articoli', 'Tipi articoli', 'Tipi articoli', '', 46, 1, '2005-10-12 16:35:55', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(64, '91.35', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Sottocategorie', 'Sottocategorie', 'Sottocategorie', '', 40, 1, '2007-01-23 12:33:09', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(65, '91.40', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Upload Immagini', 'Upload Immagini', 'Upload Immagini', '', 35, 1, '2007-07-24 18:13:54', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(66, '91.50', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Upload Files', 'Upload Files', 'Upload Files', '', 36, 1, '2007-01-23 12:32:21', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(67, '91.60', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Privacy', 'Privacy', 'Privacy', '', 13, 1, '2013-04-23 15:07:10', 1, 1, '1', '2013-04-23 15:07:24', 0, '', NULL, 0, 0, NULL),
(68, '91.08', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pubblicazione', 'Pubblicazione', 'Pubblicazione', '', 68, 1, '2006-09-20 18:53:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(69, '91.07', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Inizializzazione lingue', 'Inizializzazione lingue', 'Inizializzazione lingue', '', 55, 1, '2006-09-20 18:53:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(70, '80.07', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Attivazione Utenti', 'Attivazione Utenti', 'Attivazione Utenti', NULL, 101, 1, '2007-12-14 12:53:08', 1, 3, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(71, '85.10', 0, 10, 0, '|', '|', 0, 'D', 'P', 'N', '00', 'Modifica Messaggistica CMS', 'Modifica Messaggistica CMS', 'Modifica Messaggistica CMS', NULL, 104, 1, '2013-08-26 16:39:34', 1, 3, '1', '2013-08-26 16:39:54', 0, '', NULL, 0, 0, NULL),
(82, '82', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Categorie', 'Categorie', 'Categorie', NULL, 95, 1, '2013-07-24 10:47:16', 1, 1, '1', '2013-07-24 10:47:24', 0, '', NULL, 0, 0, NULL),
(85, '10', 0, 1, 1, '|', '|', 0, 'A', 'P', '', '00', 'Completamento registrazione', 'Completamento registrazione', 'Completamento registrazione', '', 85, 1, '2013-09-05 09:47:21', 1, 1, '1', '2013-09-05 09:47:35', 0, '', NULL, 0, 0, NULL),
(101, '01', 0, 0, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Toolbar', 'Toolbar', 'Toolbar', '', 3, 1, '2006-05-31 13:23:33', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(231, '82.10', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione Categorie', 'Gestione Categorie', 'Gestione Categorie', NULL, 220, 1, '2013-01-15 14:06:45', 1, 3, '1', '2013-01-15 14:36:09', 0, '', NULL, 0, 0, NULL),
(243, '03', 1, 1, 0, '|', '|', 0, 'D', 'L', 'S', '00', 'AREA PRIVATA', NULL, NULL, NULL, 100, 1, '2013-09-12 10:04:34', 1, 1, '1', '2013-09-12 10:05:18', 0, '', NULL, 0, 0, NULL),
(280, '00.99', 0, 7, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Client IP Address', NULL, NULL, NULL, 249, 1, '2013-04-11 16:08:34', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(281, '00.98', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pagina di Servizio', NULL, NULL, NULL, 250, 1, '2013-04-09 15:03:28', 1, 1, '1', '2013-04-09 15:03:50', 0, '', NULL, 0, 0, NULL),
(288, '89', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Gestione FAQ', NULL, NULL, NULL, 254, 1, '2013-09-12 10:51:19', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(289, '89.10', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'F.A.Q.', NULL, NULL, NULL, 254, 1, '2013-09-12 11:24:12', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(290, '89.20', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Categorie Faq', NULL, NULL, NULL, 255, 1, '2013-09-12 10:59:28', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(291, '89.30', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Sottocategorie Faq', NULL, NULL, NULL, 256, 1, '2013-09-12 11:00:19', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(292, '01.01', 0, 0, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'F.A.Q.', NULL, NULL, 'F.A.Q', 254, 1, '2013-09-12 11:31:36', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(299, '08', 1, 1, 0, '|96|97|98|', '|', 0, 'A', 'M', 'N', '00', 'Bacheca', NULL, NULL, 'Bacheca', 262, 1, '2013-09-26 13:22:02', 1, 1, '1', '2013-09-26 13:22:11', 0, '', NULL, 0, 0, NULL),
(300, '04', 0, 3, 0, '|96|', '|', 0, 'A', 'M', 'N', '00', 'Gestione Delegati', NULL, NULL, NULL, 263, 1, '2013-09-09 08:52:15', 1, 1, '1', '2013-09-09 08:52:36', 0, '', NULL, 0, 0, NULL),
(301, '05', 0, 2, 0, '|96|97|', '|', 0, 'A', 'M', 'N', '00', 'Scheda Impresa', NULL, NULL, NULL, 264, 1, '2013-09-24 09:55:14', 1, 1, '1', '2013-09-24 09:55:29', 0, '', NULL, 0, 0, NULL),
(302, '07', 0, 2, 0, '|96|97|', '|', 0, 'A', 'M', 'N', '00', 'Prodotti', NULL, NULL, NULL, 223, 1, '2013-09-09 08:51:27', 1, 1, '1', '2013-09-09 08:52:36', 0, '', NULL, 0, 0, NULL),
(303, '10', 0, 1, 0, '|96|97|98|', '|', 0, 'A', 'M', 'N', '00', 'Trattative', NULL, NULL, NULL, 265, 1, '2013-09-27 08:16:21', 1, 1, '1', '2013-09-27 08:16:29', 0, '', NULL, 0, 0, NULL),
(304, '06', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Ricerca', NULL, NULL, NULL, 266, 1, '2013-09-09 08:56:13', 1, 1, '1', '2013-09-09 08:56:17', 0, '', NULL, 0, 0, NULL),
(305, '09', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Preferiti', NULL, NULL, NULL, 267, 1, '2013-08-26 14:40:53', 1, 1, '1', '2013-08-26 14:40:59', 0, '', NULL, 0, 0, NULL),
(306, '00.07', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Conferma registrazione', NULL, NULL, NULL, 268, 1, '2013-09-05 12:28:52', 1, 1, '1', '2013-09-05 12:28:56', 0, '', NULL, 0, 0, NULL),
(307, '00.12', 0, 2, 0, '|96|97|', '|', 0, 'A', 'P', 'N', '00', 'Seleziona Impresa', NULL, NULL, NULL, 269, 1, '2013-09-09 08:57:53', 1, 1, '1', '2013-09-10 14:21:39', 0, '', NULL, 0, 0, NULL),
(310, '03', 0, 3, 0, '|96|', '|', 0, 'A', 'M', 'N', '00', 'Gestione Impresa', NULL, NULL, NULL, 272, 1, '2013-10-07 15:39:24', 1, 1, '1', '2013-10-07 15:39:28', 0, '', NULL, 0, 0, NULL),
(308, '00.13', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Login', NULL, NULL, NULL, 270, 1, '2013-09-10 14:10:05', 1, 1, '1', '2013-09-10 14:21:39', 0, '', NULL, 0, 0, NULL),
(309, '00.14', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Registrazione Smart Card', NULL, NULL, NULL, 271, 1, '2013-09-10 14:45:14', 1, 1, '1', '2013-09-10 14:45:22', 0, '', NULL, 0, 0, NULL),
(311, '00.15', 0, 1, 0, '|98|', '|', 0, 'A', 'P', 'N', '00', 'Trattativa extra bacheca', NULL, NULL, NULL, 273, 1, '2013-09-19 10:08:38', 1, 1, '1', '2013-09-24 09:53:49', 0, '', NULL, 0, 0, NULL),
(312, '00.16', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Indicizzazione Contenuti', NULL, NULL, NULL, 276, 1, '2013-10-23 08:21:55', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL),
(313, '00.17', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Indicizzazione Link', NULL, NULL, NULL, 275, 1, '2013-10-23 08:22:24', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL),
(314, '00.18', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Time Out Login', NULL, NULL, NULL, 277, 1, '2013-10-23 08:23:31', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL),
(315, '02', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Home', NULL, NULL, NULL, 274, 1, '2013-10-22 12:01:37', 1, 1, '1', '2013-10-22 12:01:41', 0, '', NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `SVIL_T_Menu_AL`
--

DROP TABLE IF EXISTS `SVIL_T_Menu_AL`;
CREATE TABLE IF NOT EXISTS `SVIL_T_Menu_AL` (
  `Id_AL` int(10) unsigned NOT NULL DEFAULT '0',
  `Lang_AL` char(2) NOT NULL DEFAULT '',
  `Titolo_AL` varchar(100) NOT NULL DEFAULT '',
  `Descrizione_AL` varchar(250) DEFAULT NULL,
  `Keywords_AL` varchar(250) DEFAULT NULL,
  `Popup_AL` mediumtext,
  `LinkEsterno_AL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id_AL`,`Lang_AL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `SVIL_T_Menu_AL`
--

INSERT INTO `SVIL_T_Menu_AL` (`Id_AL`, `Lang_AL`, `Titolo_AL`, `Descrizione_AL`, `Keywords_AL`, `Popup_AL`, `LinkEsterno_AL`) VALUES
(1, 'EN', 'Home', NULL, NULL, '', NULL),
(2, 'EN', 'Company Registration', NULL, NULL, '', NULL),
(3, 'EN', 'Comunicazioni', NULL, NULL, '', NULL),
(4, 'EN', 'Archivio News', NULL, NULL, '', NULL),
(5, 'EN', 'Eventi', NULL, NULL, '', NULL),
(6, 'EN', 'Invia ad un amico', NULL, NULL, '', NULL),
(7, 'EN', 'Pagina dinamica', NULL, NULL, '', NULL),
(8, 'EN', 'Mappa', NULL, NULL, '', NULL),
(9, 'EN', 'Modifica i tuoi dati', NULL, NULL, '', NULL),
(10, 'EN', 'RESERVED AREA', NULL, NULL, '', NULL),
(11, 'EN', 'Anagrafiche', NULL, NULL, '', NULL),
(12, 'EN', 'Editor', NULL, NULL, '', NULL),
(13, 'EN', 'Inserimento Editor', NULL, NULL, '', NULL),
(14, 'EN', 'Modifica Editor', NULL, NULL, '', NULL),
(15, 'EN', 'Redattori', NULL, NULL, '', NULL),
(16, 'EN', 'Inserimento Redattore', NULL, NULL, '', NULL),
(17, 'EN', 'Modifica Redattore', NULL, NULL, '', NULL),
(18, 'EN', 'Partner', NULL, NULL, '', NULL),
(19, 'EN', 'Inserimento Tipo Utente 6', NULL, NULL, '', NULL),
(20, 'EN', 'Modifica Tipo Utente 6', NULL, NULL, '', NULL),
(21, 'EN', 'Utenti', NULL, NULL, '', NULL),
(22, 'EN', 'Password Dimenticata', NULL, NULL, '', NULL),
(23, 'EN', 'Inserimento Utente', NULL, NULL, '', NULL),
(24, 'EN', 'Benvenuto', NULL, NULL, '', NULL),
(25, 'EN', 'Modifica Utente', NULL, NULL, '', NULL),
(26, 'EN', 'Ricerca', NULL, NULL, '', NULL),
(27, 'EN', 'Gruppi', NULL, NULL, '', NULL),
(28, 'EN', 'Gestione Gruppi', NULL, NULL, '', NULL),
(29, 'EN', 'Invio Newsletter', NULL, NULL, '', NULL),
(30, 'EN', 'Newsletter', NULL, NULL, '', NULL),
(31, 'EN', 'Newsletter News', NULL, NULL, '', NULL),
(32, 'EN', 'Template', NULL, NULL, '', NULL),
(33, 'EN', 'Email', NULL, NULL, '', NULL),
(34, 'EN', 'Argomenti', NULL, NULL, '', NULL),
(35, 'EN', 'Mappa Admin', NULL, NULL, '', NULL),
(37, 'EN', 'Eventi', NULL, NULL, '', NULL),
(38, 'EN', 'News', NULL, NULL, '', NULL),
(40, 'EN', 'Editoria', NULL, NULL, '', NULL),
(50, 'EN', 'Area CMS', NULL, NULL, '', NULL),
(51, 'EN', 'Amministrazione', NULL, NULL, '', NULL),
(52, 'EN', 'Menu', NULL, NULL, '', NULL),
(53, 'EN', 'Menu', NULL, NULL, '', NULL),
(54, 'EN', 'Menu', NULL, NULL, '', NULL),
(55, 'EN', 'Gestione T_Menu_AL', NULL, NULL, '', NULL),
(56, 'EN', 'Articoli', NULL, NULL, '', NULL),
(57, 'EN', 'Gestione T_Articoli_AL', NULL, NULL, '', NULL),
(58, 'EN', 'Templates', NULL, NULL, '', NULL),
(59, 'EN', 'Gestione T_Pagine_AL', NULL, NULL, '', NULL),
(60, 'EN', 'Gestione Box', NULL, NULL, '', NULL),
(61, 'EN', 'Gestione Articoli_X_Box', NULL, NULL, '', NULL),
(62, 'EN', 'Tipi Html', NULL, NULL, '', NULL),
(63, 'EN', 'Tipi articoli', NULL, NULL, '', NULL),
(64, 'EN', 'Sottocategorie', NULL, NULL, '', NULL),
(65, 'EN', 'Upload Immagini', NULL, NULL, '', NULL),
(66, 'EN', 'Upload Files', NULL, NULL, '', NULL),
(67, 'EN', 'Privacy', NULL, NULL, '', NULL),
(68, 'EN', 'Pubblicazione', NULL, NULL, '', NULL),
(69, 'EN', 'Inizializzazione lingue', NULL, NULL, '', NULL),
(70, 'EN', 'Attivazione Utenti', NULL, NULL, '', NULL),
(71, 'EN', 'Modifica Messaggistica CMS', NULL, NULL, '', NULL),
(82, 'EN', 'Categorie', NULL, NULL, '', NULL),
(85, 'EN', 'Completamento registrazione', NULL, NULL, NULL, NULL),
(86, 'EN', 'Completing registration', NULL, NULL, '', NULL),
(101, 'EN', 'Toolbar', NULL, NULL, '', NULL),
(231, 'EN', 'Gestione Categorie', NULL, NULL, '', NULL),
(243, 'EN', 'PRIVATE AREA', NULL, NULL, '', NULL),
(280, 'EN', '', NULL, NULL, NULL, NULL),
(281, 'EN', '', NULL, NULL, NULL, NULL),
(288, 'EN', '', NULL, NULL, NULL, NULL),
(289, 'EN', '', NULL, NULL, NULL, NULL),
(290, 'EN', '', NULL, NULL, NULL, NULL),
(291, 'EN', '', NULL, NULL, NULL, NULL),
(292, 'EN', 'F.A.Q.', NULL, NULL, '', NULL),
(299, 'EN', 'Conversations', NULL, NULL, '', NULL),
(300, 'EN', 'Delegates Management', NULL, NULL, '', NULL),
(301, 'EN', 'Card Company', NULL, NULL, '', NULL),
(302, 'EN', 'Products', NULL, NULL, '', NULL),
(303, 'EN', 'Negotiations', NULL, NULL, '', NULL),
(304, 'EN', 'Search', NULL, NULL, '', NULL),
(305, 'EN', 'Favorites', NULL, NULL, '', NULL),
(306, 'EN', 'Registration confirmation', NULL, NULL, '', NULL),
(307, 'EN', 'Select company', NULL, NULL, '', NULL),
(308, 'EN', '', NULL, NULL, NULL, NULL),
(309, 'EN', '', NULL, NULL, NULL, NULL),
(310, 'EN', 'Company management', NULL, NULL, '', NULL),
(311, 'EN', 'Negotiation extra bulletin board', NULL, NULL, '', NULL),
(300, 'FR', 'D�l�gu�s de gestion', NULL, NULL, '', NULL),
(301, 'FR', 'Card Company', NULL, NULL, '', NULL),
(302, 'FR', 'Produits', NULL, NULL, '', NULL),
(303, 'FR', 'N�gociations', NULL, NULL, '', NULL),
(304, 'FR', 'Sechercher', NULL, NULL, '', NULL),
(305, 'FR', 'Favoris', NULL, NULL, '', NULL),
(306, 'FR', 'Une confirmation d''inscription', NULL, NULL, '', NULL),
(307, 'FR', 'Choisissez entreprise', NULL, NULL, '', NULL),
(310, 'FR', 'Les soci�t�s de gestion', NULL, NULL, '', NULL),
(311, 'FR', 'N�gociation tableau d''affichage suppl�mentaire', NULL, NULL, '', NULL),
(308, 'FR', '', NULL, NULL, NULL, NULL),
(309, 'FR', '', NULL, NULL, NULL, NULL),
(299, 'FR', 'Tableau d''affichage', NULL, NULL, '', NULL),
(292, 'FR', 'F.A.Q.', NULL, NULL, '', NULL),
(312, 'EN', '', NULL, NULL, NULL, NULL),
(312, 'FR', '', NULL, NULL, NULL, NULL),
(313, 'FR', '', NULL, NULL, NULL, NULL),
(313, 'EN', '', NULL, NULL, NULL, NULL),
(314, 'EN', '', NULL, NULL, NULL, NULL),
(314, 'FR', '', NULL, NULL, NULL, NULL),
(315, 'FR', 'Home', NULL, NULL, NULL, NULL),
(315, 'EN', 'Home', NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `T_Menu`
--

DROP TABLE IF EXISTS `T_Menu`;
CREATE TABLE IF NOT EXISTS `T_Menu` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Id_Menu` varchar(50) NOT NULL DEFAULT '00',
  `Id_Sottomenu` tinyint(4) NOT NULL DEFAULT '0',
  `Id_Priorita` tinyint(4) NOT NULL DEFAULT '0',
  `Id_TipoUtente` tinyint(4) NOT NULL DEFAULT '0',
  `TipiUtente` varchar(100) NOT NULL DEFAULT '|',
  `Gruppi` varchar(250) NOT NULL DEFAULT '|',
  `MasterEditor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Stato_Menu` char(1) NOT NULL DEFAULT 'A',
  `Tipo` char(1) NOT NULL DEFAULT 'M',
  `Esploso` char(1) NOT NULL DEFAULT 'N',
  `Mtop` varchar(50) NOT NULL DEFAULT '00',
  `Titolo` varchar(100) NOT NULL,
  `Descrizione` varchar(250) DEFAULT NULL,
  `Keywords` varchar(250) DEFAULT NULL,
  `Popup` mediumtext,
  `Id_Pagina` int(10) unsigned DEFAULT NULL,
  `Id_Modifica` int(11) NOT NULL DEFAULT '0',
  `Data_Modifica` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Flag_Modifica` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Flag_Struttura` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `PubblicatoDa` varchar(20) NOT NULL DEFAULT '',
  `Data_Pubblicazione` datetime DEFAULT NULL,
  `Id_Articolo` int(11) NOT NULL DEFAULT '0',
  `LinkEsterno` varchar(100) DEFAULT '',
  `MenuTemplate` varchar(20) DEFAULT NULL,
  `Id_Ufficio` int(11) NOT NULL DEFAULT '0',
  `IPFilter` tinyint(1) NOT NULL DEFAULT '0',
  `Help_HTML` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_Menu` (`Id_Menu`,`Id_Sottomenu`,`Id_Priorita`,`Id_TipoUtente`,`Stato_Menu`,`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=316 ;

--
-- Dump dei dati per la tabella `T_Menu`
--

INSERT INTO `T_Menu` (`Id`, `Id_Menu`, `Id_Sottomenu`, `Id_Priorita`, `Id_TipoUtente`, `TipiUtente`, `Gruppi`, `MasterEditor`, `Stato_Menu`, `Tipo`, `Esploso`, `Mtop`, `Titolo`, `Descrizione`, `Keywords`, `Popup`, `Id_Pagina`, `Id_Modifica`, `Data_Modifica`, `Flag_Modifica`, `Flag_Struttura`, `PubblicatoDa`, `Data_Pubblicazione`, `Id_Articolo`, `LinkEsterno`, `MenuTemplate`, `Id_Ufficio`, `IPFilter`, `Help_HTML`) VALUES
(1, '00', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'HomePage', 'Home Page', 'Home Page', '', 0, 1, '2013-10-22 11:30:06', 1, 1, '1', '2013-10-22 11:30:17', 0, '709417', '', 0, 0, NULL),
(2, '00.01', 0, 0, 99, '|', '|', 0, 'A', 'P', '', '00', 'Registrazione Azienda', 'Registrati', 'Registrati', '', 231, 1, '2013-07-01 12:21:51', 1, 1, '1', '2013-07-01 12:22:06', 0, '', NULL, 0, 0, NULL),
(3, '00.11', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Comunicazioni', 'Comunicazioni', 'Comunicazioni', NULL, 105, 1, '2007-12-19 19:16:49', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(4, '00.04', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Archivio News', 'Archivio News', 'Archivio News', '', 57, 1, '2006-01-26 08:59:51', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(5, '00.05', 1, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Eventi', 'Eventi', 'Eventi', '', 60, 1, '2006-05-15 10:47:01', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(6, '00.06', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Invia ad un amico', 'Invia ad un amico', 'Invia ad un amico', '', 58, 1, '2006-03-01 10:12:08', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(7, '00.08', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pagina dinamica', 'Pagina dinamica', 'Pagina dinamica', '', 47, 1, '2005-10-11 14:11:40', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(8, '00.02', 0, 0, 0, '|', '|', 0, 'A', 'P', '', '02', 'Site map', 'Site map', 'Site map', '', 8, 1, '2012-06-27 10:19:38', 1, 1, '1', '2012-06-27 10:21:21', 0, '', NULL, 0, 0, NULL),
(9, '00.09', 0, 2, 0, '|', '|', 0, 'A', 'P', '', '00', 'Modifica i tuoi dati', 'Modifica i tuoi dati', 'Modifica i tuoi dati', '', 9, 1, '2013-04-11 16:08:00', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(10, '80', 0, 7, 0, '|', '|', 0, 'A', 'L', '', '00', 'AREA RISERVATA', 'Area Riservata', 'Area Riservata', '', 100, 1, '2013-06-10 16:40:47', 1, 1, '1', '2013-06-10 16:41:16', 0, '', '', 0, 0, NULL),
(11, '80', 1, 10, 0, '|', '|', 0, 'A', 'M', 'S', '00', 'Anagrafiche', 'Anagrafiche', 'Anagrafiche', '', 11, 1, '2013-08-02 09:49:09', 1, 1, '1', '2013-08-02 09:49:25', 0, '', '', 0, 0, NULL),
(12, '80.10', 0, 7, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Editor', 'Editor', 'Editor', '', 11, 1, '2013-04-11 16:13:15', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(13, '80.15', 0, 10, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Editor', 'Inserimento Editor', 'Inserimento Editor', '', 21, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(14, '80.17', 0, 10, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Editor', 'Modifica Editor', 'Modifica Editor', '', 22, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(15, '80.30', 0, 9, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Redattori', 'Redattori', 'Redattori', '', 78, 1, '2008-03-12 15:23:46', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(16, '80.35', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Redattore', 'Inserimento Redattore', 'Inserimento Redattore', '', 70, 1, '2006-08-07 16:40:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(17, '80.37', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Redattore', 'Modifica Redattore', 'Modifica Redattore', '', 71, 1, '2006-08-07 17:57:13', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(18, '80.40', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Partner', 'Partner', 'Partner', '', 79, 1, '2006-08-08 09:47:42', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(19, '80.45', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Inserimento Tipo Utente 6', 'Inserimento Tipo Utente 6', 'Inserimento Tipo Utente 6', '', 72, 1, '2004-03-08 14:07:28', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(20, '80.47', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Tipo Utente 6', 'Modifica Tipo Utente 6', 'Modifica Tipo Utente 6', '', 73, 1, '2004-11-17 16:47:11', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(21, '80.90', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Utenti', 'Utenti', 'Utenti', '', 12, 1, '2008-03-12 15:24:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(22, '80.91', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Password Dimenticata', 'Password Dimenticata', 'Password Dimenticata', '', 15, 1, '2006-04-28 13:52:30', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(23, '80.93', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Inserimento Utente', 'Inserimento Utente', 'Inserimento Utente', '', 20, 1, '2006-04-28 11:27:30', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(24, '80.94', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Benvenuto', 'Benvenuto', 'Benvenuto', '', 14, 1, '2006-04-28 12:34:48', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(25, '80.95', 0, 9, 1, '|', '|', 0, 'A', 'P', '', '00', 'Modifica Utente', 'Modifica Utente', 'Modifica Utente', '', 23, 1, '2004-11-17 18:17:50', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(26, '00.10', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Ricerca', 'Ricerca', 'Ricerca', '', 59, 1, '2006-02-07 14:01:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(27, '81', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Gruppi', 'Gruppi', 'Gruppi', NULL, 27, 1, '2010-12-15 19:51:26', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(28, '81.10', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione Gruppi', 'Gestione Gruppi', 'Gestione Gruppi', NULL, 28, 1, '2010-12-15 19:52:02', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(29, '84.40', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Invio Newsletter', 'Invio Newsletter', 'Invio Newsletter', NULL, 109, 1, '2008-12-16 10:35:13', 1, 3, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(30, '84', 1, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Newsletter', 'Newsletter', 'Newsletter', '', 51, 1, '2010-04-28 10:43:26', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(31, '84.05', 1, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Newsletter News', 'Newsletter News', 'Newsletter News', '', 56, 1, '2010-04-28 10:43:35', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(32, '84.10', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Template', 'Template', 'Template', '', 53, 1, '2008-12-16 10:34:35', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(33, '84.20', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Email', 'Email', 'Email', '', 52, 1, '2008-12-16 10:34:47', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(34, '84.30', 0, 8, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Argomenti', 'Argomenti', 'Argomenti', '', 54, 1, '2008-12-16 10:35:02', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(35, '92', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Mappa Admin', 'Mappa Admin', 'Mappa Admin', '', 38, 1, '2013-06-12 16:34:16', 1, 1, '1', '2013-06-12 16:34:20', 0, '', '', 0, 0, NULL),
(37, '94', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Eventi', 'Eventi', 'Eventi', '', 61, 1, '2012-06-05 15:08:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(38, '95', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'News', 'News', 'News', '', 50, 1, '2006-01-23 18:19:25', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(40, '97', 0, 9, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Editoria', 'Editoria', 'Editoria', '', 138, 1, '2010-04-27 16:51:40', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(50, '90', 0, 8, 0, '|', '|', 0, 'A', 'L', '', '00', 'Area CMS', 'Area CMS', 'Area CMS', '', 100, 1, '2006-08-07 19:11:18', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(51, '90.01', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Amministrazione', 'Amministrazione', 'Amministrazione', '', 4, 1, '2006-03-09 11:28:06', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(52, '91', 1, 10, 0, '|', '|', 0, 'A', 'M', 'S', '00', 'Menu', 'Menu', 'Menu', '', 62, 1, '2013-01-16 14:35:19', 1, 1, '1', '2013-01-16 14:35:54', 0, '', NULL, 0, 0, NULL),
(53, '91', 1, 8, 0, '|2|3|', '|', 0, 'A', 'M', 'S', '00', 'Menu', 'Menu', 'Menu', '', 38, 1, '2006-08-07 19:06:34', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(54, '91', 1, 8, 1, '|1|2|3|', '|', 0, 'A', 'P', 'S', '00', 'Menu', 'Menu', 'Menu', '', 62, 1, '2006-08-07 19:07:10', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(55, '91.01', 0, 8, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Menu_AL', 'Gestione T_Menu_AL', 'Gestione T_Menu_AL', '', 67, 1, '2006-08-07 19:12:44', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(56, '91.05', 0, 8, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Articoli', 'Articoli', 'Articoli', '', 64, 1, '2013-06-12 16:21:29', 1, 1, '1', '2013-06-12 16:21:34', 0, '', '', 0, 0, NULL),
(57, '91.05.01', 0, 8, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Articoli_AL', 'Gestione T_Articoli_AL', 'Gestione T_Articoli_AL', '', 66, 1, '2006-08-07 19:13:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(58, '91.10', 0, 10, 1, '|1|', '|', 0, 'A', 'M', 'N', '00', 'Templates', 'Templates', 'Templates', '', 33, 1, '2004-10-13 09:26:08', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(59, '91.10.01', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione T_Pagine_AL', 'Gestione T_Pagine_AL', 'Gestione T_Pagine_AL', '', 44, 1, '2005-09-06 16:39:24', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(60, '91.15', 0, 8, 0, '|', '|', 0, 'A', 'P', '', '00', 'Gestione Box', 'Gestione Box', 'Gestione Box', '', 63, 1, '2006-08-07 19:12:20', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(61, '91.16', 0, 8, 0, '|', '|', 0, 'A', 'P', '', '00', 'Gestione Articoli_X_Box', 'Gestione Articoli_X_Box', 'Gestione Articoli_X_Box', '', 65, 1, '2006-08-07 19:12:04', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(62, '91.20', 0, 10, 1, '|', '|', 0, 'A', 'M', 'N', '00', 'Tipi Html', 'Tipi Html', 'Tipi Html', '', 34, 1, '2004-11-15 17:49:20', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(63, '91.32', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Tipi articoli', 'Tipi articoli', 'Tipi articoli', '', 46, 1, '2005-10-12 16:35:55', 1, 1, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(64, '91.35', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Sottocategorie', 'Sottocategorie', 'Sottocategorie', '', 40, 1, '2007-01-23 12:33:09', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(65, '91.40', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Upload Immagini', 'Upload Immagini', 'Upload Immagini', '', 35, 1, '2007-07-24 18:13:54', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(66, '91.50', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'Upload Files', 'Upload Files', 'Upload Files', '', 36, 1, '2007-01-23 12:32:21', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(67, '91.60', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Privacy', 'Privacy', 'Privacy', '', 13, 1, '2013-04-23 15:07:10', 1, 1, '1', '2013-04-23 15:07:24', 0, '', NULL, 0, 0, NULL),
(68, '91.08', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pubblicazione', 'Pubblicazione', 'Pubblicazione', '', 68, 1, '2006-09-20 18:53:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(69, '91.07', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Inizializzazione lingue', 'Inizializzazione lingue', 'Inizializzazione lingue', '', 55, 1, '2006-09-20 18:53:19', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(70, '80.07', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Attivazione Utenti', 'Attivazione Utenti', 'Attivazione Utenti', NULL, 101, 1, '2007-12-14 12:53:08', 1, 3, '1', '2012-06-06 19:22:13', 0, '', NULL, 0, 0, NULL),
(71, '85.10', 0, 10, 0, '|', '|', 0, 'D', 'P', 'N', '00', 'Modifica Messaggistica CMS', 'Modifica Messaggistica CMS', 'Modifica Messaggistica CMS', NULL, 104, 1, '2013-08-26 16:39:34', 1, 3, '1', '2013-08-26 16:39:54', 0, '', NULL, 0, 0, NULL),
(82, '82', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Categorie', 'Categorie', 'Categorie', NULL, 95, 1, '2013-07-24 10:47:16', 1, 1, '1', '2013-07-24 10:47:24', 0, '', NULL, 0, 0, NULL),
(85, '10', 0, 1, 1, '|', '|', 0, 'A', 'P', '', '00', 'Completamento registrazione', 'Completamento registrazione', 'Completamento registrazione', '', 85, 1, '2013-09-05 09:47:21', 1, 1, '1', '2013-09-05 09:47:35', 0, '', NULL, 0, 0, NULL),
(86, '90.51', 0, 2, 1, '|', '|', 0, 'D', 'P', '', '00', 'Completamento registrazione', 'Completamento registrazione', 'Completamento registrazione', NULL, 0, 1, '2013-09-05 12:16:50', 1, 1, '1', '2013-09-05 12:17:08', 0, 'setUtente', NULL, 0, 0, NULL),
(101, '01', 0, 0, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Toolbar', 'Toolbar', 'Toolbar', '', 3, 1, '2006-05-31 13:23:33', 1, 1, '1', '2012-06-06 19:22:13', 0, '', '', 0, 0, NULL),
(231, '82.10', 0, 9, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Gestione Categorie', 'Gestione Categorie', 'Gestione Categorie', NULL, 220, 1, '2013-01-15 14:06:45', 1, 3, '1', '2013-01-15 14:36:09', 0, '', NULL, 0, 0, NULL),
(256, '01.01.01', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Mission', NULL, NULL, NULL, 1, 1, '2013-04-23 12:08:25', 1, 1, '1', '2013-04-23 12:08:29', 0, '', NULL, 0, 0, NULL),
(257, '01.01.02', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Consorzio', NULL, NULL, NULL, 1, 1, '2013-04-23 12:25:47', 1, 1, '1', '2013-04-23 12:25:50', 0, '', NULL, 0, 0, NULL),
(280, '00.99', 0, 7, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Client IP Address', NULL, NULL, NULL, 249, 1, '2013-04-11 16:08:34', 1, 1, '1', '2013-04-11 16:18:10', 0, '', NULL, 0, 0, NULL),
(281, '00.98', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Pagina di Servizio', NULL, NULL, NULL, 250, 1, '2013-04-09 15:03:28', 1, 1, '1', '2013-04-09 15:03:50', 0, '', NULL, 0, 0, NULL),
(288, '89', 1, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Gestione FAQ', NULL, NULL, NULL, 254, 1, '2013-09-12 10:51:19', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(289, '89.10', 0, 10, 0, '|', '|', 0, 'D', 'M', 'N', '00', 'F.A.Q.', NULL, NULL, NULL, 254, 1, '2013-09-12 11:24:12', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(290, '89.20', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Categorie Faq', NULL, NULL, NULL, 255, 1, '2013-09-12 10:59:28', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(291, '89.30', 0, 10, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'Sottocategorie Faq', NULL, NULL, NULL, 256, 1, '2013-09-12 11:00:19', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(292, '01.01', 0, 0, 0, '|', '|', 0, 'A', 'M', 'N', '00', 'F.A.Q.', NULL, NULL, 'F.A.Q', 254, 1, '2013-09-12 11:31:36', 1, 1, '1', '2013-09-16 11:57:49', 0, '', NULL, 0, 0, NULL),
(299, '08', 1, 1, 0, '|96|97|98|', '|', 0, 'A', 'M', 'N', '00', 'Bacheca', NULL, NULL, 'Bacheca', 262, 1, '2013-09-26 13:22:02', 1, 1, '1', '2013-09-26 13:22:11', 0, '', NULL, 0, 0, NULL),
(300, '04', 0, 3, 0, '|96|', '|', 0, 'A', 'M', 'N', '00', 'Gestione Delegati', NULL, NULL, NULL, 263, 1, '2013-09-09 08:52:15', 1, 1, '1', '2013-09-09 08:52:36', 0, '', NULL, 0, 0, NULL),
(301, '05', 0, 2, 0, '|96|97|', '|', 0, 'A', 'M', 'N', '00', 'Scheda Impresa', NULL, NULL, NULL, 264, 1, '2013-09-24 09:55:14', 1, 1, '1', '2013-09-24 09:55:29', 0, '', NULL, 0, 0, NULL),
(302, '07', 0, 2, 0, '|96|97|', '|', 0, 'A', 'M', 'N', '00', 'Prodotti', NULL, NULL, NULL, 223, 1, '2013-09-09 08:51:27', 1, 1, '1', '2013-09-09 08:52:36', 0, '', NULL, 0, 0, NULL),
(303, '10', 0, 1, 0, '|96|97|98|', '|', 0, 'A', 'M', 'N', '00', 'Trattative', NULL, NULL, NULL, 265, 1, '2013-09-27 08:16:21', 1, 1, '1', '2013-09-27 08:16:29', 0, '', NULL, 0, 0, NULL),
(304, '06', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Ricerca', NULL, NULL, NULL, 266, 1, '2013-09-09 08:56:13', 1, 1, '1', '2013-09-09 08:56:17', 0, '', NULL, 0, 0, NULL),
(305, '09', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Preferiti', NULL, NULL, NULL, 267, 1, '2013-08-26 14:40:53', 1, 1, '1', '2013-08-26 14:40:59', 0, '', NULL, 0, 0, NULL),
(306, '00.07', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Conferma registrazione', NULL, NULL, NULL, 268, 1, '2013-09-05 12:28:52', 1, 1, '1', '2013-09-05 12:28:56', 0, '', NULL, 0, 0, NULL),
(307, '00.12', 0, 2, 0, '|96|97|', '|', 0, 'A', 'P', 'N', '00', 'Seleziona Impresa', NULL, NULL, NULL, 269, 1, '2013-09-09 08:57:53', 1, 1, '1', '2013-09-10 14:21:39', 0, '', NULL, 0, 0, NULL),
(243, '03', 1, 1, 0, '|', '|', 0, 'D', 'L', 'S', '00', 'AREA PRIVATA', NULL, NULL, NULL, 100, 1, '2013-09-12 10:04:34', 1, 1, '1', '2013-09-12 10:05:18', 0, '', NULL, 0, 0, NULL),
(308, '00.13', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Login', NULL, NULL, NULL, 270, 1, '2013-09-10 14:10:05', 1, 1, '1', '2013-09-10 14:21:39', 0, '', NULL, 0, 0, NULL),
(309, '00.14', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Registrazione Smart Card', NULL, NULL, NULL, 271, 1, '2013-09-10 14:45:14', 1, 1, '1', '2013-09-10 14:45:22', 0, '', NULL, 0, 0, NULL),
(310, '03', 0, 3, 0, '|96|', '|', 0, 'A', 'M', 'N', '00', 'Gestione Impresa', NULL, NULL, NULL, 272, 1, '2013-10-07 15:39:24', 1, 1, '1', '2013-10-07 15:39:28', 0, '', NULL, 0, 0, NULL),
(311, '00.15', 0, 1, 0, '|98|', '|', 0, 'A', 'P', 'N', '00', 'Trattativa extra bacheca', NULL, NULL, NULL, 273, 1, '2013-09-19 10:08:38', 1, 1, '1', '2013-09-24 09:53:49', 0, '', NULL, 0, 0, NULL),
(315, '02', 0, 1, 0, '|98|', '|', 0, 'A', 'M', 'N', '00', 'Home', NULL, NULL, NULL, 274, 1, '2013-10-22 12:01:37', 1, 1, '1', '2013-10-22 12:01:41', 0, '', NULL, 0, 0, NULL),
(312, '00.16', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Indicizzazione Contenuti', NULL, NULL, NULL, 276, 1, '2013-10-23 08:21:55', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL),
(313, '00.17', 0, 10, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Indicizzazione Link', NULL, NULL, NULL, 275, 1, '2013-10-23 08:22:24', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL),
(314, '00.18', 0, 0, 0, '|', '|', 0, 'A', 'P', 'N', '00', 'Time Out Login', NULL, NULL, NULL, 277, 1, '2013-10-23 08:23:31', 1, 1, '1', '2013-10-23 08:23:34', 0, '', NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `T_Menu_AL`
--

DROP TABLE IF EXISTS `T_Menu_AL`;
CREATE TABLE IF NOT EXISTS `T_Menu_AL` (
  `Id_AL` int(10) unsigned NOT NULL DEFAULT '0',
  `Lang_AL` char(2) NOT NULL DEFAULT '',
  `Titolo_AL` varchar(100) NOT NULL DEFAULT '',
  `Descrizione_AL` varchar(250) DEFAULT NULL,
  `Keywords_AL` varchar(250) DEFAULT NULL,
  `Popup_AL` mediumtext,
  `LinkEsterno_AL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id_AL`,`Lang_AL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `T_Menu_AL`
--

INSERT INTO `T_Menu_AL` (`Id_AL`, `Lang_AL`, `Titolo_AL`, `Descrizione_AL`, `Keywords_AL`, `Popup_AL`, `LinkEsterno_AL`) VALUES
(1, 'EN', 'Home', NULL, NULL, '', NULL),
(2, 'EN', 'Company Registration', NULL, NULL, '', NULL),
(3, 'EN', 'Comunicazioni', NULL, NULL, '', NULL),
(4, 'EN', 'Archivio News', NULL, NULL, '', NULL),
(5, 'EN', 'Eventi', NULL, NULL, '', NULL),
(6, 'EN', 'Invia ad un amico', NULL, NULL, '', NULL),
(7, 'EN', 'Pagina dinamica', NULL, NULL, '', NULL),
(8, 'EN', 'Mappa', NULL, NULL, '', NULL),
(9, 'EN', 'Modifica i tuoi dati', NULL, NULL, '', NULL),
(10, 'EN', 'Area Riservata', NULL, NULL, '', NULL),
(11, 'EN', 'Anagrafiche', NULL, NULL, '', NULL),
(12, 'EN', 'Editor', NULL, NULL, '', NULL),
(13, 'EN', 'Inserimento Editor', NULL, NULL, '', NULL),
(14, 'EN', 'Modifica Editor', NULL, NULL, '', NULL),
(15, 'EN', 'Redattori', NULL, NULL, '', NULL),
(16, 'EN', 'Inserimento Redattore', NULL, NULL, '', NULL),
(17, 'EN', 'Modifica Redattore', NULL, NULL, '', NULL),
(18, 'EN', 'Partner', NULL, NULL, '', NULL),
(19, 'EN', 'Inserimento Tipo Utente 6', NULL, NULL, '', NULL),
(20, 'EN', 'Modifica Tipo Utente 6', NULL, NULL, '', NULL),
(21, 'EN', 'Utenti', NULL, NULL, '', NULL),
(22, 'EN', 'Password Dimenticata', NULL, NULL, '', NULL),
(23, 'EN', 'Inserimento Utente', NULL, NULL, '', NULL),
(24, 'EN', 'Benvenuto', NULL, NULL, '', NULL),
(25, 'EN', 'Modifica Utente', NULL, NULL, '', NULL),
(26, 'EN', 'Ricerca', NULL, NULL, '', NULL),
(27, 'EN', 'Gruppi', NULL, NULL, '', NULL),
(28, 'EN', 'Gestione Gruppi', NULL, NULL, '', NULL),
(29, 'EN', 'Invio Newsletter', NULL, NULL, '', NULL),
(30, 'EN', 'Newsletter', NULL, NULL, '', NULL),
(31, 'EN', 'Newsletter News', NULL, NULL, '', NULL),
(32, 'EN', 'Template', NULL, NULL, '', NULL),
(33, 'EN', 'Email', NULL, NULL, '', NULL),
(34, 'EN', 'Argomenti', NULL, NULL, '', NULL),
(35, 'EN', 'Mappa Admin', NULL, NULL, '', NULL),
(37, 'EN', 'Eventi', NULL, NULL, '', NULL),
(38, 'EN', 'News', NULL, NULL, '', NULL),
(39, 'EN', 'Banner', NULL, NULL, '', NULL),
(40, 'EN', 'Editoria', NULL, NULL, '', NULL),
(50, 'EN', 'Area CMS', NULL, NULL, '', NULL),
(51, 'EN', 'Amministrazione', NULL, NULL, '', NULL),
(52, 'EN', 'Menu', NULL, NULL, '', NULL),
(53, 'EN', 'Menu', NULL, NULL, '', NULL),
(54, 'EN', 'Menu', NULL, NULL, '', NULL),
(55, 'EN', 'Gestione T_Menu_AL', NULL, NULL, '', NULL),
(56, 'EN', 'Articoli', NULL, NULL, '', NULL),
(57, 'EN', 'Gestione T_Articoli_AL', NULL, NULL, '', NULL),
(58, 'EN', 'Templates', NULL, NULL, '', NULL),
(59, 'EN', 'Gestione T_Pagine_AL', NULL, NULL, '', NULL),
(60, 'EN', 'Gestione Box', NULL, NULL, '', NULL),
(61, 'EN', 'Gestione Articoli_X_Box', NULL, NULL, '', NULL),
(62, 'EN', 'Tipi Html', NULL, NULL, '', NULL),
(63, 'EN', 'Tipi articoli', NULL, NULL, '', NULL),
(64, 'EN', 'Sottocategorie', NULL, NULL, '', NULL),
(65, 'EN', 'Upload Immagini', NULL, NULL, '', NULL),
(66, 'EN', 'Upload Files', NULL, NULL, '', NULL),
(67, 'EN', 'Privacy', NULL, NULL, '', NULL),
(68, 'EN', 'Pubblicazione', NULL, NULL, '', NULL),
(69, 'EN', 'Inizializzazione lingue', NULL, NULL, '', NULL),
(70, 'EN', 'Attivazione Utenti', NULL, NULL, '', NULL),
(71, 'EN', 'Modifica Messaggistica CMS', NULL, NULL, '', NULL),
(72, 'EN', 'Messaggistica CMS', NULL, NULL, '', NULL),
(82, 'EN', 'Categorie', NULL, NULL, '', NULL),
(85, 'EN', 'Completamento registrazione', NULL, NULL, NULL, NULL),
(101, 'EN', 'Toolbar', NULL, NULL, '', NULL),
(230, 'EN', '> CONTACTS', NULL, NULL, '', NULL),
(231, 'EN', 'Gestione Categorie', NULL, NULL, '', NULL),
(232, 'EN', '> COMMITTEES', NULL, NULL, '', NULL),
(233, 'EN', '> ABOUT US', NULL, NULL, '', NULL),
(241, 'EN', '', NULL, NULL, NULL, NULL),
(243, 'EN', 'PRIVATE AREA', NULL, NULL, '', NULL),
(245, 'EN', 'Products', NULL, NULL, '', NULL),
(252, 'EN', '', NULL, NULL, NULL, NULL),
(253, 'EN', '', NULL, NULL, NULL, NULL),
(254, 'EN', '', NULL, NULL, NULL, NULL),
(255, 'EN', '', NULL, NULL, NULL, NULL),
(256, 'EN', 'Mission', NULL, NULL, '', NULL),
(257, 'EN', 'Consortium', NULL, NULL, '', NULL),
(258, 'EN', 'Enter Products', NULL, NULL, '', NULL),
(259, 'EN', 'Browse the Catalogue', NULL, NULL, '', NULL),
(260, 'EN', 'Sponsor the project', NULL, NULL, '', NULL),
(261, 'EN', 'Regulation and requirements', NULL, NULL, '', NULL),
(262, 'EN', 'Credits', NULL, NULL, '', NULL),
(263, 'EN', '', NULL, NULL, NULL, NULL),
(264, 'EN', 'Record Menu', NULL, NULL, NULL, NULL),
(265, 'EN', '', NULL, NULL, NULL, NULL),
(266, 'EN', '', NULL, NULL, NULL, NULL),
(267, 'EN', '', NULL, NULL, NULL, NULL),
(268, 'EN', '', NULL, NULL, NULL, NULL),
(269, 'EN', '', NULL, NULL, NULL, NULL),
(273, 'EN', '', NULL, NULL, NULL, NULL),
(274, 'EN', '', NULL, NULL, NULL, NULL),
(275, 'EN', '', NULL, NULL, NULL, NULL),
(276, 'EN', '', NULL, NULL, NULL, NULL),
(277, 'EN', '', NULL, NULL, NULL, NULL),
(278, 'EN', '', NULL, NULL, NULL, NULL),
(279, 'EN', '', NULL, NULL, NULL, NULL),
(280, 'EN', '', NULL, NULL, NULL, NULL),
(281, 'EN', '', NULL, NULL, NULL, NULL),
(282, 'EN', '', NULL, NULL, NULL, NULL),
(285, 'EN', '', NULL, NULL, NULL, NULL),
(286, 'EN', '', NULL, NULL, NULL, NULL),
(288, 'EN', '', NULL, NULL, NULL, NULL),
(289, 'EN', '', NULL, NULL, NULL, NULL),
(290, 'EN', '', NULL, NULL, NULL, NULL),
(291, 'EN', '', NULL, NULL, NULL, NULL),
(292, 'EN', 'F.A.Q.', NULL, NULL, '', NULL),
(299, 'EN', 'Conversations', NULL, NULL, '', NULL),
(300, 'EN', 'Delegates Management', NULL, NULL, '', NULL),
(301, 'EN', 'Card Company', NULL, NULL, '', NULL),
(302, 'EN', 'Products', NULL, NULL, '', NULL),
(303, 'EN', 'Negotiations', NULL, NULL, '', NULL),
(304, 'EN', 'Search', NULL, NULL, '', NULL),
(305, 'EN', 'Favorites', NULL, NULL, '', NULL),
(306, 'EN', 'Registration confirmation', NULL, NULL, '', NULL),
(307, 'EN', 'Select company', NULL, NULL, '', NULL),
(308, 'EN', '', NULL, NULL, NULL, NULL),
(309, 'EN', '', NULL, NULL, NULL, NULL),
(310, 'EN', 'Company management', NULL, NULL, '', NULL),
(311, 'EN', 'Negotiation extra bulletin board', NULL, NULL, '', NULL),
(307, 'FR', 'Choisissez entreprise', NULL, NULL, '', NULL),
(306, 'FR', 'Une confirmation d''inscription', NULL, NULL, '', NULL),
(305, 'FR', 'Favoris', NULL, NULL, '', NULL),
(304, 'FR', 'Sechercher', NULL, NULL, '', NULL),
(303, 'FR', 'N�gociations', NULL, NULL, '', NULL),
(302, 'FR', 'Produits', NULL, NULL, '', NULL),
(301, 'FR', 'Card Company', NULL, NULL, '', NULL),
(299, 'FR', 'Tableau d''affichage', NULL, NULL, '', NULL),
(300, 'FR', 'D�l�gu�s de gestion', NULL, NULL, '', NULL),
(310, 'FR', 'Les soci�t�s de gestion', NULL, NULL, '', NULL),
(311, 'FR', 'N�gociation tableau d''affichage suppl�mentaire', NULL, NULL, '', NULL),
(308, 'FR', '', NULL, NULL, NULL, NULL),
(309, 'FR', '', NULL, NULL, NULL, NULL),
(292, 'FR', 'F.A.Q.', NULL, NULL, '', NULL),
(312, 'EN', '', NULL, NULL, NULL, NULL),
(312, 'FR', '', NULL, NULL, NULL, NULL),
(313, 'EN', '', NULL, NULL, NULL, NULL),
(313, 'FR', '', NULL, NULL, NULL, NULL),
(314, 'EN', '', NULL, NULL, NULL, NULL),
(314, 'FR', '', NULL, NULL, NULL, NULL),
(315, 'EN', 'Home', NULL, NULL, '', NULL),
(315, 'FR', 'Home', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `T_Pagine`
--

DROP TABLE IF EXISTS `T_Pagine`;
CREATE TABLE IF NOT EXISTS `T_Pagine` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descrizione` varchar(50) DEFAULT NULL,
  `Link` varchar(100) DEFAULT NULL,
  `Flag_Pubblico` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=278 ;

--
-- Dump dei dati per la tabella `T_Pagine`
--

INSERT INTO `T_Pagine` (`Id`, `Descrizione`, `Link`, `Flag_Pubblico`) VALUES
(1, 'Homepage', 'tmpl=2', 0),
(2, 'Pagina interna', 'tmpl=2', 1),
(3, 'Pagina interna 3 colonne', 'tmpl=3', 1),
(4, 'Pagina interna login', 'tmpl=4', 0),
(5, 'Pagina interna con tab menu', 'tmpl=5', 1),
(6, 'Link esterno', 'Link esterno', 1),
(7, 'Sito esterno', 'tmpl=2&pagina=esterno', 0),
(8, 'Mappa del Sito', 'tmpl=1&pagina=mappa', 0),
(9, 'Modifica i tuoi dati', 'tmpl=4&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente={%utype%}&Id={%uid%}', 0),
(11, 'Gestione Anagrafiche', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc', 0),
(12, 'Gestione Utenti', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc&Id_TipoUtente=98', 0),
(13, 'Privacy', 'tmpl=2&pagina=elenchi&nome=T_Privacy&azione=UPD&Id=1', 0),
(14, 'Benvenuto', 'tmpl=2&pagina=registrazione', 0),
(15, 'Password', 'tmpl=2&pagina=password', 0),
(20, 'Registrazione Utente', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=98', 0),
(21, 'Inserimento Editor', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=2', 0),
(22, 'Modifica Editor', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=2', 0),
(23, 'Modifica Utente', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=98', 0),
(27, 'Mappa Gruppi', 'tmpl=4&pagina=mappa_gen&tab_nome=T_Gruppi', 0),
(28, 'Modifica Gruppi', 'tmpl=2&pagina=form&nome=T_Gruppi', 0),
(30, 'Gestione Men? e Pagine', 'tmpl=2&pagina=elenchi&nome=T_Menu&ordina=Id_Menu', 0),
(31, 'Gestione Box', 'tmpl=2&pagina=elenchi&nome=T_Box', 0),
(32, 'Gestione Articoli', 'tmpl=2&pagina=elenchi&nome=T_Articoli', 0),
(33, 'Gestione Template', 'tmpl=2&pagina=elenchi&nome=T_Pagine', 0),
(34, 'Gestione Tipi Html', 'tmpl=2&pagina=elenchi&nome=Tlk_TipiHtml', 0),
(35, 'Upload Immagini', 'tmpl=2&pagina=elenchi&nome=T_Images', 0),
(36, 'Upload Files', 'tmpl=2&pagina=elenchi&nome=T_Files', 0),
(37, 'Gestione Banners', 'tmpl=2&pagina=elenchi&nome=T_Banners', 0),
(38, 'Mappa Amministrazione', 'tmpl=4&pagina=mappa&tipomappa=admin', 0),
(40, 'Gestione Tipo Articolo 1', 'tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo1', 0),
(41, 'Gestione Articoli_X_Box_X_Menu', 'tmpl=2&pagina=elenchi&nome=TJ_Articoli_X_Box_X_Menu', 0),
(42, 'Gestione Articoli_AL', 'tmpl=2&pagina=elenchi&nome=T_Articoli_AL', 0),
(43, 'Gestione Menu_AL', 'tmpl=2&pagina=elenchi&nome=T_Menu_AL', 0),
(44, 'Gestione Pagine_AL', 'tmpl=2&pagina=elenchi&nome=T_Pagine_AL', 0),
(45, 'Gestione Sottocategorie', 'tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo2', 0),
(46, 'Gestione Tipo Articolo', 'tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo3', 0),
(47, 'Selezione articoli', 'tmpl=3&pagina=selezione', 0),
(50, 'Gestione News', 'tmpl=2&pagina=elenchi&nome=T_News', 0),
(51, 'Gestione NewsLetter', 'tmpl=2&pagina=elenchi&nome=T_NewsLetter', 0),
(52, 'Email Newsletter', 'tmpl=2&pagina=elenchi&nome=T_NewsLetter_Email', 0),
(53, 'Template NewsLetter', 'tmpl=2&pagina=form&nome=T_NewsLetter_Html&azione=UPD&Id=1', 0),
(54, 'Gestione Argomenti NewsLetter', 'tmpl=2&pagina=elenchi&nome=Tlk_NewsLetter_Argomenti', 0),
(55, 'Inizializzazione lingue', 'tmpl=2&pagina=inizializza_lingue', 0),
(56, 'Gestione Newsletter News', 'tmpl=2&pagina=form&nome=T_NewsLetter_News', 0),
(57, 'Archivio News', 'tmpl=3&pagina=archivio', 1),
(58, 'Invia ad un amico', 'tmpl=2&pagina=invia_amico', 0),
(59, 'Htdig', 'pagina=search', 0),
(60, 'Eventi', '/index.phtml?tmpl=2&pagina=eventi', 0),
(61, 'Gestione eventi', 'tmpl=2&pagina=elenchi&nome=T_Eventi', 0),
(62, 'Gestione SVIL Menu e Pagine', 'tmpl=2&pagina=elenchi&nome=SVIL_T_Menu&ordina=Id_Menu', 0),
(63, 'Gestione SVIL Box', 'tmpl=2&pagina=elenchi&nome=SVIL_T_Box', 0),
(64, 'Gestione SVIL Articoli', 'tmpl=2&pagina=elenchi&nome=SVIL_T_Articoli', 0),
(65, 'Gestione SVIL Articoli_X_Box_X_Menu', 'tmpl=2&pagina=elenchi&nome=SVIL_TJ_Articoli_X_Box_X_Menu', 0),
(66, 'Gestione SVIL Articoli_AL', 'tmpl=2&pagina=elenchi&nome=SVIL_T_Articoli_AL', 0),
(67, 'Gestione SVIL Menu_AL', 'tmpl=2&pagina=elenchi&nome=SVIL_T_Menu_AL', 0),
(68, 'Pubblicazione', '/index.phtml?tmpl=2&pagina=pubblica', 0),
(70, 'Inserimento Redattore', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=3', 0),
(71, 'Modifica Redattore', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=3', 0),
(72, 'Inserimento Partner', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=6', 0),
(73, 'Modifica Partner', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=6', 0),
(74, 'Inserimento TipoUtente 8', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=8', 0),
(75, 'Modifica TipoUtente 8', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=8', 0),
(76, 'Inserimento TipoUtente 10', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=INS&Id_TipoUtente=10', 0),
(77, 'Modifica TipoUtente 10', 'tmpl=2&pagina=form&nome=T_Anagrafica&azione=UPD&Id_TipoUtente=10', 0),
(78, 'Gestione Redattore', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc&Id_TipoUtente=3', 0),
(79, 'Gestione Partner', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc&Id_TipoUtente=6', 0),
(80, 'Gestione Tipo Utente 8', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc&Id_TipoUtente=8', 0),
(81, 'Gestione Tipo Utente 10', 'tmpl=2&pagina=elenchi&nome=T_Anagrafica&ordina=Id%20Desc&Id_TipoUtente=10', 0),
(85, 'Anagrafica Dati aggiuntivi - Inserimento', 'tmpl=2&pagina=form&nome=T_Ext_Anagrafica&azione=INS&Id_TipoUtente=97', 0),
(95, 'Mappa Categorie', 'tmpl=4&pagina=mappa_gen&tab_nome=EXPO_Tlk_Categorie', 0),
(96, 'Gestione Applicazioni', 'tmpl=2&pagina=elenchi&nome=T_Applicazioni', 0),
(97, 'Gestione Applicazioni X Utente', 'tmpl=2&pagina=elenchi&nome=Tj_Applicazioni_X_Utente', 0),
(98, 'Upload Documenti', 'tmpl=2&pagina=elenchi&nome=T_Documenti', 0),
(100, 'Label', 'Label', 1),
(101, 'Attivazione Utente', 'tmpl=2&pagina=attiva', 0),
(103, 'Gestione Mail Generiche', 'tmpl=2&pagina=elenchi&nome=T_MailGeneriche', 0),
(104, 'Form Mail Generiche', 'tmpl=2&pagina=form&nome=T_MailGeneriche', 0),
(105, 'Pagine Messaggi Standard', 'tmpl=3&pagina=standard', 0),
(109, 'Invio Neswletter', 'tmpl=2&pagina=invio_newsletter', 0),
(110, 'Importa Nominativi da file', 'tmpl=2&pagina=importa_mail', 0),
(142, 'Intranet Documenti', 'tmpl=2&pagina=form&nome=INTRANET_T_Documenti', 0),
(143, 'Intranet: Gestione documentale', 'tmpl=2&pagina=elenchi&nome=INTRANET_T_Documenti', 1),
(202, 'GREEN Risultati ricerca', 'tmpl=2&pagina=GREEN_risultati&nh=1', 0),
(203, 'SALE - Gestione Sale', 'tmpl=4&pagina=elenchi&nome=SALE_T_Sale', 0),
(217, 'SALE - Gestione Agenzie', 'tmpl=4&pagina=elenchi&nome=SALE_T_Agenzie', 0),
(218, 'SALE_Det_Struttura', 'tmpl=2&pagina=SALE_Det_Struttura', 0),
(219, 'Gestione Foto Struttura', 'tmpl=4&pagina=multifiles', 0),
(220, 'Modifica Categorie', 'tmpl=2&pagina=form&nome=EXPO_Tlk_Categorie', 0),
(223, 'Gestione Prodotti', 'tmpl=2&pagina=elenchi&nome=EXPO_T_Prodotti', 0),
(225, 'Admin - Modifica Dati estesi', 'tmpl=2&pagina=form&nome=T_Ext_Anagrafica&azione=UPD', 0),
(226, 'Modifica Prod Tab 0', 'tmpl=2&pagina=form&nome=T_Prodotti&azione=UPD&tab=0&Id={%id%}&nh=1', 0),
(240, 'Send Mail', 'tmpl=1&pagina=SIEXPO_SendMail&nh=0&idMail={%idMail%}', 0),
(244, 'Messaggi Help', 'tmpl=2&pagina=SIEXPO_MsgHelp', 0),
(249, 'Pagina IP Client', 'tmpl=2&pagina=SIEXPO_ClientIPAddress', 0),
(250, 'Pagina di servizio', 'tmpl=2&pagina=SIEXPO_Servizio', 0),
(254, 'Gestione Faq', 'tmpl=6&pagina=elenchi&nome=FAQ_T_Faq', 1),
(255, 'Gestione Categorie Faq', 'tmpl=2&pagina=elenchi&nome=FAQ_Tlk_Categorie_Faq', 0),
(256, 'Gestione Sottocategorie Faq', 'tmpl=2&pagina=elenchi&nome=FAQ_Tlk_Sottocategorie_Faq', 0),
(262, 'Gestione Bacheca', 'tmpl=2&pagina=elenchi&nome=EXPO_T_Bacheca&ordina=Id', 1),
(263, 'Gestione Delegati', 'tmpl=2&pagina=elenchi&nome=EXPO_TJ_Delegati&ordina=Id', 1),
(264, 'Gestione Imprese', 'tmpl=2&pagina=elenchi&nome=EXPO_T_Imprese&ordina=Id', 0),
(265, 'Trattative', 'tmpl=2&pagina=elenchi&nome=EXPO_T_Bacheca_Trattative&ordina=Id', 0),
(266, 'Ricerca', 'tmpl=3&pagina=elenchi&nome=Ricerca', 0),
(267, 'Preferiti', 'tmpl=2&pagina=elenchi&nome=EXPO_T_Prodotti_Preferiti', 0),
(268, 'Conferma Utenza', 'tmpl=2&pagina=elenchi&nome=EXPO_Conferma_Utenza', 0),
(269, 'Seleziona Impresa', 'tmpl=2&pagina=elenchi&nome=EXPO_TJ_Impresa_Utente', 0),
(270, 'EXPO Login', 'tmpl=2&pagina=elenchi&nome=EXPO_Login', 0),
(271, 'Registrazione Smart Card', 'tmpl=2&pagina=elenchi&nome=EXPO_Smart_Card', 0),
(272, 'Iscrizione Imprese', 'tmpl=2&pagina=elenchi&nome=EXPO_Iscrizione_Impresa', 0),
(273, 'Trattative Off-Line', 'tmpl=2&pagina=elenchi&nome=EXPO_Trattative_Offline', 0),
(274, 'Vetrina Homepage', 'tmpl=2&pagina=elenchi&nome=Homepage', 0),
(275, 'Gestione Link HTDig', 'tmpl=2&pagina=elenchi&nome=Indicizzazione_Link', 0),
(276, 'Gestione contenuti HTDig', 'tmpl=2&pagina=elenchi&nome=Indicizzazione_Contenuti', 0),
(277, 'Time Out Login', 'tmpl=2&pagina=elenchi&nome=TimeOutLogin', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `T_Pagine_AL`
--

DROP TABLE IF EXISTS `T_Pagine_AL`;
CREATE TABLE IF NOT EXISTS `T_Pagine_AL` (
  `Id_AL` int(10) unsigned NOT NULL DEFAULT '0',
  `Lang_AL` char(2) NOT NULL DEFAULT '',
  `Descrizione_AL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_AL`,`Lang_AL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `T_Pagine_AL`
--

INSERT INTO `T_Pagine_AL` (`Id_AL`, `Lang_AL`, `Descrizione_AL`) VALUES
(1, 'EN', 'Homepage'),
(2, 'EN', 'Pagina interna'),
(3, 'EN', 'Pagina interna 3 colonne'),
(4, 'EN', 'Pagina interna login'),
(5, 'EN', 'Pagina interna con tab menu'),
(6, 'EN', 'Link esterno'),
(7, 'EN', 'Sito esterno'),
(8, 'EN', 'Mappa del Sito'),
(9, 'EN', 'Modifica i tuoi dati'),
(11, 'EN', 'Gestione Anagrafiche'),
(12, 'EN', 'Gestione Utenti'),
(13, 'EN', 'Privacy'),
(14, 'EN', 'Benvenuto'),
(15, 'EN', 'Password'),
(20, 'EN', 'Registrazione Utente'),
(21, 'EN', 'Inserimento Editor'),
(22, 'EN', 'Modifica Editor'),
(23, 'EN', 'Modifica Utente'),
(27, 'EN', 'Mappa Gruppi'),
(28, 'EN', 'Modifica Gruppi'),
(30, 'EN', 'Gestione Men? e Pagine'),
(31, 'EN', 'Gestione Box'),
(32, 'EN', 'Gestione Articoli'),
(33, 'EN', 'Gestione Template'),
(34, 'EN', 'Gestione Tipi Html'),
(35, 'EN', 'Upload Immagini'),
(36, 'EN', 'Upload Files'),
(37, 'EN', 'Gestione Banners'),
(38, 'EN', 'Mappa Amministrazione'),
(40, 'EN', 'Gestione Tipo Articolo 1'),
(41, 'EN', 'Gestione Articoli_X_Box_X_Menu'),
(42, 'EN', 'Gestione Articoli_AL'),
(43, 'EN', 'Gestione Menu_AL'),
(44, 'EN', 'Gestione Pagine_AL'),
(45, 'EN', 'Gestione Sottocategorie'),
(46, 'EN', 'Gestione Tipo Articolo'),
(47, 'EN', 'Selezione articoli'),
(50, 'EN', 'Gestione News'),
(51, 'EN', 'Gestione NewsLetter'),
(52, 'EN', 'Email Newsletter'),
(53, 'EN', 'Template NewsLetter'),
(54, 'EN', 'Gestione Argomenti NewsLetter'),
(55, 'EN', 'Inizializzazione lingue'),
(56, 'EN', 'Gestione Newsletter News'),
(57, 'EN', 'Archivio News'),
(58, 'EN', 'Invia ad un amico'),
(59, 'EN', 'Htdig'),
(60, 'EN', 'Eventi'),
(61, 'EN', 'Gestione eventi'),
(62, 'EN', 'Gestione SVIL Menu e Pagine'),
(63, 'EN', 'Gestione SVIL Box'),
(64, 'EN', 'Gestione SVIL Articoli'),
(65, 'EN', 'Gestione SVIL Articoli_X_Box_X_Menu'),
(66, 'EN', 'Gestione SVIL Articoli_AL'),
(67, 'EN', 'Gestione SVIL Menu_AL'),
(68, 'EN', 'Pubblicazione'),
(70, 'EN', 'Inserimento Redattore'),
(71, 'EN', 'Modifica Redattore'),
(72, 'EN', 'Inserimento Partner'),
(73, 'EN', 'Modifica Partner'),
(74, 'EN', 'Inserimento TipoUtente 8'),
(75, 'EN', 'Modifica TipoUtente 8'),
(76, 'EN', 'Inserimento TipoUtente 10'),
(77, 'EN', 'Modifica TipoUtente 10'),
(78, 'EN', 'Gestione Redattore'),
(79, 'EN', 'Gestione Partner'),
(80, 'EN', 'Gestione Tipo Utente 8'),
(81, 'EN', 'Gestione Tipo Utente 10'),
(85, 'EN', 'Anagrafica Dati aggiuntivi - Inserimento'),
(86, 'EN', 'Anagrafica Dati aggiuntivi - Aggiornamento'),
(95, 'EN', 'Gestione Uffici'),
(96, 'EN', 'Gestione Applicazioni'),
(97, 'EN', 'Gestione Applicazioni X Utente'),
(98, 'EN', 'Upload Documenti'),
(100, 'EN', 'Label'),
(101, 'EN', 'Attivazione Utente'),
(103, 'EN', 'Gestione Mail Generiche'),
(104, 'EN', 'Form Mail Generiche'),
(105, 'EN', 'Pagine Messaggi Standard'),
(109, 'EN', 'Invio Neswletter'),
(110, 'EN', 'Importa Nominativi da file'),
(142, 'EN', 'Intranet Documenti'),
(143, 'EN', 'Intranet: Gestione documentale'),
(202, 'EN', 'GREEN Risultati ricerca'),
(203, 'EN', 'SALE - Gestione Sale'),
(217, 'EN', 'SALE - Gestione Agenzie'),
(218, 'EN', 'SALE_Det_Struttura'),
(219, 'EN', 'Gestione Foto Struttura'),
(220, 'EN', NULL),
(221, 'EN', NULL),
(222, 'EN', NULL),
(223, 'EN', NULL),
(224, 'EN', NULL),
(225, 'EN', NULL),
(226, 'EN', NULL),
(227, 'EN', NULL),
(228, 'EN', NULL),
(229, 'EN', NULL),
(245, 'EN', NULL),
(246, 'EN', NULL),
(247, 'EN', NULL),
(248, 'EN', NULL),
(249, 'EN', NULL),
(250, 'EN', NULL),
(251, 'EN', NULL),
(252, 'EN', NULL),
(253, 'EN', NULL),
(254, 'EN', NULL),
(255, 'EN', NULL),
(256, 'EN', NULL),
(257, 'EN', NULL),
(258, 'EN', NULL),
(259, 'EN', NULL),
(260, 'EN', NULL),
(261, 'EN', NULL),
(262, 'EN', NULL),
(264, 'EN', NULL),
(265, 'EN', NULL),
(266, 'EN', NULL),
(267, 'EN', NULL),
(268, 'EN', NULL),
(269, 'EN', NULL),
(270, 'EN', NULL),
(271, 'EN', NULL),
(272, 'EN', NULL),
(273, 'EN', NULL),
(274, 'EN', NULL),
(274, 'FR', NULL),
(275, 'EN', NULL),
(275, 'FR', NULL),
(276, 'EN', NULL),
(276, 'FR', NULL),
(277, 'EN', NULL),
(277, 'FR', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_Tlk_Categorie`
--


DROP TABLE IF EXISTS `EXPO_Tlk_Categorie`;
CREATE TABLE `EXPO_Tlk_Categorie` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Id_Categoria` varchar(50) DEFAULT NULL,
  `Descrizione` varchar(255) NOT NULL DEFAULT '''''',
  `DescEstesa` varchar(255) NOT NULL DEFAULT '''''',
  `Stato_Record` varchar(1) DEFAULT NULL,
  `DescEstesa_AL` varchar(255) NOT NULL DEFAULT '''''',
  `Descrizione_AL` varchar(255) NOT NULL DEFAULT '''''',
  `DescEstesa_AL2` varchar(255) NOT NULL DEFAULT '''''',
  `Descrizione_AL2` varchar(255) NOT NULL DEFAULT '''''',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=331 DEFAULT CHARSET=utf8;

#
# Data for table "expo_tlk_categorie"
#

INSERT INTO `EXPO_Tlk_Categorie` VALUES (1,'01','Design','Design','Y','Design','Design','Design','Design'),(2,'02','Costruzione','Costruzione','Y','Construction','Construction','Construction','Construction'),(3,'01.01','Architettura','Design->Architettura','Y','Design-> Architecture',' Architecture','Design-> Architecture',' Architecture'),(4,'01.02','Ingegneria-Strutture','Design->Ingegneria-Strutture','Y','Design->Engineering-Structures','Engineering-Structures','Design->Ing&eacute;nierie - Structures','Ing&eacute;nierie - Structures'),(5,'01.02.01','Strutture in acciaio','Design->Ingegneria-Strutture->Strutture in acciaio','Y','Design->Engineering-Structures->Steel structures','Steel structures','Design->Ing&eacute;nierie - Structures->Structures en acier','Structures en acier'),(6,'01.02.02','Strutture in legno','Design->Ingegneria-Strutture->Strutture in legno','Y','Design->Engineering-Structures->Wood structures','Wood structures','Design->Ing&eacute;nierie - Structures->Structures en bois','Structures en bois'),(7,'01.02.04','Strutture in cemento','Design->Ingegneria-Strutture->Strutture in cemento','Y','Design->Engineering-Structures->Cement structures','Cement structures','Design->Ing&eacute;nierie - Structures->Structures en ciment','Structures en ciment'),(8,'01.04','Ingegneria-Impianti','Design->Ingegneria-Impianti','Y','Design->Engineering-Technical systems','Engineering-Technical systems','Design->Ing&eacute;nierie - Installations','Ing&eacute;nierie - Installations'),(12,'01.04.04','Isolamento acustico','Design->Ingegneria-Impianti->Isolamento acustico','Y','Design->Engineering-Technical systems-> Soundproofing',' Soundproofing','Design->Ing&eacute;nierie - Installations->Insonorisation','Insonorisation'),(14,'01.04.02','impianti di traslazione (ascensori, montacarichi, scale mobili, tappeti mobili)','Design->Ingegneria-Impianti->impianti di traslazione (ascensori, montacarichi, scale mobili, tappeti mobili)','Y','Design->Engineering-Technical systems->lift, elevator, escalator and travelator systems','lift, elevator, escalator and travelator systems','Design->Ing&eacute;nierie - Installations->Syst&egrave;mes de d&eacute;placement (ascenseurs, monte-charge, escaliers roulants, tapis mobiles)','Syst&egrave;mes de d&eacute;placement (ascenseurs, monte-charge, escaliers roulants, tapis mobiles)'),(18,'01.04.03','Impianti frigoriferi/cucine','Design->Ingegneria-Impianti->Impianti frigoriferi/cucine','Y','Design->Engineering-Technical systems->Cooling systems / kitchen systems','Cooling systems / kitchen systems','Design->Ing&eacute;nierie - Installations-> Installations frigorifiques/cuisines',' Installations frigorifiques/cuisines'),(19,'01.04.01','Impianti idraulici ( incluso reti acque nere e rete meteorica )','Design->Ingegneria-Impianti->Impianti idraulici ( incluso reti acque nere e rete meteorica )','Y','Design->Engineering-Technical systems->Water systems (including sanitary sewer and rainwater handling system)','Water systems (including sanitary sewer and rainwater handling system)','Design->Ing&eacute;nierie - Installations->Circuits hydrauliques (r&eacute;seaux des eaux d\'&eacute;gout et des eaux de pluie)','Circuits hydrauliques (r&eacute;seaux des eaux d\'&eacute;gout et des eaux de pluie)'),(26,'02.01','Edilizia','Construction->Edilizia','Y','Construction->Construction','Construction','Construction->B&acirc;timent','B&acirc;timent'),(27,'02.01.01','Edifici civili e industriali (OG1)','Costruzione->Edilizia->Edifici civili e industriali (OG1)','Y','Construction->Construction->Civil and industrial construction (OG1)','Civil and industrial construction (OG1)','Construction->B&acirc;timent->�difices civils et industriels (OG1)','�difices civils et industriels (OG1)'),(28,'02.01.02',' Strutture prefabbricate in cemento armato (OS13)','Construction->Edilizia-> Strutture prefabbricate in cemento armato (OS13)','Y','Construction->Construction->Prefabricated reinforced concrete structures (OS13)','Prefabricated reinforced concrete structures (OS13)','Construction->B&acirc;timent->Structures pr&eacute;fabriqu&eacute;es en b&eacute;ton arm&eacute; (OS13)','Structures pr&eacute;fabriqu&eacute;es en b&eacute;ton arm&eacute; (OS13)'),(29,'02.01.03','Componenti strutturali in acciaio (OS18-A )','Construction->Edilizia->Componenti strutturali in acciaio (OS18-A )','Y','Construction->Construction->Steel structural components (OS18-A)','Steel structural components (OS18-A)','Construction->B&acirc;timent->Composants structurels en acier (OS18-A)','Composants structurels en acier (OS18-A)'),(30,'02.01.04',' Componenti per facciate continue (OS18-B)','Construction->Edilizia-> Componenti per facciate continue (OS18-B)','Y','Construction->Construction->Components for continuous façades (OS18-B)','Components for continuous façades (OS18-B)','Construction->B&acirc;timent->Composants pour fa&ccedil;ades continues (OS18-B)','Composants pour fa&ccedil;ades continues (OS18-B)'),(31,'02.01.05',' Opere strutturali speciali (OS21)','Construction->Edilizia-> Opere strutturali speciali (OS21)','Y','Construction->Construction->Special structural works (OS21)','Special structural works (OS21)','Construction->B&acirc;timent->Ouvrages structurels sp&eacute;ciaux (OS21)','Ouvrages structurels sp&eacute;ciaux (OS21)'),(32,'02.01.06',' Pavimentazioni e sovrastrutture speciali (OS26)','Construction->Edilizia-> Pavimentazioni e sovrastrutture speciali (OS26)','Y','Construction->Construction-> Pavements and special superstructures (OS26)',' Pavements and special superstructures (OS26)','Construction->B&acirc;timent-> Sols et superstructures sp&eacute;ciaux (OS26)',' Sols et superstructures sp&eacute;ciaux (OS26)'),(33,'02.01.08','Strutture in legno (OS32 )','Construction->Edilizia->Strutture in legno (OS32 )','Y','Construction->Construction->Wooden structures (OS32)','Wooden structures (OS32)','Construction->B&acirc;timent->Structures en bois (OS32)','Structures en bois (OS32)'),(34,'02.01.09',' Coperture speciali (OS33)','Construction->Edilizia-> Coperture speciali (OS33)','Y','Construction->Construction->Special roofs (OS33)','Special roofs (OS33)','Construction->B&acirc;timent->Couvertures sp&eacute;ciales (OS33)','Couvertures sp&eacute;ciales (OS33)'),(35,'02.01.10',' Interventi a basso impatto ambientale (OS35)','Construction->Edilizia-> Interventi a basso impatto ambientale (OS35)','Y','Construction->Construction->Works with low environmental impact (OS35)','Works with low environmental impact (OS35)','Construction->B&acirc;timent->Interventions &agrave; bas effet environnemental (OS35)','Interventions &agrave; bas effet environnemental (OS35)'),(36,'02.01.11','Per la costruzione di capannoni e tendostrutture leggere in PVC','Construction->Edilizia->Per la costruzione di capannoni e tendostrutture leggere in PVC','Y','Construction->Construction->For the construction of industrial sheds and light PVC tensile structures','For the construction of industrial sheds and light PVC tensile structures','Construction->B&acirc;timent->Pour la construction de hangars et de structures en toile l&eacute;g&egrave;re en PVC','Pour la construction de hangars et de structures en toile l&eacute;g&egrave;re en PVC'),(38,'02.01.14',' Finiture di opere generati di natura edile (OS7)','Construction->Edilizia-> Finiture di opere generati di natura edile (OS7)','Y','Construction->Construction->Finishing of cement works (OS7)','Finishing of cement works (OS7)','Construction->B&acirc;timent->Finitions d\'ouvrages cr&eacute;&eacute;s de nature b&acirc;timent (OS7)','Finitions d\'ouvrages cr&eacute;&eacute;s de nature b&acirc;timent (OS7)'),(39,'02.01.15',' Finiture di opere generali di natura tecnica (OS8)','Construction->Edilizia-> Finiture di opere generali di natura tecnica (OS8)','Y','Construction->Construction->Finishing of technical works (OS8)','Finishing of technical works (OS8)','Construction->B&acirc;timent->Finitions d\'ouvrages cr&eacute;&eacute;s de nature technique (OS8)','Finitions d\'ouvrages cr&eacute;&eacute;s de nature technique (OS8)'),(40,'02.02','Impianti (sia categorie generali che specialistiche)','Construction->Impianti (sia categorie generali che specialistiche)','Y','Construction->Systems (both general and specialized)','Systems (both general and specialized)','Construction->&eacute;quipements (cat&eacute;gories tant g&Eacute;n&eacute;rales que sp&eacute;ciales)','&eacute;quipements (cat&eacute;gories tant g&Eacute;n&eacute;rales que sp&eacute;ciales)'),(42,'02.02.03',' Impianti idrico-sanitario, cucine, lavanderie (OS3)','Construction->Impianti (sia categorie generali che specialistiche)-> Impianti idrico-sanitario, cucine, lavanderie (OS3)','Y','Construction->Systems (both general and specialized)->Sanitary water systems, kitchens, laundries (OS3)','Sanitary water systems, kitchens, laundries (OS3)','Construction->&eacute;quipements (cat&eacute;gories tant g&Eacute;n&eacute;rales que sp&eacute;ciales)->&Eacute;quipements hydro-sanitaires, cuisines, buanderies (OS3)','&Eacute;quipements hydro-sanitaires, cuisines, buanderies (OS3)'),(44,'03','Forniture','Forniture','Y','Supplies','Supplies','Supplies','Supplies'),(54,'04','Services','Services','Y','Services','Services','Services','Services'),(61,'04.09.05','Controllo di Qualit&agrave;�/Certificatori','Services-> Design & Construction Services->Controllo di Qualit&agrave;�/Certificatori','Y','Services-> Design & Construction Services->Quality Control / Certifiers','Quality Control / Certifiers','Services->Design Et Services de Construction ->Contr�le de Qualit�/Certificateurs ','Contr�le de Qualit�/Certificateurs '),(62,'04.09.09','Servizi linguistici e congressuali','Services-> Design & Construction Services->Servizi linguistici e congressuali','Y','Services-> Design & Construction Services->Linguistic and convention services','Linguistic and convention services','Services->Design Et Services de Construction ->Services linguistiques et pour s&eacute;minaires','Services linguistiques et pour s&eacute;minaires'),(67,'01.01.01','Architettura','Design->Architettura->Architettura','Y','Design->Architectural-> Architecture',' Architecture','Design->Architectural-> Architecture',' Architecture'),(68,'01.01.02','Paesaggio','Design->Architettura->Paesaggio','Y','Design-> Architecture->Landscape','Landscape','Design-> Architecture->Paysage','Paysage'),(70,'01.01.03','Allestimento ed arredamento indoor - outdoor','Design->Architettura->Allestimento ed arredamento indoor - outdoor','Y','Design-> Architecture->Indoor/outdoor decoration and furnishing','Indoor/outdoor decoration and furnishing','Design-> Architecture->agencements et ameublement int&eacute;rieur - ext&eacute;rieur','agencements et ameublement int&eacute;rieur - ext&eacute;rieur'),(71,'01.01.04','Scenografie','Design->Architettura->Scenografie','Y','Design-> Architecture->Scenography','Scenography','Design-> Architecture->Sc&eacute;nographies','Sc&eacute;nographies'),(72,'01.02.05','Strutture in vetro','Design->Ingegneria-Strutture->Strutture in vetro','Y','Design->Engineering-Structures->Glass structures','Glass structures','Design->Ing&eacute;nierie - Structures->Structures en verre','Structures en verre'),(73,'01.02.06','Coperture speciali (tecnsostrutture, geodetiche..)','Design->Ingegneria-Strutture->Coperture speciali (tecnsostrutture, geodetiche..)','Y','Design->Engineering-Structures->Special roofs (techno-structures, geodetic structures, etc.)','Special roofs (techno-structures, geodetic structures, etc.)','Design->Ing&eacute;nierie - Structures->couvertures sp�ciales (structures techniques, g�od�siques..)','couvertures sp�ciales (structures techniques, g�od�siques..)'),(74,'01.03','Esperti','Design->Esperti','Y','Design->Experts','Experts','Design->Experts','Experts'),(75,'01.03.01','Consulenza sostenibilit&agrave; ambientale','Design->Esperti->Consulenza sostenibilit&agrave; ambientale','Y','Design->Experts->Environmental sustainability consulting services','Environmental sustainability consulting services','Design->Experts->Conseil durabilité environnementale','Conseil durabilité environnementale'),(76,'01.03.02','Collaudatori strutture','Design->Esperti->Collaudatori strutture','Y','Design->Experts->Structure inspectors','Structure inspectors','Design->Experts->Testeurs de structures','Testeurs de structures'),(77,'01.03.03','Coordinatore della sicurezza in fase esecutiva e di cantiere','Design->Esperti->Coordinatore della sicurezza in fase esecutiva e di cantiere','Y','Design->Experts->Executive phase and worksite safety coordinator','Executive phase and worksite safety coordinator','Design->Experts->Coordinateur de la s&eacute;curit&eacute; pendant la phase ex&eacute;cutive et de chantier','Coordinateur de la s&eacute;curit&eacute; pendant la phase ex&eacute;cutive et de chantier'),(79,'01.05','Ingegneria - Mobilit&agrave;','Design->Ingegneria - Mobilit&agrave;','Y','Design->Engineering - Mobility','Engineering - Mobility','Design->Ingénierie - Mobilité','Ingénierie - Mobilité'),(80,'01.05.01','Mobilit&agrave;� pedonale indoor e outdoor','Design->Ingegneria - Mobilit&agrave;->Mobilit&agrave;� pedonale indoor e outdoor','Y','Design->Engineering - Mobility->Indoor/outdoor pedestrian mobility','Indoor/outdoor pedestrian mobility','Design->Ingénierie - Mobilité->Mobilit� pi�tonni�re en int�rieur et ext�rieur','Mobilit� pi�tonni�re en int�rieur et ext�rieur'),(81,'01.06','Verde','Design->Verde','Y','Design->Green areas','Green areas','Design->Espaces verts','Espaces verts'),(82,'01.06.01','Giardinaggio','Design->Verde->Giardinaggio','Y','Design->Green areas->Gardening','Gardening','Design->Espaces verts-> Jardinage',' Jardinage'),(83,'01.06.02','Arboricoltura e agricoltura','Design->Verde->Arboricoltura e agricoltura','Y','Design->Green areas->Arboriculture and agriculture','Arboriculture and agriculture','Design->Espaces verts->Arboriculture et agriculture','Arboriculture et agriculture'),(84,'01.06.03','Vivaismo ornamentale','Design->Verde->Vivaismo ornamentale','Y','Design->Green areas->Ornamental plants','Ornamental plants','Design->Espaces verts->Ligniculture ornementale','Ligniculture ornementale'),(85,'01.07','Cantiere','Design->Cantiere','Y','Design->Worksite','Worksite','Design->Chantier','Chantier'),(86,'01.07.01',' Progettazione del cantiere','Design->Cantiere-> Progettazione del cantiere','Y','Design->Worksite->Worksite design','Worksite design','Design->Chantier->Conception du chantier','Conception du chantier'),(87,'01.07.02','Progettazione fase smantellamento e ripristino area','Design->Cantiere->Progettazione fase smantellamento e ripristino area','Y','Design->Worksite->Planning of dismantling and area restoration phase','Planning of dismantling and area restoration phase','Design->Chantier->&Eacute;tude de la phase du d&eacute;mant&egrave;lement et de remise en &eacute;tat de la zone','&Eacute;tude de la phase du d&eacute;mant&egrave;lement et de remise en &eacute;tat de la zone'),(88,'02.01.16',' Lavori in terra, scavo eripristino di volumi di terra (OS1)','Construction->Edilizia-> Lavori in terra, scavo eripristino di volumi di terra (OS1)','Y','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)','Earthmoving, excavation and restoration of volumes of earth (OS1)','Construction->B&acirc;timent->Terrassements, excavation et r&eacute;tablissement des volumes de terre (OS1)','Terrassements, excavation et r&eacute;tablissement des volumes de terre (OS1)'),(89,'02.02.02',' Impianti elettromeccanici trasportatori, ascensori,scale mobili (OS4)','Construction->Impianti (sia categorie generali che specialistiche)-> Impianti elettromeccanici trasportatori, ascensori,scale mobili (OS4)','Y','Construction->Systems (both general and specialized)->Electromechanical transportation, elevator, escalator systems (OS4)','Electromechanical transportation, elevator, escalator systems (OS4)','Construction->&eacute;quipements (cat&eacute;gories tant g&Eacute;n&eacute;rales que sp&eacute;ciales)->&Eacute;quipements &eacute;lectrom&eacute;caniques transporteurs, ascenseurs, escaliers roulants (OS','&Eacute;quipements &eacute;lectrom&eacute;caniques transporteurs, ascenseurs, escaliers roulants (OS'),(90,'03.01','Materiali da costruzione','Forniture->Materiali da costruzione','Y','Supplies->Construction materials','Construction materials','Supplies->Mat&eacute;riaux de construction','Mat&eacute;riaux de construction'),(91,'03.01.01','Materiali per costruzioni in acciaio','Forniture->Materiali da costruzione->Materiali per costruzioni in acciaio','Y','Supplies->Construction materials->Materials for steel structures','Materials for steel structures','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en acier','Mat&eacute;riaux pour les constructions en acier'),(92,'03.01.02','Materiali per costruzioni in cemento armato','Forniture->Materiali da costruzione->Materiali per costruzioni in cemento armato','Y','Supplies->Construction materials->Materials for reinforced concrete structures','Materials for reinforced concrete structures','Supplies->Mat&eacute;riaux de construction->mat&eacute;riaux pour les constructions en b&eacute;ton arm&eacute;','mat&eacute;riaux pour les constructions en b&eacute;ton arm&eacute;'),(93,'03.01.04','Materiali per costruzioni in legno','Forniture->Materiali da costruzione->Materiali per costruzioni in legno','Y','Supplies->Construction materials->Materials for wooden structures','Materials for wooden structures','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en bois','Mat&eacute;riaux pour les constructions en bois'),(94,'03.01.05','Materiali per costruzioni in ceramica','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica','Y','Supplies->Construction materials->Materials for ceramic structures','Materials for ceramic structures','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en c&eacute;ramique','Mat&eacute;riaux pour les constructions en c&eacute;ramique'),(95,'03.01.06','Materiali per costruzioni in vetro','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro','Y','Supplies->Construction materials->Materials for glass structures','Materials for glass structures','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en verre','Mat&eacute;riaux pour les constructions en verre'),(96,'03.01.07','Materiali per costruzioni in pvc/sintetici','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials','Materials for structures in PVC or other synthetic materials','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en PVC/synth&eacute;tiques','Mat&eacute;riaux pour les constructions en PVC/synth&eacute;tiques'),(97,'03.01.08','Materiali per finiture','Forniture->Materiali da costruzione->Materiali per finiture','Y','Supplies->Construction materials->Materials for finishings','Materials for finishings','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les finitions','Mat&eacute;riaux pour les finitions'),(98,'03.01.09','Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).','Materials for technical systems (elevators, special, electrical, mechanical, etc.).','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour installations (&eacute;l&eacute;vateurs, sp&eacute;ciaux, &eacute;lectriques, m&eacute;caniques, etc.).','Mat&eacute;riaux pour installations (&eacute;l&eacute;vateurs, sp&eacute;ciaux, &eacute;lectriques, m&eacute;caniques, etc.).'),(99,'03.02','Allestimenti e Arredo','Forniture->Allestimenti e Arredo','Y','Supplies->Installations and Furnishings','Installations and Furnishings','Supplies->Agencements et Ameublement','Agencements et Ameublement'),(100,'03.02.04','Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements','General and specific indoor/outdoor furnishing elements','Supplies->Agencements et Ameublement->&Eacute;l&eacute;ments g&eacute;n&eacute;raux et sp&eacute;cifiques pour l\'ameublement d\'espaces int&eacute;rieurs et ext&eacute;rieurs','&Eacute;l&eacute;ments g&eacute;n&eacute;raux et sp&eacute;cifiques pour l\'ameublement d\'espaces int&eacute;rieurs et ext&eacute;rieurs'),(101,'03.02.01','Uffici e sale convegni','Forniture->Allestimenti e Arredo->Uffici e sale convegni','Y','Supplies->Installations and Furnishings->Offices and conference rooms','Offices and conference rooms','Supplies->Agencements et Ameublement->Bureaux et salles pour s&eacute;minaires','Bureaux et salles pour s&eacute;minaires'),(102,'03.02.02','Bagni','Forniture->Allestimenti e Arredo->Bagni','Y','Supplies->Installations and Furnishings->Bathrooms','Bathrooms','Supplies->Agencements et Ameublement->Toilettes','Toilettes'),(103,'03.02.03','Cucine e spazi ristorazione','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione','Y','Supplies->Installations and Furnishings->Kitchens and food service facilities','Kitchens and food service facilities','Supplies->Agencements et Ameublement->Cuisines et espaces de restauration','Cuisines et espaces de restauration'),(104,'03.02.05',' Spazi eventi e mostre','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre','Y','Supplies->Installations and Furnishings->Spaces for events and exhibitions','Spaces for events and exhibitions','Supplies->Agencements et Ameublement->Espaces pour &eacute;v&eacute;nements et expositions','Espaces pour &eacute;v&eacute;nements et expositions'),(106,'03.02.08','Strutture/attrezzature per simulatori','Forniture->Allestimenti e Arredo->Strutture/attrezzature per simulatori','Y','Supplies->Installations and Furnishings->Simulator equipment and structures','Simulator equipment and structures','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour simulateurs ','Structures/&eacute;quipements pour simulateurs '),(107,'03.02.09','Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )','Forniture->Allestimenti e Arredo->Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )','Y','Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )','Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )','Supplies->Agencements et Ameublement->Am&eacute;nagements sc&eacute;nographiques en int&eacute;rieur et ext&eacute;rieur (piscines, fontaines, aires de jeux, etc. )','Am&eacute;nagements sc&eacute;nographiques en int&eacute;rieur et ext&eacute;rieur (piscines, fontaines, aires de jeux, etc. )'),(108,'03.03','Impianti','Forniture->Impianti','Y','Supplies->Technical Systems','Technical Systems','Supplies->Installations','Installations'),(109,'03.03.01','Pompe di calore, UTA, fan coil, gruppi frigo, ecc','Forniture->Impianti->Pompe di calore, UTA, fan coil, gruppi frigo, ecc','Y','Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.','Heat pumps, AHUs, fan coil, chillers, etc.','Supplies->Installations->Pompes &agrave; chaleur, UTA, ventiloconvecteurs, groupes frigorifiques, etc.','Pompes &agrave; chaleur, UTA, ventiloconvecteurs, groupes frigorifiques, etc.'),(110,'03.03.02','Idranti, naspi, estintori, ecc','Forniture->Impianti->Idranti, naspi, estintori, ecc','Y','Supplies->Technical Systems->Hydrants, fire hoses, extinguishers, etc.','Hydrants, fire hoses, extinguishers, etc.','Supplies->Installations->Bouches d\'incendie, d&eacute;rouleurs de tuyaux, extincteurs, etc.','Bouches d\'incendie, d&eacute;rouleurs de tuyaux, extincteurs, etc.'),(111,'03.03.03','Ascensori, scale mobili, tappeti mobili','Forniture->Impianti->Ascensori, scale mobili, tappeti mobili','Y','Supplies->Technical Systems->Lifts, elevators, escalators, travelators','Lifts, elevators, escalators, travelators','Supplies->Installations->Ascenseurs, escaliers roulants, tapis mobiles','Ascenseurs, escaliers roulants, tapis mobiles'),(112,'03.03.04','Tubazioni in acciaio, tubazioni in pvc, pead ecc; valvolame, contatori, disconnettori, ecc.','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc; valvolame, contatori, disconnettori, ecc.','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.','Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.','Supplies->Installations->Conduites en acier, en PVC, en PEHD, etc.','Conduites en acier, en PVC, en PEHD, etc.'),(118,'04.09.06','Smaltimento rifiuti derivanti da materiali di costruzione','Services-> Design & Construction Services->Smaltimento rifiuti derivanti da materiali di costruzione','Y','Services-> Design & Construction Services->Disposal of construction wastes','Disposal of construction wastes','Services->Design Et Services de Construction ->&Eacute;vacuation des d&eacute;chets provenant de mat&eacute;riaux de construction','&Eacute;vacuation des d&eacute;chets provenant de mat&eacute;riaux de construction'),(119,'04.09.07',' Assistenza legale - amministrativa - fiscale','Services-> Design & Construction Services-> Assistenza legale - amministrativa - fiscale','Y','Services-> Design & Construction Services->Legal, administrative, tax support services','Legal, administrative, tax support services','Services->Design Et Services de Construction ->Assistance l&eacute;gale - administrative - fiscale','Assistance l&eacute;gale - administrative - fiscale'),(120,'04.09.08','Servizi di stampa','Services-> Design & Construction Services->Servizi di stampa','Y','Services-> Design & Construction Services->Printing services','Printing services','Services->Design Et Services de Construction ->Services de presse','Services de presse'),(121,'04.09.10','Traslochi e Trasporti','Services-> Design & Construction Services->Traslochi e Trasporti','Y','Services-> Design & Construction Services->Transportation and moving services','Transportation and moving services','Services->Design Et Services de Construction ->D&eacute;m&eacute;nagements et Transports','D&eacute;m&eacute;nagements et Transports'),(125,'01.01.01.01','71.11','Design->Architettura->Architettura->71.11','Y','Design-> Architecture-> Architecture->09.87.00','09.87.00','Design-> Architecture-> Architecture->09.87.00','09.87.00'),(126,'01.01.02.01','71.11','Design->Architettura->Paesaggio->71.11','Y','Design-> Architecture->Landscape->','NULL','Design-> Architecture->Landscape->','NULL'),(127,'01.01.03.01','71.11','Design->Architettura->Allestimento ed arredamento indoor - outdoor->71.11','Y','Design-> Architecture->Indoor/outdoor decoration and furnishing->','NULL','Design-> Architecture->Indoor/outdoor decoration and furnishing->','NULL'),(128,'01.01.04.01','90.02','Design->Architettura->Scenografie->90.02','Y','Design-> Architecture->Scenography->','NULL','Design-> Architecture->Scenography->','NULL'),(129,'01.02.01.01','71.12','Design->Ingegneria-Strutture->Strutture in acciaio->71.12','Y','Design->Engineering-Structures->Steel structures->','NULL','Design->Engineering-Structures->Steel structures->','NULL'),(130,'01.02.02.01','71.12','Design->Ingegneria-Strutture->Strutture in legno->71.12','Y','Design->Engineering-Structures->Wood structures->','NULL','Design->Engineering-Structures->Wood structures->','NULL'),(131,'01.02.04.01','71.12','Design->Ingegneria-Strutture->Strutture in cemento->71.12','Y','Design->Engineering-Structures->Cement structures->','NULL','Design->Engineering-Structures->Cement structures->','NULL'),(132,'01.02.05.01','71.12','Design->Ingegneria-Strutture->Strutture in vetro->71.12','Y','Design->Engineering-Structures->Glass structures->','NULL','Design->Engineering-Structures->Glass structures->','NULL'),(133,'01.02.06.01','71.12','Design->Ingegneria-Strutture->Coperture speciali (tecnsostrutture, geodetiche…)->71.12','Y','Design->Engineering-Structures->Special roofs (techno-structures, geodetic structures, etc.)->','NULL','Design->Engineering-Structures->Special roofs (techno-structures, geodetic structures, etc.)->','NULL'),(134,'01.03.01.01','70.22.0','Design->Esperti->Consulenza sostenibilit&agrave; ambientale->70.22.0','Y','Design->Experts->Environmental sustainability consulting services->','NULL','Design->Experts->Environmental sustainability consulting services->','NULL'),(135,'01.03.01.02','74.90.9','Design->Esperti->Consulenza sostenibilit&agrave; ambientale->74.90.9','Y','Design->Experts->Environmental sustainability consulting services->','NULL','Design->Experts->Environmental sustainability consulting services->','NULL'),(136,'01.03.01.03','71.12.1','Design->Esperti->Consulenza sostenibilit&agrave; ambientale->71.12.1','Y','Design->Experts->Environmental sustainability consulting services->','NULL','Design->Experts->Environmental sustainability consulting services->','NULL'),(137,'01.03.02.01','71.20','Design->Esperti->Collaudatori strutture->71.20','Y','Design->Experts->Structure inspectors->','NULL','Design->Experts->Structure inspectors->','NULL'),(138,'01.03.03.01','74.90.2','Design->Esperti->Coordinatore della sicurezza in fase esecutiva e di cantiere->74.90.2','Y','Design->Experts->Executive phase and worksite safety coordinator->','NULL','Design->Experts->Executive phase and worksite safety coordinator->','NULL'),(139,'01.04.01.01','71.12','Design->Ingegneria-Impianti->Impianti idraulici ( incluso reti acque nere e rete meteorica )->71.12','Y','Design->Engineering-Technical systems->Water systems (including sanitary sewer and rainwater handling system)->','NULL','Design->Engineering-Technical systems->Water systems (including sanitary sewer and rainwater handling system)->','NULL'),(140,'01.04.02.01','71.12','Design->Ingegneria-Impianti->impianti di traslazione (ascensori, montacarichi, scale mobili, tappeti mobili)->71.12','Y','Design->Engineering-Technical systems->lift, elevator, escalator and travelator systems->','NULL','Design->Engineering-Technical systems->lift, elevator, escalator and travelator systems->','NULL'),(141,'01.04.03.01','71.12','Design->Ingegneria-Impianti->Impianti frigoriferi/cucine->71.12','Y','Design->Engineering-Technical systems->Cooling systems / kitchen systems->','NULL','Design->Engineering-Technical systems->Cooling systems / kitchen systems->','NULL'),(142,'01.04.04.01','71.12','Design->Ingegneria-Impianti->Isolamento acustico->71.12','Y','Design->Engineering-Technical systems-> Soundproofing->','NULL','Design->Engineering-Technical systems-> Soundproofing->','NULL'),(143,'01.05.01.01','71.12','Design->Ingegneria - Mobilità->Mobilità pedonale indoor e outdoor->71.12','Y','Design->Engineering - Mobility->Indoor/outdoor pedestrian mobility->','NULL','Design->Engineering - Mobility->Indoor/outdoor pedestrian mobility->','NULL'),(144,'01.06.01.01','71.12','Design->Verde->Giardinaggio->71.12','Y','Design->Green areas->Gardening->','NULL','Design->Green areas->Gardening->','NULL'),(145,'01.06.02.01','71.12','Design->Verde->Arboricoltura e agricoltura->71.12','Y','Design->Green areas->Arboriculture and agriculture->','NULL','Design->Green areas->Arboriculture and agriculture->','NULL'),(146,'01.06.03.01','71.12','Design->Verde->Vivaismo ornamentale->71.12','Y','Design->Green areas->Ornamental plants->','NULL','Design->Green areas->Ornamental plants->','NULL'),(147,'01.07.01.01','71.12.2','Design->Cantiere-> Progettazione del cantiere->71.12.2','Y','Design->Worksite->Worksite design->','NULL','Design->Worksite->Worksite design->','NULL'),(148,'01.07.02.01','71.12.2','Design->Cantiere->Progettazione fase smantellamento e ripristino area->71.12.2','Y','Design->Worksite->Planning of dismantling and area restoration phase->','NULL','Design->Worksite->Planning of dismantling and area restoration phase->','NULL'),(149,'02.01.01.01','41.20','Construction->Edilizia->Edifici civili e industriali(OG1)->41.20','Y','Construction->Construction->Civil and industrial construction (OG1)->','NULL','Construction->Construction->Civil and industrial construction (OG1)->','NULL'),(150,'02.01.02.01','23.61','Construction->Edilizia-> Strutture prefabbricate in cemento armato (OS13)->23.61','Y','Construction->Construction->Prefabricated reinforced concrete structures (OS13)->','NULL','Construction->Construction->Prefabricated reinforced concrete structures (OS13)->','NULL'),(152,'02.01.02.03','41.20','Construction->Edilizia-> Strutture prefabbricate in cemento armato (OS13)->41.20','Y','Construction->Construction->Prefabricated reinforced concrete structures (OS13)->','NULL','Construction->Construction->Prefabricated reinforced concrete structures (OS13)->','NULL'),(153,'02.01.03.01','24.10','Construction->Edilizia->Componenti strutturali in acciaio (OS18-A )->24.10','Y','Construction->Construction->Steel structural components (OS18-A)->','NULL','Construction->Construction->Steel structural components (OS18-A)->','NULL'),(154,'02.01.04.01','43.39','Construction->Edilizia-> Componenti per facciate continue (OS18-B)->43.39','Y','Construction->Construction->Components for continuous façades (OS18-B)->','NULL','Construction->Construction->Components for continuous façades (OS18-B)->','NULL'),(155,'02.01.05.01','43.39','Construction->Edilizia-> Opere strutturali speciali (OS21)->43.39','Y','Construction->Construction->Special structural works (OS21)->','NULL','Construction->Construction->Special structural works (OS21)->','NULL'),(156,'02.01.06.01','43.33','Construction->Edilizia-> Pavimentazioni e sovrastrutture speciali (OS26)->43.33','Y','Construction->Construction-> Pavements and special superstructures (OS26)->','NULL','Construction->Construction-> Pavements and special superstructures (OS26)->','NULL'),(157,'02.01.08.01','16.23','Construction->Edilizia->Strutture in legno (OS32 )->16.23','Y','Construction->Construction->Wooden structures (OS32)->','NULL','Construction->Construction->Wooden structures (OS32)->','NULL'),(158,'02.01.08.02','16.24','Construction->Edilizia->Strutture in legno (OS32 )->16.24','Y','Construction->Construction->Wooden structures (OS32)->','NULL','Construction->Construction->Wooden structures (OS32)->','NULL'),(159,'02.01.09.01','43.91','Construction->Edilizia-> Coperture speciali (OS33)->43.91','Y','Construction->Construction->Special roofs (OS33)->','NULL','Construction->Construction->Special roofs (OS33)->','NULL'),(160,'02.01.10.01','43.9','Construction->Edilizia-> Interventi a basso impatto ambientale (OS35)->43.9','Y','Construction->Construction->Works with low environmental impact (OS35)->','NULL','Construction->Construction->Works with low environmental impact (OS35)->','NULL'),(161,'02.01.11.01','43.39','Construction->Edilizia->Per la costruzione di capannoni e tendostrutture leggere in PVC->43.39','Y','Construction->Construction->For the construction of industrial sheds and light PVC tensile structures->','NULL','Construction->Construction->For the construction of industrial sheds and light PVC tensile structures->','NULL'),(163,'02.01.14.01','43.39','Construction->Edilizia-> Finiture di opere generati di natura edile (OS7)->43.39','Y','Construction->Construction->Finishing of cement works (OS7)->','NULL','Construction->Construction->Finishing of cement works (OS7)->','NULL'),(164,'02.01.15.01','43.39','Construction->Edilizia-> Finiture di opere generali di natura tecnica (OS8)->43.39','Y','Construction->Construction->Finishing of technical works (OS8)->','NULL','Construction->Construction->Finishing of technical works (OS8)->','NULL'),(165,'02.01.16.01','43.11','Construction->Edilizia-> Lavori in terra, scavo eripristino di volumi di terra (OS1)->43.11','Y','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL'),(166,'02.01.16.02','43.12','Construction->Edilizia-> Lavori in terra, scavo eripristino di volumi di terra (OS1)->43.12','Y','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL'),(167,'02.01.16.03','43.13','Construction->Edilizia-> Lavori in terra, scavo eripristino di volumi di terra (OS1)->43.13','Y','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL','Construction->Construction->Earthmoving, excavation and restoration of volumes of earth (OS1)->','NULL'),(168,'02.02.02.01','28.22.01','Construction->Impianti (sia categorie generali che specialistiche)-> Impianti elettromeccanici trasportatori, ascensori,scale mobili (OS4)->28.22.01','Y','Construction->Systems (both general and specialized)->Electromechanical transportation, elevator, escalator systems (OS4)->','NULL','Construction->Systems (both general and specialized)->Electromechanical transportation, elevator, escalator systems (OS4)->','NULL'),(169,'02.02.03.01','43.29','Construction->Impianti (sia categorie generali che specialistiche)-> Impianti idrico-sanitario, cucine, lavanderie (OS3)->43.29','Y','Construction->Systems (both general and specialized)->Sanitary water systems, kitchens, laundries (OS3)->','NULL','Construction->Systems (both general and specialized)->Sanitary water systems, kitchens, laundries (OS3)->','NULL'),(170,'03.01.01.01','24.10','Forniture->Materiali da costruzione->Materiali per costruzioni in acciaio->24.10','Y','Supplies->Construction materials->Materials for steel structures->','NULL','Supplies->Construction materials->Materials for steel structures->','NULL'),(171,'03.01.01.02','46.73.2','Forniture->Materiali da costruzione->Materiali per costruzioni in acciaio->46.73.2','Y','Supplies->Construction materials->Materials for steel structures->','NULL','Supplies->Construction materials->Materials for steel structures->','NULL'),(172,'03.01.02.01','46.73.2','Forniture->Materiali da costruzione->Materiali per costruzioni in cemento armato->46.73.2','Y','Supplies->Construction materials->Materials for reinforced concrete structures->','NULL','Supplies->Construction materials->Materials for reinforced concrete structures->','NULL'),(173,'03.01.04.01','46.73.1','Forniture->Materiali da costruzione->Materiali per costruzioni in legno->46.73.1','Y','Supplies->Construction materials->Materials for wooden structures->','NULL','Supplies->Construction materials->Materials for wooden structures->','NULL'),(174,'03.01.04.02','16.10','Forniture->Materiali da costruzione->Materiali per costruzioni in legno->16.10','Y','Supplies->Construction materials->Materials for wooden structures->','NULL','Supplies->Construction materials->Materials for wooden structures->','NULL'),(175,'03.01.04.03','16.29.1','Forniture->Materiali da costruzione->Materiali per costruzioni in legno->16.29.1','Y','Supplies->Construction materials->Materials for wooden structures->','NULL','Supplies->Construction materials->Materials for wooden structures->','NULL'),(176,'03.01.04.04','46.13','Forniture->Materiali da costruzione->Materiali per costruzioni in legno->46.13','Y','Supplies->Construction materials->Materials for wooden structures->','NULL','Supplies->Construction materials->Materials for wooden structures->','NULL'),(177,'03.01.05.01','46.13','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->46.13','Y','Supplies->Construction materials->Materials for ceramic structures->','NULL','Supplies->Construction materials->Materials for ceramic structures->','NULL'),(178,'03.01.05.02','46.73.2','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->46.73.2','Y','Supplies->Construction materials->Materials for ceramic structures->','NULL','Supplies->Construction materials->Materials for ceramic structures->','NULL'),(179,'03.01.05.03','23.31','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->23.31','Y','Supplies->Construction materials->Materials for ceramic structures->','NULL','Supplies->Construction materials->Materials for ceramic structures->','NULL'),(180,'03.01.05.04','23.20','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->23.31->23.20','Y','Supplies->Construction materials->Materials for ceramic structures->->','NULL','Supplies->Construction materials->Materials for ceramic structures->->','NULL'),(181,'03.01.05.05','46.44.2','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->46.44.2','Y','Supplies->Construction materials->Materials for ceramic structures->','NULL','Supplies->Construction materials->Materials for ceramic structures->','NULL'),(182,'03.01.05.06','23.41','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->23.41','Y','Supplies->Construction materials->Materials for ceramic structures->','NULL','Supplies->Construction materials->Materials for ceramic structures->','NULL'),(183,'03.01.06.01','46.73.3','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro->46.73.3','Y','Supplies->Construction materials->Materials for glass structures->','NULL','Supplies->Construction materials->Materials for glass structures->','NULL'),(184,'03.01.06.02','43.34','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro->43.34','Y','Supplies->Construction materials->Materials for glass structures->','NULL','Supplies->Construction materials->Materials for glass structures->','NULL'),(185,'03.01.06.03','46.13','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro->46.13','Y','Supplies->Construction materials->Materials for glass structures->','NULL','Supplies->Construction materials->Materials for glass structures->','NULL'),(186,'03.01.06.04','23.19.2','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro->23.19.2','Y','Supplies->Construction materials->Materials for glass structures->','NULL','Supplies->Construction materials->Materials for glass structures->','NULL'),(187,'03.01.06.05','46.44.1','Forniture->Materiali da costruzione->Materiali per costruzioni in vetro->46.44.1','Y','Supplies->Construction materials->Materials for glass structures->','NULL','Supplies->Construction materials->Materials for glass structures->','NULL'),(188,'03.01.07.01','22.21','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->22.21','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL'),(189,'03.01.07.02','22.22','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->22.22','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL'),(190,'03.01.07.03','22.23','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->22.23','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL'),(191,'03.01.07.04','46.13','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->46.13','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL'),(192,'03.01.07.05','25.50','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->25.50','Y','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL','Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->','NULL'),(193,'03.01.08.01','46.13.0','Forniture->Materiali da costruzione->Materiali per finiture->46.13.0','Y','Supplies->Construction materials->Materials for finishings->','NULL','Supplies->Construction materials->Materials for finishings->','NULL'),(194,'03.01.08.02','46.73.2','Forniture->Materiali da costruzione->Materiali per finiture->46.73.2','Y','Supplies->Construction materials->Materials for finishings->','NULL','Supplies->Construction materials->Materials for finishings->','NULL'),(195,'03.01.09.01','46.13','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->46.13','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL'),(196,'03.01.09.02','47.52','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->47.52','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL'),(197,'03.01.09.03','27.12','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->27.12','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL'),(198,'03.01.09.03.01','46.18.22','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->27.12->46.18.22','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->->','NULL'),(199,'03.01.09.04','77.39.93','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->77.39.93','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL'),(200,'03.01.09.05','25.62','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->25.62','Y','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL','Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->','NULL'),(201,'03.02.04.01','31.09','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->31.09','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL'),(202,'03.02.04.02','47.59.1','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->47.59.1','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL'),(203,'03.02.04.03','46.15','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->46.15','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL'),(204,'03.02.04.04','46.47.1','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->46.47.1','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->','NULL'),(205,'03.02.01.01','31.01','Forniture->Allestimenti e Arredo->Uffici e sale convegni->31.01','Y','Supplies->Installations and Furnishings->Offices and conference rooms->','NULL','Supplies->Installations and Furnishings->Offices and conference rooms->','NULL'),(206,'03.02.01.02','31.09','Forniture->Allestimenti e Arredo->Uffici e sale convegni->31.09','Y','Supplies->Installations and Furnishings->Offices and conference rooms->','NULL','Supplies->Installations and Furnishings->Offices and conference rooms->','NULL'),(207,'03.02.02.01','31.09','Forniture->Allestimenti e Arredo->Bagni->31.09','Y','Supplies->Installations and Furnishings->Bathrooms->','NULL','Supplies->Installations and Furnishings->Bathrooms->','NULL'),(208,'03.02.02.02','47.52.2','Forniture->Allestimenti e Arredo->Bagni->47.52.2','Y','Supplies->Installations and Furnishings->Bathrooms->','NULL','Supplies->Installations and Furnishings->Bathrooms->','NULL'),(209,'03.02.03.01','31.02','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->31.02','Y','Supplies->Installations and Furnishings->Kitchens and food service facilities->','NULL','Supplies->Installations and Furnishings->Kitchens and food service facilities->','NULL'),(210,'03.02.03.02','31.09','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->31.09','Y','Supplies->Installations and Furnishings->Kitchens and food service facilities->','NULL','Supplies->Installations and Furnishings->Kitchens and food service facilities->','NULL'),(211,'03.02.05.01','31.09','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->31.09','Y','Supplies->Installations and Furnishings->Spaces for events and exhibitions->','NULL','Supplies->Installations and Furnishings->Spaces for events and exhibitions->','NULL'),(212,'03.02.05.02','73.11','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->73.11','Y','Supplies->Installations and Furnishings->Spaces for events and exhibitions->','NULL','Supplies->Installations and Furnishings->Spaces for events and exhibitions->','NULL'),(213,'03.02.07.01','31.09','Forniture->Allestimenti e Arredo->Strutture/attrezzature per spettacoli->31.09','Y','Supplies->Installations and Furnishings->Entertainment equipment and structures->','NULL','Supplies->Installations and Furnishings->Entertainment equipment and structures->','NULL'),(214,'03.02.07.02','77.39.94','Forniture->Allestimenti e Arredo->Strutture/attrezzature per spettacoli->77.39.94','Y','Supplies->Installations and Furnishings->Entertainment equipment and structures->','NULL','Supplies->Installations and Furnishings->Entertainment equipment and structures->','NULL'),(216,'03.02.08.01','31.09','Forniture->Allestimenti e Arredo->Strutture/attrezzature per simulatori->31.09','Y','Supplies->Installations and Furnishings->Simulator equipment and structures ->','NULL','Supplies->Installations and Furnishings->Simulator equipment and structures ->','NULL'),(218,'03.02.09.01','31.09','Forniture->Allestimenti e Arredo->Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )->31.09','Y','Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->','NULL','Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->','NULL'),(221,'03.02.09.02','90.02','Forniture->Allestimenti e Arredo->Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )->90.02','Y','Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->','NULL','Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->','NULL'),(222,'03.03.01.02','46.69.2','Forniture->Impianti->Pompe di calore, UTA, fan coil, gruppi frigo, ecc->46.69.2','Y','Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.->','NULL','Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.->','NULL'),(223,'03.03.01.03','28.25','Forniture->Impianti->Pompe di calore, UTA, fan coil, gruppi frigo, ecc->28.25','Y','Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.->','NULL','Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.->','NULL'),(224,'03.03.02.01','28.29.99','Forniture->Impianti->Idranti, naspi, estintori, ecc->28.29.99','Y','Supplies->Technical Systems->Hydrants, fire hoses, extinguishers, etc.->','NULL','Supplies->Technical Systems->Hydrants, fire hoses, extinguishers, etc.->','NULL'),(225,'03.03.03.01','28.22','Forniture->Impianti->Ascensori, scale mobili, tappeti mobili->28.22','Y','Supplies->Technical Systems->Lifts, elevators, escalators, travelators->','NULL','Supplies->Technical Systems->Lifts, elevators, escalators, travelators->','NULL'),(226,'03.03.03.02','43.29.01','Forniture->Impianti->Ascensori, scale mobili, tappeti mobili->43.29.01','Y','Supplies->Technical Systems->Lifts, elevators, escalators, travelators->','NULL','Supplies->Technical Systems->Lifts, elevators, escalators, travelators->','NULL'),(227,'03.03.04.01','24.20','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc ; valvolame, contatori, disconnettori, ecc.->24.20','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL'),(228,'03.03.04.02','26.11','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc ; valvolame, contatori, disconnettori, ecc.->26.11','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL'),(229,'03.03.04.03','22.21','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc ; valvolame, contatori, disconnettori, ecc.->22.21','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL'),(230,'03.03.04.04','28.14','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc ; valvolame, contatori, disconnettori, ecc.->28.14','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL'),(231,'03.03.04.05','26.51.2','Forniture->Impianti->Tubazioni in acciaio, tubazioni in pvc, pead ecc ; valvolame, contatori, disconnettori, ecc.->26.51.2','Y','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL','Supplies->Technical Systems->Steel, PVC and other types of piping; valves, meters, backflow preventers, etc.->','NULL'),(240,'04.09.05.01','71.20.2','Design & Construction Services->Controllo di Qualit&agrave;/Certificatori->71.20.2','Y','Design & Construction Services->Quality Control / Certifiers->','NULL','Design & Construction Services->Quality Control / Certifiers->','NULL'),(241,'04.09.06.01','38.2','Design & Construction Services->Smaltimento rifiuti derivanti da materiali di costruzione->38.2','Y','Design & Construction Services->Disposal of construction wastes->','NULL','Design & Construction Services->Disposal of construction wastes->','NULL'),(242,'04.09.07.01','69.10','Design & Construction Services-> Assistenza legale- amministrativa- fiscale->69.10','Y','Design & Construction Services->Legal, administrative, tax support services ->','NULL','Design & Construction Services->Legal, administrative, tax support services ->','NULL'),(243,'04.09.07.02','69.20','Design & Construction Services-> Assistenza legale- amministrativa- fiscale->69.20','Y','Design & Construction Services->Legal, administrative, tax support services ->','NULL','Design & Construction Services->Legal, administrative, tax support services ->','NULL'),(244,'04.09.08.01','63.91','Design & Construction Services->Servizi di stampa->63.91','Y','Design & Construction Services->Printing services->','NULL','Design & Construction Services->Printing services->','NULL'),(245,'04.09.09.01','74.3','Design & Construction Services->Servizi linguistici e congressuali->74.3','Y','Design & Construction Services->Linguistic and convention services->','NULL','Design & Construction Services->Linguistic and convention services->','NULL'),(246,'04.09.10.01','49.41','Design & Construction Services->Traslochi e Trasporti->49.41','Y','Design & Construction Services->Transportation and moving services->','NULL','Design & Construction Services->Transportation and moving services->','NULL'),(247,'04.09.10.02','49.42','Design & Construction Services->Traslochi e Trasporti->49.42','Y','Design & Construction Services->Transportation and moving services->','NULL','Design & Construction Services->Transportation and moving services->','NULL'),(250,'04.09',' Design & Construction Services','Services-> Design & Construction Services','Y','Services-> Design & Construction Services',' Design & Construction Services','Services->Design Et Services de Construction ','Design Et Services de Construction '),(255,'04.09.01','Direzione Lavori','Services-> Design & Construction Services->Direzione Lavori','Y','Services-> Design & Construction Services->Works Supervision','Works Supervision','Services->Design Et Services de Construction ->Direction des travaux','Direction des travaux'),(256,'04.09.01.01','71.12.1','Services-> Design & Construction Services->Direzione Lavori->71.12.1','Y','Services-> Design & Construction Services->Works Supervision->','NULL','Services-> Design & Construction Services->Works Supervision->','NULL'),(257,'04.09.01.02','71.12.3','Services-> Design & Construction Services->Direzione Lavori->71.12.3','Y','Services-> Design & Construction Services->Works Supervision->','NULL','Services-> Design & Construction Services->Works Supervision->','NULL'),(258,'04.09.03','Consulenza sostenibilit&agrave;� ambientale, sociale e sicurezza','Services-> Design & Construction Services->Consulenza sostenibilit&agrave;� ambientale, sociale e sicurezza','Y','Services-> Design & Construction Services->Environmental sustainability, social and safety consulting','Environmental sustainability, social and safety consulting','Services->Design Et Services de Construction ->Conseil sur la durabilit� environnementale, sociale et la s�curit�','Conseil sur la durabilit� environnementale, sociale et la s�curit�'),(259,'04.09.03.01','74.90.2','Services-> Design & Construction Services->Consulenza sostenibilit&agrave; ambientale, sociale e sicurezza->74.90.2','Y','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL'),(260,'04.09.03.02','70.22','Services-> Design & Construction Services->Consulenza sostenibilit&agrave; ambientale, sociale e sicurezza->70.22','Y','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL'),(261,'04.09.04',' Noleggio mezzi di cantiere','Services-> Design & Construction Services-> Noleggio mezzi di cantiere','Y','Services-> Design & Construction Services->Rental of worksite vehicles','Rental of worksite vehicles','Services->Design Et Services de Construction ->Location de moyens de chantier','Location de moyens de chantier'),(262,'04.09.04.01','77.3','Services-> Design & Construction Services-> Noleggio mezzi di cantiere->77.3','Y','Services-> Design & Construction Services->Environmental sustainability, social and safety consulting->','NULL','Services-> Design & Construction Services->Environmental sustainability, social and safety consulting->','NULL'),(263,'04.09.04.02',' 77.11','Services-> Design & Construction Services-> Noleggio mezzi di cantiere-> 77.11','Y','Services-> Design & Construction Services->Environmental sustainability, social and safety consulting->','NULL','Services-> Design & Construction Services->Environmental sustainability, social and safety consulting->','NULL'),(264,'04.09.02',' Supporto specialistico procurement & project management','Services-> Design & Construction Services-> Supporto specialistico procurement & project management','Y','Services-> Design & Construction Services->Specialist procurement and project management support','Specialist procurement and project management support','Services->Design Et Services de Construction ->Support sp&eacute;cialis&eacute; et gestion des fournitures et du projet','Support sp&eacute;cialis&eacute; et gestion des fournitures et du projet'),(265,'04.09.02.02','74.90.9','Services-> Design & Construction Services->74.90.9','Y','Services-> Design & Construction Services->','NULL','Services-> Design & Construction Services->','NULL'),(266,'04.09.02.01','70.22','Services-> Design & Construction Services-> Supporto specialistico procurement & project management->70.22','Y','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL'),(267,'04.09.02.03','46.19.03','Services-> Design & Construction Services-> Supporto specialistico procurement & project management->46.19.03','Y','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL','Services-> Design & Construction Services->Specialist procurement and project management support->','NULL'),(268,'03.02.06','Strutture/attrezzature per spettacoli','Forniture->Allestimenti e Arredo->Strutture/attrezzature per spettacoli','Y','Supplies->Installations and Furnishings->Entertainment equipment and structures','Entertainment equipment and structures','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour spectacles','Structures/&eacute;quipements pour spectacles'),(269,'03.02.04.05','32.99.90','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->32.99.90','Y','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->\'\'','\'\'','Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->\'\'','\'\''),(270,'01.02.03','Strutture in muratura di laterizio','Design->Ingegneria-Strutture->Strutture in muratura di laterizio','Y','Design->Engineering-Structures->Strutture in muratura di laterizio','Strutture in muratura di laterizio','Design->Ing&eacute;nierie - Structures->Strutture in muratura di laterizio','Strutture in muratura di laterizio'),(271,'01.02.03.01','71.12','Design->Ingegneria-Strutture->Strutture in muratura di laterizio->71.12','Y','Design->Engineering-Structures->Strutture in muratura di laterizio->\'\'','\'\'','Design->Ing&eacute;nierie - Structures->Strutture in muratura di laterizio->\'\'','\'\''),(272,'02.01.07','Componenti in laterizio','Costruzione->Edilizia->Componenti in laterizio','Y','Construction->Construction->\'Componenti in laterizio','\'Componenti in laterizio','Construction->B&acirc;timent->Componenti in laterizio','Componenti in laterizio'),(273,'02.01.07.01','23.32.01','Costruzione->Edilizia->Componenti in laterizio->23.32.01','Y','Construction->Construction->\'Componenti in laterizio->\'\'','\'\'','Construction->B&acirc;timent->Componenti in laterizio->\'\'','\'\''),(274,'02.01.07.02','43.99.09','Costruzione->Edilizia->Componenti in laterizio->43.99.09','Y','Construction->Construction->\'Componenti in laterizio->\'\'','\'\'','Construction->B&acirc;timent->Componenti in laterizio->\'\'','\'\''),(275,'02.01.07.03','43.91','Costruzione->Edilizia->Componenti in laterizio->43.91','Y','Construction->Construction->\'Componenti in laterizio->\'\'','\'\'','Construction->B&acirc;timent->Componenti in laterizio->\'\'','\'\''),(276,'02.01.12','Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)','Costruzione->Edilizia->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)','Y','Construction->Construction->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)','Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)','Construction->B&acirc;timent->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)','Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)'),(277,'02.01.12.01','43.39','Costruzione->Edilizia->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->43.39','Y','Construction->Construction->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->\'\'','\'\'','Construction->B&acirc;timent->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->\'\'','\'\''),(278,'03.01.03','Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)','Forniture->Materiali da costruzione->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)','Y','Supplies->Construction materials->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)','Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)','Supplies->Mat&eacute;riaux de construction->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)','Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)'),(279,'03.01.03.01','23.32','Forniture->Materiali da costruzione->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->23.32','Y','Supplies->Construction materials->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\''),(280,'03.01.03.02','46.73.29','Forniture->Materiali da costruzione->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->46.73.29','Y','Supplies->Construction materials->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\''),(281,'03.01.03.03','47.52.30','Forniture->Materiali da costruzione->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->47.52.30','Y','Supplies->Construction materials->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\''),(282,'03.04','Verde','Forniture->Verde','Y','Supplies->Verde','Verde','Supplies->Verde','Verde'),(283,'03.04.01','Verde e arredo urbano (OS24)','Forniture->Verde->Verde e arredo urbano (OS24)','Y','Supplies->Verde->Verde e arredo urbano (OS24)','Verde e arredo urbano (OS24)','Supplies->Verde->Verde e arredo urbano (OS24)','Verde e arredo urbano (OS24)'),(284,'03.04.01.01','46.73.29','Forniture->Verde->OS24 Verde e arredo urbano->46.73.29','Y','Supplies->Verde->OS24 Verde e arredo urbano->\'\'','\'\'','Supplies->Verde->OS24 Verde e arredo urbano->\'\'','\'\''),(285,'02.01.12.02','25.61','Costruzione->Edilizia->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->25.61',NULL,'Construction->Construction->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->\'\'','\'\'','Construction->B&acirc;timent->Finiture di opere generati in materiali lignei, plastici, metalli, in laterizio e vetrosi (OS6)->\'\'','\'\''),(286,'02.01.14.02','23.7','Costruzione->Edilizia-> Finiture di opere generati di natura edile (OS7)->23.7',NULL,'Construction->Construction->Finishing of cement works (OS7)->\'\'','\'\'','Construction->B&acirc;timent->Finitions d\'ouvrages cr&eacute;&eacute;s de nature b&acirc;timent (OS7)->\'\'','\'\''),(287,'03.01.01.03','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in acciaio->47.91 ',NULL,'Supplies->Construction materials->Materials for steel structures->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en acier->\'\'','\'\''),(288,'03.01.02.02','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in cemento armato->47.91 ',NULL,'Supplies->Construction materials->Materials for reinforced concrete structures->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->mat&eacute;riaux pour les constructions en b&eacute;ton arm&eacute;->\'\'','\'\''),(289,'03.01.03.04','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->47.91 ',NULL,'Supplies->Construction materials->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Materiali per costruzioni in laterizio (elementi per copertura, muro , solaio e piastrelle in cotto, ecc.)->\'\'','\'\''),(290,'03.01.04.05','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in legno->47.91 ',NULL,'Supplies->Construction materials->Materials for wooden structures->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en bois->\'\'','\'\''),(291,'03.01.05.07','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in ceramica->47.91 ',NULL,'Supplies->Construction materials->Materials for ceramic structures->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en c&eacute;ramique->\'\'','\'\''),(292,'03.01.07.06','47.91 ','Forniture->Materiali da costruzione->Materiali per costruzioni in pvc/sintetici->47.91 ',NULL,'Supplies->Construction materials->Materials for structures in PVC or other synthetic materials->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les constructions en PVC/synth&eacute;tiques->\'\'','\'\''),(293,'03.01.08.03','47.91 ','Forniture->Materiali da costruzione->Materiali per finiture->47.91 ',NULL,'Supplies->Construction materials->Materials for finishings->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour les finitions->\'\'','\'\''),(294,'03.01.09.06','47.91 ','Forniture->Materiali da costruzione->Materiali per impianti (elevatori, speciali, elettrici, meccanici, ecc).->47.91 ',NULL,'Supplies->Construction materials->Materials for technical systems (elevators, special, electrical, mechanical, etc.).->\'\'','\'\'','Supplies->Mat&eacute;riaux de construction->Mat&eacute;riaux pour installations (&eacute;l&eacute;vateurs, sp&eacute;ciaux, &eacute;lectriques, m&eacute;caniques, etc.).->\'\'','\'\''),(295,'03.02.01.03','43.32.02','Forniture->Allestimenti e Arredo->Uffici e sale convegni->43.32.02',NULL,'Supplies->Installations and Furnishings->Offices and conference rooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Bureaux et salles pour s&eacute;minaires->\'\'','\'\''),(296,'03.02.01.04','47.91 ','Forniture->Allestimenti e Arredo->Uffici e sale convegni->47.91 ',NULL,'Supplies->Installations and Furnishings->Offices and conference rooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Bureaux et salles pour s&eacute;minaires->\'\'','\'\''),(297,'03.02.01.05','47.59.3','Forniture->Allestimenti e Arredo->Uffici e sale convegni->47.59.3',NULL,'Supplies->Installations and Furnishings->Offices and conference rooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Bureaux et salles pour s&eacute;minaires->\'\'','\'\''),(298,'03.02.01.06','47.59.2 ','Forniture->Allestimenti e Arredo->Uffici e sale convegni->47.59.2 ',NULL,'Supplies->Installations and Furnishings->Offices and conference rooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Bureaux et salles pour s&eacute;minaires->\'\'','\'\''),(299,'03.02.02.03','47.59.3','Forniture->Allestimenti e Arredo->Bagni->47.59.3',NULL,'Supplies->Installations and Furnishings->Bathrooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Toilettes->\'\'','\'\''),(300,'03.02.02.04','47.91 ','Forniture->Allestimenti e Arredo->Bagni->47.91 ',NULL,'Supplies->Installations and Furnishings->Bathrooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Toilettes->\'\'','\'\''),(302,'03.02.02.05','43.32.02','Forniture->Allestimenti e Arredo->Bagni->43.32.02',NULL,'Supplies->Installations and Furnishings->Bathrooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Toilettes->\'\'','\'\''),(303,'03.02.02.06','47.59.2 ','Forniture->Allestimenti e Arredo->Bagni->47.59.2 ',NULL,'Supplies->Installations and Furnishings->Bathrooms->\'\'','\'\'','Supplies->Agencements et Ameublement->Toilettes->\'\'','\'\''),(304,'03.02.03.03','47.59.2 ','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->47.59.2 ',NULL,'Supplies->Installations and Furnishings->Kitchens and food service facilities->\'\'','\'\'','Supplies->Agencements et Ameublement->Cuisines et espaces de restauration->\'\'','\'\''),(305,'03.02.03.04','47.91 ','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->47.91 ',NULL,'Supplies->Installations and Furnishings->Kitchens and food service facilities->\'\'','\'\'','Supplies->Agencements et Ameublement->Cuisines et espaces de restauration->\'\'','\'\''),(306,'03.02.03.05','47.59.3','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->47.59.3',NULL,'Supplies->Installations and Furnishings->Kitchens and food service facilities->\'\'','\'\'','Supplies->Agencements et Ameublement->Cuisines et espaces de restauration->\'\'','\'\''),(307,'03.02.03.06','43.32.02','Forniture->Allestimenti e Arredo->Cucine e spazi ristorazione->43.32.02',NULL,'Supplies->Installations and Furnishings->Kitchens and food service facilities->\'\'','\'\'','Supplies->Agencements et Ameublement->Cuisines et espaces de restauration->\'\'','\'\''),(308,'03.02.05.03','47.59.3','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->47.59.3',NULL,'Supplies->Installations and Furnishings->Spaces for events and exhibitions->\'\'','\'\'','Supplies->Agencements et Ameublement->Espaces pour &eacute;v&eacute;nements et expositions->\'\'','\'\''),(309,'03.02.05.04','47.91 ','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->47.91 ',NULL,'Supplies->Installations and Furnishings->Spaces for events and exhibitions->\'\'','\'\'','Supplies->Agencements et Ameublement->Espaces pour &eacute;v&eacute;nements et expositions->\'\'','\'\''),(310,'03.02.05.05','43.32.02','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->43.32.02',NULL,'Supplies->Installations and Furnishings->Spaces for events and exhibitions->\'\'','\'\'','Supplies->Agencements et Ameublement->Espaces pour &eacute;v&eacute;nements et expositions->\'\'','\'\''),(311,'03.02.05.06','47.59.2 ','Forniture->Allestimenti e Arredo-> Spazi eventi e mostre->47.59.2 ',NULL,'Supplies->Installations and Furnishings->Spaces for events and exhibitions->\'\'','\'\'','Supplies->Agencements et Ameublement->Espaces pour &eacute;v&eacute;nements et expositions->\'\'','\'\''),(312,'03.02.04.06','47.59.2 ','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->47.59.2 ',NULL,'Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->\'\'','\'\'','Supplies->Agencements et Ameublement->&Eacute;l&eacute;ments g&eacute;n&eacute;raux et sp&eacute;cifiques pour l\'ameublement d\'espaces int&eacute;rieurs et ext&eacute;rieurs->\'\'','\'\''),(313,'03.02.04.07','43.32.02','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->43.32.02',NULL,'Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->\'\'','\'\'','Supplies->Agencements et Ameublement->&Eacute;l&eacute;ments g&eacute;n&eacute;raux et sp&eacute;cifiques pour l\'ameublement d\'espaces int&eacute;rieurs et ext&eacute;rieurs->\'\'','\'\''),(314,'03.02.04.08','47.91 ','Forniture->Allestimenti e Arredo->Elementi generali  e specifici per l\'arredo di spazi indoor e outdoor->47.91 ',NULL,'Supplies->Installations and Furnishings->General and specific indoor/outdoor furnishing elements->\'\'','\'\'','Supplies->Agencements et Ameublement->&Eacute;l&eacute;ments g&eacute;n&eacute;raux et sp&eacute;cifiques pour l\'ameublement d\'espaces int&eacute;rieurs et ext&eacute;rieurs->\'\'','\'\''),(315,'03.02.06.01','47.91 ','Forniture->Allestimenti e Arredo->Strutture/attrezzature per spettacoli->47.91 ',NULL,'Supplies->Installations and Furnishings->Entertainment equipment and structures->\'\'','\'\'','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour spectacles->\'\'','\'\''),(316,'03.02.06.02','43.32.02','Forniture->Allestimenti e Arredo->Strutture/attrezzature per spettacoli->43.32.02',NULL,'Supplies->Installations and Furnishings->Entertainment equipment and structures->\'\'','\'\'','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour spectacles->\'\'','\'\''),(317,'03.02.08.02','47.91 ','Forniture->Allestimenti e Arredo->Strutture/attrezzature per simulatori->47.91 ',NULL,'Supplies->Installations and Furnishings->Simulator equipment and structures->\'\'','\'\'','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour simulateurs ->\'\'','\'\''),(318,'03.02.08.03','43.32.02','Forniture->Allestimenti e Arredo->Strutture/attrezzature per simulatori->43.32.02',NULL,'Supplies->Installations and Furnishings->Simulator equipment and structures->\'\'','\'\'','Supplies->Agencements et Ameublement->Structures/&eacute;quipements pour simulateurs ->\'\'','\'\''),(319,'03.02.09.03','43.32.02','Forniture->Allestimenti e Arredo->Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )->43.32.02',NULL,'Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->\'\'','\'\'','Supplies->Agencements et Ameublement->Am&eacute;nagements sc&eacute;nographiques en int&eacute;rieur et ext&eacute;rieur (piscines, fontaines, aires de jeux, etc. )->\'\'','\'\''),(320,'03.02.09.04','47.91 ','Forniture->Allestimenti e Arredo->Allestimenti scenografici indoor e outdoor (piscine, fontane, play grounds, ecc. )->47.91 ',NULL,'Supplies->Installations and Furnishings->Indoor/outdoor scenery (pools, fountains, playgrounds, etc.) )->\'\'','\'\'','Supplies->Agencements et Ameublement->Am&eacute;nagements sc&eacute;nographiques en int&eacute;rieur et ext&eacute;rieur (piscines, fontaines, aires de jeux, etc. )->\'\'','\'\''),(321,'03.03.01.04','47.91 ','Forniture->Impianti->Pompe di calore, UTA, fan coil, gruppi frigo, ecc->47.91 ',NULL,'Supplies->Technical Systems->Heat pumps, AHUs, fan coil, chillers, etc.->\'\'','\'\'','Supplies->Installations->Pompes &agrave; chaleur, UTA, ventiloconvecteurs, groupes frigorifiques, etc.->\'\'','\'\''),(322,'03.03.02.02','47.91 ','Forniture->Impianti->Idranti, naspi, estintori, ecc->47.91 ',NULL,'Supplies->Technical Systems->Hydrants, fire hoses, extinguishers, etc.->\'\'','\'\'','Supplies->Installations->Bouches d\'incendie, d&eacute;rouleurs de tuyaux, extincteurs, etc.->\'\'','\'\''),(323,'03.04.01.02','47.91 ','Forniture->Verde->Verde e arredo urbano (OS24)->47.91 ',NULL,'Supplies->Verde->Verde e arredo urbano (OS24)->\'\'','\'\'','Supplies->Verde->Verde e arredo urbano (OS24)->\'\'','\'\''),(324,'04.09.02.04','82.99.99','Services-> Design & Construction Services-> Supporto specialistico procurement & project management->82.99.99',NULL,'Services-> Design & Construction Services->Specialist procurement and project management support->\'\'','\'\'','Services->Design Et Services de Construction ->Support sp&eacute;cialis&eacute; et gestion des fournitures et du projet->\'\'','\'\''),(325,'04.09.03.03','82.99.99','Services-> Design & Construction Services->Consulenza sostenibilit&agrave;� ambientale, sociale e sicurezza->82.99.99',NULL,'Services-> Design & Construction Services->Environmental sustainability, social and safety consulting->\'\'','\'\'','Services->Design Et Services de Construction ->Conseil sur la durabilit� environnementale, sociale et la s�curit�->\'\'','\'\''),(326,'04.09.07.03','82.99.99','Services-> Design & Construction Services-> Assistenza legale - amministrativa - fiscale->82.99.99',NULL,'Services-> Design & Construction Services->Legal, administrative, tax support services->\'\'','\'\'','Services->Design Et Services de Construction ->Assistance l&eacute;gale - administrative - fiscale->\'\'','\'\''),(327,'04.09.08.02','82.99.99','Services-> Design & Construction Services->Servizi di stampa->82.99.99',NULL,'Services-> Design & Construction Services->Printing services->\'\'','\'\'','Services->Design Et Services de Construction ->Services de presse->\'\'','\'\''),(328,'04.09.09.02','82.3','Services-> Design & Construction Services->Servizi linguistici e congressuali->82.3',NULL,'Services-> Design & Construction Services->Linguistic and convention services->\'\'','\'\'','Services->Design Et Services de Construction ->Services linguistiques et pour s&eacute;minaires->\'\'','\'\''),(329,'04.09.09.03','82.99.99','Services-> Design & Construction Services->Servizi linguistici e congressuali->82.99.99',NULL,'Services-> Design & Construction Services->Linguistic and convention services->\'\'','\'\'','Services->Design Et Services de Construction ->Services linguistiques et pour s&eacute;minaires->\'\'','\'\''),(330,'04.09.09.04','79.90.19','Services-> Design & Construction Services->Servizi linguistici e congressuali->79.90.19',NULL,'Services-> Design & Construction Services->Linguistic and convention services->\'\'','\'\'','Services->Design Et Services de Construction ->Services linguistiques et pour s&eacute;minaires->\'\'','\'\'');



-- 30/10/2013
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Tipo_Impresa`
--

CREATE TABLE IF NOT EXISTS `EXPO_T_Tipo_Impresa` (
  `Id` int(11) NOT NULL,
  `Descrizione` varchar(50) NOT NULL,
  `Descrizione_EN` varchar(50) NOT NULL,
  `Descrizione_FR` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_T_Tipo_Impresa`
--

INSERT INTO `EXPO_T_Tipo_Impresa` (`Id`, `Descrizione`, `Descrizione_EN`, `Descrizione_FR`) VALUES
(1, 'Micro', 'Micro', 'Micro'),
(2, 'Piccola', 'Small', 'Faible'),
(3, 'Media', 'Middle', 'M�dias'),
(4, 'Grande', 'Big', 'Magnifique');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Tipo_Impresa_Dipendenti`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Tipo_Impresa_Dipendenti` (
  `Id` int(11) NOT NULL,
  `IdTipoImpresa` int(11) NOT NULL,
  `Valore` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Tipo_Impresa_Dipendenti`
--

INSERT INTO `EXPO_TJ_Tipo_Impresa_Dipendenti` (`Id`, `IdTipoImpresa`, `Valore`) VALUES
(1, 1, '0|9'),
(2, 2, '10|49'),
(3, 3, '50|249'),
(4, 4, '250|');

--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Tipo_Impresa_Fatturato`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Tipo_Impresa_Fatturato` (
  `Id` int(11) NOT NULL,
  `IdTipoImpresa` int(11) NOT NULL,
  `Valore` varchar(10) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_Tipo_Impresa_Fatturato`
--

INSERT INTO `EXPO_TJ_Tipo_Impresa_Fatturato` (`Id`, `IdTipoImpresa`, `Valore`) VALUES
(1, 1, '0|10'),
(2, 2, '11|50'),
(3, 3, '51|100'),
(4, 4, '101|');


-- MODIFICA DEL 06/11/2013

ALTER TABLE  `EXPO_T_Bacheca_Trattative` ADD  `IdMessaggio` INT NOT NULL AFTER  `Stato`;



--