-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 08, 2013 alle 16:11
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_FascieFatturato`
--

DROP TABLE IF EXISTS `EXPO_T_FascieFatturato`;
CREATE TABLE IF NOT EXISTS `EXPO_T_FascieFatturato` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Descrizione` varchar(255) NOT NULL,
  `Importo` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_T_FascieFatturato`
--

INSERT INTO `EXPO_T_FascieFatturato` (`Id`, `Nome`, `Descrizione`, `Importo`) VALUES
(1, 'Micro impresa', 'fatturato 0-2 milione euro', '100'),
(2, 'piccole-medie imprese', 'fatturato compreso tra 2 e 50 milioni', '300'),
(3, 'grandi imprese', 'Fatturato maggiore di 50 milioni', '500');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
