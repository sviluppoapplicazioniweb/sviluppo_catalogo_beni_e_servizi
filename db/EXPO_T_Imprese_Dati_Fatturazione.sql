-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Dic 03, 2013 alle 08:45
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_T_Imprese_Dati_Fatturazione`
--

CREATE TABLE IF NOT EXISTS `EXPO_T_Imprese_Dati_Fatturazione` (
  `Id_Impresa` int(11) NOT NULL,
  `Denominazione` varchar(500) NOT NULL,
  `CodFiscale` varchar(32) NOT NULL,
  `PIVA` varchar(11) NOT NULL,
  `Via` varchar(250) NOT NULL,
  `CAP` varchar(10) NOT NULL,
  `PRV` varchar(5) NOT NULL,
  `Comune` varchar(255) NOT NULL,
  PRIMARY KEY (`Id_Impresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
