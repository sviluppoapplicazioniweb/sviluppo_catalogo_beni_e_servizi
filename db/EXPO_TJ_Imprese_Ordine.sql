-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 22, 2013 alle 09:24
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_Imprese_Ordine`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Ordine` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Result` varchar(20) NOT NULL,
  `MerchantOrderId` varchar(50) NOT NULL,
  `PaymentId` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Authorizationcode` varchar(6) NOT NULL,
  `DataRichiesta` datetime NOT NULL,
  `Amount` text NOT NULL,
  `IdTransazione` varchar(200) NOT NULL,
  `Stato` int(11) NOT NULL COMMENT '2 = completato; 1 in attesa; ',
  `SecurityToken` varchar(50) NOT NULL,
  `Hostedpageurl` varchar(300) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
