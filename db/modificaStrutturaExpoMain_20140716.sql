CREATE TABLE EXPO_T_EXT_Lista_Imprese (
  Id int(11) NOT NULL,
  CodiceFiscale varchar(16) DEFAULT NULL,
  RagioneSociale varchar(255) DEFAULT NULL,
  IdOrdineProfessionale int(11) NOT NULL DEFAULT '0',
  IscrizioneSenzaPagamento varchar(1) DEFAULT 'N',
  IscrizioneConPagamento varchar(1) DEFAULT 'N',
  IdUtente int(11) DEFAULT '0',
  note varchar(255) DEFAULT NULL,
  PRIMARY KEY (Id),
  UNIQUE KEY Id (Id),
  UNIQUE KEY CodiceFiscale (CodiceFiscale)
);



CREATE TABLE EXPO_T_EXT_Lista_LegaliRappresentanti (
  Id int(11) NOT NULL,
  Nome varchar(50) DEFAULT NULL,
  Cognome varchar(50) DEFAULT NULL,
  Email varchar(100) DEFAULT NULL,
  CodiceFiscale varchar(16) DEFAULT NULL,
  Stato varchar(1) DEFAULT 'N',
  IdUtente int(11) DEFAULT '0',
  cciaa varchar(2) DEFAULT NULL,
  note varchar(255) DEFAULT NULL,
  PRIMARY KEY (Id),
  UNIQUE KEY Id (Id),
  UNIQUE KEY CodiceFiscale (CodiceFiscale)
);
