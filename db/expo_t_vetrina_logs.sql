﻿# Host: vlximidb13.mi.cciaa.net  (Version: 5.1.48-log)
# Date: 2013-11-15 11:45:46
# Generator: MySQL-Front 5.3  (Build 4.57)

/*!40101 SET NAMES utf8 */;

#
# Structure for table expo_t_vetrina_logs
#

CREATE TABLE EXPO_T_Vetrina_Logs (
  Id int(11) NOT NULL AUTO_INCREMENT,
  Data datetime NOT NULL DEFAULT '2013-01-01 00:00:00',
  Azione varchar(10) NOT NULL DEFAULT '''''',
  Parametri varchar(255) NOT NULL DEFAULT '''''',
  Response text,
  PRIMARY KEY (Id)
);

#
# Data for table expo_t_vetrina_logs
#

INSERT INTO EXPO_T_Vetrina_Logs VALUES (1,'2013-11-15 11:31:35','Login PDMS','username=francesco.spagnoli&cyph=721a36dfd9a63cf09e0aff5d5fff4234','Prova'),(2,'2013-11-15 11:39:03','Login PDMS','username=francesco.spagnoli&cyph=721a36dfd9a63cf09e0aff5d5fff4234','Prova');
