-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Dic 02, 2013 alle 12:33
-- Versione del server: 5.5.16
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo_main`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `EXPO_TJ_FasciaFatturato_Campi`
--

CREATE TABLE IF NOT EXISTS `EXPO_TJ_FasciaFatturato_Campi` (
  `Id` int(11) NOT NULL,
  `IdFascia` int(11) NOT NULL,
  `Campo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_TJ_FasciaFatturato_Campi`
--

INSERT INTO `EXPO_TJ_FasciaFatturato_Campi` (`Id`, `IdFascia`, `Campo`) VALUES
(1, 1, 'retiImpresaF'),
(2, 1, 'certificazioneF'),
(3, 1, 'certificazioniF'),
(4, 3, 'descrizioneF'),
(4, 2, 'bilanciF'),
(5, 2, 'internazionalizzazioneF'),
(6, 2, 'referenzeF'),
(7, 1, 'brevettiF'),
(8, 2, 'aspettiF'),
(9, 3, 'prodottoF'),
(10, 1, 'default');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
