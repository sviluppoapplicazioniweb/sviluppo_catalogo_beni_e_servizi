﻿ALTER TABLE FAQ_T_Faq ADD Domanda_AL2 text;
ALTER TABLE FAQ_T_Faq ADD Risposta_AL2 text;
ALTER TABLE FAQ_T_Faq ADD Stato_Record char(1) default 'N';
ALTER TABLE FAQ_T_Faq ADD IdTipoUtente int;
ALTER TABLE FAQ_Tlk_Categorie_Faq ADD Descrizione_AL2 text;
ALTER TABLE FAQ_Tlk_Sottocategorie_Faq ADD Descrizione_AL2 text;
