ALTER TABLE  `EXPO_T_Imprese` ADD  `FasciaFatturato` INT NOT NULL;


CREATE TABLE IF NOT EXISTS `EXPO_TJ_Imprese_Pagamenti` (
  `Id` int(11) NOT NULL,
  `IdImpresa` int(11) NOT NULL,
  `Result` varchar(20) NOT NULL,
  `MerchantOrderId` varchar(50) NOT NULL,
  `PaymentId` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Authorizationcode` varchar(6) NOT NULL,
  `DataRichiesta` datetime NOT NULL,
  `Amount` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `EXPO_T_FascieFatturato` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Descrizione` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `EXPO_T_FascieFatturato`
--

INSERT INTO `EXPO_T_FascieFatturato` (`Id`, `Nome`, `Descrizione`) VALUES
(1, 'Micro impresa', 'Fatturato 0-2 milione euro'),
(2, 'Piccole-medie imprese', 'Fatturato compreso tra 2 e 50 milioni'),
(3, 'Grandi imprese', 'Fatturato maggiore di 50 milioni');
