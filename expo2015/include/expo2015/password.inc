<script type="javascript">

function validate_Validator(f) {
if (f.E_Mail.value.length < 1) {
   alert("E_Mail errato o mancante.");
   f.E_Mail.focus();
   return(false);
}
if (emailCheck(f.E_Mail.value,1) == false) {
   alert("L'indirizzo E_Mail è errato.");
   f.E_Mail.focus();
   return(false);
}
if (!confirm('Confermi l\'operazione?')) return (false);
return true;
}

function validate_Email(f) {
	if (emailCheck(f.E_Mail.value,1) == false) {
	   alert("L'indirizzo E_Mail è errato.");
	   f.E_Mail.focus();
	   return(false);
	}
return true;
}

</script>

<?php
include_once "configuration.inc";
include_once "send_mail.inc";

include_once $PROGETTO."/view/lib/passwd_function.inc";
include_once $PROGETTO."/view/lib/functions.inc";
include_once $PROGETTO."/view/languages/IT.inc";

$db = new DB_CedCamCMS;

$testo="";

if(isset($modulo))
{
	
	//HO GIA INSERITO LA MAIL
	$SQL="select * from ".$Esterno[$TAB_ANAGRAFICA].$TAB_ANAGRAFICA." where E_Mail='".$E_Mail."' and Stato_Record='A' order by E_Mail";
	//echo "SQl: ".$SQL;
	
	if ($DEBUG) echo "<!-- SQL ".$SQL." -->\n";
	if ($db->query($SQL))
	{
		if ($db->next_record() )
		{
			$trovati=$db->num_rows();
			if ($trovati==1)
			{
				//HO TROVATO UN SOLO UTENTE ED E' ATTIVO
				if(setToken($db->f('Id')))
					echo "<p>Gentile utente,<br/>abbiamo inviato una e-mail all'indirizzo ". $E_Mail.".<br/>Controlla la tua casella di posta e segui le istruzioni riportate nel messaggio per recuperare/modificare la tua password. </p>";
				//$testo .= "La vostra chiave di accesso:" ."\n";
				//$testo .= "username: ". $db->f(Login)."\n";
				//$testo .= "password: ". $db->f(Password)."\n";

				//include "send_mail.inc";
				//send_mail($E_Mail, $MAILSITO, "Dati accesso", $testo);
				

				//echo "<p>Riceverai una e-mail con la tua login e password </p>";
			}
			else
			{
				// HO TROVATO PIU UTENTI ATTIVI
				echo "<p>Sono stati trovati piu utenti con l'email indicata: <strong>". $E_Mail."</strong><br /><br />";
				echo '<a draggable="false" title="Assistenza" href="/index.phtml?Id_VMenu=501" target="_blank">Contattaci</strong></a> specificando i tuoi dati anagrafici</a>';
				//echo "<a href=\"mailto:".$MAILASSISTENZA."\"><strong>Contattaci</strong></a> specificando i tuoi dati anagrafici</p>\n";
			}

		}
		else
		{
			//NON HO TROVATO UNTENTI ATTIVI
			//VERIFICO SE ESISTONO UTENTI DISATTIVI
			//echo "Entrato";
			//exit;
			$sql="SELECT Id, Cognome, Nome, Login, Password, E_Mail from T_Anagrafica where E_Mail='".$E_Mail."' and Stato_Record='D' order by E_Mail";
			$db->query($sql);
			$trovati=$db->num_rows();	
			if ($trovati==1)
			{
				//HO TROVATO UN UTENTE DISATTIVATO
				$db->next_record();
				$pass = passwordCasuale(8);
				//echo "Pass: ".$pass.'<br/>';
				//exit;
				$passUrl = urlencode(base64_encode($pass));
				
				$utente = $db->f('Nome') . ' ' . $db->f('Cognome');
				$link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $db->f('E_Mail') . "&cyph=" . md5($db->f('E_Mail') . $keySito) . "&Lang2=IT&action=INS";
				//echo "Link:".$link;
				//exit;
				$db->query("UPDATE T_Anagrafica SET Password='".$pass."' WHERE E_Mail='".$E_Mail."'");
				//Loggo il link di attivazione
				$qryLog = "INSERT INTO `EXPO_T_Catalogo_Logs` (`data`,`azione`,`parametri`,`response`) VALUES ('".date('Y-m-d H:i:s')."','Attivazione LR: ".$db->f('Codice_Fiscale')."','".$link."','OK');";
				//echo "Qry: ".$qryLog;
				//exit;
				$db->query($qryLog);
				send_mail($db->f('E_Mail'), $MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true);
				echo "<h2>Mail di attivazione inviata!</h2><p><strong>ATTENZIONE! La tua utenza risulta registrata ma non è stata eseguita la procedura di attivazione.</strong> <br/>Abbiamo inviato una e-mail alla casella di posta da te indicata: occorre semplicemente seguire le istruzioni contenute  in questa e-mail.</p>";
				//echo '<a href="/index.phtml?Id_VMenu=11">Torna a Lista Legali Rappresentanti</a>';
				exit;
				//$db->next_record();
				//$cqs=base64_encode("Id_Utente=".$db->f("Id"));
				//$link_attiva="<a href=\"".$HOMEURL."index.phtml?pagina=attiva&cqs=".$cqs."\">Attiva Registrazione</a>";
				
				/*$link_attiva="<a href=\"".$HOMEURL."index.phtml?Id_VMenu=70&cqs=".$cqs."\">Attiva Registrazione</a>";
				$SQL="Select * from T_MailGeneriche where Id_Mail=2";
				if ($db->query($SQL))
				{
					$db->next_record();
					$oggetto_mail=$db->f("Oggetto_Mail");
					$testo_mail=$db->f("Testo_Mail");
				}
				$sito=$NOMESITO;
				$firma=$FIRMAMAIL;

				$oggetto_mail=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$oggetto_mail);
				$testo_mail=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$testo_mail);

				send_mail_html($E_Mail,$MAILSITO,$oggetto_mail,$testo_mail);
				
				echo "<p><strong>ATTENZIONE!La tua utenza risulta registrata ma non è stata eseguita la procedura di attivazione.</strong> <br/>Abbiamo inviato una e-mail alla casella di posta da te indicata: occorre semplicemente seguire le istruzioni contenute  in questa e-mail.</p>\n";*/
			}
			else
				//NON HO TROVTO NEANCHE UN UTENTE DISATTIVATO, QUINDI NON HO TROVATO NESSUN UTENTE - tuttavia dico che ho inviato i dati alla mail
				echo "<p>Gentile utente,<br/>ti abbiamo inviato una e-mail all'indirizzo ". $E_Mail.".<br/>Controlla la tua casella di posta e segui le istruzioni riportate nel messaggio per recuperare/modificare la tua password. </p>";
				
		}
			
	}
	else
		if ($DEBUG) echo "Errore SQL: ".$SQL."\n";	
}
else
{
	echo "<form name=\"modulo\" action=\"/index.phtml?pagina=password\" method=\"post\" onsubmit=\"return validate_Validator(this)\">\n";
	echo "<div class=\"box2formEmail\">";
	echo "<div class=\"form_label\">";
	echo "<label for=\"posta\">"._EMAIL_PASSWORD_."</label>";
	echo "</div>";
	echo "<div>";
	echo "<input type=\"text\" name=\"E_Mail\" id=\"E_Mail\" size=\"50\" maxlength=\"250\" />\n";
	echo "</div>";
	echo "<br style=\"clear: left;\">";
	echo "</div>";
        echo "<div id=\"footerForm\">\n";
        echo "<input type=\"submit\" name=\"modulo\" id=\"modulo\" value=\""._INVIA_."\" />\n";
	echo "</div>\n";
	echo "</form>\n";
        	
}


		
?>