<div class="paginazione">
<?php
if($nome == 'FAQ_T_Faq') {
    
    if($numeroRighe>0)
        
            echo "<br>"._FAQ_TOT_QUERY_.$numeroRighe;
    
    if($utype != 1 && $utype != 95)
        echo "<br><br><br><strong>"._FAQ_MSG_ELENCO_."</strong>\n";      
    
} elseif ($nome == 'FAQ_Tlk_Categorie_Faq') {
    
    if($numeroRighe>0)
        echo "<br>"._CATEGORIA_FAQ_TOT_QUERY_.$numeroRighe;  
    
} elseif ($nome == 'FAQ_Tlk_Sottocategorie_Faq') {
    
    if($numeroRighe>0)
        echo "<br>"._SOTTOCATEGORIA_FAQ_TOT_QUERY_.$numeroRighe;      
    
} else {

    if($estratti>0)
            echo "Totale estratti: ".$estratti."<br>\n";    
    
}


if($page_tot>1)
{
	echo "Totale Pagine: ".$page_tot."<br>\n";
	if($p>=$ELEPAGE)
	{
		$linkinizio=" <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linkinizio.="&amp;p=1";
		if ($ordina!="")
			$linkinizio.="&amp;ordina=".$ordina;
		else
			$linkinizio.="&amp;ordina=".$ordina1;
		if ($Id_TipoUtente!="")
			$linkinizio.="&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linkinizio.="\">Inizio</a>\n";

		if((((floor($p/$ELEPAGE))*$ELEPAGE)-$ELEPAGE)>0)
			$paginiz=(((floor($p/$ELEPAGE))*$ELEPAGE)-$ELEPAGE);
		else
			$paginiz=1;
		$linkprec= " <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linkprec.="&amp;p=".$paginiz;
		if ($ordina!="")
			$linkprec.="&amp;ordina=".$ordina;
		else
			$linkprec.="&amp;ordina=".$ordina1;
		if ($Id_TipoUtente!="")
			$linkprec.="&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linkprec.="\">Precedenti</a>\n";
	}
	$part_cont=floor($p/$ELEPAGE)*$ELEPAGE;

	$fin_cont=$part_cont+($ELEPAGE - 1);
	if($part_cont==0)
		$part_cont=1;	
	if($p>1)
	{
		$linkprima= " <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linkprima.="&amp;p=".($p-1);
		if ($ordina!="")
			$linkprima.= "&amp;ordina=".$ordina;
		else
			$linkprima.= "&amp;ordina=".$ordina1;
		if ($Id_TipoUtente!="")
			$linkprima.= "&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linkprima.= "\">&lt;&lt; </a>\n";
	}
	$page_link="";
	for ($page=$part_cont;$page<=$fin_cont;$page++)
	{
		if($page>$page_tot)
			break;

		if($page==$p)
			$page_link.= "<span class=\"pcorrente\">".$page."</span>\n";
		else
		{
			$page_link.= " <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
			$page_link.="&amp;p=".$page;
			if ($ordina!="")
				$page_link.= "&amp;ordina=".$ordina;
			else
				$page_link.= "&amp;ordina=".$ordina1;
			if ($Id_TipoUtente!="")
				$page_link.= "&amp;Id_TipoUtente=".$Id_TipoUtente;
			$page_link.= "\">".$page."</a>\n";
		}
	}
	if($p<$page_tot)
	{
		$linkdopo=" <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linkdopo.= "&amp;p=".($p+1);
		if ($ordina!="")
			$linkdopo.= "&amp;ordina=".$ordina;
		else
			$linkdopo.= "&amp;ordina=".$ordina1;

		if ($Id_TipoUtente!="")
			$linkdopo.= "&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linkdopo.= "\"> &gt;&gt;</a>\n";
	}
	if($fin_cont<$page_tot)
	{
		$linksucc=" <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linksucc.="&amp;p=".(((floor($p/$ELEPAGE))*$ELEPAGE)+$ELEPAGE);
		//$linkprec.= "&amp;p=".((floor($p/$ELEPAGE))*$ELEPAGE)-$ELEPAGE;

		if ($ordina!="")
			$linksucc.="&amp;ordina=".$ordina;
		else
			$linksucc.="&amp;ordina=".$ordina1;
		if ($Id_TipoUtente!="")
			$linksucc.="&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linksucc.="\">Successivi</a>\n";

		$linkfine="  <a href=\"/index.phtml?Id_VMenu=".$Id_VMenu;
		$linkfine.="&amp;p=".$page_tot;
		if ($ordina!="")
			$linkfine.="&amp;ordina=".$ordina;
		else
			$linkfine.="&amp;ordina=".$ordina1;
		if ($Id_TipoUtente!="")
			$linkfine.="&amp;Id_TipoUtente=".$Id_TipoUtente;
		$linkfine.="\">Ultimo</a>\n";
	}
	echo $linkinizio.$linkprec.$linkprima.$page_link.$linkdopo.$linksucc.$linkfine;
	//FINE GESTIONE GOOGLE
}
?>
</div>