<?php
//Lang.inc
/*
$NomeTabella["NomeCampo|TipoUtenti|Azione"] = "Label|VisTabella(S/N)|Obbligatorio(S/N)|Tabella Esterna|TipoCampo|Param1|Param2|...";

Parametri per TipoCampo

TEXT|SIZE|MAXLENGTH
PASSWORD|SIZE|MAXLENGTH
HIDDEN|SIZE|MAXLENGTH
TEXTAREA|ROWS|COLS
HTMLAREA|ROWS|COLS
HTML|ROWS|COLS
BUTTON
SELECT|TABELLA|SIZE|MULTIPLE(S/N)|TABELLA2(MULTIPLE)|FILTRO (mettere campo di confronto + valore separato da ;)
SELECTFILTRO|TABELLA|SIZE|CAMPO|FILTROWHERE
SELECTREFRESH|TABELLA|SIZE
SELECTHIDDEN|TABELLA|TABELLA|CAMPOCHIAVE|FILTROWHERE Cosi fà vedere la descrizione
SELECTHIDDEN2|TABELLA|CAMPOCHIAVE|FILTROWHERE|VALORERITORNO Non fa vedere niente ma ha un campo hidden nel form
RADIO|VALORE1|VALORE2|VALORE3|...
CHECKBOX|VALORE|CHECKED(S/N)
FILE|SIZE|MAXFILESIZE
LISTA|TIPOVISUAL(POPUP/INTERNO)
HTMLCONDIZ|TABELLA|WHERE(separati da ; campo=valore|CONDIZIONE separati da ;campo e valore|TESTOHTML
Parametri Tabella Esterna
Nome Tabella Esterna;ForeignKey;[CampoWhere;Valore];[...];[...]
Quelli tra parentesi quadre vanno sempre in coppia e sono opzionali

Parametri Tipo Utenti
ogni tipo utente sarà scritto cosi:
;1;3;4;
Quindi il punto e virgola andrà prima e dopo il numero.

Array Location
$location[Nome Tabella][azione][Tipo Utenti]=percorso|parametro
il nome del parametro é uguale alla variabile presente in moduli.phtml

Array Filtri
$Filtri[Tabella][Label]=[Tabella dei dati da mettere nella select option][campi da filtrare sulla tabella principale separati da punto e virgola]
$FiltriText["BW_T_Prezzi"]["Versione"]="||Id_Versione|30|100|HIDDEN";
*/
//$tipo_venti=20;
//echo "utype - ".$utype;
//echo "Id_TipoUtente - ".$Id_TipoUtente;
//echo "Id_VMenu - ".$Id_VMenu;
/*if ($Id_VMenu==2)
	$utype = 3;
else
	$utype = 98;*/

$condizLink_Strutture1="|4|:1";
$condizLink_Strutture2="|4|:2";
$condizLink_Strutture3="|4|:3";
$condizLink_Strutture4="|4|:4";
$condizLink_PCO="|5|:1";
$condizLink_DMC="|9|:2";

$Azioni["T_Anagrafica"]["1"]="|Elimina|InviaMailAttiv|Attiva|";
$Azioni["T_Anagrafica"]["2"]="|Elimina";
$Azioni["T_Anagrafica"]["15"]="|Attiva";

//$Filtri["T_Anagrafica"]["Tipo Utente1"]="||N|||||||(Id_TipoUtente>{%utype%} or (Id_TipoUtente>={%utype%} and '1'='{%mastereditor%}'))";
//$Filtri["T_Anagrafica"]["Tipo Utente"]="||||Tlk_TipoUtente|Id_TipoUtente|Id|Descrizione|Id|(Id>{%utype%} or (Id>={%utype%} and '1'='{%mastereditor%}'))";
$FiltriText["T_Anagrafica"]["Cognome"]="||Cognome|30|50|TEXT";
$FiltriText["T_Anagrafica"]["Nome"]="||Nome|30|50|TEXT";
$FiltriText["T_Anagrafica"]["Cod.Fisc"]="||Codice_Fiscale|30|50|TEXT";
$FiltriText["T_Anagrafica"]["E Mail"]="||E_mail|30|100|TEXT";

$Elimina["T_Anagrafica"]["1"]="";
$Elimina["T_Anagrafica"]["2"]="";
$Elimina["T_Anagrafica"]["15"]="";
$Chiave["T_Anagrafica"]="Id";

$Location["T_Anagrafica"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Anagrafica&Id_TipoUtente={%Id_TipoUtente%}";
$Location["T_Anagrafica"]["default"]["97"] = "/index.phtml?tmpl=2&pagina=registrazione&Id_Utente={%Id_Utente%}&azione={%azione%}";
$Location["T_Anagrafica"]["default"]["98"] = "/index.phtml?tmpl=2&pagina=registrazione&Id_Utente={%Id_Utente%}&azione={%azione%}";
$Location["T_Anagrafica"]["default"]["99"] = "/index.phtml?tmpl=2&pagina=registrazione&Id_Utente={%Id_Utente%}&azione={%azione%}";
$Location_me["T_Anagrafica"]["UPD"]["1"]["1"] = "/index.phtml";
$Location_me["T_Anagrafica"]["UPD"]["2"]["2"] = "/index.phtml";
$Location_me["T_Anagrafica"]["UPD"]["15"]["15"] = "/index.phtml";

$T_Anagrafica["Id||INS|"]="Id||I";
$T_Anagrafica["Id||UPD|"]="Id||N|||DIS||HIDDEN";
//$T_Anagrafica["Id_TipoUtente|||"]="Tipo Utente||N|N|T_Utente;Id;Id|\$Id!=\$uid||SELECTREFRESH|Tlk_TipoUtente;Id;Descrizione|||||||(Id>{%utype%} or (Id>={%utype%} and '1'='{%mastereditor%}'))";
$T_Anagrafica["Id_TipoUtente||||"]="Tipo Utente||N|N|T_Utente;Id;Id|\$Id==\$uid||HIDDEN";

$_SESSION["uid"] = $uid;
$_SESSION["utype"] = $utype;

//$T_Anagrafica["RagSoc|||"] = "Denominazione||S|S||||TEXT|50|100";
//$T_Anagrafica["Partita_IVA|||;15;97;"] = "CF o P.IVA||N|S||||TEXT|20|100";
$T_Anagrafica["Nome|||"] = "Nome LR &nbsp;||S|S||||TEXT|35|100";
$T_Anagrafica["Cognome|||"] = "Cognome LR &nbsp;||S|S||||TEXT|35|100";
$T_Anagrafica["Codice_Fiscale|||"] = "Cod. Fisc &nbsp;||S|S||||TEXT|20|20";
$T_Anagrafica["Telefono|||"] = "Telefono ||N|N||||TEXT|30|30";
$T_Anagrafica["Cellulare|||"] = "Cellulare &nbsp;||N|N||||TEXT|30|30";
$T_Anagrafica["Fax|||"] = "Fax &nbsp;||N|N||||TEXT|30|30";
$T_Anagrafica["E_Mail|||"] = "E-Mail LR &nbsp;||S|S||DIS||TEXT|50|100";
//$T_Anagrafica["E_Mail|;97;98;|UPD|;97;98;"] = "E-Mail||S|N||DIS||TEXT|40|100";
//$T_Anagrafica["Password|||"] = "Password (min. 8 caratt.)||N|S||||PASSWORD|15|12||8";
//$T_Anagrafica["Rep_Password||INS|"] = "Reinserisci Password||N|SNM||||PASSWORD|15|12||8";
//$T_Anagrafica["Rep_Password||UPD|"] = "Reinserisci Password||N|SNM||||PASSWORD|15|12|Password|8";

if($my_appl>0)
{
	$file = $PROGETTO."/langappl/lang_anagappl_".$my_appl.".inc";
	if (file_exists($PATHINC.$file))
		include $file;
}
elseif(is_object($auth) && $auth->auth[utype]==98)
{
	$myapplanag=explode("|",$sess_anagcompl);
	for($ii=0;$ii<sizeof($myapplanag);$ii++)
	{
		if($myapplanag[$ii]>0)
		{
			$file = $PROGETTO."/langappl/lang_anagappl_".$myapplanag[$ii].".inc";
			if (file_exists($PATHINC.$file))
				include $file;
		}
	}
}

/*$T_Anagrafica["okurl_site|||"] = "url sito||N|N|T_Utente;Id;Id|||HIDDEN|okurl_site";
$T_Anagrafica["Data_Attivazione|;1;2;3;15;97;|UPD|;20;"] = "Attivazione in data|||NNM|T_Utente;Id;Id|\$Data_Attivazione!='' && \$Data_Attivazione!='0000-00-00 00:00:00';DIS||DATAITA|15|15";
$T_Anagrafica["Data_Registrazione||INS|"] = "Data Registrazione||N|N||N||DATA";
$T_Anagrafica["Stato_Record|;1;2;15;|INS|;2;15;"] = "Stato record||N|N||||HIDDEN|ATTIVA";
$T_Anagrafica["MasterEditor|;1;||;2;"]="Master Editor||N|N|T_Utente;Id;Id|||RADIO|Si;No|1;0||0|S";
$T_Anagrafica["Abilitazioni|;1;2;||;2;3;15;"] = "Abilitazioni||F|N|T_Utente;Id;Id|\$utype==1 or \$mastereditor==1||CHECKBOX|SVIL_T_Menu|Id_Menu|Titolo|(Id_Menu like '__' or Id_Menu like '__.__' or Id_Menu like '01.__.__') and Id_Menu<'80' and (Tipo='M' or Id_Menu='00' or Id_Menu like '01.__')||1|MENU";

if ($Id_VMenu == '2'){
	$T_Anagrafica["regolamento|||;97;"]="Dati Anagrafici||N|N||||LABEL|";
	$T_Anagrafica["Check_Privacy||INS|"] = "||N|S|T_Utente;Id;Id||SPAN|CHECKBOX||1|*  Dichiaro, sotto la mia responsabilita' e consapevole delle sanzioni penali previste dall'articolo 76 del D.P.R. 28.12.2000, n. 445 nei confronti di chi attesta il falso, di aver letto attentamente quali sono gli standard qualitativi minimi per poter essere inserito nel sito e di esserne in possesso.|||";
}

//$T_Anagrafica["privacy|||;97;98;"]="Dati Anagrafici||N|N||||LABEL|";
//$T_Anagrafica["Check_Privacy2||INS|"] = "||N|S|T_Utente;Id;Id||SPAN|CHECKBOX||1|*  Acconsento al trattamento dei dati personali per gli scopi specifici di questo sito ai sensi di quanto disposto dal D.Lgs.196/2003 - Codice in materia di protezione dei dati personali.|||";
//$T_Anagrafica["obbligatori|||"]="<p><strong>* I campi contrassegnati dall'asterisco sono obbligatori.</strong></p>||N|N||||LABEL2|left";
//$msglbl='<p>Ti ricordiamo che l\'iscrizione al Catalogo Expo non comporta l\'iscrizione automatica al Registro dei Fornitori Expo2015, che pu&ograve; essere effettuata <a href="http://www.expo2015.org/appalti-e-gare">qu&igrave;</a></p>';
//$T_Anagrafica["lblRegolameno|||"]="$msglbl||N|N||||LABEL2|left";*/
$T_Anagrafica["Pulsante|||"] = "Salva||N|N||||BUTTON|CENTER";

//Anagrafica aggiuntiva per Imprese 
$T_Ext_Anagrafica["mialabel|;97;|INS||"] = "<br /><strong>Gentile utente per accedere all'inserimento Materiali On Line &egrave; necessario fornire i seguenti ulteriori dati anagrafici</strong><br />||N|N||\$my_appl>0||LABEL|CENTER";
$Chiave["T_Ext_Anagrafica"]="Id";

$Permessi["T_Ext_Anagrafica"]=";1;2;3;15;97;";
$T_Ext_Anagrafica["Id||INS||"] = "Id||N|N||||HIDDEN|uid|";
$T_Ext_Anagrafica["Id||UPD|"]="Id||H";
$T_Ext_Anagrafica["id_TipoPacchetto|;97;|INS||"] = "Tipo Pacchetto||N|S||||SELECTFILTRO|SIEXPO_Tlk_Pacchetti;Id;Descrizione|1||";
$T_Ext_Anagrafica["id_TipoPacchetto|;1;2;3;15;|UPD||"] = "Tipo Pacchetto||N|S||||SELECTFILTRO|SIEXPO_Tlk_Pacchetti;Id;Descrizione|1||";
$T_Ext_Anagrafica["URLSito|;1;2;3;15;97;|||"] = "Sito Web||S|N||||TEXTAREA|4|80";
$T_Ext_Anagrafica["LegR_Cognome|;1;2;3;15;97;|||"] = "Legale Rappr. Cognome||S|S||||TEXT|30|50|";
$T_Ext_Anagrafica["LegR_Nome|;1;2;3;15;97;|||"] = "Legale Rappr. Nome||S|S||||TEXT|30|50";
$T_Ext_Anagrafica["CertAmbDesc|;1;2;3;15;97;|||"] = "Certificazioni ambientali dell'Azienda||S|N||||TEXTAREA|4|80";
$T_Ext_Anagrafica["CertAmbAlleg|;1;2;3;15;97;|||"] = "Certificazioni ambientali dell'Azienda (PDF)||N|N||||FILE|30|100|12288|files/imprese/$uid|N||A|application/pdf|CertAmb|";
$T_Ext_Anagrafica["EnergRinnovDesc|;1;2;3;15;97;|||"] = "Utilizzo energie rinnovabili||S|N||||TEXTAREA|4|80";
$T_Ext_Anagrafica["EnergRinnovAlleg|;1;2;3;15;97;|||"] = "Utilizzo energie rinnovabili (PDF)||N|N||||FILE|30|100|12288|files/imprese/$uid|N||A|application/pdf|EnergRinnov|";
$T_Ext_Anagrafica["Profilo|;1;2;3;15;97;|||"] = "Profilo Impresa||S|N||||TEXTAREA|5|80";
$T_Ext_Anagrafica["Logo|;1;2;3;15;97;|||"] = "Logo||N|N||||FILE|30|100|12288|files/imprese/$Id|N||A|image/jpeg;image/png|Logo|";
/* Alessandro Ferla - 23/01/2013 
   Il tipo file (mime type) es: 'application/pdf' o "T" per tutti.
   E' Possibile inserire più tipi files separando i mime type con il simbolo ; es: application/pdf;application/zip
*/	
$T_Ext_Anagrafica["sep01|;1;2;3;15;97;|||"] = "Fax||N|N||||SEPARATORE";
// Alessandro Ferla 23/01/2013
// Modificata gestione per consentire la scelta di fare il display dei messaggi sul salvataggio dei dati
// Se nella configurazione del pulsante compare DIS allora il messaggio comparir࠳otto forma di msgbox Javascript 
$T_Ext_Anagrafica["Pulsante|;1;2;3;15;97;|||"] = "Salva||N|N||DIS||BUTTON|CENTER|S";

$Chiave["Tlk_TipologiaUtente"]="Id";
$Permessi["Tlk_TipologiaUtente"]=";1;2;";
$Tlk_TipologiaUtente["Id|||"] = "Id||I";
$Tlk_TipologiaUtente["Descrizione|||"] = "Descrizione||S|S||||TEXT|20|50";

$Chiave["T_Privacy"]="Id";
$Permessi["T_Privacy"]=";1;2;";
$Inserimento["T_Privacy"]["1"]="N";
$Inserimento["T_Privacy"]["2"]="N";
$Location["T_Privacy"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Privacy";
$T_Privacy["Id|||"] = "Id||I";
$T_Privacy["Descrizione|||"] = "Testo Privacy||S|S||||TEXTAREA|10|50";
$T_Privacy["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";


$Chiave["T_Gruppi"]="Id";
$Location["T_Gruppi"]["default"]["default"] = "/index.phtml?Id_VMenu=27";
$Permessi["T_Gruppi"]=";1;2;3;";
$T_Gruppi["Id|||"] = "Id||I";
$T_Gruppi["Id_Gruppo|||"] = "Gruppo||N|N||||HIDDEN";
$T_Gruppi["Descrizione|||"] = "Descrizione||S|N||||TEXT|40|100";
$T_Gruppi["Pulsante|||"] = "Salva||N|N||||BUTTON|CENTER";

$Chiave["EXPO_Tlk_Categorie"]="Id";
$Location["EXPO_Tlk_Categorie"]["default"]["default"] = "/index.phtml?Id_VMenu=82";
$Permessi["EXPO_Tlk_Categorie"]=";1;2;3;15;";
$EXPO_Tlk_Categorie["Id|||"] = "Id||I";
$EXPO_Tlk_Categorie["Id_Categoria|||"] = "Categoria||N|N||||HIDDEN";
$EXPO_Tlk_Categorie["Descrizione|||"] = "Descrizione||S|S||||TEXT|100|255";
$EXPO_Tlk_Categorie["Descrizione_AL|||"] = "Descrizione (Inglese)||S|S||||TEXT|100|255";
$EXPO_Tlk_Categorie["Descrizione_AL2|||"] = "Descrizione (Francese)||S|S||||TEXT|100|255";
$EXPO_Tlk_Categorie["Stato_Record|||"]="Abilitata||S|S||||RADIO|Si;No|Y;N|Y|N";
$EXPO_Tlk_Categorie["Pulsante|||"] = "Salva||N|N||||BUTTON|CENTER";



//-----------------------------------------------------------------
//								TABELLE STANDARD
//-----------------------------------------------------------------

$Chiave["Tlk_Province"]="Id";
$Permessi["Tlk_Province"]=";1;2;";
$Location["Tlk_Province"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_Province";
$Tlk_Province["Province|||"]="Gestione Province||N|N||||LABEL|CENTER";
$Tlk_Province["Id|||"] = "Id||I";
$Tlk_Province["Descrizione|||"] = "Descrizione||S|S||||TEXT|20|50";
$Tlk_Province["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tlk_Comuni"]="Id";
$Permessi["Tlk_Comuni"]=";1;2;";
$Filtri["Tlk_Comuni"]["Provincia"]="||||Tlk_Province|Id_Provincia|Id|Descrizione";
$Location["Tlk_Comuni"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_Comuni";
$Tlk_Comuni["Comuni|||"]="Gestione Comuni||N|N||||LABEL|CENTER";
$Tlk_Comuni["Id|||"] = "Id|I";
$Tlk_Comuni["Descrizione|||"] = "Descrizione||S|S||||TEXT|20|50";
$Tlk_Comuni["Id_Provincia|||"] = "Sigla||S|S||||SELECTFILTRO|Tlk_Province;Id;Sigla|1||";
$Tlk_Comuni["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";


//-----------------------------------------------------------------
//									TABELLE CMS
//-----------------------------------------------------------------

include $PROGETTO."/lang_CMS".$GESTIONE_SITO.".inc";
//include $PROGETTO."/lang_imprese.inc";

$Chiave["Tlk_TipiHtml"]="Id";
$Inserimento["Tlk_TipiHtml"]["2"]="N";
$Permessi["Tlk_TipiHtml"]=";1;2;";
//$Azioni["Tlk_TipiHtml"]["1"]="|Elimina";
$Location["Tlk_TipiHtml"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_TipiHtml";
//$Tlk_TipiHtml["TipoOggetti|||"]="Tipo Oggetti HTML||N|N||||LABEL|CENTER";
$Tlk_TipiHtml["Id|||"] = "Id||I";
$Tlk_TipiHtml["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|50";
$Tlk_TipiHtml["Testo_TipiHtml|;1;||"] = "Html||N|S||||HTMLAREA|10|60";
$Tlk_TipiHtml["css|;1;||"] = "CSS||S|N||||TEXT|10|20";
$Tlk_TipiHtml["Attivo|||"]="Attivo per||S|N||||RADIO|Tutti;Editor;Nessuno|2;1;0|1|0";
$Tlk_TipiHtml["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tlk_TipoArticolo1"]="Id";
$Permessi["Tlk_TipoArticolo1"]=";1;2;";
$Azioni["Tlk_TipoArticolo1"]["1"]="|Elimina";
$Azioni["Tlk_TipoArticolo1"]["2"]="|Elimina";
$Location["Tlk_TipoArticolo1"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo1";
$Tlk_TipoArticolo1["Id|||"] = "Id||I";
$Tlk_TipoArticolo1["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|100";
$Tlk_TipoArticolo1["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tlk_TipoArticolo2"]="Id";
$Permessi["Tlk_TipoArticolo2"]=";1;2;";
$Azioni["Tlk_TipoArticolo2"]["1"]="|Elimina";
$Azioni["Tlk_TipoArticolo2"]["2"]="|Elimina";
$Location["Tlk_TipoArticolo2"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo2";
$Tlk_TipoArticolo2["Id|||"] = "Id||I";
$Tlk_TipoArticolo2["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|100";
$Tlk_TipoArticolo2["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tlk_TipoArticolo3"]="Id";
$Permessi["Tlk_TipoArticolo3"]=";1;2;";
$Azioni["Tlk_TipoArticolo3"]["1"]="|Elimina";
$Azioni["Tlk_TipoArticolo3"]["2"]="|Elimina";
$Location["Tlk_TipoArticolo3"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_TipoArticolo3";
$Tlk_TipoArticolo3["Id|||"] = "Id||I";
$Tlk_TipoArticolo3["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|100";
$Tlk_TipoArticolo3["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Pagine"]="Id";
$Permessi["T_Pagine"]=";1;2;";
$Azioni["T_Pagine"]["1"]="|Elimina";
$Azioni["T_Pagine"]["2"]="|Elimina";
$Ordina["T_Pagine"]="Descrizione";
$Location["T_Pagine"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Pagine";
//$T_Pagine["Pagine|||"]="Gestione pagine||N|N||||LABEL|CENTER";
$T_Pagine["Id|||"] = "Id||I";
$T_Pagine["Descrizione|||"] = "Descrizione||S|S||||TEXT|20|50";
$T_Pagine["Link|||"] = "Link||N|S||||TEXT|40|100";
$T_Pagine["Flag_Pubblico|||"]="Pubblico?||S|N||||RADIO|Si;No|1;0|0|0";
//$T_Pagine["T_Pagine_AL2||UPD|"] = "Altre lingue||N|N||||ELENCO|T_Pagine_AL|Id_AL|Id|T_Pagine_AL.Lang_AL|";
$T_Pagine["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Pagine_AL"]="Id_AL";
$Permessi["T_Pagine_AL"]=";1;2;";
$Inserimento["T_Pagine_AL"]["1"]="N";
$Inserimento["T_Pagine_AL"]["2"]="N";
$Azioni["T_Pagine_AL"]["1"]="|Modifica";
$Azioni["T_Pagine_AL"]["2"]="|Modifica";
//$Location["T_Pagine_AL"]["default"]["default"] = "/index.phtml?Id_VMenu=31";
$T_Pagine_AL["Id_AL|||"] = "Id||H";
$T_Pagine_AL["Lang_AL|||"] = "Lingua||S|S||DIS||TEXT|2|2";
$T_Pagine_AL["Descrizione_AL|||"] = "Descrizione||S|S||||TEXT|20|50";
$T_Pagine_AL["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Images"]="Id";
$Files["T_Images"]="images/upload/;Image";
$Permessi["T_Images"]=";1;2;";
$Azioni["T_Images"]["1"]="|Elimina";
$Azioni["T_Images"]["2"]="|Elimina";
$Location["T_Images"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Images";
$T_Images["Immagini|||"]="Gestione Immagini||N|N||||LABEL|CENTER";
$T_Images["Id|||"] = "Id||I";
$T_Images["Image|||"] = "Immagine||S|S||||FILE|30|100|500000|images/upload|N";
$T_Images["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Files"]="Id";
$Files["T_Files"]="files/;File";
$Permessi["T_Files"]=";1;2;";
$Azioni["T_Files"]["1"]="|Elimina";
$Azioni["T_Files"]["2"]="|Elimina";
$Location["T_Files"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Files";
$T_Files["Files|||"]="Gestione Files||N|N||||LABEL|CENTER";
$T_Files["Id|||"] = "Id||I";
$T_Files["File|||"] = "File||S|S||||FILE|30|100|5000000|files|N";
$T_Files["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Banners"]="Id";
$Files["T_Banners"]="images/banners/;Banner";
$Permessi["T_Banners"]=";1;2;";
$Azioni["T_Banners"]["1"]="|Elimina";
$Azioni["T_Banners"]["2"]="|Elimina";
$Location["T_Banners"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Banners";
$T_Banners["Files|||"]="Gestione Banners||N|N||||LABEL|CENTER";
$T_Banners["Id|||"] = "Id||I";
$T_Banners["Banner||INS|"] = "Banner||S|S||||FILE|30|100|50000|images/banners|N";
$T_Banners["Banner||UPD|"] = "Banner||N|S||DIS||TEXT|10|10";
$T_Banners["Link|||"] = "Link||N|N||||TEXT|30|100";
$T_Banners["Alt|||"] = "Testo Alternativo||N|S||||TEXT|30|100";
$T_Banners["Sinistra|||"]="Menù di sinistra||S|N||||RADIO|Si;No|S;N|0|N";
$T_Banners["Destra|||"]="Menù di destra||S|N||||RADIO|Si;No|S;N|0|N";
$T_Banners["Ordine|||"] = "Ordine||S|S||||TEXT|3|3";
$T_Banners["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_News"]="Id";
$Permessi["T_News"]=";1;2;";
$Azioni["T_News"]["1"]="|Elimina";
$Azioni["T_News"]["2"]="|Elimina";
$Location["T_News"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_News";
$T_News["News|||"]="Gestione News||N|N||||LABEL|CENTER";
$T_News["Id|||"] = "Id||I";
$T_News["Data||INS|"] = "||N|N||||DATA|";
$T_News["Data||UPD|"] = "Data||S|N||DIS||TEXT|10|10";
$T_News["Titolo|||"] = "Titolo||S|S||||TEXT|50|250";
$T_News["Testo|||"] = "Testo||N|S||||TEXTAREA|5|40";
$T_News["Link|||"] = "Link||N|N||||TEXT|30|100";
$T_News["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_NewsLetter_Email"]="Id";
$Permessi["T_NewsLetter_Email"]=";1;2;6;";
$Azioni["T_NewsLetter_Email"]["1"]="|Elimina";
$Azioni["T_NewsLetter_Email"]["2"]="|Elimina";
$Azioni["T_NewsLetter_Email"]["6"]="|Elimina";
$Location["T_NewsLetter_Email"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_NewsLetter_Email";
$T_NewsLetter_Email["NewsLetter|||"]="Gestione Email NewsLetter||N|N||||LABEL|CENTER";
$T_NewsLetter_Email["Id|||"] = "Id||I";
$T_NewsLetter_Email["E_Mail|||"] = "Email||S|S||||TEXT|30|50";
$T_NewsLetter_Email["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_NewsLetter"]="Id";
$Permessi["T_NewsLetter"]=";1;2;6;";
$Elimina["T_NewsLetter"]="T_NewsLetter_News;Id_NewsLetter";
$Azioni["T_NewsLetter"]["1"]="|Elimina|Invio NewsLetter";
$Azioni["T_NewsLetter"]["2"]="|Elimina|Invio NewsLetter";
$Azioni["T_NewsLetter"]["6"]="|Elimina|Invio NewsLetter";
$Location["T_NewsLetter"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_NewsLetter";
$T_NewsLetter["Menu|||"]="Gestione NewsLetter||N|N||||LABEL|CENTER";
$T_NewsLetter["Id|||"] = "Id||I";
$T_NewsLetter["Titolo|||"] = "Titolo||S|S||||TEXT|30|100";
$T_NewsLetter["Testo_Html||UPD|"] = "News||N|N|T_NewsLetter_News|||ELENCO|T_NewsLetter_News|Id_NewsLetter|Id|T_NewsLetter_News.Id_News";
$T_NewsLetter["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_NewsLetter_News"]="Id_News";
$Permessi["T_NewsLetter_News"]=";1;2;6;";
$Azioni["T_NewsLetter_News"]["1"]="Id_NewsLetter,Id;Id_News,Id_News|Modifica|Elimina";
$Azioni["T_NewsLetter_News"]["2"]="Id_NewsLetter,Id;Id_News,Id_News|Modifica|Elimina";
$Azioni["T_NewsLetter_News"]["6"]="Id_NewsLetter,Id;Id_News,Id_News|Modifica|Elimina";
$Location["T_NewsLetter_News"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=form&nome=T_NewsLetter&azione=UPD&Id=$Id_NewsLetter";
$T_NewsLetter_News["Label1|||"]="Gestione NewsLetter||N|N||||LABEL|CENTER";
$T_NewsLetter_News["Id_News|||"] = "Id||S|N||||HIDDEN||";
$T_NewsLetter_News["Id_NewsLetter|||"] = "Id NewsLetter||N|N||||HIDDEN||";
$T_NewsLetter_News["Id_Argomento|||"] = "Argomento||S|S||||SELECTFILTRO|Tlk_NewsLetter_Argomenti;Id;Descrizione";
$T_NewsLetter_News["Titolo_News|||"] = "Titolo||S|S||||TEXT|30|100";
$T_NewsLetter_News["Testo_Html|||"] = "Html||N|S||||HTMLAREA|10|60";
$T_NewsLetter_News["Link|||"] = "Link||N|N||||TEXT|30|100";
$T_NewsLetter_News["Ordine_News|||"] = "Ordine||S|N||||TEXT|5|3";
$T_NewsLetter_News["TipoRegistrazione"] = "Tipologia Utente||N|N||||CHECKBOX|Tlk_TipoRegistrazione|Id_TipoRegistrazione|Descrizione|||2|MENU||N";

$T_NewsLetter_News["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_NewsLetter_Html"]="Id";
$Permessi["T_NewsLetter_Html"]=";1;2;6;";
$Location["T_NewsLetter_Html"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_NewsLetter";
$T_NewsLetter_Html["Id|||"] = "Id||I|N||DIS||TEXT";
$T_NewsLetter_Html["Header|||"] = "Header||N|N||||HTMLAREA|10|50";
$T_NewsLetter_Html["Footer|||"] = "Footer||N|N||||HTMLAREA|10|50";
$T_NewsLetter_Html["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["Tlk_NewsLetter_Argomenti"]="Id";
$Permessi["Tlk_NewsLetter_Argomenti"]=";1;2;6;";
//$Azioni["Tlk_TestoMail"]["1"]="|Elimina";
//$Inserimento["Tlk_NewsLetter_Argomenti"]="N";
$Location["Tlk_NewsLetter_Argomenti"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=Tlk_NewsLetter_Argomenti";
$Tlk_NewsLetter_Argomenti["Id|||"] = "Id||I";
$Tlk_NewsLetter_Argomenti["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|100";
$Tlk_NewsLetter_Argomenti["Ordine|||"] = "Ordine||S|N||||TEXT|5|3";
$Tlk_NewsLetter_Argomenti["AreeInteresse||INS|"] = "Associa a persone con le seguenti aree||N|NNM||||CHECKBOX|Tlk_NewsLetter_Argomenti|Id|Descrizione|Id!=$ARGOMENTO_NEWS_GENERICO||2|MENU||N";
$Tlk_NewsLetter_Argomenti["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["T_Documenti"]="Id";
$Files["T_Documenti"]="documenti/;File";
$Inserimento["T_Documenti"]["97"]="N";
$Permessi["T_Documenti"]=";1;2;4;97;";
$Azioni["T_Documenti"]["1"]="|Elimina|Scarica";
$Azioni["T_Documenti"]["2"]="|Scarica";
$Azioni["T_Documenti"]["3"]="|Scarica";
$Azioni["T_Documenti"]["4"]="|Scarica";
$Azioni["T_Documenti"]["97"]="|Scarica";
$Location["T_Documenti"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=T_Documenti";
$T_Documenti["Files|||"]="Gestione Documenti||N|N||||LABEL|CENTER";
$T_Documenti["Id|||"] = "Id||I";
$T_Documenti["File|||"] = "Documento||S|S||||FILE|30|100|5000000|documenti|N";
$T_Documenti["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

//-----------------------------EXPO_T_Bacheca -------------------------------
$Chiave["EXPO_T_Bacheca"]="Id"; // Chiave tabella

// Gestisco azioni da compiere sulla tabella specificando il tipo di utente autorizzato a farla
$Permessi["EXPO_T_Bacheca"]=";1;2;3;15;96;97";
$Azioni["EXPO_T_Bacheca"]["1"]="|Elimina|Visualizza";
$Azioni["EXPO_T_Bacheca"]["2"]="|Elimina";
$Azioni["EXPO_T_Bacheca"]["97"]="|Elimina";
$Azioni["EXPO_T_Bacheca"]["98"]="|Elimina|";
$Location["EXPO_T_Bacheca"]["default"]["default"] = "/index.phtml?tmpl=2&pagina=elenchi&nome=EXPO_T_Bacheca";
$Modifica["EXPO_T_Bacheca"]="H";

//Form
$EXPO_T_Bacheca["Id|;1;2;3;15;97;98||"] = "ID||H";
$EXPO_T_Bacheca["Id_From|;1;2;3;15;97;98|INS||"] = "Da||N|N||||HIDDEN|uid|";
$EXPO_T_Bacheca["Id_To|;1;2;3;15;97;98|INS|"] = "A||S|N||||SELECTFILTRO|q_tj_anagrafica_utente;IdUtente;RagSoc|1|||||RagSoc|(Id_TipoUtente = '97')";
$EXPO_T_Bacheca["Id_To|;1;2;3;15;97;98|UPD|"] = "A||N|N||DIS||SELECTFILTRO|q_tj_anagrafica_utente;IdUtente;RagSoc|Id_To|";
$EXPO_T_Bacheca["Title|;1;2;3;15;97;98|INS|"] = "Oggetto||S|S||||TEXT|100|100";
$EXPO_T_Bacheca["Title|;1;2;3;15;97;98|UPD|"] = "Oggetto||N|N||DIS||TEXT|100|100";
$EXPO_T_Bacheca["Description|;1;2;3;15;97;98|INS|"] = "Testo||N|S||||TEXTAREA|20|80";
$EXPO_T_Bacheca["Description|;1;2;3;15;97;98|UPD|"] = "Testo||N|N||DIS||TEXTAREA|20|80|";
$EXPO_T_Bacheca["Data|;1;2;3;15;97;98|INS||"] = "Data||S|N||||HIDDEN|";
$EXPO_T_Bacheca["Data|;1;2;3;15;97;98|UPD||"] = "Data||N|N||DIS||TEXT|100|100|";
$EXPO_T_Bacheca["Attachment|;1;2;3;15;97;98|INS|"] = "Allegato||N|N||||TEXT|100|100";
$EXPO_T_Bacheca["Attachment|;1;2;3;15;97;98|UPD|"] = "Allegato||N|N||DIS||TEXT|100|100";
$EXPO_T_Bacheca["Id_Conversazione|;1;2;3;15;97;98|UPD|"] = "ID Conv.||S|N||DIS||TEXT|100|100";

$EXPO_T_Bacheca["label_separatore|||"]="</br>||N|N||||LABEL|";
$EXPO_T_Bacheca["Pulsante||INS|"] = "Inserisci||N|N||||BUTTON|CENTER";
?>