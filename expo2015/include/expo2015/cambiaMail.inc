<?php
//Genera link di attivazione del Profilo Legale Rappresentante
//$pathroot="/internet/datiExpo/include/";

require_once "configuration.inc";

include_once $PROGETTO."/view/lib/db.class.php";
include_once $PROGETTO."/view/lib/functions.inc";
include_once $PROGETTO."/view/languages/IT.inc";
/*--------- Libraries for sendFile -------*/
include_once 'send_mail.inc';

$dbSC = new DataBase();

if(isset($_POST['submit'])){
	
	$mailnew=$_POST["mail"];
		
	$nome= $dbSC->GetRow("SELECT * FROM T_Anagrafica WHERE id = ".$idUtente, "Nome");
	$cognome = $dbSC->GetRow("SELECT * FROM T_Anagrafica WHERE id = ".$idUtente, "Cognome");
	$mail = $dbSC->GetRow("SELECT * FROM T_Anagrafica WHERE id = ".$idUtente, "E_Mail");
	//echo "LR: ".$nome." ".$cognome." (".$mail.")"; 
	$pass = passwordCasuale(8);
	//echo "Password: ".$pass;
	if (strcmp($mail, $mailnew) == 0)
		$dbSC->Query("UPDATE T_Anagrafica SET Password='".$pass."', Stato_Record='D' WHERE id=".$idUtente);
	else {
		$dbSC->Query("UPDATE T_Anagrafica SET Password='".$pass."', E_Mail='".$mailnew."', Login='".$mailnew."', Stato_Record='D' WHERE id=".$idUtente);
		$mail = $mailnew;
	}
	//echo "UPDATE T_Anagrafica SET Password='".$pass."'";
		
	$passUrl = urlencode(base64_encode($pass));
		
	$utente = $nome . ' ' . $cognome;
	$link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $mail . "&cyph=" . md5($mail . $keySito) . "&Lang2=IT&action=INS";
	
	//Loggo il link di attivazione
	$qryLog = "INSERT INTO `expo_t_catalogo_logs` (`data`,`azione`,`parametri`,`response`) VALUES ('".date('Y-m-d H:i:s')."','Attivazione LR: ".$CF."','".$link."','OK');";
	//echo "Qry: ".$qryLog;
	//exit;
	$dbSC->Query($qryLog);
	
	//echo "Link: ".$link;
	send_mail($mail, $MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true);
	echo "<h2>Mail di attivazione inviata!</h2>";
	echo '<a href="/index.phtml?Id_VMenu=11">Torna a Lista Legali Rappresentanti</a>';
	exit;
} else {
	$idUtente = $_GET['Id'];
	//echo "IdUtente: ".$idUtente;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Form di invio Link attivazione</title>
</head>
<body>
<form name="form1" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="POST">
<br/>
  <strong>ID Utente: <?php echo $idUtente; ?></strong>
  <input type="hidden" name="idUtente" value="<?php echo $idUtente; ?>">
  <br/><br/>
  <label><strong>Nuova Mail: </strong></label>
  <input type="text" name="mail" size="50" width="50" value="">
  <br/><br/>
  <input name="submit" type="submit" value="submit" />
</form>
</body>
</html>
