<?php

/**
 * Function that print the guidet Tour
 * @param $currentPage
 * @param $pages
 * @param $link
 */
function guidetTour($currentPage,$pages,$link){

	if ($pages > 1){
		print '<p id="guidetTour"> ';
		
		if ($currentPage != 1){
			$prevPage = $currentPage - 1;
			print "<a class=\"back\" href=\"$link&slide=$prevPage\">Indietro </a> ";
		}
			
		for ($page = 1; $page < $pages+1; $page++) {
			if ($currentPage != $page)
				print "<a href=\"$link&slide=$page\">$page</a> ";
			else
				print "<span class=\"blu\"><b>$page</b> </span>";
		}
		
		if ($currentPage != $pages){
			$nextPage = $currentPage + 1;
			print "<a class=\"forward\" href=\"$link&slide=$nextPage\">Avanti </a> ";
		} 
		print '</p>';
	}
}


$NUMSLIDE = 29;

$slide = (int)$_GET["slide"];
if ($slide == null){
	$slide =1;
} 

if ($slide < 1){
	$slide =1;
}

if ($slide > $NUMSLIDE){
	$slide =$NUMSLIDE;
}

$link ="/index.phtml?Id_VMenu=502";

guidetTour($slide, $NUMSLIDE,$link );

$slideArea= array();
$slideArea[3] = '<area shape="rect" coords="328,391,492,430"	href="http://fornitori.expo2015.org/index.phtml?Id_VMenu=500" target="_blank" title="http://fornitori.expo2015.org/index.phtml?Id_VMenu=500" />';

$slideArea[5]  = '<area shape="rect" coords="293,218,659,256"	href="https://cns2.digicamere.it/TestCNS.html" target="_blank" title="https://cns2.digicamere.it/TestCNS.html" />';

$slideArea[10] ='<area shape="rect" coords="151,496,741,562"	href="http://fornitori.expo2015.org/" target="_blank" title="http://fornitori.expo2015.org/" />';


$linkPdf = Config::ROOTFILESHTML."Guida_Iscrizione.pdf";

?>


<div >
	<div class="ImageContainer">
		<center>
			<img height="660" width="900"
				src="/images/tutorial/Diapositiva<?php print $slide;?>.PNG"
				usemap="#map_slide<?php print $slide;?>" />
		</center>
	</div>
	<map name="map_slide<?php print $slide;?>">
		<?php print $slideArea[$slide];?>
	</map>
</div>

<p id="pdfGuida">
		<a class="pdf" href="<?php print $linkPdf;?>" target="_blank">Scarica la guida in pdf</a>
</p>






