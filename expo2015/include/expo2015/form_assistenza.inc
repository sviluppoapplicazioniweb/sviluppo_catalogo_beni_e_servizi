<?
include 'send_mail.inc';
include $PATHINC . '/lib/PHPMailer/class.smtp.php';

$db_bandi = new DB_CedCamCMS;
$db_camera = new DB_CedCamCMS;
$db_dati = new DB_CedCamCMS;
$db_persona = new DB_CedCamCMS;


if(isset($Pulsante))
{

	$typology_problem_array = explode("||", $typology_problem);
	$typology_problem_id = $typology_problem_array[0];
	$typology_problem_description = $typology_problem_array[1];
	
	$name_problem_array = explode("||", $name_problem);
	$name_problem_id = $name_problem_array[0];
	$name_problem_description = $name_problem_array[1];
	
	if( ($typology_problem_description!="") and ($typology_problem_description!="---") )
	{

		if( $typology_problem_description == "Registrazione al Catalogo" && $name_problem_description == "CNS"){
			
			$arr_header[1]="X-OTRS-Priority:1 - Normale";
			$arr_header[2]="X-OTRS-Queue:ASSISTENZA UTENTI::SECONDI LIVELLI::EXPO_TECNICA"; 
			$arr_header[3]="X-OTRS-Type:ASSISTENZA UTENTI";
			$arr_header[4]="X-OTRS-State:closed successful";
			$arr_header[5]="X-OTRS-service:ASSISTENZA_UTENTI::EXPO";
			$arr_header[6]="X-OTRS-DynamicField-WPCategory:882";
			//$arr_header[7]="X-OTRS-DynamicField-AUDettaglioLev2:3269||Registrazione al Catalogo";
			//$arr_header[8]="X-OTRS-DynamicField-AUDettaglioLev3:3270||CNS";
			$arr_header[7]="X-OTRS-SLA:Risposta in 2 ore";
			$arr_header[8]="X-OTRS-DynamicField-AUOrigineTicketHidden:form";
			
		}
		else{
			$arr_header[1]="X-OTRS-Priority:1 - Normale";
			$arr_header[2]="X-OTRS-Queue:ASSISTENZA UTENTI::EXPO"; 
			$arr_header[3]="X-OTRS-Type:ASSISTENZA UTENTI";
			$arr_header[4]="X-OTRS-State:new";
			$arr_header[5]="X-OTRS-service:ASSISTENZA_UTENTI::EXPO";
			$arr_header[6]="X-OTRS-DynamicField-Category:$name_problem";
			//$arr_header[7]="X-OTRS-DynamicField-AUDettaglioLev2:$typology_problem";
			//$arr_header[8]="X-OTRS-DynamicField-AUDettaglioLev3:$name_problem";
			$arr_header[7]="X-OTRS-SLA:Risposta in 2 ore";
			$arr_header[8]="X-OTRS-DynamicField-AUOrigineTicketHidden:form";
		}

		
		$message="Nome Mittente: ".$nome_mittente."<br>";
		//$message.="EMail Mittente: ".$e_mail."<br>";
		$message.="Codice Fiscale Impresa: ".$cf_impresa."<br>";
		$message.="Telefono: ".$telefono."<br><br>";
		$message.="Categoria Assistenza: ".$typology_problem_description."<br>";
		$message.="Tipo Assistenza: ".$name_problem_description."<br>";
		$message.="Problema: ".$commento;

		if(send_mail_html_file_header($MAILASSISTENZA, $e_mail, "Assistenza: $typology_problem_description - $name_problem_description" ,$message, $arr_header,$nome_mittente))
		{
			$codice_html=file_get_contents($PATHDOCS."tmpl/form_assistenza_risposta.html");
			echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);
		}
		else
		{
			echo "<div id=\"form_moduli\">";
			echo "Errore nell'invio del Messaggio";
			echo "<br><br><a class=\"rosso\" href=\"#\" onClick=\"history.go(-1);return true;\">Torna indietro e riprova</a>";
			echo "</div>";
		}
	}
	else
	{
		echo "<div id=\"form_moduli\">";
		echo "Problematica non valida";
		echo "<br><br><a class=\"rosso\" href=\"#\" onClick=\"history.go(-1);return true;\">Torna indietro</a>";
		echo "</div>";
	}
}
else
{
	 
	$typology_problem_vari = "<select id=\"typology_problem\" name=\"typology_problem\" size=\"1\">\n";
	$typology_problem_vari .= "<option selected=\"selected\" data-type=\"0\" value=\"\">---</option>\n";
	$typology_problem_vari .= "<option data-type=\"1\" value=\"3284||Iscrizione alle Categorie merceologiche / Ateco mancanti\">Iscrizione alle Categorie merceologiche / Ateco mancanti</option>\n";
	$typology_problem_vari .= "<option data-type=\"2\" value=\"3275||Compilazione del Catalogo\">Compilazione del Catalogo</option>\n";
	$typology_problem_vari .= "<option data-type=\"3\" value=\"3269||Registrazione al Catalogo\">Registrazione al Catalogo</option>\n";
	$typology_problem_vari .= "<option data-type=\"4\" value=\"3288||Altro\">Altro</option>\n";
	$typology_problem_vari .= "</select>\n";

	 
	 
	$problem_vari="<select id=\"name_problem\" name=\"name_problem\" size=\"1\">\n</select>\n";

	$nome=$nominativo;
	$IdImpresa = $auth->auth['IdImpresa'];

	$db = new DataBase();
	$codFiscale = $db->GetRow("SELECT CodiceFiscale FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "CodiceFiscale");
	 
	$email = $reg_utente;
	 
	 
	$codice_html=file_get_contents($PATHDOCS."tmpl/form_assistenza.html");
	echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);
}

