
<script type="text/javascript" src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/jquery/ui/jquery-ui.js"></script>


<script type='text/javascript'>//<![CDATA[ 
    $(function() {
        var MenuTree = {
            collapse: function(element) {

                element.slideToggle(600);

            },
            walk: function() {

                $('a.first', '#tree').each(function() {

                    var $a = $(this);
                    var $li = $a.parent();

                    if ($a.next().is('ul')) {

                        var $ul = $a.next();

                        $a.click(function(e) {

                            e.preventDefault();
                            MenuTree.collapse($ul);

                            $a.toggleClass('active');

                        });

                    }



                });


            }
        };

        MenuTree.walk();
    });//]]>  

</script>



<style type='text/css'>

    .red{
        color:red;
    }

    #search{
        margin: 2em 0 0 3em;
    }

    #result{
        margin: 2em 0 0 3em;
    }

    #result li {
    	padding-bottom: 2px;
    
    }
    #search label{
        margin: 2em 2em 0 0em;
    }     

    
     .description{
        margin: 2em 0 0 2em;
    }
    
    #tree {
        margin: 2em 0 0 0em;
        /*width: 20em;*/
        list-style-type: none;
        color:#005E9D;
    }
    

    #tree > li {

        padding-bottom: 3px;
        margin-bottom: 0.5em;

    }

    #tree  li > a {

        font-size:16px;    
        text-decoration: none;

    }

    #tree  li > a.first:before {
        content: '+';
        padding-right: 4px;

    }

    
    #tree  li >a.active:before {
        content: '-';
        padding-right: 4px;

    }

    #tree li ul.level2  {
        margin: 0.5em 0 0.5em 1em;
        display: none;
        font-weight: bold;

    }

    #tree ul.level3  {
        margin: 0.5em 0em 0.5em -1em;
        font-weight: normal;
    }

    #tree li.level3  {
        margin: 0.5em 0em 0.5em 0em;
        font-weight: normal;

    }
    

    

</style>

<?php
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/languages/IT.inc");
$db = new DataBase();

/* --------------------- Start   Query for Autocomplete SearchForm --------------------------- */
$autocompleteSql = "SELECT  DISTINCT Descrizione FROM EXPO_Tlk_Categorie WHERE Id_Categoria REGEXP '([0-9]{1,3}\.){3}[0-9]' ORDER BY Descrizione";
$autocompleteResult = $db->GetRows($autocompleteSql);

print'<script>' . "\n";
print'  $(function() {' . "\n";
print'    var availableTags = [' . "\n";
foreach ($autocompleteResult as $element) {
    print '"' . trim($element['Descrizione']) . '",';
}
print'        ];' . "\n";

print'$( "#searchCode" ).autocomplete({
	appendTo: "#risultati",
    open: function() {
        var position = $("#risultati").position(),
            left = position.left, top = position.top;

        $("#risultati > ul").css({left: left + 20 + "px",
                                top: top + 4 + "px" });

    },
	source: function ( request, response ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
		response(
				$.grep(
						availableTags, function( item ){
							return matcher.test( item );
						}
						)
				);
	}
}); ';
/*
print'        $( "#searchCode" ).autocomplete({' . "\n";
print'   source: availableTags' . "\n";
print'    });' . "\n";*/
print'  });' . "\n";
print'  </script>';


/* ---------------------  End  Query for Autocomplete SearchForm --------------------------- */
?>


<div>
	
	<div id="search" style="font-style: italic; width: 90%">
    <?php 	echo _DISCLAIMER_CATEGORIE_; ?>
    </div>
		
    <form id="search" action="index.phtml?Id_VMenu=500" method="POST" >
    
    
        <label>"Ricerca per Codice Ateco"  </label>
        <input id="searchCode" type="search" name="codeAteco">

        <button>Ricerca</button>
    </form>
    
    
</div>

<?php
/* ----------------------   Start Search Categories By Ateco Code   ------------------- */

$searchCode = $_POST['codeAteco'];
if ($searchCode != NULL) {
    $correctCode = false;
    //Validation Ateco Code
    if (ereg("^([0-9]{1,3}\.){0,2}[0-9]*$", $searchCode)) {
        $correctCode = true;
    }
    print '<div id="result">';
    if ($correctCode) {
        //Query Search Ateco Code
        $searchSql = "SELECT Id_Categoria,Descrizione,DescEstesa,Stato_Record,FlagNote,Note FROM EXPO_Tlk_Categorie WHERE Descrizione like '$searchCode%' ORDER BY SUBSTRING(Id_Categoria,1,9),Descrizione ";
        $searchResult = $db->GetRows($searchSql);


        if (count($searchResult) > 0) {
            print '<ul>';
            foreach ($searchResult as $singleResult) {
/*print "<pre>";
print_r($singleResult);
print "</pre>";*/
                            	
                print '<li>' . $singleResult['DescEstesa'];
                if (strcmp ($singleResult['FlagNote'],'Y') == 0) {
                
                	$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
                	$cat = explode(".",$singleResult['Id_Categoria']);
                	$IdCategoria = $cat[0].".".$cat[1].".".$cat[2];
                	$noteCategoriaSQL = "SELECT Id_Categoria,Descrizione,DescEstesa,Stato_Record,FlagNote,Note FROM EXPO_Tlk_Categorie WHERE Id_Categoria = '$IdCategoria'";
                	$noteCategoria = $db->GetRow($noteCategoriaSQL,null,null,"albero");
                	if ((strcmp ($noteCategoria['Note'],'_DEFAULT_') != 0)){
                		$nota = $noteCategoria['Note'];
                	}
                		
                	print '<a href="#" class="tooltip" draggable="false">
        					<img src="tmpl/expo2015/catalogo/images/info1.png" name="iconaInfo" />
							<span> <img class="callout" draggable="false" src="tmpl/expo2015/catalogo/images/callout.gif" />'
                			. $nota .
                			'</span>
						</a>';
                }
                print  '</li>';
            }
            print'</ul>' . "\n";
        } else {
            print 'Nessuna Corrispodenza Trovata.';
        }
    } else {
        print '<span class="red">Codice Inserito Scorretto.</span>';
    }
    print '</div>';
}

/* ----------------------   End Search Categories By Ateco Code   ------------------- */
?>

<p class="description">Di seguito &egrave riportato l'albero delle categorie merceologiche suddiviso in livelli gererchici.
<br>Nel terzo livello, tra le parentesi tonde, sono indicati i codici Ateco relativi alla categoria merceologica</p>

<?php 

/* ----------------------   Start Printing Categories Tree   ------------------- */
$strSql = "SELECT Id_Categoria,Descrizione,DescEstesa,Stato_Record,FlagNote,Note FROM EXPO_Tlk_Categorie Where Id_Categoria like '0%'  ORDER BY Id_Categoria ";

//Array of result of $strSql query
$result = $db->GetRows($strSql);

function PrintLevel($categories, $index, $level) {
	
	$MAX_ATECO_ROW = 10;

    if ($level == 4) {
        return array($index, $level - 1);
    }
    for ($k = 0; $k < $level; $k++) {
        print "\t";
    }
    if ($level == 1) {
        print'<ul id="tree">';
    } elseif ($level > 1 && $level < 4) {
        print'<ul class="level' . $level . '">';
    } else {
        print'<ul>';
    }

    print"\n";


    while ($index < count($categories)) {

        if (count($treeBranch = explode(".", $categories[$index]['Id_Categoria']))) {
            for ($k = 0; $k < $level + 1; $k++) {
                print "\t";
            }
            print'<li class="level' . $level . '">';
            if ($level == 1) {
                print'<a href="#" class="first">';
            }
            if ($level == 3) {
            	print'<u>';
            }
            print $categories[$index]['Descrizione'];
            if ($level == 1) {
                print'</a>';
            }else if ($level == 3) {
                print'</u>';
            } 
           	if ( ($level == 3) && (strcmp ($categories[$index]['FlagNote'],'Y') == 0) ) {
                
				$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
				if ((strcmp ($categories[$index]['Note'],'_DEFAULT_') != 0)){
					$nota = $categories[$index]['Note'];
					}
					
				print '<a href="#" class="tooltip" draggable="false"> 
        					<img src="tmpl/expo2015/catalogo/images/info1.png" name="iconaInfo" />
							<span> <img class="callout" draggable="false" src="tmpl/expo2015/catalogo/images/callout.gif" />' 
        					. $nota .
							'</span>
						</a>';
            } 
            	
            

            //Printing Ateco
            if ($level == 3) {

                $atecoNode = count(explode(".", $categories[$index + 1]['Id_Categoria']));
                if ($atecoNode == 4) {
                    $index++;
                    print'<div class = "ateco" />';
                    //print' ( ';
                    $numberAtecoRow = 0;
                    while ($atecoNode == 4) {
                        print $categories[$index]['Descrizione'];
                        $atecoNode = count(explode(".", $categories[$index + 1]['Id_Categoria']));
                        if ($atecoNode == 4) {
	                        print' - ';
	                        $index++;
	                        $numberAtecoRow++;
	                        if ($numberAtecoRow == $MAX_ATECO_ROW){
								print "<br />";
								$numberAtecoRow = 0;
							}
						}			

                    }
                    //print' )';
                    print "</div>";
                    print'</li>';
                }
            }

            $nextNode = count($treeBranch = explode(".", $categories[$index + 1]['Id_Categoria']));

            if ($level == 3 && $nextNode < 3) {
                //Chiusura </ul> e </li> caso livello attuale 3 e livello successivo 1
                if ($nextNode == 1) {
                    print "\n";
                    for ($k = 0; $k < $level; $k++) {
                        print "\t";
                    }
                    print '</ul>';
                    print "\n";
                    for ($k = 0; $k < $level; $k++) {
                        print "\t";
                    }
                    print'</li>';
                    print "\n";
                    for ($k = 0; $k < $level - 1; $k++) {
                        print "\t";
                    }
                    print '</ul>';
                    print "\n";
                    for ($k = 0; $k < $level - 1; $k++) {
                        print "\t";
                    }
                    print'</li>';
                    print "\n";
                    $level = $nextNode;
                }
                //Chiusura </ul> e </li> caso livello attuale 3 e livello successivo 1
                if ($nextNode == 2) {
                    print "\n";
                    for ($k = 0; $k < $level; $k++) {
                        print "\t";
                    }
                    print '</ul>';
                    print "\n";
                    for ($k = 0; $k < $level; $k++) {
                        print "\t";
                    }
                    print'</li>';
                    print "\n";
                    $level = $nextNode;
                }
                return array($index + 1, $level);
            }
            print"\n";
        }

        $index++;
        //Chiamata ricorsiva
        if ($level < 4) {
            $level++;
            $ret = PrintLevel($categories, $index, $level);
            $index = $ret[0];
            $level = $ret[1];
        }
    }

    if ($index == count($categories)) {
        print'</ul>';
    }
    print"\n";
    return array($index + 1, $level);
}

/* --------------  End Printing Categories Tree  ------------------- */


PrintLevel($result, 0, 1);
?>