<?php 
/* new */

function ms_escape_string($data) {
	if ( !isset($data) or empty($data) ) return '';
	if ( is_numeric($data) ) return $data;

	$non_displayables = array(
			'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/',             // url encoded 16-31
			'/[\x00-\x08]/',            // 00-08
			'/\x0b/',                   // 11
			'/\x0c/',                   // 12
			'/[\x0e-\x1f]/'             // 14-31
	);
	foreach ( $non_displayables as $regex )
		$data = preg_replace( $regex, '', $data );
	$data = str_replace("'", "''", $data );
	return $data;
}


function checkPasswordPolicy($password){
	global $PROGETTO;
	require_once  ($PROGETTO.'/classes/clsCheckPasswordPolicy.php');
	$checker = new clsCheckPasswordPolicy();
	$policy = $checker->getPswPolicyChecker();

	return $policy->validate($password);

}


function verifypass($password,$salt){
	$concpass=$password.$salt;
	for($k=0;$k<10; $k++)
		$concpass= hash("sha256",$concpass );
				 
		return $concpass;
}

function unsetToken($token){

$db1 = new DB_CedCamCMS;
$sql="UPDATE T_Token SET Data_Utilizzo=NOW() WHERE Token='".$token."'";
	return $db1->query($sql);

}

function verificaScadenzaPassword($idutente){

//questa funzione verifica che la password non sia scaduta

	//recupero anagrafica da db
	$db1 = new DB_CedCamCMS;
	$sql="SELECT * FROM T_Anagrafica WHERE Id=".$idutente;
	$db1->query($sql);
	$db1->next_record();
	
	//marca temporale della scadenza password
	$scadenza=$db1->f("Scadenza_Password");
	$anno=substr($db1->f("Scadenza_Password"),0,4);
	$mese=substr($db1->f("Scadenza_Password"),5,2);
	$giorno=substr($db1->f("Scadenza_Password"),8,2);


	$marca_scadenza = mktime(0,0,0,$mese,$giorno,$anno);

	//marca temporale di questo momento
	$marca_now = mktime(0,0,0,date(m),date(d),date(Y));

	//confronto le marche temporali
	
	if($marca_now>=$marca_scadenza){//se � scaduta return 0
	return 0;
	}
	else{//se non � scaduta return 1
	return 1;
	}
		
	}


function setToken($idutente){
	
	global $PATHINC,$HOMEURL,$MAINSITE, $MAILSITO;
	//traccio gli indirizzi IP
	include_once $PATHINC."send_mail.inc";
	
	$ip1=$_SERVER['REMOTE_ADDR'];
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		$ip2=$_SERVER['HTTP_CLIENT_IP'];
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ip3=$_SERVER['HTTP_X_FORWARDED_FOR'];

		//calcolo la scadenza del token
	$scadenza_token = date('Y-m-d H:s:i', mktime(date(H),date(s),date(i),date(m),date(d)+1,date(Y)));

	//genero il token
	$stamp = mktime(date(H),date(s),date(i),date(m),date(d)+1,date(Y));
	//$seq=sqlNextSequenceValue("Token");
	$seq=rand();
	$token=hash("sha256",$stamp.$seq.$stamp);

	//inserisco il token nel db
	$db1 = new DB_CedCamCMS;
	$sql="INSERT INTO T_Token(Token,Id_Utente,IP1,IP2,IP3,Data_Scadenza,Data_Utilizzo) VALUES ('".$token."', '".$idutente."',  '".$ip1."', '".$ip2."','".$ip3."', '".$scadenza_token."', NULL)";
	$db1->query($sql);

	$sql="SELECT * FROM T_Anagrafica WHERE Id=".$idutente;
	$db1->query($sql);
	$db1->next_record();

	//marca temporale della scadenza password
	$E_Mail=$db1->f("E_Mail");
	$nominativo=$db1->f("Nome")." ".$db1->f("Cognome");
	$sito=$MAINSITE;
	$link=$MAINSITE."index.phtml?Id_VMenu=49&tk=".$token;
	
	$qryLog = "INSERT INTO `EXPO_T_Catalogo_Logs` (`data`,`azione`,`parametri`,`response`) VALUES ('".date('Y-m-d H:i:s')."','Attivazione LR Attivo: IdUser: ".$idutente."','".$link."','OK');";
	//echo "Qry: ".$qryLog;
	//exit;
	$db1->query($qryLog);
	
	$SQL="Select * from T_MailGeneriche where Id_Mail=13";
		if ($db1->query($SQL))
		{
			$db1->next_record();
			$oggetto_mail=$db1->f("Oggetto_Mail");
	$testo_mail=$db1->f("Testo_Mail");
	}

	$oggetto_mail=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$oggetto_mail);
	$testo_mail=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$testo_mail);
	
	send_mail_html($E_Mail,$MAILSITO,$oggetto_mail,$testo_mail);

	return 1;

	}



	function checkToken($token){

	$scaduto=0;
	$giausato=0;
	$db1 = new DB_CedCamCMS;
	$sql="SELECT * FROM T_Token WHERE Token='".$token."'";
	$db1->query($sql);
	if($db1->next_record())
	{

	//marca temporale della scadenza token
		$scadenza=$db1->f("Data_Scadenza");
		$anno=substr($db1->f("Data_Scadenza"),0,4);
		$mese=substr($db1->f("Data_Scadenza"),5,2);
		$giorno=substr($db1->f("Data_Scadenza"),8,2);
		$ore=substr($db1->f("Data_Scadenza"),11,2);
		$minuti=substr($db1->f("Data_Scadenza"),14,2);
		$secondi=substr($db1->f("Data_Scadenza"),17,2);
		$marca_scadenza = mktime($ore,$minuti,$secondi,$mese,$giorno,$anno);

		//marca temporale di questo momento
		$marca_now = mktime(date(H),date(i),date(s),date(m),date(d),date(Y));

		//confronto le marche temporali per vedere che il token non sia scaduto
		if($marca_now>=$marca_scadenza){
		$scaduto=1;
			
	}

	//verifico che il token non sia gia stato usato
	$datauso=$db1->f("Data_Utilizzo");
	if($datauso!=NULL){
	$giausato=1;
	}


	}
	else
	{
	return 0;
	}


	if($scaduto==1||$giausato==1){
	return 0;
	}
	else
	{
	return 1;
	}


		
	}

	/* end new */


?>