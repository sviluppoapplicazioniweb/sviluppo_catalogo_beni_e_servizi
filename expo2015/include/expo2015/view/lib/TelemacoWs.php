<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of TelemacoWs
 *
 * @author XddG123
 */

/*
 * Questa classe permette l'interrogazione del servizio Rest di Telemaco.
*/
require_once("configuration.inc");
require_once($PROGETTO . "/view/lib/XML2Array.php");

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

require_once($PROGETTO . "/view/lib/db.class.php");
class TelemacoWs {
    //put your code here
    public  $token,$user,$url,$db,$numMaxStep;

    function setCertificazioni($array_rea) {
        $array_certificazioni=$array_rea['dati']['blocchi-impresa']['info-attivita']['accreditamenti-odc'];
        $certificazioni=array();
        if(is_array($array_certificazioni)) {
            foreach ($array_certificazioni as $key => $value) {
                $certificazioni[]['descrizione']=$descrizione=$value['@attributes']['schema-accreditamento'];
                $certificazioni[]['n_certificato']=$n_certificato=$value['@attributes']['n-certificato'];
                $certificazioni[]['c-schema-accreditamento']=$n_certificato=$value['@attributes']['c-schema-accreditamento'];
                $certificazioni[]['data_emissione']=$data_emissione=$value['@attributes']['dt-emissione'];
                $certificazioni[]['data_scadenza']=$dt_scadenza=$value['@attributes']['dt_scadenza'];
            }
        }
        return $certificazioni;
    }


    public function SetAmministratoriFromRea($array_rea) {

        $arrayPersone=$array_rea['dati']['blocchi-impresa']['persone-sede']['persona'];
        $cciaa=$array_rea['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['cciaa'];
        $nrea=$array_rea['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['n-rea'];
        foreach ($arrayPersone as $key => $value) {
            $cf=$value['persona-fisica']['@attributes']['c-fiscale'];
            $cariche=$value['atti-conferimento-cariche']['atto-conferimento-cariche'];
            if(is_array($cariche) and $cariche!=null) {
                foreach ($cariche as $key1 => $value1) {
                    $carica='';

                    if(is_array($value1)) {
                        if(isset ($value1['cariche']['carica'])) {
                            $carica=$value1['cariche']['carica'];

                        }
                    }
                    if(isset ($value1['carica'])) {
                        $carica=$value1['carica'];
                    }
                    if($carica!='') {
                        $qrt="SELECT Carica from EXPO_Tlk_Cariche_Rappresentanti where Carica='".mysql_escape_string($carica)."'";

                        if($this->db->NumRows($qrt)>0) {
                            $sql_="SELECT * from EXPO_TJ_Utente_NRea where  CodiceFiscale='".$cf."'
                         and Cciaa='".$cciaa."' and NRea='".$nrea."'";

                            if($this->db->NumRows($sql_)==0) {


                                $query="
                        INSERT INTO EXPO_TJ_Utente_NRea (Id,CodiceFiscale,Ruolo,Cciaa,NRea)
                        VALUES
                        (
                        (SELECT COALESCE(MAX(A.Id),0)+1 from EXPO_TJ_Utente_NRea as A),'".$cf."','".$carica."',
                           '".$cciaa."','".$nrea."' )";

                                $this->db->query($query);
                            }
                        }
                    }
                }
            }
        }

    }

	
    public function SetCaricheFromPersona($array_persona) {
    	
    	//Ricava il numero di posizioni se non ce ne sono evita il processo di Set
    	$nPos = $array_persona['Risposta']['Testata']['Riepilogo']['NumeroPosizioni'];
    	//print_r($array_persona);
    	//echo "<br/>NumPos".$nPos;
    	//exit;
    	if ($nPos > 0 ){
    		
	    	$cf=$array_persona['Risposta']['dati']['Riconoscimento']['IdentificativoPosizione'];
	    	$arrayImprese=$array_persona['Risposta']['dati']['blocchi-persona']['imprese-persona']['impresa-persona'];

	    	if (!(is_array($arrayImprese[0]))){
	    		$temp = $arrayImprese;
	    		$arrayImprese = array ();
	    		$arrayImprese[] = $temp;
	    	}
	    	
	    	/* cicla su tutte le imprese legate alla persona e per ciascuna ricava i dati cciaa rea e
	    	 * cariche possedute.
	    	 * Verifica che tale persona non sia gi� inserita e in caso sia gi� presente ne aggiunge le
	    	 * cariche nuove. Le vecchie cariche vengono mantenute per evitare di perdere lo storico.
	    	*/
	    	
	    	foreach ($arrayImprese as $key => $value) {
	    		//Ricava cciaa e NRea
	    		$cciaa=$value['dati-identificativi-impresa']['@attributes']['cciaa'];
	    		$nrea=$value['dati-identificativi-impresa']['@attributes']['n-rea'];
	    		$cfImp=$value['dati-identificativi-impresa']['@attributes']['c-fiscale'];
	    		
	    		$cariche=$value['persona']['cariche']['carica'];
	    		if(is_array($cariche) and $cariche!=null) {
	    			//Se ci sono cariche abbinate
	    			//echo "NRea: $nrea Num Cariche: ".count($cariche)."<br/>";
	    			foreach ($cariche as $key1 => $value1) {
	    				$carica='';
	    				
	    				if(is_array($value1) and $value1!=null) {
	    					//$caricadesc=$value1['@value'];
	   						//echo "Carica1: <br/>".print_r($value1);
	    					$carica=$value1['@attributes']['c-carica'];
	    					if (strlen($carica) == 0){
	    						$carica=$value1['c-carica'];
	    					} 
	    					//echo "Carica: ".$carica." Nrea: $nrea<br/>";
	    					
	    				} 
	    				
	    				if (strlen($carica) > 0){
	    					//echo "Carica2: ".$caricadesc." Nrea: $nrea<br/>";
		    				$sql_="SELECT * from EXPO_TJ_Utente_NRea where  CodiceFiscale='".$cf."'
	                         and Cciaa='".$cciaa."' and NRea='".$nrea."' and Ruolo='".$carica."'";
		    				
		    				if($this->db->NumRows($sql_)==0) {
		    					//Aggiunge carica mancante
		    					$query="
	                        INSERT INTO EXPO_TJ_Utente_NRea (Id,CodiceFiscale,Ruolo,Cciaa,NRea,LR,CFiscImp) VALUES
	                        ((SELECT COALESCE(MAX(A.Id),0)+1 from EXPO_TJ_Utente_NRea as A),'".$cf."','".$carica."',
	                           '".$cciaa."','".$nrea."','N','".$cfImp."' )";
		    					
		    					$this->db->query($query);
		    				}
	    				}
	    				
	    			}
	    		}
	    	}
    	}
    
    }
    
    public function  __construct($token="pippo",$user="pippo",$numMaxStep=6) {

        /*
         * /token
         *     String securityTokenKey="1234567890abcdefghilmnop";
               String securityTokenAlgorithm="MD5";
               String securityTokenDateFormat="yyMMDD";
         *     String composit = user + date + securityTokenKey;
        */
        $securityTokenDateFormat=date("ymz",time());
        $securityTokenKey="1234567890abcdefghilmnop";
        $token_md5=md5($user.$securityTokenDateFormat.$securityTokenKey);
        $this->token=$token_md5;
        $this->user=$user;
        $this->url=Config::URLTELEMACO;
        $this->db = new DataBase();
        $this->numMaxStep=$numMaxStep;
    }

    private  function CallAPI($method,$data) {
        $url=$this->url.$method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        $result = curl_exec($ch);

        if($result  === false) {
            $return['STATUS']="ERROR";
            $return['INFO']=curl_error($ch);

        }
        else {
            $return['STATUS']="SUCCESS";
            $return['INFO']=$result ;
        }

        curl_close($ch);

        return $return;
    }
    /*
    protected function GetCertificazioniFromVisura($cciaa,  $rea) {
        $data="cciaa=".$cciaa."&rea=".$rea."&token=".$this->token."&user=".$this->user;

       	$xml = $this->CallAPI('richiestaBlocchi', $data);
        return $xml['INFO'];
    }
    */
    public function GetInfoAziendaFromCf($cf) {
        global $debugIscrzione;
        $infoAzienda=explode("-", $cf);

        if($debugIscrzione['visura'] == FALSE) {
            $xml_rea = $this->GetInfoAziendaFromVisuraOrdinaria($infoAzienda[2], $infoAzienda[1]);
        }else {
//            $filename=__DIR__."/xml/visuraOrdinaria-".$infoAzienda[2]."-".$infoAzienda[1].".xml";
          $filename=__DIR__."/xml/Bravobus_srl.xml";
          $f=fopen($filename, "r");
            $visuraOrdinariaXml=fread($f, filesize($filename));
            fclose($f);
            $xml_rea['INFO']=$visuraOrdinariaXml ;
        }


        $array_rea = @json_decode(json_encode((array)simplexml_load_string($xml_rea['INFO'])),1);
        //$this->SetAmministratoriFromRea($array_rea);
        $info_certificazione=array();

        $info_categorie=array();
        $descrizione_attivita=$array_rea['dati']['blocchi-impresa']['info-attivita']['attivita-esercitata'];
        $array_categorie=$array_rea['dati']['blocchi-impresa']['info-attivita']['classificazioni-ateco'];
        $iCategorie=0;

        if($array_categorie['classificazione-ateco']['@attributes']) {
        	if(!in_array_r($array_categorie['classificazione-ateco']['@attributes']['c-attivita'], $info_categorie)) {
	        	$info_categorie[$iCategorie]['c-attivita']=$array_categorie['classificazione-ateco']['@attributes']['c-attivita'];
	            $info_categorie[$iCategorie]['attivita']=$array_categorie['classificazione-ateco']['@attributes']['attivita'];
	            $iCategorie++;
        	}
        }else {

            foreach ($array_categorie['classificazione-ateco'] as $key => $value) {
            	if(!in_array_r($value['@attributes']['c-attivita'], $info_categorie)) {
	                $info_categorie[$iCategorie]['c-attivita']=$value['@attributes']['c-attivita'];
	                $info_categorie[$iCategorie]['attivita']=$value['@attributes']['attivita'];
	                $iCategorie++;
            	}
            }
        }
 
        $array_loc=$array_rea['dati']['blocchi-impresa']['localizzazioni'];
        $value=$array_loc['localizzazione']; 
        if(count($array_loc)>0) {
            if(isSet($value['classificazione-ateco']['@attributes'])) {
                    foreach ($value['classificazione-ateco'] as $key1 => $value1) {
                        if(count($value['classificazione-ateco'])>1) {
                            if(!in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
                                $iCategorie++;
                            }
                        }else {
                            if(!in_array_r($value1['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['attivita'];
                                $iCategorie++;
                            }
                        }
                    }
                }
                if(isSet($value['classificazioni-ateco']['classificazione-ateco'])) {
                 
                    foreach ($value['classificazioni-ateco']['classificazione-ateco'] as $key1 => $value1) {

                        if(count($value['classificazioni-ateco']['classificazione-ateco'])>1) {
                            if(!in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
                                $iCategorie++;
                            }
                        }else {
                            if(!in_array_r($value1['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['attivita'];
                                $iCategorie++;
                            }
                        }
                    }
                }
            foreach ($array_loc['localizzazione'] as $key => $value) {

                if(isSet($value['classificazione-ateco']['@attributes'])) {
                    foreach ($value['classificazione-ateco'] as $key1 => $value1) {
                        if(count($value['classificazione-ateco'])>1) {                            
                            if(!in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
                                $iCategorie++;
                            }
                        }else {
                            if(!in_array_r($value1['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['attivita'];
                                $iCategorie++;
                            }
                        }
                    }
                }
                if(isSet($value['classificazioni-ateco']['classificazione-ateco'])) {
                    foreach ($value['classificazioni-ateco']['classificazione-ateco'] as $key1 => $value1) {

                        if(count($value['classificazioni-ateco']['classificazione-ateco'])>1) {
                            if(!in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
                                $iCategorie++;
                            }
                        }else {
                            if(!in_array_r($value1['c-attivita'], $info_categorie)) {
                                $info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
                                $info_categorie[$iCategorie]['attivita']=$value1['attivita'];
                                $iCategorie++;
                            }
                        }
                    }
                }
            }
        
        }else {

        }


        $infoArray['Certificazioni']=$this->setCertificazioni($array_rea);
        if(!isset ($array['ListaImpreseRI']['Impresa']['AnagraficaImpresa'])) {
            $prefix=$array['ListaImpreseRI']['Impresa']['0']['AnagraficaImpresa'];
        }else {
            $prefix=$array['ListaImpreseRI']['Impresa']['AnagraficaImpresa'];
        }
        $prefix=$array_rea['dati']['blocchi-impresa']['dati-identificativi'];
        $infoArray['denominazione']=$prefix['@attributes']['denominazione'];
        if($array_rea['dati']['blocchi-impresa']['elenco-soci']['capitale-sociale']['@attributes']['ammontare']=='') {
            $infoArray['C_sociale']='';
        }else {
            $infoArray['C_sociale']=$array_rea['dati']['blocchi-impresa']['elenco-soci']['capitale-sociale']['@attributes']['ammontare'];
        }

        /* aggiunta partita iva e numero rea */
        $infoArray['PIva'] = $array_rea['dati']['blocchi-impresa']['info-sede']['partita-iva'];
        $infoArray['NRea'] = $array_rea['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['n-rea'];
        /* aggiunta partita iva e numero rea */
        
        $infoArray['ViaSede']=$prefix['indirizzo-localizzazione']['@attributes']['toponimo']." ".$prefix['indirizzo-localizzazione']['@attributes']['via'];
        $infoArray['NCivicoSede']=$prefix['indirizzo-localizzazione']['@attributes']['n-civico'];
        $infoArray['DescComSede']=$prefix['indirizzo-localizzazione']['@attributes']['comune'];
        $infoArray['SglPrvSede']=$prefix['indirizzo-localizzazione']['@attributes']['provincia'];
        $infoArray['CapSede']=$prefix['indirizzo-localizzazione']['@attributes']['cap'];
        $infoArray['IndirizzoPostaCertificata']=$prefix['indirizzo-posta-certificata'];
        $infoArray['attivitaEsercitata']=$descrizione_attivita;
        $infoArray['forma-giuridica']=$prefix['forma-giuridica'];
        $infoArray['Categorie']=$info_categorie;
        $ndipe=$array_rea['dati']['blocchi-impresa']['info-attivita']['addetti-impresa']['@attributes']['n-dipendenti'];
        if($ndipe=='') {
            $numeromesi=count($array_rea['dati']['blocchi-impresa']['info-attivita']['addetti-impresa']['info-mesi']['info-mese']);
            $infoArray['ndipendenti']=$array_rea['dati']['blocchi-impresa']['info-attivita']['addetti-impresa']['info-mesi']['info-mese'][$numeromesi-1]['@attributes']['n-dipendenti'] ;
        }else {
            $infoArray['ndipendenti']=$ndipe;
        }
        $infoArray['attivita-esercitata']=$array_rea['dati']['blocchi-impresa']['info-attivita']['attivita-esercitata'];
        return $infoArray;
    }
	/*
    private function GetInfoAziendaFromDenominazione($denominazione) {
        $data="denominazione=".$denominazione."&token=".$this->token."&user=".$this->user;
        $xml = $this->CallAPI('ricercaDenominazione', $data);


    }
	*/
    protected  function GetInfoAziendaFromVisuraOrdinaria($cciaa,$rea) {

        $data="cciaa=".$cciaa."&rea=".$rea."&token=".$this->token."&user=".$this->user;
        $req_datiri="cciaa=".$cciaa."&rea=".$rea."&user=".$this->user;
        /* Verifica se esiste una copia fresca di giornata e in caso affermativo
         * la recupera dal DB datiri_main
        */
        $query = "SELECT * FROM interrogazioni_telemaco WHERE url_chiamata = 'visuraOrdinaria:".$req_datiri."'";
        //echo $query;
        //exit;
        $num = $this->db->NumRows($query, Config::DB_DATIRI_NAME);
        //echo "N:".$num;
        //exit;
        $diffgg = 0;
        if ($num > 0){
        //Recupero la data del record
        $datareq = $this->db->GetRow($query, 'data_chiamata',Config::DB_DATIRI_NAME);
        $datanow = date('Y-m-d H:i:s');

        //echo "datareq:".$datareq;
        //$diffgg = getDiffgg($datareq, $datanow);
        $ts1 = strtotime($datareq);
        $ts2 = strtotime($datanow);

        $diffgg = (($ts2 - $ts1)/3600/24);
        //echo "diffgg:".$diffgg;
        //$diffgg = floor($seconds_diff/3600/24);
        //echo "N:".$num;
        //exit;
        }
        if (($num == 0) or ($diffgg > Config::EXPIRE_VIS_GG)) {
            $xml = $this->CallAPI('visuraOrdinaria', $data);

            $str=null;
            foreach ($xml as $k => $v) {
                $str .= "[$k] => $v\n";
            }

            $str = str_replace("'", "\'", $str);
            if ($diffgg > Config::EXPIRE_VIS_GG) {
                //Elimina le visure precedenti
                $queryst="DELETE FROM interrogazioni_telemaco WHERE url_chiamata='visuraOrdinaria:".$req_datiri."'";
                //echo $queryst;
                //exit;
                $this->db->Query($queryst, Config::DB_DATIRI_NAME);
            }
            $queryst = "INSERT INTO `interrogazioni_telemaco` (`user`,`url_chiamata`,`risposta`,`data_chiamata`)
						VALUES('Expo2015','visuraOrdinaria:".$req_datiri."','".$str."','".date('Y-m-d H:i:s')."')";
            $this->db->Query($queryst, Config::DB_DATIRI_NAME);
        } else {
            //Carica i dati da db
            $query = "SELECT * FROM interrogazioni_telemaco WHERE url_chiamata = 'visuraOrdinaria:".$req_datiri."'";
            $strRisposta = $this->db->GetRow($query, "risposta", Config::DB_DATIRI_NAME);

            //echo "Risposta: ".$strRisposta;
            $rispo = explode("=>", $strRisposta);
            $replace = trim(str_ireplace("[INFO]", '', $rispo[1]));
            //$replace = trim(str_ireplace(".", '', $replace));
            $strXML = trim($rispo[2]);

            $xml = array(
                    "STATUS" => $replace,
                    "INFO" => $strXML,
            );
            //print_r($xml);
            //exit;
        }

        return $xml;

    }

    private function GetPersoneFisicheFromNome($denominazione) {
        $data="denominazione=".$denominazione."&token=".$this->token."&user=".$this->user;
        return $this->CallAPI('schedaPersona', $data);
    }

    public function GetAziendeFromCfRappresentante($cf , $cacheMode = FALSE) {
        global  $debugIscrzione;
        
        if($debugIscrzione['visura']==false) {
            $data="codiceFiscale=".$cf."&token=".$this->token."&user=".$this->user;
            $req_datiri="codiceFiscale=".$cf."&user=".$this->user;
            /* Verifica se esiste una copia fresca di giornata e in caso affermativo
        	 * la recupera dal DB datiri_main
            */
            $query = "SELECT * FROM interrogazioni_telemaco WHERE url_chiamata = 'schedaPersona:".$req_datiri."'";
            //echo $query;
            //exit;
            $num = $this->db->NumRows($query, Config::DB_DATIRI_NAME);
            $diffgg = 0;
            if ($num > 0){
            //Recupero la data del record
            $datareq = $this->db->GetRow($query, 'data_chiamata',Config::DB_DATIRI_NAME);
            $datanow = date('Y-m-d H:i:s');

            //echo "datareq:".$datareq;
            //$diffgg = getDiffgg($datareq, $datanow);
            $ts1 = strtotime($datareq);
            $ts2 = strtotime($datanow);

            $diffgg = (($ts2 - $ts1)/3600/24);
            //echo "diffgg:".$diffgg;
            //$diffgg = floor($seconds_diff/3600/24);
            //echo "N:".$num;
            //exit;
            }
            if (($num == 0) or ($diffgg > Config::EXPIRE_SCP_GG)) {
            	
            	/* $cache mode == TRUE DISABILITA la chiamata a TELEMACO WEB SERVICE */
            	if ($cacheMode){

            		return false;
            	
            	}else{
            		/* $cache mode == FALSE ABILITA la chiamata a TELEMACO WEB SERVICE */ 
            		$xml= $this->CallAPI('schedaPersona', $data);
            		header('Content-type: text/xml');

            		$str=null;
            		foreach ($xml as $k => $v) {
            			$str .= "[$k] => $v\n";
            		}

            		$str = str_replace("'", "\'", $str);
            		if ($diffgg > Config::EXPIRE_SCP_GG) {
            			//Elimina le visure precedenti
            			$queryst="DELETE FROM interrogazioni_telemaco WHERE url_chiamata='schedaPersona:".$req_datiri."'";
            			//echo $queryst;
            			//exit;
            			$this->db->Query($queryst, Config::DB_DATIRI_NAME);
            		}
            		$queryst = "INSERT INTO `interrogazioni_telemaco` (`user`,`url_chiamata`,`risposta`,`data_chiamata`)
            				VALUES ('Expo2015','schedaPersona:".$req_datiri."','".$str."','".date('Y-m-d H:i:s')."')";
            		$this->db->Query($queryst, Config::DB_DATIRI_NAME);
            	}
            	
            } else {

                //Carica i dati da db
                $query = "SELECT * FROM interrogazioni_telemaco WHERE url_chiamata = 'schedaPersona:".$req_datiri."'";
                $strRisposta = $this->db->GetRow($query, "risposta", Config::DB_DATIRI_NAME);

                //echo "Risposta: ".$strRisposta;
                $rispo = explode("=>", $strRisposta);
                $replace = trim(str_ireplace("[INFO]", '', $rispo[1]));
                //$replace = trim(str_ireplace(".", '', $replace));
                $strXML = trim($rispo[2]);

                $xml = array(
                        "STATUS" => $replace,
                        "INFO" => $strXML,
                );
                /*
                print "<pre>";
                print_r($xml);
                print "</pre>";
                */
                //exit;
            }

        }else {
            
        	$filename=__DIR__."/xml/SchedaPersona.xml";
            $f=fopen($filename, "r");
            $schedaPersonaXml=fread($f, filesize($filename));
            fclose($f);
            $xml['STATUS']="SUCCESS";
            $xml['INFO']=$schedaPersonaXml ;
        }

       
       
        if($xml['STATUS']=="SUCCESS") {
        	
            $pos=strpos($xml['INFO'], "<NumeroPosizioni>0</NumeroPosizioni>");
            if($pos>0) {
				//Scheda Persona senza cariche da registrare
            }else {

//           $array = json_decode(json_encode((array)simplexml_load_string($xml['INFO'])),1);

                $array = XML2Array::createArray($xml['INFO']);
				
                //@json_decode(json_encode((array)simplexml_load_string($xml_rea['INFO'])),1);
                $this->SetCaricheFromPersona($array);
                
                if($array==false) {
                    return false;
                }
                $arrayImprese= array();
                $key_imprese=0;
                if(isset ($array['Risposta']['dati']['blocchi-persona']['imprese-persona']['impresa-persona'][0])) {
                    foreach ($array['Risposta']['dati']['blocchi-persona']['imprese-persona']['impresa-persona'] as $key => $value) {

                        $stato=$value['info-attivita']['@attributes']['stato'];
                        $carica=$value['persona']['cariche']['carica'];

                        $carica_verificata= "FALSE";
                        if(isset ($carica['@attributes'])) {

                            $qrt="SELECT Carica from EXPO_Tlk_Cariche_Rappresentanti where Sigla='".$carica['@attributes']['c-carica']."'";
                            if($this->db->NumRows($qrt)>0) {
                                $carica_verificata="TRUE";
                                $Carica_db=$this->db->GetRow($qrt,"Carica");
                                $nome_carica=$Carica_db;

                            }
                        }else {
							
                            foreach ($carica as $key_carica => $value_carica_arr) {

                                $qrt="SELECT Carica from EXPO_Tlk_Cariche_Rappresentanti where Sigla='".$value_carica_arr['@attributes']['c-carica']."'";

                                if($this->db->NumRows($qrt)>0) {
                                    $carica_verificata="TRUE";
                                    $Carica_db=$this->db->GetRow($qrt,"Carica");
                                    $nome_carica=$Carica_db;
                                    continue;
                                }
                            }
                            
                        }

                        $cfImpresa=$value['dati-identificativi-impresa']['@attributes']['c-fiscale'];
                        $tipostato=$value['procedure-concorsuali']['procedura-concorsuale']['@attributes']['tipo'];

                        $ctipostato=$value['procedure-concorsuali']['procedura-concorsuale']['@attributes']['c-tipo'];
                        if($ctipostato!='') {
                            $qrt="SELECT Sigla,Nome from EXPO_Tlk_Tipo_Contestazioni where Nome='".$tipostato."'";
                            if($this->db->NumRows($qrt)>0) {
                                continue;
                            }
                        }
                        if(strpos($stato,'INATTIVA')>0) {

                            continue;
                        }else {
                        	//Recupera il flag Rappresentante dalla tabella di Join
                        	$sql_cLR = "SELECT * FROM EXPO_TJ_Utente_NRea WHERE CodiceFiscale='$cf' AND Cciaa='".$value['dati-identificativi-impresa']['@attributes']['cciaa']."' AND NRea = '".$value['dati-identificativi-impresa']['@attributes']['n-rea']."';";
                        	//echo "$sql_cLR";
                        	//exit;
                        	$Carica_LR=$this->db->GetRow($sql_cLR,"LR");
                        	//echo "Par LR: ".$Carica_LR;
                        	
                        	if($carica_verificata=="TRUE" or $Carica_LR == "Y") {
								/*echo "Entrato LR<br/>".
								"Verificata: ".$carica_verificata."<br/>".
								"Par LR: ".$Carica_LR;
								exit;*/
                                $sql="SELECT StatoRegistrazione FROM  EXPO_T_Imprese where CodiceFiscale='".$cfImpresa."'";

                                if($this->db->NumRows($sql)==0) {


                                    $arrayImprese[$key_imprese]['denominazione']=$value['dati-identificativi-impresa']['@attributes']['denominazione'];
                                    $arrayImprese[$key_imprese]['n-rea']=$value['dati-identificativi-impresa']['@attributes']['n-rea'];
                                    $arrayImprese[$key_imprese]['cciaa']=$value['dati-identificativi-impresa']['@attributes']['cciaa'];
                                    $arrayImprese[$key_imprese]['c-fiscale']=$cfImpresa;
                                    $arrayImprese[$key_imprese]['stato_registrazione']=0;
                                    $arrayImprese[$key_imprese]['carica']=$nome_carica;
                                    $arrayImprese[$key_imprese]['stato_azienda']=$stato;

                                    $key_imprese++;

                                }else {
                                    $StatoRegistrazione = $this->db->GetRow($sql,'StatoRegistrazione');
                                    if($StatoRegistrazione<$this->numMaxStep) {
                                        $arrayImprese[$key_imprese]['denominazione']=$value['dati-identificativi-impresa']['@attributes']['denominazione'];
                                        $arrayImprese[$key_imprese]['n-rea']=$value['dati-identificativi-impresa']['@attributes']['n-rea'];
                                        $arrayImprese[$key_imprese]['cciaa']=$value['dati-identificativi-impresa']['@attributes']['cciaa'];
                                        $arrayImprese[$key_imprese]['c-fiscale']=$cfImpresa;
                                        $arrayImprese[$key_imprese]['stato_registrazione']=$StatoRegistrazione;
                                        $arrayImprese[$key_imprese]['carica']=$nome_carica;
                                        $arrayImprese[$key_imprese]['stato_azienda']=$stato;
                                        $key_imprese++;
                                    }
                                }
                            }
                        }

                    }

                }else {
                    $value=$array['Risposta']['dati']['blocchi-persona']['imprese-persona']['impresa-persona'];

                    $stato=$value['info-attivita']['@attributes']['stato'];
                    $carica=$value['persona']['cariche']['carica'];
					
					
                    $carica_verificata= "FALSE";
                    if(isset ($carica['@attributes'])) {

                        $qrt="SELECT Carica from EXPO_Tlk_Cariche_Rappresentanti where Sigla='".$carica['@attributes']['c-carica']."'";
                        if($this->db->NumRows($qrt)>0) {
                            $carica_verificata="TRUE";
                            $Carica_db=$this->db->GetRow($qrt,"Carica");
                            $nome_carica=$Carica_db;

                        }
                    }else {

                        foreach ($carica as $key_carica => $value_carica_arr) {

                            $qrt="SELECT Carica from EXPO_Tlk_Cariche_Rappresentanti where Sigla='".$value_carica_arr['@attributes']['c-carica']."'";

                            if($this->db->NumRows($qrt)>0) {
                                $carica_verificata="TRUE";
                                $Carica_db=$this->db->GetRow($qrt,"Carica");
                                $nome_carica=$Carica_db;
                                continue;
                            }
                        }
                    }


                    $cfImpresa=$value['dati-identificativi-impresa']['@attributes']['c-fiscale'];
                    $tipostato=$value['procedure-concorsuali']['procedura-concorsuale']['@attributes']['tipo'];

                    $ctipostato=$value['procedure-concorsuali']['procedura-concorsuale']['@attributes']['c-tipo'];
                    if($ctipostato!='') {
                        $qrt="SELECT Sigla,Nome from EXPO_Tlk_Tipo_Contestazioni where Nome='".$tipostato."'";
                        if($this->db->NumRows($qrt)>0) {
                            continue;
                        }
                    }
                    
                    if(strpos($stato,'INATTIVA')>0) {
                    	
                        //continue; //php Fatal ERROR quando l'utente entra nella selezione impresa
                    }else {

                    	//Recupera il flag Rappresentante dalla tabella di Join
                    	$sql_cLR = "SELECT * FROM EXPO_TJ_Utente_NRea WHERE CodiceFiscale='$cf' AND Cciaa='".$value['dati-identificativi-impresa']['@attributes']['cciaa']."' AND NRea = '".$value['dati-identificativi-impresa']['@attributes']['n-rea']."';";
                    	//echo "$sql_cLR";
                    	//exit;
                    	$Carica_LR=$this->db->GetRow($sql_cLR,"LR");
                    	//echo "Par LR: ".$Carica_LR;
                    	 
                    	if($carica_verificata=="TRUE" or $Carica_LR == "Y") {
                    	
                        //if($carica_verificata=="TRUE") {

                            $sql="SELECT StatoRegistrazione
                            FROM  EXPO_T_Imprese where CodiceFiscale='".$cfImpresa."'";

                            if($this->db->NumRows($sql)==0) {
                                $arrayImprese[$key_imprese]['denominazione']=$value['dati-identificativi-impresa']['@attributes']['denominazione'];
                                $arrayImprese[$key_imprese]['n-rea']=$value['dati-identificativi-impresa']['@attributes']['n-rea'];
                                $arrayImprese[$key_imprese]['cciaa']=$value['dati-identificativi-impresa']['@attributes']['cciaa'];
                                $arrayImprese[$key_imprese]['c-fiscale']=$cfImpresa;
                                $arrayImprese[$key_imprese]['stato_registrazione']=0;
                                $arrayImprese[$key_imprese]['carica']=$nome_carica;
                                $arrayImprese[$key_imprese]['stato_azienda']=$stato;
                                $key_imprese++;
                            }else {
                                $StatoRegistrazione = $this->db->GetRow($sql,'StatoRegistrazione');
                                if($StatoRegistrazione<$this->numMaxStep) {
                                    $arrayImprese[$key_imprese]['denominazione']=$value['dati-identificativi-impresa']['@attributes']['denominazione'];
                                    $arrayImprese[$key_imprese]['n-rea']=$value['dati-identificativi-impresa']['@attributes']['n-rea'];
                                    $arrayImprese[$key_imprese]['cciaa']=$value['dati-identificativi-impresa']['@attributes']['cciaa'];
                                    $arrayImprese[$key_imprese]['c-fiscale']=$cfImpresa;
                                    $arrayImprese[$key_imprese]['stato_registrazione']=$StatoRegistrazione;
                                    $arrayImprese[$key_imprese]['carica']=$nome_carica;
                                    $arrayImprese[$key_imprese]['stato_azienda']=$stato;
                                    $key_imprese++;
                                }
                            }
                        }
                    }


                }


            }
        }

        /*
        print "<h1>array telemaco imprese</h1>";
        print "<pre>";
        print_r($arrayImprese);
        print "</pre>";
        */
        
        return $arrayImprese;
    }

}



?>
