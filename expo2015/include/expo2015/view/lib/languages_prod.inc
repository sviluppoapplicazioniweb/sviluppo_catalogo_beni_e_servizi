<script language="javascript">

    function selezionaLingua(lang, id) {
        var par = 'lang=' + lang + '&Id=' + id
		        + "&Ordine=" + $("#Ordine").val()
		    	+ "&IsStudio=" + $("#IsStudio").val()
                + "&Nome=" + $("#Nome").val()
                + "&NomeEN=" + $("#NomeEN").val()
                + "&NomeFR=" + $("#NomeFR").val()

                + "&descrizione=" + $("#descrizione").val()
                + "&descrizioneEN=" + $("#descrizioneEN").val()
                + "&descrizioneFR=" + $("#descrizioneFR").val()
                + "&prodottoL=" + $("#prodottoL").val()

                + "&certificazione=" + $("#certificazione").val()
                + "&certificazioneEN=" + $("#certificazioneEN").val()
                + "&certificazioneFR=" + $("#certificazioneFR").val()
                + "&certificazioneL=" + $("#certificazioneL").val()

                + "&aspetti=" + $("#aspetti").val()
                + "&aspettiEN=" + $("#aspettiEN").val()
                + "&aspettiFR=" + $("#aspettiFR").val()
                + "&aspettiL=" + $("#aspettiL").val()
                + "&isSiexpo=" + $("#isSiexpo").val()

                + "&brevetti=" + $("#brevetti").val()
                + "&brevettiEN=" + $("#brevettiEN").val()
                + "&brevettiFR=" + $("#brevettiFR").val()
                + "&brevettiL=" + $("#brevettiL").val();

        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc",
            data: par,
            success: function(response) {
                $("#box_popup").html(response);
            }});
    }
    /*function selezionaLingua(lang, id) {
        $("#box_popup").load('ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc?Id=' + id + '&lang=' + lang);
    }*/
</script>
<ul>
<?php
$dbLang = new DataBase();

foreach ($dbLang->GetRows("SELECT Label,Lang FROM EXPO_T_Lingue_Sito ORDER BY Id") AS $rowLang) {
    echo "<li>";
    if (strcmp($Lang, $rowLang['Lang']) == 0)
        echo "<span>".$rowLang['Label']."</span>";
    else
        echo '<a class="' . strtolower($rowLang['Lang']) . '" onclick="selezionaLingua(\'' . $rowLang['Lang'] . '\',\'' . $IdProdotto . '\')" >' . $rowLang['Label'] . '</a>';
    echo "</li>";
}
?>
</ul>