<?php

/*$pathroot="/internet/datiExpo/include/";
require_once ($pathroot."configuration.inc");
*/
require_once("configuration.inc");
require_once("send_mail.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");


class GestioneListeExt {
	
	private $NOME_FILE_CLASS ="gestioneListeExt.class";
	private $MAILSITO;
	
	private $db;
	private $rootFile;
	private $fileCSV;
	private $arrayCSV;
	private $codiciFiscaliPresenti;
	private $tipo;
	private $uid;
	private $keySito;
	private $lineError;				  
	private $tipoConsentiti = array ('legaliRappresentanti','imprese');
	private $tracciato = array (
			'legaliRappresentanti' => array('Nome' => 0,'Cognome'=> 1,'Email' => 2,'CodiceFiscale'=> 3),
			'imprese'=> array('CodiceFiscale' => 0,'RagioneSociale' => 1)
	);
	
	private $campiTabella = array (
			'legaliRappresentanti' => array('Nome','Cognome','Email','CodiceFiscale','Stato'),
			'legaliRappresentanti_error' => array('Nome','Cognome','Email','CodiceFiscale','Stato'),
			'imprese'=> array('CodiceFiscale','RagioneSociale','IscrizioneSenzaPagamento','IscrizioneConPagamento'),
			'imprese_error'=> array('CodiceFiscale','RagioneSociale','Stato')
	);

	
	private $stato = array (
			'legaliRappresentanti' => array('N'=> 'Non Presente','P'=> 'Gi&agrave; Presente','C'=>'Creato'),
			'imprese'=> array('CodiceFiscale' => 0,'RagioneSociale' => 1)
	);
	
	private $numCampi;

	/**
	 * Costruttore della classe
	 */
	public function __construct($tipo,$uid,$keySito="1234567890abcdefghilmnop",$MAILSITO = "noreply.catalogue@expo2015.org"){

		if ($this->setTipoFile($tipo)){
			$this->db = new DataBase();
			$this->inizializaArray();
			$this->rootFile = Config::ROOTFILES."listeEsterne/$this->tipo/";
			$this->fileCSV = $this->tipo."_".$uid.".csv";
			$this->rootFile.$this->fileCSV;
			$this->uid=$uid;
			//$this->stampaArray($this->tracciato);
			$this->numCampi = count($this->tracciato[$this->tipo]);
			$this->keySito = $keySito;
			$this->MAILSITO = $MAILSITO;
		}else{
			scriviLog("Tipo file Errato", "Inserimento $this->tipo CSV","Caricamento $this->fileCSV utente: $this->uid");
			print "Tipo file Errato";
			exit;
		}

	}

	private function inizializaArray(){
		$this->codiciFiscaliPresenti = array();
		$this->arrayCSV = array();
		$this->lineError = array();
	}

	public function stampa(){
		print "<h1>arrayCSV</h1>";
		$this->stampaArray($this->arrayCSV);
		print "<h1>error line</h1>";
		$this->stampaArray($this->lineError);
	}
	private function stampaArray($array, $nome=null){
		print "<h4>stampa array $nome</h4>";
		print "<pre>";
		print_r($array);
		print "</pre>";
		//print "count: ".count($array);
	}

	/**
	 *
	 * @param String $tipo
	 */
	Private function setTipoFile ($tipo){
		if (in_array($tipo, $this->tipoConsentiti)){
			$this->tipo=$tipo;
			return true;
		}
		return false;
	}

	/**
	 * Funzione che sposta il file nella cartella temporanea dedicata.
	 */
	public function caricaCSV($fileTemp){
		
		if (move_uploaded_file($fileTemp["tmp_name"], $this->rootFile.$this->fileCSV)) {
			$this->leggiCSV($this->rootFile.$this->fileCSV);
		}else {
			echo $log="Errore Nel Caricamento del File";
			scriviLog("$log", "Inserimento $this->tipo CSV","Caricamento $this->fileCSV utente: $this->uid");
			return false;
		}
	}

	
	private function controllaIntestazione($data){
		//$this->stampaArray($data,"data intestazione");
		foreach ($data as $key => $value){
			
			//print "<br>controllo: $key -> $value";
			//$this->stampaArray($this->tracciato[$this->tipo],"tracciao intestazione");
			//print "<br>VALORE:".$this->tracciato[$this->tipo][$value];
			if (!$this->tracciato[$this->tipo][$value]==$key){
				//print "<h3>FALSE</h3>";
				return false;
			}
			
		}
		//print "<h3>true</h3>";
		return true;
		
	}
	
	/**
	 * Funzione che legge il fileCSV fisico
	 */
	private function leggiCSV($file){

		$row = 0;
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				//$this->arrayCSV[] = $data;
				//$this->stampaArray($data,"Riga: ".$row);
				if (!$this->controllaIntestazione($data)){
					$dataValid = $this->validaCSV($data,$row);
					$this->arrayCSV[$row] = $dataValid['Data'];
					if (!$dataValid['Check']){
						$this->lineError[]= $dataValid["Fault"];
					}else{
						$this->popolaDB($dataValid['Data']);
					}
				}
				$row ++;
			}
			fclose($handle);
		}
		
		$errorLog = "";
		if (count($this->lineError)>0){
			$errorLog = "Errori nel tracciato csv";
		} 
		scriviLog("Caricamento completato. $errorLog", "Inserimento $this->tipo CSV","Caricamento $this->fileCSV utente: $this->uid");
		print "<h2>Caricamento completato</h2>";

	}

	private function controlloDuplicazione($codiceFiscale){
		switch ($this->tipo){
			case "legaliRappresentanti":
				$duplicazioneSQL ="SELECT * FROM EXPO_T_EXT_Lista_LegaliRappresentanti WHERE CodiceFiscale='$codiceFiscale'";
				break;
			case "imprese": 		
				$duplicazioneSQL ="SELECT * FROM EXPO_T_EXT_Lista_Imprese WHERE CodiceFiscale='$codiceFiscale'";
				break;
		}
		$duplicazione = $this->db->GetRow($duplicazioneSQL,null,null,"Estrazione Duplicazione $this->tipo $NOME_FILE_CLASS LN.166");
		
		if ($duplicazione != null){
			return true;
			
		}
		return false;
			
	}

	private function popolaDB($data){
		
		if (!$this->controlloDuplicazione(($data[$this->tracciato[$this->tipo]['CodiceFiscale']]))){
			switch ($this->tipo){
				case "legaliRappresentanti":

					$statoLR = "N";
					if ($this->controllaPresenzaLR($data[$this->tracciato['legaliRappresentanti']['CodiceFiscale']])){
						$statoLR = "P";
					}

					//$this->stampaArray($data);
					//$this->stampaArray($this->tracciato['legaliRappresentanti'],"Tracciato");

					$legaliRappresentantiSQL = "INSERT into EXPO_T_EXT_Lista_LegaliRappresentanti (Id,Nome,Cognome,Email,CodiceFiscale,Stato,IdUtente)
							VALUE ((SELECT COALESCE(MAX(LR.Id),0)+1 FROM EXPO_T_EXT_Lista_LegaliRappresentanti as LR),'"
							.$data[$this->tracciato['legaliRappresentanti']['Nome']]."','".$data[$this->tracciato['legaliRappresentanti']['Cognome']]."','"
									.$data[$this->tracciato['legaliRappresentanti']['Email']]."','".$data[$this->tracciato['legaliRappresentanti']['CodiceFiscale']]
									."','$statoLR',$this->uid)";
					$this->db->Query($legaliRappresentantiSQL,null,null,"insertLegaliRappresentati $NOME_FILE_CLASS ln.195");

					break;
				case "imprese":
					
						$statoIMP = $this->controllaPresenzaImpresa($data[$this->tracciato['imprese']['CodiceFiscale']]);
						
						$iscrizioneConPagamento = "N";
						
						if ($statoIMP > 3){
							$iscrizioneConPagamento = "Y";
						}
						//$this->stampaArray($data);
						//$this->stampaArray($this->tracciato[$this->tipo],"Tracciato");

						$impreseSQL = "INSERT into EXPO_T_EXT_Lista_Imprese (Id,CodiceFiscale,RagioneSociale,IscrizioneConPagamento,IdUtente)
								VALUE ((SELECT COALESCE(MAX(IMP.Id),0)+1 FROM EXPO_T_EXT_Lista_Imprese as IMP),'"
								.$data[$this->tracciato['imprese']['CodiceFiscale']]."','".$data[$this->tracciato['imprese']['RagioneSociale']]."','$iscrizioneConPagamento',$this->uid)";
						$this->db->Query($impreseSQL,null,null,"insertImprese Lista $NOME_FILE_CLASS ln.213");
						
					break;
			}
		}

	}


	/**
	 * funzione che valida il formato del file CSV
	 */
	private  function validaCSV($data,$row){
		$check = true;
		$fault = array();
		if(count($data) != $this->numCampi){
			$fault['Error']['NumeroCampi']= false;
			$check = false;
		}
		$fault;
		foreach ($this->tracciato[$this->tipo] as $key => $dataIndex){
			$erroregrave=false;
			$pregret = 1;
			switch($key){
				case "CodiceFiscale":
					switch ($this->tipo){
						case "legaliRappresentanti":
							$pregret = preg_match("/^([0-9]{11}|[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z])$/", $data[$dataIndex]);
							break;
						case "imprese":
							$pregret = preg_match("/^([0-9]{11}|[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z])$/", $data[$dataIndex]);
							break;
					}
					
					break;
				case "Email":
					//$data[$dataIndex] = filter_var($data[$dataIndex], FILTER_SANITIZE_EMAIL);
					$pregret = preg_match("/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/", $data[$dataIndex]);
					break;
				case "Nome":
				case "Cognome":
				case "RagioneSociale":
					$tempValue = strip_tags($this->remove_noscript($this->remove_javascript($data[$dataIndex])));
					if (strcmp($data[$dataIndex], $tempValue) != 0){
						//print "<br>$data[$dataIndex]";
						//print "<br>$tempValue";
						$data[$dataIndex] = $tempValue;
						$pregret = 0;
					}
					$data[$dataIndex]  = htmlentities(trim($data[$dataIndex]),ENT_QUOTES );
					break;
			}
			if ($pregret!==1){
				$fault['Error'][$key]= false;
				$check = false;
			}
			if (!$check){
				$fault["Row"] = $row;
			}
		}
		
		return array ("Check" => $check,"Data" => $data, "Fault" => $fault);
	}


	private function controllaPresenzaLR($codiceFiscale){

		$querySQL = "SELECT * FROM	T_Anagrafica WHERE Codice_Fiscale ='$codiceFiscale'";
		$resultQuery = $this->db->GetRow($querySQL,null,null,"Estrazione LR $NOME_FILE_CLASS LN.276");
		
		if ($resultQuery != null){
			return true;
		}
		
		return false;
	}


	private function controllaPresenzaImpresa($codiceFiscale){

		$querySQL = "SELECT StatoRegistrazione FROM	EXPO_T_Imprese WHERE CodiceFiscale ='$codiceFiscale'";
		$resultQuery = $this->db->GetRow($querySQL,null,null,"Estrazione Imprese $NOME_FILE_CLASS LN.289");

		if ($resultQuery != null){
			return $resultQuery['StatoRegistrazione'];
		}

		return 0;
	}

	
	
	
	
	public function stampaElenco(){
	$campiSQL = '';
	foreach ($this->campiTabella[$this->tipo] as $key => $field){
		$campiSQL .= "$field,";
	}
	$campiSQL = substr($campiSQL, 0, -1);
	switch ($this->tipo){
		case "legaliRappresentanti":
			$elencoSQL = 	"SELECT $campiSQL FROM EXPO_T_EXT_Lista_LegaliRappresentanti WHERE IdUtente =$this->uid Order By Id";			
			break;
		case "imprese":
			$elencoSQL = 	"SELECT $campiSQL FROM EXPO_T_EXT_Lista_Imprese WHERE IdUtente =$this->uid Order By Id";
			break;
	}
	$elencoLR = $this->db->GetRows($elencoSQL,null,"Estrazione Elenco LR $NOME_FILE_CLASS LN.316");
	//$this->stampaArray($elencoLR,"ELENCO LR");	
	
	print $this->createTableELenco($elencoLR);
	}
	
	
	private function createTableELenco($elenco){
		$table ='';
		if (count($elenco) > 0){
			$table .= '<table id="tabella_elenchi" name="'.$this->tipo.'">';
			$table .= '<tr class="tdHeader" style="border-right: 0;">';
			foreach ($this->campiTabella[$this->tipo] as $key => $field){
				$table .="<th>$field</th>";
			}
			$table .= '</tr>';
			$table .= '<tbody>';
			$isColor = 1;
			foreach ($elenco AS $key => $row){
				$table .="<tr>";
				foreach ($this->campiTabella[$this->tipo] as $key => $field){
					$campo = $row[$field];
					if (strcasecmp($field, "Stato") == 0){
						$campo = $this->stato[$this->tipo][$row[$field]];
					}
					$table .="<td".getColor("lineTable", $isColor).">$campo</td>";
				}
				$table .="</tr>";
				$isColor++;
			}

			$table .= '</tbody>';
			$table .= "</table>";
		}
		return $table;
	}
	
	public function numLegaliDaCreare(){
		$utenzeDaCreareSQL = "SELECT count(CodiceFiscale) as Num 
		FROM EXPO_T_EXT_Lista_LegaliRappresentanti
		WHERE IdUtente = $this->uid AND Stato = 'N' Order By Id";
		$utenzeDaCreare = $this->db->GetRow($utenzeDaCreareSQL,'Num',null,"Creazioni legali rappresentanti $NOME_FILE_CLASS ln. 357");

		return $utenzeDaCreare;
		
	}
	
	
	public function creaLR(){
		$utenzeDaCreareSQL = "SELECT Nome,Cognome,CodiceFiscale,Email 
								FROM EXPO_T_EXT_Lista_LegaliRappresentanti
								WHERE IdUtente = $this->uid AND Stato = 'N' Order By Id";
		$utenzeDaCreare = $this->db->GetRows($utenzeDaCreareSQL,null,"Creazioni legali rappresentanti $NOME_FILE_CLASS ln. 368");
		$i=0;
		foreach ($utenzeDaCreare as $key=> $utenza){
			$this->creaUtenza($utenza);
			$i++;
		}
		$log = "Create $i utenze";
		scriviLog("$log", "Creazione utenze  $this->tipo CSV","Creazione utenze $this->fileCSV utente: $this->uid");
	}
	

	private function creaUtenza($data){
		
		//$this->stampaArray($data,"LR");
		
		$nome = $data['Nome'];
		$cognome = $data['Cognome'];
		$CFISC = $data['CodiceFiscale'];
		$sendMail = $username = $mail = $data['Email'];
		
		$sendMail = "fabio.bertoletti@digicamere.it";
		
		/*$presente = $this->db->GetRow("SELECT Id FROM T_Anagrafica WHERE Login = '$username'", "Id");
		if (strcmp($presente, "") != 0) {
			print "<div class=\"duplicateMail\"><img src=\"/images/expo2015/alert.png\" width=\"24px\"> "._EMAIL_PRESENTE_."</div><br><br>";
		} else {*/
			//print "<br>SELECT Id FROM T_Anagrafica WHERE Codice_Fiscale='$CFISC'";
			$presente = $this->db->GetRow("SELECT Id FROM T_Anagrafica WHERE Codice_Fiscale='$CFISC'", "Id",null,"$NOME_FILE_CLASS ln. 392");
			//print "<br>Presente:". $presente;
			
			if (strcmp($presente, "") != 0) {								
				$updateLR ="UPDATE EXPO_T_EXT_Lista_LegaliRappresentanti Set Stato = 'P' WHERE CodiceFiscale = '$CFISC' AND Stato != 'C'";
				$this->db->Query($updateLR,null,"Utenza trovata $this->NOME_FILE_CLASS ln.379");
				//print "<div class=\"duplicateMail\"><img src=\"/images/expo2015/alert.png\" width=\"24px\"> "._LR_PRESENTE_."</div><br><br>";
				//exit;
			} else {
				$pass = passwordCasuale(8);
				
				$sqlMaxId = "(SELECT @idUtente := COALESCE(MAX(TA.Id),0)+1 AS Id FROM T_Anagrafica AS TA)";
				$return = $this->db->Query("INSERT INTO T_Anagrafica VALUES($sqlMaxId,'$nome','$cognome','','','','$mail','','','','',0,'',0,'','','','','$CFISC','',NOW(),'$username','$pass','2015-12-31','D')",null,"$NOME_FILE_CLASS ln.386");
				if ($return != false){
					//$idUtente = $this->db->GetRow("SELECT @idUtente AS idUtente", 'idUtente',null,"estrazione idUtente $NOME_FILE_CLASS ln 388");
					$this->db->Query("INSERT INTO T_Utente VALUES((SELECT @idUtente),NULL,0,0,NULL,0,'96','|',0,'|','|','','','|','',1,'|','',0,'',0)",null,"$NOME_FILE_CLASS ln 389");
					$updateLR ="UPDATE EXPO_T_EXT_Lista_LegaliRappresentanti Set Stato = 'C' WHERE CodiceFiscale ='$CFISC'";
			
				}
				$passUrl = urlencode(base64_encode($pass));
				
				$utente = $nome . ' ' . $cognome;
				$link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $mail . "&cyph=" . md5($mail . $this->keySito) . "&Lang2=IT&action=INS";
		
				if(send_mail($sendMail, $this->MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true)){
					$this->db->Query($updateLR,null,"$this->NOME_FILE_CLASS ln.391");
				}
				//echo _OK_INVIO_LINK_;
				//exit;
			}
		//}
	}
	
	public function numLegaliPresenti(){
		$utenzePresentiSQL = "SELECT count(CodiceFiscale) as Num
		FROM EXPO_T_EXT_Lista_LegaliRappresentanti
		WHERE IdUtente = $this->uid";
		$utenzePresenti = $this->db->GetRow($utenzePresentiSQL,'Num',null,"Presenze legali rappresentanti $NOME_FILE_CLASS ln. 338");
	
		return $utenzePresenti;
	
	}
	
	public function errorCSV(){
		//$this->stampaArray($this->arrayCSV,"array CSV");
		//$this->stampaArray($this->lineError,"errore");
		
		$table ='';
		if (count($this->lineError) > 0){
			print "<h2>Errori riscontrati nel file CSV</h2>";
			$table .= '<table id="tabella_elenchi" name="'.$this->tipo.'_error">';
			$table .= '<tr class="tdHeader" style="border-right: 0;">';
			foreach ($this->campiTabella[$this->tipo."_error"] as $key => $field){
				$table .="<th>$field</th>";
			}
			$table .= '</tr>';
			$table .= '<tbody>';
			$isColor = 1;
			foreach ($this->lineError AS $key => $row){
				$table .="<tr".getColor("lineTable", $isColor).">";
				foreach ($this->campiTabella[$this->tipo."_error"] as $key => $field){
					$campo = $this->arrayCSV[$row['Row']][$key]; 
					if (strcasecmp($field, "Stato") == 0){
						
						$campo='<font style="font-weight: bold; color: red;">';
						foreach ($row['Error'] AS $keyError => $valid){
							if (!$valid){
								$campo .= $keyError. " Error<br>";
							}
							
						}
						$campo .='</font>';
					}
					$table .="<td>$campo</td>";
				}
				$table .="</tr>";
				$isColor++;
			}
		
			$table .= '</tbody>';
			$table .= "</table><br /><br />";
		}
		print $table;
		}
	
	
	private function remove_javascript($java){
	
		return preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $java);
		//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);
	
	}
	
	private function remove_noscript($java){
	
		return preg_replace('~<\s*\bnoscript\b[^>]*>(.*?)<\s*\/\s*noscript\s*>~is', '', $java);
		//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);
	
	}
	
	
	public function numImpresePresenti(){
		$impresePresentiSQL = "SELECT count(CodiceFiscale) as Num
		FROM EXPO_T_EXT_Lista_Imprese
		WHERE IdUtente = $this->uid";
		$impresePresenti = $this->db->GetRow($impresePresentiSQL,'Num',null,"Presenze imprese $NOME_FILE_CLASS ln. 338");
	
		return $impresePresenti;
	
	}

	private function ControlloPIVA($pi){
		if( $pi == '' )  return false;
		if( strlen($pi) != 11 ){
			//print "!= 11";
			return false;
		}
		if( ! ereg("^[0-9]+$", $pi) ){
			//print "!= numeri";
			return false;
		}
		$s = 0;
		for( $i = 0; $i <= 9; $i += 2 )
			$s += ord($pi[$i]) - ord('0');
		for( $i = 1; $i <= 9; $i += 2 ){
			$c = 2*( ord($pi[$i]) - ord('0') );
			if( $c > 9 )  $c = $c - 9;
			$s += $c;
		}
		if( ( 10 - $s%10 )%10 != ord($pi[10]) - ord('0') )
			return false;
		return true;
	}
	
	
	/**
	 * Funzione che elimina il file
	 * @param unknown $dir
	 * @param unknown $subDir
	 * @param unknown $nome
	 */
	public function deleteFile($dir, $subDir, $nome) {
		$pathDir = $this->rootFile . $dir . $subDir;
		@unlink($pathDir . "/" . $nome);
	}

}