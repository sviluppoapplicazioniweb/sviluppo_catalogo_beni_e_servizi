<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Anagrafica
 *
 * @author XddG123
 */
require_once($PROGETTO . "/view/lib/TelemacoWs.php");
require_once($PROGETTO . "/view/lib/db.class.php");

class Anagrafica  extends TelemacoWs {
    //put your code here

    private $cf,$db;

    public function  __construct($cf) {
        $this->cf=$cf;
        $this->db=new DataBase();
    }

    private function getAziendeFromCF(){
       $arrayImprese=$this->GetAziendeFromCfRappresentante($this->cf);
       return $arrayImprese;
    }

    public function AllineaGestoriAzienda(){
        $allAziende=$this->getAziendeFromCF();
        foreach ($allAziende as $key => $value) {
            $cfiscale=$value['c-fiscale'];
            $sql="SELECT Id from EXPO_T_Imprese where CodiceFiscale = '".$cfiscale."'";
            if($this->db->NumRows($sql)>0){
               $idAzienda=$this->db->GetRow($sql,'Id');
               $sqlAnagrafica="select Id from T_Anagrafica where Codice_Fiscale = '".$this->cf."'";
               $idUser=$this->db->GetRow($sqlAnagrafica,'Id');
               $sqlImpreseAnagrafica="SELECT * FROM EXPO_TJ_Imprese_Utenti where IdUtente='".$idUser."' and IdImpresa = '".$idAzienda."'";
               if($this->db->NumRows($sqlImpreseAnagrafica)==0){
                   $insertImpreseAnagrafica="
                       INSERT EXPO_TJ_Imprese_Utenti (Id,IdImpresa,IdUtente)
                       values((SELECT MAX(i.Id)+1 from EXPO_TJ_Imprese_Utenti as i),'".$idAzienda."','".$idUser."')";
                   $this->db->Query($insertImpreseAnagrafica);
               }
            }
        }
    }

}
?>
