<?php

/* * *********************************************************************
 * ************ D A T A B A S E   C L A S S *****************************
 * *********************************************************************
 *
 * Filename:    db.class.php
 * Version:     1.1
 * Author:      Mr.DoT <admin@mrdot.it>
 * License:     GNU/GPL
 * Release:     16th November, 2012
 * Description: Questo script consente una gestione più semplificata del
 *              database MySQL da script PHP
 *
 * Changelog:
 *      1.1     - Privatizzate le variabili utilizzate per connettersi
 *                al database (utente, password, ecc.);
 *              - Eliminato le variabili globali, snellendo di parecchio
 *                tutto il codice;
 *              
 *      1.0 - Versione iniziale
 * ******************************************************************* */

include_once("configuration.inc");

class DataBaseParix {

    private $connected = false; // Variabile di controllo che rileva se
    // la connessione al
    // database è attiva oppure no
    private $lastConnection;    // Variabile che identifica l'ultimo
    // collegamento al database
    private $dbhost, // Indirizzo server MySQL
            $dbuser, // Nome utente MySQL
            $dbpwd, // Password MySQL
            $dbname;            // Nome database

    // Costruttore di classe

    public function __construct() {
        // Inizializzo le variabili di classe, per accedere a MySQL
        $this->dbhost = Config::DB_PARIX_SERVER;
        $this->dbuser = Config::DB_PARIX_USER;
        $this->dbpwd = Config::DB_PARIX_PASSWORD;
        $this->dbname = Config::DB_PARIX_MI_NAME;
    }
    
    public function SelectServer($dbhost,$dbuser,$dbpwd,$dbname=null) {
    	// Inizializzo le variabili di classe, per accedere a MySQL
    	$this->dbhost = $dbhost;
    	$this->dbuser = $dbuser;
    	$this->dbpwd = $dbpwd;
    	$this->dbname = $dbname;
    	
    	/*
    	print "DB HOST: $this->dbhost<br />";
    	print "DB USER: $this->dbuser<br />";
    	print "DB PASSWORD: $this->dbpwd<br />";
    	print "DB NAME: $this->dbname<br />";
    	
    	*/
    }
    

    // Apre la connessione con il database
    public function OpenConnection() {
        // Se è già connesso, esco dalla funzione
        if ($this->connected)
            return $this->lastConnection;

        // Effettuo la connessione al database
        $link = mysql_connect($this->dbhost, $this->dbuser, $this->dbpwd);

        // Se non ci sono stati errori di connessione:
        if ($link) {
            // Attivo la connessione
            $this->connected = true;
            // Memorizzo l'ultima connessione
            $this->lastConnection = $link;
            // Esco dalla funzione
            return $link;
        }
        // Se ci sono stati errori di connessione restituisco FALSE
        return false;
    }

    // Chiude la connessione con il database
    public function CloseConnection($link = null) {
        // Se è già disconnesso, esco dalla funzione
        if (!$this->connected)
            return true;

        // Se non viene specificato $link uso l'ultimo collegamento
        if (!$link)
            $link = $this->lastConnection;

        // Se la disconnessione dal database è avvenuta senza errori:
        if (mysql_close($link)) {
            // Disattivo la connessione ed esco dalla funzione
            $this->connected = false;
            return true;
        }
        // Se invece ci sono stati errori esco dalla funzione con FALSE
        return false;
    }

    // Seleziona un database
    public function SelectDatabase($db_name = null, $link = null) {
        // Imposto il nome del database di default, se non specificato
        if (!$db_name)
            $db_name = $this->dbname;

        // Imposto la connessione di default, se non viene specificato
        if (!$link)
            $link = $this->lastConnection;

        // Seleziono il database. Se non ci sono errori esco dalla funzione
        if (mysql_select_db($db_name, $link))
            return true;

        // Se invece ci sono errori esco dalla funzione con FALSE
        return false;
    }

    // Esegue una query SQL
    public function Query($query, $db_name = null, $from = null) {
        // Mi connetto al database (se disconnesso)
        $link = $this->OpenConnection();
        if (!$link)
            return false;

        // Seleziono il database
        if (!$this->SelectDatabase($db_name))
            return false;

        // Eseguo la query
        $result = mysql_query($query, $link) or ($errorlog=mysql_error());     
        if($errorlog!=''){
	        $f=fopen(__DIR__ ."/Log/Error_Log_".date("Y-m-d",time()).".log",'a+');
	        fwrite($f, $errorlog."--".$query."\n");
	        fclose($f);
	        include_once("send_mail.inc");
	        send_mail_html('alessandro.ferla@digicamere.it; fabio.bertoletti@digicamere.it', 'noreply.catalogue@expo2015.org', "Svil. Alert DBPARIX: SQL From: ".$from, $errorlog."--".$query."\n");
        }
        if (!$result)
            return false;

        // Mi disconnetto dal database (se connesso)
        if (!$this->CloseConnection())
            return false;

        // Infine, se non ci sono problemi, esco dalla funzione
        // restituendo il risultato della query
        return $result;
    }

    // Calcola il numero di righe di una determinata query
    public function NumRows($query, $db_name = null, $from = null) {
    	        
        // Esegue una query
        $result = $this->Query($query, $db_name, $from);

        // Restituisce il numero di righe della query presa in oggetto
        return mysql_num_rows($result);
    }

    // Ottiene un array di valori di una singola riga (la prima trovata),
    // in base alla query,
    // oppure un singolo valore, specificando $key (indice della riga)
    public function GetRow($query, $key = null, $db_name = null, $from = null) {
        // Ottengo i risultati della query
       
        
        $result = $this->Query($query, $db_name,$from);

        // Ottengo l'array della prima riga, dai risultati di prima
        $row = mysql_fetch_array($result);

        // Se è specificato il nome della colonna ($key) restituisco
        // il valore specifico, altrimenti tutta la riga ($row)
        if ($key)
            return $row[$key];
        else
            return $row;
    }

    public function GetRows($query, $db_name = null, $from = null) {
        // Ottengo i risultati della query
        $result = $this->Query($query, $db_name,$from);
        $rows = array();
        while ($row = mysql_fetch_array($result))
        // Ottengo l'array della prima riga, dai risultati di prima
            $rows[] = $row;

        // Se è specificato il nome della colonna ($key) restituisco
        // il valore specifico, altrimenti tutta la riga ($row)

        return $rows;
    }

}

?>
