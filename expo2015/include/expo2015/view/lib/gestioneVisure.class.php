<?php

require_once($PROGETTO . "/view/lib/TelemacoWs.php");
require_once($PROGETTO . "/view/lib/parix.class.php");

class GestioneVisure{
	
	private $telemacoWs;
	private $parix;
	
	const CHIAMATA_PARIX = "Parix";
	const CHIAMATA_TELEMACO = "Telemaco";
	
	const PRINT_LISTA_IMPRESE = false;
	
	public function __construct(){
		$this->telemacoWs = new TelemacoWs();
		$this->parix = new Parix();
	}
	
	/**
	 * Funzione che gestisce l'estazione delle imprese associate ad un legale rappresentante, identificato per codice fiscale.
	 * 
	 * 
	 * 
	 * @param String $codiceFiscale
	 * @param String $chiamata default "Parix". Altro valore accettato "Telemaco"
	 * 
	 * 
	 * @return array ("chiamata", "listaImprese")
	 */
	public function GetAziendeFromCfRappresentante($codiceFiscale, $chiamata = self::CHIAMATA_PARIX){
		
		
		// se la chimata � differenta da Parix o Telemaco imposto Parix
		if ( (strcmp($chiamata, self::CHIAMATA_PARIX) != 0) && (strcmp($chiamata, self::CHIAMATA_TELEMACO) != 0 ) ){
			$chiamata = self::CHIAMATA_PARIX;
		}
		
		//se chimata Parix
		if (strcmp($chiamata, self::CHIAMATA_PARIX) == 0){

			// verifico l'esistenza nella cache di telemaco della scheda persona
			$arrayListaImprese = $this->telemacoWs->GetAziendeFromCfRappresentante($codiceFiscale, TRUE);

			if (self::PRINT_LISTA_IMPRESE){
				print "<h1>IMPRESE DA CACHE TELEMACO</h1><pre>";
				print_r($arrayListaImprese);
				print "</pre>";
			}
			
			if (is_array($arrayListaImprese) && (count($arrayListaImprese) > 0)){
				return array ("chiamata" => self::CHIAMATA_TELEMACO ,"listaImprese" => $arrayListaImprese);
			}
			
			// verifico l'esistenza nella cache di telemaco della scheda persona
			$arrayListaImprese = $this->parix->GetAziendeFromCfRappresentante($codiceFiscale);
			
			if (self::PRINT_LISTA_IMPRESE){
				print "<h1>IMPRESE PARIX</h1><pre>";
				print_r($arrayListaImprese);
				print "</pre>";
			}
			
			if (count($arrayListaImprese) > 0){

				return array ("chiamata" => self::CHIAMATA_PARIX ,"listaImprese" => $arrayListaImprese);
			} else {

				$chiamata = self::CHIAMATA_TELEMACO;
			}
		}
		
		if (strcmp($chiamata, self::CHIAMATA_TELEMACO) == 0){

			
			$arrayListaImprese = $this->telemacoWs->GetAziendeFromCfRappresentante($codiceFiscale);
			
			if (self::PRINT_LISTA_IMPRESE){
				print "<h1>IMPRESE DA TELEMACO</h1><pre>";
				print_r($arrayListaImprese);
				print "</pre>";
			}
				
			if (is_array($arrayListaImprese)){
			
				return array ("chiamata" => self::CHIAMATA_TELEMACO ,"listaImprese" => $arrayListaImprese);
			}
		}
		
		
	}

	
	public function GetInfoAziendaFromCf($CFS){

		$CFSExplode = explode("-", $CFS);
		$cf = $CFSExplode[0];
		$NRea = $CFSExplode[1];
		$cciaa = $CFSExplode[2];
		
		
		$arrayInfoImpresa = array();
		
		if ( (strcmp($cciaa,"MI") == 0 ) || (strcmp($cciaa,"MB") == 0 )){
			
			$arrayInfoImpresa = $this->parix->GetInfoAziendaFromCf($NRea, $cciaa);
		}
		
		if (count($arrayInfoImpresa) > 0){
			/*
			Print "<h1> ho trovato imprese</h1>";
			print "<pre>";
			print_r($arrayInfoImpresa);
			print "</pre>";
			*/
			return $arrayInfoImpresa;
		}
		
		//Print "<h1> NON ho trovato imprese</h1>";
		
		$arrayInfoImpresa = $this->telemacoWs->GetInfoAziendaFromCf($CFS);
		
		return $arrayInfoImpresa;
		
	}
	
	
}

?>