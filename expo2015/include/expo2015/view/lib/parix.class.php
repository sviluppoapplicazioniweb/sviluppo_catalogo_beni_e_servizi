<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("configuration.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/db.parix.class.php");
require_once($PROGETTO . "/view/lib/XML2Array.php");

/**
 * Description of parix
 *
 * @author Francesco Spagnoli
 */
class Parix {

	
    public $db;
    public $dbParix;

    public function __construct() {
        $this->db = new DataBase();
        $this->dbParix = new DataBaseParix();
        //$this->dbParix->SelectServer(Config::DB_PARIX_SERVER, Config::DB_PARIX_USER, Config::DB_PARIX_PASSWORD); 
    }

    /**
     * Funzione che logga le chiamate a parix dei legali rappresentati
     * @param $cf
     * @param $arrayListaImprese
     */
    private function logLegaleRappresentante($cf, $arrayListaImprese){
    	$azienda = "";
    	foreach ($arrayListaImprese as $key => $value){
    		$valueCodFisc = $value['c-fiscale'];
    		$valueNRea = $value['n-rea'];
    		$valueCciaa = $value['cciaa'];
    		$valueDenominazione = mysql_escape_string($value['denominazione']);
    		$valueStato = $value['stato_registrazione'];
    	
    		$azienda .= "<azienda>
    		<c-fiscale>$valueCodFisc</c-fiscale>
    		<n-rea>$valueNRea</n-rea>
    		<cciaa>$valueCciaa</cciaa>
    		<denominazione>$valueDenominazione</denominazione>
    		<stato_registrazione>$valueStato</stato_registrazione>
    		</azienda>
    		";
    	}
    		 
    		$xml="<?xml version = \'1.0\' ?>
    		<scheda-persona>
    		<codice-fiscale>$cf</codice-fiscale>
    		<aziende>
    		$azienda
    		</aziende>
    		</scheda-persona>";
    		 
    		 
    		$req_datiri="codiceFiscale=".$cf."&user=parix";
   
    	$queryst = "INSERT INTO `interrogazioni_parix` (`user`,`url_chiamata`,`risposta`,`data_chiamata`)
            				VALUES ('Expo2015','schedaPersona:".$req_datiri."','".$xml."','".date('Y-m-d H:i:s')."')";
    	$this->db->Query($queryst, Config::DB_DATIRI_NAME);
    	
    }
    
    
    /**
     * Funzione che logga le chiamate a parix delle aziende
     * @param $cf
     * @param $arrayListaImprese
     */
    private function logAzienda($arrayInfoImpresa){
	   	$denominazione = "<![CDATA[\"".$arrayInfoImpresa['denominazione']."\"]]>";
	   	$piva = $arrayInfoImpresa['PIva'];
	   	
	   	$xml="<?xml version = \'1\' ?>\n<visura-parix>\n";
    	foreach ($arrayInfoImpresa as $key => $value){
    		
    		$openTag = "<$key>";
	   		
    		$text = "";
    		if (strcmp($key,"denominazione") == 0 ){
    			$text = "<![CDATA[\"".mysql_escape_string($value)."\"]]>";
    		}else
    		if (strcmp($key,"Categorie") == 0 ){
    			
    			foreach ($arrayInfoImpresa['Categorie'] as $categoria){
    				foreach ($categoria as $keyCat => $valueCat){
    					$openCatTag = "<$keyCat>";
    					$textCat = mysql_escape_string($valueCat);
    					$closeCatTag = "</$keyCat>\n";

    					$text .= $openCatTag . $textCat . $closeCatTag;
    				}
    			}
    			
    		} else{

    			$text = mysql_escape_string($value);
    		}
    		
	   		$closeTag = "</$key>\n";
	   		
    		
    		$xml .= $openTag . $text . $closeTag; 
    	}
    
    	$xml .= "</visura-parix>";
    	 
    	 
    	$req_datiri="cciaa=".$arrayInfoImpresa['SglPrvSede']."&rea=".$arrayInfoImpresa['NRea']."&user=parix";
    	
    	
    	$queryst = "INSERT INTO `interrogazioni_parix` (`user`,`url_chiamata`,`risposta`,`data_chiamata`)
            				VALUES('Expo2015','visuraOrdinaria:".$req_datiri."','".$xml."','".date('Y-m-d H:i:s')."')";
    	$this->db->Query($queryst, Config::DB_DATIRI_NAME);
    	
    }
    
    private function popolaTjUtenteNRea ($cf,$carica,$cciaa,$nrea,$codFiscImp){
    	
    	if($carica!='') {
    		$sql_="SELECT * from EXPO_TJ_Utente_NRea where  CodiceFiscale='".$cf."'and Cciaa='".$cciaa."' and NRea='".$nrea."'";
    		if($this->db->NumRows($sql_)==0) {
    			$query="INSERT INTO EXPO_TJ_Utente_NRea (Id,CodiceFiscale,Ruolo,Cciaa,NRea,CFiscImp)
                        VALUES(
                        (SELECT COALESCE(MAX(A.Id),0)+1 from EXPO_TJ_Utente_NRea as A),'".$cf."','".$carica."',
                           '".$cciaa."','".$nrea."','".$codFiscImp."' )";
    			$this->db->query($query);
    			}
    	}
    	
    }
    
    private function VerificaForzaturaCarica ($cf,$carica,$cciaa,$nrea){
    	 
    	if($carica!='') {
    		$sqlLR="SELECT LR from EXPO_TJ_Utente_NRea where  CodiceFiscale='".$cf."'and Cciaa='".$cciaa."' and NRea='".$nrea."'";
    		$LR = $this->db->GetRow($sqlLR,'LR');
    		//print $LR;print"<br>";
    		if (strcmp($LR, "Y") == 0){
    			
    			return true;
    		}
    	}
    	 
    	return false;
    }
    
    
    /**
     * Funzione che estrae, da un Database parix passoto per paramentro, la lista delle imprese associate al $cf passato.
     * 
     * @param string $cf
     * @param string $db
     * @return array
     */
    private function GetAziendeCfRappresentanteFromParix($cf,$db) {
        $arrayImprese = array();
        $arrayListaImprese = array();

        /*
        $sqlCariche = "SELECT Sigla FROM EXPO_TLK_Cariche_Rappresentanti";
        $cariche = $this->db->GetRows($sqlCariche);
        $caricheList = "(";
        foreach ($cariche as $carica){
        	$caricheList .= "'".$carica['Sigla']. "', ";	
        }
        $caricheList = substr($caricheList, 0, -2);
        $caricheList .= " )";
        */
        
        
        
        //LR.C_CARICA AS carica, 
        //,LR.C_CARICA AS carica,
        $query = "SELECT DISTINCT(I.C_FISCALE_IMPRESA) AS c_fiscale, LR.n_rea AS n_rea,LR.cciaa AS cciaa,LR.C_CARICA AS carica, 
        			 I.DENOMINAZIONE AS denominazione
					FROM persone_tot AS LR join imprese_tot AS I on LR.n_rea = I.n_rea
					WHERE LR.pf_codice_fiscale = '$cf' AND LR.TIPO_PERSONA = 'F' AND I.T_LOCALIZZAZIONE = 'SE' AND I.I_STATO_ATTIVITA = 'A'";
        			//AND LR.C_CARICA IN $caricheList";
        
        
        //print "<br><h1>$db</h1><br>";
        //print $query;
        //print "<br><h1>..---...--...---..</h1><br>";
        
        $legali = $this->dbParix->GetRows($query,$db,"Estrazione Imprese Legale Rappresentante da ".$db);
        
        /*print "<h1>legali rappresentati</h1><pre>";
        print_r($legali);
        print "</pre><br>";
        print "++++++++++++++++++++++++++++++++++++++++++++++++++<br>";
        
        
        print "<br>";*/
        $imprese = array();
        foreach ($legali AS $row) {
			$carica = $row['carica'];
        	$this->popolaTjUtenteNRea($cf, $carica, $row['cciaa'],$row['n_rea'],$row['c_fiscale']);
        	
        	$sqlCaricaValida = "SELECT Sigla FROM EXPO_TLK_Cariche_Rappresentanti WHERE Sigla = '$carica'";
        	$caricaForzata = $this->VerificaForzaturaCarica($cf, $carica, $row['cciaa'],$row['n_rea']);
        	/*print $sqlCaricaValida;
        	print "<br>";*/
        	if( ($this->db->NumRows($sqlCaricaValida) == 1) || ( $caricaForzata ) ) {
        		$imprese[$row['c_fiscale']]["c_fiscale"] = $row['c_fiscale'];
        		$imprese[$row['c_fiscale']]["n_rea"] = $row['n_rea'];
        		$imprese[$row['c_fiscale']]["cciaa"] = $row['cciaa'];
        		$imprese[$row['c_fiscale']]["denominazione"] = $row['denominazione'];
        		$imprese[$row['c_fiscale']]["carica"][] = $row['carica'];
        	}
        }
        
        
        
        
        /*
        print "<h1>legali rappresentati</h1><pre>";
        print_r($imprese);
        print "</pre>";
        */
        foreach ($imprese AS $row) {
            $arrayImprese = array();
            $arrayImprese['c-fiscale'] = $cfImpresa = $row['c_fiscale'];
            $arrayImprese['n-rea'] = $row['n_rea'];
            $arrayImprese['cciaa'] = $row['cciaa'];
            $arrayImprese['denominazione'] = $row['denominazione'];
            $arrayImprese['stato_registrazione'] = $row['stato_registrazione'];
            $sql = "SELECT StatoRegistrazione
                            FROM  EXPO_T_Imprese where CodiceFiscale='" . $cfImpresa . "'";

            if ($this->db->NumRows($sql) == 0) {
                $arrayImprese['stato_registrazione'] = 0;
            } else {
                $arrayImprese['stato_registrazione'] = $this->db->GetRow($sql, 'StatoRegistrazione');
            }
            //if ($arrayImprese['stato_registrazione'] < 4){
            	$arrayListaImprese[] = $arrayImprese;
           //}
            	
        }
        
        /*
        print "<h1>LISTA IMPRESE $db</h1><pre>";
        print_r ($arrayListaImprese);
        print "</pre>";
        */
        
        return $arrayListaImprese;
    }
    
    
    /**
     * Funzione che restituisce la lista delle imprese di un legale rappresentate dai server Parix_MI e Parix_MB.
     * 
     * @param string $cf 
     * @return array
     */
    public function GetAziendeFromCfRappresentante($cf,$log = true) {
    	$aziendeParixMI = $this->GetAziendeCfRappresentanteFromParix($cf, Config::DB_PARIX_MI_NAME);
    	
    	$aziendeParixMB = $this->GetAziendeCfRappresentanteFromParix($cf, Config::DB_PARIX_MB_NAME);
    	
    	$arrayListaImprese = array_merge((array)$aziendeParixMI, (array)$aziendeParixMB);

    	/*
    	print "<h1>LISTA IMPRESE</h1><pre>";
    	print_r ($arrayListaImprese);
    	print "</pre>";
		*/
    	if ($log){
    		$this->logLegaleRappresentante($cf, $arrayListaImprese);
    	}
    	
    	return $arrayListaImprese;
    }
    
    
    
	/**
	 * Funzione che estrae i dati di una azienda da parix in base alla $cciaa e al NREA. 
	 * @param unknown $CFS
	 * @param string $ccia
	 * @return multitype:multitype: unknown number Ambigous <> multitype:multitype:string Ambigous <>
	 */
    public function GetInfoAziendaFromCf($NRea,$cciaa, $log = true) {
        /*
    	$CFSExplode = explode("-", $CFS);
    	$cf = $CFSExplode[0];
    	$NRea = $CFSExplode[1];
    	$cciaa = $CFSExplode[2];
    	 
    	 */
    	/*
    	print "<h1>VODICE FICSCALE: $CFS</h1>";
    	
    	
    	//$cf ="ciao";
    	print "<pre>";
    	print_r($CFSExplode);
    	print "</pre>";
    	*/
    	
    	$arrayInfoImpresa = array();
    	
    	if (strcmp($cciaa,"MI") == 0){
    		$dbName = Config::DB_PARIX_MI_NAME;
    	} else if (strcmp($cciaa,"MB") == 0){
    		$dbName = Config::DB_PARIX_MB_NAME;
    	} else{
    		return false;
    	}
    	
    	//print "<h1>$dbName</h1>";
    
    	
		
        $query="SELECT DENOMINAZIONE AS denominazione, PARTITA_IVA AS PIva, N_REA AS NRea,CAPT_SOCL_DLBT_EURO AS C_sociale,
			CONCAT(C_VIA,' ',VIA) AS ViaSede, N_ADDETTI_FAMILIARI AS ndipendenti1, N_ADDETTI_SUBORDINATI AS ndipendenti2,
			N_Civico as NCivicoSede,COMUNE AS DescComSede,SGL_PRV AS SglPrvSede, CAP AS CapSede,
			INDIRIZZO_PEC AS IndirizzoPostaCertificata, desatt AS attivita_esercitata, C_NATURA_GIURIDICA AS forma_giuridica, C_FISCALE_IMPRESA as codiceFiscale
			FROM  imprese_tot
			WHERE N_REA = '$NRea' AND T_LOCALIZZAZIONE = 'SE'";
        	

        if ($row = $this->dbParix->GetRow($query,null,$dbName,"Estrazione Impresa da ".$dbName)) {

        	$ndipendenti = 0 + ($row['ndipendenti1']) + ($row['ndipendenti2']);
			$sqlFormaGiuridica = "SELECT Descrizione FROM EXPO_TLK_Forme_Giuridiche WHERE Codice ='".$row['forma_giuridica']."'";
			
			$formaGiuridica = $this->db->GetRow($sqlFormaGiuridica,"Descrizione",null,"estrazione forma giuridica");
			
            $arrayInfoImpresa['denominazione'] = $row['denominazione'];
            $arrayInfoImpresa['CodiceFiscale'] = $row['codiceFiscale'];
            $arrayInfoImpresa['PIva'] = $row['PIva'];
            $arrayInfoImpresa['NRea'] = $row['NRea'];
            $arrayInfoImpresa['C_sociale'] = $row['C_sociale'];
            $arrayInfoImpresa['ViaSede'] = $row['ViaSede'];
            $arrayInfoImpresa['NCivicoSede'] = $row['NCivicoSede'];
            $arrayInfoImpresa['CapSede'] = $row['CapSede'];
            $arrayInfoImpresa['DescComSede'] = $row['DescComSede'];
            $arrayInfoImpresa['SglPrvSede'] = $row['SglPrvSede'];
            $arrayInfoImpresa['IndirizzoPostaCertificata'] = $row['IndirizzoPostaCertificata'];
            $arrayInfoImpresa['attivita-esercitata'] = $row['attivita_esercitata'];
            $arrayInfoImpresa['ndipendenti'] = $ndipendenti;
            $arrayInfoImpresa['forma-giuridica'] = $formaGiuridica;
			
			/*
            print "<pre>";
            print_r($arrayInfoImpresa);
            print "</pre>";
			*/

            $arrayListaCategorie = array();
            $sqlAteco = "SELECT DISTINCT(catp) AS c_attivita FROM codici_ateco_ul WHERE n_rea = '".$arrayInfoImpresa['NRea'] ."'";
            
            
            foreach ($this->dbParix->GetRows($sqlAteco,$dbName,"Estrazione ateco Impresa ". $arrayInfoImpresa['NRea']." from $dbName") AS $rowCategorie) {
                $arrayCategorie = array();
                $arrayCategorie['c-attivita'] = $rowCategorie['c_attivita'];
                $arrayCategorie['attivita'] = ''; //$rowCategorie['attivita'];
                $arrayListaCategorie[] = $arrayCategorie;
            }
            $arrayInfoImpresa['Categorie'] = $arrayListaCategorie;

            $arrayListaCertificazioni = array();
           /* $query = "SELECT * FROM  EXPO_T_Imprese where CodiceFiscale='$cf'";
            foreach ($this->dbParix->GetRows($query,$db) AS $rowCertificazioni) {
                $arrayCertificazioni = array();
                $arrayCertificazioni['descrizione'] = $rowCertificazioni['descrizione'];
                $arrayCertificazioni['n_certificato'] = $rowCertificazioni['n_certificato'];
                $arrayCertificazioni['c-schema-accreditamento'] = $rowCertificazioni['c-schema-accreditamento'];
                $arrayCertificazioni['data_emissione'] = $rowCertificazioni['data_emissione'];
                $arrayCertificazioni['data_scadenza'] = $rowCertificazioni['data_scadenza'];
                $arrayListaCertificazioni[] = $arrayCertificazioni;
                
            }
            */
    
            $arrayInfoImpresa['Certificazioni'] = $arrayListaCertificazioni;
            
        }
        
        if ($log){
        	$this->logAzienda($arrayInfoImpresa);
        }
        
               
        return $arrayInfoImpresa;
    }

    public function RecuperaAziendeCfRappresentanteFromLogParix($cf) {

    	$chiamataLog = "schedaPersona:codiceFiscale=$cf&user=parix";
    	$query = "SELECT * FROM interrogazioni_parix WHERE url_chiamata = '".$chiamataLog."' order by Id Desc limit 1";
    	$result = $this->db->GetRow($query, null, Config::DB_DATIRI_NAME);
    	
    	$strRisposta = $result['risposta'];
    	$dataChiamata = $result['data_chiamata'];
    	
    	$visura = XML2Array::createArray(trim($strRisposta));
    	/*
    	print "<pre>";
    	print_r($visura);
    	print "</pre>";
		*/    	 
    	return array("DataSurces" =>"Parix","DataChiamata" => $dataChiamata,"Visura" => $visura);
    }
    
    
    public function RecuperaInfoAziendaFromCfLogParix($NRea,$cciaa) {

		$chiamataLog = "visuraOrdinaria:cciaa=$cciaa&rea=$NRea&user=parix";
    	$query = "SELECT * FROM interrogazioni_parix WHERE url_chiamata = '".$chiamataLog."' order by Id Desc limit 1";
    	$result = $this->db->GetRow($query, null, Config::DB_DATIRI_NAME);
    	
    	$strRisposta = $result['risposta'];
    	$dataChiamata = $result['data_chiamata'];
    	
    	$visura = XML2Array::createArray(trim($strRisposta));
    	/*
    	print "<pre>";
    	print_r($visura);
    	print "</pre>";
    	 */
    	return array("DataSurces" =>"Parix","DataChiamata" => $dataChiamata,"Visura" => $visura);
    }
    
    	
    

}
