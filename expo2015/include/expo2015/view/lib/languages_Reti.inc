<script language="javascript">
    
    function selezionaLingua(lang,id){
    var par = 'lang='+lang+'&Id=' + id
    + "&NRete=" + $("#Ordine").val() 
;

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/visualizza_Preview_Rete.inc",
        data: par,
        success: function(response) {
            $("#box_popup").html(response);

        }});
        
    }
</script>
<ul>
<?php

$dbLang = new DataBase();
 
foreach ($dbLang->GetRows("SELECT Label,Lang FROM EXPO_T_Lingue_Sito ORDER BY Id") AS $rowLang) {
    echo "<li>";
    if (strcmp($Lang, $rowLang['Lang']) == 0)
        echo "<span>".$rowLang['Label']."</span>";
    else
        echo '<a class="' . strtolower($rowLang['Lang']) . '" onclick="selezionaLingua(\''.$rowLang['Lang'].'\',\''.$IdRete.'\')" >' . $rowLang['Label'] . '</a>';
    echo "</li>";
}
?>
</ul>