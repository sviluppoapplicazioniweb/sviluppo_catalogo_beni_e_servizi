<?php

/**
 * Description of Professionista
 *
 * @author Bertoletti Fabio
 */

require_once($PROGETTO . "/view/lib/db.class.php");

class Professionista{

	private $db;

	public function  __construct() {
		$this->db = new DataBase();

	}

	/**
	 * Estrazione delle categorie abilitate per $ordine
	 * @param int $ordine
	 * @return
	 */
	private function getCategorieOrdiniProfessionali($ordine){
		$sqlCategorie ="SELECT Id_Categoria
		FROM EXPO_TJ_Categorie_OrdiniProfessionali
		WHERE Id_OrdineProfessionale = $ordine
		ORDER BY Id_Categoria";
		$categorie = $this->db->GetRows($sqlCategorie,null,"Estrazione categorie Professionisti - Professionista.php Ln.26");
		 
		return $categorie;
	}

	public function gestisciCategorie($ordine,$idImpresa) {

		$categorie = $this->getCategorieOrdiniProfessionali($ordine);


		foreach ($categorie as $key => $categoria) {
			//print "<br>".$categoria["Id_Categoria"];
				
			$sqlImpresaCategorie="SELECT COUNT(*) AS NumeroRighe from EXPO_TJ_Imprese_Categorie where IdImpresa = '".$idImpresa."' AND IdCategoria = '".$categoria['Id_Categoria']."'";
			
			$rows = $this->db->GetRow($sqlImpresaCategorie);
			
			if ($rows['NumeroRighe'] == 0) {
					
				$queryInser="INSERT INTO EXPO_TJ_Imprese_Categorie (Id,IdImpresa,IdCategoria,IsIscritta)
						values(
						(select max(a.Id) from EXPO_TJ_Imprese_Categorie as a)+1,
						'".$idImpresa."',
								'".$categoria['Id_Categoria']."',
										'N'
										)";
					
				//print $queryInser;
				$this->db->Query($queryInser);

			}

		}
	
	}


}
?>
