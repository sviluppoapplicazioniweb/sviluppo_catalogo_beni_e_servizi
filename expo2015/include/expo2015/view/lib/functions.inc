<?php

/**
 * Scrive nella tabella expo_t_catalogo_logs i dati forniti in input
 * response: messaggio da inserire nella colonna response
 * action: azione svolta che ha scatenato il log
 * param: parametri usati nella request
 *
 **/

function scriviLog($response, $action, $param, $sendMail = false){
	$db = new DataBase();
	
	$queryst = "INSERT INTO EXPO_T_Catalogo_Logs (`data`,`azione`,`parametri`,`response`)
								VALUES (NOW(),'".str_replace("'", "\'", $action)."','".str_replace("'", "\'", $param)."','".str_replace("'", "\'", $response)."')";
	$db->Query($queryst);
	if ($sendMail == true){
		include_once("send_mail.inc");
		send_mail_html('alessandro.ferla@digicamere.it; fabio.bertoletti@digicamere.it', 'noreply.catalogue@expo2015.org', $param. " | " .$response);
	}
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function verificaSeTransAttiva($idImpresa){
	global $timeout;
	
	$db = new DataBase();
	
	$verifica = "SELECT * FROM EXPO_TJ_Imprese_Ordine WHERE IdImpresa = '" . $idImpresa . "' ";
	//echo "Query ".$verifica;
	if ($db->NumRows($verifica,null, null, "functions.inc Ln.13") > 0) {
		
		$DataRichiesta = $db->GetRow($verifica,'DataRichiesta',null,"functions.inc Ln.16");
		
		//echo "Verifica";
		$adesso = time();
		
		//echo "Data: ".$DataRichiesta;
		$time = strtotime($DataRichiesta);
		
		$tempotimeout = $timeout * 60;
		//echo "Timeout".$timeout;
		$restante = $time + $tempotimeout;
		//echo "Rest:".$restante." Adesso:".$adesso;
		
		if ($restante > $adesso){ 
			//echo "Evviva";
			return true;
		}

	}
	return false;
}

//Chiama Siexpo e verifica se l'impresa � iscritta
function getIsSiexpo($piva){
	global $keySito;
	
	$result = false;
	$username=config::SIEXPO_USR;
		
	$fields = array(
			'cyph' => urlencode(md5($username . date("ymd") . $keySito)),
			'user' => urlencode($username),
			'piva' => urlencode($piva)
	);
	
	//url-ify the data for the POST
	//foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	//rtrim($fields_string, '&');
	
	//$URL = "http://siexpo2015sv.mi.camcom.it/index.phtml?Id_VMenu=319";
	$URL = config::SIEXPO_URL;
	//echo "URL: $URL";
	//exit;
	//setting the curl parameters.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL,$URL);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		
	if (curl_errno($ch))
	{
		// moving to display page to display curl errors
		echo curl_errno($ch) ;
		echo curl_error($ch);
	}
	else
	{
		//echo "entrato";
		//exit;
		//getting response from server
		$response = curl_exec($ch);
		//echo "Response:".$response;
		//exit;
		if (strpos($response,"TRUE") !== FALSE){
			$result = true;
		}
		//exit;
		curl_close($ch);
		
	}
	
	//echo "<h2>$result</h2>";
	//exit;
	return $result;
}

//SETTA LE VARIANBILI DEL FROM
function settaVar($type, $nome, $defoult) {

    if (isset($type[$nome])) {
        return $type[$nome];
    } else {
        return $defoult;
    }
}

function searchFile($folder, $srch, &$results) {
	$folder = rtrim($folder, "/") . '/';
	if ($hd = opendir($folder)) {
		while (false !== ($file = readdir($hd))) {
			if($file != '.' && $file != '..') {
				if(is_dir($folder . $file)) {
					searchFile($folder. $file, $srch, $results);
				} elseif(preg_match("#\.$srch$#", $file)) {
					$results[] = $folder . $file;
				}
			}
		}
		closedir($hd);
	}
}

function cercaFileFirmato($IdImpresa,$uid=null){
	$db = new DataBase();
	
	$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
	$row = $db->GetRow($query,null, null, "functions.inc Ln.148 utente: ".$uid);
	//$row = $db->GetRow($query,null, null, "functions.inc Ln.69");
	$RagioneSociale=$row['RagioneSociale'];
	$pathImpresa=selectRootDoc($RagioneSociale);
	$pathFolderFirma = Config::ROOTFILES . $pathImpresa . "/".$IdImpresa."/filesFirmati/";
	//echo $pathFolderFirma;
	//Cerca nella cartella dei files Firmati se c'� il file firmato
	//delete($pathFolderFirma.'RegExpo_83.pdf.p7m');
	//exit;
	
	if (file_exists($pathFolderFirma)){
		$nomeFileFirmato='';
		$r=array();
		searchFile($pathFolderFirma, 'p7m', $r);
		foreach($r as $f) {
			$nomeFileFirmato=$f;
				//echo "N:".$nomeFileFirmato, '<br />';
		}
	
		if ($nomeFileFirmato==''){
			searchFile($pathFolderFirma, 'pdf', $r);
			foreach($r as $f) {
				$nomeFileFirmato=$f;
				//echo "F:".$nomeFileFirmato, '<br />';
			}
		}
	} else {
		//echo "Errore: \"" . $pathFolderFirma . "\" non esiste!";
		return false;
	}
	return ($nomeFileFirmato !='');

}

function redirect($link) {
    echo "   <script type=\"text/javascript\">

   function doRedirect() {
   location.href = \"$link\";
   }
   window.setTimeout(\"doRedirect()\", 0);
    </script >
    ";
}

//GESTIONE CHECKED
function getCheck($value) {
    if (strcmp($value, "Y") == 0) {
        return ' checked="checked" ';
    } 
    return '';
}

//METTE LE IMMAGINI SUL I CHECK SELEZINATI O NO
function getCheckImg($value) {
    $img = '<img src="images/expo2015/';
    if (strcmp($value, "Y") == 0) {
        $img .= 'ico_check.png"';
    } else {
        $img .= 'ico_delete.png"';
    }
    $img .= ' width="16px" />';

    return $img;
}

function getLinkWeb($value) {
	$value = urldecode($value);
    if (strpos($value, "http://") === false) {
        $value = "http://" . $value;
    }
    return $value;
}

//VISUALIZZA LE IMMAGINI E LINK A FILE E VIDEO
function getAllegati($tipo, $value) {

    $tag = "";
    if (strcmp($tipo, 'file') == 0) {
        //return 'files/' . $value . '.pdf';
        if (is_file(Config::ROOTFILES . $value . '.pdf')) {
            $tag = '<strong>' . _ALLEGA_DOC_ANTEPRIMA . ':</strong> <a style="text-decoration:underline" href="' . Config::ROOTFILESHTML . $value . '.pdf" target="_blank" >' . substr($value, strrpos($value, '/') + 1) . '.pdf</a>';
        }
    } elseif (strcmp($tipo, 'link') == 0 && strcmp($value, '') != 0) {
		$value = urldecode($value);
        if (strpos($value, "http://") === false) {
            $value = "http://" . $value;
        }
        $link=explode("http://", $value);
        if($link[1]!=''){
         $tag = '<strong>' . _ALLEGA_LINK_ANTEPRIMA_ . ':</strong> <a style="text-decoration:underline" href="' . $value . '" target="_blank" >' . $value . '</a>';
        }
        
    }

    return $tag;
}

function getAllegatiFiles($tipo, $value, $text) {
    $tag = "";
    if (strcmp($tipo, 'file') == 0) {
        //return 'files/' . $value . '.pdf';
        if (is_file(Config::ROOTFILES . $value . '.pdf')) {
            $tag = '<strong>' . _ALLEGA_DOC_ANTEPRIMA . ':</strong> <a style="text-decoration:underline" href="' . Config::ROOTFILESHTML . $value . '.pdf" target="_blank" >' . $text . '.pdf</a><br>';
        }
    } elseif (strcmp($tipo, 'link') == 0 && strcmp($value, '') != 0) {
        if (strpos($value, "http://") === false) {
            $value = "http://" . $value;
        }
        $tag = '<strong>' . _ALLEGA_LINK_ANTEPRIMA_ . ':</strong> <a style="text-decoration:underline" href="' . $value . '" target="_blank" >' . $value . '</a><br>';
    }
    return $tag;
}

//RITORNA SE SELECT
function getSelected($value, $r) {
    if (strcmp($value, $r) == 0) {
        return ' selected ';
    }
    return '';
}

//BLOCCO DEI CAMPI TELEMACO
function getCheckHiddenTelemaco($value) {
    if (strcmp($value, "Y") == 0) {
        return ' disabled="disabled" ';
    }
    return '';
}

//BLOCCO DEI CAMPI
function getCheckHidden($value) {
    if (strcmp($value, "N") == 0) {
        return ' disabled="disabled" ';
    }
    return '';
}

function getCheckCategorie($stato, $id) {

    if ((($id == -1))) {
        return '  disabled="disabled" ';
    } else if (($stato != '' && ($stato == "N")) and ($id >= 0)) {
        return '  disabled="disabled" ';
    } else {
        return '  ';
    }
}

//COLORE PER LE RIGHE ALTERNATE IN TAB
function getColor($classe, $i) {

    if ($i % 2 == 1)
        return ' class="' . $classe . '" ';
    else
        return '';
}

//VISALIZZA IMG LOGO
function viewLogo($pathAvatar,$website) {

    $img = "";
 
//    if (strcmp($pathAvatar, "") == 0 || !file_exists(Config::ROOTFILESIMAGE . $pathAvatar)) {
//        $img = '<img id="logoImgSrc" draggable="false" src="images/expo2015/NoLogo.jpg"  width="100px"/>';
//    } else {
        $img = '<img id="logoImgSrc" draggable="false" src="getImageLogo.php" />';
//    }
        if (strcasecmp($website, "") != 0){
      		$img = '<a href="'.getLinkWeb($website).'" target="_blank">'.$img.'</a>';
        }
    return $img;
}

//SETTA LA QUERY DI RICERCA
function setSQLW($tab, $campo, $value) {
    if (strcmp($value, 'Y') == 0) {
        return "AND $tab.$campo = '$value' ";
    }
    return '';
}

//SETTA LA QUERY DI RICERCA PER CAMPI TESTO
function setTextSQLW($tab, $campo, $value) {
    if (strcmp($value, '') != 0) {
        return "AND $tab.$campo = '$value' ";
    }
    return '';
}

//SETTA LA QUERY DI RICERCA PER CAMPI NUMERO
function setNumSQLW($tab, $campo, $value) {
    if (strcmp($value, '') != 0) {
        return "AND $tab.$campo >= $value ";
    }
    return '';
}

//VISUALIZZAZIONE TAB PRODOTTI
function viewTabProd($testo, $allegati) {


    if (strcmp($testo, "") != 0 || strcmp($allegati[0], "") != 0 || strcmp($allegati[1], "") != 0)
        return true;

    return false;
}

//CREATE TAB
function ceateTab($arryCol, $numCol) {
    $widthTd = floor(100 / $numCol);
    $html = '<table width="100%">';
    $iCol = 0;
    foreach ($arryCol AS $value) {
        if ($iCol == 0)
            $html .= "<tr>";
        elseif ($iCol == 3) {
            $html .= "</tr>";
            $iCol = 0;
        }

        $html .= '<td width="' . $widthTd . '">' . $value . '</td>';
        $iCol++;
    }
    if ($iCol < $numCol) {
        for ($i = $iCol; $i < $numCol; $i++)
            $html .= "<td></td>";
        $html .= "</tr>";
    }


    $html.="</table>";
    return $html;
}

//DIVIDE IN PAGE IL RITORNO DEL QUERY
function multiPage($rTitolo, $arrayRighe, $numElementPag, $evento) {
    $html = "";
    $numElemeti = count($arrayRighe);
    $multiPage = false;
    if (strcmp($numElementPag, _ALL_) != 0) {
        $numPage = ceil($numElemeti / $numElementPag);
        if ($numPage > 1) {
            $multiPage = true;
            $html .= '<script type="text/javascript" src="/jquery/js/virtualpaginate.js"></script>';
            $html .= '<div id="contenetPages" style="width: 100%;">';
        }
    }
    $i = 0;
    for ($p = 1; $p <= $numPage; $p++) {
        if ($multiPage) {
            if ($p > 1) {
                $html .= '<div id="pag' . $p . '" style="display:none">';
            } else {
                $html .= '<div id="pag' . $p . '" >';
            }
        }
        $html .= '<table id="tabella_elenchi" >';
        $html .= $rTitolo;
        for ($r = 0; $r < $numElementPag; $r++) {
            if ($i < $numElemeti) {
                $html .= $arrayRighe[$i];
                $i++;
            } else {
                $r = $numElementPag;
            }
        }
        $html .= "</table>";

        if ($multiPage) {
            $html .= "</div>";
        }
    }


    if ($multiPage) {
        $numElemet = array(25, 50, 75, 100, 150, 200, _ALL_);
        $html .= '</div>
            <div class="navPage">
            
        <div>';
        $html .= '<a href="javascript:virtualpaginateP(\'' . $numPage . '\')" ><img src="images/expo2015/freccia_sinistra_bianco.png"></a>&nbsp;';
        for ($p = 1; $p <= $numPage; $p++) {
            $html .= '<a href="javascript:virtualpaginate(\'' . $p . '\',\'' . $numPage . '\')">' . $p . '</a> | ';
        }
        $html .= '&nbsp;<a href="javascript:virtualpaginateN(\'' . $numPage . '\')" ><img src="images/expo2015/freccia_destra_bianco.png"></a>';
        $html .= '&nbsp;&nbsp;N. <select id="elementiN"  name="elementiN" onchange="' . $evento . '">';
        foreach ($numElemet AS $value) {
            $select = "";
            if ($numElementPag == $value)
                $select = "selected";
            $html .= '<option value="' . $value . '" ' . $select . '>' . $value . '</option>';
        }

        $html .= '</select>
            </div>
            </div>';
    }

    return $html;
}

//VISUALIZZA E GESTISCE FILE
function getFileProductInput($nome,$nomeFile,$rootfile,$idimpresa){
    $html='';

    $rootFilesFolder = urlencode(Config::ROOTFILES);

    if (is_file(Config::ROOTFILES . $rootfile.'/'.$nomeFile . '.pdf')) {
        $html = '<div id="' . $nome . 'Delete" class="viewfile">';
        $html.= $_SESSION['errore']['file'][$nome];
        $_SESSION['errore']['file'][$nome]='';
        $html .=  '<div style="float:left;width:90%"><a   href="' . Config::ROOTFILESHTML . $rootfile.'/'.$nomeFile . '.pdf" target="_blank"  draggable="false" ><span class="linkColor">' . $nomeFile . '.pdf</span></a></div>
           <div class="icondelete"><a href="javascript:deleteFile(\'' . $nomeFile . '.pdf\',\'' . $nome . '\',\''.$rootfile. '\',\''.$idimpresa.'\')" name="link_form"   draggable="false" ><img src="/images/deletefile.png"></a></div> ';
        $html .= '<br></div >';
        //' . $rootFilesFolder .$rootfile. '/
        $html.='<div><div class="upload-file-container_edit" id="' . $nome . 'Upload" style = "display:none">';
        
        $html.='<input type="file" class="inputfileupload" name="'.$nome.'" id="'.$nome.'">
            </div><div id="testofilename"></div><div>';
    }else{
        $html= $_SESSION['errore']['file'][$nome];
        $_SESSION['errore']['file'][$nome]='';
        $html.='<div style="float:left;margin-left:10px;width:260px;"><div class="upload-file-container_edit">';
        
        $html.='<input type="file" class="inputfileupload"  name="'.$nome.'" id="'.$nome.'">
            </div><div  id="testofilename'.$nome.'" >Nessun file selezionato</div></div>';
        $html.= $_SESSION['errore']['file'][$nome];
        $_SESSION['errore']['file'][$nome]='';
    }

    return $html;
}
function getFileInput($nome,$nomeFile,$rootfile,$idimpresa){
    $html='';
	
    $rootFilesFolder = urlencode(Config::ROOTFILES);
	//include_once("send_mail.inc");
	//send_mail_html('alessandro.ferla@digicamere.it', 'noreply.catalogue@expo2015.org', "Svil. Alert: " .$query."\n");
    //$query= "Root Files ".Config::ROOTFILES . $rootfile.'/'.$nomeFile . '.pdf';
    //echo $query;
    
    if (is_file(Config::ROOTFILES . $rootfile.'/'.$nomeFile . '.pdf')) {
        $html = '<div id="' . $nome . 'Delete" class="viewfile">';
        $html .='<div style="float:left;width:90%">
                  <a href="' . Config::ROOTFILESHTML . $rootfile.'/'.$nomeFile . '.pdf" target="_blank"  draggable="false" ><span class="linkColor">' . $nomeFile . '.pdf</span></a>
                </div>
                <div class="icondelete">
                    <a href="javascript:deleteFile(\'' . $nomeFile . '.pdf\',\'' . $nome . '\',\''.$rootfile.'\',\''.$idimpresa.'\')" name="link_form"   draggable="false" ><img src="/images/deletefile.png"></a>
                        </div> ';
        //' . $rootFilesFolder .$rootfile. '/\
        $html .= '</div >';
        $html.='<div><div class="upload-file-container_edit" id="' . $nome . 'Upload" style = "display:none">';
        $html.= $_SESSION['errore']['file'][$nome];
        $_SESSION['errore']['file'][$nome]='';
        $html.= '<input type="file" class="inputfileupload"  name="'.$nome.'" id="'.$nome.'">
            </div><div id="testofilename'.$nome.'" style="float:left;"></div></div>';
    }else{
        $html= $_SESSION['errore']['file'][$nome];
        $_SESSION['errore']['file'][$nome]='';
        $html.='<div style="float:left;margin-left:10px;width:300px;"><div  class="upload-file-container_edit" style="float:left">';
        
        $html.='<input type="file"  class="inputfileupload"   name="'.$nome.'" id="'.$nome.'">
            </div><div id="testofilename'.$nome.'" >Nessun file selezionato</div></div>';
    }
   
    return $html;
}

function getFileInDir($nomeFile, $nomeElemento) {
    $rootFilesFolder = urlencode(Config::ROOTFILES);

    $tag = "";

    if (is_file(Config::ROOTFILES . $nomeFile . '.pdf')) {
        $tag .= '<div id="' . $nomeElemento . 'Delete">';
        $tag .= _FILE_PRESENTE_ . '<a href="' . Config::ROOTFILESHTML . $nomeFile . '.pdf" target="_blank" class="buttonRed" draggable="false" >' . _VISUALIZZA_ . '</a> 
            <a href="javascript:deleteFile(\'' . $nomeFile . '.pdf\',\'' . $nomeElemento . 'Delete\',\'\')" name="link_form" class="buttonRed" draggable="false" >' . _ELIMINA_ . '</a>';
        $tag .= '<br></div >';
        //' . $rootFilesFolder . '
    }

    return $tag;
}

//VISUALIZZA DESCRIZIONE DI DEFOULT
function descrizioneDefoult($descrizione, $file, $link) {
    if (strcmp($descrizione, "") != 0) {
        return $descrizione;
    } else {
        $testo = "";

        if (strcmp($file, "") != 0 && strcmp($link, "") != 0) {
            $testo.= _FILELINK_V_;
        } else {
            if (strcmp($file, "") != 0)
                $testo .= _FILE_V_;
            if (strcmp($link, "") != 0)
                $testo .= _LINK_V_;
        }

        return html_entity_decode($testo);
    }
}

//RITORNA ROOTDOC
function selectRootDoc($nomeImpresa) {
    global $arrayRootDoc;
    global $elseRootDoc;
        
    $inizialeImpresa = strtoupper(substr($nomeImpresa, 0, 1));
    
    foreach ($arrayRootDoc as $key => $value) {   	
        if (in_array($inizialeImpresa, $value)) {
            return $key;
        } 
                
    }
    return $elseRootDoc;
}

function selectRootDocReti($nomeRete) {
	global $arrayRootDocReti;
	global $elseRootDocReti;

	$inizialeRete = strtoupper(substr($nomeRete, 0, 1));

	foreach ($arrayRootDocReti as $key => $value) {
		if (in_array($inizialeRete, $value)) {
			return $key;
		}

	}
	return $elseRootDocReti;
}

function selectRootDocPersona($persona) {
	global $arrayRootDocPersone;
	global $elseRootDocPersone;

	$inizialePersona = strtoupper(substr($persona, 0, 1));

	foreach ($arrayRootDocPersone as $key => $value) {
		if (in_array($inizialePersona, $value)) {
			return $key;
		}

	}
	return $elseRootDocReti;
}

function checkLink($link) {
    if (strlen($link) < 8)
        $link = "";
    return $link;
}

function checkLinkVuoto($link) {
    if (strcmp($link, "") == 0)
        $link = "http://";
    return $link;
}

function getColorNameCategorie($stato, $id) {
    $stato = strtoupper($stato);
    if ((($id == -1))) {
        return '   style="color:#999999; font-style: italic" ';
    } else if (($stato != '' && ($stato == "N")) and ($id >= 0)) {
        return $id . '   style="color:#888787; font-weight:bold" ';
    } else {
        return ' style="color:#000; font-weight:bold" ';
    }
}

function passwordCasuale($lunghezza = 6) {
	$caratteri_disponibili = "abcdefghijklmnopqrstuvwxyz";
    $password = "";
    for ($i = 0; $i < $lunghezza; $i++) {
        $password = $password . substr($caratteri_disponibili, rand(0, strlen($caratteri_disponibili) - 1), 1);
    }
    return $password;
}

function removeDirectory($dir)
{
	$iterator = new RecursiveDirectoryIterator($dir);
	$iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST);

	foreach ($iterator as $path) {
		if ($path->isDir()) {
			rmdir((string)$path);
		} else {
			unlink((string)$path);
		}
	}
	
	rmdir((string)$dir);
}

function createPdf($arrayXmlDati, $rootFile) {
    global $URLPDF;

	//Corregge simbolo & sostituendolo con &amp;
	//$arrayXmlDati['denominazione'] = htmlentities($arrayXmlDati['denominazione'], ENT_QUOTES);
    $arrayXmlDati['denominazione'] = "<![CDATA[".$arrayXmlDati['denominazione']."]]>";
    
    $xmlDati = "<pagine>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
    <pagina>
        <p10>
            <denominazione>" . $arrayXmlDati['denominazione'] . "</denominazione>
            <codfisc>" . $arrayXmlDati['codfisc'] . "</codfisc>
            <fasciafatt>" . $arrayXmlDati['fasciafatt'] . "</fasciafatt>
            <fee>" . $arrayXmlDati['fee'] . "</fee>
        </p10>
    </pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
<pagina><p10><denominazione></denominazione><codfisc></codfisc><fasciafatt></fasciafatt><fee></fee></p10></pagina>
</pagine>";


    $xmlRequest = '<exec>
<passepartout>' . base64_encode(base64_encode(base64_encode(date('z') + 1))) . '</passepartout>
<TemplateName>RegExpo.pdf</TemplateName>
<XmlData>' . base64_encode($xmlDati) . '</XmlData>
</exec>';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $URLPDF);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);

    if (curl_errno($ch)) {
                // echo curl_errno($ch);
          //echo curl_error($ch); 
        //exit;
          return false;
    } else {
        $fileName = $rootFile . 'RegExpo.pdf';
        $response = curl_exec($ch);
        touch($fileName);
        chmod($fileName, 0775);
        $f = fopen($fileName, "w");
        fwrite($f, base64_decode($response));
        fclose($f);
        curl_close($ch);
        return true;
    }
}

function getStatoImpresa($value) {
    $img = '';
    if (strcmp($value, "4") == 0) {
        $img = '<img src="images/expo2015/icona_modifica.png" width="12px" style="float:left;vertical-align: middle;margin:5px;" />';
    }

    return $img;
}

function getStatoImpresaSelect($value) {
    $img = '';
    if (strcmp($value, "4") == 0) {
        $img = 'style="background: red;"';
    } elseif (strcmp($value, "3") == 0){
    	$img = 'style="background: orange;"';
    }

    return $img;
}

function getStatoProdotto($value) {
    $img = '';
    if (strcmp($value, "D") == 0) {
        $img = '<img src="images/expo2015/icona_modifica.png" width="12px" style="vertical-align: middle;margin:5px;"/>';
    }

    return $img;
}

$statoTrattative = array("A" => _ATTESA_, "C" => _CONFERMATO_, "D" => _DINIEGA_, "S" => _SOSPESO_);

$widthImgLogo = 120;
$heightImgLogo = 120;

function checkIfDisplayableImpresa($idImpresa,$tab,$row){
    $db = new DataBase();
    $rootDoc = selectRootDoc($db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", "RagioneSociale", null, "functions.inc ln.576")) . "/";
         $query = "SELECT Lingua,EXPO_TJ_Imprese_Lingue.Id AS IdL FROM EXPO_Tlk_Lingue,EXPO_TJ_Imprese_Lingue
          WHERE EXPO_TJ_Imprese_Lingue.IdImpresa = '$idImpresa' AND
          EXPO_TJ_Imprese_Lingue.IdLingua = EXPO_Tlk_Lingue.Id ORDER BY Lingua";
           $querylingua = "SELECT Lingua, TJ.Id AS IdL 
                FROM EXPO_Tlk_Lingue AS T
                INNER JOIN EXPO_TJ_Imprese_Lingue AS TJ ON T.Id = TJ.IdLingua 
                WHERE TJ.IdImpresa = '$idImpresa' 
                ORDER BY Lingua";
    if($tab=='dettagli'){
//        $query = "SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImpresa'";
//        $row = $db->GetRow($query);
        
    $row['internazionalizzazioneL']=str_replace("http://", "", $row['internazionalizzazioneL']);
    $row['referenze']=str_replace("http://", "", $row['referenze']);
    
        if(
                $row['IsGeneralContractor']!='Y'
                and $row['IsProduttore']!='Y'
                and $row['IsDistributore']!='Y'
                and $row['IsPrestatoreDiServizi']!='Y'
                and $row['IsSediEstere']!='Y'
                and $row['IsNetwork']!='Y'
                and $row['internazionalizzazioneL']==''
                and $row['referenze']==''
                and $row['partecipazioneExpo']!='Y'
                and $row['partecipazioniInternazionali']!='Y'
                and $row['IsPartecipazioneExpo']!='Y'
                and $row['IsPartecipazioniInternazionali']!='Y' 
                 
                and !file_exists(Config::ROOTFILES . $rootDoc . $row['Id'] . "/internazionalizzazione.pdf")
                and !file_exists(Config::ROOTFILES . $rootDoc . $row['Id'] . "/referenze.pdf")
                and $db->NumRows($query) == 0
                and $db->NumRows($querylingua) == 0
                
                ){
            return false;
        }else{
            return true;
        }
    }
    if($tab=='qualita'){
          $query = "SELECT IDCert,T.Descrizione AS Descrizione,Descrizione_FR,Descrizione_EN
                                    FROM EXPO_Tlk_Certificazioni AS T
                                    JOIN EXPO_TJ_Imprese_Certificazione TJ ON T.Id = TJ.IdCertificazione
                                    WHERE IdImpresa =  '$idImpresa'
                                    ORDER BY IDCert";
        
$query1 = "SELECT A.Descrizione,A.Sigla
                                          FROM EXPO_TJ_Imprese_Accreditamenti as IA
                                          join  EXPO_Tlk_Accreditamenti as A on IA.IdAccreditamento=A.Id
                                          where IA.IdImpresa='" . $idImpresa . "'
                                          ORDER BY A.Descrizione";

            $checked = false;
            if ($db->NumRows($query) > 0 or $db->NumRows($query1)>0) {
                $checked = true;
            }
 
          $row['qualitaL']=str_replace("http://", "", $row['qualitaL']);
        if(
                $row['aModello']!='Y'
               and $row['qualita']==''
               and  $row['qualitaEN']==''
               and  $row['qualitaFR']==''
               and  $row['qualitaL']==''
                
                and $row['IsMasterSpecialistico']!='Y'
                and $row['pMasetr']!='Y'
                and $row['pCertificazione']!='Y'
                and $checked!=true
                
//                and !file_exists(Config::ROOTFILES . $rootDoc . $row['Id'] . "/master.pdf")
//                and !file_exists(Config::ROOTFILES . $rootDoc . $row['Id'] . "/pCertificazione.pdf")
               
                ){
            return false;
        }else{
            return true;
        }
    }
    if($tab=='associazioni'){
          
             $query1 = "SELECT EXPO_TJ_Imprese_Associazioni.Id AS Id,EXPO_Tlk_Associazioni.Denominazione AS Denominazione FROM EXPO_Tlk_Associazioni,EXPO_TJ_Imprese_Associazioni
                        WHERE EXPO_TJ_Imprese_Associazioni.IdImpresa = '$idImpresa' AND
                      EXPO_TJ_Imprese_Associazioni.IdAssociazione = EXPO_Tlk_Associazioni.id ORDER BY EXPO_TJ_Imprese_Associazioni.Id";
                      
                      $row['retiImpresaL']=str_replace("http://", "", $row['retiImpresaL']);
                       
            if(
                $row['retiImpresa']!='Y'
                and  $row['retiImpresaL']==''
                and $db->NumRows($query1)==0
                ){
            return false;
        }else{
            return true;
        }
    }
}


function Delete($path)
{
    if (is_dir($path) === true)
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
        {
            Delete(realpath($path) . '/' . $file);
        }

        return rmdir($path);
    }

    else if (is_file($path) === true)
    {
        return unlink($path);
    }

    return false;
}

function getCheckNo($value) {
    if (strcmp($value, "N") == 0) {
        return 'checked="checked" ';
    }
    return '';
}

function serverOS() {
    $sys = strtoupper(PHP_OS);
 
    if(substr($sys,0,3) == "WIN")
    {
        $os = 1;
    }
    elseif($sys == "LINUX")
    {
        $os = 2;
    }
    else
    {
        $os = 3;
    }
 
    return $os;
}

function addParametriAzienda($arrayValori, $contatore) {
    foreach ($arrayValori AS $v) {
        if (strcmp($v, "") != 0 && strcmp($v, "N") != 0)
            $contatore ++;
    }

    return $contatore;
}


?>
