<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Impresa
 *
 * @author XddG123
 */

require_once($PROGETTO . "/view/lib/db.class.php");
class Impresa {

    private $db;

    public function  __construct() {
        $this->db = new DataBase();
 
    }

    public function gestisciAteco($infoAziendaArray,$cfAzienda) {

        foreach ($infoAziendaArray as $value) {

            $cattivita=$value['c-attivita'];
            $attivita=$value['attivita'];
           // controllo se esiste già una catecoria con questa c-attivita
            $sql="SELECT * from EXPO_Tlk_Categorie where Descrizione = '".trim($cattivita)."' ";
           
            if($this->db->NumRows($sql)==0){
                //se non esiste imposto il valore a n
                $found='N';
            }
            else{
               
                $found='Y';
//                $sql="SELECT * from EXPO_Tlk_Categorie where Descrizione like '%".$cattivita."%' ";
                $sql="SELECT b . Id
                        FROM EXPO_Tlk_Categorie AS a
                        JOIN EXPO_Tlk_Categorie AS b ON SUBSTRING( a.Id_Categoria, 1, 8 ) = b.Id_Categoria
                        WHERE a.Descrizione = TRIM( '".$cattivita."' )
                         ";
                
                $list=$this->db->GetRows($sql);
                $idAzienda="SELECT Id from EXPO_T_Imprese where CodiceFiscale ='".$cfAzienda."'";
                 $idAzienda=$this->db->GetRow($idAzienda, "Id");
                 foreach ($list as $key => $value) { 
                     
                     $sqlImpresaCategorie="SELECT COUNT(*) AS NumeroRighe from EXPO_TJ_Imprese_Categorie where IdImpresa = '".$idAzienda."' AND IdCategoria = '".$value['Id']."'";
                     
                     $rows = $this->db->GetRow($sqlImpresaCategorie);
                     
                     if ($rows['NumeroRighe'] == 0) {
                         
                         $queryInser="INSERT INTO EXPO_TJ_Imprese_Categorie (Id,IdImpresa,IdCategoria,IsIscritta)
                           values(
                        (select max(a.Id) from EXPO_TJ_Imprese_Categorie as a)+1,
                        '".$idAzienda."',
                        '".$value['Id']."',
                         'N'
                        )";
                         
                         $this->db->Query($queryInser);
                         
                     }
                     
                    
                } 
 
            }
            //cerco se esiste gia il codice ateco
            $sqlAteco="SELECT * from EXPO_T_Ateco where CodiceAteco='".$cattivita."'";
            
              if($this->db->NumRows($sqlAteco)==0){
                  // se non esiste l'aggiungo
                /*$ultimo="SELECT MAX(Id) as massimo from EXPO_T_Ateco ";
                
                $prossimo=$this->db->GetRow($ultimo, "massimo");
                $prossimo++;
                $prossimoateco=$prossimo;*/
                $insert="INSERT INTO EXPO_T_Ateco (Id,CodiceAteco,Descrizione,IsPresente)
                    VALUES((SELECT @prossimo:= COALESCE(MAX(TA.Id),0)+1 as prossimo FROM EXPO_T_Ateco AS TA),'".$cattivita."','".mysql_escape_string($attivita)."','".$found."')";
                $this->db->Query($insert);
                $prossimoateco=$prossimo=$this->db->GetRow("SELECT @prossimo as prossimo","prossimo",null,"inserimento ateco impresa php ln.86");
              }else{
                 $prossimoateco=$prossimo=$this->db->GetRow($sqlAteco, 'Id');
              }
              // prendo i dati dell'azienda
              $idAzienda="SELECT Id from EXPO_T_Imprese where CodiceFiscale ='".$cfAzienda."'";
              $idAzienda=$this->db->GetRow($idAzienda, "Id");

              //verifico se esiste gia la relazione tra ateco e impresa
              $qry="SELECT * from EXPO_TJ_Imprese_Ateco where IdImpresa='".$idAzienda."' and IdAteco='".$prossimo."'";
              if($this->db->NumRows($qry)==0){
                 // inserisco relazione
                /*$ultimo="SELECT MAX(Id) as massimo from EXPO_TJ_Imprese_Ateco ";
                $prossimo=$this->db->GetRow($ultimo, "massimo");
                $prossimo++;*/
                $insert="INSERT INTO EXPO_TJ_Imprese_Ateco (Id,IdImpresa,IdAteco)
                    VALUES((SELECT @prossimo:= COALESCE(MAX(TJA.Id),0)+1 as prossimo FROM EXPO_TJ_Imprese_Ateco AS TJA),'".$idAzienda."','".$prossimoateco."')";
                $this->db->Query($insert);
                $prossimoateco=$prossimo=$this->db->GetRow("SELECT @prossimo as prossimo","prossimo",null,"inserimento ateco impresa php ln.86");
              }
            }        
    }

    public function gestisciCertificazioni($infoAziendaArray,$cfAzienda){

    }
}
?>
