<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestioneCategorie
 *
 * @author Francesco Spagnoli
 */
class GestioneCategorie {

    //put your code here
    public $db;

    public function __construct() {
        $this->db = new DataBase();
    }

    public function getDescEstesa($Id_Categoria, $azione) {
        if (strcmp($azione, "M") == 0) {
            $Id_Categoria = substr($Id_Categoria, 0, strlen($Id_Categoria) - 3);
        }
        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id_Categoria = '$Id_Categoria'";
        $row = $this->db->GetRow($query);
        $arrayRespose = array("DescEstesa" => $row['DescEstesa'], "DescEstesa_AL" => $row['DescEstesa_AL'], "DescEstesa_AL2" => $row['DescEstesa_AL2']);
        return $arrayRespose;
    }

    public function getIdCategoria($IdCategoria) {
        $lIdCategoria = strlen($IdCategoria);
        $subCategoria = "";
        if (strcmp($IdCategoria, "00") != 0) {
            $subCategoria = " AND SUBSTR(Id_Categoria,1,$lIdCategoria) = '$IdCategoria' ORDER BY Num Desc limit 0,1";
            $lIdCategoria = $lIdCategoria + 3;
        }
        $IdCat = $this->db->GetRow("SELECT Id_Categoria AS Num FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = '$lIdCategoria' $subCategoria", "Num");
        
        $arrayNum = explode(".", $IdCat);
        $Num = $arrayNum[count($arrayNum)-1]+1;
        
        if ($Num <= 9) {
            $Num = "0" . $Num;
        }
        return $Num;
    }

    public function modificaDescEstesa($Id_Categoria, $arrayDescrizioneAll) {
        $lIdCat = strlen($Id_Categoria);
        $index = count(explode(".", $Id_Categoria)) - 1;
        for ($i = 3; $i < 10; $i = $i + 3) {
            $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = '" . ($lIdCat + $i) . "' AND SUBSTR(Id_Categoria,1,$lIdCat) = '$Id_Categoria'";
            foreach ($this->db->GetRows($query) AS $rows) {
                $Id = $rows['Id'];
                $arrayDescEstesa = explode(" -> ", $rows['DescEstesa']);
                $arrayDescEstesa[$index] = $arrayDescrizioneAll[0];
                $descEstesa = implode("->", $arrayDescEstesa);

                $arrayDescEstesa = explode(" -> ", $rows['DescEstesa_AL']);
                $arrayDescEstesa[$index] = $arrayDescrizioneAll[1];
                $descEstesa_AL = implode(" -> ", $arrayDescEstesa);

                $arrayDescEstesa = explode(" -> ", $rows['DescEstesa_AL2']);
                $arrayDescEstesa[$index] = $arrayDescrizioneAll[2];
                $descEstesa_AL2 = implode("->", $arrayDescEstesa);

                $this->db->Query("UPDATE EXPO_Tlk_Categorie SET DescEstesa = '$descEstesa' , DescEstesa_AL= '$descEstesa_AL', DescEstesa_AL2 = '$descEstesa_AL2' WHERE Id = '$Id' ");
            }
        }
    }

    public function cancellaCategoria($Id, $Id_Categoria) {


        $querySQL = "SELECT Descrizione FROM EXPO_Tlk_Categorie AS A WHERE A.Id_Categoria = '" . $Id_Categoria . "'";

        $Descrizione = $this->db->GetRow($querySQL, 'Descrizione');

        $querySQL = "SELECT DISTINCT B.DescEstesa AS CategoriaDesc,C.Descrizione AS AtecoDesc,F.RagioneSociale,A.Id,A.IdImpresa
                                                    FROM EXPO_TJ_Imprese_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B
                                                                                                ON A.IdCategoria = B.Id
                                                                                        INNER JOIN EXPO_Tlk_Categorie AS C
                                                                                                ON B.Id_Categoria = SUBSTRING(C.Id_Categoria,1,8) AND LENGTH(C.Id_Categoria) = 11
                                                                                        INNER JOIN EXPO_T_Imprese AS F
                                                                                                ON A.IdImpresa = F.Id
                                                                                        INNER JOIN (SELECT DISTINCT D.CodiceAteco,
                                                                                                                    E.IdImpresa
                                                                                                               FROM EXPO_T_Ateco AS D INNER JOIN EXPO_TJ_Imprese_Ateco AS E
                                                                                                                                              ON E.IdAteco = D.Id) AS H
                                                                                                ON H.IdImpresa = A.IdImpresa
                                                                                        LEFT JOIN (SELECT EXPO_TJ_Imprese_Categorie.Id
                                                                                                     FROM EXPO_TJ_Imprese_Categorie INNER JOIN EXPO_Tlk_Categorie
                                                                                                                                            ON EXPO_TJ_Imprese_Categorie.IdCategoria = EXPO_Tlk_Categorie.Id
                                                                                                                                    INNER JOIN EXPO_Tlk_Categorie AS Cat2
                                                                                                                                            ON EXPO_Tlk_Categorie.Id_Categoria = SUBSTRING(Cat2.Id_Categoria,1,8)
                                                                                                                                           AND LENGTH(Cat2.Id_Categoria) = 11
                                                                                                                                    INNER JOIN EXPO_T_Imprese
                                                                                                                                            ON EXPO_TJ_Imprese_Categorie.IdImpresa = EXPO_T_Imprese.Id
                                                                                                                                    INNER JOIN (SELECT DISTINCT EXPO_T_Ateco.CodiceAteco,
                                                                                                                                                                EXPO_TJ_Imprese_Ateco.IdImpresa
                                                                                                                                                           FROM EXPO_T_Ateco INNER JOIN EXPO_TJ_Imprese_Ateco
                                                                                                                                                                                     ON EXPO_TJ_Imprese_Ateco.IdAteco = EXPO_T_Ateco.Id) AS Ateco
                                                                                                                                            ON Ateco.IdImpresa = EXPO_TJ_Imprese_Categorie.IdImpresa
                                                                                                                                           AND Ateco.CodiceAteco = Cat2.Descrizione
                                                                                                    WHERE Cat2.Id_Categoria <> '".$Id_Categoria."') AS X
                                                                                              ON A.Id = X.Id
                                                   WHERE C.Id_Categoria = '".$Id_Categoria."'
                                                     AND H.CodiceAteco NOT IN ('".$Descrizione. "')
                                                     AND A.IsIscritta = 'Y'
                                                     AND X.Id IS NULL";
        

        /*$querySQL = "SELECT DISTINCT B.DescEstesa AS CategoriaDesc,C.Descrizione AS AtecoDesc,F.RagioneSociale,A.Id
                                                    FROM EXPO_TJ_Imprese_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B 
                                                                                                ON A.IdCategoria = B.Id 
                                                                                        INNER JOIN EXPO_Tlk_Categorie AS C 
                                                                                                ON B.Id_Categoria = SUBSTRING(C.Id_Categoria,1,8) AND LENGTH(C.Id_Categoria) = 11 
                                                                                        INNER JOIN EXPO_T_Imprese AS F 
                                                                                                ON A.IdImpresa = F.Id
                                                                                        INNER JOIN (SELECT DISTINCT D.CodiceAteco,
                                                                                                                    E.IdImpresa 
                                                                                                               FROM EXPO_T_Ateco AS D INNER JOIN EXPO_TJ_Imprese_Ateco AS E 
                                                                                                                                              ON E.IdAteco = D.Id) AS H
                                                                                                ON H.IdImpresa = A.IdImpresa                     
                                                   WHERE C.Id_Categoria = '$Id_Categoria'   
                                                     AND H.CodiceAteco NOT IN ('$Descrizione')
                                                     AND A.IsIscritta = 'Y'
                                                     AND A.Id NOT IN (SELECT EXPO_TJ_Imprese_Categorie.Id
                                                                   FROM EXPO_TJ_Imprese_Categorie INNER JOIN EXPO_Tlk_Categorie
                                                                                                          ON EXPO_TJ_Imprese_Categorie.IdCategoria = EXPO_Tlk_Categorie.Id 
                                                                                                  INNER JOIN EXPO_Tlk_Categorie AS Cat2
                                                                                                          ON EXPO_Tlk_Categorie.Id_Categoria = SUBSTRING(Cat2.Id_Categoria,1,8) 
                                                                                                         AND LENGTH(Cat2.Id_Categoria) = 11 
                                                                                                  INNER JOIN EXPO_T_Imprese 
                                                                                                          ON EXPO_TJ_Imprese_Categorie.IdImpresa = EXPO_T_Imprese.Id
                                                                                                  INNER JOIN (SELECT DISTINCT EXPO_T_Ateco.CodiceAteco,
                                                                                                                              EXPO_TJ_Imprese_Ateco.IdImpresa 
                                                                                                                         FROM EXPO_T_Ateco INNER JOIN EXPO_TJ_Imprese_Ateco
                                                                                                                                                   ON EXPO_TJ_Imprese_Ateco.IdAteco = EXPO_T_Ateco.Id) AS Ateco
                                                                                                          ON Ateco.IdImpresa = EXPO_TJ_Imprese_Categorie.IdImpresa   
                                                                                                         AND Ateco.CodiceAteco = Cat2.Descrizione                   
                                                                   WHERE Cat2.Id_Categoria <> '$Id_Categoria'
                                                                     AND EXPO_Tlk_Categorie.Id = B.Id)";*/


        $html = "";
        $query1 = $this->db->GetRows($querySQL);
        
       if (count($query1) > 0) {

           $listaImprese = '';

	        foreach ($query1 AS $rows) {

                $CategoriaMerc = $row['CategoriaDesc'];

                $listaImprese .= ' - ' . $row['RagioneSociale'] . ';<br>';
            }
            $html = '<div class="divClose"><input type="button" onclick="chiudi()" id="close" value="X CHIUDI" /></div>';
            $html .= '<h2>Attenzione!</h2>';
            $html .= 'Non è possibile eliminare il Codice Ateco "' . $Descrizione . ' dal momento che le seguenti imprese risultano già iscritte alla Categoria Merceologica "' . $CategoriaMerc . '" associata al codice Ateco che si sta tentando di eliminare:<br><br>';
            $html .= $listaImprese . '<br><br>';
        } else {
        	
        	$querySQL = "SELECT DISTINCT A.Id
                                               FROM EXPO_TJ_Imprese_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B
                                                                                           ON A.IdCategoria = B.Id
                                                                                   INNER JOIN EXPO_Tlk_Categorie AS C
                                                                                           ON B.Id_Categoria = SUBSTRING(C.Id_Categoria,1,8) AND LENGTH(C.Id_Categoria) = 11
                                                                                    LEFT JOIN (SELECT EXPO_TJ_Imprese_Categorie.Id
                                                                                                      FROM EXPO_TJ_Imprese_Categorie INNER JOIN EXPO_Tlk_Categorie
                                                                                                                                             ON EXPO_TJ_Imprese_Categorie.IdCategoria = EXPO_Tlk_Categorie.Id
                                                                                                                                     INNER JOIN EXPO_Tlk_Categorie AS Cat2
                                                                                                                                             ON EXPO_Tlk_Categorie.Id_Categoria = SUBSTRING(Cat2.Id_Categoria,1,8)
                                                                                                                                            AND LENGTH(Cat2.Id_Categoria) = 11
                                                                                                                                     INNER JOIN EXPO_T_Imprese
                                                                                                                                             ON EXPO_TJ_Imprese_Categorie.IdImpresa = EXPO_T_Imprese.Id
                                                                                                                                     INNER JOIN (SELECT DISTINCT EXPO_T_Ateco.CodiceAteco,
                                                                                                                                                                 EXPO_TJ_Imprese_Ateco.IdImpresa
                                                                                                                                                            FROM EXPO_T_Ateco INNER JOIN EXPO_TJ_Imprese_Ateco
                                                                                                                                                                                      ON EXPO_TJ_Imprese_Ateco.IdAteco = EXPO_T_Ateco.Id) AS Ateco
                                                                                                                                             ON Ateco.IdImpresa = EXPO_TJ_Imprese_Categorie.IdImpresa
                                                                                                                                            AND Ateco.CodiceAteco = Cat2.Descrizione
                                                                                                     WHERE Cat2.Id_Categoria <> '" . $Id_Categoria . "'
                                                                                                       AND EXPO_Tlk_Categorie.Id = EXPO_Tlk_Categorie.Id) AS X
                                                                                           ON A.Id = X.Id
                                              WHERE C.Id_Categoria = '" . $Id_Categoria . "'
                                                AND X.Id IS NULL";
        	
    
  /*      	
            $querySQL = "SELECT DISTINCT A.Id
                                               FROM EXPO_TJ_Imprese_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B 
                                                                                           ON A.IdCategoria = B.Id 
                                                                                   INNER JOIN EXPO_Tlk_Categorie AS C 
                                                                                           ON B.Id_Categoria = SUBSTRING(C.Id_Categoria,1,8) AND LENGTH(C.Id_Categoria) = 11                     
                                              WHERE C.Id_Categoria = '$Id_Categoria'
                                                AND A.Id NOT IN (SELECT EXPO_TJ_Imprese_Categorie.Id
                                                                   FROM EXPO_TJ_Imprese_Categorie INNER JOIN EXPO_Tlk_Categorie
                                                                                                          ON EXPO_TJ_Imprese_Categorie.IdCategoria = EXPO_Tlk_Categorie.Id 
                                                                                                  INNER JOIN EXPO_Tlk_Categorie AS Cat2
                                                                                                          ON EXPO_Tlk_Categorie.Id_Categoria = SUBSTRING(Cat2.Id_Categoria,1,8) 
                                                                                                         AND LENGTH(Cat2.Id_Categoria) = 11 
                                                                                                  INNER JOIN EXPO_T_Imprese 
                                                                                                          ON EXPO_TJ_Imprese_Categorie.IdImpresa = EXPO_T_Imprese.Id
                                                                                                  INNER JOIN (SELECT DISTINCT EXPO_T_Ateco.CodiceAteco,
                                                                                                                              EXPO_TJ_Imprese_Ateco.IdImpresa 
                                                                                                                         FROM EXPO_T_Ateco INNER JOIN EXPO_TJ_Imprese_Ateco
                                                                                                                                                   ON EXPO_TJ_Imprese_Ateco.IdAteco = EXPO_T_Ateco.Id) AS Ateco
                                                                                                          ON Ateco.IdImpresa = EXPO_TJ_Imprese_Categorie.IdImpresa   
                                                                                                         AND Ateco.CodiceAteco = Cat2.Descrizione                   
                                                                   WHERE Cat2.Id_Categoria <> '$Id_Categoria'
                                                                     AND EXPO_Tlk_Categorie.Id = B.Id)";
*/

            foreach ($this->db->GetRows($querySQL) AS $row) {

                $this->db->Query("DELETE FROM EXPO_TJ_Imprese_Categorie WHERE Id = " . $row['Id']);
            }

            if (strlen($Id_Categoria) == 11) {

                $querySQL = "SELECT COUNT(*) AS NumeroRighe 
                                                   FROM EXPO_Tlk_Categorie AS A
                                                  WHERE A.Id_Categoria <> '$Id_Categoria'
                                                    AND A.Descrizione = (SELECT Descrizione FROM EXPO_Tlk_Categorie AS A WHERE A.Id_Categoria = '$Id_Categoria')";

                $numRighe = $this->db->GetRow($querySQL, "NumeroRighe");

                if ($numRighe == 0) {
                    $this->db->Query("UPDATE EXPO_T_Ateco SET IsPresente = 'N' WHERE CodiceAteco IN (SELECT Descrizione FROM EXPO_Tlk_Categorie AS A WHERE A.Id_Categoria = '$Id_Categoria')");
                }
            }

            $this->db->Query("DELETE FROM EXPO_Tlk_Categorie WHERE Id_Categoria = '$Id_Categoria'");
        }
        return $html;
    }

    private function isSubCategoria($Id_Categoria) {
        //controllo se sotto categorie sono presenti in EXPO_TJ_Imprese_Categorie
        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id_Categoria LIKE '$Id_Categoria%'";
        foreach ($this->db->GetRows($query) AS $rows) {
            $Id = $rows['Id'];
            $isCategoria = $this->db->NumRows("SELECT * FROM EXPO_TJ_Imprese_Categorie WHERE IdCategoria = '$Id'");
            if ($isCategoria > 0) {
                return false;
            }
        }
        return true;
    }

    private function notExistDescription($descrizione, $defoult) {
        if (strcmp($descrizione, "") == 0) {
            return $defoult;
        }
        return $descrizione;
    }

    public function codAtecoImpresa($Id_Categoria) {
        if (strlen($Id_Categoria) > 8) {
            $query = "SELECT A.Id,E.IdImpresa
                FROM EXPO_Tlk_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B
                                                     ON A.Id_Categoria = SUBSTRING(B.Id_Categoria,1,8)
                                                    AND LENGTH(B.Id_Categoria) = 11
                                             INNER JOIN EXPO_T_Ateco AS D 
                                                     ON TRIM(B.Descrizione) = TRIM(D.CodiceAteco)
                                             INNER JOIN EXPO_TJ_Imprese_Ateco AS E
                                                     ON E.IdAteco = D.Id
                                                    AND 0 = (SELECT COUNT(*) AS Count2 FROM EXPO_TJ_Imprese_Categorie WHERE IdImpresa = E.IdImpresa AND IdCategoria = A.Id)
                                              LEFT JOIN EXPO_TJ_Imprese_Categorie AS C
                                                     ON A.Id = C.IdCategoria
                                                    AND E.IdImpresa = C.IdImpresa
               WHERE B.Id_Categoria IN ('" . $Id_Categoria . "')    
                 AND C.IdImpresa IS NULL";

            foreach ($this->db->GetRows($query) AS $rows) {

                $massimo = 0;

                $query2 = "SELECT MAX(Id) AS Massimo
                     FROM EXPO_TJ_Imprese_Categorie";

                $rows2 = $this->db->GetRow($query2);

                $massimo = $rows2['Massimo'];
                $massimo += 1;

                $query3 = "INSERT INTO EXPO_TJ_Imprese_Categorie VALUES (" . $massimo . "," . $rows['IdImpresa'] . "," . $rows['Id'] . ",'N')";

                $this->db->Query($query3);
            }
        }
    }

    public function ordinaLista($arrayList) {
        $livello_1_ID = 0;
        $livello_1_TEXT_IT = "";
        $livello_1_TEXT_EN = "";
        $livello_1_TEXT_FR = "";
        $Id_Categoria = "";
        $f = fopen(__DIR__ . "/Log/Error_Log_Ordinamento_Categorie" . date("Y-m-d", time()) . ".log", 'a+');
        foreach ($arrayList AS $value) {
            foreach ($value AS $keyLivello1 => $valueLivello1) {

                switch ($keyLivello1) {
                    case "id":
                        $livello_1_ID ++;
                        if ($livello_1_ID < 9) {
                            $Id_Categoria = "0" . $livello_1_ID;
                        } else {
                            $Id_Categoria = $livello_1_ID;
                        }
                        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id = '$valueLivello1'";
                        $livello_1_TEXT_IT = $this->db->GetRow($query, "Descrizione");
                        $livello_1_TEXT_EN = $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL"), $livello_1_TEXT_IT);
                        $livello_1_TEXT_FR = $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL2"), $livello_1_TEXT_EN);
                        $query = "UPDATE EXPO_Tlk_Categorie SET Id_Categoria = '$Id_Categoria', DescEstesa = '$livello_1_TEXT_IT' , DescEstesa_AL= '$livello_1_TEXT_EN', DescEstesa_AL2 = '$livello_1_TEXT_FR' WHERE Id = '$valueLivello1' ";
                        fwrite($f, $query . "\n");
                        $this->db->Query($query);
                        break;
                    case "children":
                        $livello_2_ID = 0;
                        $livello_2_TEXT_IT = "";
                        $livello_2_TEXT_EN = "";
                        $livello_2_TEXT_FR = "";
                        $Id_Categoria_2 = "";
                        foreach ($valueLivello1 AS $value1) {
                            foreach ($value1 AS $keyLivello2 => $valueLivello2) {
                                switch ($keyLivello2) {
                                    case "id":
                                        $livello_2_ID ++;
                                        if ($livello_2_ID < 9) {
                                            $Id_Categoria_2 = $Id_Categoria . ".0" . $livello_2_ID;
                                        } else {
                                            $Id_Categoria_2 = $Id_Categoria . "." . $livello_2_ID;
                                        }
                                        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id = '$valueLivello2'";
                                        $livello_2_TEXT_IT = $livello_1_TEXT_IT . "->" . $this->db->GetRow($query, "Descrizione");
                                        $livello_2_TEXT_EN = $livello_1_TEXT_EN . "->" . $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL"), $livello_2_TEXT_IT);
                                        $livello_2_TEXT_FR = $livello_1_TEXT_FR . "->" . $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL2"), $livello_2_TEXT_EN);
                                        $query = "UPDATE EXPO_Tlk_Categorie SET Id_Categoria = '$Id_Categoria_2', DescEstesa = '$livello_2_TEXT_IT' , DescEstesa_AL= '$livello_2_TEXT_EN', DescEstesa_AL2 = '$livello_2_TEXT_FR' WHERE Id = '$valueLivello2' ";
                                        fwrite($f, $query . "\n");
                                        $this->db->Query($query);
                                        break;
                                    case "children":
                                        $livello_3_ID = 0;
                                        $livello_3_TEXT_IT = "";
                                        $livello_3_TEXT_EN = "";
                                        $livello_3_TEXT_FR = "";
                                        $Id_Categoria_3 = "";
                                        foreach ($valueLivello2 AS $value2) {
                                            foreach ($value2 AS $keyLivello3 => $valueLivello3) {
                                                switch ($keyLivello3) {
                                                    case "id":
                                                        $livello_3_ID ++;
                                                        if ($livello_3_ID < 9) {
                                                            $Id_Categoria_3 = $Id_Categoria_2 . ".0" . $livello_3_ID;
                                                        } else {
                                                            $Id_Categoria_3 = $Id_Categoria_2 . "." . $livello_3_ID;
                                                        }
                                                        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id = '$valueLivello2'";
                                                        $livello_3_TEXT_IT = $livello_2_TEXT_IT . "->" . $this->db->GetRow($query, "Descrizione");
                                                        $livello_3_TEXT_EN = $livello_2_TEXT_EN . "->" . $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL"), $livello_3_TEXT_IT);
                                                        $livello_3_TEXT_FR = $livello_2_TEXT_FR . "->" . $this->notExistDescription($this->db->GetRow($query, "Descrizione_AL2"), $livello_3_TEXT_EN);
                                                        $query = "UPDATE EXPO_Tlk_Categorie SET Id_Categoria = '$Id_Categoria_3', DescEstesa = '$livello_3_TEXT_IT' , DescEstesa_AL= '$livello_3_TEXT_EN', DescEstesa_AL2 = '$livello_3_TEXT_FR' WHERE Id = '$valueLivello3' ";
                                                        fwrite($f, $query . "\n");
                                                        $this->db->Query($query);
                                                        break;
                                                    case "children":
                                                        $livello_4_ID = 0;
                                                        $livello_4_TEXT_IT = "";
                                                        $livello_4_TEXT_EN = "";
                                                        $livello_4_TEXT_FR = "";
                                                        $Id_Categoria_4 = "";
                                                        foreach ($valueLivello3 AS $value3) {
                                                            foreach ($value3 AS $keyLivello4 => $valueLivello4) {
                                                                switch ($keyLivello4) {
                                                                    case "id":
                                                                        $livello_4_ID ++;
                                                                        if ($livello_4_ID < 9) {
                                                                            $Id_Categoria_4 = $Id_Categoria_3 . ".0" . $livello_4_ID;
                                                                        } else {
                                                                            $Id_Categoria_4 = $Id_Categoria_3 . "." . $livello_4_ID;
                                                                        }
                                                                        $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id = '$valueLivello2'";
                                                                        $livello_4_TEXT_IT = $livello_3_TEXT_IT . "->" . $this->db->GetRow($query, "Descrizione");
                                                                        $livello_4_TEXT_EN = $livello_3_TEXT_EN . "->" . $this->db->GetRow($query, "Descrizione");
                                                                        $livello_4_TEXT_FR = $livello_3_TEXT_FR . "->" . $this->db->GetRow($query, "Descrizione");
                                                                        $query = "UPDATE EXPO_Tlk_Categorie SET Id_Categoria = '$Id_Categoria_4', DescEstesa = '$livello_4_TEXT_IT' , DescEstesa_AL= '$livello_4_TEXT_EN', DescEstesa_AL2 = '$livello_4_TEXT_FR' WHERE Id = '$valueLivello4' ";
                                                                        fwrite($f, $query . "\n");
                                                                        $this->db->Query($query);
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        break;
                }
            }
        }
        fclose($f);
    }

}
