<script language="javascript">
    
    function selezionaLingua(lang,id){
    var par = 'lang='+lang+'&Id=' + id
    + "&Ordine=" + $("#Ordine").val()
	+ "&IsStudio=" + $("#IsStudio").val()
    +"&NumeroDipendenti="+$("#NumeroDipendenti").val()
    +"&descrizioneL="+$("#descrizioneL").val()
    +"&Telefono="+$("#tel").val()
    +"&Fax="+$("#fax").val()
    +"&Email="+$("#mail").val()
    +"&WebSiteURL="+$("#webSite").val()
    +"&UltimoFatturato="+$("#ultimoFatturato").val()
    +"&FatturatoUltimoEsercizio="+$("#FatturatoUltimoEsercizio").val()
    +"&DescrizioneAttivita="+$("#descrizione").val()
    +"&DescEstesa="+$("#descrizione").serialize()
    +"&DescEstesa_AL="+$("#descrizioneEN").serialize()
    +"&DescEstesa_AL2="+$("#descrizioneFR").serialize()

    +"&internazionalizzazioneL="+$("#internazionalizzazioneL").val()
    +"&referenzeL="+$("#referenzeL").val()
    +"&qualitaL="+$("#qualitaL").val()
    +"&qualita="+$("#qualita").val()
    +"&qualitaEN="+$("#qualitaEN").val()
    +"&qualitaFR="+$("#qualitaFR").val()
    +"&retiImpresaL="+$("#retiImpresaL").val()

    +"&aModello="+$("#aModello:checked").val()
    +"&IsMasterSpecialistico="+$("#pMasetr:checked").val()
    +"&IsCertificazione="+$("#pCertificazione:checked").val()
    +"&IsGeneralContractor="+$("#gereralContractor:checked").val()

    +"&IsProduttore="+$("#produttore:checked").val()
    +"&IsDistributore="+$("#distributore:checked").val()
    +"&IsPrestatoreDiServizi="+$("#IsPrestatoreDiServizi:checked").val()
    +"&IsSediEstere="+$("#sedEstera:checked").val()

    +"&IsNetwork="+$("#netetwork:checked").val()
    +"&partecipazioneExpo="+$("#partecipazioneExpo:checked").val()
    +"&partecipazioniInternazionali="+$("#partecipazioniInternazionali:checked").val()
 
;


    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc",
        data: par,
        success: function(response) {
            $("#box_popup").html(response);

        }});
        
    }
</script>
<ul>
<?php

$dbLang = new DataBase();
 
foreach ($dbLang->GetRows("SELECT Label,Lang FROM EXPO_T_Lingue_Sito ORDER BY Id") AS $rowLang) {
    echo "<li>";
    if (strcmp($Lang, $rowLang['Lang']) == 0)
        echo "<span>".$rowLang['Label']."</span>";
    else
        echo '<a class="' . strtolower($rowLang['Lang']) . '" onclick="selezionaLingua(\''.$rowLang['Lang'].'\',\''.$IdImpresa.'\')" >' . $rowLang['Label'] . '</a>';
    echo "</li>";
}
?>
</ul>