<?php

include_once("configuration.inc");

class UpdateFile {

    private $allowedExts = array("pdf", "p7m"); // ESTENZIONE FILE
    private $rootFile = Config::ROOTFILES;
    private $sizeMax = 2000000; //PESO PASSIMO DEL FILE
    private $extension;

//$_FILES["file"]["size"] < 20000
    //CONTROLLA IL SIZE DELLA CARTELLA IMPRESA
    private function du($dir) {
        $res = `du -sk $dir`;             // Unix command
        preg_match('/\d+/', $res, $KB); // Parse result
        $MB = round($KB[0] / 1024, 1);  // From kilobytes to megabytes
        return $MB;
    }

    //CONTROLA IL FILE
    private function controlloFile($key, $dir) {

        $temp = explode(".", $_FILES[$key]["name"]);
        $this->extension = strtolower(end($temp));
        $spazioLibero = round($_FILES[$key]["size"] / 1024, 1) + $this->du($dir);
        if (in_array($this->extension, $this->allowedExts) && $spazioLibero < $this->sizeMax) {
            if ($_FILES[$key]["error"] == 0)
                return TRUE;
        }

        return FALSE;
    }

    public function deleteFile($dir, $subDir, $nome) {
        $pathDir = $this->rootFile . $dir . $subDir;
        @unlink($pathDir . "/" . $nome);
    }

    public function updateFiles($listFiles, $dir, $subDir, $db, $idimpresa) {

        $pathDir = $this->rootFile . $dir . $subDir;

        if (!file_exists($pathDir)) {
            mkdir($pathDir, 0700, true);
        }

        foreach ($listFiles as $key => $value) {

            $nameFile = substr($key, 0, strlen($key) - 1);

            if ($value['name'] != '') {
                $temp = explode(".", $_FILES[$key]["name"]);
                $query = "SELECT fd.Dimensione from EXPO_TJ_FasciaUpload_Campi as ff
                 join EXPO_Tlk_FasciaDownload as fd on ff.IdFascia=fd.Id
                 where ff.Campo='" . $key . "'";
                if ($db->NumRows($query) > 0) {
                    $this->sizeMax = $db->GetRow($query, 'Dimensione');
                } else {
                    $query = "SELECT fd.Dimensione from EXPO_TJ_FasciaUpload_Campi as ff
                 join EXPO_Tlk_FasciaDownload as fd on ff.IdFascia=fd.Id
                 where ff.Campo='default'";
                    $this->sizeMax = $db->GetRow($query, 'Dimensione');
                }
                $this->extension = strtolower(end($temp));
                if (!in_array($this->extension, $this->allowedExts)) {
                    $_SESSION['errore']['file'][$key] = "<div class='error'>Tipo di file errato. E' permesso il caricamento di soli file pdf</div>";
                } elseif ($_FILES[$key]["size"] >= $this->sizeMax) {
                    $_SESSION['errore']['file'][$key] = "<div class='error'>Dimensione massima di upload &egrave; di " . ceil($this->sizeMax / 1024 / 1024) . " Mbyte</div>";
                } else {

                    if ($this->controlloFile($key, $dir)) {

                        if (move_uploaded_file($_FILES[$key]["tmp_name"], $pathDir . $nameFile . "." . $this->extension)) {
                            switch ($key) {
                                case 'masterF':
                                    $update = "UPDATE EXPO_T_Imprese set IsMasterSpecialistico='Y' where Id='" . $idimpresa . "'";
                                    $db->Query($update);
                                    break;
                                case 'pCertificazione':
                                    $update = "UPDATE EXPO_T_Imprese set IsCertificazione='Y' where Id='" . $idimpresa . "'";
                                    $db->Query($update);
                                    break;
                                /*case 'retiImpresaF':
                                    $update = "UPDATE EXPO_T_Imprese set IsRetiImpresa='Y' where Id='" . $idimpresa . "'";
                                    $db->Query($update);
                                    break;*/
                                default:
                                    break;
                            }
                        } else {
                            echo "Errore nel caricamento del file";
                        }
                    } else {
                        echo "Errore nel caricamento del file";
                    }
                }
            }
        }
    }

    public function updateF($listFiles, $dir, $subDir) {

        $pathDir = $dir;

        if (!file_exists($pathDir)) {
            @mkdir($pathDir, 0777);
        }

        if (strcmp($subDir, "") != 0) {
            $pathDir .= $subDir . "/";
            if (!is_dir($pathDir)) {
                @mkdir($pathDir, 0777);
            }
        }



        foreach ($listFiles as $key => $value) {
//            $nameFile = substr($key, 0, strlen($key) - 1);

            if ($this->controlloFile($key, $dir)) {
                if (file_exists($pathDir . $_FILES[$key]["name"])) {
                    unlink($pathDir . $_FILES[$key]["name"]);
                }
                move_uploaded_file($_FILES[$key]["tmp_name"], $pathDir . $_FILES[$key]["name"]);
                return $_FILES[$key]["name"];
            } else {
                echo "Impossibile caricare il file. Le possibili motivazioni sono :<br>
                    <ul>
                       <li>Estensione diversa da quelle permesse</li>
                       <li>Dimensione troppo grande del file</li>
                    </ul>";
            }
        }
    }

    public function getInfoFile($file, $sizeMax) {
        $iFile = array();

        if ($file['size'] <= $sizeMax) {

            $fp = fopen($file['tmp_name'], 'r');
            $content = fread($fp, filesize($file['tmp_name']));
            $content = addslashes($content);
            fclose($fp);
            $iFile['tmp_name'] = $file['tmp_name'];
            $iFile['name'] = $fileName = addslashes($file['name']);
            $iFile['type'] = $file['type'];
            $iFile['size'] = $file['size'];
            $iFile['content'] = $content;
            return $iFile;
        }

        return NULL;
    }

}

?>
