<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("configuration.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

/**
 * Description of getVisiteVetrina
 *
 * @author Francesco Spagnoli
 */
class GetReport {

    //put your code here
    public $db;
    public $arrayMesi = array("01" => 'Genanio', "02" => 'Febbraio', "03" => 'Marzo', "04" => 'Aprile', "05" => 'Maggio', "06" => 'Giugno', "07" => 'Luglio', "08" => 'Agosto', "09" => 'Settembre', "10" => 'Ottobre', "11" => 'Novembre', "12" => 'Dicembre');

    public function __construct() {
        $this->db = new DataBase();
    }

    public function getReportVetrina($dataInizio, $dataFine) {

        list($annoInizio, $meseInizio, $giornoInizio) = explode("-", $dataInizio);
        list($annoFine, $meseFine, $giornoFine) = explode("-", $dataFine);

        $arrayLogs = array();
        $arrayLogs['totVisiteImprese'] = 0;
        $arrayLogs['totVisiteProdotti'] = 0;
        $arrayLogs['totVisite'] = 0;

        for ($anno = $annoInizio; $anno <= $annoFine; $anno++) {
            $meseFineCiclo = 12;
            $meseInizioCiclo = 1;
            if ($anno == $annoInizio) {
                $meseInizioCiclo = $meseInizio;
            }

            if ($anno == $annoFine) {
                $meseFineCiclo = $meseFine;
            }

            for ($i = $meseInizioCiclo; $i <= $meseFineCiclo; $i++) {

                $mese = sprintf("%02d", $i);

                //QUERY PER VISITE IMPRESE / PRODOTTI
                $giornoInizioCiclo = "01";
                if (strcmp($anno, $annoInzio) == 0 && strcmp($mese, $meseInizio) == 0) {
                    $giornoInizioCiclo = $giornoInizio;
                }
                $giornoFineCiclo = "31";
                if (strcmp($anno, $annoFine) == 0 && strcmp($mese, $meseFine) == 0) {
                    $giornoFineCiclo = $giornoFine;
                }

                $dataInizioDB = $anno . "-" . $mese . "-" . $giornoInizioCiclo;
                $dataFineDB = $anno . "-" . $mese . "-" . $giornoFineCiclo;

                $arrayLog['mese'] = $this->arrayMesi[$mese];
                $arrayLog['anno'] = $anno;
                $arrayLog['periodo'] = $this->arrayMesi[$mese] . " " . $anno;

                $arrayLogin = array();
                $arrayPaeseLogin = array();
                $arrayLog['totaliLoginMese'] = 0;
                $query = "SELECT * FROM EXPO_T_Vetrina_Logs WHERE Data >= '$dataInizioDB 24:00:00' AND Data <= '$dataFineDB 24:00:00' AND Azione = 'Login PDMS2' AND parametri <> 'idPartecipante=1468'";

                foreach ($this->db->GetRows($query) AS $rows) {
                    parse_str($rows['parametri'], $arrayParametri);
                    $queryPartecipante = "SELECT * FROM T_Anagrafica AS T JOIN Tlk_Nazioni AS T1 ON T.Sigla_Nazione = T1.Sigla_Nazione WHERE Id = '" . $arrayParametri['idPartecipante'] . "'";
                    $paese = $this->db->GetRow($queryPartecipante, 'Nome_Nazione');
                    if (in_array($paese, $arrayPaeseLogin)) {
                        $arrayLogin[$paese] ++;
                    } else {
                        $arrayPaeseLogin[] = $paese;
                        $arrayLogin[$paese] = 1;
                    }
                    $arrayLog['totaliLoginMese'] ++;
                    $arrayLogs['totVisite'] = $arrayLogs['totVisite'] + 1;
                }





                $arrayImprese = array();
                $arrayProdotti = array();
                $arrayID = array();
                $arrayIDProd = array();
                $arrayPaese = array();
                $arrayLog['totaliVisiteMeseImprese'] = 0;
                $arrayLog['totaliVisiteMeseProdotti'] = 0;
                $query = "SELECT * FROM EXPO_Log_Visite_Vetrina WHERE DataVisita >= '$dataInizioDB' AND DataVisita <= '$dataFineDB'";
                foreach ($this->db->GetRows($query) AS $rows) {

                    $idImpresa = $rows['Id_Impresa'];
                    $idPartecipante = $rows['Id_Partecipante'];
                    $idProdotto = $rows['Id_Prodotto'];

                    $queryPartecipante = "SELECT * FROM T_Anagrafica AS T JOIN Tlk_Nazioni AS T1 ON T.Sigla_Nazione = T1.Sigla_Nazione WHERE Id = '$idPartecipante'";
                    $paese = $this->db->GetRow($queryPartecipante, 'Nome_Nazione');

                    if (strcmp($paese, "") != 0) {
                        if (!in_array($idImpresa, $arrayID) && strcmp($idProdotto, "0") == 0) {
                            $nome = $this->db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", 'RagioneSociale');
                            $arrayID[] = $idImpresa;
                            $arrayImprese[$idImpresa]['nome'] = $nome;
                            $arrayImprese[$idImpresa]['paesi'][$paese] = 1;
                            $arrayImprese[$idImpresa]['vistite'] = 1;
                            $arrayLog['totaliVisiteMeseImprese'] ++;
                            $arrayLogs['totVisiteImprese'] ++;
                        } elseif (strcmp($idProdotto, "0") == 0) {
                            $arrayImprese[$idImpresa]['vistite'] ++;
                            $arrayImprese[$idImpresa]['paesi'][$paese] ++;
                            $arrayLog['totaliVisiteMeseImprese'] ++;
                            $arrayLogs['totVisiteImprese'] ++;
                        }

                        if (!in_array($idProdotto, $arrayIDProd) && strcmp($idProdotto, "0") != 0) {
                            $impresa = $this->db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$idImpresa'", 'RagioneSociale');
                            $prodotto = $this->db->GetRow("SELECT * FROM EXPO_T_Prodotti WHERE Id = '$idProdotto'", 'Nome',null,"getReport.class LN.126");
                            $arrayIDProd[] = $idProdotto;
                            $arrayProdotti[$idProdotto]['impresa'] = $impresa;
                            $arrayProdotti[$idProdotto]['nome'] = $prodotto;
                            $arrayProdotti[$idProdotto]['paesi'][$paese] = 1;
                            $arrayProdotti[$idProdotto]['vistite'] = 1;
                            $arrayLog['totaliVisiteMeseProdotti'] ++;
                            $arrayLogs['totVisiteProdotti'] ++;
                        } elseif (strcmp($idProdotto, "0") != 0) {
                            $arrayProdotti[$idProdotto]['paesi'][$paese] ++;
                            $arrayProdotti[$idProdotto]['vistite'] ++;
                            $arrayLog['totaliVisiteMeseProdotti'] ++;
                            $arrayLogs['totVisiteProdotti'] ++;
                        }
                        $arrayPaese[$paese] = $arrayPaese[$paese] + 1;
                    }
                }
                $arrayLog['totaliVisitePaese'] = $arrayPaese;
                $arrayLog['login'] = $arrayLogin;
                $arrayLog['imprese'] = $arrayImprese;
                $arrayLog['prodotti'] = $arrayProdotti;
                $arrayLogs[] = $arrayLog;
            }
        }
        return $arrayLogs;
    }

    public function getReportTrattative($dataInizio, $dataFine) {
        list($annoInizio, $meseInizio, $giornoInizio) = explode("-", $dataInizio);
        list($annoFine, $meseFine, $giornoFine) = explode("-", $dataFine);

        $arrayLogs = array();
        $arrayLogs['totChiuse'] = 0;
        $arrayLogs['totChiuseCategorie'] = 0;

        for ($anno = $annoInizio; $anno <= $annoFine; $anno++) {
            $meseFineCiclo = 12;
            $meseInizioCiclo = 1;
            if ($anno == $annoInizio) {
                $meseInizioCiclo = $meseInizio;
            }

            if ($anno == $annoFine) {
                $meseFineCiclo = $meseFine;
            }

            for ($i = $meseInizioCiclo; $i <= $meseFineCiclo; $i++) {

                $mese = sprintf("%02d", $i);

                //QUERY PER VISITE IMPRESE / PRODOTTI
                $giornoInizioCiclo = "01";
                if (strcmp($anno, $annoInzio) == 0 && strcmp($mese, $meseInizio) == 0) {
                    $giornoInizioCiclo = $giornoInizio;
                }
                $giornoFineCiclo = "31";
                if (strcmp($anno, $annoFine) == 0 && strcmp($mese, $meseFine) == 0) {
                    $giornoFineCiclo = $giornoFine;
                }

                $dataInizioDB = $anno . "-" . $mese . "-" . $giornoInizioCiclo;
                $dataFineDB = $anno . "-" . $mese . "-" . $giornoFineCiclo;

                $arrayLog['mese'] = $this->arrayMesi[$mese];
                $arrayLog['anno'] = $anno;
                $arrayLog['periodo'] = $this->arrayMesi[$mese] . " " . $anno;

                $arrayTrattative = array();
                $arrayPaeseTrattativa = array();
                $arrayLog['totaliMese'] = 0;
                $query = "SELECT  Nome_Nazione "
                        . "FROM EXPO_T_Bacheca AS T JOIN  EXPO_T_Bacheca_Trattative AS T1 ON T.Id = T1.IdBacheca "
                        . "JOIN T_Anagrafica AS T2 ON T2.Id = T.IdPartecipante "
                        . "JOIN Tlk_Nazioni AS T3 ON T2.Sigla_Nazione = T3.Sigla_Nazione "
                        . "WHERE T1.Data >= '$dataInizioDB 24:00:00' "
                        . "AND T1.Data <= '$dataFineDB 24:00:00' "
                        . "AND Stato = 'C'";
                foreach ($this->db->GetRows($query) AS $rows) {
                    $paese = $rows['Nome_Nazione'];
                    if (in_array($paese, $arrayPaeseTrattativa)) {
                        $arrayTrattative[$paese] ++;
                    } else {
                        $arrayPaeseTrattativa[] = $paese;
                        $arrayTrattative[$paese] = 1;
                    }
                    $arrayLog['totaliMese'] ++;
                    $arrayLogs['totChiuse'] ++;
                }


                $arrayTrattativeCategorie = array();
                $arrayCategorie = array();
                $arrayPaeseTrattativa = array();
                $arrayLog['totaliMeseCategorie'] = 0;
                $query = "SELECT DISTINCT Nome_Nazione,T7.Descrizione AS d "
                        . "FROM EXPO_T_Bacheca AS T "
                        . "JOIN EXPO_T_Bacheca_Trattative AS T1 ON T.Id = T1.IdBacheca "
                        . "JOIN T_Anagrafica AS T2 ON T2.Id = T.IdPartecipante "
                        . "JOIN Tlk_Nazioni AS T3 ON T2.Sigla_Nazione = T3.Sigla_Nazione "
                        . "JOIN EXPO_T_Imprese AS T4 ON T.IdImpresa = T4.Id "
                        . "JOIN EXPO_TJ_Imprese_Categorie AS T5 ON T4.Id = T5.IdImpresa "
                        . "JOIN EXPO_Tlk_Categorie AS T6 ON T5.IdCategoria = T6.Id "
                        . "JOIN EXPO_Tlk_Categorie AS T7 ON SUBSTRING(T6.Id_Categoria,1,2) = T7.Id_Categoria AND LENGTH(T7.Id_Categoria) = 2 "
                        . "WHERE T1.Data >= '$dataInizioDB 24:00:00' AND T1.Data <= '$dataFineDB 24:00:00' AND Stato = 'C'";

                foreach ($this->db->GetRows($query) AS $rows) {
                    $paese = $rows['Nome_Nazione'];
                    $categoria = $rows['d'];
                    if (in_array($categoria, $arrayCategorie)) {
                        $arrayTrattativeCategorie[$categoria]["paese"][$paese] ++;
                        $arrayTrattativeCategorie[$categoria]["visite"] ++;
                    } else {
                        $arrayCategorie[] = $categoria;
                        $arrayTrattativeCategorie[$categoria]["paese"][$paese] = 1;
                        $arrayTrattativeCategorie[$categoria]["visite"] = 1;
                    }

                    $arrayLog['totaliMeseCategorie'] ++;
                    $arrayLogs['totChiuseCategorie'] ++;
                }

                $arrayLog['categorie'] = $arrayTrattativeCategorie;
                $arrayLog['trattative'] = $arrayTrattative;
                $arrayLogs[] = $arrayLog;
            }
        }
        return $arrayLogs;
    }

}
