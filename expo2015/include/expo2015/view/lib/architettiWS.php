<?php
//include "/lib/nusoap/nusoap.php";
//include "/view/lib/db.class.php";

global $debugProfessionisti;

class architettiWS {
	private $db;
	const DEBUG = FALSE;
	

	public function __construct(){
		$this->db = new DataBase();
		
	}
	
	
	private function traceLog($cf,$risultato,$stato){
		$result = mysql_escape_string($risultato);
		$xml="<?xml version = \'1.0\' ?>
    		<scheda-persona>
    		<codice-fiscale>$cf</codice-fiscale>
    		<ordine>Architetti
    		</ordine>
    		<risposta>
    		$result
    		</risposta>
    		<stato>
    		$stato
    		</stato>
    		
    		</scheda-persona>";
		
		$req_datiri="professionista=".$cf."&user=architetti";
		 
		$queryst = "INSERT INTO `interrogazioni_professionisti` (`user`,`url_chiamata`,`risposta`,`data_chiamata`)
            				VALUES ('Architetti','schedaPersona:".$req_datiri."','".$xml."','".date('Y-m-d H:i:s')."')";

		$this->db->Query($queryst, Config::DB_DATIRI_NAME,"trace log architettiWS Ln.39");
	}

	private function callWebServiceOrdineArchitetti ($cf){

		$string = "----";
		if (!(self::DEBUG)){
				
			try {
				$gsearch = new SoapClient ('http://www.cnappc.it/webservices/ruWS.asmx?WSDL',array("timeout" => 5));
				$params = array("codiceFiscale" => $cf);


				//print "<h1>TRY</h1><br/>";
				$stringArray = (array)$gsearch->cercaIscritti_CodiceFiscaleRetStream($params);

				if (strlen($stringArray['cercaIscritti_CodiceFiscaleRetStreamResult']) > 0){
				
				$info = explode("|", $stringArray['cercaIscritti_CodiceFiscaleRetStreamResult']);
					
				$string = $info[0]."|".$info[1]."|".$info[4]."|".$info[5]."|".$info[6];
				
				
					$stato = 4;
				}else {
					$stato = 3;
				}
				
				
			} catch (SoapFault $e) {
				/*
				 print "<h1>CATCH</h1><br/>";
				print "<pre>";
				print_r($e);
				print "</pre>";
				*/
					
				$string = "FAULT";
				$stato = 2;
			}

		}else {
			
			$result = "GIULIANO|AZZINARI|CORIGLIANO CALABRO|16/02/1968|ZZNGLN68B16D005R|124|13418|1";
			$info = explode("|", $result);
				
			$string = $info[0]."|".$info[1]."|".$info[4]."|".$info[5]."|".$info[6];
			$stato = 4;
			
		}
		/*
		 $sql = "INSERT INTO __test_ws_architetti values ('',NOW(),'$string')";
		$this->db->Query($sql,null,"Test WS Architetti");
		*/
		$result = array("Risultato" =>$string, "Stato"=>$stato);
		$this->traceLog($cf, $string,$stato);
		
		return $result;
	}


	public function getArchitetto($cf){
		$result =  $this->callWebServiceOrdineArchitetti($cf);
		
		
		return $result;
	}








	public function reportCall (){
		$sql ="SELECT count(*) as Totale FROM __test_ws_architetti";
		$sqlRisposteVuote ="SELECT count(*) as RisposteVuote  FROM __test_ws_architetti WHERE StringaValore = ''";
		$sqlRisposte ="SELECT count(*) as Risposte  FROM __test_ws_architetti WHERE StringaValore <> '' and StringaValore <> 'FAULT';";
		$sqlFault ="SELECT count(*) as Fault  FROM __test_ws_architetti WHERE StringaValore = 'FAULT';";

		$totale = $this->db->GetRow($sql,"Totale");
		$risposteVuote = $this->db->GetRow($sqlRisposteVuote,"RisposteVuote");
		$risposte = $this->db->GetRow($sqlRisposte,"Risposte");
		$fault = $this->db->GetRow($sqlFault,"Fault");

		?>

<table>
	<tr>
		<td>Risposte</td>
		<td><?php echo $risposte?></td>
	</tr>
	<tr>
		<td>Risposte Vuote</td>
		<td><?php echo $risposteVuote?></td>
	</tr>
	<tr>
		<td>Fault</td>
		<td><?php echo $fault?></td>
	</tr>
	<tr>
		<td>Totale Risposte</td>
		<td><?php echo $totale?></td>
	</tr>
</table>


<?php 


	}

}