<?php

require_once($PROGETTO . "/view/lib/architettiWS.php");
require_once($PROGETTO . "/view/lib/parix.class.php");
require_once($PROGETTO . "/view/lib/db.class.php");



class GestioneProfessionista{

	
	private $db;
	private $architettiWs;
	private $anagrafica;
	private $rootDoc;
	private $idProf;
	private $arrayfiles;
	
	const ARCHITETTI = 1;
	
	
	const PRINT_LISTA_IMPRESE = false;
	
	public function __construct(){
		$this->architettiWs = new architettiWS();
		$this->db = new DataBase();
		
	}

	private function getRootDoc($codiceFiscale){
		$sql = "SELECT Concat(Cognome,' ',Nome) as Persona,Id FROM T_Anagrafica WHERE Codice_Fiscale = '$codiceFiscale'";
		
		$sqlResult = $this->db->GetRow($sql,null,null, "estrai persona gestione professionisti.inc");
		
		$this->rootDoc = selectRootDocPersona($sqlResult['Persona']).  "/" . $sqlResult['Id'];
				
	}
	

	private function elencafiles(){
		$dir = Config::ROOTFILES.$this->rootDoc;
		$dirAutocertificazioni = $dir."/autocertificazioni/";
		$this->arrayfiles=Array();
		if(file_exists($dirAutocertificazioni)){
			$handle = opendir($dirAutocertificazioni);
			while (false !== ($file = readdir($handle))) {
				if(is_file($dirAutocertificazioni.$file)){
					//array_push($this->arrayfiles,$file);
					$end = explode('.',$file);
					$this->arrayfiles[$end[0]] = $file;
				}
			}
			$handle = closedir($handle);
		}

	}
	
	
	private function checkCertification($idOrdine){
		$dir = Config::ROOTFILES.$this->rootDoc;
		$dirAutocertificazioni = $dir."/autocertificazioni";
		
		$fileName = $this->arrayfiles[$idOrdine];
		$autocertificazione = $dirAutocertificazioni."/".$fileName;
		
		if (file_exists($autocertificazione) && (!is_dir($autocertificazione))){
			return 5;
		}
		return 0;
	}
	
	
	private function genericWS ($cf){
	
		$anagraficaSQL = "SELECT Id,Nome,Cognome,Codice_Fiscale FROM T_Anagrafica WHERE Codice_Fiscale = '$cf'";
		$anagraficaDB = $this->db->GetRow($anagraficaSQL,null,null,"estrazione professionista ln.79");
		$this->idProf = $anagraficaDB['Id'];
		$this->anagrafica= $anagraficaDB['Nome'].'|'.$anagraficaDB['Cognome'].'|'.$anagraficaDB['Codice_Fiscale'].'||';
	}
	
	
	public function isRegistered($cf,$idProf,$ordine){
		$statoLP = $this->getStatoProfessionista($cf,$ordine);
		
		if ($statoLP > 3){
			return true;
		} else {
			$resultStudi = $this->getStudioProfessionista($idProf, $ordine);
			foreach ($resultStudi AS $value) {
				if ($value['Stato'] > 3){
					return true;
				}
			}
		}
		return false;
	}
	
	public function getStatoProfessionista($codiceFiscale,$ordine){
		$sqlStato= "SELECT I.CodiceFiscale,I.StatoRegistrazione as Stato,TJ.Id_OrdineProfessionale 
					FROM EXPO_T_Imprese as I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa
					WHERE I.CodiceFiscale = '$codiceFiscale' AND TJ.Id_OrdineProfessionale = $ordine AND TJ.IsStudio = 'N'";
		
		$stato = $this->db->GetRow($sqlStato, "Stato");
		
		if (!$stato){
			$stato = 0;
		}
		return $stato;
	}
	
	
	public function getStudioProfessionista($id,$ordine){
		$sqlStudi= "SELECT I.RagioneSociale, I.CodiceFiscale,I.StatoRegistrazione as Stato,TJ.Id_OrdineProfessionale AS Ordine, I.OrdineProvinciale,I.NumeroIscrizione
					FROM ((T_Anagrafica AS A JOIN EXPO_TJ_Imprese_Utenti AS U ON A.Id = U.IdUtente)
					JOIN EXPO_T_Imprese as I ON U.IdImpresa = I.Id) JOIN EXPO_TJ_Imprese_OrdiniProfessionali AS TJ ON I.Id = TJ.Id_Impresa 
					WHERE A.Id = $id AND TJ.Id_OrdineProfessionale = $ordine AND TJ.IsStudio = 'Y'";
		
		//print $sqlStudi;
		$studi = $this->db->GetRows($sqlStudi,null, "Estrazione studio Ln. 49");
	
		return $studi;
	}
	
	private function interrogaWS($idOrdine,$codiceFiscale){
		$result = false;
		switch ($idOrdine){
			//Architetti
			case self::ARCHITETTI: $result = $this->architettiWs->getArchitetto($codiceFiscale);
									break;
			default: 
				$result = array("Risultato" =>$this->anagrafica, "Stato"=>$this->checkCertification($idOrdine));
				break;
		}
		
		return $result;
	}
	
	private function estraiOrdini(){
		
		
		$sqlOrdini = "SELECT Id,NomeOrdineProfessionale as Ordine FROM EXPO_Tlk_OrdiniProfessionali WHERE Id > 0";
		$resultOrdini = $this->db->GetRows($sqlOrdini, null, null, "esrazione EXPO_Tlk_Ordini");
		
		return $resultOrdini;
	}
	
	
	
	
	
	public function getInfoProfessionista($cf){
		$resultOrdini = $this->estraiOrdini();
		$this->getRootDoc($cf);
		$this->elencafiles();
		$this->genericWS($cf);
		
		foreach ($resultOrdini as $key => $value){
			$ordine = array();
			$ordine["Id"] = $value["Id"];
			$ordine["Ordine"] = $value["Ordine"];
			
			
			$ws = $this->interrogaWS($value["Id"], $cf);
			
			$ordine["RisultatoWS"] = $ws["Risultato"];
			$ordine["stato_registrazione"] = $ws["Stato"];
				
			$arrayOrdini[] = $ordine;
		}
		
		/*
		print "<pre>";
		print_r ($arrayOrdini);
		print "</pre>";
		*/
		
		return $arrayOrdini;
	} 
	
}

?>