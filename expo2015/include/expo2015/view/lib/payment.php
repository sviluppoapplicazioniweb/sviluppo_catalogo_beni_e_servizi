<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of payment
 *
 * @author XddG123
 */
require_once($PROGETTO . "/view/lib/XML2Array.php");
require_once($PROGETTO . "/view/lib/db.class.php");
    
class payment {

        public function getInfoPagamentoMyBank($cfimpresa,  $fascia,  $amount,$uid, $ordineProfessionista) {
        	//echo "Info MyBank";
        	//exit;
        	//echo "Ordine ".$ordineProfessionista;
        	//exit;
        	if ($ordineProfessionista > 0){
        		$id_VMenu = 507;
        	} else {
        		$id_VMenu = 310;
        	}
        	
	        global $idClienteMyBank,$passCliente,$debugIscrzione,$MAINSITE,$debugPagam;
	        if($debugIscrzione['pagamento']==true){
	        	$url = "https://test.monetaonline.it/monetaweb/payment/2/xml"; //url test
	    //$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
	        }else{
	          	$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
	        }
	
	        $description = "Pagamento EXPO2015";
	        $customField = "";
	
	//$idClienteMyBank='97679899';
	//$passClientemybank='Expo2015ua12';
	        $idOrder =  uniqid();
	        $fields = array(
	            'description' => 'Iscrizione Catalogo Fornitori Impresa : '.$cfimpresa .' - Ordine '.$ordineProfessionista,
	            'amount' => $amount,
	            'merchantOrderId' => $idOrder,
	            'language' => 'ITA',
	            'responseToMerchantUrl' => urlencode($MAINSITE.'index.phtml?Id_VMenu='.$id_VMenu.'&azione=PAYMYBANK&idorder=' . $idOrder . '&cfimpresa=' . $cfimpresa . '&ordineProfessionista=' . $ordineProfessionista),
	        		//'responseToMerchantUrl' => urlencode($MAINSITE.'testMyBank.php'),
	            'recoveryUrl' => urlencode($MAINSITE.'index.phtml?Id_VMenu='.$id_VMenu.'&azione=PAYMYBANK&idorder=' . $idOrder . '&cfimpresa=' . $cfimpresa  . '&ordineProfessionista=' . $ordineProfessionista),
	        		//'recoveryUrl' => urlencode($MAINSITE.'testMyBank.php'),
	            'operationType' => 'initializemybank',
	            'currencyCode' => '978',
	            'id' => $idClienteMyBank,
	            'password' => $passCliente
	        );
	        $fields_string = '';
	    //url-ify the data for the POST
	        foreach ($fields as $key => $value) {
	            $fields_string .= $key . '=' . $value . '&';
	        }
	//echo "<pre>";
	//echo "URL :".$url;
	//echo "<br>";
	//echo $fields_string;
	//echo "</pre>";
	//exit;
	        rtrim($fields_string, '&');
	//echo $fields_string;echo "<br>";
	    //open connection
	        $ch = curl_init();
	
	    //set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_POST, count($fields));
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	
	    //execute post
	        $result = curl_exec($ch) or die(curl_error($ch));
	 		
        //echo "Debug".$uid;
        //exit;
        //Utenza 167: Ferla Alessandro
        if ($debugPagam == true and $uid == 167){
	    //if ($debugPagam == true){
    		echo "Request: ".$fields_string."<br/>";
        	echo "Result: <br/>";
    		print_r($result);
    		exit;
    	}
	        $array = XML2Array::createArray($result);
	
	        $array['response']['amount'] = $amount;
	        $array['response']['orderid'] = $idOrder;
	        $array['response']['description'] = 'Iscrizione Catalogo Fornitori Impresa : '.$cfimpresa .' - Ordine '.$ordineProfessionista; 
	        
	        //'Iscrizione Catalogo Fornitori Impresa : '.$cfimpresa;
	        return $array;

        }
    //put your code here
	
    public function getInfoTransazione($idtransazione,$idOrder,$amount){
        global $idCliente,$passCliente;
        global  $debugIscrzione;
        
        $db = new Database();
        
        if($debugIscrzione['pagamento']==true){
        $url = "https://test.monetaonline.it/monetaweb/payment/2/xml"; //url test
    //$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
        }else{
          $url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
        }
        $description = "Pagamento EXPO2015";
        $customField = "";

        $fields = array(
    //    'card' => urlencode($_POST['numeroCarta']),
    //    'expiryMonth' => urlencode($_POST['meseScadenzaCarta']),
    //    'expiryYear' => urlencode($_POST['annoScadenzaCarta']),
    //    'cardHolderName' => urlencode($_POST['intestatarioCarta']),

          	'merchantOrderId' => $idOrder,
            'operationType' => 'inquiry',
            'id' => $idCliente,
            'password' => $passCliente,
            'paymentId'=>$idtransazione
        );

		
        $fields_string = '';
    //url-ify the data for the POST
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        $fields_string=rtrim($fields_string, '&');
        
        //open connection
        $ch = curl_init();

    //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		
        //execute post
        $result = curl_exec($ch) or die($errlog = curl_error($ch));
        /*echo "Result: <br/>";
		print_r($result);
		exit;*/
        if (isset($errlog)){
        	//echo "Errore: ".$errlog;
  			
        	$queryst = "INSERT INTO `EXPO_T_Catalogo_Logs` (`data`,`azione`,`parametri`,`response`)
						VALUES ('".date('Y-m-d H:i:s')."','TRANMONETA','".$fields_string."','".$errlog."')";
        	$db->Query($queryst);
        }
        	
        $array = XML2Array::createArray($result);
        
        return $array;
    }
    
    public function getInfoTransazioneMyBank($idtransazione,$idOrder,$amount){
    	//echo "Get Trans MyBank";
    	//exit;
    	global $idClienteMyBank,$passCliente,$debugPagam;
    	global  $debugIscrzione;
    
    	$db = new Database();
    
    	if($debugIscrzione['pagamento']==true){
    		$url = "https://test.monetaonline.it/monetaweb/payment/2/xml"; //url test
    		//$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
    	}else{
    		$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
    	}
    	$description = "Pagamento EXPO2015";
    	$customField = "";
    
    	$fields = array(
    			//    'card' => urlencode($_POST['numeroCarta']),
    			//    'expiryMonth' => urlencode($_POST['meseScadenzaCarta']),
    			//    'expiryYear' => urlencode($_POST['annoScadenzaCarta']),
    			//    'cardHolderName' => urlencode($_POST['intestatarioCarta']),
    
    			'merchantOrderId' => $idOrder,
    			'operationType' => 'inquirymybank',
    			'id' => $idClienteMyBank,
    			'password' => $passCliente,
    			'paymentId'=>$idtransazione
    	);
    
    	$fields_string = '';
    	//url-ify the data for the POST
    	foreach ($fields as $key => $value) {
    		$fields_string .= $key . '=' . $value . '&';
    	}
    
    	$fields_string=rtrim($fields_string, '&');
    
    	//open connection
    	$ch = curl_init();
    
    	//set the url, number of POST vars, POST data
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_POST, count($fields));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    
    	//execute post
    	$result = curl_exec($ch) or die($errlog = curl_error($ch));
    	if ($debugPagam == true && $uid == 167){
    	//if ($debugPagam == true){
    		echo "Result: <br/>";
    		print_r($result);
    		exit;
    	}
    	if (isset($errlog)){
    		//echo "Errore: ".$errlog;
    			
    		$queryst = "INSERT INTO `EXPO_T_Catalogo_Logs` (`data`,`azione`,`parametri`,`response`)
						VALUES ('".date('Y-m-d H:i:s')."','TRANMYBANK','".$fields_string."','".$errlog."')";
    		$db->Query($queryst);
    	}
    	 
    	$array = XML2Array::createArray($result);
    	//print_r($array);
    	//exit;
    	return $array;
    }
    

    public function getInfoPagamento($cfimpresa, $fascia, $amount,$uid,$ordineProfessionista) {
        //echo "Info CRedito";
        //exit;
        
    	if ($ordineProfessionista > 0){
    		$id_VMenu = 507;
    	} else {
    		$id_VMenu = 310;
    	}
    	
    	global $idCliente,$passCliente,$debugIscrzione,$MAINSITE,$debugPagam;
        if($debugIscrzione['pagamento']==true){
        $url = "https://test.monetaonline.it/monetaweb/payment/2/xml"; //url test
    //$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
        }else{
          $url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione
        }

        $description = "Pagamento EXPO2015";
        $customField = "";


        $idOrder =  uniqid();
        $fields = array(
 
            'description' => 'Iscrizione Catalogo Fornitori Impresa : '.$cfimpresa .'- Ordine '.$ordineProfessionista,
            'amount' => $amount,
            'merchantOrderId' => $idOrder,
            'language' => 'ITA',
            'responseToMerchantUrl' => urlencode($MAINSITE.'index.phtml?Id_VMenu='.$id_VMenu.'&azione=PAY&idorder=' . $idOrder . '&cfimpresa=' . $cfimpresa  . '&ordineProfessionista=' . $ordineProfessionista),
            'recoveryUrl' => urlencode($MAINSITE.'index.phtml?Id_VMenu='.$id_VMenu.'&azione=PAY&idorder=' . $idOrder . '&cfimpresa=' . $cfimpresa  . '&ordineProfessionista=' . $ordineProfessionista),
            'operationType' => 'initialize',
            'currencyCode' => '978',
            'id' => $idCliente,
            'password' => $passCliente
        );
        
        $fields_string = '';
    //url-ify the data for the POST
        foreach ($fields as $key => $value) {
        	$fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');

    //open connection
        $ch = curl_init();

    //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

    //execute post
        $result = curl_exec($ch) or die(curl_error($ch));
        //echo "Debug".$uid;
        //exit;
        //Utenza 167: Ferla Alessandro
        if ($debugPagam == true && $uid == 167){
        	echo "Request: ".$fields_string."<br/>";
        	echo "Result: <br/>";
        	print_r($result);
        	exit;
        }
        
        $array = XML2Array::createArray($result);
 
        $array['response']['amount'] = $amount;
        $array['response']['orderid'] = $idOrder;
        $array['response']['description'] = 'Iscrizione Catalogo Fornitori Impresa : '.$cfimpresa;
        return $array;
    }
}
?>
