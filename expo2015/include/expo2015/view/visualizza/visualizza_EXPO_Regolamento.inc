<?php
require_once($PROGETTO . "/view/lib/updateFile.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");

$db = new DataBase();

//$IdImpresa = $auth->auth['IdImpresa'];
if ($IdImpresa > 0 && $auth->auth['IdImpresa']==""){
	$auth->auth['IdImpresa'] = $IdImpresa;
} elseif ($IdImpresa == "" && $auth->auth['IdImpresa'] > 0) {
	$IdImpresa = $auth->auth['IdImpresa'];
} elseif ($IdImpresa == "" && $auth->auth['IdImpresa'] == "") {
	//Lourdes No?
	//echo "Lista ".$IdImpresa." - ".$auth->auth['IdImpresa'];
	//exit;
	echo "<script type='text/javascript'>window.location.replace('index.phtml?Id_VMenu=307');</script>";
}

if (empty($IdImpresa) or $IdImpresa == "" or $IdImpresa == null){
	include_once("send_mail.inc");
	//cerca l'abbinamento Impresa user id e se c'� un'unica impresa ne recupera l'Id
	$qry = "SELECT EXPO_T_Imprese.Id FROM T_Utente INNER JOIN ((T_Anagrafica INNER JOIN EXPO_TJ_Imprese_Utenti ON T_Anagrafica.Id = EXPO_TJ_Imprese_Utenti.IdUtente) 
			INNER JOIN EXPO_T_Imprese ON EXPO_TJ_Imprese_Utenti.IdImpresa = EXPO_T_Imprese.Id) ON T_Utente.Id = T_Anagrafica.Id
			WHERE T_Utente.Id=$uid;";
	if ($db->NumRows($qry) == 1){
		$IdImpresa = $db->GetRow($qry, 'Id', null, "visualizza_EXPO_Regolamento.inc Ln. 25");
		
		$cmd = "Svil. Alert: visualizza_EXPO_Regolamento | IdImpresa recuperato: ".$IdImpresa;
		scriviLog($cmd,'Recupero IdImpresa','',true);
		//send_mail_html('alessandro.ferla@digicamere.it', 'noreply.catalogue@expo2015.org', 'Prod. Alert: visualizza_EXPO_Regolamento', $cmd.'\n');
	} else {
		$cmd = "Svil. Alert: visualizza_EXPO_Regolamento | IdImpresa non recuperato: fatto redirect!";
		scriviLog($cmd,'Recupero IdImpresa','',true);
		//send_mail_html('alessandro.ferla@digicamere.it', 'noreply.catalogue@expo2015.org', 'Prod. Alert: visualizza_EXPO_Regolamento', $cmd.'\n');
		redirect($MAINSITE."index.phtml?Id_VMenu=307");
	}
}
	

$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
$row = $db->GetRow($query,null, null, "visualizza_EXPO_Regolamento.inc Ln. 37");
$RagioneSociale=$row['RagioneSociale']; 
$codiceFiscaleAzienda=$row['CodiceFiscale'];

$query="SELECT EXPO_T_Imprese.Id, EXPO_T_Imprese.RagioneSociale, EXPO_T_Imprese.CodiceFiscale, T_Anagrafica.Nome, T_Anagrafica.Cognome, T_Anagrafica.Codice_Fiscale AS Codice_FiscaleLR
FROM T_Utente INNER JOIN ((T_Anagrafica INNER JOIN EXPO_TJ_Imprese_Utenti ON T_Anagrafica.Id = EXPO_TJ_Imprese_Utenti.IdUtente) INNER JOIN EXPO_T_Imprese ON EXPO_TJ_Imprese_Utenti.IdImpresa = EXPO_T_Imprese.Id) ON T_Utente.Id = T_Anagrafica.Id
WHERE T_Utente.Id_TipoUtente=96 AND EXPO_T_Imprese.Id=$IdImpresa";
//echo $query;

$rowLR = $db->GetRow($query);
$codiceFLR=$rowLR['Codice_FiscaleLR'];

$pathImpresa=selectRootDoc($RagioneSociale);

$pathFolderFirma = Config::ROOTFILES . $pathImpresa . "/".$IdImpresa."/filesFirmati/";
//echo $pathFolderFirma;
//Cerca nella cartella dei files Firmati se c'� il file firmato
//delete($pathFolderFirma.'RegExpo_83.pdf.p7m');
//exit;
if ($azione==''){
	if (file_exists($pathFolderFirma)){
		$nomeFileFirmato='';
		$r=array();
		searchFile($pathFolderFirma, 'p7m', $r);
		foreach($r as $f) {
			$nomeFileFirmato=$f;
			//echo "N:".$nomeFileFirmato, '<br />';
		}
	
		if ($nomeFileFirmato==''){
			searchFile($pathFolderFirma, 'pdf', $r);
			foreach($r as $f) {
				$nomeFileFirmato=$f;
				//echo "F:".$nomeFileFirmato, '<br />';
			}
		}
	} else {
		//echo "Errore: \"" . $pathFolderFirma . "\" non esiste!";
	} 
	//exit;
} else if ($azione=='UPD')  {
	//Aggiunta file firmato
	//echo "Entrato:". $_FILES['contratto']['name'];
	//exit;
	if ($_FILES['contratto']['name'] != '') {
		$rootDoc = selectRootDoc($RagioneSociale) . "/" . $IdImpresa . "/";
		$rootFile = Config::ROOTFILES . $rootDoc;
		
		//CARICO PDF
		$f = new UpdateFile();
		$nomeFile = $f->updateF($_FILES, $rootFile, "filesFirmati");
	
		$fileOriginale = $rootFile . "RegExpo.pdf";
		//echo "File Orig:".$fileOriginale;
		/*if (!copy('files/RegExpo_83.pdf', $fileOriginale))
			echo "Copia di $file non riuscita ...\n";*/
		
		//exit;
		$fileFirmato = $rootFile . "filesFirmati/" . $nomeFile;
		//VERIFICO PDF
		//echo "Root:".$fileFirmato;
		//exit;
		if (file_exists($fileOriginale)){
			if(serverOS() == 1){
				$cmd='java.exe -cp ' . $PATHINC . 'lib/java/DocSignChecker-1.2.2-EXECUTABLE.jar -DrootCerts.prop=' . $PATHINC . 'lib/java/cacerts DocChecker "' . $fileOriginale . '" "' . $fileFirmato . '"';
			} else {
				$cmd='java -cp ' . $PATHINC . 'lib/java/DocSignChecker-1.2.2-EXECUTABLE.jar -DrootCerts.prop=' . $PATHINC . 'lib/java/cacerts DocChecker "' . $fileOriginale . '" "' . $fileFirmato . '"';
		
			}
			exec($cmd, $output);
		
			$result = explode("@", end($output));
			$strVuota = "<?xml version=\"1.0\"?><root></root>";
			if ((strpos($result[1],$strVuota) !== FALSE)){
				$queryst = "INSERT INTO EXPO_T_Catalogo_Logs (`data`,`azione`,`parametri`,`response`)
								VALUES (NOW(),'Iscrizione: Ctr Firma Reg.','Impresa Id:".$idImpresa." - ".$cmd."','Result ICheck:".$result[0]." - PDF NON Firmato Cod. Fiscale LR: ". $codiceFLR."')";
			
				$db->Query($queryst);
				/*include_once("send_mail.inc");
				 send_mail_html('alessandro.ferla@digicamere.it', 'noreply.catalogue@expo2015.org', 'Prod. Alert: Ctrl Firma', $cmd.'\n');*/
				echo "<h2>" . _CERTIFICATO_ER_ . "</h2>";
			} else {
				$arrayFirmaPDF = XML2Array::createArray($result[1]);
				$subject=explode(',',$arrayFirmaPDF['root']['signature']['certificate']['subject']);
				$algName = $arrayFirmaPDF['root']['signature']['certificate']['sigAlgName'];
				
				for ($k=0;$k<=count($subject)-1;$k++){
					if (strpos($subject[$k],"SERIALNUMBER=") !== FALSE ){
						$cfFirma=str_replace("SERIALNUMBER=", "", $subject[$k]);
					} elseif (strlen($cfFirma) == 0 && strpos($subject[$k],"CN=") !== FALSE){
						//echo "entrato CN";
						$cfFirma=substr($subject[$k], 4, 16);
					}
				}
				
				if (strpos($cfFirma , $codiceFLR) == FALSE){
				//if ($result[0] != 0 or strpos($result[1], $codiceFLR) == FALSE) {
					/* Rinomina File perch� il controllo � andato male, ma vogliamo tenerlo
					per fare ulteriori verifiche */
					rename($fileFirmato,$fileFirmato.".err");
					
					$xmlr = str_replace("'", "\'", $result[1]);
					$xmlr = mysql_escape_string($xmlr);
					 
					$queryst = "INSERT INTO EXPO_T_Catalogo_Logs (`data`,`azione`,`parametri`,`response`)
								VALUES (NOW(),'Iscrizione: Ctr Firma Reg.','Impresa Id:".$idImpresa." - ".$cmd."','Result ICheck:".$result[0]." - XML Cert. PDF: ".$xmlr." Cod. Fiscale LR: ".$codiceFLR."')";
					
					$db->Query($queryst);
					include_once("send_mail.inc");
					send_mail_html('alessandro.ferla@digicamere.it; fabio.bertoletti@digicamere.it', 'noreply.catalogue@expo2015.org', 'Svil. Alert: visualizza_EXPO_Regolamento', $cmd.'\n');
					echo "<h2>" . _CERTIFICATO_ER_ . "</h2>";
				} else {
					if ($result[0] != 0 or strpos($algName,"SHA256") == FALSE){
						//Registra l'errore di integrit� o di Algoritmo
						//$slq_err="SELECT * FROM EXPO_T_Imprese WHERE Id=$idImpresa";
						//$Errori = $db->GetRow($slq_err, 'errors',null,'visualizza_EXPO_Regolamento Ln.124');
						$Errori = "";
						if ($result[0] != 0)
							$Errori = "ICK:".$result[0]."|";
						if (strpos($algName,"SHA256") == FALSE)
							$Errori .= "ALG:".$algName."|";
						$db->Query("UPDATE EXPO_T_Imprese SET errors ='$Errori' WHERE Id = $idImpresa",null,'visualizza_EXPO_Regolamento Ln.129');
					}
					$nomeFileFirmato=$fileFirmato;
					//echo "OK";
				}
			}
		}
		
		
	}
}
?>
<form  id="form_moduli" name="form_moduli" enctype="multipart/form-data">
<input type="hidden"  id="titoloBarraAppend" value="" />
<table style="width: 90%;padding-left: 10px;padding-right: 10px" >
    <?php if ($nomeFileFirmato==''){ ?>
    <tr>
        <td colspan="2">
       <?php echo '<div style="text-align:center;width:100%;font-size:1.1em;font-weight: bold;"><img src="/images/expo2015/alert.png" style="vertical-align:middle;width:32px"/> '._CERTIFICATO_MANCA_ . '</div>'; ?>
        </td>
    </tr>
    <?php } ?>
    <tr>
        <td colspan="2" class="boxBlu">
            <h3>Ragione sociale impresa :</h3>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="background-color: #fff;color: #c10a1e">
            <h3><?php echo $RagioneSociale; ?></h3>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="boxBlu">
            <h3>&nbsp;</h3>
        </td>
    </tr>

    <tr style="height: 50px">
        <td style="width: 50%" ><br><br>
            Scarica il contratto
            <a href="ajaxlib/requester/downloadFile.php?idImpresa=<?php echo $IdImpresa ?>" target="_blank" draggable="false"><img src="/images/expo2015/iconapdf.png" width="35px" draggable="false"></a>
        </td>
        <?php if ($nomeFileFirmato!=''){ ?>
        	<td><br><br>
        	
            <?php echo '<div style="text-align:center;width:100%;font-size:1.4em;font-weight: bold;"><img src="/images/expo2015/ok.png" style="vertical-align:middle;width:32px"/> '._REG_UPLOADED_ . '</div>'; ?>
        	</td>
        
        <?php } else   if ($utype != 94){  ?>
        	<td><br><br>
            Carica il contratto
            <input type="file" name="contratto" id="contratto" >
            <input type="hidden" name="impresa" id="impresa" value="<?php echo $codiceFiscaleAzienda; ?>" >
            <a class="buttonRed" draggable="false" href="javascript:invia(document.form_moduli,'index.phtml?Id_VMenu=504&amp;azione=UPD')" ><?php echo _ACCETTA_; ?></a>
        	</td>
        <?php } ?>
    </tr>
</table>
<br><br>
</form>