<?php
$campis="";
if ($tabelles!="")
	$tabelles.=",".$Esterno[$nome].$nome;
else
	$tabelles=$Esterno[$nome].$nome;

if ($DEBUG) echo "<!--WHEREFILTRI $wherefiltri -->\n";
$wheres="".$wherefiltri;
$leftjoin="";
$tdcampi=0;
if ($nome==$TAB_ANAGRAFICA)
{
	$leftjoin.=" LEFT JOIN T_Utente ON ".$Esterno[$nome].$nome.".Id=T_Utente.Id";
	if($Id_TipoUtente>0)
	{
		if ($wheres!="") $wheres.= " and ";
		$wheres.=$Esterno[$nome]."T_Utente.Id_TipoUtente=".$Id_TipoUtente;
	}
}
$orders="";
echo "<table id=\"tabella_elenchi\" summary=\"Elenco\">\n";
echo "<tr>\n";

reset($tabella);
$conta_tab=1;

if ($auth->auth["utype"]=="")  
	$utype2=99;
else
	$utype2=$auth->auth["utype"];

while (list ($key, $val) = each ($tabella))
{
	$temp=explode("|",$key);
	$key=$temp[0];
	$utenti_abilitati=$temp[1];
	$azione=$temp[2];
	$id_tipoutente2=$temp[3];
	$abilitato=strpos($utenti_abilitati,";".$utype2.";");
	$da_visualizzare=strpos($id_tipoutente2,";".$Id_TipoUtente.";");
	if (( $utenti_abilitati =="" || !($abilitato===false)) && ( $Id_TipoUtente =="" || $id_tipoutente2 =="" || !($da_visualizzare===false)))
	{

		$campo= explode("|",$val);

		if ($campo[$Parametri["VisTab"]]=="S" || $campo[$Parametri["VisTab"]]=="I" || $campo[$Parametri["VisTab"]]=="F" || $campo[$Parametri["VisTab"]]=="H")
		{
			$ordina1="";
			if ($campo[$Parametri["VisTab"]]=="I" || $campo[$Parametri["VisTab"]]=="H")
			{

				$chiave=$key;
			}


			if ($campo[$Parametri["TipoCampo"]]=="SELECT")
			{
				$tmptab=explode(";",$campo[$Parametri["Parametro4"]]);
				$nome2=$tmptab[0];
				$campo1=$tmptab[2];
				$campo2=$tmptab[1];
				$tmptab=explode(";",$campo[$Parametri["Parametro1"]]);

				if ($campo[$Parametri["VisTab"]]!="F")
				{

					$asjoin="";
					$postab=strpos($campis,$tmptab[0].".".$tmptab[2]);
					if ($postab===false)
					{
						if ($campis!="") $campis .= ",";
						$campis .= $tmptab[0].".".$tmptab[2];
						$leftjoin.=" LEFT JOIN ".$tmptab[0]." ON ".$Esterno[$nome].$nome.".".$key."=".$tmptab[0].".".$tmptab[1];
					}
					else
					{
						if ($campis!="") $campis .= ",";
						$leftjoin=" LEFT JOIN ".$tmptab[0]." AS ";
						$asjoin=$tmptab[0]."_".$conta_tab;
						$asjoin=str_replace(".","_",$asjoin);
						$leftjoin.=asjoin." ON ".$Esterno[$nome].$nome.".".$key."=".$asjoin.".".$tmptab[1];
						$conta_tab++;
						$campis .= $asjoin.".".$tmptab[2];

					}
					$tdcampi++;

					if ($ordina1!="") $ordina1.=", ";
					{
						if ($asjoin!="")
							$ordina1=$asjoin.".".$tmptab[2];
						else
							$ordina1.=$tmptab[0].".".$tmptab[2];
					}
				}


				$postab=strpos($tabelles,$nome2);
				if ($postab===false)
					$tabelles .= ",".$nome2;
				else
				{
					$nome2.="_".$conta_tab;
					$conta_tab++;
				}


				if ($$key!="")
				{
					if ($wheres!="") $wheres .= " and ";
					$wheres.=$nome2.".".$key."='".$$key."'";
					$wheres.=" and ".$nome2.".".$campo2."='".$Esterno[$nome].$nome.".".$campo1."'";
				}
				if ($DEBUG) echo "<!--SELECT $key: ".$wheres."-->\n";
				if ($DEBUG) echo "<!--SELECT ORDINA1: ".$ordina1."-->\n";
			}
			elseif ($campo[$Parametri["TipoCampo"]]=="SELECTJOIN")
			{

				$tmptab=explode(",",$campo[$Parametri["Parametro1"]]);
				$nome2=$tmptab[0];

				$postableft=strpos($leftjoin,$nome2);
				if ($postableft===false)
					$leftjoin.=" LEFT JOIN ".$nome2." ON ".$Esterno[$nome].$nome.".".$key."=".$nome2.".".$campo[$Parametri["Parametro5"]];
				else
				{
					$leftjoin.=" LEFT JOIN ".$nome2." AS ";
					$asjoin=$nome2."_".$conta_tab;
					$asjoin=str_replace(".","_",$asjoin);
					$leftjoin.=$asjoin." ON ".$Esterno[$nome].$nome.".".$key."=".$asjoin.".".$campo[$Parametri["Parametro5"]];					

                    $conta_tab++;
				}
				$tmptab=explode(",",$campo[$Parametri["Parametro4"]]);


				if ($campo[$Parametri["VisTab"]]!="F")
				{
					$ascampo=$tmptab[0]."_".$conta_tab;
					if (sizeof($tmptab)<2)
						$campotabella=$nome2.".".$tmptab[0];
					else
					{
						$campotabella="Concat(".$nome2.".".$tmptab[0];
						for($i=1;$i<sizeof($tmptab);$i++)
							$campotabella .=", \" \", ".$nome2.".".$tmptab[$i];
						$campotabella.=")";
					}

					if ($campis!="") $campis .= ",";
					$campis .= $campotabella." as ".$ascampo;

					$tdcampi++;

					if ($ordina1!="") $ordina1.=", ";
					$ordina1.=$ascampo;


					if ($$key!="")
					{
						if ($wheres!="") $wheres .= " and ";
						$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
					}
					if ($DEBUG) echo "<!--SELECTJOIN $key: ".$wheres."-->\n";
					if ($DEBUG) echo "<!--SELECTJOIN ORDINA1: ".$ordina1."-->\n";
				}

			}
			elseif ($campo[$Parametri["TipoCampo"]]=="SELECTDISTINCT")
			{
				if ($campo[$Parametri["VisTab"]]!="F")
				{
					if ($campis!="") $campis .= ",";
					$campis .= $Esterno[$nome].$nome.".".$key;
					$tdcampi++;
				}
				$ordina1=$key;

				if ($$key!="")
				{
					if ($wheres!="") $wheres .= " and ";
					$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
				}
				if ($DEBUG) echo "<!--SELECTDISTINCT $key: ".$wheres."-->\n";
				if ($DEBUG) echo "<!--SELECTDISTINCT ORDINA1: ".$ordina1."-->\n";
			}
			elseif (substr($campo[$Parametri["TipoCampo"]],0,6)=="SELECT")
			{
				$condiz=$campo[$Parametri["Parametro6"]];
				if ($condiz=="" || ($condiz!="" && (eval ("if (".$condiz.") return 1; else return 0;"))))
				{
					$tmptab=explode(";",$campo[$Parametri["Parametro1"]]);
					$nome2=$tmptab[0];
					$campo1=$tmptab[1];
					$campo2=$tmptab[2];
					$asjoin="";
					$postableft=strpos($leftjoin,$nome2);
					if ($postableft===false && $tmptab[0]!=$nome)
					{
						if ($campo[$Parametri["AltraTab"]]!="")
						{
							$altratab=explode(";",$campo[$Parametri["AltraTab"]]);
							$nomealtratab=$altratab[0];
							$campoaltratab1=$altratab[1];
							$campoaltratab3=$altratab[2];
							$campoaltratab2=$key;
							$leftjoin.=" LEFT JOIN ".$nome2." ON ".$nomealtratab.".".$key."=".$nome2.".".$campo1;
							$postab=strpos($tabelles,$nomealtratab);
							$postab2=strpos($leftjoin,$nomealtratab." ON");
							$postab3=strpos($leftjoin,$nomealtratab." AS");
							if ($postab===false && $postab2===false && $postab3===false)
							{
								$leftjoin.=" LEFT JOIN ".$nomealtratab." AS ".$nomealtratab."_".$conta_tab." ON ".$Esterno[$nome].$nome.".".$campoaltratab3."=".$nomealtratab."_".$conta_tab.".".$campoaltratab1;
								$asjoin=$nomealtratab."_".$conta_tab;
								if ($wheres!="") $wheres .= " and ";					
								$wheres.=$Esterno[$nome].$nome.".".$campoaltratab3."=".$nomealtratab."_".$conta_tab.".".$campoaltratab1." ";
							}
						

						}
						else
							$leftjoin.=" LEFT JOIN ".$nome2." ON ".$Esterno[$nome].$nome.".".$key."=".$nome2.".".$campo1;
					}
					else
					{
						if ($campo[$Parametri["AltraTab"]]!="")
						{
							$altratab=explode(";",$campo[$Parametri["AltraTab"]]);
							$nomealtratab=$altratab[0];
							$campoaltratab1=$altratab[1];
							$campoaltratab2=$key;
							$leftjoin.=" LEFT JOIN ".$nome2." AS ".$nome2."_".$conta_tab." ON ".$nomealtratab.".".$key."=".$nome2."_".$conta_tab.".".$campo1;
						}
						else
						{
							
							$leftjoin.=" LEFT JOIN ".$nome2." AS ";
							$asjoin=$nome2."_".$conta_tab;
							$asjoin=str_replace(".","_",$asjoin);
							$leftjoin.=$asjoin." ON ".$Esterno[$nome].$nome.".".$key."=".$asjoin.".".$campo1;

						}

						$nome2.= "_".$conta_tab;
						$conta_tab++;
					}
					if ($campo[$Parametri["Parametro5"]]!="") $leftjoin.=$campo[$Parametri["Parametro5"]];
					if ($asjoin!="")
						$nome2=$asjoin;
					if ($campo[$Parametri["VisTab"]]!="F") 
					{

						$tmptab=explode(",",$campo2);
//						$ascampo=$tmptab[0]."_".$nome2;
						$ascampo=$tmptab[0]."_".$conta_tab;
						if (sizeof($tmptab)<2)
							$campotabella=$nome2.".".$tmptab[0];
						else
						{
							$campotabella="Concat(".$nome2.".".$tmptab[0];
							for($i=1;$i<sizeof($tmptab);$i++)
								$campotabella .=", \" \", ".$nome2.".".$tmptab[$i];
							$campotabella.=")";
						}

						if ($campis!="") $campis .= ",";
						$campis .= $campotabella." as ".$ascampo;

						$tdcampi++;
						if ($ordina1!="") $ordina1.=", ";
						$ordina1.=$ascampo;
					}

					if ($$key!="")
					{
						if ($wheres!="") $wheres .= " and ";

// Eccezione per amministrazione avanzata
						if ($nome=="SVIL_T_Articoli" && $key=="Id_TipoArticolo1")
						{
							$wheres.= " Id_Menu like '".$$key."%'";
						}
// Fine Eccezione per amministrazione avanzata
						elseif ($campo[$Parametri["AltraTab"]]!="")
							$wheres.=$nomealtratab.".".$key."='".$$key."'";
						else
							$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
					}
					if ($DEBUG) echo "<!--SELECT... $key: ".$wheres."-->\n";
					if ($DEBUG) echo "<!--SELECT... ORDINA1: ".$ordina1."-->\n";
				}
			}
			elseif ($campo[$Parametri["AltraTab"]]!="")
			{
				
				$tmptab=explode(";",$campo[$Parametri["AltraTab"]]);
				$nome2=$tmptab[0];
				$campo1=$tmptab[1];
				$campo2=$key;


				$my_campis=$nome2;
				$my_ordina1=$nome2;

				$postab=strpos($tabelles,$nome2);
				$postab2=strpos($leftjoin,$nome2." ON");
				$postab3=strpos($leftjoin,$nome2." AS");

				if ($postab===false && $postab2===false && $postab3===false)
				{
					$leftjoin.=" LEFT JOIN ".$nome2." AS ".$nome2."_".$conta_tab." ON ".$Esterno[$nome].$nome.".".$tmptab[2]."=".$nome2."_".$conta_tab.".".$campo1;
					$my_campis.="_".$conta_tab;
					$my_ordina1.="_".$conta_tab;
				}
				$my_campis.=".".$campo2;
				$my_ordina1.=".".$campo2;
				if($campo[$Parametri["VisTab"]]!="F")
				{
					if ($campis!="") $campis .= ",";
					$campis .= $my_campis;
					if ($campo[$Parametri["TipoCampo"]]=="DATAITA")
						if ($campo[$Parametri["VisTab"]]!="F")
							$ind_date.="|".$tdcampi."|";
					$tdcampi++;
					$ordina1=$my_ordina1;
				}
				
				if ($$key!="")
				{
					if ($wheres!="") $wheres .= " and ";
					$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
				}
				
				$conta_tab++;				

				if ($DEBUG) echo "<!--ALTRATAB $key: ".$wheres."-->\n";
				if ($DEBUG) echo "<!--ALTRATAB ORDINA1: ".$ordina1."-->\n";
			}			
			
			elseif ($campo[$Parametri["TipoCampo"]]=="DATATEXT")
			{
				if ($campo[$Parametri["VisTab"]]!="F")
				{
					if ($campis!="") $campis .= ",";
					$data="concat(substring(".$Esterno[$nome].$nome.".".$key.",7),\"/\",substring(".$Esterno[$nome].$nome.".".$key.",5,2),\"/\",substring(".$Esterno[$nome].$nome.".".$key.",1,4))";
					$campis .= $data;
					$tdcampi++;
				}

				if ($$key!="")
				{
					if ($wheres!="") $wheres .= " and ";
					$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
				}
				if ($DEBUG) echo "<!--ELSE $key: ".$wheres."-->\n";
			}
			elseif ($campo[$Parametri["TipoCampo"]]=="DATAITA")
			{
				if ($campo[$Parametri["VisTab"]]!="F")
				{
					$ind_date.="|".$tdcampi."|";
					if ($campis!="") $campis .= ",";
					$campis .= $Esterno[$nome].$nome.".".$key;
					$ordina1=$key;
					$tdcampi++;
				}

				if ($DEBUG) echo "<!--ELSE $key: ".$wheres."-->\n";
			}
			else
			{
				if ($campo[$Parametri["TipoCampo"]]=="MAIL")
					$campo_mail2=$tdcampi;
					
				if ($campo[$Parametri["TipoCampo"]]=="FILE")
				{
					$link_scarico=$campo[$Parametri["Parametro4"]];
					$Parametro7=$campo[$Parametri["Parametro7"]];
					$campo_scarico=$key;
				}
				if ($campo[$Parametri["VisTab"]]!="F")
				{
					if ($campis!="") $campis .= ",";
					$campis .= $Esterno[$nome].$nome.".".$key;
					$tdcampi++;
				}
				$ordina1=$key;

				if ($$key!="")
				{
					if ($wheres!="") $wheres .= " and ";
					$wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
				}
				
				if ($DEBUG) echo "<!--ELSE $key: ".$wheres."-->\n";
				if ($DEBUG) echo "<!--ELSE ORDINA1: ".$ordina1."-->\n";
			}

			
		}
                
		$condiz=$campo[$Parametri["Parametro6"]];
		if ($condiz=="" || ($condiz!="" && (eval ("if (".$condiz.") return 1; else return 0;"))))
		{
			if ($campo[$Parametri["VisTab"]]=="S" || $campo[$Parametri["VisTab"]]=="I" )
			{
				$col = "c".$conta_tab."";
				echo "<th scope=\"col\" class=\"tdHeader\">\n";
				echo "<a  href=\"/index.phtml?tmpl=".$tmpl."&amp;pagina=elenchi&amp;nome=".$nome;
				if ($ordina==$ordina1)
					echo "&amp;ordina=".$ordina1."%20DESC";
				else
					echo "&amp;ordina=".$ordina1;
				if ($Id_TipoUtente!="")
					echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
				echo "\">".$campo[$Parametri["Label"]]."</a>\n";
				echo "</th>\n";
			}

		}
	}
	$conta_tab++;
}

if ($auth->auth["utype"]=="")
	$utype=99;
else
	$utype=$auth->auth["utype"];

if(isset($Azioni[$nome][$utype]) && strlen($Azioni[$nome][$utype])>1)
	$AzArray=explode("|",$Azioni[$nome][$utype]);
else
	$AzArray=explode("|",  $Azioni[$nome]);

if ($AzArray[1]!="")
	echo "<th scope=\"col\" class=\"tdHeader\">&nbsp;</th>\n";

echo "</tr>\n";

$db_tab = new DB_CedCamCMS;
$db_gen = new DB_CedCamCMS;

//$SQLconta="select count(*) from ".$tabelles.$tabelle_filtri;
$SQLconta="select distinct ";
if(isset($Chiave[$nome]))
	$SQLconta.=$nome.".".$Chiave[$nome];
else
	$SQLconta.=$campis;


$SQLconta.=" from ".$tabelles.$tabelle_filtri;
if ($leftjoin!="") $SQLconta.= $leftjoin;
if ($wheres!="") $SQLconta.= " where ".$wheres;

if ($iniz!="" and $campoiniz!="")
{
	if ($wheres!="")
		$SQLconta.= " and ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
	else
		$SQLconta.= " where ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
}
	
if ($wheres!="" && $nome==$TAB_ANAGRAFICA)
	$SQLconta.= " and ".$Esterno[$nome].$nome.".Stato_Record!='E'";
else if ($wheres=="" && $nome==$TAB_ANAGRAFICA)
	$SQLconta.= " where ".$Esterno[$nome].$nome.".Stato_Record!='E'";

/*
 * echo per visualizzare SQL delle Query
 */
//echo $SQLconta;
if ($db_tab->query($SQLconta))
{
	$db_tab->next_record();
	$estratti=$db_tab->num_rows();
}
else
	if ($DEBUG)  echo "Errore SQLnew: ".$SQLconta."\n";
	
$SQLnew="select distinct ".$campis. " from ".$tabelles.$tabelle_filtri;
if ($leftjoin!="") $SQLnew.= $leftjoin;
if ($wheres!="") $SQLnew.= " where ".$wheres;

if ($iniz!="" and $campoiniz!="")
{
	if ($wheres!="")
		$SQLnew.= " and ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
	else
		$SQLnew.= " where ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
}

if ($wheres!="" && $nome==$TAB_ANAGRAFICA)
	$SQLnew.= " and ".$Esterno[$nome].$nome.".Stato_Record!='E'";
else if ($wheres=="" && $nome==$TAB_ANAGRAFICA)
	$SQLnew.= " where ".$Esterno[$nome].$nome.".Stato_Record!='E'";
if ($ordina=="" && $Ordina[$nome]!="")
{
	$SQLnew.= " order by ".$Ordina[$nome];
	$ordina1=$Ordina[$nome];
}
else
	if ($ordina!="")
		$SQLnew.= " order by ".$ordina;
	elseif ($ordina1!="")
		$SQLnew.= " order by ".$ordina1;

if ($DEBUG) echo "<!-- SQLnew: $SQLnew -->\n";
//echo "<!-- SQLconta: $SQLconta -->\n";

//echo "<!-- SQLnew: $SQLnew -->\n";


/*
if ($p==0)
{
	$p=1;
	$lim_inf=0;

	if ($db_tab->query($SQLnew))
	{
		$estratti=$db_tab->num_rows();
		$sess->register(estratti);
		$page_tot=ceil($estratti/$RESFORPAGE);
		$sess->register(page_tot);
		
	}
	else
		if ($DEBUG)  echo "Errore SQLnew: ".$SQLnew."\n";
}
else
{

	$lim_inf=$RESFORPAGE*($p-1);
}

*/
$page_tot=ceil($estratti/$RESFORPAGE);
if($p>$page_tot)
{
	$p=0;
	$page_elenchi=$p;
	$sess->register(page_elenchi);		
	

}
if($p==0)
{
	$p=1;
	$lim_inf=0;
}
else
	$lim_inf=$RESFORPAGE*($p-1);

$SQLnew.=" limit ".$lim_inf.",".$RESFORPAGE;
if ($DEBUG) echo "<!-- limite: $SQLnew -->\n";

if ($db_tab->query($SQLnew))
{
	$y=0;
	while($db_tab->next_record())
	{
		$bgc = ( $y % 2 ? 'tdDark' : 'tdLight' );
		//if ($Id_TipoUtente!="" and $nome==$TAB_ANAGRAFICA)
		if ($nome==$TAB_ANAGRAFICA)
		{
			$db_stato = new DB_CedCamCMS;
			$SQL="select Stato_Record,Id_TipoUtente from ".$Esterno[$nome].$nome." inner join T_Utente on ".$Esterno[$nome].$nome.".Id=T_Utente.Id where ".$Esterno[$nome].$nome.".".$Chiave[$nome]."=".$db_tab->f(0);
			if ($DEBUG) echo "<!-- SQL Stato_Record: ".$SQL." -->";
			if ($db_stato->query($SQL))
			{
				$db_stato->next_record();
				$tmp_stato_ut=$db_stato->f('Stato_Record');
				$Id_TipoUtente=$db_stato->f('Id_TipoUtente');
			}
			else
				if ($DEBUG) echo "Errore db_stato: ".$SQL."\n";
		}

		if (($Id_TipoUtente!="" && $tmp_stato_ut!="E")||($Id_TipoUtente==""))
		{
			echo "<tr>\n";
			//echo "<td width=\"4\" valign=\"top\"><img src=\"/images/shim.gif\" width=\"4\" height=\"10\"></td>\n";

			//$ARRAYcampis=explode(",",$campis);
			for ($i=0;$i<$tdcampi;$i++)
			{
				if ($i!=0)
				{
					$align="left";

					if($i==0) echo "<td scope=\"row\" class=\"".$bgc."\" align=\"".$align."\">\n";
					else echo "<td class=\"".$bgc."\" align=\"".$align."\">\n";
					
					$trova_indate=strpos($ind_date,"|".$i."|");
					if ($trova_indate===false)
					{
						if ($db_tab->f($i)!="0000-00-00 00:00:00")
						{
							if($campo_mail2==$i)
							{
								echo "<a href=\"mailto:".$db_tab->f($i)."\">";
								echo $db_tab->f($i);
								echo "</a>";
							}
							else
								echo $db_tab->f($i);
						}
						else
							echo "&nbsp;";
					}
					else
					{	
						$aaaa=substr($db_tab->f($i),0,4);
						$mm=substr($db_tab->f($i),5,2);
						$gg=substr($db_tab->f($i),8,2);
						if (strlen($db_tab->f($i))>10) $ora=substr($db_tab->f($i),10);
						$datatext=$gg."/".$mm."/".$aaaa.$ora;
						echo $datatext;
					}
						

					echo "</td>\n";
				}
				else if (!($Modifica[$nome][$utype]=="H" || $Modifica[$nome]=="H"))
				{

					//if (is_numeric($db_tab->f($i))) $align="right"; else $align="left";
					$align="left";
					if($i==0) echo "<td scope=\"row\" class=\"".$bgc." ".$align."\">\n";
					else echo "<td class=\"".$bgc." ".$align."\">\n";
					if ($Modifica[$nome][$utype]!="N" && $Modifica[$nome]!="N")
					{
						switch ($nome){
							case "T_Prodotti":
								echo '<a href="/index.phtml?tmpl=2&pagina=form&nome='.$nome.'&azione=UPD&tab=0&Id='.$db_tab->f(0).'&nh=1"';
								break;
							/*case "SIEXPO_T_Var_X_Prod":
								echo '<a href="/index.phtml?tmpl=2&pagina=form&nome='.$nome.'&azione=UPD&tab=4&nh=0&Id_Prod='.$Id_Prod;
								break;*/
							case "SIEXPO_TJ_Crit_X_Prod":
								echo '<a href="/index.phtml?tmpl=2&pagina=form&nome='.$nome.'&azione=UPD&tab=6&nh=0&Id_Prod='.$Id_Prod;
								break;
							case "SIEXPO_T_Refer_X_Prod":
								echo '<a href="/index.phtml?tmpl=2&pagina=form&nome='.$nome.'&azione=UPD&tab=5&nh=0&Id_Prod='.$Id_Prod;
								break;
							default:
								echo "<a href=\"/index.phtml?tmpl=".$tmpl."&amp;pagina=form&amp;nome=".$nome."&amp;azione=UPD";
								if ($Id_TipoUtente!="")
									echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
								break;
						}
						
						echo "&amp;".$chiave."=".$db_tab->f($i)."\">".$db_tab->f($i)."</a>";
					}
					else
						echo $db_tab->f($i);
					echo "</td>\n";

				}

			}

			if ($AzArray[1]!="")
			{
				echo "<td class=\"".$bgc."\">";
				reset($AzArray);
				next($AzArray);
				if($nome=="CONCORSI_T_Iscrizioni_Concorsi")
				{
					$sql="select Id_Stato_Iscrizione, Id_Utente, Id_Concorso from CONCORSI_T_Iscrizioni_Concorsi where Id_Iscrizione=".$db_tab->f(0);
					$db_gen->query($sql);
					$db_gen->next_record();
					$Id_Concorso=$db_gen->f("Id_Concorso");
					$Id_Utente=$db_gen->f("Id_Utente");					
					$_myStatoIscriz=$db_gen->f("Id_Stato_Iscrizione");
				}
				else
					$_myStatoIscriz=99;

				while (list ($key, $val) = each ($AzArray))
				{
					$my_elimina=1;
					$my_modifica=1;
					if($nome=="T_Multimedia" && $utype=="3")
					{
						$sql="select Flag from T_Multimedia where Id_Multimedia=".$db_tab->f(0);
						$db_gen->query($sql);
						$db_gen->next_record();
						if($db_gen->f(0)<3)
						{
							$my_elimina=0;
							$my_modifica=0;
						}
					}
					switch ($val)
					{
					
					case "Dettagli":
					case "Visualizza":
					case "Modifica":
					case "Vedi risposta":
					case "See answer":
						$my_visualizza=1;
						if($_myStatoIscriz==$NUOVAISCR)
							$my_visualizza=0;
						if($my_visualizza)
						{
							echo "<a href=\"/index.phtml?pagina=form&amp;nome=".$nome."&amp;explode=".$explode."&amp;azione=UPD";
							if ($Id_TipoUtente!="")
								echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
							echo "&amp;".$chiave."=".$db_tab->f(0)."\" class=\"buttonRed\">".$val."</a><br />";
						}
						
						break;
					case "Scheda":

							echo "<a href=\"/index.phtml?pagina=scheda&amp;".$chiave."=".$db_tab->f(0)."\">".$val."</a><br />";
						break;
					case "Elimina":
					case "Delete":
						//echo "elimina";
						$my_elimina=1;
						
						if($my_elimina)
						{
							echo "<a href=\"/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0);
							if ($Id_TipoUtente!="")
								echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
							if ($Lang == $LinguaPrincipale)
								echo "&amp;op_esegui=".$val."\" onclick=\"if (!confirm('Confermi l\'operazione?')) return false\">".$val."</a><br />";
							else 
								echo "&amp;op_esegui=".$val."\" onclick=\"if (!confirm('Validate the operation?')) return false\">".$val."</a><br />";
						}
						break;
					case "Attiva":
					case "Enable":		
						$DescOp = "Attiva";
						$DescOpDis = "Disabilita";
						if ($Lang != $LinguaPrincipale){ 
							$DescOp = "Enable";
							$DescOpDis = "Disable";
						}
						echo "<a href=\"/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0);
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;op_esegui=";
						if ($tmp_stato_ut=="D")
							
							echo "Attiva\">".$DescOp."</a><br />";
						elseif ($tmp_stato_ut=="A")
							echo "Disattiva\">".$DescOpDis."</a><br />";
						
						break;
					case "Anteprima":
						echo "<a href=\"javascript:finestra('/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0);
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;op_esegui=".$val."')\">".$val."</a><br />";
						break;
					case "Scarica":
					case "Download":
						if($Parametro7=="P")
							echo "<a href=\"/download_private.php?".$chiave."=".$db_tab->f(0)."&nome=".$nome."&Id_VMenu=".$Id_VMenu."\">".$val."</a><br />";
						else
							echo "<a href=\"/".$link_scarico."/".$db_tab->f($campo_scarico)."\">".$val."</a><br />";
						break;
					case "Blocca":
							$sql="select Id_Stato,Id_Utente_Blocco from ".$nome." where ".$chiave."=".$db_tab->f(0);
							$db_gen->query($sql);
							$db_gen->next_record();
							if($db_gen->f("Id_Stato")==1)
								echo "<a href=\"/download_private.php?".$chiave."=".$db_tab->f(0)."&nome=".$nome."&Id_VMenu=".$Id_VMenu."&blocca=S\">Blocca</a><br />";
							else
							if($db_gen->f("Id_Utente_Blocco")==$uid)
								echo "<a href=\"/download_private.php?".$chiave."=".$db_tab->f(0)."&nome=".$nome."&Id_VMenu=".$Id_VMenu."&blocca=N\">Sblocca</a><br />";
						break;
					case "Iscrivi":
						//echo "<a href=\"/index.phtml?pagina=CONVEGNO_iscrizione";
						echo "<a href=\"/index.phtml?Id_VMenu=82";
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;".$chiave."=".$db_tab->f(0)."\">".$val."</a><br />";
						break;
					case "VisIscr":
						//echo "<a href=\"/index.phtml?pagina=CONVEGNO_form_convegno";
						echo "<a href=\"/index.phtml?Id_VMenu=88";
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;".$chiave."=".$db_tab->f(0)."\">Dettagli</a><br />";
						break;
					case "iscrizioni":
						//echo "<a href=\"/index.phtml?tmpl=2&pagina=elenchi&nome=CONVEGNI_T_Iscrizione_Convegni";
						echo "<a href=\"/index.phtml?Id_VMenu=89";
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;".$chiave."=".$db_tab->f(0)."\" >Iscrizioni</a><br />";
					break;

					case "Invio NewsLetter":
						//echo "<a href=\"/index.phtml?tmpl=2&pagina=invio_newsletter";
						echo "<a href=\"/index.phtml?Id_VMenu=29";
						if ($Id_TipoUtente!="")
							echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
						echo "&amp;".$chiave."=".$db_tab->f(0)."\">".$val."</a><br />";
						break;
					case "Pubblica_Fin":	
						$db_stato = new DB_CedCamCMS;
						$SQL="select Check_Validazione from FINANZIAMENTI_T_Finanziamenti where Id=".$db_tab->f(0);
						if ($db_stato->query($SQL))
						{
							$db_stato->next_record();
							$valido=$db_stato->f("Check_Validazione");
						}
						else
							if ($DEBUG)  echo "Errore SQL: ".$SQL1."\n";									

						echo "<a href=\"/admin/azioni.phtml?nome=".$nome."&explode=".$explode."&ordina=".$ordina."&Id=".$db_tab->f(0)."&Id_VMenu=".$Id_VMenu;
							echo "&op_esegui=";
							if ($valido==2 || $valido==0)
								echo "Pubblica_Fin\">Pubblica</a><br>";
							elseif ($valido==1)
								echo "Offline_Fin\">Metti in Offline</a><br>";
						break;

					case "Presenza":	
						$db_stato = new DB_CedCamCMS;
						$SQL="select Check_Presenza from CONVEGNI_T_Iscrizione_Convegni where Id_Iscrizione=".$db_tab->f(0);
						if ($db_stato->query($SQL))
						{
							$db_stato->next_record();
							$valido=$db_stato->f("Check_Presenza");
						}
						else
							if ($DEBUG)  echo "Errore SQL: ".$SQL1."\n";									
						echo "<a href=\"/admin/azioni.phtml?nome=".$nome."&explode=".$explode."&ordina=".$ordina."&Id_Iscrizione=".$db_tab->f(0)."&Id_VMenu=".$Id_VMenu;
							echo "&op_esegui=";
							if ($valido==0)
								echo "Presenza_In\">Segna Presenza</a><br>";
							elseif ($valido==1)
								echo "Presenza_Out\">Togli Presenza</a><br>";
						break;
					case "EliCat":
						$DescOp = "Elimina";
						if ($Lang != $LinguaPrincipale) $DescOp = "Delete"; 
							
						echo '<a href="/index.phtml?Id_VMenu=254&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
						
						break;
					case "EliMat":
						$DescOp = "Elimina";
						if ($Lang != $LinguaPrincipale) $DescOp = "Delete";
						
						echo '<a href="/index.phtml?Id_VMenu=267&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
						
						break;
					/*case "EliVar":
							echo '<a href="/index.phtml?Id_VMenu=270&id='.$db_tab->f(0).'">Elimina</a>';
							break;*/
					case "EliRefer":
							$DescOp = "Elimina";
							if ($Lang != $LinguaPrincipale) $DescOp = "Delete";
							
							echo '<a href="/index.phtml?Id_VMenu=277&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
							break;
					case "EliCrit":
							$DescOp = "Elimina";
							if ($Lang != $LinguaPrincipale) $DescOp = "Delete";
						
							echo '<a href="/index.phtml?Id_VMenu=273&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
							break;
					case "Pub2":
						$DescOp = "Pubblica";
						if ($Lang != $LinguaPrincipale) $DescOp = "Public";
						
						if ($db_tab->f(6)!="On line")
						echo '<a href="/index.phtml?Id_VMenu=240&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
						break;
					case "Antep1":
						$DescOp = "Anteprima";
						if ($Lang != $LinguaPrincipale) $DescOp = "Preview";
						$onck="_gaq.push(['_trackEvent', 'Scheda Prodotto', 'Click Link']);";
						$href="JavaScript:newPopup('/ShowProd.phtml?Id_Prod=".$db_tab->f(0)."&Tab=1',1100,900);";
						$htmlref='<a onclick='.$onck.' href="'.$href.'">'.$DescOp.'</a>';
						echo $htmlref;
						//echo '<a href="/index.phtml?Id_VMenu=240&id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
							
						break;
					case "VisCritS":
						$DescOp = "Visualizza";
						if ($Lang != $LinguaPrincipale) $DescOp = "Show";
						//http://siexpo2015.sv.local/index.phtml?tmpl=2&pagina=form&nome=SIEXPO_TJ_Crit_X_Prod&azione=UPD&tab=7&nh=0&Id_Prod=27&Id=7
						echo '<a href="/index.phtml?tmpl=2&pagina=form&nome=SIEXPO_TJ_Crit_X_Prod&azione=UPD&tab=7&nh=0&Id_Prod='.$Id_Prod.'&id='.$db_tab->f(0).'">'.$DescOp.'</a></br>';
						break;
					case "InviaMailAttiv":
						$DescOp = "Invia Mail attivazione";
						echo '<a href="/index.phtml?Id_VMenu=505&Id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
						break;
					case "AnagExt":
						$DescOp = "Anagrafica Estesa";
						if ($Lang != $LinguaPrincipale) $DescOp = "Extended Master";
						echo '<a href="/index.phtml?Id_VMenu=241&Id='.$db_tab->f(0).'">'.$DescOp.'</a><br>';
						break;
					default:
						$func="";
						include_once($PROGETTO."/azioni/funzioni.inc");
						if (function_exists("az_".$val."_".$nome))
							$func="az_".$val."_".$nome;
						elseif (function_exists("az_".$val))
							$func="az_".$val;
						if ($func!="")
							echo $func($db_tab->f(0));
						else
						{
							echo "<a href=\"javascript:finestra('/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0);
							if ($Id_TipoUtente!="")
								echo "&amp;Id_TipoUtente=".$Id_TipoUtente;
							echo "&amp;op=".$val."')\" >".$val."</a><br />";
							break;
						}
					}
				}
				echo "</td>";
			}
			echo "</tr>\n";
			$y++;
		}
	}
}
else
	if ($DEBUG)  echo "Errore SQLnew: ".$SQLnew."\n";

echo "</table>\n";

include $PROGETTO."/paginazione.inc";

if ($Ar_Export[$nome][$utype2]!="")
{
	$export=explode("|", $Ar_Export[$nome][$utype2]);
	if ($export[0]=="" || $export[0]==$Id_TipoUtente)
	{
		$ele1=explode(";", $export[1]);
		$ele2=explode(";", $export[2]);
		for($i=0;$i<sizeof($ele1);$i++)
		{
			if($pagepar_post=="")
				$pagepar_post=base64_decode($par_elenchi_post);
			$trans_arr=explode("&",$pagepar_post);

			$lungh=sizeof($trans_arr);
			for($y=0;$y<$lungh;$y++)
			{
				if(substr($trans_arr[$y],0,6)=="array_")
				{
					$var_valori_arr=explode("=",$trans_arr[$y]);
					$var_arr=substr($var_valori_arr[0],6);
					$pagepar_post.="&".$var_arr."=".$var_valori_arr[1];
				}
			}

			if($pagepar_get=="")
				$pagepar_get=base64_decode($par_elenchi_get);
			$trans_arr=explode("&",$pagepar_get);
			$lungh=sizeof($trans_arr);
			for($y=0;$y<$lungh;$y++)
			{
				if(substr($trans_arr[$y],0,6)=="array_")
				{
					$var_valori_arr=explode("=",$trans_arr[$y]);
					$var_arr=substr($var_valori_arr[0],6);
					$pagepar_get.="&".$var_arr."=".$var_valori_arr[1];
				}
			}
				
			echo "<br><a href=\"".$ele1[$i];
			if (strpos($ele1[$i],"?")===false)
				echo "?";
			else
				echo"&";
			echo $pagepar_post.$pagepar_get;
			echo"\">".$ele2[$i]."</a>";
			//echo "&nbsp;&nbsp;&nbsp;";
		}
	}
}

?>