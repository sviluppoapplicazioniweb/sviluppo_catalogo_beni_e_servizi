<?php
$db = new DataBase();
?>

<div id="log"></div>
<div>
    <input draggable="false" type="button" onclick="javascript:ordina();return false;" value="Salva" />
    <input draggable="false" type="button" onclick="javascript:popup_categorie('azione=NEW');return false;" value="Nuova Categoria" />
</div>
<br>
<div class="dd" id="nestable3">
    <ol class="dd-list">
        <?php
        $livello1 = "SELECT Id,Descrizione,Id_Categoria FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = 2 ORDER BY Id_Categoria";
        foreach ($db->GetRows($livello1) AS $l1) {
            $livello1Id = $l1['Id_Categoria'];
            ?>
            <li class="dd-itemRoot"  data-id="<?php echo $l1['Id']; ?>" >
                <div class="dd-handleRoot" >
                    <?php echo $l1['Descrizione']; ?>
                    <a href="javascript:popup_categorie('azione=UPD&Id=<?php echo $l1['Id']; ?>&Id_Categoria=<?php echo $livello1Id; ?>')"><img src="/images/expo2015/gray-write-new-icon.png"/></a>
                    <a href="javascript:popup_categorie('azione=NEW&Id_Categoria=<?php echo $livello1Id; ?>')"><img src="/images/expo2015/gray-add-to-list-icon.png"/></a>
                    <a href="javascript:popup_cancella('azione=DEL&Id=<?php echo $l1['Id']; ?>&Id_Categoria=<?php echo $livello1Id; ?>')"><img src="/images/expo2015/gray-trash-icon.png"/></a>
                </div>
                <?php
                $livello2 = "SELECT Id,Descrizione,Id_Categoria FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = 5 AND SUBSTR(Id_Categoria,1,2) = '$livello1Id' ORDER BY Id_Categoria";
                if ($db->NumRows($livello2) > 0) {
                    ?>
                    <ol class="dd-list">
                        <?php
                        foreach ($db->GetRows($livello2) AS $l2) {
                            $livello2Id = $l2['Id_Categoria'];
                            ?>
                            <li class="dd-item dd3-item"  data-id="<?php echo $l2['Id']; ?>">
                                <div class="dd-handle dd3-handle">Drag</div>
                                <div class="dd3-content">
                                    <?php echo $l2['Descrizione']; ?>
                                    <a href="javascript:popup_categorie('azione=UPD&Id=<?php echo $l2['Id']; ?>&Id_Categoria=<?php echo $livello2Id; ?>')"><img src="/images/expo2015/gray-write-new-icon.png"/></a>
                                    <a href="javascript:popup_categorie('azione=NEW&Id_Categoria=<?php echo $livello2Id; ?>&abilitato=1')"><img src="/images/expo2015/gray-add-to-list-icon.png"/></a>
                                    <a href="javascript:popup_cancella('azione=DEL&Id=<?php echo $l2['Id']; ?>&Id_Categoria=<?php echo $livello2Id; ?>')"><img src="/images/expo2015/gray-trash-icon.png"/></a>
                                </div>
                                <?php
                                $livello3 = "SELECT Id,Descrizione,Id_Categoria FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = 8 AND SUBSTR(Id_Categoria,1,5) = '$livello2Id' ORDER BY Id_Categoria";
                                if ($db->NumRows($livello3) > 0) {
                                    ?>
                                    <ol class="dd-list">
                                        <?php
                                        foreach ($db->GetRows($livello3) AS $l3) {
                                            $livello3Id = $l3['Id_Categoria'];
                                            ?>
                                            <li class="dd-item dd3-item"  data-id="<?php echo $l3['Id']; ?>">
                                                <div class="dd-handle dd3-handle">Drag</div>
                                                <div class="dd3-content">
                                                    <?php echo $l3['Descrizione']; ?>
                                                    <a href="javascript:popup_categorie('azione=UPD&Id=<?php echo $l3['Id']; ?>&Id_Categoria=<?php echo $livello3Id; ?>&abilitato=1')"><img src="/images/expo2015/gray-write-new-icon.png"/></a>
                                                    <a href="javascript:popup_categorie('azione=NEW&Id_Categoria=<?php echo $livello3Id; ?>&descrizioneALL=0')"><img src="/images/expo2015/gray-add-to-list-icon.png"/></a>
                                                    <a href="javascript:popup_cancella('azione=DEL&Id=<?php echo $l3['Id']; ?>&Id_Categoria=<?php echo $livello3Id; ?>')"><img src="/images/expo2015/gray-trash-icon.png"/></a>
                                                </div>
                                                <?php
                                                $livello4 = "SELECT Id,Descrizione,Id_Categoria FROM EXPO_Tlk_Categorie WHERE LENGTH(Id_Categoria) = 11 AND SUBSTR(Id_Categoria,1,8) = '$livello3Id' ORDER BY Id_Categoria";
                                                if ($db->NumRows($livello4) > 0) {
                                                    ?>
                                                    <ol class="dd-list">
                                                        <?php
                                                        foreach ($db->GetRows($livello4) AS $l4) {
                                                            $livello4Id = $l4['Id_Categoria'];
                                                            ?>
                                                            <li class="dd-item dd3-item" data-id="<?php echo $l4['Id']; ?>">
                                                                <div class="dd-handle dd3-handle">Drag</div>
                                                                <div class="dd3-content">
                                                                    <?php echo $l4['Descrizione']; ?>
                                                                    <a href="javascript:popup_categorie('azione=UPD&Id=<?php echo $l4['Id']; ?>&Id_Categoria=<?php echo $livello4Id; ?>&descrizioneALL=0')"><img src="/images/expo2015/gray-write-new-icon.png"/></a>
                                                                    <a href="javascript:popup_cancella('azione=DEL&Id=<?php echo $l4['Id']; ?>&Id_Categoria=<?php echo $livello4Id; ?>')"><img src="/images/expo2015/gray-trash-icon.png"/></a>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ol>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ol>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ol>
                <?php } ?>
            </li>
        <?php } ?>
    </ol>
</div>
<input type="hidden" id="nestable-output" />
<script type='text/javascript' src='/jquery/js/jquery.nestable.js'></script>
<div id="coprente" ><div id="box_popup"></div></div>

