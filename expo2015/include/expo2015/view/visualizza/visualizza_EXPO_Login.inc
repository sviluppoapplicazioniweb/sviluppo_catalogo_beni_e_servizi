<script type="text/javascript">

$(document).ready(function() {

	

	$("#visualizza").hover(
		function () {
	        //mostra sottomenu
			$("input#password").prop('type', 'text');
     
		}, 
		function () {
			//nascondi sottomenu
			$("input#password").prop('type', 'password');       
		}

		
	);
});

function occhio() {

	if ($("input#password").val().length >0){
		$("#visualizza").show();
	} else {
		$("#visualizza").hide();
	}
}



	

</script>

<?php
if ($debugIscrzione['login'] == true) {

    if (strcmp($reg_messaggio, "") != 0) {
        if (defined($reg_messaggio)) {
            $reg_messaggio = constant($reg_messaggio);
        }
        ?>
        <span id="errorLogin"><?php echo $reg_messaggio; ?></span>
        <?php
    }

    $sess->delete();
    page_close();
    unset($sess);
    unset($auth);
    page_open(array("sess" => "CedCamCMS_Session"));
    page_close();
    unset($reg_utente);

    $loginType = settaVar($_GET, "option", "");

    if (strcmp($loginType, "") == 0) {
        ?>
        <div class="preLogin">
            <a href="/index.phtml?Id_VMenu=308&option=firmadig" draggable="false" ><?php echo _ACCEDI_FIRMA_DIG_; ?></a>   
        </div>
        <div class="preLogin" >
            <a href="/index.phtml?Id_VMenu=308&option=user" draggable="false" ><?php echo _ACCEDI_USER_; ?></a>   
        </div>
    <?php } elseif (strcmp($loginType, "firmadig") == 0) { ?> 
        <script language="JavaScript" type="text/javascript">
            $("#titoloBarra").html('Accedi > dispositivo di firma digitale');
        </script>
        <div class="preLogin">
            <form method="post" id="formSmartCard" name="formSmartCard" >
                <span><?php echo _SMART_CARD_; ?></span>
                <a style="margin-left: 2px" class="buttonRed" draggable="false" href="javascript:inviaSmartCard(document.formSmartCard,'index.phtml?Id_VMenu=510')" draggable="false"><?php echo _ACCEDI_; ?></a>
            </form>
        </div>
    <?php } elseif (strcmp($loginType, "regfirmadig") == 0) { ?> 
        <script language="JavaScript" type="text/javascript">
            $("#titoloBarra").html('Registrazione > dispositivo di firma digitale');
        </script>
        <div class="preLogin">
            <form method="post" id="formSmartCard" name="formSmartCard" >
                <span><?php echo _SMART_CARD_; ?></span>
                <a style="margin-left: 2px" class="buttonRed" draggable="false" href="javascript:inviaSmartCard(document.formSmartCard,'index.phtml?Id_VMenu=510')" draggable="false"><?php echo _REGISTRATI_; ?></a>
            </form>
        </div>
    <?php } else { ?>   
        <script language="JavaScript" type="text/javascript">
            $("#titoloBarra").html('Accedi > Nome utente e Password');
        </script>

        <script language="JavaScript" type="text/javascript">
		
			$(document).ready(function() {
		
				$('#formLogin').keypress(function(e) {
			        if (e.which == 13) {
			        	inviaFormLogin(document.formLogin,'/jslogin.phtml');
			        }
			    });
		
			});
		    
	    </script>
        
        <div class="preLogin" style="width: 500px;">
            <form method="post" id="formLogin" name="formLogin" >
                <input type="hidden" name="okurl" id="okurl" value="index.phtml?Id_VMenu=307"/>
                <input type="hidden" name="action" id="action" value="Entra"/>
                <p><b><?php echo _ENTRA_LOG_; ?></b></p>
                <span>E-Mail: <input style="margin-left: 35px" id="utente" name="Login" type="text" size="40" class="login"/><br/></span>
                <br>
                <span><?php echo _PASSWORD_; ?>:<input style="margin-left: 11px" id="password" onkeyup="occhio()" name="Password" type="password" size="40" class="login" autocomplete="off" /><img id="visualizza" style="display:none" height="12px" src="images/occhioPWD.jpg">
                    <br/></span>
                <br>
                <a style="float: right" class="buttonRed" href="javascript:inviaFormLogin(document.formLogin,'/jslogin.phtml')"  draggable="false"><?php echo _ACCEDI_; ?></a>
                <br>
                <br>
                <a class="testoRed" href="/index.phtml?pagina=password" draggable="false"><?php echo _DIM_PAS_; ?></a>
            </form>
        </div>


        <!--
               <div id="contentLogin" style="width: 100%">

                   <div class="boxLogin">
                       <form method="post" id="formLogin" name="formLogin" >
                           <table >
                               <tr>
                                   <td><h3><?php echo _ENTRA_LOG_; ?></h3></td>
                               </tr>
                               <tr>
                                   <td id="loginForm" class="boxLoginSfondo">
                                       <input type="hidden" name="okurl" id="okurl" value="index.phtml?Id_VMenu=307"/>
                                       <input type="hidden" name="action" id="action" value="Entra"/>
                                       <div>
                                           E-Mail:
                                           <br /><input id="utente" name="Login" type="text" size="40" class="login"/><br/>
        <?php echo _PASSWORD_; ?>:
                                           <br /><input id="password" name="Password" type="password" size="40" class="login"/>
                                       </div>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="buttonLogin" >
                                       <a href="javascript:invia(document.formLogin,'/jslogin.phtml')"  draggable="false"><?php echo _ACCEDI_; ?></a>
                                   </td> 
                               </tr>
                               <tr>
                                   <td align="right"> <a href="/index.phtml?pagina=password" draggable="false"><?php echo _DIM_PAS_; ?></a></td>

                               </tr>
                           </table>
                       </form>
                   </div>
        <!--
        <div class="boxCard"> 
            <form method="post" id="formSmartCard" name="formSmartCard" >
                <input type="hidden" name="test" id="test" value="0"/>
                <input type="hidden" name="redirectTo" id="redirectTo" value="<?php echo urlencode($MAINSITE . 'index.phtml?Id_VMenu=309'); ?>"/>
                <input type="hidden" name="ha" id="ha" value="expo2015"/>
                <table >
                    <tr>
                        <td><h3><?php echo _ENTRA_SC_; ?></h3></td> 
                    </tr>
                    <tr>
                        <td id="loginCard" class="boxLoginSfondo">
                            <div><?php echo _SMART_CARD_; ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="buttonLogin" ><a href="javascript:inviaSmartCard(document.formSmartCard,'<?php echo $URLCNS; ?>')" draggable="false"><?php echo _ACCEDI_; ?></a></td> 
                    </tr>
                    <tr>
                        <td><a href="javascript:inviaSmartCard(document.formSmartCard,'<?php echo $URLCNS; ?>')"  draggable="false"><?php echo _REG_; ?></a></td> 
                    </tr>
                </table>
            </form>
        </div>


        <?php
//require_once($PROGETTO . "/view/lib/createPdf.inc"); 
        ?>
        </div>
        <!-- fine tab form login -->
        <?php
    }
} else {
    echo '<div style="text-align: center;width: 100%">
	<img src="images/manuntenzione1.jpg">
	<br /><br /><h1>'.$sitoManuntenzione.'</h1>
	</div>';
    //<img src="images/underconstruction.jpg">
}