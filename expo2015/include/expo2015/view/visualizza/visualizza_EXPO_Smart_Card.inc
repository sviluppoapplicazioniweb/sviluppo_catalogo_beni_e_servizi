<?php
include_once "configuration.inc";
require_once($PROGETTO . "/view/languages/IT.inc");
$dbSC = new DataBase();
$error = false;
$hourDelay = 1;

$delay = $hourDelay * 3600;

$time = time();

$d1 = date("Ymd_H",time() - $delay);
$d2 = date("Ymd_H",time());
$d3 = date("Ymd_H",time() + $delay);


$s1="";
$s2="";

$s1.="*".base64_decode($_GET[SSL_CLIENT_M_SERIAL])."*";
$s1.="*".base64_decode($_GET[SSL_CLIENT_S_DN_Email])."*";
$s1.="*".base64_decode($_GET[SSL_CLIENT_S_DN])."*";
$s1.="*".base64_decode($_GET[SSL_CLIENT_S_DN_G])."*";

$s2.="*".base64_decode($_GET[SSL_CLIENT_S_DN_CN])."*";
$s2.="*".base64_decode($_GET[SSL_CLIENT_S_DN_S])."*";

$sh1=$s1.$d1.$s2;
$sh2=$s1.$d2.$s2;
$sh3=$s1.$d3.$s2;

$hashGen1 = hash('sha256', "expo2015" . $sh1 . "expo2015");
$hashGen2 = hash('sha256', "expo2015" . $sh2 . "expo2015");
$hashGen3 = hash('sha256', "expo2015" . $sh3 . "expo2015");

$validHash = ( (strcmp($hashGen1, $_GET['hash']) == 0) || (strcmp($hashGen2, $_GET['hash']) == 0) || (strcmp($hashGen3, $_GET['hash']) == 0));


if ($validHash && (strcmp ('c928d1eaeabb79c442cbbbdf3c02e18a', $_GET['cyph']) == 0)){


if (isset($_POST['CF'])) {
	
	$CFISC = settaVar($_POST, 'CF', "");
    $CNS_nome = settaVar($_POST, 'Nome', "");
    $CNS_cognome = settaVar($_POST, 'Cognome', "");

    if (strcmp($CFISC, "") != 0) {

    	
    	$CFISCcrc = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_CN])));
    	$CFISCcrc = $CFISCcrc[0];
    	
    	//$CFISCcrc = "FERLA";
    	$patterncod = "^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$";
    	if ((strlen($CFISCcrc) != 16) or (!ereg($patterncod, $CFISCcrc))) {
    		//Verifico se il CF � nello stesso array ma con indice 2
    		$CFISCcrc = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_CN])));
    		$CFISCcrc= $CFISCcrc[2];
    		if ((strlen($CFISCcrc)!=16) or (!ereg($patterncod,$CFISCcrc))){
    			//Verifica se il CF � nel campo SSL_CLIENT_S_DN
    			$CFISCcrc = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN])));
    			//print_r($CFISCcrc);
    			//exit;
    			$CFISCcrc = str_ireplace("serialNumber=", "", $CFISCcrc[6]);
    			
    		}
    	}
    	    	
    	if (strcmp($CFISC, $CFISCcrc) == 0) {
    	
        $nome = mysql_escape_string(htmlspecialchars(settaVar($_POST, "Nome", "")));
        $cognome = mysql_escape_string(htmlspecialchars(settaVar($_POST, "Cognome", "")));
        $mail = settaVar($_POST, "Email", "");
        $mailAlternativa = settaVar($_POST, "EmailAlternativa", "");
        $username = $mail;
        
        $sendMail = $mail;
        if (strlen($mailAlternativa) > 5){
        	$sendMail.="; $mailAlternativa";
        }
	
        /*
        print strlen($mailAlternativa);
        print "<br>";
        print $sendMail;
        exit;
        */
    	$presente = $dbSC->GetRow("SELECT Id FROM T_Anagrafica WHERE Login = '$username'", "Id");
        if (strcmp($presente, "") != 0) {
            print "<div class=\"duplicateMail\"><img src=\"/images/expo2015/alert.png\" width=\"24px\"> "._EMAIL_PRESENTE_."</div><br><br>";
        } else {
        	$presente = $dbSC->GetRow("SELECT Id FROM T_Anagrafica WHERE Codice_Fiscale='$CFISC'", "Id");
        	if (strcmp($presente, "") != 0) {
        		print "<div class=\"duplicateMail\"><img src=\"/images/expo2015/alert.png\" width=\"24px\"> "._LR_PRESENTE_."</div><br><br>";
        	} else { 
	            $pass = passwordCasuale(8);
	            //$idUtente = $dbSC->GetRow("SELECT MAX(Id) AS id FROM T_Anagrafica", "id") + 1;
	            $sqlMaxId = "(SELECT @idUtente := COALESCE(MAX(TA.Id),0)+1 AS Id FROM T_Anagrafica AS TA)";
	            $return = $dbSC->Query("INSERT INTO T_Anagrafica VALUES($sqlMaxId,'$nome','$cognome','','','','$mail','','','','',0,'',0,'','','','','$CFISC','',NOW(),'$username','$pass','2015-12-31','D')");
	            if ($return != false)
	            $dbSC->Query("INSERT INTO T_Utente VALUES((SELECT @idUtente),NULL,0,0,NULL,0,'96','|',0,'|','|','','','|','',1,'|','',0,'',0)");
	            $passUrl = urlencode(base64_encode($pass));
	            include "send_mail.inc";
	            $utente = $nome . ' ' . $cognome;
	            $link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $mail . "&cyph=" . md5($mail . $keySito) . "&Lang2=IT&action=INS";
	
	            send_mail($sendMail, $MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true);
	            echo _OK_INVIO_LINK_;
	            exit();
        	}
        }
        
       
    }else{
    	//print _ERROR_CNS_;
       	//redirect('index.phtml');
       	$error = true;
       }
    }
} else {
	
	$paramURL = "SSL_CLIENT_M_SERIAL=" . $_GET[SSL_CLIENT_M_SERIAL];
	$paramURL .= "&SSL_CLIENT_S_DN=" . $_GET[SSL_CLIENT_S_DN];
	$paramURL .= "&SSL_CLIENT_S_DN_CN=" . $_GET[SSL_CLIENT_S_DN_CN];
	$paramURL .= "&SSL_CLIENT_S_DN_G=" . $_GET[SSL_CLIENT_S_DN_G];
	$paramURL .= "&SSL_CLIENT_S_DN_S=" . $_GET[SSL_CLIENT_S_DN_S];
	$paramURL .= "&SSL_CLIENT_S_DN_Email=" . $_GET[SSL_CLIENT_S_DN_Email];
	$paramURL .= "&hash=" . $_GET['hash'];
	$paramURL .= "&cyph=" . $_GET['cyph'];
		
	//Salva nella tabella Log i dati della CNS - 13 Gennaio 2014
	$param = "SSL_CLIENT_M_SERIAL=" . urldecode(base64_decode($_GET[SSL_CLIENT_M_SERIAL]));
    $param .= "&SSL_CLIENT_S_DN=" . urldecode(base64_decode($_GET[SSL_CLIENT_S_DN]));
    $param .= "&SSL_CLIENT_S_DN_CN=" . urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_CN]));
    $param .= "&SSL_CLIENT_S_DN_G=" . urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_G]));
    $param .= "&SSL_CLIENT_S_DN_S=" . urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_S]));
    $param .= "&SSL_CLIENT_S_DN_Email=" . urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_Email]));
	
    

    $queryst = "INSERT INTO `EXPO_T_Catalogo_Logs` (`data`,`azione`,`parametri`,`response`)
						VALUES('" . date('Y-m-d H:i:s') . "','Iscrizione','Iscrizione LR','" .  str_replace("'", "''", $param) . "')";
    $dbSC->Query($queryst);
    
    
    	$CFISC = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_CN])));
    	$CFISC = $CFISC[0];
    	$CNS_nome = urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_G]));
    	$CNS_cognome = urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_S]));

    	//$CFISC = "FERLA";
    	$patterncod = "^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$";
    	if ((strlen($CFISC) != 16) or (!ereg($patterncod, $CFISC))) {
    		//Verifico se il CF � nello stesso array ma con indice 2
    		$CFISC = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_CN])));
    		$CFISC= $CFISC[2];
    		if ((strlen($CFISC)!=16) or (!ereg($patterncod,$CFISC))){
    			//Verifica se il CF � nel campo SSL_CLIENT_S_DN
    			$CFISC = explode("/", urldecode(base64_decode($_GET[SSL_CLIENT_S_DN])));
    			//print_r($CFISC);
    			//exit;
    			$CFISC = str_ireplace("serialNumber=", "", $CFISC[6]);
    			$CNS_nome = urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_G]));
    			$CNS_cognome = urldecode(base64_decode($_GET[SSL_CLIENT_S_DN_S]));
    		}
    	}
   
}
}else {
	$error = true;
}
if (!$error){
?>
<form  id="form_moduli" name="form_moduli">
    <input type = "hidden" name = "CF" id = "CF" value = "<?php echo $CFISC; ?>"/>
    <?php $stato = "0"; ?>

    <div class = "boxSmartCard">
        <div class = "form_label">
            <label for = "Nome" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _NOME_; ?> *</label>

        </div>
        <div>
            <input type = "text" id="Nome" name = "Nome" size = "65" maxlength = "100" value = "<?php echo $CNS_nome; ?>"/> 
        </div>
        <br style = "clear: left;" />

        <div class = "form_label">
            <label for = "Cognome" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _COGNOME_; ?> *</label>
        </div>
        <div>
            <input type = "text" id="Cognome" name = "Cognome" size = "65" maxlength = "100" value = "<?php echo $CNS_cognome; ?>"/>
        </div>
        <br style = "clear: left;" />

        <div class = "form_label">
            <label for = "Email" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _EMAIL_; ?> *</label>
        </div>
        <div>
            <input type = "text" id="Email" name = "Email" size = "65" maxlength = "200" value = "<?php echo $CNS_email; ?>"/>
        </div>
        <br style = "clear: left;" />
        <div class = "form_label">
            <label for = "Email" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _CONFERMA_EMAIL_; ?> *</label>
        </div>
        <div>
            <input type = "text" id="EmailC" name = "EmailC" size = "65" maxlength = "200" value = ""/>
        </div>
         <br style = "clear: left;" />
         <div class = "form_label">
            <label for = "EmailAlternativa" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _EMAIL_ALTERNATIVA_; ?></label>
        </div>
        <div>
            <input type = "text" id="EmailAlternativa" name = "EmailAlternativa" size = "65" maxlength = "200" value = ""/>
        </div>
        <br style = "clear: left;" />
        <div class = "form_label">
            <label for = "EmailAlternativa" style='font-size: 1.0em;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;'><?php echo _CONFERMA_EMAIL_ALTERNATIVA_; ?></label>
        </div>
        <div>
            <input type = "text" id="EmailCAlternativa" name = "EmailCAlternativa" size = "65" maxlength = "200" value = ""/>
        </div>
        <br style = "clear: left;" />
        <div id="footerForm" style="text-align: right">
            <a class="buttonLogin" href="javascript:salva(document.form_moduli,'index.phtml?Id_VMenu=<?php echo $Id_VMenu; ?>&<?php echo $paramURL; ?>','<?php echo $stato; ?>')"><?php echo _INVIA_; ?></a>
        </div>
    </div>


</form>

<?php 
}else {
	print _ERROR_CNS_;
	//redirect('index.phtml');
}


	 


?>

