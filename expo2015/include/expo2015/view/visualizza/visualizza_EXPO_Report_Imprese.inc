

<?php
require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();
$idNonTest = 27;
?>

<div class="tsc_tabs_type_3_container" >
    <ul id="tsc_tabs_type_3">
        <li><a href="#" name="#gtab1">Forme giuridiche</a></li>
        <li><a href="#" name="#gtab2">Imprese appartenti a rete Imprese</a></li>
        <li><a href="#" name="#gtab3">Fascia fatturato</a></li>
        <li><a href="#" name="#gtab4">Dipendenti</a></li>
        <li><a href="#" name="#gtab5">Province</a></li>
        <li><a href="#" name="#gtab6">Iscrizione Mensile</a></li>
        <li><a href="#" name="#gtab7">Categorie Merceologiche</a></li>
    </ul>
    <div id="tsc_tabs_type_3_content" >
        <!-- Tab 1 Start -->
        <div id="gtab1">
            <br>
            <p>
                <a href="/downloadCSV.php?tab=formeGiuridiche" class="buttonGrey">Download CSV</a>
            </p>

            <table>
                <tr style="border-bottom: 1px solid #000;font-weight: bold;">
                    <td><a href="javascript:ordina('descrizione','formeGiuridiche')" >Forme giuridiche</a></td>
                    <td style="text-align: right"><a href="javascript:ordina('iscritti','formeGiuridiche')" >Iscritti</a></td>
                </tr>
                <tr>
                    <td colspan="2" id="formeGiuridiche" >
                        <dl class="tsc_accordion2" style="width:100%;margin-left: -10px" >
                            <?php
                            //FORME GIURIDICHE
                            $query = "SELECT * FROM EXPO_Tlk_Forme_Giuridiche ORDER BY Descrizione";
                            foreach ($db->GetRows($query) AS $rows) {
                                $desc = $rows['Descrizione'];
                                $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and FormaGiuridica = '" . mysql_escape_string(htmlentities($desc)) . "' ORDER BY RagioneSociale";
                                $numImprese = $db->NumRows($queryImprese);
                                ?>
                                <dt ><?php
                                echo $desc;
                                print "<span style=\"float:right;\">" . $numImprese . "</span>";
                                ?></dt>
                                <dd >
                                    <p>
                                        <?php
                                        foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                            echo $rowsImprese['RagioneSociale'] . "<br>";
                                        }
                                        ?>
                                    </p>
                                </dd>
                            <?php } ?>
                        </dl>
                    </td>
                </tr>
            </table>

        </div>
        <!-- Tab 1 End -->
        <!-- Tab 2 Start -->
        <div id="gtab2">
            <br>
            <p>
                <a href="/downloadCSV.php?tab=retiImprese" class="buttonGrey">Download CSV</a>
            </p>

            <table>
                <tr style="border-bottom: 1px solid #000;font-weight: bold;">
                    <td><a href="javascript:ordina('imprese','retiImprese')" >Imprese</a></td>
                </tr>
                <tr>
                    <td id="retiImprese">
                        <?php
//RETE IMPRESE
                        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and IsRetiImpresa = 'Y' ORDER BY RagioneSociale";
                        foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                            echo $rowsImprese['RagioneSociale'] . "<br>";
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Tab 2 End -->
        <!-- Tab 3 Start -->
        <div id="gtab3">
            <dl class="tsc_accordion2" style="width:80%;">

                <?php
//FATTURATO
                $query = "SELECT * FROM EXPO_T_FascieFatturato ORDER BY Id";
                foreach ($db->GetRows($query) AS $rows) {
                    /*
                    $fatturato = explode("|", $rows['Valore']);
                    $min = $fatturato[0] . "000";
                    $max = $fatturato[1] . "000";
                    */
                    $idFatt = $rows['Id'];
                    $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE  Id > $idNonTest and FasciaFatturato = $idFatt and StatoRegistrazione > 3 ORDER BY RagioneSociale";

                    /*if (strcmp($max, "000") == 0) {
                        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and UltimoFatturato >= $min ORDER BY RagioneSociale";
                    } else {
                        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE  Id > $idNonTest and UltimoFatturato >= $min AND  UltimoFatturato <= $max ORDER BY RagioneSociale";
                    }*/
                    $numImprese = $db->NumRows($queryImprese);
                    ?>
                    <dt ><?php
                    $idTipo = $idFatt+1;
                    echo $rows['Nome']. " - ". $rows['Descrizione'];// . " " . $fatturato[0] . " > " . $fatturato[1];
                    print "<span style=\"float:right;\">" . $numImprese . "</span>";
                    ?></dt>
                    <dd >
                        <p>
                            <?php
                            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                echo $rowsImprese['RagioneSociale'] . "<br>";
                            }
                            ?>
                        </p>
                    </dd>
                <?php } ?>
            </dl>
        </div>
        <!-- Tab 3 End -->
        <!-- Tab 4 Start -->
        <div id="gtab4">
            <dl class="tsc_accordion2" style="width:80%;">
                <?php
//NUMERO DIPENDENTI
                $query = "SELECT * FROM EXPO_TJ_Tipo_Impresa_Dipendenti  ORDER BY Id";
                foreach ($db->GetRows($query) AS $rows) {
                    $dipendenti = explode("|", $rows['Valore']);
                    $min = $dipendenti[0];
                    $max = $dipendenti[1];
                    $idDip = $rows['IdTipoImpresa'];

                    if (strcmp($max, "") == 0) {
                        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and NumeroDipendenti >= $min and StatoRegistrazione >3 ORDER BY RagioneSociale";
                    } else {
                        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and NumeroDipendenti >= $min AND  NumeroDipendenti <= $max and StatoRegistrazione >3 ORDER BY RagioneSociale";
                    }
                    $numImprese = $db->NumRows($queryImprese);
                    ?>
                    <dt ><?php
                    echo $db->GetRow("SELECT * FROM EXPO_T_Tipo_Impresa WHERE Id > $idNonTest and Id = '$idDip'", "Descrizione") . " " . $min . " > " . $max;
                    print "<span style=\"float:right;\">" . $numImprese . "</span>";
                    ?></dt>
                    <dd >
                        <p>
                            <?php
                            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                echo $rowsImprese['RagioneSociale'] . "<br>";
                            }
                            ?>
                        </p>
                    </dd>
                <?php } ?>
            </dl>
        </div>
        <!-- Tab 4 End -->
        <!-- Tab 5 Start -->
        <div id="gtab5">
            <br>
            <p>
                <a href="/downloadCSV.php?tab=province" class="buttonGrey">Download CSV</a>
            </p>
            <table >
                <tr style="border-bottom: 1px solid #000;font-weight: bold;">
                    <td ><a href="javascript:ordina('province','province')" >Province</a></td>
                    <td style="text-align: right"><a href="javascript:ordina('iscritti','province')" >Iscritti</a></td>
                </tr>
                <tr>
                    <td id="province" colspan="2">
                        <dl class="tsc_accordion2" style="width:100%;margin-left: -10px">

                            <?php
//PROVINCIA
                            $query = "SELECT DISTINCT Descrizione,Pr FROM EXPO_T_Imprese AS T JOIN Tlk_Province AS T2 "
                                    . "ON T.Pr = T2.Sigla"
                                    . " WHERE T.Id > $idNonTest ORDER BY Pr";
                            foreach ($db->GetRows($query) AS $rows) {
                                $pr = $rows['Pr'];
                                $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and Pr = '$pr' and StatoRegistrazione >3  ORDER BY RagioneSociale";
                                $numImprese = $db->NumRows($queryImprese);
                                ?>
                                <dt ><?php
                                echo $rows['Descrizione'];
                                print "<span style=\"float:right;\">" . $numImprese . "</span>";
                                ?></dt>
                                <dd >
                                    <p>
                                        <?php
                                        foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                            echo $rowsImprese['RagioneSociale'] . "<br>";
                                        }
                                        ?>
                                    </p>
                                </dd>
                            <?php } ?>
                        </dl>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Tab 5 End -->
        <!-- Tab 6 Start -->
        <div id="gtab6">

            <form id="form_filtro" name="form_filtro" method="post" action="/downloadCSV.php?tab=iscrizioni">
                <br>
                <p>
                <input type="submit" name="submit" value="Download CSV">
                </p>
                <p>
                    <b style="margin-right: 36px">Periodo</b>
        			<select id="periodo" name="periodo" style="width: 346px; margin-right: 20px;">
                        <option value="" >Seleziona...</option>
                        <?php
                        $arrayPeriodo = array(1, 3, 6, 12);
                        foreach ($arrayPeriodo AS $i) {

                            $data = date('Y-m-d', strtotime('-' . $i . ' months'));
                            $periodo = "Ultimi " . $i . " mesi";
                            if ($i == 1) {
                                $periodo = "Ultimo mese";
                            } elseif ($i == 12) {
                                $periodo = "Ultimo anno";
                            }
                            $selected = "";
                            if (strcmp($dataPeriodo, $data) == 0) {
                                $selected = "selected";
                            }
                            ?>
                            <option value="<?php echo $data; ?>" <?php echo $selected; ?>><?php echo $periodo; ?></option>
                        <?php } ?>
                    </select>
                    <input type="button" value="Visualizza"  onclick="javascript:invia('periodo')"/>
                </p>
                <p>
                    <b>Intervallo dal</b> 
                    <input type="text" id="dataInizio" name="dataInizio" value="<?php echo $dataInizioInput; ?>" />
                    <b>al</b>
                    <input type="text" id="dataFine" name="dataFine" value="<?php echo $dataFineInput; ?>" />
                    <input type="button" value="Visualizza"  onclick="javascript:invia('data')"/>
                </p>


                <!-- DATA -->

                
            </form>
            <br>
            <table>
                <tr style="border-bottom: 1px solid #000;font-weight: bold;">
                    <td><a href="javascript:ordina('data','iscrizioni')" >Data</a></td>
                    <td style="text-align: right"><a href="javascript:ordina('iscritti','iscrizioni')" >Iscritti</a></td>
                </tr>
                <tr>
                    <td id="iscrizioni" colspan="2">
                        <dl class="tsc_accordion2" style="width:100%;margin-left: -10px" >
                            <?php
//NUMERO DI IMPRESE X MESE
                            $arrayMesi1 = array("08" => "Agosto", "09" => "Settembre", "10" => "Ottobre", "11" => "Novembre", "12" => "Dicembre");
                            $arrayMesi2 = array("01" => "Gennaio", "02" => "Febbraio", "03" => "Marzo", "04" => "Aprile", "05" => "Maggio", "06" => "Giugno", "07" => "Luglio", "08" => "Agosto", "09" => "Settembre", "10" => "Ottobre", "11" => "Novembre", "12" => "Dicembre");
                            for ($anno = 2013; $anno <= date("Y"); $anno ++) {
                                if ($anno == 2013) {
                                    $mesi = $arrayMesi1;
                                } else {
                                    $mesi = $arrayMesi2;
                                }
                                foreach ($mesi AS $k => $m) {
                                    $data = $anno . " - " . $m;
                                    $dataDB = $anno . "-" . $k;
                                    $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and DataRegistrazione LIKE '$dataDB%' ORDER BY RagioneSociale";
                                    $numImprese = $db->NumRows($queryImprese);
                                    ?>
                                    <dt ><?php
                                    echo $data;
                                    print "<span style=\"float:right;\">" . $numImprese . "</span>";
                                    ?></dt>
                                    <dd >
                                        <p>
                                            <?php
                                            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                                echo $rowsImprese['RagioneSociale'] . "<br>";
                                            }
                                            ?>
                                        </p>
                                    </dd>
                                    <?php
                                }
                            }
                            ?>
                        </dl>
                    </td>
                </tr>
            </table>
        </div>
        <!-- Tab 6 End -->
        <!-- Tab 7 Start -->
        <div id="gtab7">
            <dl class="tsc_accordion2" style="width:80%;">
                <?php
                //Categorie Merceologiche
                $query = "SELECT DISTINCT C.Id AS IdCategoria, C.Descrizione As Descrizione FROM EXPO_Tlk_Categorie as C
                WHERE LENGTH(C.Id_Categoria)=8 ORDER BY C.Id_Categoria";
                
                
                // C.Id_Categoria like '0$cat%' And  
                //$query = "SELECT DISTINCT I.IdCategoria, C.Descrizione As Descrizione FROM EXPO_TJ_Imprese_Categorie as I join EXPO_Tlk_Categorie as C on I.IdCategoria = C.Id WHERE IdImpresa > $idNonTest ORDER BY C.Id_Categoria";
                foreach ($db->GetRows($query) AS $rows) {
						
                    $category = $rows;
                   
                    $queryImprese = "SELECT DISTINCT(I.Id) As Id, I.RagioneSociale as RagioneSociale FROM EXPO_T_Imprese AS I join EXPO_TJ_Imprese_Categorie AS IC on I.Id=IC.IdImpresa WHERE I.Id > $idNonTest and IC.IdCategoria = '" . $category['IdCategoria'] . "'and IC.IsIscritta='Y' ORDER BY RagioneSociale";

                    $numImprese = $db->NumRows($queryImprese);
                    ?>
                    <dt ><?php
                    echo $category['Descrizione'];
                    print "<span style=\"float:right;\">" . $numImprese . "</span>";
                    ?></dt>
                    <dd >
                        <p>
                            <?php
                            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                                echo $rowsImprese['RagioneSociale'] . "<br>";
                            }
                            ?>
                        </p>
                    </dd>
                <?php } ?>
            </dl>
        </div>
        <!-- Tab 7 End -->

    </div>
</div>
<!-- DC Flat Tabs End -->
<div class="tsc_clear"></div> <!-- line break/clear line -->
<script type="text/javascript" src='/jquery/js/tsc/visualizza_EXPO_Report_Imprese.js'></script>