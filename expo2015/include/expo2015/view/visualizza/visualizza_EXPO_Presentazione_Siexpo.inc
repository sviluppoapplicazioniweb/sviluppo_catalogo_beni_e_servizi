<?php 

if (isset($uid) && $uid > 0){
	$db = new DataBase();
	
	$query = "SELECT T_Anagrafica.Id AS IdLR, T_Anagrafica.Nome AS NomeLR, T_Anagrafica.Cognome AS CognomeLR, EXPO_T_Imprese.Telefono, EXPO_T_Imprese.Email, EXPO_T_Imprese.RagioneSociale, 
	EXPO_T_Imprese.Indirizzo AS IndImpresa, EXPO_T_Imprese.Cap AS CapImpresa, EXPO_T_Imprese.Citta, EXPO_T_Imprese.Pr, EXPO_T_Imprese.CodiceFiscale, EXPO_T_Imprese.PartitaIva, CONCAT_WS('',Pr, Nrea) AS Numrea, 
	T_Anagrafica.Login, EXPO_T_Imprese.CapitaleSociale, EXPO_T_Imprese.WebSiteURL, EXPO_T_Imprese.NumeroDipendenti
	FROM (EXPO_T_Imprese INNER JOIN EXPO_TJ_Imprese_Utenti ON EXPO_T_Imprese.Id = EXPO_TJ_Imprese_Utenti.IdImpresa) INNER JOIN T_Anagrafica ON EXPO_TJ_Imprese_Utenti.IdUtente = T_Anagrafica.Id
	WHERE T_Anagrafica.Id=$uid AND EXPO_T_Imprese.Id = $IdImpresa;";
	//echo "Qy: ".$query;
	//exit;
	$row = $db->GetRow($query);
}
?>
<div style="padding:0px;height:100%;width:100%;background-color:#FFFFFF;">
	<div style="float:left;width:370px;background-image: url(/images/siexpo/pres.jpg);background-color:#FFFFFF;margin-right:10px;padding-top:30px; height:331px;background-size:100%;">
	</div>
	<div style="float:left;background-color:#FFFFFF; width:400px; height:450px;">
		<div style="margin:5px ; width:400px;">
			<p><strong>SiExpo</strong> &egrave; un catalogo progettato per contenere materiali e prodotti sostenibili e innovativi messi a disposizione di progettisti e allestitori di Expo 2015 e accessibile a tutti tramite il sito <a href="http://www.siexpo2015.it" target="_blank">www.siexpo2015.it</a>.<br /><br />SiExpo &egrave; organizzato per prodotti e applicazioni, suddivise in cinque sezioni principali:</p>
			<ul>
				<li>costruzioni e allestimenti</li>
				<li>arredo per interni</li>
				<li>arredo urbano</li>
				<li>packaging</li>
				<li>complementi fieristici</li>
			</ul>
			<p>Le aziende iscritte al Catalogo INExpo che intendono promuovere i loro prodotti green possono iscriversi a SiExpo purch&eacute; rispondano a determinati criteri di sostenibilit&agrave; e innovazione individuati dal Comitato Tecnico Scientifico di SiExpo e indicati nel <a href="http://www.siexpo2015.it/index.phtml?Id_VMenu=261" target="_blank">regolamento</a>. L'iscrizione non presenta costi aggiuntivi.</p>
			<?php if (isset($uid) && $uid > 0){ ?>
			<form  name="siexpoForm" id="siexpoForm" >
            <input type="hidden" name="Nome" value="<?php echo $row["NomeLR"] ?>">
            <input type="hidden" name="Cognome" value="<?php echo $row["CognomeLR"] ?>">
            <input type="hidden" name="Telefono" value="<?php echo $row["Telefono"]; ?>">
            <input type="hidden" name="Email" value="<?php echo $row["Email"]; ?>">
            <input type="hidden" name="RagSoc" value="<?php echo $row["RagioneSociale"]; ?>">
            <input type="hidden" name="Indirizzo" value="<?php echo $row["IndImpresa"] ?>">
            <input type="hidden" name="CAP" value="<?php echo $row["CapImpresa"] ?>">
            <input type="hidden" name="Comune" value="<?php echo $row["Citta"]; ?>">
            <input type="hidden" name="Prov" value="<?php echo $row["Pr"]; ?>">
            <input type="hidden" name="PIVA" value="<?php echo $row["PartitaIva"]; ?>">
			<input type="hidden" name="CF" value="<?php echo $row["CodiceFiscale"] ?>">
            <input type="hidden" name="Login" value="<?php echo $row["Login"] ?>">
            <input type="hidden" name="NRea" value="<?php echo $row["Numrea"]; ?>">
            <input type="hidden" name="SitoURL" value="<?php echo $row["WebSiteURL"]; ?>">
            <input type="hidden" name="NumDip" value="<?php echo $row["NumeroDipendenti"]; ?>">
            <input type="hidden" name="CapSoc" value="<?php echo $row["CapitaleSociale"] ?>">
            
            <input type="hidden" name="cyph" value="<?php echo md5($row["Login"].  date("ymd") ."siexpoexpo2015@@@"); ?>">
            <?php echo _TEXT_ISCR_SIEXPO_LG_; ?>			
        	</form>
        	<?php } else { 
        		echo _TEXT_ISCR_SIEXPO_NOLG_;	
        	} ?>
        	<p><span style="font-size:11px">Per informazioni | <a href="mailto:info@siexpo2015.it">info@siexpo2015.it</a></span></p>
		</div>
	</div>
</div>

