<form enctype="multipart/form-data" id="form_moduli" name="form_moduli"  >
    <?php
    $IdImpresa = $auth->auth['IdImpresa'];
    $idPage = $_GET['Id_VMenu'];
    //require_once($PROGETTO . "/view/lib/db.class.php");
    $db = new DataBase();


    if (strcmp($azione, "VIEW") == 0) {

        $idBacheca = str_replace("Id", "", base64_decode(settaVar($_GET, 'Id', '')));
        $query = "SELECT * FROM EXPO_T_Bacheca WHERE Id = '$idBacheca' AND IdImpresa = '$IdImpresa'";
        if (strcmp($db->GetRow($query, "Id",null,"visualizza_EXPO_T_Bacheca.inc Ln.13"), "") == 0) {
            redirect("index.phtml?Id_VMenu=319" );
            exit;
        }

        $isColor = 1;


        $queryBachSQL = "SELECT Oggetto,T.Nome AS Nome, Cognome,RagSoc
			FROM T_Anagrafica AS T
			RIGHT JOIN EXPO_T_Bacheca AS TB ON T.Id = TB.IdPartecipante
			WHERE TB.Id =  '$idBacheca'
			ORDER BY TB.Id";
        $queryBach = $db->GetRow($queryBachSQL, null, null, "visualizza_EXPO_T_Bacheca.inc Ln.26"); 
        
        ?>
        <input type="hidden" id="Id" name="Id" value="<?php echo $idBacheca; ?>" />
        <div style="border-bottom: 1px solid #26ACE2;border-top: 1px solid #26ACE2;padding: 10px 0px 10px;">
            <strong>ID : </strong>#<?php echo $idBacheca; ?> <br>
            <br>
            <strong><?php echo _MITTENTE_; ?> : </strong><?php echo $queryBach["Nome"]. ' ' . $queryBach["Cognome"]. ' - ' . $queryBach["RagSoc"]; ?> <br>
            <br>
            <strong><?php echo _PRODOTTO_; ?> : </strong> <ul>               
                <?php
                $query = "SELECT DISTINCT Nome
                                  FROM EXPO_T_Prodotti AS T
                                  RIGHT JOIN EXPO_TJ_Bacheca_Prodotti AS TJ ON T.Id = TJ.IdProdotto
                                WHERE TJ.IdBacheca = '$idBacheca'
                                ORDER BY Nome";
                foreach ($db->GetRows($query, null, "visualizza_EXPO_T_Bacheca.inc Ln.42") AS $rows) {
                    ?>
                    <li><?php echo $rows ['Nome']; ?></li>
                <?php } ?>
            </ul>
            <br>
            <strong><?php echo _OGGETTO_; ?> : </strong><?php echo $queryBach["Oggetto"]; ?>
            <br><br>
            <table width="100%">

                <?php
                $query = "SELECT * FROM EXPO_T_Bacheca_Messaggi WHERE IdBacheca = " . $idBacheca . " order by Data asc ";
                // conto gli elementi dela query
                $totRow = $db->NumRows($query, null, "visualizza_EXPO_T_Bacheca.inc Ln.55");
                $contRow = 0;
                // creo il testo per visualizzare il totale elementi
                $n_elementi = str_replace("%n%", $totRow, _NUMERO_OGGETTI_BACHECA_);
                ?>
                <tr style="height: 50px; cursor: pointer; border-top: 2px solid #26ACE2; border-bottom: 2px solid #26ACE2" onclick="showMail('.rowMessagge');">
                    <td colspan="3"  style="text-align: center"><b style="color: #26ACE2" ><?php echo $n_elementi; ?></b></td>
                </tr>
                <tr>
                    <td colspan="3" width="100%">

                        <table width="100%" style="display: none" class="rowMessagge" >
                            <?php
                            // creo il box esterno  che conterrà gli header dei messaggi
                            $bgcolor = "background-color: #cccccc;";
                            foreach ($db->GetRows($query, null, "visualizza_EXPO_T_Bacheca.inc Ln.70") AS $rows) {
                                $contRow++;
                                if ($totRow != $contRow) {
                                    $bgcolor = "background-color: #E6E6E6;";
                                } else {
                                    $bgcolor = "background-color: #FAFAFA;";
                                }

                                if ($rows['TipoMittente'] == 1) {
                                    // il mittente è impresa
                                    $infoAzienda = "SELECT * from EXPO_T_Imprese where Id='" . $rows['IdMittente'] . "'";
                                    $Mittente = $db->GetRow($infoAzienda, 'RagioneSociale', null, "visualizza_EXPO_T_Bacheca.inc Ln.81");
                                }

                                if ($rows['TipoMittente'] == 2) {
                                    // il mittente è utente
                                    $infoAzienda = "SELECT * from T_Anagrafica where Id='" . $rows['IdMittente'] . "'";
                                    $Mittente = $db->GetRow($infoAzienda, 'Nome', null, "visualizza_EXPO_T_Bacheca.inc Ln.87") . " " . $db->GetRow($infoAzienda, 'Cognome',87);
                                }
                                // creo gli header dei messaggi che visualizzano il contenuto all'onclick
                                ?>
                                <tr  style="<?php echo $bgcolor; ?>;height: 50px; " >

                                    <td colspan="2" onClick="$('#divScorrevole<?php echo $rows['Id']; ?>').toggle('slow');"  style="cursor: pointer;text-align: left;"><?php echo "<b>" . $Mittente . "</b> il " . date("d-m-Y H:i", strtotime($rows['Data'])) . "  ha scritto   <i>" . substr($rows['Testo'], 0, 70) . "..</i>"; ?></td>
                                    <td>
                                        <?php
                                        // seleziono tutti  allegati
                                        $record = $db->GetRow("SELECT name FROM EXPO_T_Bacheca_Allegati WHERE Id = " . $rows['Id'], "name",null, null, "visualizza_EXPO_T_Bacheca.inc Ln.97");
                                        if (strcmp($record, "") != 0) {
                                            ?>
                                            <a href="ajaxlib/requester/viewFile.inc?Id=<?php echo $rows['Id']; ?>" target="_blank"><?php echo _VISUALIZZA_ . " " . _ALLEGATO_; ?> ></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr id="divScorrevole<?php echo $rows['Id']; ?>" style="<?php
                                if ($totRow == $contRow) {
                                    echo "display: ;";
                                } else {
                                    echo "display: none;";
                                }
                                ?>">
                                    <td colspan="3"  ><?php echo $rows['Testo']; ?></td>
                                </tr>
                                <tr  >
                                    <td colspan="3" ><hr style="color: #26ACE2"></td>
                                </tr>
                                <?php
                            }
                            ?>        
                        </table>
                    </td>
                </tr>
                <?php if ($utype != 94){ ?>
                <tr>
                    <td colspan="3" ><h4><?php echo _MESSAGGIO_; ?> *</h4></td>
                </tr> 
                <tr>
                    <td colspan="3" ><textarea rows="12" cols="70" id="messaggio" name="messaggio"></textarea></td>
                </tr>
              <tr>
                    <td colspan="3" style="padding-top: 10px;"><?php echo _ALLEGATO_; ?> : <input type="file" name="allegato" id="allegato"></td>   
                </tr> 
                <?php }?>
            </table>  
        </div>
        <p style="text-align: right">
            <a  href="/index.phtml?Id_VMenu=<?php echo $idPage; ?>" class="buttonRed" draggable="false"  ><?php echo _INDIETRO_; ?></a>
            <?php if ($utype != 94){?>
            <a href="javascript:salva(document.form_moduli,'index.phtml?Id_VMenu=<?php echo $idPage; ?>&amp;azione=SEND')" class="buttonRed" draggable="false"  ><?php echo _INVIA_; ?></a>
			<?php }?>
            <?php
            $stato = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = $idBacheca", "Stato", null, "visualizza_EXPO_T_Bacheca.inc Ln.141");
            if (strcmp($stato, "S") == 0 || strcmp($stato, "") == 0) {
                ?>
                <a href="javascript:invia(document.form_moduli,'index.phtml?Id_VMenu=303&amp;azione=CLS&Id=<?php echo base64_encode("Id" . $idBacheca); ?>')" class="buttonRed" draggable="false"  ><?php echo _CHIUSURA_; ?></a>
            <?php } ?>
        </p>
        <?php
    } else {

        if (strcmp($azione, "SEND") == 0) {
            $idBacheca = settaVar($_POST, "Id", "");
            $testo = settaVar($_POST, "messaggio", "");
            $sqlBacheca = "SELECT IdImpresa, IdPartecipante from EXPO_T_Bacheca where Id='" . $idBacheca . "'";
            $bacheca = $db->GetRow($sqlBacheca,null, null, "visualizza_EXPO_T_Bacheca.inc Ln.154");
            $idImpresa = $bacheca['IdImpresa'];
            $IdPartecipante = $bacheca['IdPartecipante'];
             
            $idMittente = $idImpresa;
            $tipoMittente = 1;
            $idDestinatario = $IdPartecipante;
            $tipoDestinatario = 2;
            $db->Query("INSERT INTO EXPO_T_Bacheca_Messaggi VALUES((SELECT @IdMsg := COALESCE(MAX(M.Id),0)+1 AS Id FROM EXPO_T_Bacheca_Messaggi AS M),'$idBacheca','" . $idMittente . "',
                    '" . $tipoMittente . "','" . $idDestinatario . "','" . $tipoDestinatario . "','" . mysql_escape_string($testo) . "',NOW())",null,"visualizza_EXPO_T_Bacheca.inc Ln.163");
            $idMsg = $db->GetRow("SELECT @IdMsg as IdMsg", "IdMsg",null,"estrazione max msg visualizza_EXPO_T_Bacheca.inc Ln.253");
            
            
            /*$idMsg = $db->GetRow("SELECT MAX(Id) AS Id FROM EXPO_T_Bacheca_Messaggi", "Id", null, "visualizza_EXPO_T_Bacheca.inc Ln.162") + 1;
            $db->Query("INSERT INTO EXPO_T_Bacheca_Messaggi VALUES($idMsg,'$idBacheca','" . $idMittente . "',
                    '" . $tipoMittente . "','" . $idDestinatario . "','" . $tipoDestinatario . "','" . mysql_escape_string($testo) . "',NOW())",null, null, "visualizza_EXPO_T_Bacheca.inc Ln.164");
*/


            if (isset($_FILES['allegato']) && $_FILES['allegato']['size'] > 0) {

                require_once($PROGETTO . "/view/lib/updateFile.class.php");
                $f = new UpdateFile();
                $allegato = $f->getInfoFile($_FILES['allegato'], 50000000);

                if ($allegato != NULL) {

					$ragioneSocialeSql = "SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
					$ragioneSociale = $db->GetRow($ragioneSocialeSql,'RagioneSociale',null, 'estrazione ragione sociale visualizza_EXPO_T_Bacheca.inc Ln. 177'); 

                    $name = $allegato['name'];
                    $type = $allegato['type'];
                    $size = $allegato['size'];
                    $content = '';

                    $end = explode('.',$name);
                    $extension = end ($end);
                    $name = "Allegato$idMsg.$extension";
                    $db->Query("INSERT INTO  EXPO_T_Bacheca_Allegati VALUES($idMsg,'$name','$type','$size','$content')",null, null, "visualizza_EXPO_T_Bacheca.inc upload allegato Ln. 184");
                    
                    $rootDoc = selectRootDoc($ragioneSociale) . "/" . "$IdImpresa";
                    $dir = Config::ROOTFILES.$rootDoc."/bacheca";
                    $dirBacheca = $dir."/$idBacheca";
                    if (!is_dir($dir)){
                    	@mkdir($dir, 0775,true);
                    	@mkdir($dirBacheca,0775,true);
                    }
                    else 
					if (!is_dir($dirBacheca)){
                    	@mkdir($dirBacheca,0775,true);
                    }
                    $bachecaFile = $dirBacheca."/$name";
                    move_uploaded_file($allegato['tmp_name'],$bachecaFile);
                    chmod ($bachecaFile, 0775);
                   
                }
            }
            $query = "SELECT Nome,Cognome 
              FROM T_Anagrafica 
              WHERE Id = " . $auth->auth["uid"];
            $mittente = $db->GetRow($query, "Nome", null, "visualizza_EXPO_T_Bacheca.inc Ln.187") . " " . $db->GetRow($query, "Cognome", null, "visualizza_EXPO_T_Bacheca.inc Ln.187");

            $query = "SELECT DISTINCT oggetto,E_Mail,Nome,Cognome 
              FROM T_Anagrafica AS T
              RIGHT JOIN EXPO_T_Bacheca AS TB
              ON T.Id = TB.IdPartecipante
              WHERE TB.Id =  '$idBacheca'";
            $utente = $db->GetRow($query, "Nome", null, "visualizza_EXPO_T_Bacheca.inc Ln.194") . " " . $db->GetRow($query, "Cognome", null, "visualizza_EXPO_T_Bacheca.inc Ln.194");
            include "send_mail.inc";
            send_mail($db->GetRow($query, "E_Mail", null, "visualizza_EXPO_T_Bacheca.inc Ln.196"), $MAILSITO, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _OGG_NOT_BACHECA_), preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NOTIFICA_TRA_), true);

            $idTratativa = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$idBacheca' AND Stato = 'D'", "Id", null, "visualizza_EXPO_T_Bacheca.inc Ln.198");
            if (strcmp($idTratativa, "") != 0) {
				print "UPDATE EXPO_T_Bacheca_Trattative SET Valore = '',Stato = 'S' WHERE IdBacheca = $idTratativa";
               	$db->Query("UPDATE EXPO_T_Bacheca_Trattative SET Valore = '',Stato = 'S' WHERE Id = $idTratativa", null, "visualizza_EXPO_T_Bacheca.inc Ln.200");
            }

            echo _SEND_OK_;
        }
        $arrayRighe = array();
        $rTitolo = '<tr>
                <th scope="col" class="tdHeader">' . _MITTENTE_ . '</th>
                <th scope="col" class="tdHeader">' . _OGGETTO_ . '</th>
                <th scope="col" class="tdHeader">' . _DATA_ . '</th>
                <th scope="col" class="tdHeaderEnd">' . _AZIONE_ . '</th>
            </tr>';


        $isColor = 0;
        $query = "SELECT TB.Id AS IdT, MAX( TBM.Data ) AS DataUltimoAggiornamento, DATE_FORMAT( MAX( TBM.Data ) ,  '%d-%m-%Y %H:%i:%s' ) AS Data, Oggetto, Nome, Cognome, E_Mail, RagSoc
FROM EXPO_T_Bacheca AS TB
JOIN T_Anagrafica AS T ON TB.IdPartecipante = T.Id
JOIN EXPO_T_Bacheca_Messaggi AS TBM ON TB.Id = TBM.IdBacheca
WHERE TB.IdImpresa =  '$IdImpresa'
GROUP BY TB.Id, Oggetto, Nome, Cognome, E_Mail
ORDER BY DataUltimoAggiornamento DESC";

        if ($db->NumRows($query, null, "visualizza_EXPO_T_Bacheca.inc Ln.223") == 0) {
            ?>
            <h2><?php echo _NO_BACHECA_; ?></h2>
            <?php
        } else {

            foreach ($db->GetRows($query, null, "visualizza_EXPO_T_Bacheca.inc Ln.229") AS $rows) {

                $html = '<tr ' . getColor("lineTable", $isColor) . ' >
                   
                    <td align="left">' . $rows["Cognome"] . ' ' . $rows["Nome"] . ' - ' . $rows["RagSoc"] . ' </td>
                    <td  align="left">' . $rows["Oggetto"] . '</td>
                    <td  >' . $rows["Data"] . '</td>
                    <td  align="center"><a href="/index.phtml?Id_VMenu=' . $idPage . '&amp;azione=VIEW&Id=' . base64_encode("Id" . $rows["IdT"]) . '" class="buttonRed" draggable="false"  >' . _VISUALIZZA_ . '</a></td>
                </tr>';
                $arrayRighe[] = $html;
                $isColor++;
            }
            $evento = "javascript:visualizza('index.phtml?Id_VMenu=" . $idPage . "')";
            $nElementi = settaVar($_POST, "elementiN", "25");
            echo multiPage($rTitolo, $arrayRighe, $nElementi, $evento);
        }
    }
    ?>
</form>