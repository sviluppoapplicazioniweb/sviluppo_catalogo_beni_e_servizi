<?php
echo $azione_messaggio;
$azione_messaggio = "";
$sess->register(azione_messaggio);
page_close();
?>
<form  id="form_moduli" name="form_moduli">
    <?php
    $IdImpresa = $auth->auth['IdImpresa'];



    //require_once($PROGETTO . "/view/lib/db.class.php");
    $db = new DataBase();

    if (strcmp($azione, "UPD") == 0 || strcmp($azione, "INS") == 0) {
        $nome = "";
        $cognome = "";
        $cell = "";
        $mail = "";
        $fax = "";
        //$idUrl = "";
        if (strcmp($azione, "UPD") == 0) {
            $idUtente = base64_decode(settaVar($_GET, 'Id', ''));
            $query = "SELECT * FROM EXPO_TJ_Imprese_Utenti WHERE IdUtente = '$idUtente' AND IdImpresa = '$IdImpresa'";
            if (strcmp($db->GetRow($query, "Id",null,'delegati LN. 26'), "") == 0) {
                redirect("index.phtml?Id_VMenu=319");
                exit;
            }
            $query = "SELECT * FROM T_Anagrafica WHERE Id = $idUtente";
            $nome = $db->GetRow($query, "Nome",null,'delegati LN. 31' );
            $cognome = $db->GetRow($query, "Cognome",null,'delegati LN. 32');
            $cell = $db->GetRow($query, "Telefono",null,'delegati LN. 33');
            $mail = $db->GetRow($query, "E_Mail",null,'delegati LN. 34');
            $fax = $db->GetRow($query, "Fax",null,'delegati LN. 35');
            //$idUrl = "&Id=" . $idUtente;
            
            $query = "SELECT * FROM T_Utente WHERE Id = '$idUtente'";
            $dirittiLettura = "";
            if ($db->GetRow($query, "Id_TipoUtente",null,'delegati LN. 40') == 94){
            	$dirittiLettura = 'checked';
            }
            
            
        }
        ?>
        <input type="hidden" id="Id" name="Id" value="<?php echo $idUtente; ?>"/>
        <div style="border-bottom: 1px solid #26ACE2;border-top: 1px solid #26ACE2;padding: 10px 0px 10px;">
            <table cellpadding="5">
                <tr>
                    <td><strong><?php echo _NOME_; ?>:*</strong></td>
                    <td><strong><?php echo _TELEFONO_; ?>:*</strong></td>
                </tr>
                <tr>
                    <td><input type = "text" id="Nome" name = "Nome" size = "50" maxlength = "100" value = "<?php echo $nome; ?>"/></td>
                    <td><input type = "text" id = "Telefono" name = "Telefono" size = "30" maxlength = "40" value = "<?php echo $cell; ?>"/></td>
                </tr>
                <tr>
                    <td><strong><?php echo _COGNOME_; ?>:*</strong></td>
                    <td><strong>Fax:</strong></td>
                </tr>
                <tr>
                    <td><input type = "text" id = "Cognome" name = "Cognome" size = "50" maxlength = "100" value = "<?php echo $cognome; ?>"/></td>
                    <td><input type = "text" id="Fax" name="Fax" size = "30" maxlength = "30" value = "<?php echo $fax; ?>"/></td>
                </tr>
                <tr>
                    <td><strong><?php echo _EMAIL_; ?>:*</strong></td>
                    <td><strong><?php echo _DELEGATO_GUEST_; ?>:</strong></td>
                </tr>
                <tr>
                    <td><input type = "text" id = "E_Mail" name = "E_Mail" size = "50" maxlength = "100" value = "<?php echo $mail; ?>"/></td>
                    <td><input type="checkbox" id = "Delegato_Guest" name = "Delegato_Guest" <?php print $dirittiLettura;?>></td>
                </tr>
            </table>

        </div>

        <br>
        <p style="text-align:right"><input type="button" onclick="javascript:salva(document.form_moduli, 'index.phtml?Id_VMenu=300&amp;azione=SAL&option=<?php echo $azione; ?>')" value="<?php echo _SALVA_; ?>"/></p>
        <?php
    } else {



        if (strcmp($azione, "SAL") == 0) {
            $idUtente = settaVar($_POST, "Id", "");
            $option = settaVar($_GET, "option", "");

            $nome = settaVar($_POST, "Nome", "");
            $cognome = settaVar($_POST, "Cognome", "");
            $tel = settaVar($_POST, "Telefono", "");
            $mail = settaVar($_POST, "E_Mail", "");
            $fax = settaVar($_POST, "Fax", "");
            $delegatoGuest = settaVar($_POST, "Delegato_Guest", "");

            $row = $db->GetRow("SELECT Id_TipoUtente,T1.Id AS IdU
            FROM T_Anagrafica AS T1
            LEFT JOIN T_Utente AS T2 ON T1.Id = T2.Id
            WHERE T1.E_Mail =  '$mail'",null ,null,'delegati LN. 99');

            if (strcmp($row['IdU'], "") != 0 && strcmp($option, "INS") == 0) {
                $idUtente = $row['IdU'];
                $row = $db->GetRow("SELECT *
            FROM EXPO_TJ_Imprese_Utenti
            WHERE IdImpresa =  '$IdImpresa' AND IdUtente = '$idUtente'", 'Id',null,null,'delegati LN. 105');
                if (strcmp($row, "") != 0) {
                    $azione_messaggio = "<h2>" . _UTENTE_PRESENTE_ . "</h2>";
                    $sess->register(azione_messaggio);
                    page_close();
                    redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
                    exit;
                } else {
                    //$idImpresaUtente = $db->GetRow("SELECT MAX(Id) AS Id FROM EXPO_TJ_Imprese_Utenti", "Id") + 1;
                    $db->Query("INSERT INTO EXPO_TJ_Imprese_Utenti VALUES((SELECT COALESCE(MAX(TJU.Id),0)+1 AS Id FROM EXPO_TJ_Imprese_Utenti AS TJU),'$IdImpresa','$idUtente')",null,"creazione delegati ln 114");
                    $utente = $nome . ' ' . $cognome;
                    $lg = $db->GetRow("SELECT CONCAT(Nome, ' ', Cognome) AS lg FROM T_Anagrafica WHERE Id = $uid", "lg");
                    $mail = $db->GetRow("SELECT E_Mail FROM T_Anagrafica WHERE Id = $idUtente", "E_Mail");
                    $RagSoc = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale", null, "visualizza_EXPO_TJ_Delegati.inc Ln.118");
                    //MAIL AGGIUNTO AD UN AIMPREA
                    include "send_mail.inc";
                    send_mail($mail, $MAILSITO, _OGG_AGG_I_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_AGG_I_), true);
                    $azione_messaggio = "<h2>" . _UTENTE_INS_ . "</h2>";
                    $sess->register(azione_messaggio);
                    page_close();
                    redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
                    exit;
                }
            } else {
                if (strcmp($idUtente, "") == 0) {
                    $RagioneSociale = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale", null, "visualizza_EXPO_TJ_Delegati.inc Ln.130");
                    $username = $mail;
                    $pass = passwordCasuale(8);
			
                    //$idUtente = $db->GetRow("SELECT MAX(Id) AS id FROM T_Anagrafica", "id") + 1;
                    $sqlMaxId = "(SELECT @idUtente := COALESCE(MAX(TA.Id),0)+1 AS Id FROM T_Anagrafica AS TA)";
                    $db->Query("INSERT INTO T_Anagrafica VALUES($sqlMaxId,'".mysql_escape_string(htmlspecialchars($nome))."','".mysql_escape_string(htmlspecialchars($cognome))."','$tel','$tel','$fax','$mail','".mysql_escape_string(htmlspecialchars($RagioneSociale))."','','','',0,'',0,'','','','','','',NOW(),'$username','$pass','2015-12-31','D')", null, "visualizza_EXPO_TJ_Delegati.inc Ln.135");
                    $idUtente = $db->GetRow("SELECT @idUtente as idUtente","idUtente",null,"Estrazione max id tj delegati");
                    $tipoDelegato = 97;
                    if ($delegatoGuest){
                    	$tipoDelegato = 94;
                    }
                    $db->Query("INSERT INTO T_Utente VALUES($idUtente,NULL,0,0,NULL,0,'$tipoDelegato','|',0,'|','|','','','|','',1,'|','',0,'',0)", null, "visualizza_EXPO_TJ_Delegati.inc Ln.141");


                    
                    $db->Query("INSERT INTO EXPO_TJ_Imprese_Utenti VALUES((SELECT COALESCE(MAX(TJU.Id),0)+1 AS Id FROM EXPO_TJ_Imprese_Utenti AS TJU),'$IdImpresa','$idUtente')", null, "visualizza_EXPO_TJ_Delegati.inc Ln.144");
					
                    include_once "send_mail.inc";
                    $utente = $nome . ' ' . $cognome;
                    $passUrl = urlencode(base64_encode($pass));
                    $link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $mail . "&cyph=" . md5($mail . $keySito) . "&Lang2=IT&action=INS";

                    send_mail($mail, $MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true);

                    $azione_messaggio = "<h2>" . _UTENTE_INS_ . "</h2>";
                    $sess->register(azione_messaggio);
                    page_close();
                    redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
                    exit;
                } else {
                    $mailControllo = $db->GetRow("SELECT * FROM T_Anagrafica WHERE Id != '$idUtente' AND E_Mail = '$mail'", "Id", null, "visualizza_EXPO_TJ_Delegati.inc Ln.159");
                    if (strcasecmp($mailControllo, "") == 0) {
                        $query = "UPDATE T_Anagrafica SET Nome ='".mysql_escape_string(htmlspecialchars($nome))."',Cognome ='".mysql_escape_string(htmlspecialchars($cognome))."', Telefono='$tel',Cellulare='$tel', Fax='$fax',E_Mail='$mail' WHERE Id = $idUtente";
                        $db->Query($query, null, "visualizza_EXPO_TJ_Delegati.inc Ln.162");
                        $tipoDelegato = 97;
                        if ($delegatoGuest){
                        	$tipoDelegato = 94;
                        }
                        $query = "UPDATE T_Utente SET Id_TipoUtente=$tipoDelegato WHERE Id = $idUtente";
                        $db->Query($query, null, "visualizza_EXPO_TJ_Delegati.inc Ln.168");
                        $azione_messaggio = "<h2>" . _UTENTE_UPD_ . "</h2>";
                        $sess->register(azione_messaggio);
                        page_close();
                        redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
                        exit;
                    } else {
                        $azione_messaggio = "<h2>" . _EMAIL_PRESENTE_ . "</h2>";
                        $sess->register(azione_messaggio);
                        page_close();
                        redirect("index.phtml?Id_VMenu=" . $Id_VMenu . '&azione=UPD&Id=' . $idUtente);
                        exit;
                    }
                }
            }
        } elseif (strcmp($azione, "DEL") == 0) {

            $idUtente = base64_decode(settaVar($_GET, 'Id', ''));
            $query = "SELECT * FROM EXPO_TJ_Imprese_Utenti WHERE IdUtente = '$idUtente' AND IdImpresa = '$IdImpresa'";
            if (strcmp($db->GetRow($query, "Id", null, "visualizza_EXPO_TJ_Delegati.inc Ln.187"), "") == 0) {
                redirect("index.phtml?Id_VMenu=319");
                exit;
            }
            $lg = $db->GetRow("SELECT CONCAT(Nome, ' ', Cognome) AS lg FROM T_Anagrafica WHERE Id = $uid", "lg", null, "visualizza_EXPO_TJ_Delegati.inc Ln.191");
            $mail = $db->GetRow("SELECT E_Mail FROM T_Anagrafica WHERE Id = $idUtente", "E_Mail", null, "visualizza_EXPO_TJ_Delegati.inc Ln.192");
            $RagSoc = $db->GetRow("SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale", null, "visualizza_EXPO_TJ_Delegati.inc Ln.193");
            $nId = $db->GetRow("SELECT COUNT(Id) AS num FROM EXPO_TJ_Imprese_Utenti WHERE IdUtente = $idUtente", "num", null, "visualizza_EXPO_TJ_Delegati.inc Ln.193");
            include "send_mail.inc";
            if ($nId == 1) {
                $db->Query("DELETE FROM T_Utente WHERE id = $idUtente", null, "visualizza_EXPO_TJ_Delegati.inc Ln.197");
                $db->Query("DELETE FROM T_Anagrafica WHERE id = $idUtente", null, "visualizza_EXPO_TJ_Delegati.inc Ln.198");
                send_mail($mail, $MAILSITO, _OGG_DELETE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_DEL_REG_DEF_), true);
            } else {
                send_mail($mail, $MAILSITO, _OGG_DELETE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_DEL_REG_), true);
            }

            $db->Query("DELETE FROM EXPO_TJ_Imprese_Utenti WHERE IdUtente = $idUtente AND IdImpresa = $IdImpresa", null, "visualizza_EXPO_TJ_Delegati.inc Ln.204");

            $azione_messaggio = "<h2>" . _UTENTE_DEL_ . "</h2>";
            $sess->register(azione_messaggio);
            page_close();
            redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
            exit;
        } elseif (strcmp($azione, "RLN") == 0) {
            $idUtente = base64_decode(settaVar($_GET, 'Id', ''));

            $query = "SELECT * FROM EXPO_TJ_Imprese_Utenti WHERE IdUtente = '$idUtente' AND IdImpresa = '$IdImpresa'";
            if (strcmp($db->GetRow($query, "Id", null, "visualizza_EXPO_TJ_Delegati.inc Ln.215"), "") == 0) {
                redirect("index.phtml?Id_VMenu=319");
                exit;
            }
            $query = "SELECT * FROM T_Anagrafica WHERE Id = $idUtente";
            $mail = $db->GetRow($query, "E_Mail", null, "visualizza_EXPO_TJ_Delegati.inc Ln.220");
            $pass = $db->GetRow($query, "Password", null, "visualizza_EXPO_TJ_Delegati.inc Ln.221");
            $username = $db->GetRow($query, "Login", null, "visualizza_EXPO_TJ_Delegati.inc Ln.222");
            $passUrl = urlencode(base64_encode($pass));
            include "send_mail.inc";
            $utente = $db->GetRow($query, "Nome", null, "visualizza_EXPO_TJ_Delegati.inc Ln.225") . ' ' . $db->GetRow($query, "Cognome", null, "visualizza_EXPO_TJ_Delegati.inc Ln.225");

            $link = "http://" . $_SERVER['HTTP_HOST'] . "/index.phtml?option=" . $passUrl . "&username=" . $username . "&cyph=" . md5($username . $keySito) . "&Lang2=IT&action=INS";

            $azione_messaggio = "<h2>" . _UTENTE_RLN_ . "</h2>";
            $sess->register(azione_messaggio);
            page_close();
            send_mail($mail, $MAILSITO, _OGG_REGISTRAZIONE_, preg_replace("/\{%(\w+)%\}/e", "\$\\1", _MSG_NEW_REG_), true);
            redirect("index.phtml?Id_VMenu=" . $Id_VMenu);
            exit;
        }
        ?>
        <p>
            <a class="buttonRed"  draggable="false"  href="/index.phtml?Id_VMenu=300&amp;azione=INS"><?php echo _AGGIUNGI_; ?></a>
        </p> 
        <input type = "hidden" name = "IdImp" id = "IdImp" value = "<?php echo $IdImpresa; ?>"/>
        <?php
        $arrayRighe = array();
        $rTitolo = '<tr>
            <th scope="col" class="tdHeader">' . _COGNOME_ . '</th>
            <th scope="col" class="tdHeader">' . _NOME_ . '</th>
            <th scope="col" class="tdHeader">' . _EMAIL_ . '</th>
            <th scope="col" class="tdHeaderEnd">' . _AZIONE_ . '</th>
        </tr> ';


// Scrivo la query e...
        $isColor = 0;
        $idTipoUtente = $auth->auth["utype"];
        $query = "SELECT T1.Id AS idA,Nome,Cognome,E_Mail,Stato_Record
    FROM (T_Anagrafica AS T1 LEFT JOIN T_Utente AS T2 ON T1.Id = T2.Id )
    LEFT JOIN EXPO_TJ_Imprese_Utenti AS T3 ON T1.Id = T3.IdUtente
    WHERE T3.IdImpresa = '$IdImpresa' AND T2.Id_TipoUtente != '$idTipoUtente' and T3.IdUtente != '7' and T3.IdUtente != '6' and T3.IdUtente != '5'
    ORDER BY Cognome,Nome";
        if ($db->NumRows($query, null, "visualizza_EXPO_TJ_Delegati.inc Ln.259") == 0) {
            ?>
            <h2><?php echo _NO_DELEGATI_; ?></h2>
            <?php
        } else {
            foreach ($db->GetRows($query, null, "visualizza_EXPO_TJ_Delegati.inc Ln.264") AS $row) {

                $html = '<tr ' . getColor("lineTable", $isColor) . '>
                    <td  align="left"><a draggable="false" href="/index.phtml?Id_VMenu=300&amp;azione=UPD&Id=' . base64_encode($row["idA"]) . '">' . $row["Cognome"] . ' </a></td>
                    <td  align="left"> ' . $row["Nome"] . '</td>
                    <td  align="left"> ' . $row["E_Mail"] . '</td>
                    <td  align="center">
                        <a class="buttonRed" draggable="false"  href="/index.phtml?Id_VMenu=300&amp;azione=DEL&Id= ' . base64_encode($row["idA"]) . '"> ' . _ELIMINA_ . '</a>';
                if (strcmp($row["Stato_Record"], "D") == 0) {
                    $html .= '<a class="buttonRed" draggable="false"  href="/index.phtml?Id_VMenu=300&amp;azione=RLN&Id= ' . base64_encode($row["idA"]) . '"> ' . _G_LINK_ . '</a>';
                }
                $html .= '</td>
                </tr>';
                $arrayRighe[] = $html;
                $isColor++;
            }

            $evento = "javascript:visualizza('index.phtml?Id_VMenu=" . $Id_VMenu . "')";
            $nElementi = settaVar($_POST, "elementiN", "25");
            echo multiPage($rTitolo, $arrayRighe, $nElementi, $evento);
        }
    }
    ?>
</form>