<?php
require_once($PROGETTO . "/view/lib/getReport.class.php");
$db = new DataBase();

//QUERY PER ACCESSI ALLA VETRINA
$dataPeriodo = "";
if (isset($_POST['dataInizio']) && strcmp($_POST['dataInizio'], "") != 0 && isset($_POST['dataFine']) && strcmp($_POST['dataFine'], "") != 0) {
    $dataInizioInput = $_POST['dataInizio'];
    $dataFineInput = $_POST['dataFine'];
    $dataInizio = date_format(date_create($_POST['dataInizio']), "Y-m-d");
    $dataFine = date_format(date_create($_POST['dataFine']), "Y-m-d");
} elseif (isset($_POST['periodo']) && strcmp($_POST['periodo'], "") != 0) {
    $dataInizio = $_POST['periodo'];
    $dataPeriodo = $_POST['periodo'];
    $dataFine = date("Y-m-d");
} else {
    $dataInizio = "2014-01-01";
    $dataFine = date("Y-m-d");
}


$dataTextInizio = date_format(date_create($dataInizio), 'd-m-Y');
$dataTextFine = date_format(date_create($dataFine), 'd-m-Y');

$r = new GetReport();
$arrayLogs = $r->getReportVetrina($dataInizio, $dataFine);

$MAX_LEN_NAME = 50;
$i = 0;
$numTab = 0;
?>

<!-- PERIODO -->
<form id="form_filtro" name="form_filtro" >
	<input type="hidden" id="urlCSV" value="/downloadCSV.php?tab=login" />
	<p>
		<b style="margin-right: 36px">Periodo</b>
        <select id="periodo" name="periodo" style="width: 346px; margin-right: 20px;">
            <option value="" >Seleziona...</option>
            <?php
            $arrayPeriodo = array(1, 3, 6, 12);
            foreach ($arrayPeriodo AS $i) {

                $data = date('Y-m-d', strtotime('-' . $i . ' months'));
                $periodo = "Ultimi " . $i . " mesi";
                if ($i == 1) {
                    $periodo = "Ultimo mese";
                } elseif ($i == 12) {
                    $periodo = "Ultimo anno";
                }
                $selected = "";
                if (strcmp($dataPeriodo, $data) == 0) {
                    $selected = "selected";
                }
                ?>
                <option value="<?php echo $data; ?>" <?php echo $selected; ?>><?php echo $periodo; ?></option>
            <?php } ?>
        </select>
        <input id="data2" type="button" value="Visualizza" />
    </p>
    <p>
        <span>
	        <b>Intervallo dal</b> 
	        <input type="text" id="dataInizio" name="dataInizio" value="<?php echo $dataInizioInput; ?>" />
	        <b>al</b>
	        <input type="text" id="dataFine" name="dataFine" value="<?php echo $dataFineInput; ?>" />
        </span>
        <input id="data" type="button" value="Visualizza"  />
    </p>

</form>

<div class="tsc_tabs_type_3_container" >
    <ul id="tsc_tabs_type_3">
        <li><a href="#" name="#gtab1">Login</a></li>
        <li><a href="#" name="#gtab2">Visite Imprese</a></li>
        <li><a href="#" name="#gtab3">Visite Prodotti</a></li>
    </ul>
    <div id="tsc_tabs_type_3_content" >
        <!-- Tab 1 Start -->
        <div id="gtab1">
            <br>
            <h3>Le visite totali per il periodo che va da <?php echo $dataTextInizio; ?> a <?php echo $dataTextFine; ?> sono <?php echo $arrayLogs['totVisite']; ?></h3>

            <?php
            foreach ($arrayLogs AS $arrayLog) {
                if (is_array($arrayLog)) {
                    ?> 
                    <div class="alberoCategorie" style="width: 97%">
                        <div class="collapsible" id="section<?php echo $i; ?>">
                            <?php if ($arrayLog['totaliLoginMese'] > 0) { ?> <span></span> <?php } ?>
                            <?php echo $arrayLog['periodo']; ?>
                            ( <?php echo $arrayLog['totaliLoginMese']; ?> Visite )
                        </div> 
                        <?php if ($arrayLog['totaliLoginMese'] > 0) { ?>
                            <div class="container">
                                <div class="content">
                                    <div class="alberoCategorie" id="" >
                                        <table name="tabella<?php echo $numTab; ?>" class="tablesorter">
                                            <thead> 
                                                <tr style="font-weight: bold;text-align: center">
                                                    <th style="text-align: center">Paese</th>
                                                    <th style="text-align: center">Visite</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($arrayLog['login'] AS $nome => $visite) { ?>
                                                    <tr style="text-align: center">
                                                        <td> <?php echo $nome; ?></td>
                                                        <td > <?php echo $visite; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                        $numTab++;
                        ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

        <div id="gtab2">
            <br>
            <h3>Le visite totali per il periodo che va da <?php echo $dataTextInizio; ?> a <?php echo $dataTextFine; ?> sono <?php echo $arrayLogs['totVisiteImprese']; ?></h3>

            <?php
            foreach ($arrayLogs AS $arrayLog) {
                if (is_array($arrayLog)) {
                    ?>  
                    <div class="alberoCategorie" style="width: 97%">
                        <div class="collapsible" id="section<?php echo $i; ?>">
                            <?php if ($arrayLog['totaliVisiteMeseImprese'] > 0) { ?> <span></span> <?php } ?>
                            <?php echo $arrayLog['periodo']; ?>
                            ( <?php echo $arrayLog['totaliVisiteMeseImprese']; ?> Visite )
                        </div> 
                        <?php if ($arrayLog['totaliVisiteMeseImprese'] > 0) { ?>
                            <div class="container">
                                <div class="content">
                                    <div class="alberoCategorie">

                                        <?php
                                        foreach ($arrayLog['imprese'] AS $idImpresa => $arrayImprese) {
                                            $nomeImpresa = $arrayLog['imprese'][$idImpresa]['nome'];
                                            if (strcmp($nomeImpresa, "") != 0) {

                                                if (strlen($nomeImpresa) > $MAX_LEN_NAME) {
                                                    $nomeImpresa = substr($nomeImpresa, 0, $MAX_LEN_NAME) . "...";
                                                }
                                                ?>
                                                <div class="collapsible" id="section<?php echo $i; ?>">
                                                    <span></span>
                                                    <?php echo $nomeImpresa; ?>
                                                    ( <?php echo $arrayLog['imprese'][$idImpresa]['vistite']; ?> Visite )
                                                </div>
                                                <div class="container">
                                                    <div class="content">
                                                        <br>
                                                        <table name="tabella<?php echo $numTab; ?>" class="tablesorter">
                                                            <thead> 
                                                                <tr style="font-weight: bold;text-align: center">
                                                                    <th style="text-align: center">Paese</th>
                                                                    <th style="text-align: center">Visite</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $arrayListaPesi = $arrayLog['imprese'][$idImpresa]['paesi'];
                                                                ksort($arrayListaPesi);
                                                                foreach ($arrayListaPesi AS $nome => $visite) {
                                                                    ?>
                                                                    <tr style="text-align: center">
                                                                        <td> <?php echo $nome; ?></td>
                                                                        <td > <?php echo $visite; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                        ?>
                    </div>
                    <?php
                }
            }
            ?>


        </div>
        <div id="gtab3">
            <br>
            <h3>Le visite totali per il periodo che va da <?php echo $dataTextInizio; ?> a <?php echo $dataTextFine; ?> sono <?php echo $arrayLogs['totVisiteProdotti']; ?></h3>

            <?php
            foreach ($arrayLogs AS $arrayLog) {
                if (is_array($arrayLog)) {
                    ?>  

                    <div class="alberoCategorie" style="width: 97%">
                        <div class="collapsible" id="section<?php echo $i; ?>">
                            <?php if ($arrayLog['totaliVisiteMeseProdotti'] > 0) { ?> <span></span> <?php } ?>
                            <?php echo $arrayLog['periodo']; ?>
                            ( <?php echo $arrayLog['totaliVisiteMeseProdotti']; ?> Visite )
                        </div> 
                        <?php if ($arrayLog['totaliVisiteMeseProdotti'] > 0) { ?>
                            <div class="container">
                                <div class="content">
                                    <div class="alberoCategorie">

                                        <?php
                                        foreach ($arrayLog['prodotti'] AS $idProdotto => $arrayImprese) {
                                            $nomeImpresa = $arrayLog['prodotti'][$idProdotto]['impresa'];
                                            $nomeProdotto = $arrayLog['prodotti'][$idProdotto]['nome'];
                                            if (strcmp($nomeImpresa, "") != 0) {

                                                if (strlen($nomeImpresa) > $MAX_LEN_NAME) {
                                                    $nomeImpresa = substr($nomeImpresa, 0, $MAX_LEN_NAME) . "...";
                                                }
                                                ?>
                                                <div class="collapsible" id="section<?php echo $i; ?>">
                                                    <span></span>
                                                    <?php echo $nomeProdotto; ?> - 
                                                    <?php echo $nomeImpresa; ?>
                                                    ( <?php echo $arrayLog['prodotti'][$idProdotto]['vistite']; ?> Visite )
                                                </div>
                                                <div class="container">
                                                    <div class="content">
                                                        <br>
                                                        <table name="tabella<?php echo $numTab; ?>" class="tablesorter">
                                                            <thead> 
                                                                <tr style="font-weight: bold;text-align: center">
                                                                    <th style="text-align: center">Paese</th>
                                                                    <th style="text-align: center">Visite</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $arrayListaPesi = $arrayLog['prodotti'][$idProdotto]['paesi'];
                                                                ksort($arrayListaPesi);
                                                                foreach ($arrayListaPesi AS $nome => $visite) {
                                                                    ?>
                                                                    <tr style="text-align: center">
                                                                        <td> <?php echo $nome; ?></td>
                                                                        <td > <?php echo $visite; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                        ?>
                    </div>
                    <?php
                }
            }
            ?>



        </div>
    </div>
</div>
<p style="min-height: 5px;">
	<input type="button" id="csv" value="Download CSV">
</p>
    
	
<!-- DC Flat Tabs End -->
<script>

    /* Tabs 3 */
    function resetTabs() {
        $("#gtab1,#gtab2,#gtab3").hide(); //Hide all content
        $("#tsc_tabs_type_3 a").attr("id", ""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0, 4); // For the above example, myUrlTabName = #tab


    (function() {
        $("#gtab1,#gtab2,#gtab3").hide(); // Initially hide all content
        $("#tsc_tabs_type_3 li:first a").attr("id", "current"); // Activate first tab
        $("#tsc_tabs_type_3_content div:first").fadeIn(); // Show first tab content

        $("#tsc_tabs_type_3 a").on("click", function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current") { //detection for current tab
                return
            }
            else {
                resetTabs();
                $(this).attr("id", "current"); // Activate this
                $($(this).attr('name')).fadeIn(); // Show content for current tab
                var tab = "";
                if ($(this).attr('name') === '#gtab1') {
                    tab = "login";
                } else if ($(this).attr('name') === '#gtab2') {
                    tab = "visiteImprese";
                } else if ($(this).attr('name') === '#gtab3') {
                    tab = "visiteProdotti";
                }
                $("#urlCSV").val('/downloadCSV.php?tab=' + tab);
            }
        });
        for (i = 1; i <= $("#tsc_tabs_type_3 li").length; i++) {
            if (myUrlTab == myUrlTabName + i) {
                resetTabs();
                $("a[name='" + myUrlTab + "']").attr("id", "current"); // Activate url tab
                $(myUrlTab).fadeIn(); // Show url tab content 

            }
        }
    })()
</script>
