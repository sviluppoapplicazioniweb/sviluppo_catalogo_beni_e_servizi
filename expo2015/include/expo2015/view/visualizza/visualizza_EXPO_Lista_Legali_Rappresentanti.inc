<?php 

require_once("configuration.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/gestioneListeExt.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");

$gestioneListe = new GestioneListeExt("legaliRappresentanti",$uid);

$crea = settaVar($_POST, 'CreaLR', 0);
if ($crea == 1){
	$gestioneListe->creaLR();
}

if ($_FILES){
	$gestioneListe->caricaCSV($_FILES["lrCSV"]);

	//$gestioneListe->stampa();
}
?>
<div id="formUpload">
	<form id="upload" action="index.phtml?Id_VMenu=511" method="post" enctype="multipart/form-data">
		<input type="hidden" name="typeCSV" id="typeCSV" value="legaliRappresentati">
		<input type="file" name="lrCSV" id="auto"><br>
		<input id="carica" style="display:none; float: right;" class="buttonRed" type="submit" value="CARICA FILE" />
	</form>
	<?php if ($gestioneListe->numLegaliDaCreare() > 0 ){?>
	<form id="formCreaLR" action="index.phtml?Id_VMenu=511" method="post">
		<input type="hidden" name="CreaLR" id="CreaLR" value="1">
		<input style="float: right;" class="buttonRed" type="submit" value="CREA UTENZE" />	
	</form>
	<?php }?>
	<!--
	<div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
    </div>
    <div id="message"></div>-->
</div>


<?php


if ($gestioneListe->numLegaliPresenti() > 0){
?>
<div id="elencoTabella">
	<?php 
	$gestioneListe->errorCSV();
	$gestioneListe->stampaElenco();
	?>
</div>
<?php  }
else{
 print '<h3 style="margin-left:18px;">Nessun legale rappresentante caricato nel sistema</h3>';
}

?>

