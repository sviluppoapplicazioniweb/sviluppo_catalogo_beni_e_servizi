<?php
include $PROGETTO."/view/languages/IT.inc";

$campis="";
if ($tabelles!="")
	$tabelles.=",".$Esterno[$nome].$nome;
else
	$tabelles=$Esterno[$nome].$nome;

if ($DEBUG) echo "<!--WHEREFILTRI $wherefiltri -->\n";
$wheres="".$wherefiltri;
$leftjoin="";
$tdcampi=0;

$orders="";
echo "<table id=\"tabella_elenchi\" summary=\"Elenco\">\n";
echo "<tr>\n";

reset($tabella);
$conta_tab=1;

if ($auth->auth["utype"]=="")  
    $utype2=99;
else 
    $utype2=$auth->auth["utype"];

while (list ($key, $val) = each ($tabella)) {
    
    $temp=explode("|",$key);
    $key=$temp[0];
    $utenti_abilitati=$temp[1];
    $azione=$temp[2];
    $id_tipoutente2=$temp[3];
    $abilitato=strpos($utenti_abilitati,";".$utype2.";");
    $da_visualizzare=strpos($id_tipoutente2,";".$Id_TipoUtente.";");

    if (( $utenti_abilitati =="" || !($abilitato===false)) && ( $Id_TipoUtente =="" || $id_tipoutente2 =="" || !($da_visualizzare===false))) {

        $campo= explode("|",$val);

        if ($campo[$Parametri["VisTab"]]=="S" || $campo[$Parametri["VisTab"]]=="I" || $campo[$Parametri["VisTab"]]=="F" || $campo[$Parametri["VisTab"]]=="H") {
            
            $ordina1="";
            
            if ($campo[$Parametri["VisTab"]]=="I" || $campo[$Parametri["VisTab"]]=="H")
                $chiave=$key;
                
            if ($campo[$Parametri["TipoCampo"]]=="SELECT") {
                
                $tmptab=explode(";",$campo[$Parametri["Parametro4"]]);
                $nome2=$tmptab[0];
                $campo1=$tmptab[2];
                $campo2=$tmptab[1];
                $tmptab=explode(";",$campo[$Parametri["Parametro1"]]);

                if ($campo[$Parametri["VisTab"]]!="F") {

                    $asjoin="";
                    $postab=strpos($campis,$tmptab[0].".".$tmptab[2]);
                    
                    if ($postab===false) {
                        if ($campis!="") $campis .= ",";
                        $campis .= $tmptab[0].".".$tmptab[2];
                        $leftjoin.=" LEFT JOIN ".$tmptab[0]." ON ".$Esterno[$nome].$nome.".".$key."=".$tmptab[0].".".$tmptab[1];
                        
                    } else {
                        
                        if ($campis!="") $campis .= ",";
                        $leftjoin=" LEFT JOIN ".$tmptab[0]." AS ";
                        $asjoin=$tmptab[0]."_".$conta_tab;
                        $asjoin=str_replace(".","_",$asjoin);
                        $leftjoin.=asjoin." ON ".$Esterno[$nome].$nome.".".$key."=".$asjoin.".".$tmptab[1];
                        $conta_tab++;
                        $campis .= $asjoin.".".$tmptab[2];

                    }
                    
                    $tdcampi++;

                    if ($ordina1!="") $ordina1.=", ";
                    {
                            if ($asjoin!="")
                                    $ordina1=$asjoin.".".$tmptab[2];
                            else
                                    $ordina1.=$tmptab[0].".".$tmptab[2];
                    }
                    
                }

                $postab=strpos($tabelles,$nome2);
                
                if ($postab===false)
                        $tabelles .= ",".$nome2;
                else {
                    $nome2.="_".$conta_tab;
                    $conta_tab++;
                }

                if ($$key!="") {
                    
                    if ($wheres!="") $wheres .= " and ";
                    $wheres.=$nome2.".".$key."='".$$key."'";
                    $wheres.=" and ".$nome2.".".$campo2."='".$Esterno[$nome].$nome.".".$campo1."'";
                    
                }

            } else {
                
                if ($campo[$Parametri["VisTab"]]!="F") {
                    
                    if ($campis!="") $campis .= ",";
                    $campis .= $Esterno[$nome].$nome.".".$key;
                    $tdcampi++;
                    
                }
                
                $ordina1=$key;

                if ($$key!="") {
                    
                    if ($wheres!="") $wheres .= " and ";
                    $wheres.=$Esterno[$nome].$nome.".".$key."='".$$key."'";
                    
                }

            }

        }

        $condiz=$campo[$Parametri["Parametro6"]];

        if ($condiz=="" || ($condiz!="" && (eval ("if (".$condiz.") return 1; else return 0;")))) {

            if ($campo[$Parametri["VisTab"]]=="S" || $campo[$Parametri["VisTab"]]=="I" ) {

                $col = "c".$conta_tab."";

                if(strcmp($campo[$Parametri['Label']], _FAQ_DOMANDA_) == 0) {

                    if($utype == 1 || $utype == 95)
                        $larghezza = "60%";
                    else
                        $larghezza = "80%";

                }

                if(strcmp($campo[$Parametri['Label']], _FAQ_PUBBLICATA_) == 0)
                        $larghezza = "10%";

                echo "<th scope=\"col\" class=\"tdHeader\" style=\" width: ".$larghezza."; text-align: center;\">\n";
                echo "<a href=\"/index.phtml?tmpl=".$tmpl."&amp;pagina=elenchi&amp;nome=".$nome;

                if ($ordina==$ordina1)
                        echo "&amp;ordina=".$ordina1."%20DESC ";
                else
                        echo "&amp;ordina=".$ordina1;

                echo "\">".$campo[$Parametri["Label"]]."</a>\n</th>\n";
               
            }

        }
    }
    
    $conta_tab++;
}

if ($auth->auth["utype"]=="")
	$utype=99;
else
	$utype=$auth->auth["utype"];

if(isset($Azioni[$nome][$utype]) && strlen($Azioni[$nome][$utype])>1)
	$AzArray=explode("|",$Azioni[$nome][$utype]);
else
	$AzArray=explode("|",  $Azioni[$nome]);

if ($AzArray[1]!="")
    echo "<th scope=\"col\" class=\"tdHeader\" style=\" width: 10%; text-align: center;\">"._FAQ_AZIONE_."</th>\n";

echo "</tr>\n";

$db_tab = new DB_CedCamCMS;
$db_gen = new DB_CedCamCMS;

$SQLconta="select distinct ";
if(isset($Chiave[$nome]))
	$SQLconta.=$nome.".".$Chiave[$nome];
else
	$SQLconta.=$campis;


$SQLconta.=" from ".$tabelles.$tabelle_filtri;
if ($leftjoin!="") $SQLconta.= $leftjoin;
if ($wheres!="") $SQLconta.= " where ".$wheres;

if ($iniz!="" and $campoiniz!="")
{
	if ($wheres!="")
            $SQLconta.= " and ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
	else
            $SQLconta.= " where ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
}

if($utype !=  1 && $utype != 95 && $nome == 'FAQ_T_Faq') {
    
	if ($wheres!="")
            $SQLconta.= " and Stato_Record <> 'N' ";
	else
            $SQLconta.= " where Stato_Record <> 'N' ";  
    
}

if ($db_tab->query($SQLconta))
{
	$db_tab->next_record();
	$estratti=$db_tab->num_rows();
}
else
	if ($DEBUG) echo "Errore SQLnew: ".$SQLconta."\n";
	
$SQLnew="select distinct ".$campis. " from ".$tabelles.$tabelle_filtri;
if ($leftjoin!="") $SQLnew.= $leftjoin;
if ($wheres!="") $SQLnew.= " where ".$wheres;

if ($iniz!="" and $campoiniz!="") {
    if ($wheres!="")
            $SQLnew.= " and ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
    else
            $SQLnew.= " where ".$Esterno[$nome].$nome.".".$campoiniz." like '".$iniz."%'";
}

if($utype != 1 && $utype != 95) {
    
    if ($wheres!="")
        $SQLnew.= " and Stato_Record <> 'N'";
    else
        $SQLnew.= " where Stato_Record <> 'N'";
    
}

if ($ordina=="" && $Ordina[$nome]!="") {
    
    $SQLnew.= " order by ".$Ordina[$nome];
    $ordina1=$Ordina[$nome];
    
} else
	if ($ordina!="")
		$SQLnew.= " order by ".$ordina;
	elseif ($ordina1!="")
		$SQLnew.= " order by ".$ordina1;

$page_tot=ceil($estratti/$RESFORPAGE);

if($p>$page_tot) {
    
    $p=0;
    $page_elenchi=$p;
    $sess->register(page_elenchi);		
	
}

if($p==0) {
    $p=1;
    $lim_inf=0;
} else
    $lim_inf=$RESFORPAGE*($p-1);

$SQLnew.=" limit ".$lim_inf.",".$RESFORPAGE;

$numeroRighe = $db_tab->num_rows($SQLnew);

if($numeroRighe > 0) {

    if ($db_tab->query($SQLnew)) {

        $y=0;

        while($db_tab->next_record())  {

            $bgc = ( $y % 2 ? 'tdDark' : 'tdLight' );

            echo "<tr>\n";

            for ($i=0;$i<$tdcampi;$i++) {

                if($utype==1 || $utype == 95) {

                    $align=($i==2 ? "center" : "left");

                    if ($i!=0) {

                        echo "<td scope=\"row\" class=\"".$bgc."\" align=\"".$align."\">\n";

                            $db_tab->f($i);
 
                            if($i==2) {
                                
                                if($db_tab->f($i) == 'Y') {
                                    
                                    echo 'SI';
                                    $pubblicare = 0;
                                    $offline = 1;
                                    
                                } else {
                                    
                                    echo 'NO';
                                    $pubblicare = 1;
                                    $offline = 0;
                                    
                                }
                                    
                            } else {
                                
                                echo $db_tab->f($i);
                                
                            }

                            echo "&nbsp;";
                            echo "</td>\n";

                    }

                } else {

                    if($i!=0)
                    echo "<td scope=\"row\" class=\"".$bgc."\" align=\"".$align."\">".$db_tab->f($i)."</td>\n";
                }                 

            }

            if ($AzArray[1]!="") {

                echo "<td class=\"".$bgc."\" style=\"text-align: center;\">";
                reset($AzArray);
                next($AzArray);

                while (list ($key, $val) = each ($AzArray)) {
                                 
                    if(strcmp($val,_FAQ_MODIFICA_) == 0 && $pubblicare == 1) {

                        echo "<a title='"._FAQ_TITLE_MODIFICA_."' href=\"/index.phtml?pagina=form&amp;nome=".$nome."&amp;explode=".$explode."&amp;azione=UPD&amp;tipoOperazione="._FAQ_OP_MODIFICA_."&amp;".$chiave."=".$db_tab->f(0)."\"><img alt=\"Modifica Faq\" src=\"/images/admin/modify.gif\"/></a>&nbsp";

                    } elseif (strcmp($val,_FAQ_MODIFICA_) == 0) {
                        
                        echo "<a title='"._FAQ_TITLE_MODIFICA_."' href=\"/index.phtml?pagina=form&amp;nome=".$nome."&amp;explode=".$explode."&amp;azione=UPD&amp;tipoOperazione="._FAQ_OP_MODIFICA_."&amp;".$chiave."=".$db_tab->f(0)."\" onclick=\"if (!confirm('Modificando il contenuto di questa Faq questa non sarà visibile sul Catalogo Fornitori e sulla Vetrina Partecipanti fino ad una nuova pubblicazione. Procedere?')) return false\"><img alt=\"Modifica Faq\" src=\"/images/admin/modify.gif\"/></a>&nbsp";                                        
                        
                    }
                    
                    if(strcmp($val,_FAQ_VISUALIZZA_) == 0) {
                        
                        if($utype != 1 && $utype != 95) {
                            
                            echo "<a class=\"buttonRed\" title='"._FAQ_TITLE_VISUALIZZA_."' href=\"/index.phtml?pagina=form&amp;nome=".$nome."&amp;explode=".$explode."&amp;azione=UPD&amp;tipoOperazione="._FAQ_OP_VISUALIZZA_."&amp;".$chiave."=".$db_tab->f(0)."\" >"._FAQ_VISUALIZZA_."</a>&nbsp";                                        
                                
                        } else {
                            
                            echo "<a title='"._FAQ_TITLE_VISUALIZZA_."' href=\"/index.phtml?pagina=form&amp;nome=".$nome."&amp;explode=".$explode."&amp;azione=UPD&amp;tipoOperazione="._FAQ_OP_VISUALIZZA_."&amp;".$chiave."=".$db_tab->f(0)."\" ><img alt=\"Visualizza Faq\" src=\"/images/admin/preview.gif\"/></a>&nbsp";
                            
                        }
                        
                    }
                        

                    if(strcmp($val,_FAQ_ELIMINA_) == 0)                                       
                            echo "<a title='"._FAQ_TITLE_ELIMINA_."' href=\"/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0)."&amp;op_esegui=".$val."\" onclick=\"if (!confirm('"._FAQ_MSG_ELIMINA_."')) return false\"><img alt='"._FAQ_ALT_ELIMINA_."' src=\"/images/admin/trash.gif\"/></a>&nbsp";

                    if(strcmp($val,_FAQ_PUBBLICA_) == 0 && $pubblicare == 1)
                            echo "<a  title='"._FAQ_TITLE_ONLINE_."' href=\"/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0)."&amp;op_esegui=".$val."\" onclick=\"if (!confirm('"._FAQ_MSG_ONLINE_."')) return false\"><img alt='"._FAQ_ALT_ONLINE_."' src=\"/images/admin/publish.png\"/></a><br />";                        
                    
                    if(strcmp($val,_FAQ_OFFLINE_) == 0 && $offline == 1)
                            echo "<a  title='"._FAQ_TITLE_OFFLINE_."' href=\"/admin/azioni.phtml?nome=".$nome."&amp;explode=".$explode."&amp;ordina=".$ordina."&amp;".$chiave."=".$db_tab->f(0)."&amp;op_esegui=".$val."\" onclick=\"if (!confirm('"._FAQ_MSG_OFFLINE_."')) return false\"><img alt='"._FAQ_ALT_OFFLINE_."' src=\"/images/admin/offline.png\"/></a><br />";

                }

                echo "</td>";

            }

            echo "</tr>\n";

            $y++;

        }
    }
    else
            if ($DEBUG) echo "Errore SQLnew: ".$SQLnew."\n";

} else {
    
    echo "<tr><td colspan=\"".$tdcampi."\" style=\"text-align: center\">"._FAQ_NO_RISULTATI_."</td></tr>";
    
}

echo "</table>\n";

include $PROGETTO."/paginazione.inc";

?>