<?php

require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();
$idNonTest = 27;
//$IdImpresa = $auth->auth['IdImpresa'];
if ($IdImpresa > 0 && $auth->auth['IdImpresa']==""){
	$auth->auth['IdImpresa'] = $IdImpresa;
} elseif ($IdImpresa == "" && $auth->auth['IdImpresa'] > 0) {
	$IdImpresa = $auth->auth['IdImpresa'];
} elseif ($IdImpresa == "" && $auth->auth['IdImpresa'] == "") {
	//Lourdes No?
	//echo "Lista ".$IdImpresa." - ".$auth->auth['IdImpresa'];
	//exit;
	echo "<script type='text/javascript'>window.location.replace('index.phtml?Id_VMenu=307');</script>";
}

if (empty($IdImpresa) or $IdImpresa == "" or $IdImpresa == null){
	include_once("send_mail.inc");
	//cerca l'abbinamento Impresa user id e se c'� un'unica impresa ne recupera l'Id
	$qry = "SELECT EXPO_T_Imprese.Id FROM T_Utente INNER JOIN ((T_Anagrafica INNER JOIN EXPO_TJ_Imprese_Utenti ON T_Anagrafica.Id = EXPO_TJ_Imprese_Utenti.IdUtente)
	INNER JOIN EXPO_T_Imprese ON EXPO_TJ_Imprese_Utenti.IdImpresa = EXPO_T_Imprese.Id) ON T_Utente.Id = T_Anagrafica.Id
	WHERE T_Utente.Id=$uid;";
	if ($db->NumRows($qry) == 1){
		$IdImpresa = $db->GetRow($qry, 'Id', null, "visualizza_EXPO_Statistiche.inc Ln. 24");
		$cmd = "Errore IdImpresa recuperato: ".$IdImpresa;
		scriviLog($cmd, 'Recupero IdImpresa', '', true);
		//send_mail_html('alessandro.ferla@digicamere.it', 'noreply.catalogue@expo2015.org', 'Prod. Alert: visualizza_EXPO_Statistiche', $cmd.'\n');
	} else {
		$cmd = "IdImpresa non recuperato: fatto redirect!";
		scriviLog($cmd, 'Recupero IdImpresa', '', true);
		redirect($MAINSITE."index.phtml?Id_VMenu=307");
	}
}


$uidArrayAllow = array (5,124,152,735);

if (in_array($uid, $uidArrayAllow)){
	$uidAllow = true;
	//print "utenza allow";
	//print "</br>";
}else {
	$uidAllow = false;
	//print "utenza not allow";
	//print "</br>";
}

function selectLetteraAzienda($nomeImpresa) {
	global $arrayRootDoc;
	 
	$inizialeImpresa = strtoupper(substr($nomeImpresa, 0, 1));
	 
	foreach ($arrayRootDoc as $key => $value) {
		if (in_array($inizialeImpresa, $value)) {
			return $inizialeImpresa;
		}

	}
	return 'Altro';
}

?>
<style>
h4 {
	width: 100%;
	text-transform: uppercase;
}

.tdHeader {
border-right: 0;
}

.center{
	text-align: center;
}

</style>
<div class="tsc_tabs_type_3_container" style="padding: 1em;" >
    <ul id="tsc_tabs_type_3">
		<li><a href="javascript:showTab(1)" name="tab1" >Numero di Visite</a></li>
		<?php if ($uidAllow){?>
		<li><a href="javascript:showTab(2)" name="tab2" >Elenco Iscritti</a></li>
		<?php }?>
    </ul>
        
    <div id="tsc_tabs_type_3_content" style="border-top:0px;" >
    <!-- Tab 1 Start -->
    <div id="gtab1">
    <?php $titoloAppend = "";	

 	if ( $ordine== 0){ 
		$titoloAppend .= _IMPRESA_;
	} else if (strcmp($studio,"Y")== 0){
		$titoloAppend .= _STUDIO_;
	} else if (strcmp($studio,"N")== 0){
		$titoloAppend .= _PROFESSIONISTA_;
	} ?>
    	
    	<h4><?php echo $titoloAppend;?></h4> 
    	
    	<?php 
    		$visiteImpresaSQL = "SELECT VisiteTotali,VisitePrecedenti FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
    		$visiteImpresa = $db->GetRow($visiteImpresaSQL,null,null,"statistiche impresa Ln.41");
    	?>
<!--   	<ul >
			<li>Numero di Visite totali: <?php print $visiteImpresa['VisiteTotali'];?></li>
			<li>Numero di Visite nell'ultima settimana: <?php print ($visiteImpresa['VisiteTotali'] - $visiteImpresa['VisitePrecedenti']);?></li>
    	</ul>
-->    
    	<table id="tabella_elenchi">
    		<tr class="tdHeader">
    			<th class="center" style ="width:50%">Numero di Visite totali</th>
    			<th class="center">Numero di Visite nell'ultima settimana</th>
    		</tr>
    		<tr <?php echo getColor("lineTable", 1); ?>>
    			<td class="center"><?php print $visiteImpresa['VisiteTotali'];?></td>
    			<td class="center"><?php print ($visiteImpresa['VisiteTotali'] - $visiteImpresa['VisitePrecedenti']);?></td>
    		</tr>
    	</table>
    
    

    <?php
    $visiteProdottiSQL = "SELECT Nome,VisiteTotali,VisitePrecedenti FROM EXPO_T_Prodotti WHERE IdImpresa = $IdImpresa";
    $visiteProdotti = $db->GetRows($visiteProdottiSQL,null,null,"statistiche impresa Ln.41");
    
    $numProdotti = count($visiteProdotti);
    
    if ($numProdotti > 0){
    
 	if ( $ordine== 0){ 
		$titoloProdottiAppend = _PRODOTTO_;
		if ($numProdotti > 1 ){
			$titoloProdottiAppend = "Prodotti";
		} else {
			$titoloProdottiAppend = _PRODOTTO_;
		} 
		
	} else{
		
		if ($numProdotti > 1 ){
			$titoloProdottiAppend = "Servizi";
		} else {
			$titoloProdottiAppend = _SERVIZIO_;
		}
		
		
	}
	?>
    	
    	<h4><?php echo $titoloProdottiAppend;?></h4> 
    	
    	<table id="tabella_elenchi">
    		<tr class="tdHeader">
    			<th>Nome <?php echo $titoloProdottiAppend;?></th>
    			<th class="center">Numero di Visite totali</th>
    			<th class="center">Numero di Visite nell'ultima settimana</th>
    		</tr>
    <?php 	
    $isColor = 1;
		foreach ($visiteProdotti AS $key => $prodotto){

	?>
   		
    		
    		<tr <?php echo getColor("lineTable", $isColor); ?>>
    			<td><?php print $prodotto['Nome'];?></td>
    			<td class="center"><?php print $prodotto['VisiteTotali'];?></td>
    			<td class="center"><?php print ($prodotto['VisiteTotali'] - $prodotto['VisitePrecedenti']);?></td>
    		</tr>
    	
	<?php 
		$isColor ++;
		}
	
		print "</table>";
	}
	?>
    
    <?php 
    	
    /*$isReteImpresaSQL = "SELECT IsRetiImpresa FROM EXPO_T_Imprese WHERE Id = $IdImpresa";
    $isReteImpresa = $db->GetRow($isReteImpresaSQL,"IsRetiImpresa",null, "statistiche impresa Ln.142");
    
    if (strcmp($isReteImpresa, "Y") == 0 ){
    */
    
    $visiteRetiImpresaSQL = "SELECT NomeRete, VisiteTotali,VisitePrecedenti
			FROM EXPO_TJ_Imprese_RetiImpresa AS TJ join EXPO_T_RetiImpresa AS R ON TJ.Id_Rete = R.Id
			WHERE Id_Impresa = $IdImpresa ORDER BY NomeRete";
    
    $visiteRetiImpresa = $db->GetRows($visiteRetiImpresaSQL,null,"statistiche impresa ln.151");
    
    if (count($visiteRetiImpresa)>0){
    
    ?>
    	<h4>FORME DI AGGRAGAZIONE</h4> 
    	
    	<table id="tabella_elenchi">
    		<tr class="tdHeader">
    			<th>Nome Rete</th>
    			<th class="center">Numero di Visite totali</th>
    			<th class="center">Numero di Visite nell'ultima settimana</th>
    		</tr>
    <?php 	
    $isColor = 1;
		foreach ($visiteRetiImpresa AS $key => $rete){

	?>
   		
    		
    		<tr <?php echo getColor("lineTable", $isColor); ?>>
    			<td><?php print $rete['NomeRete'];?></td>
    			<td class="center"><?php print $rete['VisiteTotali'];?></td>
    			<td class="center"><?php print ($rete['VisiteTotali'] - $rete['VisitePrecedenti']);?></td>
    		</tr>
    	
	<?php 
		$isColor ++;
		}
	
		print "</table>";
	}
	?>
    
    
    
    
    	
    </div>
    <?php if ($uidAllow){?>
    <div id="gtab2">
			<h4>Elenco Iscritti</h4>
            <?php
            
            
            $arrayAziendeIscritte = array();
            
           /* print "<pre>";
            print_r($arrayAziendeIscritte);
            print "</pre>";
            */
            $aziendeIscritteSQL = "SELECT RagioneSociale From EXPO_T_Imprese WHERE StatoRegistrazione > 3  AND Id > $idNonTest ORDER BY RagioneSociale";
            $aziendeIscritte = $db->GetRows($aziendeIscritteSQL,null, 'statistiche ln 62');
            
            foreach ($aziendeIscritte AS $key => $azienda){
				$nomeImpresa = $azienda['RagioneSociale'];
				$arrayAziendeIscritte[selectLetteraAzienda($nomeImpresa)][] = $nomeImpresa;
				
			}
            
			/*print "<pre>";
			print_r($arrayAziendeIscritte);
			print "</pre>";
			*/
			$i=0;
           ?>
           
           
           <?php foreach ( $arrayRootDoc as $firstlevel => $lettere){ ?>
           
        <div class="alberoCategorie">
        <div class="collapsibleCat" id="section<?php echo $i; ?>">
            <span></span>
            <?php  print $lettere['0'].' -> '.$lettere[count($lettere)-1];?>
                            
        </div>
        <div class="container">
            <div class="content">
               <?php
                   $i++;
                   foreach ( $lettere as $key => $value){
                   ?>
                    <div class="alberoCategorie">
                        <div class="collapsibleCat" id="section<?php echo $i; ?>">
                        <span></span>     <?php print $value;	 ?>
                        </div>
                        <div class="container">  
                            <div class="content">
			                        <?php
			                        foreach ($arrayAziendeIscritte[$value] AS $key => $impresa) {
			                        ?>
                                    <p><?php echo $impresa; ?></p>
                                    <?php
                                }
                                ?> 
                            </div>        
                        </div>                                                            
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>  
    </div>
    <?php 
       }
     ?>
     <div class="alberoCategorie">
        <div class="collapsibleCat" id="section<?php echo $i; ?>">
            <span></span>
            Altro
        </div>
        <div class="container">
            <div class="content">
                    <div class="alberoCategorie">
                        <div class="container">  
                            <div class="content">
			                        <?php
			                         foreach ($arrayAziendeIscritte["Altro"] AS $key => $impresa) {
			                        ?>
                                    <p>
                                        <?php echo $impresa; ?>
                                    </p>
                                    <?php
                                }
                                ?> 
                            </div>        
                        </div>                                                            
                    </div>
            </div>
        </div>  
    </div>      
                  
	</div>
	<?php }?>
</div>
</div>

<!-- DC Flat Tabs End -->
                            
                           
 <div class="tsc_clear"></div> <!-- line break/clear line -->
<!--<script type="text/javascript" src="/jquery/js/visualizza_Log_Vetrina.js"></script>-->
<!--<script type="text/javascript" src='/jquery/js/tsc/visualizza_EXPO_Report_Imprese.js'></script>-->    