<?php 
require_once("configuration.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/gestioneListeExt.class.php");

$gestioneListe = new GestioneListeExt("imprese",$uid);

if ($_FILES){
	$gestioneListe->caricaCSV($_FILES["impreseCSV"]);

	//
}
?>
<div id="formUpload">
	<form id="upload" action="index.phtml?Id_VMenu=512" method="post" enctype="multipart/form-data">
		<input type="hidden" name="typeCSV" id="typeCSV" value="imprese">
		<input type="file" name="impreseCSV" id="auto"><br>
		<input id="carica" style="display:none; float: right;" class="buttonRed" type="submit" value="CARICA FILE" />
	</form>
	<!-- <div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
    </div>
    <div id="message"></div>-->
</div>


<?php

if ($gestioneListe->numImpresePresenti()> 0){
?>
<div id="elencoTabella">
	<?php 
	$gestioneListe->errorCSV();
	$gestioneListe->stampaElenco();
	?>
</div>
<?php  }
else{
 print '<h3 style="margin-left:18px;">Nessuna Impresa caricata nel sistema</h3>';
}

?>
