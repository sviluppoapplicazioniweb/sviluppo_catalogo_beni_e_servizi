<?php

//TITOLI TAB
define(_EMAIL_, "E-mail (non PEC)");
define(_NOME_, "Name");
define(_COGNOME_, "Surname");
define(_TELEFONO_, "Phone");
define(_PRODOTTI_, "Products");
define(_PRODOTTO_, "Product");
define(_RAGIONESOCILE_, "Company name");
define(_PARAMETRI_, "Parameters");
define(_BENVENUTO_, "Welcome");
define(_DISCONNETTI_, "Disconnect");
define(_CERTIFICAZIONI_, "Certifications and Awards");
define(_ASPETTI_, "Innovation and Sustainability");
define(_BREVETTI_, "Patents");
define(_C_MERC_, "Product Categories");

define(_C_MERC_T_, "Product<br>Categories");
define(_DATI_GENERALI_T_, "General<br>Data");
define(_CERTIFICAZIONI_T_, "Certifications and <br>Awards");
define(_ASPETTI_T_, "Innovation and<br>Sustainability");
define(_BREVETTI_T_, "Patents<br><br>");


define(_C_LOGO_, "Change Logo");
define(_TIT_1_, "Data from the Companies Register");
define(_TIT_2_, "Additional information");
define(_TIT_3_, "Information material");

define(_DATI_GENERALI_, "General <br> data");
define(_DETTAGLI_, "Details");
define(_QUALIT_T_, "Characteristics");
define(_QUALIT_, "Characteristics of the business/professional");
define(_ASSOCIZIONI_, "Associations");
define(_ASSOCIZIONI_RETI_, "Forms of aggregation");
define(_RAGSOC_, "Company");
define(_CAPISOC_, "Share capital");
define(_U_FATTURATO_, "Sum of turnover for the last 5 years");
define(_U_FATTURATO_ESERC, "Turnover last financial year");

define(_ALLEGA_DOC_, "<strong>Depth document</strong>");
define(_ALLEGA_DOC_ANTEPRIMA, "<strong>Depth document</strong>");
define(_ALLEGA_LINK_ANTEPRIMA_, "<strong>Links for further information</strong>");
define(_ALLEGA_ELENCO_, "<strong>List of references most qualified</strong>");
define(_SEDE_LEG_, "Legal domicile");
define(_TEL_, "Pho");
define(_DESCRIZIONE_, "Description");
define(_DESCRIZIONE_ATT_, "Description of activity");
define(_N_DIP_, "Number of employees");
define(_SEDI_, "Degree of enterprise internationalization");
define(_S_ESTERA_, "Presence based abroad");
define(_NET_INTER_, "Membership in international Network");
define(_T_IMPRESA_, "Type Company");
define(_GENER_CONT_, "Gereral contractor");
define(_PRODUTTORE_, "Producer");
define(_DISTRIBUTORE_, "Distributor");
define(_REFERENZE_, "References");
define(_PARTECIPZIONI_EXPO_, "Participation in EXPO");
define(_PAR_INTERNAZIONALI_, "Investments International");
define(_ESPERIENZA_IN_, "International Experiences");
define(_CON_LINGUE_, "Knowledge of languages");
define(_LINGUE_, "languages");
define(_SELEZIONA_, "Select...");
define(_GREEN_, "Attention to the issue of sustainability Green and Social");
define(_CERTIFICAZIONI2_, "Certifications");
define(_APP_ASS_, "Membership of Category Associations");
define(_APP_RETE_IMP_, "Membership in enterprises aggregations (networks, consortia, associations)");
define(_R_RETE_IMP_, "Network of companies");
define(_R_N_PARAMETRI_, "Selected parameters: ");
define(_R_SINDACALE_, "Presence Board of Statutory Auditors");
define(_R_P_CERTIFICAZIONE_, "SOA Certification");

define(_SINDACALE_, "Presence of a Board of Statutory Auditors");
define(_A_MODELLO_, "Organizational Model 231/01");
define(_P_LEGALITA_, "Rating of legality");
define(_P_MASTER_R_, "Professional holding Masters / PhD");
define(_P_MASTER_, "Masters/PhD in its business");
define(_P_CERTIFICAZIONE_, "SOA Certification");
define(_IMPRESA_, "Company");
define(_PREFERITI_, "Favorites");
define(_OSSERVAZIONI_, "Reason:");
define(_PARTECIPANTE_, "Participant");
define(_FORNITORE_, "Supplier");
define(_VALORE_, "Value");
define(_DATA_CHIUSURA_, "Closing date");
define(_STATO_, "Status");
define(_STATO_TRATTATIVA_, "Status Negotiations");
define(_TESTO_MOD_IMG_, "<h2> Logo Change </ h2>
Select a New Logo to load <br>
Your logo will be resized if needed to a maximum dimension of 120 pixels width x 120 height automatically.");
define(_PASSWORD_, "Password");
define(_PASS_, "Password (min. 8 char.)*");
define(_R_PASS_, "Re-enter Password*");
define(_SEND_OK_, "<h2>Message sent successfully!</h2>");
define(_FILE_PRESENTE_, "A file ");
define(_FILE_V_, "See File below for details<br>");
define(_LINK_V_, "See Web Link below for details<br>");
define(_NOTE_1_, "Model of organization and management aimed at preventing the liability of the undertakings in criminal liability arising from the Legislative Decree of 8 June 2001. 231.");
define(_NOTE_2_, "Rating ethical managed by the Authority for Competition and Market Authority (AGCM) resulting from the Decree Law of 24 March 2012, n. 29 in order to promote ethical principles in Italy in corporate behavior.");
define(_NOTE_3_, "Products that have opened negotiations will not be displayed.");
define(_NOTE_4_, "<br>Allegare file in PDF<br><br>");
define(_NOTE_4_, "<br>* Mandatory in case of refusal<br><br>");

//BOTTONI
define(_SALVA_, "Save");
define(_ELIMINA_, "Delete");
define(_AGGIUNGI_, "Add");
define(_VISUALIZZA_, "View");
define(_INVIA_, "Send");
define(_CHIUSURA_, "Closed negotiation");
define(_INDIETRO_, "< Back");
define(_CONFERMA_, "Confirmed");
define(_DINIEGA_, "Denied");
define(_SOSPESO_, "Suspended");
define(_CHIUDI_, "CLOSE");
define(_RICHIESTA_, "Send Request");
define(_RICHIESTE_, "Request");
define(_MODIFICA_, "Edit");
define(_ATTESA_, "Wait");
define(_CONFERMATO_, "Confirmed");
define(_ANTEPRIMA_, "Preview");
define(_G_LINK_, "Regenerate Links");
define(_ACCEDI_, "Login");
define(_REGISTRATI_, "Register");
define(_CAMBIA_, "Change");
define(_EXTRA_B_, "Negotiation extra bulletin board");
define(_ALL_, "All");
define(_SELEZIONA_B_, "Select");

//MSG RISPOSTA
define(_PRODOTTO_CATEGORIA_, "The category you are clearing is associated with some products.");
define(_CAMPI_OB_CAT_, "Select at least one category.");
define(_EMAIL_ER_, "E-Mail field not valid");
define(_SMART_CARD_, "Smart Card and press enter login.");
define(_OK_INVIO_LINK_, "<h2>You have been sent an email with a confirmation link recording</h2>");
define(_PASSWORD_ERR_, "<h2>Password is incorrect</h2>");
define(_TRA_INCORSO_, "For these products are already in the negotiations:");
define(_CAMPI_OB_, "Parameters marked with an asterisk are required.");
define(_CAMPI_OB_TR_, "Enter the text for all languages​​.");
define(_CAMPI_PAS_NO_, "Invalid Password");
define(_CAMPI_PAS_UG_, "Password does not match confirmation");

define(_OK_ATTIVAZINE_, "Activation was successful");

define(_MITTENTE_, "From");
define(_DESTINATARIO_, "Address");
define(_OGGETTO_, "Subject");
define(_DATA_, "Date");
define(_OGGETTO_MAIL_, "Request a quote for ");
define(_MESSAGGIO_, "Message");
define(_ALLEGATO_, "Enclosure");

//MAIL DI RISPOSTA
define(_N_RISP_, "<br /><br />Automatically generated message please do not respond.");
define(_OGG_DATI_UTENTE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Data recording");
define(_MSG_DATI_UTENTE_, "<p>Dear user,<br /> 
we confirm successful registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
Here are your Username and Password: <br>
<br />
USER: {%user%} <br />
<br />
PASSWORD: {%Password%} <br />
<br /> <br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_AGG_I_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_AGG_I_, "<p>Dear {%utente%},<br /> 
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has assigned the role of Managing Director of the Company in the Catalogue Goods and Services Suppliers Expo 2015. 
Can access the catalog with the logon credentials that are already in its possession 
and then select the / Enterprise which is already written for you accederne to content. <br /><br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_REGISTRAZIONE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_NEW_REG_, "<p>Dear {%utente%},
     <br /> we confirm the registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
By clicking on the link below which will activate your registration. <br />
     {%link%} <br /><br />
     Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_DELETE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Cancellation Personal Profile");
define(_MSG_DEL_REG_DEF_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
carried out the cancellation of his role as Chief Executive Officer of the Company. <br> 
Since it is not included on other companies in the Catalog its user has been deleted and can no 
longer use the login credentials in his possession. <br> For any information to contact regarding the 
direction of EXPO. 
<br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_MSG_DEL_REG_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>,as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has reset the memory of his role as Chief Executive Officer of the Company. <br />
Can still access the catalog with the logon credentials that are already in its possession and then select the / Enterprise which is already written for you accederne to content.
    <br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_BACHECA_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - negotiation ID #{%idBacheca%}");
define(_MSG_NOTIFICA_TRA_, "<p> Dear {%user%}  <br />
     the user <strong>{%mittente%}</ strong> has sent you a message. <br />
     You can find it in the Message section of your personal area of ​​the Goods and Services Catalog Suppliers Expo 2015. <br />
     <br />
     Regards <br /> </ p>{%TITLESITE%}
    <br />-----------------------------------<br />
    <p>Gentile {%utente%} ,<br />
    l&#39;utente  <strong>{%mittente%}</strong> ti ha inviato un messaggio. <br />
    Lo trovi nella sezione Bacheca della tua area personale del Catalogo Fornitori Beni e Servizi Expo 2015.<br />
    <br />
    Distinti Saluti <br /> </p>{%TITLESITE%}"._N_RISP_);

define(_OGG_NOT_CD_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%}  ({%oggetto%})");
define(_MSG_NOT_CD_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the Enterprise {%Impresa%}
<strong>refused</strong> the closing of the deal. And &#39;possible to read the <strong>reasons</strong> for the refusal and send
message logging Showcase Area Board of Participants.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l'Impresa {%Impresa%} 
ha <strong>rifiutato</strong> la chiusura della trattativa. E' possibile leggere le motivazioni del rifiuto ed inviare 
un messaggio accedendo nell'Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CD_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi – Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_P_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation with Company {%Impresa%}, we inform you that the Participant {%mittente%} 
<strong>refused</strong> the closure of the deal. <br>
You can read the reasons for the refusal and send a message logging Area Board
Suppliers of Goods & Services Catalog.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha <strong>rifiutato</strong> la chiusura della trattativa.<br> 
E' possibile leggere le motivazioni del rifiuto ed inviare un messaggio accedendo nell'Area Bacheca 
del Catalogo Fornitori Beni & Servizi.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation,
we inform you that the company has <strong>confirmed</strong> {%Impresa%}
the closure of the negotiation<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto,
la informiamo che l'Impresa {%Impresa%} ha <strong>confermato</strong> 
la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation,
we inform you that Participant {%mittente%} has confirmed the closing of the deal.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha confermato la chiusura della trattativa<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_I_, "<p>Good morning / Good evening , <br>
with reference to the aforementioned negotiation,
we inform you that company {%Impresa%} responsible for sealing the deal.
It requires you to confirm whether or not the outcome of the Showcase Participants accessing Area Board.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera ,<br>
con riferimento alla Trattativa Commerciale in oggetto, 
la informiamo che l'Impresa {%Impresa%} ha proceduto alla chiusura della trattativa. 
Si richiede di confermarne o meno l'esito accedendo nell'Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that Participant {%mittente%} responsible for sealing the deal. <br>
It requires you to confirm whether or not the outcome accessing Area Wall Suppliers Catalog Goods & Services<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l'Impresa {%Impresa%},  
la informiamo che il partecipante {%mittente%} ha proceduto alla chiusura della trattativa.<br>
Si richiede di confermarne o meno l'esito accedendo nell'Area Bacheca del Catalogo Fornitori Beni & Servizi<br />
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);

//GESTIONE LINGUE
define(_ITALIANO_, "Italian");
define(_INGLESE_, "English");
define(_FRANCESE_, "French");

//MODIFICA 25/09/2013
define(_FILE_NO_UP_, "<h2>Unable to upload.</h2>");
define(_FILE_MAX_, "<h2>File is too big.</h2>");
define(_ENTRA_SC_, "Sign in using Smart Card");
define(_ENTRA_LOG_, "Sign in using credentials recorded");
define(_DIM_PAS_, "Forgot your password?");
define(_REG_, "Registering");

//MODIFICA 26/09/2013
define(_RICERCA_, "Find");
define(_IMPRESE_, "Companies");
define(_AGGIUNGI_, "Add parameter");
define(_CERCA_PAR_, "SEARCH FOR PARAMETERS");
define(_ANNULLA_, "Cancel");

//MODIFICA 30/09/2013
define(_UTENTE_RLN_, "Link forwarded.");
define (_NUMERO_OGGETTI_BACHECA_,"There are %n% messages");

//MODIFICA 01/10/2013
define(_NO_PRODOTTI_,"To be able to enter new Products/Services, you must subscribe to at least one Product Category");

define (_AGGIUNGI_CATEGORIA_,"Add Product Category");
define (_GESTISCI_CATEGORIA_,"Manage Product Category");
define (_NESSUNA_CATEGORIA_,"None Product Category registered");

// MODIFICA DEL 02/10/2013
define(_DESCRIZIONE_FILE_,"Description");
define(_BILANCI_FILE_,"Budget");
define(_INTERNAZIONALIZZAZIONE_FILE_,"Internationalization");
define(_REFERENZE_FILE_,"References");
define(_QUALITA_FILE_,"Quality");
define(_RETI_FILE_,"Forms of aggregation");

define(_PRODOTTO_FILE_,"Product/Service");
define(_CERTIFICAZIONE_FILE_,"Certification");
define(_ASPETTI_FILE_,"Aspects");
define(_BREVETTI_FILE_,"Patents");

define(_FILELINK_V_, "See File/Web link below for details<br>");

// MODIFICA DEL 04/10/2013
define(_NO_TRATTATIVA_,"Nessuna Trattativa presente FR");
define(_NO_BACHECA_,"Nessun Messaggio in Bacheca FR");
define(_NO_DELEGATI_,"Nessun Delegato Associato all'Impresa EN");
define(_IsPrestatoreDiServizi_,"Services provider");

//MODIFICA 05/11/2013
define(_P_MASTER_FILE_, "Masters/PhD in its business");
define(_P_CERTIFICAZIONE_FILE_, "SOA Certification");
define(_CERTIFICAZIONI_FILE_, "Certification");

//MODIFICA 27/12/2013
define(_TEXT_HOME_1_, "<span>Welcome to the Suppliers Catalogue for Expo 2015 <br>Participants!<br></span>
A virtual environment for meeting participating countries and <br>helping them in the design, construction and 
furnishing of their pavilions through the provision of products or services.");
define(_TEXT_HOME_2_, "<span><br>What can you do with the catalogue?</span>");
define(_TEXT_HOME_3_, "<a href='index.phtml?Id_VMenu=308'>Click here</a> to access the catalogue");
define(_TEXT_HOME_4_, "<div><strong>REGISTER</strong> by providing information <br>of the legal representative<br><a class=\"blu\" href=\"index.phtml?Id_VMenu=502\" target=\"blank\">Consult the registration Guide</a></div>");
define(_TEXT_HOME_5_, "<div><strong>ACCEPT<strong> the rules of registration<br><a class=\"blu\" href=\"ajaxlib/requester/downloadFile.php\" target=\"blank\">Download the Regulation</a></div>");
define(_TEXT_HOME_6_, "<div><strong>COMPLETE</strong> the company profile <br>with the sector of operations, characteristics, <br>certifications and references<br><a class=\"blu\" href=\"index.phtml?Id_VMenu=500\" target=\"blank\">Product Categories List</a></div>");
define(_TEXT_HOME_7_, "<div>Further information on products and services</div>");
define(_TEXT_HOME_8_, "<div>Receive requests from participating countries via the messaging system</div>");


define(_NOTE_15_, "SOA certifications are listed only to facilitate choice of category, they are not a requisite.");
//MODIFICA 20/11/2013
define(_CERTIFICATO_NO_,"Allegare il file PDF firmato");
define(_CATEGORIE_MERCIOLOGICHE_, "Product<br>Categories ");
define(_NESSUNA_LINGUA_, "No language selected");

//MODIFICA 10/12/2013

define(_SIEXPO_NOTE_1_,"Available in Siexpo Catalogue");

//MODIFICA  02/04/2014 PROFESSIONISTI
define(_ORDINE_, "Ordine Professionale");
define(_ORDINE_TIPO_ISCRIZIONE_, "Iscrizione in qualit&agrave; di:");
define(_STATO_ALBO_, "Stato Iscrizione<br/> Albo Nazionale");
define(_CODICE_FISCALE_, "Codice Fiscale");
define(_NUMERO_ISCRIZIONE_, "Numero Iscrizione");
define(_COF_DATI_PROFSSIONISTA_,"Nel riquadro sottostante si riportano le informazioni tratte dall'Albo Nazionale.");
define(_ALTRO_STUDIO_,"Studio di associati");

define(_TIT_1_LP_, "Professional information");
define(_TIT_1_STUDIO_, "Studio information");
define(_N_DIP_ORDINI_, "Number of employees or external personnel");

define(_QUALIT_LP_, "Characteristics of the professional");
define(_QUALIT_STUDIO_, "Characteristics of the studio");
define(_SEDI_LP_, "Degree of internationalization of the professional");
define(_SEDI_STUDIO_, "Degree of internationalization of the studio");
define(_RAGSOC_PROF_, "Name");
define(_APP_RETE_IMP_PROF_, "Membership in collective bodies (e.g. networks, consortia, associations)");

define(_IMPRESA_OFFLINE_PRODOTTI_, "L'impresa &egrave; offline. I prodotti/servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_LP_, "Il Professionista &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_STUDIO_, "Lo studio &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");

define(_PROFESSIONISTA_, "Professional");
define(_STUDIO_, "Studio");
define(_PUBBLICA_ALL_PROF_, "Public services");

define(_SERVIZI_, "List of services");
define(_SERVIZIO_, "Service");
define(_NO_PRODOTTI_PROF_,"Per poter inserire nuovi servizi è necessario iscrivere l'Impresa ad almeno una Categoria Merceologica");

define(_PRODOTTO_SAVE_,"Prodotto salvato con successo");
define(_SERVIZIO_SAVE_,"Servizio salvato con successo");

//Aggiunto 09 Maggio 2014
define(_ANT_NOME_RETE_, "Network: ");
define(_DESCR_RETE_, "Description: ");
define(_ANT_CONTATTO_RETE, "Contact of Network: ");
define(_IMPRESE_RETE_,"<strong>Other companies and members of the Network to Catalogue</strong><br/>");
define(_ANT_IMP_CAPOG_,"Enterprise group leader");
define(_TEXT_HOME_9_,"<div>Search eco-friendly and innovative products on <a class=\"blu\" href=\"http://www.siexpo2015.it/index.phtml?Id_VMenu=322\" target=\"_blank\">SIEXPO</a></div>");
define(_NO_RETI_,'No network selected');

define(_DISCLAIMER_CATEGORIE_DEFAULT_PDMS_ ,"The products and services in this category are provided by Expo 2015 Partners or Sponsors, whose product/service portfolios are detailed in the <b>Official Partners Catalogue</b> (http://catalogue.expo2015.org), however other companies may be found by continuing the search.");
?>

