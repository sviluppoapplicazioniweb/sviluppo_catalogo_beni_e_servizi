<?php

//TITOLI TAB
define(_EMAIL_, "E-mail (non PEC)");
define(_NOME_, "Nom");
define(_COGNOME_, "Pr&eacute;nom");
define(_TELEFONO_, "Phone");
define(_PRODOTTI_, "Produits");
define(_PRODOTTO_, "Produit");
define(_RAGIONESOCILE_, "Company name");
define(_PARAMETRI_, "Param&egrave;tres");
define(_BENVENUTO_, "Welcome");
define(_DISCONNETTI_, "Disconnect");
define(_CERTIFICAZIONI_, "Certifications et Reconnaissances");
define(_ASPETTI_, "Aspects d'innovation et durabilit&eacute;");
define(_BREVETTI_,"Brevets");
define(_C_MERC_, "Cat&eacute;gories de Produits");

define(_C_MERC_T_, "Cat&eacute;gories<br>de Produits");
define(_DATI_GENERALI_T_, "Donn&eacute;es<br>G&eacute;n&eacute;rales");
define(_CERTIFICAZIONI_T_, "Certifications et<br>Reconnaissances");
define(_ASPETTI_T_, "Aspects d'innovation et <br>durabilit&eacute;");
define(_BREVETTI_T_, "Brevets<br><br>");


define(_C_LOGO_, "Change Logo");
define(_TIT_1_, "Data from the Companies Register");
define(_TIT_2_, "Additional information");
define(_TIT_3_, "Information material");

define(_DATI_GENERALI_, "Donn&eacute;es <br> g&eacute;n&eacute;rales");
define(_DETTAGLI_, "D&eacute;tails");
define(_QUALIT_T_, "Characteristics");
define(_QUALIT_, "Characteristics of the business/professional");
define(_ASSOCIZIONI_, "Associations");
define(_ASSOCIZIONI_RETI_, "Formes d&rsquo;agr&eacute;gation");
define(_RAGSOC_, "Raison sociale");
define(_CAPISOC_, "Capital social");
define(_U_FATTURATO_, "Somme des cinq derniers chiffres d'affaires");
define(_U_FATTURATO_ESERC, "Chiffre d'affaires du dernier exercice");

define(_ALLEGA_DOC_, "<strong>Document d'approfondissement</strong>");
define(_ALLEGA_DOC_ANTEPRIMA, "<strong>Document d'approfondissement</strong>");
define(_ALLEGA_LINK_ANTEPRIMA_, "<strong>Liens pour plus d'informations</strong>");
define(_ALLEGA_ELENCO_, "<strong>Liste des r&eacute;f&eacute;rences les plus qualifi&eacute;s</strong>");
define(_SEDE_LEG_, "Si&egrave;ge l&eacute;gal");
define(_TEL_, "Pho");
define(_DESCRIZIONE_, "Description");
define(_DESCRIZIONE_ATT_, "Description des activit&eacute;s");
define(_N_DIP_, "Nombre de salari&eacute;s");
define(_SEDI_, "Degr&eacute; d'internationalisation de l'entreprise");
define(_S_ESTERA_, "Pr&eacute;sence de si&egrave;ge &agrave; l'&eacute;tranger");
define(_NET_INTER_, "Membership in international Network");
define(_T_IMPRESA_, "Type d'Entreprise");
define(_GENER_CONT_, "Entrepreneur G&eacute;n&eacute;ral");
define(_PRODUTTORE_, "Producteur");
define(_DISTRIBUTORE_, "Distributeur");
define(_REFERENZE_, "R&eacute;f&eacute;rences");
define(_PARTECIPZIONI_EXPO_, "Participation in EXPO");
define(_PAR_INTERNAZIONALI_, "Appartenance &agrave; un r&eacute;seau international");
define(_ESPERIENZA_IN_, "Fournisseur d'autres Expositions ou d'autres grands &eacute;v&eacute;nements internationaux");
define(_CON_LINGUE_, "Connaissance des langues");
define(_LINGUE_, "langues");
define(_SELEZIONA_, "S&eacute;lectionnez...");
define(_GREEN_, "Attention to the issue of sustainability Green and Social");
define(_CERTIFICAZIONI2_, "Certifications");
define(_APP_ASS_, "Appartenance &agrave; des associations professionnelles");
define(_APP_RETE_IMP_, "Appartenance &agrave; des agr&eacute;gations d&rsquo;Entreprises (r&eacute;seaux, consortiums, associations)");
define(_R_RETE_IMP_, "Formes d'agr&eacute;gation");
define(_R_N_PARAMETRI_, "Param&egrave;tres s&eacute;lectionn&eacute;s: ");
define(_R_SINDACALE_, "Pr&eacute;sence du Coll&egrave;ge des Commissaires aux Comptes");
define(_R_P_CERTIFICAZIONE_, "Certification SOA");

define(_SINDACALE_, "Pr&eacute;sence du Coll&egrave;ge des Commissaires aux Comptes");
define(_A_MODELLO_, "Mod&egrave;le Organisationnel 231/01");
define(_P_LEGALITA_, "Note de l&eacute;galit&eacute;");
define(_P_MASTER_R_, "Professionnel poss&eacute;dant un Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_P_MASTER_, "Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_P_CERTIFICAZIONE_, "Certification SOA");
define(_IMPRESA_, "Entreprise");
define(_PREFERITI_, "Favoris");
define(_OSSERVAZIONI_, "Reason:");
define(_PARTECIPANTE_, "Participant");
define(_FORNITORE_, "Fournisseur");
define(_VALORE_, "Valeur");
define(_DATA_CHIUSURA_, "Date de fermeture");
define(_STATO_, "&Eacute;tat");
define(_STATO_TRATTATIVA_, "&Eacute;tat N&eacute;gociations");
define(_TESTO_MOD_IMG_, "<h2> Modifier Logo </ h2>
S&eacute;lectionner un nouveau Logo &agrave; t&eacute;l&eacute;charger <br>
Si besoin est, votre logo sera automatiquement r&eacute;duit &agrave; 120 pixels de largeur x 120 de hauteur.");
define(_PASSWORD_, "Password");
define(_PASS_, "Password (min. 8 char.)*");
define(_R_PASS_, "Re-enter Password*");
define(_SEND_OK_, "<h2>Message sent successfully!</h2>");
define(_FILE_PRESENTE_, "A file ");
define(_FILE_V_, "Voir ci-dessous pour plus de d&eacute;tails fichier<br>");
define(_LINK_V_, "Voir les liens Web ci-dessous pour plus de d&eacute;tails<br>");
define(_NOTE_1_, "Mod�le d'organisation et de gestion visant � pr�venir la responsabilit� objective des entreprises au niveau p�nal; d�coulant du D�cret L�gislatif n� 231 du 8 juin 2001.");
define(_NOTE_2_, "Rating ethical managed by the Authority for Competition and Market Authority (AGCM) resulting from the Decree Law of 24 March 2012, n. 29 in order to promote ethical principles in Italy in corporate behavior.");
define(_NOTE_3_, "Products that have opened negotiations will not be displayed.");
define(_NOTE_4_, "<br>Allegare file in PDF<br><br>");
define(_NOTE_4_, "<br>* Obligatoire en cas de refus<br><br>");

//BOTTONI
define(_SALVA_, "Enregistrer");
define(_ELIMINA_, "Eliminer");
define(_AGGIUNGI_, "Ajouter");
define(_VISUALIZZA_, "Visualiser");
define(_INVIA_, "Envoyer");
define(_CHIUSURA_, "Cl&ocirc;ture de la n&eacute;gociation");
define(_INDIETRO_, "< En arri&egrave;re");
define(_CONFERMA_, "Confirmer");
define(_DINIEGA_, "Refuser");
define(_SOSPESO_, "Suspended");
define(_CHIUDI_, "Fermer");
define(_RICHIESTA_, "Envoyer Demande");
define(_RICHIESTE_, "Demande");
define(_MODIFICA_, "Modifier");
define(_ATTESA_, "Attente");
define(_CONFERMATO_, "Confirm&eacute;");
define(_ANTEPRIMA_, "Preview");
define(_G_LINK_, "Regenerate Link");
define(_ACCEDI_, "Login");
define(_REGISTRATI_, "Register");
define(_CAMBIA_, "Change");
define(_EXTRA_B_, "Negotiation extra bulletin board");
define(_ALL_, "All");
define(_SELEZIONA_B_, "S&eacute;lectionnez");

//MSG RISPOSTA
define(_PRODOTTO_CATEGORIA_, "The category you are clearing is associated with some products.");
define(_CAMPI_OB_CAT_, "Select at least one category.");
define(_EMAIL_ER_, "E-Mail field not valid");
define(_SMART_CARD_, "Smart Card and press enter login.");
define(_OK_INVIO_LINK_, "<h2>You have been sent an email with a confirmation link recording</h2>");
define(_PASSWORD_ERR_, "<h2>Password is incorrect</h2>");
define(_TRA_INCORSO_, "For these products are already in the negotiations:");
define(_CAMPI_OB_, "Les param&egrave;tres signal&eacute;s par un ast&eacute;risque sont obligatoires.");
define(_CAMPI_OB_TR_, "Enter a text for all languages.");
define(_CAMPI_PAS_NO_, "Invalid Password");
define(_CAMPI_PAS_UG_, "Password does not match confirmation");

define(_OK_ATTIVAZINE_, "Activation was successful");

define(_MITTENTE_, "Exp&eacute;diteur");
define(_DESTINATARIO_, "Destinataire");
define(_OGGETTO_, "Objet");
define(_DATA_, "Date");
define(_OGGETTO_MAIL_, "Demande de devis pour ");
define(_MESSAGGIO_, "Message");
define(_ALLEGATO_, "Pi&egrave;ce jointe");

//MAIL DI RISPOSTA
define(_N_RISP_, "<br /><br />Automatically generated message please do not respond.");
define(_OGG_DATI_UTENTE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Data recording");
define(_MSG_DATI_UTENTE_, "<p>Dear user,<br /> 
we confirm successful registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
Here are your Username and Password: <br>
<br />
USER: {%user%} <br />
<br />
PASSWORD: {%Password%} <br />
<br /> <br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_AGG_I_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_AGG_I_, "<p>Dear {%utente%},<br /> 
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has assigned the role of Managing Director of the Company in the Catalogue Goods and Services Suppliers Expo 2015. 
Can access the catalog with the logon credentials that are already in its possession 
and then select the / Enterprise which is already written for you accederne to content. <br /><br />
Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_REGISTRAZIONE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Enable Personal Profile");
define(_MSG_NEW_REG_, "<p>Dear {%utente%},
���� <br /> we confirm the registration to Catalogue Goods and Services Suppliers Expo 2015 <br />
By clicking on the link below which will activate your registration. <br />
���� {%link%} <br /><br />
���� Regards <br /></ p>{%TITLESITE%}"._N_RISP_);
define(_OGG_DELETE_, "[EXPO2015 Catalog Suppliers Goods & Services  Site Administrator] - Notification Cancellation Personal Profile");
define(_MSG_DEL_REG_DEF_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>, as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
carried out the cancellation of his role as Chief Executive Officer of the Company. <br> 
Since it is not included on other companies in the Catalog its user has been deleted and can no 
longer use the login credentials in his possession. <br> For any information to contact regarding the 
direction of EXPO. 
<br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_MSG_DEL_REG_, "<p>Dear {%utente%}, <br />
we confirm that <strong>{%lg%}</ strong>,as Legal Representative of the Company <strong>{%RagSoc%}</ strong>,
has reset the memory of his role as Chief Executive Officer of the Company. <br />
Can still access the catalog with the logon credentials that are already in its possession and then select the / Enterprise which is already written for you accederne to content.
    <br /> <br /> Regards <br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_BACHECA_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - negotiation ID #{%idBacheca%}");
define(_MSG_NOTIFICA_TRA_, "<p> Dear {%user%}  <br />
���� the user <strong>{%mittente%}</ strong> has sent you a message. <br />
���� You can find it in the Message section of your personal area of the Goods and Services Catalog Suppliers Expo 2015. <br />
���� <br />
���� Regards <br /> </ p>{%TITLESITE%}
    <br />-----------------------------------<br />
    <p>Gentile {%utente%} ,<br />
    l&#39;utente  <strong>{%mittente%}</strong> ti ha inviato un messaggio. <br />
    Lo trovi nella sezione Bacheca della tua area personale del Catalogo Fornitori Beni e Servizi Expo 2015.<br />
    <br />
    Distinti Saluti <br /> </p>{%TITLESITE%}"._N_RISP_);

define(_OGG_NOT_CD_TRA_I_, "[EXPO2015 Catalogo Fornitori Beni & Servizi - Bacheca Messaggi]  Trattativa ID #{%idTrattativa%}  ({%oggetto%})");
define(_MSG_NOT_CD_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation, we inform you that the Enterprise {%Impresa%}
<strong>refused</strong> the closing of the deal. And &#39;possible to read the <strong>reasons</strong> for the refusal and send
message logging Showcase Area Board of Participants.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto, la informiamo che l�Impresa {%Impresa%} 
ha <strong>rifiutato</strong> la chiusura della trattativa. E� possibile leggere le motivazioni del rifiuto ed inviare 
un messaggio accedendo nell�Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CD_TRA_P_, "[EXPO2015 Catalogo Fornitori Beni & Servizi � Bacheca Messaggi]  Trattativa ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CD_TRA_P_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation with Company {%Impresa%}, we inform you that the Participant {%mittente%} 
<strong>refused</strong> the closure of the deal. <br>
You can read the reasons for the refusal and send a message logging Area Board
Suppliers of Goods & Services Catalog.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha <strong>rifiutato</strong> la chiusura della trattativa.<br> 
E� possibile leggere le motivazioni del rifiuto ed inviare un messaggio accedendo nell�Area Bacheca 
del Catalogo Fornitori Beni & Servizi.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_I_, "<p>Good morning / Good evening {%utente%}, <br>
with reference to the aforementioned negotiation,
we inform you that the company has <strong>confirmed</strong> {%Impresa%}
the closure of the negotiation<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera {%utente%},<br>
con riferimento alla Trattativa Commerciale in oggetto,
la informiamo che l�Impresa  {%Impresa%} ha <strong>confermato</strong> 
la chiusura della trattativa.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_CC_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_CC_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation,
we inform you that Participant {%mittente%} has confirmed the closing of the deal.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%}, 
la informiamo che il Partecipante {%mittente%} ha confermato la chiusura della trattativa<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_I_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_I_, "<p>Good morning / Good evening , <br>
with reference to the aforementioned negotiation,
we inform you that company {%Impresa%} responsible for sealing the deal.
It requires you to confirm whether or not the outcome of the Showcase Participants accessing Area Board.<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera ,<br>
con riferimento alla Trattativa Commerciale in oggetto, 
la informiamo che l�Impresa {%Impresa%} ha proceduto alla chiusura della trattativa. 
Si richiede di confermarne o meno l�esito accedendo nell�Area Bacheca della Vetrina Partecipanti.<br>
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);
define(_OGG_NOT_A_TRA_P_, "[EXPO2015 Catalog Suppliers Goods & Services - Message Board] - Negotiation ID #{%idTrattativa%} ({%oggetto%})");
define(_MSG_NOT_A_TRA_P_, "<p>Good morning / Good evening, <br>
with reference to the aforementioned negotiation with Company {%Impresa%},
we inform you that Participant {%mittente%} responsible for sealing the deal. <br>
It requires you to confirm whether or not the outcome accessing Area Wall Suppliers Catalog Goods & Services<br />
Best Regards <br /> </ p> {%TITLESITE%}
<br /><br />-----------------------------------<br />
<p>Buongiorno/Buonasera,<br>
con riferimento alla Trattativa Commerciale in oggetto con l�Impresa {%Impresa%},  
la informiamo che il partecipante {%mittente%} ha proceduto alla chiusura della trattativa.<br>
Si richiede di confermarne o meno l�esito accedendo nell�Area Bacheca del Catalogo Fornitori Beni & Servizi<br />
Cordiali Saluti<br /></p>{%TITLESITE%}"._N_RISP_);

//GESTIONE LINGUE
define(_ITALIANO_, "Italian");
define(_INGLESE_, "English");
define(_FRANCESE_, "French");

//MODIFICA 25/09/2013
define(_FILE_NO_UP_, "<h2>Unable to upload.</h2>");
define(_FILE_MAX_, "<h2>File is too big.</h2>");
define(_ENTRA_SC_, "Sign in using Smart Card");
define(_ENTRA_LOG_, "Sign in using credentials recorded");
define(_DIM_PAS_, "Forgot your password?");
define(_REG_, "Registering");

//MODIFICA 26/09/2013
define(_RICERCA_, "Find");
define(_IMPRESE_, "Companies");
define(_AGGIUNGI_, "Add parameter");
define(_CERCA_PAR_, "SEARCH FOR PARAMETERS");
define(_ANNULLA_, "Cancel");

//MODIFICA 30/09/2013
define(_UTENTE_RLN_, "Link forwarded.");
define (_NUMERO_OGGETTI_BACHECA_,"There are %n% messages");

//MODIFICA 01/10/2013
define(_NO_PRODOTTI_,"To be able to enter new Products/Services, you must subscribe to at least one Product Category");

define (_AGGIUNGI_CATEGORIA_,"Add Product Category");
define (_GESTISCI_CATEGORIA_,"Manage Product Category");
define (_NESSUNA_CATEGORIA_,"None Product Category registered");

// MODIFICA DEL 02/10/2013
define(_DESCRIZIONE_FILE_,"Description");
define(_BILANCI_FILE_,"Budget");
define(_INTERNAZIONALIZZAZIONE_FILE_,"Internationalization");
define(_REFERENZE_FILE_,"R&eacute;f&eacute;rences");
define(_QUALITA_FILE_,"Qualit&eacute;");
define(_RETI_FILE_,"Formes d&rsquo;agr&eacute;gation");

define(_PRODOTTO_FILE_,"Produit/Service");
define(_CERTIFICAZIONE_FILE_,"Certification");
define(_ASPETTI_FILE_,"Aspects");
define(_BREVETTI_FILE_,"Brevets");

define(_FILELINK_V_, "See File/Web link below for details<br>");

// MODIFICA DEL 04/10/2013
define(_NO_TRATTATIVA_,"Nessuna Trattativa presente FR");
define(_NO_BACHECA_,"Nessun Messaggio in Bacheca FR");
define(_NO_DELEGATI_,"Nessun Delegato Associato all'Impresa FR");
define(_IsPrestatoreDiServizi_,"Fournisseur de services");

//MODIFICA 05/11/2013
define(_P_MASTER_FILE_, "Master sp&eacute;cialis&eacute; / Doctorat de recherche");
define(_P_CERTIFICAZIONE_FILE_, "Certification SOA");
define(_CERTIFICAZIONI_FILE_, "Certification");

//MODIFICA 27/12/2013
define(_TEXT_HOME_1_, "<span>Bienvenue dans le Catalogue des Fournisseurs pour les Participants &agrave; Expo 2015!<br></span>
L&#039;environnement virtuel pour se pr&eacute;senter aux Pays Participants et <br>les aider &agrave; concevoir, construire et 
am&eacute;nager leur pavillon, gr&acirc;ce &agrave; la fourniture de services et de produits.");
define(_TEXT_HOME_2_, "<span><br>Que peut-on faire avec le catalogue?</span>");
define(_TEXT_HOME_3_, "<a href='index.phtml?Id_VMenu=308'>Cliquer ici</a> pour acc&eacute;der au catalogue");
define(_TEXT_HOME_4_, "<div><strong>S&#039;ENREGISTRER</strong> en ins&eacute;rant les donn&eacute;es d&#039;&eacute;tat civil du repr&eacute;sentant l&eacute;gal.<br><a class=\"blu\" href=\"index.phtml?Id_VMenu=502\" target=\"blank\">Consulter la Guide d&#039;inscription</a></div>");
define(_TEXT_HOME_5_, "<div><strong>ACCEPTER<strong> le r&egrave;glement d&#039;inscription<br><a class=\"blu\" href=\"ajaxlib/requester/downloadFile.php\" target=\"blank\">T&eacute;l&eacute;charger la R&eacute;glementation</a></div>");
define(_TEXT_HOME_6_, "<div><strong>COMPL&Eacute;TER</strong> le profil de l&#039;entreprise <br>en ins&eacute;rant la cat&eacute;gorie commerciale d&#039;appartenance, <br>les caract&eacute;ristiques, les certifications et les r&eacute;f&eacute;rences<br><a class=\"blu\" href=\"index.phtml?Id_VMenu=500\" target=\"blank\">Liste des Cat&eacute;gories</a></div>");
define(_TEXT_HOME_7_, "<div>Approfondir la pr&eacute;sentation des services et des produits fournis.</div>");
define(_TEXT_HOME_8_, "<div>&Ecirc;tre contact&eacute; par les Pays Participants en recevant des demandes au travers du syst&egrave;me de messagerie.</div>");

// MODIFICA 08/11/2013
define(_NOTE_15_, "Les attestations SOA sont indiqu&eacute;es seulement pour faciliter le choix de la cat&eacute;gorie 
d&rsquo;appartenance, mais leur possession n&rsquo;est pas indispensable.");
//MODIFICA 20/11/2013
define(_CERTIFICATO_NO_,"Allegare il file PDF firmato");
define(_CATEGORIE_MERCIOLOGICHE_, "Cat&eacute;gories<br>de Produits ");
define(_NESSUNA_LINGUA_, "Aucune langue s&eacute;lectionn&eacute;e");

//MODIFICA 10/12/2013

define(_SIEXPO_NOTE_1_,"Disponible dans le Catalogue Siexpo");

//MODIFICA  02/04/2014 PROFESSIONISTI
define(_ORDINE_, "Ordine Professionale");
define(_ORDINE_TIPO_ISCRIZIONE_, "Iscrizione in qualit&agrave; di:");
define(_STATO_ALBO_, "Stato Iscrizione<br/> Albo Nazionale");
define(_CODICE_FISCALE_, "Codice Fiscale");
define(_NUMERO_ISCRIZIONE_, "Numero Iscrizione");
define(_COF_DATI_PROFSSIONISTA_,"Nel riquadro sottostante si riportano le informazioni tratte dall'Albo Nazionale.");
define(_ALTRO_STUDIO_,"Studio di associati");

define(_TIT_1_LP_, "Donn&eacute;es du Professionnel");
define(_TIT_1_STUDIO_, "Donn&eacute;es du Cabinet");
define(_N_DIP_ORDINI_, "Nombre de pr&eacute;pos&eacute;s ou de collaborateurs");

define(_QUALIT_LP_, "Caract&eacute;ristiques du professionnel");
define(_QUALIT_STUDIO_, "Caract&eacute;ristiques du cabinet");
define(_SEDI_LP_, "Degr&eacute; d'internationalisation du professionnel");
define(_SEDI_STUDIO_, "Degr&eacute; d'internationalisation du cabinet");
define(_RAGSOC_PROF_, "Nom/D&eacute;nomination");
define(_APP_RETE_IMP_PROF_, "Appartenance &agrave; un regroupement (ex. : r&eacute;seaux, consortiums, associations)");

define(_IMPRESA_OFFLINE_PRODOTTI_, "L'impresa &egrave; offline. I prodotti/servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_LP_, "Il Professionista &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");
define(_IMPRESA_OFFLINE_PRODOTTI_STUDIO_, "Lo studio &egrave; offline. I servizi, anche se online, non saranno visibili nella ricerca");

define(_PROFESSIONISTA_, "Professionnel");
define(_STUDIO_, "Cabinet");
define(_PUBBLICA_ALL_PROF_, "Services publics");

define(_SERVIZI_, "Liste des services");
define(_SERVIZIO_, "Service");
define(_NO_PRODOTTI_PROF_,"Per poter inserire nuovi servizi è necessario iscrivere l'Impresa ad almeno una Categoria Merceologica");

define(_PRODOTTO_SAVE_,"Prodotto salvato con successo");
define(_SERVIZIO_SAVE_,"Servizio salvato con successo");

//Aggiunto 09 Maggio 2014
define(_ANT_NOME_RETE_, "R&eacute;seau: ");
define(_DESCR_RETE_, "Description: ");
define(_ANT_CONTATTO_RETE, "Contacter le R&eacute;seau: ");
define(_IMPRESE_RETE_,"<strong>D'autres entreprises et membres du R&eacute;seau au catalogue</strong><br/>");
define(_ANT_IMP_CAPOG_,"Chef de groupe de l'entreprise");
define(_TEXT_HOME_9_,"<div>Recherche produits durables et innovants sur <a class=\"blu\" href=\"http://www.siexpo2015.it/index.phtml?Id_VMenu=322\" target=\"_blank\">SIEXPO</a></div>");
define(_NO_RETI_,'Pas de r&eacute;seau s&eacute;lectionn&eacute;');

define(_DISCLAIMER_CATEGORIE_DEFAULT_PDMS_ ,"Les produits/services inclus dans cette cat&eacute;gorie commerciale sont fournis par le Partenaire/Sponsor d&#039;Expo 2015, dont l&#039;offre est d&eacute;taill&eacute;e dans le <b>Catalogue Des Partenaires Officiels</b> (http://catalogue.expo2015.org). D&#039;autres entreprises peuvent toutefois &ecirc;tre consult&eacute;es en poursuivant la recherche.");

?>