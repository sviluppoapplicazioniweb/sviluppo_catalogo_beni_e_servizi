<?php

if ($auth->auth["utype"]=="")  $utype2=99;
else $utype2=$auth->auth["utype"];

if ($Id_VMenu==8)
	include $PROGETTO."/dynamic_menu.inc";

$force_menu['tipo-menu']="mappa";

if ($utype>3 && $tipomappa=="admin")
	$tipomappa="";

$db_menu = new DB_CedCamCMS;
$db_menu2 = new DB_CedCamCMS;
if($area!="")
{
	$SQLmenu="select Id_Menu from ".$tabsvil."T_Menu where Id='$area'";
	$db_menu->query($SQLmenu);
	$db_menu->next_record();
	$area2=$db_menu->f('Id_Menu');
}
$SQLmenu="select Mtop from ".$tabsvil."T_Menu where Id_Menu='$explode'";
$db_menu->query($SQLmenu);
$db_menu->next_record();
$mtop=$db_menu->f('Mtop');

$SQLmenu="select ".$tabsvil."T_Menu.Id, ".$tabsvil."T_Menu.Id_Menu, ".$tabsvil."T_Menu.Gruppi, ".$tabsvil."T_Menu.Id_Sottomenu, ".$tabsvil."T_Menu.Mtop, ".$tabsvil."T_Menu.Stato_Menu, ".$tabsvil."T_Menu.Tipo, ".$tabsvil."T_Menu.Esploso, ".$tabsvil."T_Menu.Titolo, ".$tabsvil."T_Menu.Flag_Struttura, ".$tabsvil."T_Menu.Flag_Modifica, ".$tabsvil."T_Menu.Id_Modifica, ".$tabsvil."T_Menu.LinkEsterno, T_Pagine.Link, T_Pagine.Flag_Pubblico";
if ($Lang!=$LinguaPrincipale)
	$SQLmenu.=", ".$tabsvil."T_Menu_AL.Titolo_AL, ".$tabsvil."T_Menu_AL.LinkEsterno_AL";
$SQLmenu.=" from ".$tabsvil."T_Menu, T_Pagine";
if ($Lang!=$LinguaPrincipale)
	$SQLmenu.=", ".$tabsvil."T_Menu_AL";
$SQLmenu.=" where ".$tabsvil."T_Menu.Id_Pagina=T_Pagine.Id";
if ($Lang!=$LinguaPrincipale)
{
	$SQLmenu.=" and ".$tabsvil."T_Menu_AL.Id_AL=".$tabsvil."T_Menu.Id and ".$tabsvil."T_Menu_AL.Lang_AL='".$Lang."'";
	if ($noPredefinita) $SQLmenu.=" and (".$tabsvil."T_Menu_AL.Titolo_AL<>'' or ".$tabsvil."T_Menu.Id_Menu>='80')";
}
$SQLmenu.=" and (".$tabsvil."T_Menu.Tipo='M' or ".$tabsvil."T_Menu.Tipo='L'";
//if ($tipomappa=="admin")
	$SQLmenu.=" or ".$tabsvil."T_Menu.Tipo='P'";
$SQLmenu.=")";
if ($tipomappa=="admin")
{
	$SQLmenu.=" and (".$tabsvil."T_Menu.Id_Menu>='01' or ".$tabsvil."T_Menu.Id_Menu='00') and ".$tabsvil."T_Menu.Id_Menu<'80'";
	if ($utype>2)
		$SQLmenu.=" and T_Pagine.Flag_Pubblico=1";
	if (($utype==2 || $utype==3) && $mastereditor==0)
		$SQLmenu.=" and (instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|')) or instr('$myabil2',concat('|',substring(Id_Menu,1,5),'|')))";
//		$SQLmenu.=" and instr('$myabil2',concat('|',substring(Id_Menu,1,2),'|'))";
}
if ($Priorita[$utype2]>=8 && $tipomappa=="admin")
	$SQLmenu.=" and (".$tabsvil."T_Menu.Stato_Menu='A' or ".$tabsvil."T_Menu.Stato_Menu='D')";
else
	$SQLmenu.=" and ".$tabsvil."T_Menu.Stato_Menu='A'";

if ($tipomappa=="admin")
{
	if($area!="")
		$SQLmenu.=" and (".$tabsvil."T_Menu.Id_Menu like '".$area2."%' or ".$tabsvil."T_Menu.Id_Menu like '__')";
	else
		$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Menu like'__'";
}


//$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.Id_TipoUtente=".$utype2." or ".$tabsvil."T_Menu.Id_TipoUtente=0)";
if ($tipomappa=="admin")
	$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2];
else
	$SQLmenu.=" and ".$tabsvil."T_Menu.Id_Priorita<=".$Priorita[$utype2]." and (".$tabsvil."T_Menu.TipiUtente like '%|".$utype2."|%' or ".$tabsvil."T_Menu.TipiUtente='|' or ".$tabsvil."T_Menu.TipiUtente='|0|')";

// Aggiungere controllo accessi Gruppi

$SQLmenu.=" order by Id_Menu, Tipo, Id_Sottomenu";

//echo $SQLmenu;
$gruppiout="";
if ($DEBUG) echo "<!-- SQLmenu ".$SQLmenu."-->\n";
if ($db_menu->query($SQLmenu))
{
	$pages=array();
	$i=1;
	while($db_menu->next_record())
	{

	  if (checkgroup($db_menu->f('Gruppi')) && ((substr($db_menu->f('Id_Menu'),0,strlen($gruppiout))!=$gruppiout) || $gruppiout==""))
	  {
			if(!strncmp($db_menu->f('Link'),"http",4) || !strncmp($db_menu->f('Link'),"mailto",6)) // � un programma esterno
				$link= preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_menu->f('Link')); // PM041021
			else
				$link="/index.phtml?Id_VMenu=".$db_menu->f(Id);
			$esploso="S";
			if ($Lang!=$LinguaPrincipale && $db_menu->f('Titolo_AL')!="")
				$titolo=$db_menu->f('Titolo_AL');
			else
				$titolo=$db_menu->f('Titolo');
//echo "-1-",$db_menu->f('Id')." ".$titolo."--<br />";
	// Controllo che serve a far vedere la voce di menu principale ai redattori non abilitati.		
/*
			if(strlen($db_menu->f('Id_Menu'))=="5" && $utype!=1 && $mastereditor!=1 && strpos($myabil2,"|".substr($db_menu->f('Id_Menu'),0,2)."|")===false)
			{
				//$SQLmenu2="select * from ".$tabsvil."T_Menu where Id_Menu='".substr($db_menu->f('Id_Menu'),0,2)."'";
				$SQLmenu2="select * from ".$tabsvil."T_Menu where Id_Menu='".$db_menu->f('Id_Menu')."'";
				$db_menu2->query($SQLmenu2);
				$db_menu2->next_record();
				if ($Lang!=$LinguaPrincipale && $db_menu2->f('Titolo_AL')!="")
					$titolo2=$db_menu2->f('Titolo_AL');
				else
					$titolo2=$db_menu2->f('Titolo');
//echo $SQLmenu2."-".$db_menu2->f('Id')." ".$titolo2."--<br />";
				$pages[$db_menu2->f('Tipo').$db_menu2->f('Id_Menu')]=array($link=>$titolo2, target=>"", sub=>$db_menu2->f('Id_Sottomenu'), stato=>$db_menu2->f('Stato_Menu'), tipo=>$db_menu2->f('Tipo'), esploso=>$esploso, mtop=>$db_menu2->f('Mtop'), primo=>"N", Id_VMenu=>$db_menu2->f('Id'), flag_struct=>$db_menu2->f('Flag_Struttura'), flagmod=>$db_menu2->f('Flag_Modifica'), idmod=>$db_menu2->f('Id_Modifica'));
			}
//echo $db_menu->f('Id')." ".$titolo."--<br />";
*/
	// echo "<!-- ID ".$db_menu->f(Id)."-->\n";
//echo "<!-- i ".$i." --- ".$db_menu->f('Titolo')." -->\n";
			if ($i==1)
				$pages[$db_menu->f('Tipo').$db_menu->f('Id_Menu')]=array($link=>$titolo, target=>"", sub=>$db_menu->f('Id_Sottomenu'), stato=>$db_menu->f('Stato_Menu'), tipo=>$db_menu->f('Tipo'), esploso=>$esploso, mtop=>$db_menu->f('Mtop'), primo=>"S", Id_VMenu=>$db_menu->f('Id'), flag_struct=>$db_menu->f('Flag_Struttura'), flagmod=>$db_menu->f('Flag_Modifica'), idmod=>$db_menu->f('Id_Modifica'));
			else
				$pages[$db_menu->f('Tipo').$db_menu->f('Id_Menu')]=array($link=>$titolo, target=>"", sub=>$db_menu->f('Id_Sottomenu'), stato=>$db_menu->f('Stato_Menu'), tipo=>$db_menu->f('Tipo'), esploso=>$esploso, mtop=>$db_menu->f('Mtop'), primo=>"N", Id_VMenu=>$db_menu->f('Id'), flag_struct=>$db_menu->f('Flag_Struttura'), flagmod=>$db_menu->f('Flag_Modifica'), idmod=>$db_menu->f('Id_Modifica'));
			$i++;
			$gruppiout="";
		}
		else
		{
			if ($gruppiout=="")
				$gruppiout=$db_menu->f('Id_Menu');
		}
	}
}
else
	if ($DEBUG) echo "Errore SQLmenu: ".$SQLmenu."\n";

$menustring="";
$anchorstring="<p class=\"center\">";
if(!empty($explode))
	show_nav($explode);
else
	show_nav("00");

if ($tipomappa=="admin")
	$anchorstring.="<br /><a href=\"/index.phtml?Id_VMenu=".$Id_VMenu."&amp;area=_\"><!--htdig_noindex-->Mappa completa<!--/htdig_noindex--></a><br />\n";
$anchorstring.="</p>";

if ($utype<3)
{
	$linkpubblica="<li class=\"sottotitolo\"><div class=\"right\"><a href =\"/index.phtml?Id_VMenu=68\" title=\"Pubblicazione\">Pubblica pagine approvate</a><br />\n";
	$linkpubblica.="(<img src=\"/images/admin/check_red.gif\" alt=\"\" /> In modifica |\n";
	$linkpubblica.="<img src=\"/images/admin/check_blue.gif\" alt=\"\" /> Da approvare |\n";
	$linkpubblica.="<img src=\"/images/admin/check_green.gif\" alt=\"\" /> Approvato)\n";
	$linkpubblica.="</div></li>\n";
}

	$codice_html=file_get_contents($PATHDOCS."tmpl/mappa".$tipomappa.".html");
	echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$codice_html);

?>
