<?php
$db_menu = new DB_CedCamCMS;
$db_html = new DB_CedCamCMS;

if ($auth->auth["utype"]=="")
	$utype2=99;
else
	$utype2=$auth->auth["utype"];

$colonne=1;
if (!(isset($tipoart)))
	$tipoart=1;

if(!(isset($daabstract)))
{
	$SQLhtml="select ".$tabsvil."T_Articoli.*,Tlk_TipiHtml.Testo_TipiHtml";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=", ".$tabsvil."T_Articoli_AL.*";
	$SQLhtml.=" from ".$tabsvil."T_Articoli,Tlk_TipiHtml,Tlk_TipoArticolo3";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=",".$tabsvil."T_Articoli_AL";
	$SQLhtml.=" where ".$tabsvil."T_Articoli.Id_TipoArticolo2 like '%|".$expmenu."|%' and ".$tabsvil."T_Articoli.Id_TipoArticolo3=".$tipoart." and Tlk_TipoArticolo3.Id=".$tipoart." and Tlk_TipiHtml.Descrizione=Tlk_TipoArticolo3.Descrizione";
	if ($Lang!=$LinguaPrincipale)
	{
		$SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Id_AL=".$tabsvil."T_Articoli.Id_Articolo and ".$tabsvil."T_Articoli_AL.Lang_AL='".$Lang."'";
		if ($noPredefinita) $SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Titolo_Html_AL<>''";
	}

	$box=0;
	if ($db_html->query($SQLhtml))
	{
		$TipoTesto="Abstract";
		while($db_html->next_record())
		{
			if ($utype2<=3 && $preview!=1)
				include $PROGETTO."/admintool.inc";
			if ($Lang!=$LinguaPrincipale)
				$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_AL');
			else
				$titolo_barra=$db_html->f('Titolo_'.$TipoTesto);
			//$object_width=$object_width-($margine*2);
			$object_width_int=$object_width-20;
			if ($Lang!=$LinguaPrincipale)
				$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_AL'));
			else
				$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto));

			if ($Lang!=$LinguaPrincipale)
				$link=$db_html->f('Link_'.$TipoTesto.'_AL');
			else
				$link=$db_html->f('Link_'.$TipoTesto);

			if (is_numeric($link))
				$link= "index.phtml?Id_VMenu=".$link;
			elseif ($link=="")
				$link= "index.phtml?Id_VMenu=".$Id_VMenu."&daabstract=".$db_html->f('Id_Articolo');

			echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_TipiHtml'));
		}
	}
}
else
{
	$colonne=1;
	$object_width=floor(($center_width2/$colonne)-($margine*2));

	$SQLhtml="select ".$tabsvil."T_Articoli.*,Tlk_TipiHtml.Testo_TipiHtml";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=", ".$tabsvil."T_Articoli_AL.*";
	$SQLhtml.=" from ".$tabsvil."T_Articoli,Tlk_TipiHtml";
	if ($Lang!=$LinguaPrincipale)
		$SQLhtml.=",".$tabsvil."T_Articoli_AL";
	$SQLhtml.=" where Id_Articolo=".$daabstract." and Tlk_TipiHtml.Id=2";
	if ($Lang!=$LinguaPrincipale)
	{
		$SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Id_AL=".$tabsvil."T_Articoli.Id_Articolo and ".$tabsvil."T_Articoli_AL.Lang_AL='".$Lang."'";
		if ($noPredefinita) $SQLhtml.=" and ".$tabsvil."T_Articoli_AL.Titolo_Html_AL<>''";
	}
//	if ($Lang!=$LinguaPrincipale)
	if ($db_html->query($SQLhtml))
	{
		$db_html->next_record();
		$TipoTesto="Html";
		if ($utype2<=3 && $preview!=1)
			include $PROGETTO."/admintool.inc";
		if ($Lang!=$LinguaPrincipale)
			$titolo_barra=$db_html->f('Titolo_'.$TipoTesto.'_AL');
		else
			$titolo_barra=$db_html->f('Titolo_'.$TipoTesto);
		//$object_width=$object_width-($margine*2);
		$object_width_int=$object_width-20;
		if ($Lang!=$LinguaPrincipale)
			$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto.'_AL'));
		else
			$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_'.$TipoTesto));

		if (substr($corpo, 0,3)=="<p>" && (strpos($corpo,"<p",1) === false) && substr($corpo, -4)=="</p>")
			$corpo=substr($corpo,3,-4);

		$corpo=preg_replace("/\{%(\w+)%\}/e", "\$\\1",$corpo);

		if ($Lang!=$LinguaPrincipale)
			$link=$db_html->f('Link_'.$TipoTesto.'_AL');
		else
			$link=$db_html->f('Link_'.$TipoTesto);

		if (is_numeric($link))
			$link= "index.phtml?Id_VMenu=".$link;

		echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$db_html->f('Testo_TipiHtml'));
	}
}

?>