<script type="text/javascript">


function checkPasswordPolicy(f){
    	var pattern_email=/((?=.*\d)(?=.*[a-zA-Z])).{8,16}/;
    	var pattern_email_NoSpecialChar=/[@%;]/;
    	
    	var password = f.Password.value;
		var conferma_password = f.Conferma_Password.value;
		if(conferma_password.length>0 && password.length>0){
			if(password==conferma_password){
			
				if(pattern_email.test(password) && !pattern_email_NoSpecialChar.test(password)){//
					return true;
				}else{
					alert("ATTENZIONE!! Hai inserito una password non conforme ai criteri di sicurezza. Sono valide le password che hanno una lunghezza compresa tra gli 8 e i 16 caratteri e contengono almeno una lettera e un numero.");
					return false;
				  }
			}
			else
			{
						alert("ATTENZIONE!! Le password inserite sono diverse.");
					return false;
			}
		}
		else{
			alert("ATTENZIONE!! Per procedere &egrave; necessario compilare tutti i campi.");
			return false;
		}
}


</script>

<?
$db = new DB_CedCamCMS;
//include_once "send_mail.inc";
include_once "view/lib/passwd_function.inc";

$testo="";

echo "<div align=\"center\">\n";


if(isset($modulo))
{
	$tk=$_POST['tk'];
	$pass=$_POST['Password'];
	$confpass=$_POST['Conferma_Password'];
	
	if(checkToken($tk) && $pass==$confpass && checkPasswordPolicy($pass)){
			
		//hasn della password
		$hashpass= hash("sha256",$pass);
		//recupero l utente dal token
		$SQL="select * from T_Token where Token='".$tk."'";
		$db->query($SQL);
		$db->next_record();
		$iduser=$db->f("Id_Utente");
		
		//update password  e scadenza
		$scadenza_password = date('Y-m-d H:s:i', mktime(date(H),date(s),date(i),date(m),date(d),date(Y)+1));
		$SQL="UPDATE T_Anagrafica SET Password='".$hashpass."', Scadenza_Password='".$scadenza_password."' WHERE Id=".$iduser; 
		$db->query($SQL);
		$db->next_record();
				
		//unset token
		unsetToken($tk);
		$okurl ="";
		
		echo "<p align='left'><strong>Password modificata con successo!</strong></p>";
		echo "<p align='left'>Ora puoi accedere al sito con la tua nuova password. </p><br/>";
	}
	else{		
		echo "<p align='left'><br/>ATTENZIONE! Si &egrave; verificato un errore. <br/>
		Hai inserito una password non conforme ai criteri di sicurezza. Sono valide le password che hanno una lunghezza compresa tra gli 8 e i 16 caratteri e contengono almeno una lettera e un numero.
		<br/>";
	}
		
	
}
else
{
	if(isset($_GET['tk'])){
		$tk=$_GET['tk'];
		if(checkToken($tk)){
		
					
			echo "<p align='left'><br/>Gentile Utente,<br/>";
			echo "in questa pagina puoi inserire la tua nuova password. Ti ricordiamo che sono valide le password che hanno una lunghezza compresa tra gli 8 e i 16 caratteri e contengono almeno una lettera e un numero.<br/></p>";
			echo "<form name=\"modulo\" action=\"/index.phtml?Id_VMenu=49\" method=\"post\" onsubmit=\"return checkPasswordPolicy(this);\">\n";
			echo "<fieldset>";
			echo "<div class=\"box2form\">";
			echo "<br style=\"clear: left;\">";
			echo "</div>";
			echo "<div class=\"box2form\">";
			echo "<div class=\"form_label\">";
			echo "<label for=\"posta\">Password</label>";
			echo "</div>";
			echo "<div>";
			echo "<input type=\"hidden\" name=\"tk\" id=\"tk\" value=\"".$tk."\" />\n";
			echo "<td><input type=\"password\" name=\"Password\" id=\"Password\" size=\"30\" maxlength=\"100\" />\n";
			echo "</div>";
			echo "<div class=\"form_label\">";
			echo "<label for=\"posta\">Conferma password</label>";
			echo "</div>";
			echo "<div>";
			echo "<td><input type=\"password\" name=\"Conferma_Password\" id=\"Conferma_Password\" size=\"30\" maxlength=\"100\" />\n";
			echo "</div>";
			echo "<div  style=\"width:35%;height:0px\">";
			echo "</div>";
			echo "<div>";
			echo "<input type=\"submit\" name=\"modulo\" id=\"modulo\" value=\"Esegui\" />\n";
			echo "</div>";
			echo "<br style=\"clear: left;\">";
			echo "</div>";
			echo "</fieldset>";
			echo "</form>\n";
					
		}
		else{
			echo "<p><strong><br/>ATTENZIONE! Si &egrave; verificato un errore. <br/> Il link utilizzato non &egrave; valido, &egrave; scaduto, o &egrave; gi&agrave; stato utilizzato</strong></p>\n";
		}
		
	}
	else{
		echo "<p><strong><br/>ATTENZIONE! Si &egrave; verificato un errore. <br/> Il link utilizzato non &egrave; valido, &egrave; scaduto, o &egrave; gi&agrave; stato utilizzato</strong></p>\n";
	}
	
	
		
}
echo "</div>";


		
?>