<?php

$db_titolo = new DB_CedCamCMS;
$db_titolo2 = new DB_CedCamCMS;

$SQLtit = "select Id, Titolo, Help_HTML, MTop, Descrizione, Keywords, Popup, Data_modifica, Data_Pubblicazione, LinkEsterno";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=", Titolo_AL, Descrizione_AL, Keywords_AL, Popup_AL, LinkEsterno_AL";
$SQLtit.=" from " . $tabsvil . "T_Menu";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=", " . $tabsvil . "T_Menu_AL";
$SQLtit.=" where Id_Menu='" . $explode . "' and Tipo<>'L'";
if ($Lang != $LinguaPrincipale)
    $SQLtit.=" and " . $tabsvil . "T_Menu_AL.Id_AL=" . $tabsvil . "T_Menu.Id and " . $tabsvil . "T_Menu_AL.Lang_AL='" . $Lang . "'";
if ($Priorita[$utype] > 8)
    $SQLtit.=" and (Stato_Menu='A' or Stato_Menu='D')";
else
    $SQLtit.=" and Stato_Menu='A'";

if ($db_titolo->query($SQLtit))
    $db_titolo->next_record();

$titolo_abs = "";

if (isset($daabstract) && (!(is_numeric($daabstract))))
    unset($daabstract);

if (isset($daabstract)) {
    $SQLtit2 = "select Titolo_Html";
    if ($Lang != $LinguaPrincipale)
        $SQLtit2.=", Titolo_Html_AL";
    $SQLtit2.=" from " . $tabsvil . "T_Articoli ";
    if ($Lang != $LinguaPrincipale)
        $SQLtit2.=", " . $tabsvil . "T_Articoli_AL";
    $SQLtit2.=" where Id_Articolo='" . $daabstract . "'";

    if ($db_titolo2->query($SQLtit2))
        $db_titolo2->next_record();
    if ($Lang != $LinguaPrincipale)
        $titolo_abs = $db_titolo2->f('Titolo_Html_AL');
    else
        $titolo_abs = $db_titolo2->f('Titolo_Html');
}

$myabil_page = 1;
if (($utype == 2 || $utype == 3) && $mastereditor == 0) {
    $db_admintool = new DB_CedCamCMS;
    $SQLadmintool = "select Id_Menu as myabil2 from " . $tabsvil . "T_Menu where instr('$myabil',concat('|',Id,'|'))";
    $db_admintool->query($SQLadmintool);
    $myabil2 = "|";
    $myabil_page = 0;
    while ($db_admintool->next_record()) {
        $myabil2.=$db_admintool->f('myabil2') . "|";
        if ($db_admintool->f('myabil2') == substr($explode, 0, strlen($db_admintool->f('myabil2'))))
            $myabil_page = 1;
    }
}

$al = $AltreLingue;
array_unshift($al, $LinguaPrincipale);
$scelta_lingua = "";
if ($QUERY_STRING != "")
    $scelta_lingua2 = "?" . $QUERY_STRING . "&amp;";
else
    $scelta_lingua2 = "?";
for ($i = 0; $i < sizeof($al); $i++)
    $scelta_lingua.="<a href=\"" . $PHP_SELF . $scelta_lingua2 . "Lang2=" . $al[$i] . "\">" . $al[$i] . "</a>&nbsp;";

//include "browser.inc";

if ($db_titolo->f('Data_Pubblicazione') > $db_titolo->f('Data_modifica'))
    $meta_date = substr($db_titolo->f('Data_Pubblicazione'), 0, 10);
else
    $meta_date = substr($db_titolo->f('Data_modifica'), 0, 10);
if ($Lang != $LinguaPrincipale && $db_titolo->f('Descrizione_AL') != "")
    $description = $db_titolo->f('Descrizione_AL');
else
    $description = $db_titolo->f('Descrizione');
if ($Lang != $LinguaPrincipale && $db_titolo->f('Keywords_AL') != "")
    $keywords = $db_titolo->f('Keywords_AL');
else
    $keywords = $db_titolo->f('Keywords');
if ($Lang != $LinguaPrincipale && $db_titolo->f('Popup_AL') != "")
    $Popup = $db_titolo->f('Popup_AL');
else
    $Popup = $db_titolo->f('Popup');
if ($Lang != $LinguaPrincipale && $db_titolo->f('LinkEsterno_AL') != "")
    $LinkEsterno2 = $db_titolo->f('LinkEsterno_AL');
else
    $LinkEsterno2 = $db_titolo->f('LinkEsterno');

$mtop = $db_titolo->f('MTop');

if ($titolo_abs != "")
    $titolo_pagina = $titolo_abs;
else {
    if ($Lang != $LinguaPrincipale) {
        if ($db_titolo->f('Popup_AL') != "")
            $titolo_pagina = $db_titolo->f('Popup_AL');
        else
            $titolo_pagina = $db_titolo->f('Titolo_AL');
    }
    else {
        if ($db_titolo->f('Popup') != "")
            $titolo_pagina = $db_titolo->f('Popup');
        else
            $titolo_pagina = $db_titolo->f('Titolo');
    }
}

$titoloPage = $titolo_pagina . " - " . $TITLESITE;
?>