<?php
/* OOHForms: textarea
 *
 * Copyright (c) 1998 by Jay Bloodworth
 *
 * $Id: of_textarea.inc,v 1.6 1999/10/14 15:37:41 negro Exp $
 */

class of_textarea extends of_element {

  var $rows;
  var $cols;
  var $wrap;

  // Constructor
  function of_textarea($a) {
    $this->setup_element($a);
  }

  function self_get($val,$which, &$count) {
    $str  = "";
/*
    if(isset($GLOBALS["ly"]) && !empty($GLOBALS["ly"]->font_def['font']))
    {
	eval("\$font=\"".$GLOBALS["ly"]->font_def['font']."\";");
      	$str.="<FONT $font>";
    }
*/
    $n = $this->name . ($this->multiple ? "[]" : "");
    $str .= "<textarea name='$n'";
    $str .= " rows='$this->rows' cols='$this->cols'";
    if ($this->wrap) 
      $str .= " wrap='$this->wrap'";
    if ($this->extrahtml) 
      $str .= " $this->extrahtml";
    $str .= ">";
    $str.= htmlspecialchars($this->value);
    $str.="</textarea>";
/*
    if(isset($GLOBALS["ly"]) && !empty($GLOBALS["ly"]->font_def['font']))
	$str.="</FONT>";
*/    
    $count = 1;
    return $str;
  }

  function self_get_frozen($val,$which, &$count) {
    $str  = "";
    $str .= "<input type='hidden' name='$this->name'";
    $str .= " value='$this->value'>\n";
    $str .= "<table border=1><tr><td>\n";
    $str .=  nl2br($this->value);
    $str .= "\n</td></tr></table>\n";
    
    $count = 1;
    return $str;
  }

} // end TEXTAREA

?>
