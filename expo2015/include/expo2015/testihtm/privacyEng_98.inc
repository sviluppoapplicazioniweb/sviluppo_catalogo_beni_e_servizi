<p><b><span>PRIVACY NOTICE</span></b></p>
<textarea cols="80" name="prvmsg" rows="10" readonly>
Subject and Purposes
This privacy notice is addressed to any Users willing to sign in to www.siexpo2015.it (the &quot;Website&quot;) in order to obtain access to the Catalogue SiExpo 2015 and relevant contents available therein.
The controller of the personal data is the Consortium SiExpo, having seat in Milan (MI), Via Achille Papa no. 30, Zip Code 20149, Fiscal Code 07996310962, VAT 07996310962 (the &quot;Controller&quot;). The Controller must mandatorily provide the Users with some information relating to the handling of personal data filed by the Users during the registration to the Website.
By reading and accepting this Privacy Notice, the Users agree to the filing and processing of their personal data by the Controller, exclusively for the purposes relating to, or connected with, the performance of the requested services. The data shall be treated in compliance with the obligations under the applicable Italian and European laws, as well as with the instructions set by the surveillance and control agencies empowered to do so by the mentioned laws. In particular, the Controller hereby guarantees that the Users&rsquo; data will be treated in compliance with Section 11 of Legislative Decree no. 193/2003 (so called Privacy Code).

Users&rsquo; Rights
The Users will be always entitled to exert their rights towards the Controller, pursuant to Section 7 of Legislative Decree no. 193/2003. This Section is fully quoted here below, for the sake of clarity and completeness to the benefit of the Users:
Section 7 of Legislative Decree no. 193/2003 - Right to Access Personal Data and Other Rights
1. A data subject shall have the right to obtain confirmation as to whether or not personal data concerning him exist, even if not already recorded, as well as to receive communication of such data in intelligible form.
2. A data subject shall have the right to be informed of the:
a) source of the personal data;
b) purposes and methods of the processing;
c) logic applied to the processing, if the latter is carried out with electronic means;
d) identification details concerning the data controller, data processors and the representative appointed as per Section 5(2);
e) entities or categories of entities to whom the personal data may be communicated and who or which may become aware of said data in their capacity as appointed representative(s) in the State&rsquo;s territory, data processor(s) or person(s) in charge of the processing.
3. A data subject shall have the right to obtain:
a) update, amendment or, if of interest, integration of the data;
b) deletion, anonymization or blocking of data that have been processed against the law, including data whose retention is unnecessary for the purposes for which they have been collected or subsequently processed;
c) certification to the effect that the operations under letters a) and b) above have been notified,  also with respect to relevant contents, to the entities to whom the data had been communicated or circulated, unless this fulfilment is prevented or requires a disproportionate effort compared with the right to be protected.
4. A data subject shall have the right to object, in whole or in part: 
a) to the processing of its personal data on the basis of legitimate grounds, even though they are relevant to the purpose of the collection;
b) to the processing of its personal data, to the extent the processing is carried out for the purpose of sending advertising materials, direct selling, the performance of commercial surveys or delivery of marketing communication.

Treatment Duration
The data processing will be carried out, within the limits set forth above, exclusively for the timeframe necessary to guarantee the services requested by the Users by signing in to the Website. In any case, the Users will be always entitled to request their cancellation as Users of the Website, thus resulting in the direct deletion of any and all personal data (provided during the registration process) from the files and records held by the Controller.

Data Processor
The processor of the data is DigiCamere SCaRL, who will be available for contacts and communication to the following: 

Address: via Viserba, 20 &ndash; 20126 Milano
Tel: 02 8515 2115
</textarea>
