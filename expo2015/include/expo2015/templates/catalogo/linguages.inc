<?php

$dbLing = new DataBase();

$arrayLan = array("IT" => 'Italiano', "EN" => "English", "FR" => "Français");

foreach ($dbLing->GetRows("SELECT Label,Lang FROM EXPO_T_Lingue_Sito ORDER BY Id") AS $row) {
    echo '<li class="' . strtolower($row['Lang']) . '">';
    if (strcmp($Lang, $row['Lang']) == 0) {
        echo '<img src="tmpl/expo2015/catalogo/images/sel_' . strtolower($row['Lang']) . '.jpg" class="language-icon" width="23" height="16" alt="'.$arrayLan[$row['Lang']].'" />';
    } else {
        echo '<a href="' . $row['Lang'] . '" class="language-link" title="'.$arrayLan[$row['Lang']].'" alt="'.$arrayLan[$row['Lang']].'">';
        echo '<img src="tmpl/expo2015/catalogo/images/' . strtolower($row['Lang']) . '.jpg" class="language-icon" width="23" height="16" alt="'.$arrayLan[$row['Lang']].'" />';
        echo '</a>';
    }
    echo "</li>";
}

if (strcmp($Lang, "FR") == 0) {
    $widthMenu = "500px";
} elseif (strcmp($Lang, "EN") == 0) {
    $widthMenu = "450px";
}
?>