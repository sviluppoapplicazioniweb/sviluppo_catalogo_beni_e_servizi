<span>© 2013 - Expo 2015 S.p.A. - C.F. e P.I. 06398130960</span>
<div id="block-custom_blocks-0" class="block block-custom_blocks">
    <div class="content">
        <div class="menu"></div>
    </div>
</div>
<div id="block-custom_blocks-1" class="block block-custom_blocks">
    <div class="content">
        <div id="under-footer">
            <div class="menu">
                <p class="footer-partner">I nostri soci</p>
                <ul>
                    <li>
                        <a href="http://www.mi.camcom.it" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/logo1.jpg" width="113" height="52" title="Camera di commercio Milano" alt="camera di commercio milano"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.comune.milano.it" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/logo2.jpg" width="91" height="52" title="Comune di Milano" alt="Comune di Milano"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.provincia.milano.it" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/logo3.jpg" width="77" height="52" title="Provincia di Milano" alt="Provincia di Milano"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.regione.lombardia.it" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/logo4.jpg" width="105" height="52" title="Regione Lombardia" alt="Regione Lombardia"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.mef.gov.it" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/logo5.jpg" width="76" height="52" title="Mef" alt="Mef"/>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="menu_right">
                <p class="footer-partner">Expo 2015 - Official Global Partners</p>
                <div class="riga_grigia">
                    <div class="fpartner-1">
                        <a target="_blank" href="http://www.accenture.com/it-it/Pages/index.aspx"><img alt="Accenture" title="Accenture" width="75" src="/tmpl/expo2015/catalogo/images/acc.jpg"/></a>
                        
                        <a target="_blank" href="http://www.enel.it/it-IT/"><img width="65" title="Enel" alt="Enel" src="/tmpl/expo2015/catalogo/images/enel.jpg"/></a>
                        <a target="_blank" href="http://www.fiatspa.com/"><img width="130" title="FIAT" alt="FIAT" src="/tmpl/expo2015/catalogo/images/fiat.jpg"/></a>
                    </div>
                    <div class="fpartner-2">
                        <a target="_blank" href="http://www.intesasanpaolo.com/"><img width="100" title="Intesa San Paolo" alt="Intesa San Paolo" src="/tmpl/expo2015/catalogo/images/sanpaolo.jpg"/></a>
                        <a target="_blank" href="http://www.samsung.com/"><img title="Samsung" alt="Samsung" src="/tmpl/expo2015/catalogo/images/samsung.jpg"/></a>
                        <a target="_blank" href="http://selex-es.com/"><img width="99" title="Selex ES" alt="Selex ES" src="/tmpl/expo2015/catalogo/images/selex.jpg"/></a>
                        <a target="_blank" href="http://www.telecomitalia.com/"><img width="80" title="Telecom Italia" alt="Ielecom Italia" src="/tmpl/expo2015/catalogo/images/telecom.jpg"/></a>
                    </div>
                </div>
            </div>
            <div class="iniziative-supporto">
                <p class="footer-supporto">Le iniziative a supporto di Expo 2015</p>
                <ul>
                    <li>
                        <a href="http://www.tavoliexpo.it/" onclick="window.open(this.href);
                                return false;" onkeypress="window.open(this.href);
                                return false;">
                            <img src="/tmpl/expo2015/catalogo/images/tavoli.jpg" width="100" height="37" title="Tavoli Tematici" alt="tavoli tematici"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div> 
        <div class="spacer"></div>
    </div>
</div>