<?php

include $PROGETTO . "/libraries/setDataPage.inc";
require_once($PROGETTO . "/view/lib/db.class.php");
?>
<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="it">

    <head>
        <title><?php echo $titoloPage; ?></title>   
        <!-- <meta http-equiv="x-ua-compatible" content="IE=9;"> -->
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="title" content="<?php echo $db_titolo->f('Titolo'); ?>" />
        <meta name="Description" content="<?php echo $description; ?>" />
        <meta name="Keywords" content="<?php echo $keywords; ?>" />
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="date" content="<?php echo "$meta_date"; ?>" />

		<!--[if lt IE 8]> <div style=' clear: both; height: 42px; padding:0 0 0 15px; position: relative;'> 
<a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-9/worldwide-languages" TARGET="_blank"> 
<img src="/images//expo2015/warning_bar_0000_it.jpg" border="0" height="42" width="820" 
 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
        
        <link rel="stylesheet" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/generale.css" />
        <link rel="stylesheet" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/template.css" />
        

        <link rel="stylesheet" href="/jquery/themes/base/jquery-ui.css" /> 
        <link rel="stylesheet" href="/jquery/themes/base/jquery.ui.menu.css" />

        <script type="text/javascript" src="/jquery/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/jquery/ui/jquery-ui.js"></script>
        <script type="text/javascript" src='/jquery/jquery.form.js'></script>

        <script src="/jquery/js/functions.js"></script>



        <?php if ($Id_VMenu == 321) { ?>
            <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/tsc/tabs.css" />
            <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/tsc/accordion_toggle.css" />
           <!-- <script type="text/javascript" src='/jquery/js/tsc/tabs.js'></script>-->
            <script type="text/javascript" src='/jquery/js/tsc/accordion_toggle.js'></script>
        <?php } elseif ($Id_VMenu == 323 || $Id_VMenu == 324 || $Id_VMenu == 509) { ?>
            <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/tsc/tabs.css" />
            <link rel="stylesheet" type="text/css" href="/tmpl/<?php echo $PROGETTO; ?>/<?php echo $templateUse; ?>/css/tsc/accordion_toggle.css" />
            <link rel="stylesheet" href="/jquery/themes/blue/style.css" type="text/css" media="print, projection, screen" />
        <?php } elseif ($Id_VMenu == 82) { ?>
            <script type="text/javascript" src="/jquery/js/visualizza_EXPO_Tlk_Categorie.js"></script>
        <?php } ?>


        <?php if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/jquery/js/visualizza_" . $nome . ".js")) { ?>
            <script type="text/javascript" src="/jquery/js/visualizza_<?php echo $nome ?>.js"></script>
        <?php } ?> 

    </head>

    <body >
        <div style="display: none" id="InfoLang"><?php echo $Lang; ?></div>
        <div style="display: none" id="IdError"><?php echo $uid; ?></div>
        <div id="page">
            <div class="content_menu">
                <div id="main-menu"></div> 
            </div>
            <div id="wrapper">
                <div id="header">
                    <div id="logo">
                        <a title="Expo 2015 - Home" href="javascript:exitConferma('http://www.expo2015.org/')" draggable="false">
                            <img draggable="false" src="/tmpl/<?php echo $PROGETTO; ?>/catalogo/images/logo_<?php echo strtolower($Lang); ?>.jpg" alt="Expo 2015 - Home"/>
                        </a>
                    </div>
                    <?php if ($Id_VMenu == 318) { ?>
                        <div id="block-locale-0" class="clear-block block block-locale">
                            <div class="content">
                                <ul>
                                    <?php include $PROGETTO . "/templates/" . $templateUse . "/linguages.inc"; ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?> 
                    <div id="block-custom_blocks-8" class="block block-custom_blocks">
                        <div class="content">
                            <?php if (!((!isset($auth->auth["utype"]) || $auth->auth["utype"] == 99)) || ($Id_VMenu == 308)) { ?>
                                <div id="header-assistenza">
                                    <a draggable="false" title="Assistenza" href="/index.phtml?Id_VMenu=501" target = "_blank">Assistenza</a>
                                </div>
                            <?php } ?> 
                            <div id="header-faq">

                                <?php
                                if ($utype > 95 || $utype == 94) {

                                    echo '<a draggable="false" title="Domande / Risposte" href="/index.phtml?Id_VMenu=292">Domande / Risposte</a>';
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <div id="block-custom_blocks-10" class="block block-custom_blocks">
                        <div class="content">
                            <div id="login">
                                <?php if (!isset($auth->auth["utype"]) || $auth->auth["utype"] == 99) { ?>
                                    <a draggable="false" href="/index.phtml?Id_VMenu=308"><?php echo _ACCEDI_; ?></a> / 
                                    <a draggable="false" href="/index.phtml?Id_VMenu=308&option=regfirmadig"><?php echo _REGISTRATI_; ?></a>
                                <?php } else { ?>

                                    <strong><?php echo _BENVENUTO_; ?></strong> <h1><?php echo $nominativo; ?></h1 > <a draggable="false" href="/jslogin.phtml?action=logout&okurl=<?php echo $MAINSITE; ?>"><?php echo _DISCONNETTI_; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="breadcrumb">
                    <?php if ($Id_VMenu != 318) { ?>
                        <div id="orientationInfo"><a href="index.phtml">HOME CATALOGO</a></div>
                    <?php } ?>
                    <?php
                    $db = new DataBase();

                    if (isset($_POST['IdImpresa'])) {

                        $IdImpresa = $auth->auth['IdImpresa'] = $_POST['IdImpresa'];
                        $auth->auth['NomeImpresa'] = $db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE  Id = '$IdImpresa'", "RagioneSociale");
                        page_close();
                    } elseif ($IdImpresa == "" && $auth->auth['IdImpresa'] > 0){
						$IdImpresa = $auth->auth['IdImpresa'];
					}
					
                    if (isset($auth->auth['NomeImpresa'])) {
					    $IdImpresa = $auth->auth['IdImpresa'] ;
						
						$sqlInfoImpresa = "	SELECT TJ.Id_OrdineProfessionale AS Ordine, TJ.IsStudio
						FROM EXPO_T_Imprese AS I JOIN EXPO_TJ_Imprese_OrdiniProfessionali AS TJ ON I.Id = TJ.Id_Impresa
						WHERE I.Id=$IdImpresa";


						$infoImpresa = $db->GetRow($sqlInfoImpresa);
						$ordine = $infoImpresa['Ordine'];
						$studio = $infoImpresa['IsStudio'];
							                    
						$ragioneStampa = "";	
						if ($ordine > 0){	$ragioneStampa.= _RAGSOC_PROF_;}else {$ragioneStampa.=_RAGSOC_;}

						$ragioneStampa.= ": <h1>" . $auth->auth['NomeImpresa'] . "</h1>  ";
						echo $ragioneStampa;
                        
                        $query = "SELECT * FROM EXPO_T_Imprese AS T
                            LEFT JOIN EXPO_TJ_Imprese_Utenti AS TJ ON T.Id = TJ.IdImpresa
                            WHERE TJ.IdUtente =  '$uid'";

                        if ($db->NumRows($query) > 1) {
                            ?>
                            <a draggable="false" href="/index.phtml?Id_VMenu=307"><?php echo _CAMBIA_; ?></a>
                            <?php
                        }
                    }
                    ?>
                    <input type="hidden"  id="Ordine" value="<?php echo $ordine;?>" />
					<input type="hidden"  id="IsStudio" value="<?php echo $studio;?>" />
                </div>
                <div id="main">
                    <?php
                    $large = "large";
                    if (isset($auth->auth['IdImpresa']) || isset($_POST['IdImpresa']) || $utype == 95 || $utype == 93) {
                        $large = "small";
                        ?>
                        <div id="col1">
                            <div id="content-main1">
                                <div id="block-custom_blocks-2" class="block block-custom_blocks">
                                    <div class="content">
                                        <?php
                                        if ($template[$tmpl]["left_item"] != "") {
                                            $barra = "Sinistra";
                                            $left_item = explode("|", $template[$tmpl]["left_item"]);
                                            foreach ($left_item as $item)
                                                include $PROGETTO . "/" . $item . ".inc";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div id="col2" class="page_<?php echo $large; ?>" >
                        <div class="block-h">
                        
                        	<?php
                        	if ($Id_VMenu == 318) {
                        	include $PROGETTO . "/boxNews.inc"; }?>
                            <div style="text-align: left" id="topStatoContenuti"></div>
                            <?php
                            if (!(isset($nh)) && $Id_VMenu != 1) {


                                if ($Lang != $LinguaPrincipale && $db_titolo->f('Titolo_AL') != "")
                                    $titolo_barra = $db_titolo->f('Titolo_AL');
                                else
                                    $titolo_barra = $db_titolo->f('Titolo');



                                if ($Id_VMenu == 302) {
                                    if (strcmp($azione, "INS") == 0) {
                                        echo '<h1 id="titoloProd">' . _NEW_PROD_ . " </h1>";
                                    } elseif (strcmp($azione, "UPD") == 0 || strcmp($azione, "SAL") == 0) {
                                        $dbProdotto = new DataBase();
                                        $IdProdottoTitolo = base64_decode($_GET['Id']);
                                        echo '<h1 id="titoloProd">' . $dbProdotto->GetRow("SELECT Nome FROM EXPO_T_Prodotti WHERE Id = '$IdProdottoTitolo'", "Nome") . " </h1>";
                                    } else {
                                        echo "<h1 id='titoloBarra'>" . $titolo_barra . " </h1>";
                                    }
                                } elseif ($Id_VMenu != 318) {
                                    echo "<h1 id='titoloBarra' >" . $titolo_barra . " </h1>";
                                }
                            }
                            ?>

                        </div>

                        <div id="dx-no-social">

                            <?php
                            if ($pagina != "") {
                                include $PROGETTO . "/" . $pagina . ".inc";
                            } else {
                                include $PROGETTO . "/corpo.inc";
                            }
                            ?>

                        </div>

                        <div class="hr-shadow-bis"></div>
                        <div class="spacer"></div>
                        <div id="contentBottom"></div>
                    </div>
                </div>
                <div id="footer">
                    <?php include $PROGETTO . "/templates/" . $templateUse . "/footer.inc"; ?>
                </div>
            </div>
        </div>
    </body>
</html>