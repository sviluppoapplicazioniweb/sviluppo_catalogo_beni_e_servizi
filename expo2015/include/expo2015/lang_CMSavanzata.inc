<?php
$Chiave["SVIL_T_Menu"]="Id";
$Permessi["SVIL_T_Menu"]=";1;2;3;";
$Azioni["SVIL_T_Menu"]["1"]="|Elimina";
$Azioni["SVIL_T_Menu"]["2"]="|Elimina";
$Filtri["SVIL_T_Menu"]["Requisito minimo"]="||||Tlk_Priorita|Id_Priorita|Id|Descrizione|Id";
$FiltriText["SVIL_T_Menu"]["Menu"]="||Id_Menu|20|100|TEXT";
$FiltriText["SVIL_T_Menu"]["Titolo"]="||Titolo|20|100|TEXT";
$Elimina["SVIL_T_Menu"]="SVIL_T_Box;Id_Menu2|SVIL_T_Menu_AL;Id_AL";
$Location["SVIL_T_Menu"]["ELM"]["1"] = "/index.phtml?Id_VMenu=35";
$Location["SVIL_T_Menu"]["ELM"]["2"] = "/index.phtml?Id_VMenu=53";
$Location["SVIL_T_Menu"]["default"]["default"] = "/index.phtml?Id_VMenu=52";
$SVIL_T_Menu["Id|||"] = "Id||I";
$SVIL_T_Menu["Titolo|||"] = "Titolo||S|S||||TEXT|50|100";
$SVIL_T_Menu["Popup|||"] = "Titolo barra||N|N||\$Tipo!='L'||TEXT|30|100";
$SVIL_T_Menu["Id_Menu|;1;||"] = "Menu||S|S||||TEXT|10|50";
$SVIL_T_Menu["Id_Menu|;2;3;||"] = "Menu||S|S||DIS||TEXT|10|50";
$SVIL_T_Menu["Tipo|||"] = "Tipo||S|S||||SELECTREFRESH|Tlk_TipoPagina;Id;Descrizione|1";
$SVIL_T_Menu["Id_Sottomenu|||"]="Ha Sottomen�?||N|N||\$Tipo=='M'||RADIO|Si;No|1;0|0|0";
$SVIL_T_Menu["Esploso|||"]="Esploso?||N|N||\$Tipo=='M'||RADIO|Si;No|S;N|N|N";
if ($AREE_RISERVATE)
{
	$SVIL_T_Menu["Id_Priorita|||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
	$SVIL_T_Menu["TipiUtente|||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
	$SVIL_T_Menu["Gruppi|||"] = "Gruppi||N|N||||CHECKBOX|T_Gruppi|Id_Gruppo|Descrizione|||2|MENU";
}
else
{
	$SVIL_T_Menu["Id_Priorita|;1;||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
	$SVIL_T_Menu["TipiUtente|;1;||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
	$SVIL_T_Menu["Gruppi|;1;||"] = "Gruppi||N|N||||CHECKBOX|T_Gruppi|Id_Gruppo|Descrizione|||2|MENU";
}
$SVIL_T_Menu["Stato_Menu|||"] = "Stato||S|S||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$SVIL_T_Menu["Mtop|;1;2;||"] = "Menu iniziale||S|S||\$Tipo!='L'||TEXT|10|50";
$SVIL_T_Menu["Descrizione|||"] = "Descrizione||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$SVIL_T_Menu["Keywords|||"] = "Keywords||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$SVIL_T_Menu["Id_Pagina|;1;||"] = "Template||S|N||\$Tipo!='L'||SELECTFILTRO|T_Pagine;Id;Descrizione";
$SVIL_T_Menu["Id_Pagina|;2;3;||"] = "Template||S|N||\$Tipo!='L'||SELECTFILTRO|T_Pagine;Id;Descrizione||Flag_Pubblico||1";
$SVIL_T_Menu["Id_Pagina||||"] = "Template||N|N||\$Tipo=='L'||HIDDEN|TEMPLATE_LABEL";
$SVIL_T_Menu["LinkEsterno|||"] = "Link esterno||N|N||\$Tipo!='L'||TEXT|30|100";
$SVIL_T_Menu["SVIL_T_Menu_AL2||UPD|"] = "Altre lingue||N|N||||ELENCO|SVIL_T_Menu_AL|Id_AL|Id|SVIL_T_Menu_AL.Lang_AL|";
$SVIL_T_Menu["Testo_Html||UPD|"] = "Box di contenuti||N|N||\$Tipo!='L'||ELENCO|SVIL_T_Box|Id_Menu2|Id|SVIL_T_Box.Ordine|Id_Box";
$SVIL_T_Menu["IPFilter|||"]="Filtro IP?||N|N||||RADIO|Si;No|1;0|0|0";
$SVIL_T_Menu["Id_Ufficio|||"] = "Ufficio||S|N||||SELECTFILTRO|T_Uffici;Id_Ufficio;Ufficio|Id|";
$SVIL_T_Menu["Data_Modifica|||"] = "||N|N||||DATA|";
$SVIL_T_Menu["Data_Modifica||UPD|"] = "Data Modifica||N|N||DIS||TEXT|10|10";
$SVIL_T_Menu["Id_Modifica||UPD|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|Id_Modifica|";
$SVIL_T_Menu["Id_Modifica||UPD||"] = "Proprietario||N|N||||HIDDEN|uid|";
$SVIL_T_Menu["Id_Modifica||INS|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|uid|";
$SVIL_T_Menu["Flag_Modifica||UPD|"] = "Stato Pagina||S|S||||SELECTFILTRO|Tlk_Flag_Modifica;Id;Descrizione|Id|||||Id DESC|Id>1 and Id>={%utype%}|";
$SVIL_T_Menu["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["SVIL_T_Menu_AL"]="Id_AL";
$Permessi["SVIL_T_Menu_AL"]=";1;2;3:";
$Inserimento["SVIL_T_Menu_AL"]["1"]="N";
$Inserimento["SVIL_T_Menu_AL"]["2"]="N";
$Inserimento["SVIL_T_Menu_AL"]["3"]="N";
$Azioni["SVIL_T_Menu_AL"]["1"]="|Modifica";
$Azioni["SVIL_T_Menu_AL"]["2"]="|Modifica";
$Azioni["SVIL_T_Menu_AL"]["3"]="|Modifica";
$Location["SVIL_T_Menu_AL"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=SVIL_T_Menu&amp;azione=UPD&amp;Id=$Id_AL";
$SVIL_T_Menu_AL["Id_AL|||"] = "Id||H";
$SVIL_T_Menu_AL["Lang_AL|||"] = "Lingua||S|S||||TEXT|2|2";
$SVIL_T_Menu_AL["Titolo_AL|||"] = "Titolo||S|S||||TEXT|20|100";
$SVIL_T_Menu_AL["Descrizione_AL|||"] = "Descrizione||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$SVIL_T_Menu_AL["Keywords_AL|||"] = "Keywords||N|N||\$Tipo!='L'||TEXTAREA|2|40";
$SVIL_T_Menu_AL["Popup_AL|||"] = "Pop-up||N|N||\$Tipo!='L'||HTMLAREA|3|40";
$SVIL_T_Menu_AL["LinkEsterno_AL|||"] = "Link esterno||N|N||\$Tipo!='L'||TEXT|30|100";
$SVIL_T_Menu_AL["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["SVIL_T_Box"]="Id_Box";
$Permessi["SVIL_T_Box"]=";1;2;3;";
$Azioni["SVIL_T_Box"]["1"]="Id_Menu2,Id_Menu2;Id_Box,Id_Box|Modifica|box_del|box_up|box_down";
$Azioni["SVIL_T_Box"]["2"]="Id_Menu2,Id_Menu2;Id_Box,Id_Box|Modifica|box_del|box_up|box_down";
$Azioni["SVIL_T_Box"]["3"]="Id_Menu2,Id_Menu2;Id_Box,Id_Box|Modifica|box_del|box_up|box_down";
$Location["SVIL_T_Box"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=SVIL_T_Menu&amp;azione=UPD&amp;Id=$Id";
$SVIL_T_Box["Id_Box|||"] = "Id_Box||H";
$SVIL_T_Box["Id_Menu2|||"] = "Titolo Menu||N|N||||SELECTHIDDEN|SVIL_T_Menu;Id;Titolo|Id|Id_Menu2|";
$SVIL_T_Box["Id_Box||||"] = "Id Box||N|N||DIS||TEXT|5|5|";
$SVIL_T_Box["Ordine|||"] = "Ordine||S|N||DIS||TEXT|4|4";
$SVIL_T_Box["Stato_Box|||"] = "Stato||S|N||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$SVIL_T_Box["Pagine||UPD|"] = "Articoli||N|N|SVIL_TJ_Articoli_X_Box_X_Menu|||ELENCO|SVIL_TJ_Articoli_X_Box_X_Menu|Id_Box;Id_Menu|Id_Box;Id_Menu2|SVIL_TJ_Articoli_X_Box_X_Menu.Ordine2|Id_Articolo";
$SVIL_T_Box["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["SVIL_TJ_Articoli_X_Box_X_Menu"]="Id_Articolo;Id_Box;Id_Menu";
$Permessi["SVIL_TJ_Articoli_X_Box_X_Menu"]=";1;2;3;";
$Azioni["SVIL_TJ_Articoli_X_Box_X_Menu"]["1"]="|Modifica|Articolo|Elimina";
$Azioni["SVIL_TJ_Articoli_X_Box_X_Menu"]["2"]="|Modifica|Elimina";
$Azioni["SVIL_TJ_Articoli_X_Box_X_Menu"]["3"]="|Modifica|Elimina";
$Location["SVIL_TJ_Articoli_X_Box_X_Menu"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=SVIL_T_Box&amp;azione=UPD&amp;Id_Box=$Id_Box&amp;Id_Menu2=$Id_Menu";
$SVIL_TJ_Articoli_X_Box_X_Menu["Label1|||"]="Gestione Articolo||N|N||||LABEL|CENTER";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Menu|||"] = "Titolo Menu||N|N||||SELECTHIDDEN|SVIL_T_Menu;Id;Titolo|Id|Id_Menu|";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Box|||"] = "Id_Box||N|N||||HIDDEN";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Box||||"] = "Id Box||N|N||DIS||TEXT|5|5|";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Articolo||UPD|"] = "Descrizione||S|N||||SELECTHIDDEN|SVIL_T_Articoli;Id_Articolo;Descrizione|Id_Articolo|Id_Articolo";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_TipoArticolo||INS|"] = "Categoria||N|S||||SELECTREFRESH|SVIL_T_Menu;Id;Id_Menu,Titolo|1||||N|Id_Menu|(Id_Menu like '__' or Id_Menu like '__.__' or Id_Menu like '01.__.__') and Id_Menu<'80' and (Tipo='M' or Id_Menu='00' or Id_Menu like '01%')";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Articolo||INS|"] = "Descrizione||N|S||||SELECTFILTRO|SVIL_T_Articoli;Id_Articolo;Descrizione|1|Id_TipoArticolo1|Id_TipoArticolo";
$SVIL_TJ_Articoli_X_Box_X_Menu["TipoTesto|||"]="Abstract||S|N||||RADIO|Si;No|Abstract;Html|0|Html";
$SVIL_TJ_Articoli_X_Box_X_Menu["Ordine2|||"] = "Ordine||S|S||||TEXT|4|4";
$SVIL_TJ_Articoli_X_Box_X_Menu["Stato_Articolo|||"] = "Stato||S|N||||SELECTFILTRO|Tlk_StatoMenu;Id;Descrizione";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_TipoHtml|;3;||"] = "Tipo||N|S||||SELECTFILTRO|Tlk_TipiHtml;Id;Descrizione|1||||||Attivo=2";
$SVIL_TJ_Articoli_X_Box_X_Menu["Id_TipoHtml|;1;2;||"] = "Tipo||N|S||||SELECTFILTRO|Tlk_TipiHtml;Id;Descrizione|1||||||Attivo>=1";

if ($AREE_RISERVATE)
{
	$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Priorita|||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
	$SVIL_TJ_Articoli_X_Box_X_Menu["TipiUtente|||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
}
else
{
	$SVIL_TJ_Articoli_X_Box_X_Menu["Id_Priorita|;1;||"] = "Requisito minimo||S|S||||SELECTFILTRO|Tlk_Priorita;Id;Descrizione";
	$SVIL_TJ_Articoli_X_Box_X_Menu["TipiUtente|;1;||"] = "Tipo Utente||N|N||||CHECKBOX|Tlk_TipoUtente|Id|Descrizione|||2";
}

$SVIL_TJ_Articoli_X_Box_X_Menu["Pulsante"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["SVIL_T_Articoli"]="Id_Articolo";
$Permessi["SVIL_T_Articoli"]=";1;2;3;";
$Azioni["SVIL_T_Articoli"]["1"]="|Elimina|Pubblica";
$Azioni["SVIL_T_Articoli"]["2"]="|Elimina|Pubblica";
$Azioni["SVIL_T_Articoli"]["3"]="|";
$Ordina["SVIL_T_Articoli"]="Id_Menu_2";
$Filtri["SVIL_T_Articoli"]["Categoria"]="||||SVIL_T_Menu|Id_TipoArticolo1|Id_Menu|Id_Menu,Titolo|Id_Menu|(Id_Menu like '__' or Id_Menu like '__.__' or Id_Menu like '01.__.__');Id_Menu<'80';(Tipo='M' or Id_Menu='00' or Id_Menu like '01%')|Id_Menu|||S";
$Filtri["SVIL_T_Articoli"]["Tipo Articolo"]="|;1;2;|||Tlk_TipoArticolo3|Id_TipoArticolo3|Id|Descrizione|Id";
$Filtri["SVIL_T_Articoli"]["Tipo Articol1"]="|;3;|N|Id_TipoArticolo3;UNO|";
$Filtri["SVIL_T_Articoli"]["Stato Articolo"]="||||Tlk_Flag_Modifica|Flag_Modifica|Id|Descrizione|Id";
$FiltriText["SVIL_T_Articoli"]["Descrizione"]="||Descrizione|20|100|TEXT";
$FiltriText["SVIL_T_Articoli"]["Testo Html"]="|;1;2;|Testo_Html|20|100|TEXT";
$FiltriText["SVIL_T_Articoli"]["Testo Abstract"]="|;1;2;|Testo_Abstract|20|100|TEXT";
$Location["SVIL_T_Articoli"]["default"]["default"] = "/index.phtml?tmpl=2&Id_VMenu=56&pagina=elenchi&nome=SVIL_T_Articoli";
$SVIL_T_Articoli["Id_Articolo|||"] = "Id||I";
$SVIL_T_Articoli["Id_TipoArticolo1|||"] = "Categoria principale||S|S||||SELECTFILTRO|SVIL_T_Menu;Id;Id_Menu,Titolo|1|||||Id_Menu|(Id_Menu like '__' or Id_Menu like '__.__' or Id_Menu like '01.__.__') and Id_Menu<'80' and (Tipo='M' or Id_Menu='00' or Id_Menu like '01%')";
$SVIL_T_Articoli["Id_TipoArticolo3|;1;2;|INS|"] = "Tipo articolo||S|S||||SELECTREFRESH|Tlk_TipoArticolo3;Id;Descrizione|1";
$SVIL_T_Articoli["Id_TipoArticolo3|;1;2;|UPD|"] = "Tipo articolo||N|S||||SELECTFILTRO|Tlk_TipoArticolo3;Id;Descrizione|1";
$SVIL_T_Articoli["Id_TipoArticolo3|;3;|INS||"] = "Tipo articolo||N|N||||HIDDEN|UNO|";
$SVIL_T_Articoli["Descrizione|||"] = "Descrizione||S|S||||TEXT|50|100";
$SVIL_T_Articoli["Titolo_Html|||"] = "Titolo||N|S||||TEXT|50|250";
$SVIL_T_Articoli["Testo_Html|||"] = "Html||N|S||\$Id_TipoArticolo3!=4|SPAN|HTMLAREA|10|60";
$SVIL_T_Articoli["Testo_Html|;1;2;||"] = "Html||N|S||\$Id_TipoArticolo3==4|SPAN|TEXTAREA|10|60";
$SVIL_T_Articoli["Link_Html|||"] = "Link||N|N||||TEXT|40|100";
$SVIL_T_Articoli["Titolo_Abstract|||"] = "Titolo Abstract||N|N||||TEXT|50|250";
$SVIL_T_Articoli["Testo_Abstract|||"] = "Html Abstract||N|N||\$Id_TipoArticolo3!=4|SPAN|HTMLAREA|10|60";
$SVIL_T_Articoli["Testo_Abstract|;1;2;||"] = "Html Abstract||N|N||\$Id_TipoArticolo3==4|SPAN|TEXTAREA|10|60";
$SVIL_T_Articoli["Link_Abstract|||"] = "Link Abstract||N|N||||TEXT|40|100";
$SVIL_T_Articoli["Image_Html|||"]="In homepage?||N|N||\$Id_TipoArticolo3==2||RADIO|Si;No|n;s|0|s";
$SVIL_T_Articoli["Data_Scadenza|||"] = "Data Scadenza (aaaa-mm-gg)||N|N||\$Id_TipoArticolo3<5||TEXT|20|20";
$SVIL_T_Articoli["Data_Modifica|||"] = "||N|N||||DATA|";
$SVIL_T_Articoli["Data_Modifica||UPD|"] = "Data Modifica||N|N||DIS||TEXT|20|20";
$SVIL_T_Articoli["Flag_Modifica|||"] = "Stato Articolo||S|S||||SELECTFILTRO|Tlk_Flag_Modifica;Id;Descrizione|Id|||||Id DESC|Id>1 and Id>={%utype%}|";
$SVIL_T_Articoli["Id_Modifica||UPD|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|Id_Modifica|";
$SVIL_T_Articoli["Id_Modifica||UPD||"] = "Proprietario||N|N||DIS||HIDDEN|uid|";
$SVIL_T_Articoli["Id_Modifica||INS|"] = "Proprietario||N|N||||SELECTHIDDEN|T_Anagrafica;Id;Login|Id|uid|";
$SVIL_T_Articoli["Pagine||UPD|"] = "Presente nelle pagine||N|N||DIS||FUNZIONE|elencopagine|Id_Articolo|FORM|E";
$SVIL_T_Articoli["SVIL_T_Articoli_AL2||UPD|"] = "Altre lingue||N|N||||ELENCO|SVIL_T_Articoli_AL|Id_AL|Id_Articolo|SVIL_T_Articoli_AL.Lang_AL|";
$SVIL_T_Articoli["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

$Chiave["SVIL_T_Articoli_AL"]="Id_AL";
$Permessi["SVIL_T_Articoli_AL"]=";1;2;3;";
$Inserimento["SVIL_T_Articoli_AL"]["1"]="N";
$Inserimento["SVIL_T_Articoli_AL"]["2"]="N";
$Inserimento["SVIL_T_Articoli_AL"]["3"]="N";
$Azioni["SVIL_T_Articoli_AL"]["1"]="|Modifica";
$Azioni["SVIL_T_Articoli_AL"]["2"]="|Modifica";
$Azioni["SVIL_T_Articoli_AL"]["3"]="|Modifica";
$Location["SVIL_T_Articoli_AL"]["default"]["default"] = "/index.phtml?tmpl=2&amp;pagina=form&amp;nome=SVIL_T_Articoli&amp;azione=UPD&amp;Id_Articolo=$Id_AL";
$SVIL_T_Articoli_AL["Id_AL|||"] = "Id||H";
$SVIL_T_Articoli_AL["Lang_AL|||"] = "Lingua||S|S||DIS||TEXT|2|2";
$SVIL_T_Articoli_AL["Titolo_Html_AL|||"] = "Titolo||S|S||||TEXT|40|100";
$SVIL_T_Articoli_AL["Testo_Html_AL|||"] = "Html||N|N||||HTMLAREA|10|60";
$SVIL_T_Articoli_AL["Link_Html_AL|||"] = "Link||N|N||||TEXT|40|100";
$SVIL_T_Articoli_AL["Titolo_Abstract_AL|||"] = "Titolo Abstract||N|N||||TEXT|40|100";
$SVIL_T_Articoli_AL["Testo_Abstract_AL|||"] = "Html Abstract||N|N||||HTMLAREA|10|60";
$SVIL_T_Articoli_AL["Link_Abstract_AL|||"] = "Link Abstract||N|N||||TEXT|40|100";
$SVIL_T_Articoli_AL["Pulsante|||"] = "Esegui||N|N||||BUTTON|CENTER";

?>