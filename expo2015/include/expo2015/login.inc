<?php

if (isset($auth->auth["uid"])) {
    $operazione = "logout";
} else {
    $operazione = "login";
    $okurl = "index.phtml?Id_VMenu=307";
}
if (isset($okurl)) {
    $OKURL = $okurl;
} elseif ($operazione == "logout") {
//    $OKURL = $HOMEURL . "index.phtml?Id_VMenu=51";
    session_destroy();
    $OKURL = $HOMEURL;
} else {
    $OKURL = $_SERVER['REQUEST_URI'];
}
if (is_object($sess)) {
    $sess->unregister("okurl");
    unset($okurl);
    page_close();
}
//if ($operazione == "logout") {
//    $OKURL = $HOMEURL . "index.phtml?Id_VMenu=51";
//}
//$OKURL="http://puma.mi.camcom.it/index.phtml?Id_VMenu=1";
unset($okurl);
$codice_html = file_get_contents($PATHDOCS . "tmpl/" . $operazione . ".html");
echo preg_replace("/\{%(\w+)%\}/e", "\$\\1", $codice_html);
?>