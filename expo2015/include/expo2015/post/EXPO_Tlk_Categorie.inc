<?php
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();

$db_pub = new DB_CedCamCMS;

$Codici = explode(".", $Id_Categoria);
$numcol = count($Codici);
if ($numcol==1){
	
}
$CodiceCat = "";
$DescEstesa = "";
$DescEstesa_AL = "";
$DescEstesa_AL2 = "";
for ($i=0;$i<$numcol;$i++){
	if (strlen($CodiceCat)==0){
		$CodiceCat .= $Codici[$i];
	}else{
		$CodiceCat .= ".".$Codici[$i];
	}
	
	$sql_cat = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id_Categoria='".$CodiceCat."'";      
        
	$db_pub->query($sql_cat);
	if ($db_pub->num_rows()>0){
		$db_pub->next_record();
		if (strlen($DescEstesa)>0){
			$DescEstesa = $DescEstesa."->".str_replace("'","''",$db_pub->f('Descrizione'));
			$DescEstesa_AL = $DescEstesa_AL."->".str_replace("'","''",$db_pub->f('Descrizione_AL'));
                        $DescEstesa_AL2 = $DescEstesa_AL2."->".str_replace("'","''",$db_pub->f('Descrizione_AL2'));
		}
		else{ 
			$DescEstesa = str_replace("'","''",$db_pub->f('Descrizione'));
			$DescEstesa_AL = str_replace("'","''",$db_pub->f('Descrizione_AL'));
                        $DescEstesa_AL2 = str_replace("'","''",$db_pub->f('Descrizione_AL2'));

		}
	}
}

if(strlen($Id_Categoria) <= 8){
       
    //Aggiorna le Descrizioni estese
    $sql_pub = "UPDATE EXPO_Tlk_Categorie SET DescEstesa='".$DescEstesa."', DescEstesa_AL='".$DescEstesa_AL."', DescEstesa_AL2='".$DescEstesa_AL2."' WHERE Id_Categoria ='".$Id_Categoria."';";

    $db_pub->query($sql_pub);

 } else {
     
    $query = "SELECT A.Id,E.IdImpresa
                FROM EXPO_Tlk_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B
                                                     ON A.Id_Categoria = SUBSTRING(B.Id_Categoria,1,8)
                                                    AND LENGTH(B.Id_Categoria) = 11
                                             INNER JOIN EXPO_T_Ateco AS D 
                                                     ON TRIM(B.Descrizione) = TRIM(D.CodiceAteco)
                                             INNER JOIN EXPO_TJ_Imprese_Ateco AS E
                                                     ON E.IdAteco = D.Id
                                                    AND 0 = (SELECT COUNT(*) AS Count2 FROM EXPO_TJ_Imprese_Categorie WHERE IdImpresa = E.IdImpresa AND IdCategoria = A.Id)
                                              LEFT JOIN EXPO_TJ_Imprese_Categorie AS C
                                                     ON A.Id = C.IdCategoria
                                                    AND E.IdImpresa = C.IdImpresa
               WHERE B.Id_Categoria IN ('".$Id_Categoria."')    
                 AND C.IdImpresa IS NULL";

    foreach ($db->GetRows($query) AS $rows) {
        
        $massimo = 0;
                
        $query2 = "SELECT MAX(Id) AS Massimo
                     FROM EXPO_TJ_Imprese_Categorie";
        
        $rows2 = $db->GetRow($query2);
        
        $massimo = $rows2['Massimo'];
        $massimo += 1;
        
        $query3 = "INSERT INTO EXPO_TJ_Imprese_Categorie VALUES (".$massimo.",".$rows['IdImpresa'].",".$rows['Id'].",'N')";

        $db->Query($query3);
        
    }
      
 }
 
 
 //Aggiorna le Descrizioni estese
 include "$PROGETTO/view/lib/UpdateTreeCategories.php";
 
 ?>