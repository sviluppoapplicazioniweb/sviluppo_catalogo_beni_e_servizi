<?php
$db_insadm = new DB_CedCamCMS;

$tmpl_pag_def=3;
$titolo_pag_def="";

if ($utype=="3")
	$newflag=3;
else
	$newflag=2;

$newflag_struct=3;

	switch ($tipoins)
	{
	case "prima":

		$azione="INS";
		$par1exp=explode(".",$par1);
		$par2exp=$par1exp;
		$par2exp[sizeof($par1exp)-1]="79";
		$par2b=implode(".",$par2exp);
		$SQLinsadm="select Id from ".$tabsvil."T_Menu where Id_Menu='".$par2b."'";
		$db_insadm->query($SQLinsadm);
		if($db_insadm->num_rows()>0)
			$messaggio="Non � possibile inserire altre voci di menu a questo livello/sottolivello.";
		else
		{
			$par1=implode(".",$par1exp);
			array_pop($par1exp);
			$par2=implode(".",$par1exp);

			//Aggiorno Id_Menu delle voci e sottovoci successive al punto di inserimento e con lo stesso nodo padre
			$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par1."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par1."' ) -1 ) , 2)+1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par1."' )+1)))";
			$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
			$SQLinsadm.=" WHERE Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
			$db_insadm->query($SQLinsadm);

			//Aggiorno MTop, se diverso da '00', delle voci e sottovoci successive al punto di inserimento e con lo stesso nodo padre
			$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
			$SQLinsadm.=" WHERE MTop<>'00' AND Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
			$db_insadm->query($SQLinsadm);

			//Inserisco nuova voce
			$SQLinsadm="INSERT into ".$tabsvil."T_Menu SET Id_Menu='".$par1."', Id_Pagina=".$tmpl_pag_def.",Titolo='".$titolo_pag_def."', Stato_Menu='A',Data_modifica=now(), Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"];
			$db_insadm->query($SQLinsadm);
			include $PROGETTO."/post/".$tabsvil."T_Menu.inc";

			echo "<script> document.location='/popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$db_al->f('massimo')."'; </script>";
		}
		break;

	case "dopo":

		$azione="INS";
		$par1exp=explode(".",$par1);
		$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]+1);
		if($par1exp[sizeof($par1exp)-1]=="80")
			$messaggio="Non � possibile inserire altre voci di menu a questo livello/sottolivello.";
		else
		{
			$par1=implode(".",$par1exp);
			array_pop($par1exp);
			$par2=implode(".",$par1exp);

			//Aggiorno Id_Menu delle voci e sottovoci successive al punto di inserimento e con lo stesso nodo padre
			$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par1."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par1."' ) -1 ) , 2)+1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par1."' )+1)))";
			$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
			$SQLinsadm.=" WHERE Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
			$db_insadm->query($SQLinsadm);

			//Aggiorno MTop, se diverso da '00', delle voci e sottovoci successive al punto di inserimento e con lo stesso nodo padre
			$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
			$SQLinsadm.=" WHERE MTop<>'00' AND Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
			$db_insadm->query($SQLinsadm);

			//Inserisco nuova voce
			$SQLinsadm="INSERT into ".$tabsvil."T_Menu SET Id_Menu='".$par1."', Id_Pagina=".$tmpl_pag_def.",Titolo='".$titolo_pag_def."', Stato_Menu='A',Data_modifica=now(), Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"];
			$db_insadm->query($SQLinsadm);
			include $PROGETTO."/post/".$tabsvil."T_Menu.inc";

			echo "<script> document.location='/popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$db_al->f('massimo')."'; </script>";
		}
		break;

	case "sotto":
		$azione="INS";
		
		//Cerco ultima sottovoce presente
		$SQLinsadm="select MAX(Id_Menu) AS massimo from ".$tabsvil."T_Menu where Id_Menu like '".$par1.".__'";
		$db_insadm->query($SQLinsadm);
		$db_insadm->next_record();
		if ($db_insadm->f('massimo')!="" )
			$par1=$db_insadm->f('massimo');
		else
			$par1.=".00";

		$par1exp=explode(".",$par1);
		$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]+1);
		if($par1exp[sizeof($par1exp)-1]=="80")
			$messaggio="Non � possibile inserire altre voci di menu a questo livello/sottolivello.";
		else
		{
			$par1=implode(".",$par1exp);

			//Inserisco sottovoce com ultima
			$SQLinsadm="INSERT into ".$tabsvil."T_Menu SET Id_Menu='".$par1."', Id_Pagina=".$tmpl_pag_def.",Titolo='".$titolo_pag_def."', Stato_Menu='A',Data_modifica=now(), Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"];
			$db_insadm->query($SQLinsadm);
			include $PROGETTO."/post/".$tabsvil."T_Menu.inc";

			echo "<script> document.location='/popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$db_al->f('massimo')."'; </script>";
		}

		break;

	case "declassa":
		$azione="DEC";
		$par1exp=explode(".",$par1);
		$par2exp=explode(".",$par1);
		$par2exp[sizeof($par2exp)-1]= sprintf("%02s",$par2exp[sizeof($par2exp)-1]-1);
		$par2=implode(".",$par2exp);

		// Cerco ultima sottovoce della voce superiore.
		$SQLinsadm="select MAX(Id_Menu) AS massimo from ".$tabsvil."T_Menu where Id_Menu like '".$par2.".__'";

		$db_insadm->query($SQLinsadm);
		$db_insadm->next_record();
		if ($db_insadm->f('massimo')!="" )
			$par2=$db_insadm->f('massimo');
		else
			$par2.=".00";
		$par2exp=explode(".",$par2);
		$par2exp[sizeof($par2exp)-1]= sprintf("%02s",$par2exp[sizeof($par2exp)-1]+1);
		if($par1exp[sizeof($par1exp)-1]=="80")
			$messaggio="Non � possibile inserire altre voci di menu a questo livello/sottolivello.";
		else
		{
			$par2=implode(".",$par2exp);

			// Aggiorno voce declassata
			$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par2."')";
			$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
			$SQLinsadm.=" where Id_Menu like '".$par1."%'";
			$db_insadm->query($SQLinsadm);
			array_pop($par1exp);
			$par3=implode(".",$par1exp);

			// Aggiorno eventuali voci successive alla vecchia posizione come se l'avessi cancellata
			$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par1."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par1."' ) -1 ) , 2)-1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par1."' )+1)))";
			$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
			$SQLinsadm.=" WHERE Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par3."%'";
			$db_insadm->query($SQLinsadm);
		}

		break;

	case "promuovi":
		$azione="PRO";
		$par1exp=explode(".",$par1);
		array_pop($par1exp);
		$par2=implode(".",$par1exp);
		$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]+1);
		$par3=implode(".",$par1exp);
		array_pop($par1exp);
		$par4=implode(".",$par1exp);

		// Aggiorno voci successive al livello superiore per far posto alla voce spostata
		$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par3."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par3."' ) -1 ) , 2)+1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par3."' )+1)))";
		$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
		$SQLinsadm.=" WHERE Id_Menu >= '".$par3."' AND Id_Menu < '80' AND Id_Menu like '".$par4."%'";
		$db_insadm->query($SQLinsadm);

		//Aggiorno MTop, se diverso da '00', delle voci e sottovoci successive al punto di inserimento e con lo stesso nodo padre
		$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
		$SQLinsadm.=" WHERE MTop<>'00' AND Id_Menu >= '".$par3."' AND Id_Menu < '80' AND Id_Menu like '".$par4."%'";
		$db_insadm->query($SQLinsadm);

		// Aggiorno voce promossa
		$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par3."')";
		$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
		$SQLinsadm.=" where Id_Menu like '".$par1."%'";
		$db_insadm->query($SQLinsadm);

		$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par1."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par1."' ) -1 ) , 2)-1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par1."' )+1)))";
		$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
		$SQLinsadm.=" WHERE Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
		$db_insadm->query($SQLinsadm);

		$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
		$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
		$SQLinsadm.=" WHERE MTop<>'00' AND Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
		$db_insadm->query($SQLinsadm);

		break;

	case "alto":
		$azione="MOV";
		$par1exp=explode(".",$par1);
		$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]-1);

		if($par1exp[sizeof($par1exp)-1]=="00")
			$messaggio="Non � possibile spostare in alto questa voce.";
		else
		{
			$par2=implode(".",$par1exp);
			$SQLinsadm="select Id,Id_Menu from ".$tabsvil."T_Menu WHERE Id_Menu='".$par2."'";
			$db_insadm->query($SQLinsadm);
			if ($db_insadm->num_rows()>0)
			{
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par1."b')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par1."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par1."')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
				$SQLinsadm.=" where MTop<>'00' AND Id_Menu like '".$par1."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".((strlen($par1))+1).",'".$par2."')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par1."b%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
				$SQLinsadm.=" where MTop<>'00' AND Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);
			}
			else
				$messaggio="Non � possibile spostare in basso questa voce.";
		}
		break;

	case "basso":
		$azione="MOV";
		$par1exp=explode(".",$par1);
		$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]+1);

		if($par1exp[sizeof($par1exp)-1]=="80")
			$messaggio="Non � possibile spostare in basso questa voce.";
		else
		{
			$par2=implode(".",$par1exp);
			$SQLinsadm="select Id,Id_Menu from ".$tabsvil."T_Menu WHERE Id_Menu='".$par2."'";
			$db_insadm->query($SQLinsadm);
			if ($db_insadm->num_rows()>0)
			{
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par1."b')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par1."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".strlen($par1).",'".$par1."')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
				$SQLinsadm.=" where MTop<>'00' AND Id_Menu like '".$par1."%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu set Id_Menu=INSERT(Id_Menu,1,".((strlen($par1))+1).",'".$par2."')";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" where Id_Menu like '".$par1."b%'";
				$db_insadm->query($SQLinsadm);
				$SQLinsadm="update ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
				$SQLinsadm.=" where MTop<>'00' AND Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);
			}
			else
			$messaggio="Non � possibile spostare in basso questa voce.";
		}
		break;

	case "elimina":

		if ($par1>"01" && $par1<"80")
		{
			$azione="ELM";
			$par1exp=explode(".",$par1);
			$par1exp[sizeof($par1exp)-1]= sprintf("%02s",$par1exp[sizeof($par1exp)-1]+1);
			if($par1exp[sizeof($par1exp)-1]<"80")
			{
				$par1=implode(".",$par1exp);
				array_pop($par1exp);
				$par2=implode(".",$par1exp);

				$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET Id_Menu=concat(SUBSTRING( Id_Menu, 1,(LENGTH( '".$par1."' ) -2 )),LPAD(  (SUBSTRING( Id_Menu, (LENGTH( '".$par1."' ) -1 ) , 2)-1), 2,  '0'  ),SUBSTRING( Id_Menu,(LENGTH( '".$par1."' )+1)))";
				$SQLinsadm.=", Flag_Struttura=".$newflag_struct;
				$SQLinsadm.=" WHERE Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);

				$SQLinsadm="UPDATE ".$tabsvil."T_Menu SET MTop= SUBSTRING(Id_Menu,1,LENGTH(MTop))";
				$SQLinsadm.=" WHERE MTop<>'00' AND Id_Menu >= '".$par1."' AND Id_Menu < '80' AND Id_Menu like '".$par2."%'";
				$db_insadm->query($SQLinsadm);
			}
			$idelim="|".$Id."|";

                        $SQLinsadm="UPDATE ".$tabsvil."T_Articoli SET Id_TipoArticolo1=1";
			$SQLinsadm.=", Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"];
			$SQLinsadm.=" WHERE Id_TipoArticolo1 =  '".$idelim."'";

			$db_insadm->query($SQLinsadm);
		}
		break;

	case "box_ins":
		//Cerco ultima sottovoce presente
		$SQLinsadm="select MAX(Ordine) AS massimo from ".$tabsvil."T_Box where Id_Menu2='".$Id_Menu2."'";
		$db_insadm->query($SQLinsadm);
		$db_insadm->next_record();
		if ($db_insadm->f('massimo')!="" )
			$par1=$db_insadm->f('massimo')+1;
		else
			$par1=1;

		//Inserisco box come ultima
		$SQLinsadm="INSERT into ".$tabsvil."T_Box SET Id_Menu2='".$Id_Menu2."',Ordine=".$par1.", Stato_Box='A'";
		$db_insadm->query($SQLinsadm);

		$SQLinsadm="update ".$tabsvil."T_Menu SET Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"]." where Id=".$Id_Menu2;
		$db_insadm->query($SQLinsadm);

		if ($popup_azioni=="N")
			header("Location: /popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);
		else
			header("Location: /index.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);

		break;

	case "box_del":

		$SQLinsadm="select Ordine from ".$tabsvil."T_Box where Id_Box=".$Id_Box;
		$db_insadm->query($SQLinsadm);
		$db_insadm->next_record();
		$ordine1=$db_insadm->f('Ordine');
		//Elimino box 
		$SQLinsadm="DELETE from ".$tabsvil."T_Box where Id_Box=".$Id_Box;
		$db_insadm->query($SQLinsadm);
		//Elimino TJ_Articoli_X_Box_X_Menu 
		$SQLinsadm="DELETE from ".$tabsvil."TJ_Articoli_X_Box_X_Menu where Id_Box=".$Id_Box;
		$db_insadm->query($SQLinsadm);

		$SQLinsadm="update ".$tabsvil."T_Box set Ordine=(Ordine-1) where Id_Menu2='".$Id_Menu2."' and Ordine > ".$ordine1;
		$db_insadm->query($SQLinsadm);
		$SQLinsadm="update ".$tabsvil."T_Menu SET Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"]." where Id=".$Id_Menu2;
		$db_insadm->query($SQLinsadm);

		if ($popup_azioni=="N")
			header("Location: /popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);
		else
			header("Location: /index.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);

		break;

	case "box_up":
	case "box_down":
		
		if($nome==$tabsvil."T_Menu" || $nome=="")
		{
			$SQLinsadm="select Ordine,Stato_Box from ".$tabsvil."T_Box where Id_Box=".$Id_Box;
			$db_insadm->query($SQLinsadm);
			$db_insadm->next_record();
			$ordine1=$db_insadm->f('Ordine');
			$stato1=$db_insadm->f('Stato_Box');

			if ($tipoins=="box_up")
			{
				$segno1="+";
				$segno2="-";
				$ordine2=$ordine1-1;
			}
			else
			{
				$segno1="-";
				$segno2="+";
				$ordine2=$ordine1+1;
			}
			$SQLinsadm="update ".$tabsvil."T_Box set Stato_Box='Z' where Id_Box='".$Id_Box."'";
			$db_insadm->query($SQLinsadm);

			$SQLinsadm="update ".$tabsvil."T_Box set Ordine=(Ordine".$segno1."1) where Id_Menu2='".$Id_Menu2."' and Ordine =".$ordine2;
			$db_insadm->query($SQLinsadm);

			$SQLinsadm="update ".$tabsvil."T_Box set Ordine=(Ordine".$segno2."1),Stato_Box='".$stato1."' where Id_Box='".$Id_Box."'";
			$db_insadm->query($SQLinsadm);

			$SQLinsadm="update ".$tabsvil."T_Menu SET Flag_Modifica=".$newflag.", Id_Modifica=".$auth->auth["id_utente"]." where Id=".$Id_Menu2;
			$db_insadm->query($SQLinsadm);

			if ($boxadm=="1")
			{
                            header("Location: /index.phtml?Id_VMenu=".$Id_Menu2);
                            exit;
			}
			if ($popup_azioni=="N")
				header("Location: /popup_admin.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);
			else
				header("Location: /index.phtml?tmpl=2&pagina=form&nome=".$tabsvil."T_Menu&azione=UPD&Id=".$Id_Menu2);
		}

		break;

	}

if ($tipoins!="elimina" && (substr($tipoins,0,3)!="box"))
{
	echo "<SCRIPT>\n";
	if ($messaggio!="")
		echo "alert('".$messaggio."');\n";
	else
		echo "parent.opener.location.reload();\n";
	echo "self.close();\n";
	echo "</SCRIPT>\n";
}

?>
