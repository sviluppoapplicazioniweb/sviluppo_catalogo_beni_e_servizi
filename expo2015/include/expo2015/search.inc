<?php
   # $Id: search.php,v 1.2 2001/10/12 17:51:28 junkmale Exp $
   #
   # This is a php wrapper for use with htdig (http://www.htdig.org/)
   #
   # Copyright (c) 2001 DVL Software Limited
   # http://www.dvl-software.com/
   #

   # This is basically a BSD License.  I got this from 
   # http://www.FreeBSD.org/copyright/freebsd-license.html
   #
   # Redistribution and use in source and binary forms, with or without 
   # modification, are permitted provided that the following conditions are met: 
   #
   #   1.Redistributions of source code must retain the above copyright notice, 
   #     this list of conditions and the following disclaimer. 
   #   2.Redistributions in binary form must reproduce the above copyright notice, 
   #     this list of conditions and the following disclaimer in the documentation 
   #     and/or other materials provided with the distribution. 

   # Items which are often configurable
   #
   $Debug = 0;  # set to non-zero to display debugging messages

   # change this to the location of the shell script call to htsearch
   #
   $HTSEARCH_PROG = "/usr/lib/cgi-bin/htsearch";
	setlocale(LC_ALL, 'it_IT');

   #
   # end of items which are often configurable

   #
   # these are the variables from the form. populated only
   # if the user clicked on Search
   #
   $Parameters = $HTTP_POST_VARS;

   #
   # these are the variables within the URL.  These are populated
   # on pages after the first page.  we perform this step to ensure
   # the form is populated with the appropriate values
   #
   $QueryString = $HTTP_SERVER_VARS["QUERY_STRING"];
   if ($QueryString) {
      $ArrayParm  = ConvertQueryStringToArray($QueryString, ";");

      #
      # these are the fields which the user fills in
      #
      $method   = $ArrayParm["method"];
      $format   = $ArrayParm["format"];
      $sort     = $ArrayParm["sort"];
      $words    = urldecode($ArrayParm["words"]);
      $pagina     = $ArrayParm["pagina"];

      # these fields are hidden and therefore don't need to be populated
      # but are provided for completeness

      $config   = $ArrayParm["config"];
      #$restrict = $ArrayParm["restrict"];
      #$exclude  = $ArrayParm["exclude"];
      #$submit   = $ArrayParm["submit"];
      #$page     = $ArrayParm["page"];
   }

   #
   # don't let the bastards include HTML/PHP in their search strings
   # as a test case, include something like this "<img src=http://199.125.85.46/time.jpg>"
   # in the search field.  If you see the image in the results, the test has failed
   # The function strip_tags will strip all HTML and PHP tags.  With the above test case
   # you wind up with an empty string.  No big deal.  But a better solution might
   # return the original value to the search screen.  The current solution does not.
   #
   # Dan Langille 2001.10.13
   $words = strip_tags($words);

?>
<?php
/*
if (!$Parameters && !strlen($HTTP_SERVER_VARS["QUERY_STRING"])) {
?>
<pre>
#
# htdig php wrapper 1.0
#
# Copyright (c) 2001 DVL Software Limited
# http://www.dvl-software.com/
#
# this source code can be obtained from:
#      http://freebsddiary.org/samples/htdig-php-wrapper.tar.gz
#
# If you have any trouble with htdig, try the htdig mailing lists at
# http://www.htdig.org/
#
# If you have any suggestions for improvments, bug reports, etc,
# please send patches to me.  Thanks.
#
# I'd also like to hear about where it's being used.  That's just me
# being curious, that's all.
#
# dan@langille.org
#
</pre>
<?php
}
*/
//   require("search-form.php");

function ConvertQueryStringToArray($QueryString, $Delimiter) {
   # this function takes a string which contains parameters.  The parameters are delimited
   # by $Delimiter.  It splits these parameters up into an array.  It then takes the key/value
   # pairs of this array and puts it into an associative array.
   #
   # for example, if the input is: $QueryString = "size=10;colour=L;fabric=cotton"
   #                               $Delimiter   = ";"
   #
   # then the output will be this array:
   #
   #   array (
   #       "size"   => "10",
   #       "colour" => "L",
   #       "fabric" => "cotton"
   #   );
   #
   # It is assumed that the string is of the format: "key1=value1<*>key2=value2<*>key3=value3..."
   # where <*> is $Delimiter.
   #
   # This function returns an empty variable if no parameters are found.

#   echo "ConvertParametersToArray: QueryString = '$QueryString' with length " . strlen($QueryString) . "<br>\n";

   #
   # if there's nothing to do, do nothing.
   #
   if (strlen($QueryString)) {

      #
      # split the query string into an array.
      #
      $SimpleArray = explode($Delimiter, $QueryString);

      #
      # taken each element of the array, which will have
      # elements 0..n where each element is of the form
      # "keyn"="valuen" and split them into "keyn" and "valuen"
      #
      while (list($key, $value) = each($SimpleArray)) {
         list($KeyN, $ValueN) = split("=", $value);
//         echo "key=$KeyN"."---".$ValueN;
         #
         # put that key/value pair into the result we are going to pass back
         #
         $Result[$KeyN] = $ValueN;
      }
   } else {
      echo "nothing found<br>\n";
   }

   return $Result;
}

function CompileQuery($pippo) {

   $query = '';

   while (list($name, $value) = each($pippo)) {
      $query = $query . "$name=$value;";
   }
//echo $query;
   # remove the trailing ;
   $query = substr($query, 0, strlen($query) - 1);
   return $query;
}

function ultimamodifica($idpagina,$idabstract=0) {

	$db = new DB_CedCamCMS;
	if ($idabstract!=0)
	{
		$sql="select Data_Modifica from T_Articoli where Id_Articolo=".$idabstract;
		$db->query($sql);
		$db->next_record();
		$dtmod = $db->f("Data_Modifica");
	}
	else
	{
		$sql="select T_Menu.Data_Modifica as DataMenu,T_Articoli.Data_Modifica as DataArticolo from TJ_Articoli_X_Box_X_Menu,T_Articoli,T_Menu where";
		$sql.=" TJ_Articoli_X_Box_X_Menu.Id_Menu=".$idpagina;
		$sql.=" and T_Articoli.Id_Articolo=TJ_Articoli_X_Box_X_Menu.Id_Articolo";
		$sql.=" and T_Menu.Id=".$idpagina;
		$sql.=" order by DataArticolo desc";
		$db->query($sql);
		$db->next_record();
		if ($db->f("DataMenu")>$db->f("DataArticolo"))
			$dtmod = $db->f("DataMenu");
		else
			$dtmod = $db->f("DataArticolo");
	}
	return substr($dtmod,8,2).substr($dtmod,4,4).substr($dtmod,0,4);

}
#
# if the user clicked on Search or we have a query string
#
if ($Parameters || strlen($HTTP_SERVER_VARS["QUERY_STRING"])) {

   if (count($Parameters)) {
      $query = CompileQuery($Parameters);
   } else {
      $query = $HTTP_SERVER_VARS["QUERY_STRING"];
   }
   #
   # this code courtesy of an article by Colin Viebrock [colin@easyDNS.com]
   # at http://www.devshed.com/Server_Side/PHP/search/
   # which formed the basis of this work
   #
   #
   # execute the htsearch code
   #
   $command="$HTSEARCH_PROG \"$query\"";
   exec($command,$result);

   # debug: look at the output.  useful for seeing what is where
   if ($Debug) {
      while (list($k,$v) = each($result)) {
         echo "$k -> $v \n<br \>";
      }
   }

   # how many rows do we have?
   $rc = count($result);
	$db_htdig = new DB_CedCamCMS;

?>
<form class="nomargin" method="post" action="/index.phtml" id="trova">
<input class="cerca" type="text" name="words" size="10" <?php if ($words!="") echo " value=\"".$words."\"";?> />&nbsp;<input type="submit" value="Cerca" />&nbsp;&nbsp;&nbsp;
<input type="hidden" name="pagina" value="search" />
<br />Tutte le parole<input type="radio" name="method" value="and"<?php if ($method=="and") echo " checked=\"checked\"";?> />
Qualsiasi parola<input type="radio" name="method" value="or"<?php if ($method=="or") echo " checked=\"checked\"";?> />
<input type="hidden" name="format" value="long" />
<input type="hidden" name="sort" value="score" />
<br />Ordina risultati per:<input type="radio" name="sort" value="score"<?php if ($sort=="score") echo " checked=\"checked\"";?> /> Rilevanza 
<input type="radio" name="sort" value="time"<?php if ($sort=="time") echo " checked=\"checked\"";?> /> Data 
<input type="radio" name="sort" value="title"<?php if ($sort=="title") echo " checked=\"checked\"";?> /> Titolo 
<input type="hidden" name="config" value="pavia" />
<input type="hidden" name="exclude" value="" />
<input type="hidden" name="submit"   value="" />
<input type="hidden" name="matchesperpage" value="10" />
</form>

<?php
$formato_output="<dl><dt><strong><a href='{%URL%}'>{%TITLE%}</a></strong>&nbsp;{%STARSLEFT%}</dt>\n";
$formato_output.="<dd>{%EXCERPT%}<br /><em><a href='{%URL%}'>{%URL%}</a></em> {%MODIFIED%} &nbsp; {%SIZE%} bytes</dd></dl>\n";


   # all these magic numbers have got to go
   if ($rc < 3) {
      echo "There was an error executing this query.  Please try later.\n";
   } else {
      if ($result[2]=="NOMATCH") {
         echo "There were no matches for <b>$words</b> found on the website.<p>\n";
      } else {
         if ($result[2]=="SYNTAXERROR") {
            echo "There is a syntax error in your search for <b>$search</b>:<br \>";
            echo "<pre>" . $result[3] . "</pre>\n";
         } else {
            #
            # display the headers, this includes the forum, the search
            # parameters, etc.
            #

            $ResultSetStart = 1;
            for ($i = $ResultSetStart; $i < 9; $i++) {
					echo $result[$i];
            }
            while ($i<$rc)
            {
            //for ($i = $ResultSetStart; $i < $rc; $i++) {
            	if ($result[$i]=="<!-- INIZIO RISULTATO -->")
            	{
						$URL=$result[$i+1];
						$TITLE=$result[$i+2];
						$STARSLEFT=$result[$i+3];
						$EXCERPT=str_replace("* * ", "", $result[$i+4]);
						
						$EXCERPT=str_replace("&Atilde;&nbsp;", "à", $EXCERPT);
						$EXCERPT=str_replace("&Atilde;&uml;", "è", $EXCERPT);
						$EXCERPT=str_replace("&Atilde;&not;", "ì", $EXCERPT);
						$EXCERPT=str_replace("&Atilde;&sup1;", "ù", $EXCERPT);
						$EXCERPT=str_replace("&Atilde;&sup2;", "ò", $EXCERPT);
						
						//$EXCERPT=str_replace("&amp;#8217;", "'", $EXCERPT);
						$EXCERPT=str_replace("&Acirc;&deg;", "°", $EXCERPT);
						//$EXCERPT=str_replace("&amp;#8220;", "&quot;", $EXCERPT);
						//$EXCERPT=str_replace("&amp;#8221;", "&quot;", $EXCERPT);
						$EXCERPT=str_replace("&amp;#", "&#", $EXCERPT);

 						$MODIFIED=$result[$i+5];
						$SIZE=$result[$i+6];
					
						$i=$i+7;
						//if (substr($TITLE,0,15)==" - CCIAA Varese")
						//	$TITLE= substr($TITLE,15);

						if (substr($TITLE,((strlen($TITLESITE)+3)*(-1)))==" - ".$TITLESITE)
							$TITLE= substr($TITLE,0,((strlen($TITLESITE)+3)*(-1)));


						$pos = strpos($URL, "Id_VMenu");
						if($pos===false)
						{
							$MODIFIED=$MODIFIED;
						}
						else
						{
							$query=parse_url($URL);
							parse_str($query[query]);
							if(isset($daabstract))
								$MODIFIED=ultimamodifica(0,$daabstract);
							else
								$MODIFIED=ultimamodifica($Id_VMenu);
						}

						echo preg_replace("/\{%(\w+)%\}/e", "\$\\1",$formato_output);
	             }
	             else
	             {
	             	echo $result[$i];
	             	echo $result[$i+1];
	             	echo $result[$i+2];
	             	echo $result[$i+3];
	             	echo $result[$i+4];
	             	$i=$i+5;
	             }
            }
         }
      }
   }
}
/*
if ($HTTP_SERVER_VARS["QUERY_STRING"]!="")
	$urlsearch=$HTTP_SERVER_VARS["QUERY_STRING"];
else
	$urlsearch='exclude=;config=crotone;method=and;format=long;sort=score;matchesperpage=10;words='.$words;

readfile ("http://testconsumatori.cedcamera.com/search.php3?".$urlsearch);
*/
?>
