<?php
$Priorita["1"]=10; // Admin
$Priorita["2"]=9; // Editor
$Priorita["3"]=8; // Redattori
$Priorita["93"]=5; // CCIAA
$Priorita["94"]=2; // Utente Delegato only Read
$Priorita["95"]=4; // EXPO
$Priorita["96"]=3; // Utente Impresa
$Priorita["97"]=2; // Utente Delegato
$Priorita["98"]=1; // Partecipante
$Priorita["99"]=0; // Guest

$Parametri["Label"]=0;
$Parametri["Label2"]=1;
$Parametri["VisTab"]=2;
$Parametri["Obbligatorio"]=3;
$Parametri["AltraTab"]=4;
$Parametri["Display"]=5;
$Parametri["Span"]=6;
$Parametri["TipoCampo"]=7;
$Parametri["Parametro1"]=8;
$Parametri["Parametro2"]=9;
$Parametri["Parametro3"]=10;
$Parametri["Parametro4"]=11;
$Parametri["Parametro5"]=12;
//Usato nella selectJoin come identificativo della tabella a cui appartiene il campo descritto in parametro5
$Parametri["Parametro6"]=13;
$Parametri["Parametro7"]=14;
$Parametri["Parametro8"]=15;
$Parametri["Parametro9"]=16;


$Parfiltri["TipoUtente"]=0;
$Parfiltri["UtentiAbilitati"]=1;
$Parfiltri["Visualizza"]=2;
// Il campo filtro viene utilizzato come default quando 'Visualizza' non � 'N'
$Parfiltri["CampoFiltro"]=3;
$Parfiltri["Tabella"]=4;
$Parfiltri["CampoWhere"]=5;
$Parfiltri["CampoChiave"]=6;
$Parfiltri["Descrizione"]=7;
$Parfiltri["Ordine"]=8;
$Parfiltri["Filtro"]=9;
$Parfiltri["Value"]=10;
$Parfiltri["FiltroSelect"]=11;
$Parfiltri["AltriCampi"]=12; //Altri campi/variabili da estrarre da DB e inizializzare
$Parfiltri["Nullo"]=13;
$Parfiltri["VariabiliDaAzzerare"]=14; //Variabili su cui deve essere eseguito unset alla variazione del campo
$Parfiltri["NoConsideraNumRighe"]=15;
$Parfiltri["TipoCampo"]=16;//Nel caso di una data viene formattata in italiano
$Parfiltri["FiltroWhere"]=17;


$ParfiltriText["TipoUtente"]=0;
$ParfiltriText["UtentiAbilitati"]=1;
$ParfiltriText["Nome"]=2;
$ParfiltriText["Size"]=3;
$ParfiltriText["MaxLengthMultiple"]=4;
$ParfiltriText["TipoCampo"]=5;
$ParfiltriText["Parametro1"]=6;
$ParfiltriText["Parametro5"]=7;
$ParfiltriText["Parametro6"]=8;
$ParfiltriText["Parametro7"]=9;
$ParfiltriText["Parametro8"]=10;


// In caso di CMS multilingua
if ($Lang != $LinguaPrincipale){
	include $PROGETTO."/lang_".$Lang.".inc";
	include $PROGETTO."/lang_appl_".$Lang.".inc";
} else { 
	include $PROGETTO."/lang_".$LinguaPrincipale.".inc";
	include $PROGETTO."/lang_appl.inc";
}

// In caso di CMS multilingua
include $PROGETTO."/view/languages/".$Lang.".inc";
?>