<?php
require_once ("clsPasswordPolicy.php");

class clsCheckPasswordPolicy {

	public $rules = array();
	/**
	 *Oggetto restituito con cui
	 *validare le password
	 **/
	public $pswPolicyChecker = NULL;

	public function __construct(){
		
		$this->rules['min_length'] = 8;
		$this->rules['max_length'] = 16;
		$this->rules['min_numeric_chars'] = 1;
		$this->rules['max_numeric_chars'] = 15;
		$this->rules['min_caseInsensitive_chars'] = 1;
		$this->rules['max_caseInsensitive_chars'] = 15;
		$this->rules['no_special_char'] = TRUE;

		$this->pswPolicyChecker = new PasswordPolicy($this->rules);
		
	}
	
	public function getPswPolicyChecker(){
		
		return $this->pswPolicyChecker;
		
	}
}