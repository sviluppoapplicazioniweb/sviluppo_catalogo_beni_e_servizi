<?php
//include "configuration.inc";
include_once "$PATHINC/$PROGETTO/view/lib/db.class.php";
include_once "$PATHINC/$PROGETTO/prepend.php3";
/**
 * The UpdateCategories class contains function to update the categories
 */
class UpdateCategories
{
	private $db;
	private $dbCed;


	public function __construct()
	{
		$this->db = new DataBase();
		$this->dbCed = new DB_CedCamCMS;
	}


	private function convertLanguage($language){
		switch($language){
			case "IT": return "";

			case "EN": return "_AL";

			case "FR": return "_AL2";
		}
	}

	/**
	 * Function that converts htmlspecialchars  to UTF-8 charset
	 * @param $column colums Database to convert
	 * @param $language (IT, EN, FR)
	 */
	public function ConvertHtmlSpecialchars ($column,$language) {
		$column.=$this->convertLanguage($language);
		$strSql = "SELECT Id,$column FROM EXPO_Tlk_Categorie ORDER BY Id ";
		$allData=$this->db->GetRows($strSql);

		foreach($allData as $data){
			$dataColumn = htmlentities( $data["$column"], ENT_QUOTES, "UTF-8");
			$id =$data['Id'];
			$updateSql= "UPDATE EXPO_Tlk_Categorie SET $column='$dataColumn'	WHERE Id= $id";
			$var=$this->dbCed->query($updateSql);
		}
	}

	/**
	 * UpdateCategory updates DescEstesa of the category denoted by $id and  $language into EXPO_Tlk_Categorie
	 * @param $id
	 * @param $language (IT, EN, FR)
	 */
	public function UpdateCategoryByLanguage($id,$language){
		/*
		 *Extract row denoted by $id FROM EXPO_Tlk_Categorie
		*/

		$strSql = "SELECT Distinct Id_Categoria FROM EXPO_Tlk_Categorie WHERE Id like $id ";
		$lineResult=$this->db->GetRow($strSql);

		//Explode Id_Categoria
		$treeBranch = explode(".", $lineResult['Id_Categoria']);

		/*
		 * Create DescEstesa
		*/

		$level =count($treeBranch);

		for ($index=0; $index<$level; $index++){
			//Compose Id_Categoria by level
			if ($index==0){
				$idCategoria.=$treeBranch[0];
			}else{
				$idCategoria="$idCategoria.$treeBranch[$index]";
			}

			$column = "Descrizione".$this->convertLanguage($language);
			$brachSql = "SELECT $column FROM EXPO_Tlk_Categorie WHERE Id_Categoria like '$idCategoria' ";
			$branchResult=$this->db->GetRow($brachSql);
			$descLevel[]=$branchResult[$column];

		}
		//$descEstesa=htmlentities( implode(" -> ", $descLevel), ENT_QUOTES);
		$descEstesa=implode(" -> ", $descLevel);

		//print"<br>Descrizone Estesa: $descEstesa";

		/*
		 *UPDATE DescEstesa
		*/
		$column = "DescEstesa".$this->convertLanguage($language);
		$updateSql= "UPDATE EXPO_Tlk_Categorie SET $column='$descEstesa'	WHERE Id=$id";
		$var=$this->dbCed->query($updateSql);
	}


	/**
	 * UpdateCategory updates DescEstesa of the category denoted by $id into EXPO_Tlk_Categorie
	 * @param $id
	 *
	 */
	public function UpdateCategory($id){
		/*
		 *Extract row denoted by $id FROM EXPO_Tlk_Categorie
		*/

		$strSql = "SELECT Distinct Id_Categoria FROM EXPO_Tlk_Categorie WHERE Id like $id ";
		$lineResult=$this->db->GetRow($strSql);

		//Explode Id_Categoria
		$treeBranch = explode(".", $lineResult['Id_Categoria']);

		/*
		 * Create DescEstesa
		*/

		$level =count($treeBranch);

		for ($index=0; $index<$level; $index++){
			//Compose Id_Categoria by level
			if ($index==0){
				$idCategoria.=$treeBranch[0];
			}else{
				$idCategoria="$idCategoria.$treeBranch[$index]";
			}

				
			$brachSql = "SELECT Descrizione,Descrizione_AL,Descrizione_AL2 FROM EXPO_Tlk_Categorie WHERE Id_Categoria like '$idCategoria' ";
			$branchResult=$this->db->GetRow($brachSql);
				
			$descLevelIT[]=$branchResult["Descrizione"];
			$descLevelEN[]=$branchResult["Descrizione_AL"];
			$descLevelFR[]=$branchResult["Descrizione_AL2"];

		}
		//$descEstesa=htmlentities( implode(" -> ", $descLevel), ENT_QUOTES);
		$descEstesaIT=implode(" -> ", $descLevelIT);
		$descEstesaEN=implode(" -> ", $descLevelEN);
		$descEstesaFR=implode(" -> ", $descLevelFR);

		//print"<br>Descrizone Estesa: $descEstesa";

		/*
		 *UPDATE DescEstesa
		*/
		$column = "DescEstesa".$this->convertLanguage($language);
		$updateSql= "UPDATE EXPO_Tlk_Categorie SET DescEstesa='$descEstesaIT',DescEstesa_AL='$descEstesaEN',DescEstesa_AL2='$descEstesaFR'	WHERE Id=$id";
		$var=$this->dbCed->query($updateSql);
	}



	/**
	 * Update All Categories by language
	 * @param $language (IT, EN, FR)
	 *
	 */
	public function UpdateAllCategoriesByLanguage($language){
		/*
		 *Extract all Id FROM EXPO_Tlk_Categorie
		*/
		$strSql = "SELECT Distinct Id FROM EXPO_Tlk_Categorie ORDER BY Id ";
		$allId=$this->db->GetRows($strSql);

		foreach ($allId as $singleId){
			$this->UpdateCategoryByLanguage($singleId['Id'],$language);
		}
	}

	/**
	 * Update All Categories
	 *
	 */
	public function UpdateAllCategories(){
		/*
		 *Extract all Id FROM EXPO_Tlk_Categorie
		*/
		$strSql = "SELECT Distinct Id FROM EXPO_Tlk_Categorie ORDER BY Id ";
		$allId=$this->db->GetRows($strSql);

		foreach ($allId as $singleId){
			$this->UpdateCategory($singleId['Id']);
		}
	}

	/**
	 * Copy All Ateco Code from IT to EN and FR
	 */
	public function completeAllAtecoOtherLanguage(){
		$atecoSql = "SELECT  Id,Descrizione FROM EXPO_Tlk_Categorie WHERE Id_Categoria REGEXP '([0-9]{1,3}\.){3}[0-9]' ORDER BY Descrizione";
		$allAteco=$this->db->GetRows($atecoSql);

		foreach ($allAteco as $atecoRecord){
			$ateco = $atecoRecord['Descrizione'];
			$id= $atecoRecord['Id'];
			$updateSql= "UPDATE EXPO_Tlk_Categorie SET Descrizione_AL='$ateco',Descrizione_AL2='$ateco'	WHERE Id=$id";
			$var=$this->dbCed->query($updateSql);

		}
	}


	/**
	 * Copy Ateco Code from IT to EN and FR by $IdCategoria
	 */
	public function completeAtecoOtherLanguage($IdCategoria){
		$atecoSql = "SELECT  Descrizione FROM EXPO_Tlk_Categorie WHERE Id_Categoria='$IdCategoria' ";
		$ateco=$this->db->GetRow($atecoSql,Descrizione);		

		$updateSql= "UPDATE EXPO_Tlk_Categorie SET Descrizione_AL='$ateco',Descrizione_AL2='$ateco'	WHERE Id_Categoria='$IdCategoria'";
		$var=$this->dbCed->query($updateSql);


	}

	/**
	 * Clear ALL DescEstese
	 */
	public function svuotDescEstesa()
	{
		/*
		 *Extract all Id FROM EXPO_Tlk_Categorie
		*/
		$strSql = "SELECT Distinct Id FROM EXPO_Tlk_Categorie ORDER BY Id ";
		$allId=$this->db->GetRows($strSql);

		foreach ($allId as $singleId){
			$id = $singleId['Id'];
			$updateSql= "UPDATE EXPO_Tlk_Categorie SET DescEstesa='',DescEstesa_AL='',DescEstesa_AL2=''	WHERE Id=$id";
			$var=$this->dbCed->query($updateSql);
		}
	}



}


?>