<?php
require_once("dompdf_config.inc.php");

class CREAPDF extends DOMPDF {
	function CreaMioPdf($html_code,$path_dest,$file_dest,$header_mode="") {
		//$this->set_paper("A4");
		$path_templates = dirname(__FILE__);
		$html_pre = file_get_contents($path_templates."/html_pre.html");
		if (($header_mode == "1") || ($header_mode == "2") || ($header_mode == "3"))
			$html_post = file_get_contents($path_templates."/html_post_".$header_mode.".html");
		else
			$html_post = file_get_contents($path_templates."/html_post_0.html");
		//$html_code = $html_pre.$html_code.$html_post;
		$this->load_html($html_code);
		// $this->load_html_file($path_orig);
		$this->render();
		// $this->stream($path_dest.$file_dest);
		$pdf = $this->output();
		file_put_contents($path_dest.$file_dest, $pdf);

	}
}
?>
