<?php
include "cleaninput.inc";
if ($Id_VMenu != 309){

if(!cleanvariableglobal ($_GET , null, "GET")) {header ("Location: ".$HOMEURL."index.phtml");exit();}
if(!cleanvariableglobal ($_POST , null, "POST")) {header ("Location: ".$HOMEURL."index.phtml");exit();}
if(!cleanvariableglobal ($_COOKIE , null, "COOKIE")) {header ("Location: ".$HOMEURL."index.phtml");exit();}
// echo "@".__LINE__; exit;
}

/* PM-TODO - controllo su url:
  - pagina="/......": estraggo la directory e cerco se settata la variabile
  $Articolo["/".$directory]:
  - � settata: effettuo il controllo in procedura utenti
  - non � settata: proseguo
  - pagina="elenchi" o "form":
  controllo se esiste nome=$nome:
  - se non esiste su T_Pagine: vedo se � settata la variabile $Articoli[$nome]
  - � settata: effettuo il controllo in procedura utenti
  - non � settata: proseguo
  - se esiste: vedi punto successivo
  - Se aggancio il menu, leggo i dati di permission e verifico l'auth

 */
//	if(isset($CedCamCMS_Session))

page_open(array("sess" => "CedCamCMS_Session"));

// errori
$auth_errors["USER"] = "Utente già autenticato!";
$auth_errors["NO_USER"] = "Utente non autenticato!";
$auth_errors["NO_PERM"] = "Utente con autorizzazioni insufficienti!";
$auth_errors["NO_ID"] = "Applicazione non riconosciuta!";
//	$auth_errors["UAPI"]="Applicazione non riconosciuta!";

$db_menu = new DB_CedCamCMS;
$sSQL = "";
$_pag = "";
$_Id_Pagina = 0;
$_err_auth = 0;
if (is_object($auth)) {
	$utype = $auth->auth["utype"];
    $uname = $auth->auth["uname"];
    $uid = $auth->auth["uid"];
    $uid_utente = $auth->auth["id_utente"];
    $prov = $auth->auth["prov"];
    if ($utype < 4 and $GESTIONE_SITO == "avanzata")
        $tabsvil = "SVIL_";
} else {
    $utype = 99;
    $uid = 0;
    $uid_utente = 0;
    $uname = "invalid user";
}

$entra = 1;

if ($utype == "")
    $utype2 = 99;
else
    $utype2 = $utype;

include $PROGETTO . "/lang.inc";

if ($entra) {

    if (empty($Id_VMenu) && empty($pagina) && empty($explode)) {
        if ($utype == 1) {
            $Id_VMenu = 52;
        } elseif ($utype != 99) {
            if ($utype == 95) {
                $Id_VMenu = 321;
            }elseif ($utype == 93) {
                $Id_VMenu = 511;
            }elseif(strcmp($auth->auth['IdImpresa'], "") != 0){
                $Id_VMenu = 301;
            } else {
                $Id_VMenu = 307;
            }
        } else {
            $Id_VMenu = 318;
        }
    }
    if (!empty($Id_VMenu)) {
        $sSQL = "select * from " . $tabsvil . "T_Menu where Id='$Id_VMenu'";
    } elseif (!empty($explode)) {
        $sSQL = "select * from " . $tabsvil . "T_Menu where Id_Menu='$explode'";
    } elseif (!empty($pagina)) {
        if ($pagina == "form" || $pagina == "elenchi" || $pagina == "stampa")
            $_pag = $nome;
        elseif (substr($pagina, 0, 1) == "/") // è un modulo esterno
            $_pag = substr($pagina, 0, strpos($pagina, "/", 1));
        else
            $_pag = $pagina;

        if (!empty($_pag))
            $_Id_Articolo = $Articolo[$_pag];

        if ($nome == $TAB_ANAGRAFICA)
            $sSQL = "select " . $tabsvil . "T_Menu.* from T_Pagine inner join " . $tabsvil . "T_Menu ON " . $tabsvil . "T_Menu.Id_Pagina=T_Pagine.Id where Link like '%=$pagina%=$_pag%=$azione%'";
        else {
            $sSQL = "select " . $tabsvil . "T_Menu.* from T_Pagine inner join " . $tabsvil . "T_Menu ON " . $tabsvil . "T_Menu.Id_Pagina=T_Pagine.Id where (Link like '%=$_pag&%' or Link like '%=$_pag') and Id_Priorita<=" . $Priorita[$utype];
            if ($_Id_VMenu != "")
                $sSQL.=" and " . $tabsvil . "T_Menu.Id=" . $_Id_VMenu;
            $sSQL.=" order by Id";
        }

    }
    
    if ($DEBUG)
        echo "0- $sSQL<br>";
    if (!empty($sSQL)) {
        if ($DEBUG)
            echo "1- $sSQL<br>";
        $db_menu->query($sSQL);
        while ($db_menu->next_record()) {

            $_Id_Pagina = $db_menu->f(Id_Pagina);
            $explode = $db_menu->f(Id_Menu);
            $Id_VMenu = $db_menu->f(Id);
            $_Id_Priorita = $db_menu->f("Id_Priorita");
            $_Tipi_Utente = $db_menu->f("TipiUtente");
            $_Id_Articolo = $db_menu->f("Id_Articolo");
            $_Gruppi = $db_menu->f("Gruppi");
            if (!(strstr($_Tipi_Utente, "|" . $utype . "|")) === FALSE)
                break;
            if ($_Tipi_Utente == '|' || $_Tipi_Utente == '||' || $_Tipi_Utente == '|0|')
                $_Tipi_Utente = '';
        }
        if ($db_menu->num_rows() == 0)
            $_err_auth = "NO_ID";
    }
    if (!empty($_Id_Pagina)) {
        $sSQL = "select * from T_Pagine where Id='$_Id_Pagina'";
        if ($DEBUG)
            echo "2-" . $sSQL . "<br />";
        $db_menu->query($sSQL);
        if ($db_menu->next_record()) {
            if ($DEBUG)
                echo "2b-" . $sSQL . "<br />";

            $_prova = preg_replace("/\{%(\w+)%\}/e", "\$\\1", $db_menu->f(Link));
            parse_str($_prova, $_arr_pagine);
            reset($_arr_pagine);
            while (list($_key, $_val) = each($_arr_pagine)) {
                if ($DEBUG)
                    echo "3- $_key=$_val|" . ${$_key} . "|<br>";
                if ($_key == "nome" || $_key == "tmpl" || empty(${$_key}))
                    ${$_key} = $_val;
            }
            $flag_pubblico = $db_menu->f(Flag_Pubblico);
        }
    }
    if ($DEBUG)
        echo "4- tmpl=$tmpl|pagina=$pagina|nome=$nome|flag_pubblico=$flag_pubblico<br />";
    if ($_Tipi_Utente == '|99|') { // non deve essere loggato altrimenti va in errore
        if (is_object($auth)) {
            $_err_auth = "USER";
        }
    } elseif ($_Id_Priorita > 0 || !empty($_Tipi_Utente) || !empty($_Id_Articolo)) {
        if ($DEBUG)
            echo "-5-<br />";
        if (!is_object($auth)) {
            $_err_auth = "NO_USER";
        } else {
            if ($_Id_Priorita) {
                $Id_TipiUtente = array();
                $sSQL = "select Id_TipiUtente from Tlk_Priorita where Id='$_Id_Priorita'";
                if ($DEBUG)
                    echo "6-" . $sSQL . "<br />";
                $db_menu->query($sSQL);
                if ($db_menu->next_record())
                    $Id_TipiUtente = explode("|", substr($db_menu->f(Id_TipiUtente), 1, -1));
                if ($utype > $Id_TipiUtente[0] && !in_array($utype, $Id_TipiUtente)) {
                    $_err_auth = "NO_PERM";
                    if ($DEBUG)
                        echo "6a-" . $utype . " " . $Id_TipiUtente[0] . "<br />";
                }
                elseif (!empty($_Tipi_Utente)) {
                    $Id_TipiUtente = explode("|", substr($_Tipi_Utente, 1, -1));
                    if (!in_array($utype, $Id_TipiUtente))
                        $_err_auth = "NO_PERM";
                    if ($DEBUG)
                        echo "6b-" . $utype . " " . $Id_TipiUtente . "<br />";
                }
            }
        }

        if ($DEBUG)
            echo "7- _Id_Articolo=$_Id_Articolo|_err_auth|$_err_auth<br />";
        if (!$_err_auth && !empty($_Id_Articolo) && $utype > 1) {

            $sSQL = "select * from Tj_Applicazioni_X_Utente where Id_Utente='" . $uid . "' and Id_Applicazione=" . $_Id_Articolo;
            if ($DEBUG)
                echo "8-" . $sSQL . "<br />";
            $db_menu->query($sSQL);
            $db_menu->next_record();
            if ($db_menu->num_rows() == 0) {
                $_err_auth = "NO_PERM";
            }
        }
    }
    if ($DEBUG)
        echo "7b- _Gruppi=$_Gruppi|mygroup=$mygroup|_err_auth=$_err_auth|explode=$explode<br />";
    if (!$_err_auth && (($utype > 1 && $utype < 98))) {
        if ($DEBUG)
            echo "7c- " . $utype . "<br />";

        $checkgroup2 = checkgroup2($_Gruppi, $explode);
        if ($checkgroup2 == 0) {
            $_err_auth = "NO_PERM";
        }
    } else if ($utype == 98 && $_Gruppi != "|") {
        $_groupappl = explode("|", $_Gruppi);
        for ($idgroup = 0; $idgroup < sizeof($_groupappl); $idgroup++) {
            $file = $PROGETTO . "/langappl/lang_anagappl_" . $_groupappl[$idgroup] . ".inc";
            if (file_exists($PATHINC . $file)) {

                $group_appl = strpos($sess_anagcompl, "|" . $_groupappl[$idgroup] . "|");
                if ($group_appl === false) {
                    $okurl_site = $_SERVER['REQUEST_URI'];
                    $sql = "update T_Utente set okurl_site='" . $okurl_site . "' where Id=" . $uid_utente;
                    $db_menu->query($sql);
                    header("Location: " . $HOMEURL . "index.phtml?Id_VMenu=9&my_appl=" . $_groupappl[$idgroup]);
                }
            }
        }
    } else if ($utype == 97 && $Id_VMenu != 85) {

        // Se non presente in T_Ext_Anagrafica
        $sql = "select * from T_Ext_Anagrafica where Id=" . $uid_utente;
        $db_menu->query($sql);
        if ($db_menu->num_rows() == 0)
            header("Location: " . $HOMEURL . "index.phtml?Id_VMenu=85");
    }
}
// -- Modifica per problemi sicurezza su form T_Anagrafica
if ($nome == $TAB_ANAGRAFICA && $pagina == "form") {
    if (($azione == "INS" && $Id_TipoUtente != "" && $utype != 99 && $Id_TipoUtente <= $utype) || ($azione == "INS" && $Id_TipoUtente != "" && $utype == 99 && $Id_TipoUtente < 97))
        $_err_auth = "NO_PERM";
    elseif ($azione == "UPD") {
        $sSQL = "select Id_TipoUtente from T_Utente where Id='" . $Id . "'";
        if ($DEBUG)
            echo "8-" . $sSQL . "<br />";
        $db_menu->query($sSQL);
        if ($db_menu->num_rows() == 0)
            $_err_auth = "NO_PERM";
        else {
            $db_menu->next_record();
            if ($Id != $uid_utente) {
                if ($db_menu->f('Id_TipoUtente') <= $utype)
                    $_err_auth = "NO_PERM";
            }
            else if ($Id == $uid_utente) {
                if ($db_menu->f('Id_TipoUtente') != $utype)
                    $_err_auth = "NO_PERM";
            }
        }
    } else
        $entra = 1;
} else
    $entra = 1;
if ($DEBUG) {
    echo "_err_auth-" . $_err_auth . "<br />";
    echo "Id_VMenu-" . $Id_VMenu . "<br />";
    echo "entra-" . $entra . "<br />";
}

// -- Fine Modifica per problemi sicurezza su form T_Anagrafica

if ($DEBUG)
    echo "9-utype=" . $utype . "<br />";
if ($DEBUG)
    echo "10-uname=" . $uname . "<br />";
if ($DEBUG)
    echo "11-id_utente=" . $uid_utente . "<br />";
if ($DEBUG)
    echo "12-_err_auth=" . $_err_auth . "<br />";
if ($_err_auth) {

    $reg_mex = $auth_errors[$_err_auth];

    if (!is_object($sess)) {
        setcookie("reg_mex", $reg_mex, 0, "/","",true,true);
    } else {
        $sess->register("reg_mex");
        page_close();
    }

    if (!is_object($sess)) {
        setcookie("reg_mex", $reg_mex, 0, "/","",true,true);
        setcookie("okurl", $_SERVER['REQUEST_URI'], 0, "/","",true,true);
    } else {
        $sess->register("reg_mex");
        $okurl = $_SERVER['REQUEST_URI'];
        $sess->register("okurl");
        setcookie("okurl", $_SERVER['REQUEST_URI'], -1, "/","",true,true);
        page_close();
    }

    header("Location: " . $HOMEURL . "index.phtml");

    exit();
} else
    setcookie("okurl", "", 0, "/","",true,true);

if (empty($explode))
    $explode = "00";


if(!empty($$nome))
{
	if(!cleanvariableglobal ($_GET , $$nome, "GET")) {header ("Location: ".$HOMEURL."index.phtml");exit();}
	if(!cleanvariableglobal ($_POST , $$nome, "POST")) {header ("Location: ".$HOMEURL."index.phtml");exit();}
}

?>