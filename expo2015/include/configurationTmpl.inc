<?php

class Config {

      const DB_SERVER = 'test.digicamere.it',
            DB_NAME = 'expo_main',
            DB_USERNAME = 'expo2015',
            DB_PASSWORD = 'expo12!!',
            ROOTFILES = "D:/www/expo2015/docs/files/",
            ROOTFILESHTML = "files/",
            ROOTFILESIMAGE = "files/",
            SIZEMAX = "102400",  //KB
            URLTELEMACO = "http://localhost:8080/Telemaco_2_1/rest/ws/";
}

//MAIL SITO
$MAILADMIN = "alessandro.ferla@digicamere.it";
$MAILSITO = "noreply.catalogue@expo2015.org";
$MAILASSISTENZA = "assistenzacatalogo@expo2015.org";

//CARTELLE SITO
$PATHHOME = "/var/www/vhosts/digicamere.it/subdomains/test/";
$PATHDOCS = $PATHHOME."/httpdocs/";
$PATHLOG = $PATHDOCS . "logs/";
$PATHINC = $PATHHOME . "/include/";
$PATHPDFS = $PATHHOME . "pdf_delibere/";
$TAB_ANAGRAFICA_EXT = "T_Ext_Anagrafica";
$AZIONI = $PROGETTO . '/azioni/';

//DATI SITO
$PROGETTO = "expo2015";
$TITLESITE = "EXPO 2015";
$DESCSITE = "EXPO 2015";
$MAINSITE = "http://test.digicamere.it/";
$HOMEURL = "http://test.digicamere.it/";
$URLCNS = "https://cns2sv.mi.cciaa.net/autentica/getCNSinfo.php";
$URLPDF = "http://matrix.mi.cciaa.net/MatrixService.svc/MatrixListener/PL_AutoGeneratePDF";

//VARIABILI SITO
$arrayRootDoc = array(
    "aziende1" => array("A", "B", "C", "D", "E", "F"),
    "aziende2" => array("G", "H", "I", "J", "K", "L"),
    "aziende3" => array("M", "N", "O", "P", "Q", "R", "S"),
    "aziende4" => array("T", "U", "V", "W", "X", "Y", "Z")
);

$statoImpresa = array("4" => '<span class="testoStatoRed" >Off-Line</span>', "5" => '<span class="testoStato">On-Line</span>');
$templateUse = "catalogo";
$includeConfig = 1;
$includePrepend = 0;
$keySito = "1234567890abcdefghilmnop";
$DEBUG = 0;
$SPEDISCIMAIL = 1;
$LOGMAIL = 0;
$RESFORPAGE = 20;

// info per il pagamento
$idCliente="99999999";
$passCliente="99999999";

$widthImgLogo = 120;
$heightImgLogo = 120;

$timeout = '10'; //in minuti

$debugIscrzione=array(
    'iscrizione' => true, //True = Attiva - False = UnderConstruction.
	'visura' => false,	//True = Visura fake - False = Visura presa da Telemaco.
    'pdf' => true,	//True = Salta Verifica PDF - False = Verifica il PDF.
    'pagamento' => true, 	//True = Pagameno con TestMoneta - False = Pagamento con Moneta Online Produzione.
    'impresa' => false,	//True = Mette il tasto cancella Impresa - False = Toglie il tasto cancella Impresa.
    'visualizzapaginapagamento' =>true 	//True = Consente il pagamento - False = Mette Under construction sul pagamento.	
);

//$tmp_upload_path = "/tmp/"; //Versione ufficiale
$tmp_upload_path = "C:\\xampp\\tmp\\"; //Versione XAMPP
?>