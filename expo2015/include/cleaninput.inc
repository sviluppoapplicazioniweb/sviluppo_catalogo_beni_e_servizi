<?

function cleanvariable($array_variabili, $tipo_filtro)
{
	$out_array= array(); 

	foreach($array_variabili as $key => $value)
	{
		if (is_array($value))
			$out_array[$key] = filter_var_array($value, $tipo_filtro);
		else
			$out_array[$key] = filter_var($value, $tipo_filtro);

	}

	return $out_array;
}

function registraLog($chiave, $tipoArray, $messaggio, $oldvalue, $newvalue)
{
	global $SEMID,$CLEANERLOGFILE,$_SERVER;
	
	
	$sem=sem_get($SEMID);
	if(sem_acquire($sem))
	{
		$fp=fopen($CLEANERLOGFILE,"a");
		if($fp)
		{
			fprintf($fp,"[%s];%s;%s;%s;%s;%s;%s;%s;%s\n",date("D, d M H:i:s Y "),$tipoArray,$messaggio,$chiave,$oldvalue,$newvalue, $_SERVER["HTTP_REFERER"],$_SERVER["REMOTE_ADDR"],$_SERVER['HTTP_USER_AGENT']);
			fclose($fp);
		}
		sem_release($sem);
	}
}

function remove_javascript($java){

	return preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}

function remove_noscript($java){

	return preg_replace('~<\s*\bnoscript\b[^>]*>(.*?)<\s*\/\s*noscript\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}

function urldecode_array_element(&$item1, $key)
{
	$item1 = urldecode($item1);
}

function cleanvariableglobal($array_variabili, $tabella, $tipo_array="")
{
	if(empty($array_variabili))
		return true;
	
	foreach($array_variabili as $key => $value)
	{
		if (!(empty($tabella)))
		{
			if (is_array($value))
				array_walk($value, 'urldecode_array_element');
			else
			$value=urldecode($value);
		}

		$tempvalue=$value;
		$cleaned=0;
		$erroregrave=0;
		
//		if (empty($tabella))
//		{
			if (!(empty($value)))
			{
				switch ($key)
				{
					// variabili del tipo "01.01.03"
					case "Id_Gruppo":
					case "explode":
					case "Id_Fascia":
						$pregret=preg_match('/^(([0-9][0-9])([.][0-9][0-9])*)$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					case "Id_Menu":
						$pregret=preg_match('/^(([0-9][0-9])([.][0-9][0-9])*)$/', $value);
						if ($pregret!==1)
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
					case "my_numproc":
						$pregret=preg_match('/^([0-9\/])+$/', $value);
						if ($pregret!==1)
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
					case "Id_GiornoSettimana":
						$clean_array = array('Fri','Mon','Sat','Sun','Thu','Tue','Wed');
						if (!in_array($value,$clean_array))				
							$tempvalue = 'Fri';
						$cleaned=1;
						break;
					case "categoria":
						$pregret=preg_match('/^(([0-9]+)([,][0-9]+)*)$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					case "azione":
						$clean_array = array('INS','UPD','SEL','PAY','END','SAL','PAYMYBANK','UPR','ADR','UPDONE','DEL','RLN','UPDALL','VIEW','SEND','CLS','NEW','C','D','UPDCAT');
						if (!in_array($value,$clean_array))				
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "tipoOperazione":
						$clean_array = array('VisualizzaFaq');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "action":
						$clean_array = array('logout','Entra','INT','INS','UPD','DEL','VIEW','mybank','add','delete','ADD','EDIT','normal');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "option":
						$clean_array = array('user','regfirmadig','firmadig','INS','M','P','UPD');
						
						if (!in_array($value,$clean_array)){
							$varDecode = urldecode(base64_decode($value));
							//registraLog($key,$tipo_array,"OPTION",$value,$varDecode);
							$pregret=preg_match("/^([a-zA-Z]{8})+$/",$varDecode );
							if ($pregret!==1){
								$erroregrave=1;
								$messaggioclean="Check-base64".$key." fallito";
								$tempvalue = '';
							}else {
								$tempvalue = $value;
							}
						}
							
						$cleaned=1;
						/*
						
						if ($pregret!==1){
							$varTemp = base64_decode($value);
							$pregret1=preg_match("/^([0-9])+$/", $varTemp);
							if ($pregret1!==1){
								$erroregrave=1;
								$messaggioclean="Check-base64".$key." fallito";
								$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
							}else {
								$tempvalue = $value;
							}
						} else {
								
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						}*/
						
						break;
							
						
					case "pulsante":
					case "Pulsante":
						$clean_array = array('Aggiorna','Invia','Esegui','Salva');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "optype":
						$clean_array = array('mybank','normal');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "Login":
					case "E_Mail":
					case "EmailPEC":
					case "EmailFatturazione":
					case "mail":
					case "email":
					case "e_mail":
					case "username":
					case "ContattoRete":
					case "Email":
					case "a_E_mail":
						$tempvalue = filter_var($value, FILTER_SANITIZE_EMAIL);
						$pregret=preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					case "tmpl":
						$pregret=preg_match("/^(catalogo)+$/", $value);
						if ($pregret!==1){
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
							if($tempvalue<0 || $tempvalue>10)		
							{
								$erroregrave=1;
								$messaggioclean="Check ".$key." fallito";
								$tempvalue="";
							}
						}
						$cleaned=1;
						break;
					case "nome":
					case "tab_nome":
					case "pagina":
					
						case "tk":
						$pregret=preg_match('/^([a-zA-Z0-9_])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "PHPSESSID":
					case "CedCamCMS_Session":
						$pregret=preg_match('/^([a-zA-Z0-9])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						else
						{
							/*$db_clean=new DB_CedCamCMS;
							$db_clean->connect();
							$sSQL="select * from active_sessions where sid='$value'";
							$db_clean->query($sSQL);
							if($db_clean->num_rows()==0)
							{
								$erroregrave=1;
								$messaggioclean="Check A".$key." fallito";
								$tempvalue="";
							}*/
						}

						$cleaned=1;
						break;
					case "password":
					case "Password":
					case "Password_Input":
					case "Rep_Password":
					case "New_Password":
					case "Old_Password":
					case "Conferma_Password":
		/*
						$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);
						$pregret=preg_match('/^([a-zA-Z0-9_!])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
						}
		*/
						$cleaned=1;
						break;
					case "ordina":
						$pregret=preg_match('/^([a-zA-Z0-9_\s,])+$/', $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;

						break;

					case "uid":
					case "uid_utente":
					case "utype":
					case "nh":
					case "p":
					case "daabstract":
					case "tribunale":
					case "msx":
					case "Id_VMenu":
					case "idNazione":
					case "id_vmenu":
					case "Cap":
					case "CAPFatt":
					case "NumeroDipendenti":
					case "NumeroCategorie":
					case "fasciafatturato":
					case "CAP":
					case "NRea":
					case "sedeLegaleRadio":
					case "slide":
					case "Id_Faq":
					case "Id_Sottocategoria_Faq":
					case "Id_Sottocategoria_Faq_old":		
					
					case "IdImpresa":
					case "impresaID":
					case "idimpresa":
					case "idImpresa":
					case "idimp":						
					case "categioriaID":
					case "idcat":
					case "IdQuestionarioDefault":
					case" IdQuestionarioAltro":
					case "IdQuestionario":			
					case "ordineProfessionista":
					case "StatoRegistrazione":
					case "IdRete":
					case "idImp":
					case "idLg":
					case "fascia":
					case "IdQuestionarioAltro":
					case "idCert":
					case "lingue":
					case "idProf":
					case "ididOrdine":
					case "OrdineProfessionista":
					case "amount":
					case "paymentid":
					case "CreaLR":
					case "idOrdine":
						
						$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						break;
					case "messaggio":
						$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
						$cleaned=1;
						break;
					case "okurl":
						$tempvalue = filter_var($value, FILTER_SANITIZE_URL);
						$cleaned=1;
						break;
					//case "Specializzazioni":
					//	$tempvalue = filter_var_array($value, FILTER_SANITIZE_NUMBER_INT);
					//	$cleaned=1;
					//	break;
					// variabili Id
					case "cqs":
							$cqs_string=base64_decode($value);
						parse_str($cqs_string, $cqs_array);
							$cqs_array2=array();
						foreach ($cqs_array as $cqs_key => $cqs_value)
							{
								if ($cqs_key=="my_where")
									$cqs_array2[$cqs_key]= $cqs_value;
								else
									$cqs_array2[$cqs_key]= filter_var($cqs_value, FILTER_SANITIZE_STRING);
							}
							$tempvalue=base64_encode(http_build_query($cqs_array2));
							registraLog($key,$tipo_array,"CHECK CQS",$value,base64_encode(http_build_query($cqs_array2)));
							//$tempvalue=$cqs;
						$cleaned=1;
						break;
					
					/*
					case "telefono":
					case "Telefono":
					case "tel":
					case "Fax":
					case "fax":
						registraLog($key,$tipo_array,"CONTROLLARE",'','');
						/*$pregret=preg_match("/^([0-9\+\.-\S]+)$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
*/
						
						
					case "Pr":
					case "PRV":
					case "Provincia":
						$pregret=preg_match("/^([A-Z]{2})$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "CodFiscale":
					case "cf_impresa":
					case "cfimpresa":
					case "CodiceFiscale":
					case "CF":
						$pregret=preg_match("/^([0-9]{11}|[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
					case "nuovoInserimento":
						$pregret=preg_match("/^(true)+$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						
						break;
						
					case "ParIVA":
					case "PIva";
					case "PartitaIVA":
						
						$pregret=preg_match("/^([0-9]{11}|[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])$/", $value);
					
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
							
					/*	
					case "noteAltro":
						$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					*/	
						
					case "description":
						$pregret=preg_match("/^([a-zA-Z0-9\s:-])+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "alboProfessionista":
					case "professionista":
						$pregret=preg_match("/^([a-zA-Z0-9\s'\|])+$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;

						break;
						
					case "Certificazioni":
					case "Categorie":	
						/*
						 * Array passati dalla visura
						 */
						
						/*$pregret=preg_match('/^([a-zA-Z0-9\s])+$/', $value);
						
						if ($pregret!==1)
						{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
						}*/
						$cleaned=1;
						
						break;
						
					/*case "nav":
					case "navCat":
							$cleaned=0;
						break;	
					*/	
						
					case "listaLingueSito":
					case "listaLingue":
						$pregret=preg_match("/^(\|[A-Z]{2}){0,2}$/", $value);
						
						if ($pregret!==1)
						{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "ultimoFatturato":
					case "UltimoFatturato":
					case "FatturatoUltimoEsercizio":
					case "valore":
						$pregret=preg_match("/^([0-9\.,])+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
					/*case "lsReti":
						registraLog($key,$tipo_array,"CONTROLLARE",'','');
						/*$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
					
						$value = strip_tags($value);
						if ($pregret!==1)
						{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
						}
						//$tempvalue = ($value);
						$cleaned=1;
						break;
						*/
						
					case "nav":
					case "navCat":
					case "navCat":
					case "navReti":
						$pregret=preg_match("/^([a-zA-Z0-9,\s])+$/", $value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;

						
					case "lsReti":
					/*	 registraLog($key,$tipo_array,"CONTROLLARE",'','');
						/*$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
							
						$value = strip_tags($value);
						if ($pregret!==1)
						{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
						}
						//$tempvalue = ($value);
						$cleaned=1;
						break;
						*/
						
					//dati iscrizione
					case "formaGiuridica" :
					case "RagioneSociale" :
					case "Indirizzo" :
					case "Denominazione" :
					case "Via":
					/*	
					$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
					if ($pregret!==1)
					{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
					}
					$cleaned=1;
					break;
					*/
					
					case "telefono":
					case "Telefono":
					case "tel":
					case "Fax":
					case "fax":
					
					case "Citta":
					case "Comune":
					case "ComuneFatt":
					case "Denominazione":
					case "Via":
					case "CapitaleSociale":
					case "a_Domanda":
						
					case "descrizioneL":
					case "qualitaL":
					case "internazionalizzazioneL":
					case "referenzeL":
					case "webSite":
					case "WebSiteURL":
					case "retiImpresaL":
					case "linkRete":
					case "brevetti":
					case "brevettiEN":
					case "brevettiFR":
					case "aspetti":
					case "aspettiEN":
					case "aspettiFR":
					case "certificazione":
					case "certificazioneEN":
					case "certificazioneFR":
					case "prodottoL":
					case "aspettiL":
					case "brevettiL":
					case "certificazioneL":
						
					case "descrizione":
					case "descrizioneEN":
					case "descrizioneFR":
					case "qualita":
					case "qualitaEN":
					case "qualitaFR":
					case "DescrizioneAttivita";
					case "DescEstesa":
					case "DescEstesa_AL":
					case "DescEstesa_AL2":
					case "code":
					case "NomeRete":
					case "NRete":
					case "rete":
					case "descrizioneR":
					case "descrizioneREN":
					case "descrizioneRFR":
					case "Nome";
					case "NomeEN":
					case "NomeFR":
					case "Cognome":
					case "testo":
					case "a_Cognome":
						
					case "noteAltro":
					case "par":
					case "valueSelect":
						
					case "Descrizione":
					case "Descrizione_AL":
					case "Descrizione_AL2":
					case "commento":
					case "typology_problem":	
					case "name_problem":
					case "nome_mittente":
						
						//registraLog($key,$tipo_array,"CONTROLLARE",'','');
						/*$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
						
						$value = strip_tags($value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}*/
						$tempvalue = strip_tags(remove_noscript(remove_javascript($value)));
						$tempvalue = trim($tempvalue);
						$value = $tempvalue;
						$cleaned=1;
						break;

						
						
					case "reg_Lang":
					case "Lang2":
					case "lang":
						$pregret=preg_match("/^([A-Z]{2})$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "DataSurces":
						$pregret=preg_match("/^(Telemaco|Parix)+$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						
						break;
						
					case "impresa":
						
						$pregret=preg_match("/^((([0-9]{11}|[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])-([0-9]{5,9})-([A-Z]{2}))||^([0-9]{11}|[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]))$/", $value);
												
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						
						break;
						
					/*case "codeAteco":
						
						$pregret=$pregret=preg_match("/^([0-9]{1,3}\.){0,2}[0-9]*$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						
						break;
					*/	
					//registrazione legale rappresentante
					case "SSL_CLIENT_M_SERIAL":
					case "SSL_CLIENT_S_DN":
					case "SSL_CLIENT_S_DN_CN":
					case "SSL_CLIENT_S_DN_G":
					case "SSL_CLIENT_S_DN_S":
					case "SSL_CLIENT_S_DN_Email":
						registraLog($key,$tipo_array,"CONTROLLARE",$value,$tempvalue);
						//$erroregrave = 0;
						//$cleaned=1;
						break;
							
						
					case "hash":
					case "cyph":
						$pregret=preg_match("/^([a-z0-9])+$/", $value);

						$value = strip_tags($value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
						
					/*	//Delegato
					case "Nome":				
					case "Cognome":
					case "NomeEN":
					case "NomeFR":
						registraLog($key,$tipo_array,"CONTROLLARE",$value,$tempvalue);
						/*$pregret=preg_match("/^([a-zA-Z0-9\s[:punct:]])+$/", $value);
						
						$value = strip_tags($value);
						if ($pregret!==1)
						{
						$erroregrave=1;
						$messaggioclean="Check ".$key." fallito";
						$tempvalue="";
						}
						$cleaned=1;
						break;
						*/
					//pagamento
					
					case "orderid":
					case "securitytoken":
					case "idorder":
					case "merchantorderid":
						$pregret=preg_match("/^([a-z0-9])+$/", $value);

						$value = strip_tags($value);
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "gereralContractor":
					case "produttore":
					case "distributore":
					case "IsPrestatoreDiServizi":
					case "sedEstera":
					case "netetwork":
					case "partecipazioneExpo":
					case "partecipazioniInternazionali":
					case "referenzeL":
					case "aModello":
					case "IsGeneralContractor":
					case "IsProduttore":
					case "IsDistributore":
					case "IsPrestatoreDiServizi":
					case "IsSediEstere":
					case "IsNetwork":
					case "partecipazioneExpo":
					case "partecipazioniInternazionali":
					case "IsStudio":
					case "IsMasterSpecialistico":
					case "IsCertificazione":
					case "isCapogruppo":
					case "isSiexpo":
					case "pMaster":
					case "isStudio":
					case "pCertificazione":
					
					
						$pregret=preg_match("/^(Y|N|undefined)$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}else {
							if (strcmp($value, 'undefined') == 0){
								$tempvalue=$value = 'N';
							}
						}
						$cleaned=1;
						break;
					
					case "Delegato_Guest":	
						$pregret=preg_match("/^([a-zA-Z])+$/", $value);
												
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "tab":
						$pregret=preg_match("/^([a-zA-Z0-9])+$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "periodo":
					case "inizio":
					case "fine":
					case "dataInizio":
					case "dataFine":
						$pregret=preg_match("/^([0-9\/-])+$/", $value);
						
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "Id":
						$pregret=preg_match("/^([0-9])+$/", $value);

						if ($pregret!==1){
							$varTemp = base64_decode($value);
							$pregret1=preg_match("/^([0-9A-Za-z])+$/", $varTemp);
							if ($pregret1!==1){
								$erroregrave=1;
								$messaggioclean="Check-base64".$key." fallito";
								$tempvalue = '';
							}else {
								$tempvalue = $value;
							}
						} else {
							
							$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						}
						
						$cleaned=1;
						break;
						
					case "path":
					case "rootFile":
					case "filePath":
					case "nameinput":	
						$value1 = str_replace("../", "", $value);
						$pregret=preg_match("/^([a-zA-Z0-9\/\._-])+$/", $value1);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "StatoProdotto":
						$pregret=preg_match("/^(A|D)$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
					case "html":
						registraLog($key,$tipo_array,"HTML - PDF",'','');
						$cleaned=1;
						break;
					
					case "typeCSV":
						$clean_array = array('imprese','legaliRappresentati');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
					
					case "modulo":
						$clean_array = array('Invia','Esegui');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;

					case "chiamata":
						$clean_array = array('Telemaco','Parix','telemaco','parix');
						if (!in_array($value,$clean_array))
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
							
					case "maskedpan":
						$pregret=preg_match("/^([0-9\*])+$/", $value);
						//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
						if ($pregret!==1)
						{
							$erroregrave=1;
							$messaggioclean="Check ".$key." fallito";
							$tempvalue="";
						}
						$cleaned=1;
						break;
						
					case "codeAteco":
						$cleaned=1;
						break;
						
					default:
						
						/*if((substr($key,0,2) == "Id"))
						 {
						$tempvalue = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
						$cleaned=1;
						}*/
						
						$keyPregret=preg_match("/^(categori[e|a][0-9]+|certificazione[0-9]+)$/", $key);
						
						if ($keyPregret==1)
						{
							$pregret=preg_match("/^(Y|N|undefined)$/", $value);
							//registraLog($key,$tipo_array,"CATEGORIE",$value,$tempvalue);
							
							if ($pregret!==1)
							{
								$erroregrave=1;
								$messaggioclean="Check ".$key." fallito";
								$tempvalue="";
							}else {
								if (strcmp($value, 'undefined') == 0){
									$tempvalue = 'N';
								}
							}
							$cleaned=1;
						}else {
							if ($tipo_array != "COOKIE"){
								registraLog($key,$tipo_array,"DEFAULT",$value,$tempvalue);
							}
						}
						break;
				}
			}
			else
			{	
				//registraLog($key,$tipo_array,"VUOTE",$value,$tempvalue);
				$cleaned=1;
			}
//		}

		if(!$cleaned && !$erroregrave)
		{
			if (isset($tabella))
			{
				foreach ($tabella as $chiave => $valore)
				{
					$campo= explode("|",$valore);


					$needlePos=strpos($chiave,$key.'|');
					if((string)$needlePos === (string)0)
					{
						switch ($campo[7])
						{
							case "TEXT":
							case "PASSWORD":
							case "HIDDEN":
							case "FILE":
							case "LABEL":
							case "LABEL2":
							case "SELECTREFRESH":
							case "SELECTFILTRO":
							case "SELECTDISTINCT":
							case "SELECTJOIN":
							case "SELECT":
							case "SELECTHIDDEN":
							case "SELECTHIDDEN2":
							case "SELECTHIDDEN3":
							case "ELENCO":
							case "DATATEXT":
							case "DATAITA":
							case "WWW":
							case "MAIL":
							case "LINKS":
							case "FUNZIONE":
							case "HTMLCONDIZ":
							case "SEPARATORE":
							case "DATA":
							case "RADIO":
							case "BUTTON":
							case "TEXTAREA":
								$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
								break;
							case "CHECKBOX":
								$tempvalue = filter_var_array($value, FILTER_SANITIZE_STRING);
								break;
							case "HTMLAREA":
								$tempvalue = mysql_real_escape_string($value);
//								$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);
								break;

							default:
								$tempvalue = filter_var($value, FILTER_SANITIZE_STRING);
								break;
						}
						$cleaned=1;
						$GLOBALS[$key]=$tempvalue;
						break;
					}
				}
			}
		}

		if (!$cleaned)
		{
			if (is_array($value))
				$tempvalue = filter_var_array($value, FILTER_SANITIZE_ENCODED);
			else
				$tempvalue = filter_var($value, FILTER_SANITIZE_ENCODED);

			$cleaned=1;
			$GLOBALS[$key]=$tempvalue;
		}


		if ($cleaned==1)
		{
		//	registraLog($key,$tipo_array,"variabile pulita1",$value,$tempvalue);
			$GLOBALS[$key]=$tempvalue;
			if ($tipo_array=="GET")
				$_GET[$key]=$tempvalue;
			if ($tipo_array=="POST")
				$_POST[$key]=$tempvalue;
			if ($tipo_array=="COOKIE")
				$_COOKIE[$key]=$tempvalue;
		//	registraLog($key,$tipo_array,"variabile pulita2",$value,$tempvalue);
		}


		if ($erroregrave==1)
			registraLog($key,$tipo_array,$messaggioclean,$tempvalue,$value);
		elseif (empty($value))
		{
			//registraLog($key,"variabile pulita",$value,$tempvalue);
		}
		elseif ($tempvalue==$value || $key=="__utma" || $key=="__utmb" || $key=="__utmc" || $key=="__utmz") // || $tempvalue==urldecode($value) 
		{
			//registraLog($key,$tipo_array,"VVAriabile pulita",$value,$tempvalue);
		}
		elseif ($tempvalue!=$value && ($key=="password" || $key=="Password" || $key=="Password_Input" || $key=="Rep_Password" || $key=="New_Password" || $key=="Old_Password"))
			registraLog($key,$tipo_array,"VARIABILE MODIFICATA","PASSWORD","PASSWORD");
		elseif ($tempvalue!=$value)
			registraLog($key,$tipo_array,"VARIABILE MODIFICATA",$value,$tempvalue);
		else
			registraLog($key,$tipo_array,"VARIABILE SCONOSCIUTA",$value,$tempvalue);

	}
//	registraLog($key,"CHECK EXIT",$cleaned,$erroregrave);

	return (!$erroregrave && $cleaned==1);
}
