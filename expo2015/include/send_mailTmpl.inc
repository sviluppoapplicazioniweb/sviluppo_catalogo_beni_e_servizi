<?php

function send_mail($to_address, $from_address, $subject, $message, $html = 0, $fax = 0, $bcc = 0,$header="") {
    global $PROGETTO, $LOGMAIL, $PATHINC, $PATHLOG;
    //if (!(isset($PROGETTO))) $PROGETTO="_log";
    if (!(isset($LOGMAIL)))
        $LOGMAIL = 0;
    if (!(isset($PATHLOG)))
        $PATHLOG = "logs/";
    //if (!(isset($PATHINC))) $PATHINC="/var/www/vhosts/expo2015.it/include/";
    //echo dir(__DIR__);
    require("lib/PHPMailer/class.phpmailer.php");
    $mail = new PHPMailer();

    include "lib/PHPMailer/language/phpmailer.lang-it.php";
    $mail->SetLanguage("it", "lib/PHPMailer/language/");

    $mail->IsSMTP();                                      // Set mailer to use SMTP
    $mail->Host = '192.168.10.21';  // Specify main and backup server
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'abc-noreply@soft.it';                            // SMTP username
    $mail->Password = 'mantis2012ABC!%';                           // SMTP password
    //$mail->Port = 993;
    //$mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted

    $pospv = strpos($to_address . $from_address, ";");
    if ($pospv === false)
        $char_exp = ",";
    else
        $char_exp = ";";

    $from = explode($char_exp, $from_address);
    $mail->From = $from[0];
    $mail->FromName = "Catalogo Fornitori Beni e Servizi Expo 2015";


    if ($html && $fax == 0) {
        include ("lib/html2text.inc");
        $mail->IsHTML(true);             // set email format to HTML
        $asciiText = new Html2Text($message, 80); // 80 columns maximum
        $message_txt = $asciiText->convert();
        $mail->Body = $message;
        $mail->AltBody = $message_txt;
    } elseif ($fax) {
        $head = "<!DOCTYPE html PUBLIC \" - //W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        $head.="<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">";
        $head.="<head>";
        $head.="<title>" . $subject . "</title>";
        $head.="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />";
        $head.="</head>";
        $head.="<body>";
        $foot = "</body></html>";

        $ffax = fopen("tmpfax.html", "w");
        fputs($ffax, $head);
        fputs($ffax, $message);
        fputs($ffax, $foot);
        fclose($ffax);

        $mail->AddCustomHeader("X-Cover: no\r\n");
        system($PATHINC . "html2ps/html2ps tmpfax.html > tmpfax.ps");
        $mail->AddAttachment("tmpfax.ps");    // add attachments
        $mail->AddAddress($to_address . "<desc^" . $to_address . "@fax.mi.camcom.it>");
        $to_address = $to_address . "<desc^" . $to_address . "@fax.mi.camcom.it>";
    } else {
        $mail->IsHTML(false);             // set email format to HTML
        $mail->Body = $message;
    }

    $mail->Subject = $subject;

    if (!($fax)) {
        $to = explode($char_exp, $to_address);
        for ($i = 0; $i < sizeof($to); $i++) {
            if ($bcc)
                $mail->AddBCC($to[$i]);
            else
                $mail->AddAddress($to[$i]);
        }
    }



    if (!$mail->Send()) {
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();
        $mail->ClearCustomHeaders();
        return 'Mailer error: ' . $mail->ErrorInfo;

        return 0;
    }
    $mail->SmtpClose();
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
    $mail->ClearCustomHeaders();

    return 1;
}

function send_mail_html($to_address, $from_address, $subject, $message) {
    return send_mail($to_address, $from_address, $subject, $message, 1);
}

function send_mail_html_file($to_address, $from_address, $subject, $message, $my_file)
{
	return send_mail($to_address, $from_address, $subject, $message, 1,0,0, $my_file);
}

function send_mail_html_file_header($to_address, $from_address, $subject, $message, $header)
{
	return send_mail($to_address, $from_address, $subject, $message, 1,0,0,"", $header);
}

?>