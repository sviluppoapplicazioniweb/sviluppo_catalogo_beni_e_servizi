<?php
/*
Programma per l'invio via email di una form.
E' necessario definiri i seguenti campi hidden:
- e_mail
- soggetto_messaggio
- destinatario_messaggio
- ritorno_messaggio (URL di ritorno da inserire senza 'http://').
Il campo e_mail pu� anche non essere nascosto per far risultare come mittente
la persona che compila il messaggio ma deve essere obbligatorio e deve esserci
un controllo di validit� dello stesso.
*/

include "send_mail.inc";

function scrivi_dati($valore){
$myFile = "file.txt";

if (!$valore){
	#controllo che nessun campo sia vuoto
	$errore=true;
}
#controllo che il file sia accessibile
if (!$fh = fopen($myFile,"a+")) {
	$errore=true;
	//echo "non posso aprire il file";
	//exit;
}

if (!fwrite($fh, $valore)){
	//echo "non siamo riusciti a salvare il testo";
	//exit;
}
fclose ($fh);
}

$count = count($_POST);
$g=0;
$message="";
$riga="";
while(list($name, $value) = each($_POST))
{
  switch ($name) {
  case ("idMail"):
  	$idMail = $value;
	$riga .= $value & ";";
	break;
  case ("soggetto_messaggio"):
  	$subject=$value;
  	break;
  case ("destinatario_messaggio"):
  	$to_address=$value;
  	break;
  case ("ritorno_messaggio"):
  	$ritorno=$value;
  	break;
  case ("e_mail"):
  	$from_address=$value;
  	break;
  case ("privacy"):
  	break;
  default:
  	if ($name != "nome_form"){
		$riga .= $value.";"; 
		$message.=$name.": ".$value."\n";
	}
  	break;
  }
$g++;
}

if ($ritorno=="") $ritorno='/index.phtml?Id_VMenu=263&idMail='.$idMail.'&risp=99';
if ($from_address=="") $from_address="info@siexpo2015.it";
if ($to_address=="") 
{
	switch ($nome_form)
	{
		case "test":
			$to_address="alessandro.ferla@digicamere.it";
			break;
		case "iscrizione":
			$to_address="info@siexpo2015.it";
			//$to_address="alessandro.ferla@digicamere.it";
			break;

		default:
			break;
	}
			
}
//echo $g."---".$to_address."---".$from_address."---".$subject."---".$message;exit;
send_mail($to_address, $from_address, $subject, $message);
scrivi_dati($riga."\r\n");
header ('Location: '.$ritorno);


?>