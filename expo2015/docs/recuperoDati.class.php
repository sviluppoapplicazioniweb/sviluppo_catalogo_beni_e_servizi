<?php
include "configuration.inc";
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/parix.class.php");



class recuperoDati {
	
	private $db;
	private $parix;
	
	private $countParix;
	private $countTelemaco;
	
	Const PRINT_T_VISURE = false;
	Const PRINT_DATI_RI_VISURE = false;
	
	Const PRINT_VISURA_ATECO = false;
	
	Const PRINT_IMPRESE_ESTRATTE_DA_SCHEDA_PERSONA = false;
	
	Const PRINT_CATEGORIE_DA_VISURA = false;
	Const PRINT_CATEGORIE_DA_DB = false;
	
	Const PRINT_PRODOTTI_IMPRESA = false;
	
	Const PRINT_CATEGORIA_ECCEDENTE = false;

	Const PRINT_CATEGORIE_ECCEDENTI = false;
	
	public function __construct(){
		$this->db =  new DataBase();
		$this->parix = new Parix();
		
		$this->countParix = 0;
		$this->countTelemaco = 0;
	}
	
	public function stampaBoolean ($value){
		if ($value){
			print "TRUE<br>";
		}else {
			print "FALSE<br>";
		}
	}
	
	
	public function in_array_r($needle, $haystack, $strict = false) {
		foreach ($haystack as $item) {
			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Funzione che recupera tutte le imprese dalle schede persona salvate in DATIRI_MAIN
	 * e ritorna un array contente codiceFiscale, RagioneSociale, cciaa e Numero Rea.
	 * 
	 * L'identificazione di un'impresa nell'array avviene mediante codice fiscale.
	 * 
	 * @return array $imprese;
	 * 
	 */
	function recuperoImpreseDaSchedePersone(){
		// Estrazione tutte le schede persona
		//$sqlAllSchedaPersona = "SELECT risposta FROM interrogazioni_telemaco WHERE url_chiamata like 'schedaPersona:codiceFiscale=RGNCRL40M42A010Q&user=pippo'";
		$sqlAllSchedaPersona = "SELECT risposta FROM interrogazioni_telemaco WHERE url_chiamata like 'schedaPersona:codiceFiscale=HLTCLS56T16Z112Q&user=pippo'";
		//$sqlAllSchedaPersona = "SELECT risposta FROM interrogazioni_telemaco WHERE url_chiamata like 'schedaPersona:codiceFiscale%'";
		
		$allSchedaPersona = $this->db->GetRows($sqlAllSchedaPersona, Config::DB_DATIRI_NAME);
		 
		/*
		print "<pre>all";
		print_r ($allSchedaPersona);
		print "</pre>";
		*/
		$arrayImpreseDaSchedePersona = array ();
		
		
		foreach ($allSchedaPersona as $key => $persona){

			//echo "Risposta: ".$strRisposta;
			$rispo = explode("=>", $persona[0]);
	
			$replace = trim(str_ireplace("[INFO]", '', $rispo[1]));
			
			//$replace = trim(str_ireplace(".", '', $replace));
			$strXML = trim($rispo[2]);
				
			$xml = array(
					"STATUS" => $replace,
					"INFO" => $strXML,
			);
			
				

				
			$array = @json_decode(json_encode((array)simplexml_load_string($xml['INFO'])),1);
			/*
			print "<pre>all";
			print_r ($array);
			print "</pre>";
			*/
			
			
			/*
			print "<pre><h5>Array-----------------------++++++++++++</h5>";
			print_r ($array['dati']);
			print "Count:"; 
			
			print count($array['dati']['blocchi-persona']['imprese-persona']['impresa-persona']['dati-identificativi-impresa']);
			print_r ($array['dati']['blocchi-persona']['imprese-persona']['impresa-persona']);
			
			print "</pre>";	
			*/
				
			if(count($array['dati']['blocchi-persona']['imprese-persona']['impresa-persona']['dati-identificativi-impresa']) == 4) {
				//	print "pippo";

					$value = $array['dati']['blocchi-persona']['imprese-persona']['impresa-persona'];
					/*
					print "<h4>Key:$key aaa</h4>";
					print "<pre><h4>value</h4>";
					print_r ($value);
					print "</pre>";
						*/
					
					$impresa = array ();
					$impresa["Denominazione"] = $value['dati-identificativi-impresa']['@attributes']['denominazione'];
					$impresa["CodiceFiscale"] = $cFiscaleImpresa = $value['dati-identificativi-impresa']['@attributes']['c-fiscale'];
					$impresa["cciaa"] = $value['dati-identificativi-impresa']['@attributes']['cciaa'];
					$impresa["NRea"] = $value['dati-identificativi-impresa']['@attributes']['n-rea'];
						
						
					$arrayImpreseDaSchedePersona[$cFiscaleImpresa]= $impresa;
						
				
			}	else
			
			if(count($array['dati']['blocchi-persona']['imprese-persona']['impresa-persona'])>0) {
			//	print "pippo";
				foreach ($array['dati']['blocchi-persona']['imprese-persona']['impresa-persona'] as  $value) {
					
					/*if ( ($key == 0) && ($value['@attributes']) ) {
						print "ciao";
						//$value = $value['@attributes'];
					}*/
					/*
					print "<h4>Key:$key aaa</h4>";
					print "<pre><h4>value</h4>";
					print_r ($value);
					print "</pre>";
					*/
					$impresa = array ();
					$impresa["Denominazione"] = $value['dati-identificativi-impresa']['@attributes']['denominazione'];
					$impresa["CodiceFiscale"] = $cFiscaleImpresa = $value['dati-identificativi-impresa']['@attributes']['c-fiscale'];
					$impresa["cciaa"] = $value['dati-identificativi-impresa']['@attributes']['cciaa'];
					$impresa["NRea"] = $value['dati-identificativi-impresa']['@attributes']['n-rea'];
				
					
					$arrayImpreseDaSchedePersona[$cFiscaleImpresa]= $impresa;
				
				}
			}
			
			
		
		
		}
		
		//stampa imprese estratte
		if (self::PRINT_IMPRESE_ESTRATTE_DA_SCHEDA_PERSONA){
			print "<pre><h1>impresa</h1>";
			print_r ($arrayImpreseDaSchedePersona);
			print "</pre>";
			print count($arrayImpreseDaSchedePersona);
		}
		
		return $arrayImpreseDaSchedePersona;
	}
	
	
	/**
	 * Funzione che recupera il numero rea dell'impresa.
	 * impresa: Id,RagioneSociale,CodiceFiscale
	 *
	 * @param unknown $impresa
	 */
	public function associazioneNrea($impresaId,$codiceFiscale,$arrayImpreseDaSchedePersona){
	
		if ($arrayImpreseDaSchedePersona[$codiceFiscale]){
			print "<h4 style=\"color: blue\">Impresa trovata in datiRI</h4>";
			return $arrayImpreseDaSchedePersona[$codiceFiscale]['NRea'];
		}else{
			print "<h4 style=\"color: blue\">cerco associazione da Utenti Nrea</h4>";
			$sqlUtentiImpresa="Select EXPO_TJ_Utente_NRea.NRea from EXPO_TJ_Imprese_Utenti
       			join T_Anagrafica on T_Anagrafica.Id=EXPO_TJ_Imprese_Utenti.IdUtente
       			join EXPO_TJ_Utente_NRea on EXPO_TJ_Utente_NRea.CodiceFiscale=T_Anagrafica.Codice_Fiscale
       			where EXPO_TJ_Imprese_Utenti.IdImpresa='".$impresaId."' ";
			//print $sqlUtentiImpresa;
			$utentiImpresa =array();
			$utentiImpresa = $this->db->GetRows($sqlUtentiImpresa);
				
			return $utentiImpresa;
		}
	
	}
	
	
	/**
	 * Parserizzazione della visura estratta da DB EXPO_T_Visure in un array.
	 * @param $visuraDB
	 * @return arrayVisura
	 */
	public function parseXMLVisuraExpo_T_Visure ($visuraDB){
		/*
		print "<pre><h2>Visura da DB</h2>";
		print_r($visuraDB);
		print "</pre>";
		*/
		
		
		//$array = @json_decode(json_encode((array)simplexml_load_string($xml['INFO'])),1);
		$visuraArray = @json_decode(json_encode((array)simplexml_load_string($visuraDB)),1);
		
		if (self::PRINT_T_VISURE){
			print "<pre><h2>Visura parse</h2>";
			print_r($visuraArray);
			print "</pre>";
		}
		
		return $visuraArray;
	}
	
	
	
	
	/**
	 * Parserizzazione della visura estratta da DB DatiRI_Main in un array.
	 * @param $visuraDB
	 * @return arrayVisura
	 */
	public function parseXMLVisuraDatiRI ($visuraDB){
		/*
		print "<pre><h2>Visura da DB</h2>";
		print_r($visuraDB);
		print "</pre>";
		*/
		//echo "Risposta: ".$strRisposta;
		$rispo = explode("=>", $visuraDB);
		
		$replace = trim(str_ireplace("[INFO]", '', $rispo[1]));
		
		//$replace = trim(str_ireplace(".", '', $replace));
		$strXML = trim($rispo[2]);
		
		$xml = array(
				"STATUS" => $replace,
				"INFO" => $strXML,
		);
		
		
		/*
		print "<pre><h2>Visura da DB xml</h2>";
		print_r($xml['INFO']);
		print "</pre>";
		*/

		
		//$array = @json_decode(json_encode((array)simplexml_load_string($xml['INFO'])),1);
		$visuraArray = @json_decode(json_encode((array)simplexml_load_string($xml['INFO'])),1);
	
		if (self::PRINT_DATI_RI_VISURE){
			print "<pre><h2>Visura parse</h2>";
			print_r($visuraArray);
			print "</pre>";
		}
	
		return $visuraArray;
	}
	
	
	/**
	 * Funzione che cerca la visura in datiRi_main.
	 * Se la trova ritorna la visura altrimenti ritorna false
	 * 
	 * @param $cciaa
	 * @param $rea
	 * @return boolean|visura 
	 */
	public function estraiVisuraDaDatiRIMain($cciaa,$rea){
		$user="pippo";

		$req_datiri="cciaa=".$cciaa."&rea=".$rea."&user=".$user;
		$sqlVisuraImpresa = "SELECT * FROM interrogazioni_telemaco WHERE url_chiamata = 'visuraOrdinaria:".$req_datiri."' ORDER BY data_chiamata DESC Limit 0,1";
		// estrazione 
		//print $sqlVisuraImpresa."<br>";
		 $result = $this->db->GetRow($sqlVisuraImpresa, null,Config::DB_DATIRI_NAME);
				 
		 $visuraImpresa = $result['risposta'];
		 $dataChiamata = $result['data_chiamata'];
		 
		if ($visuraImpresa){
			print "<h4 style=\"color: green\">Visura trovata in DatiRI_Main ( $dataChiamata )</h4>";
		}else {
			print "<h4 style=\"color: red\">Visura Non trovata in DatiRI_Main</h4>";
			return false;
		}
		
		//print "<h3>visura</h3>";
		return array("DataSurces" =>"Telemaco","DataChiamata" => $dataChiamata,"Visura" => $visuraImpresa);
		
	}
	
	
	

	/**
	 * Funzione che cerca la visura nella tabella expo_t_visure.
	 * Se la trova ritorna la visura altrimenti ritorna false
	 *
	 * @param $cciaa
	 * @param $rea
	 * @return boolean|visura
	 */
	public function estraiVisuraDaT_Visure($cciaa,$rea){
		
		$sqlVisuraImpresa = "Select * from expo_t_visure where cciaa='".$cciaa."' and rea='".$rea."' ";
		//print $sqlVisuraImpresa."<br>";
	
		// estrazione
		$visuraImpresa = $this->db->GetRow($sqlVisuraImpresa,'xml');
		if ($visuraImpresa){
			print "<h4 style=\"color: green\">Visura trovata in expo_t_visure</h4>";
		}else {
			print "<h4 style=\"color: red\">Visura Non trovata in expo_t_visure</h4>";
			
			return false;
		}
	
		
		return array("DataSurces" =>"Telemaco","DataChiamata" => '',"Visura" => $visuraImpresa);
	
	}
	
	
	/**
	 * Funziona che controlla associazione tra la visura e l'impresa.
	 * @param unknown $codiceFiscaleDB
	 * @param unknown $codiceFiscaleVisura
	 * @return boolean
	 */
	public function controlloAssociazioneVIsuraImpresa($codiceFiscaleDB,$codiceFiscaleVisura){
		
		if (strcmp($codiceFiscaleDB, $codiceFiscaleVisura) == 0){
			print "<h3 style=\"color: green\">Corrispondenza tra Visura ed Impresa</h3>";
			return true;
		}
		print "<h3 style=\"color: red\">Non Corrispondenza tra Visura ed Impresa</h3>";
		return false;
	}
	
	
	
	public function verificaPresenzaAtecoDB($ateco,$printPresenza = false){
		$sqlSelectAteco = "Select * from EXPO_T_Ateco where CodiceAteco='$ateco'";
		$idSelectAteco = $this->db->GetRow($sqlSelectAteco,'Id');
		
		if ($idSelectAteco>0){
			if ($printPresenza){
				print "<h4 style=\"color: green\">Ateco Presente nel DB: $idSelectAteco</h4>";
			}
			return $idSelectAteco;
		}
		if ($printPresenza){
			print "<h4 style=\"color: red\">Ateco Non presente nel DB</h4>";
		}
		
		return false;
	}
	
	
	public function cercaVisuraParix($cciaa,$nrea){
		
		if ( (strcmp($cciaa,"MI") == 0) || (strcmp($cciaa,"MB") == 0) ){
			$logParix = $this->parix->RecuperaInfoAziendaFromCfLogParix($nrea, $cciaa);


			if ($logParix['Visura']){
				print "<h4 style=\"color: green\">Visura trovata in Parix ( ".$logParix['DataChiamata']." )</h4>";
			}else {
				print "<h4 style=\"color: red\">Visura Non trovata in Parix</h4>";
				return false;
			}

		}else {
			return false;
		}

		return $logParix;
	}

	/**
	 * Funzione che controlla l'esistenza della visura nei DB e l'effettivo abbinamento impresa visura.  
	 * @param $cciaa
	 * @param $nrea
	 * 
	 * return $arrayVisura
	 */
	public function cercaVisura($cciaa,$nrea,$codiceFiscaleDB){
		
		$dataChiamata='';
		$visuraDatiRiMain = $this->estraiVisuraDaDatiRIMain($cciaa, $nrea);
		$visuraXML = $visuraDatiRiMain['Visura'];
		$dataChiamata = $visuraDatiRiMain['DataChiamata'];
		//Visura trovata in DatiRI_Main
		if ($visuraXML){
			$visura = $this->parseXMLVisuraDatiRI($visuraXML);
				
			$codiceFiscaleVisura = $visura['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['c-fiscale'];
			if (!($this->controlloAssociazioneVIsuraImpresa($codiceFiscaleDB, $codiceFiscaleVisura))){
				$visuraXML = false;
				//$this ->stampaBoolean($visuraXML);
			}
		}
		if (!$visuraXML){
			if (is_array($nrea)){
				$findVisura = false;
				foreach ($nrea as $keyRea => $valueRea ){
					//se non ho gi� trovato la visura proseguo nella ricerca
					if (!($findVisura)){

						//print $keyRea."<br>";
						//print $valueRea['NRea']."<br>";
						$visuraT_Visure = $this->estraiVisuraDaT_Visure($cciaa, $valueRea['NRea']);
						$visuraXML = $visuraT_Visure['Visura'];
						$visura = $this->parseXMLVisuraExpo_T_Visure($visuraXML);
						if (!$visura){
							//Visura non trovata
							return false;
						}
						$codiceFiscaleVisura = $visura['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['c-fiscale'];
						if ($this->controlloAssociazioneVIsuraImpresa($codiceFiscaleDB, $codiceFiscaleVisura)){
							$findVisura = true;
						}
						
					}
					
				}

				if (!($findVisura)){
					return false;
				}
			}else{
				$visuraT_Visure = $this->estraiVisuraDaT_Visure($cciaa, $nrea);
				$visuraXML = $visuraT_Visure['Visura'];
				$visura = $this->parseXMLVisuraExpo_T_Visure($visuraXML);
				$codiceFiscaleVisura = $visura['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['c-fiscale'];
				if (!($this->controlloAssociazioneVIsuraImpresa($codiceFiscaleDB, $codiceFiscaleVisura))){
					return false;
				}
			}
		}
		
	return array("DataChiamata" => $dataChiamata,"Visura" => $visura);
		
	}
	
	
	
	
	/**
	 * Funzione che estrae i codici ateco dalla visura 
	 * @param array $visura
	 * @return array $atecoVisura
	 */
	public function estrazioneAtecoDaVisura($visura,$dataSources){
		$atecoVisura = array();
		if (strcmp($dataSources, "Parix") == 0){
			//print_r($visura['visura-parix']['Categorie']);
			$branchAteco = $visura['visura-parix']['Categorie']['c-attivita'];
			if (is_array($branchAteco)){
				foreach ($branchAteco as $key => $value){
					//if (strcmp($key, 'c-attivita') == 0){
						$atecoVisura[trim($value)] = trim($value);
					//}
				}
			} else {
				$atecoVisura[trim($branchAteco)] = trim($branchAteco);
			}
			
			
		}else {
			$info_categorie=array();
			$descrizione_attivita=$visura['dati']['blocchi-impresa']['info-attivita']['attivita-esercitata'];
			$array_categorie=$visura['dati']['blocchi-impresa']['info-attivita']['classificazioni-ateco'];
			$iCategorie=0;

			if (self::PRINT_VISURA_ATECO){
				print "<pre><h4>Array categorie</h4>";
				print_r ($array_categorie);
				print "</pre>";
			}

			if($array_categorie['classificazione-ateco']['@attributes']) {
				if(!$this->in_array_r($array_categorie['classificazione-ateco']['@attributes']['c-attivita'], $info_categorie)) {
					$info_categorie[$iCategorie]['c-attivita']=$array_categorie['classificazione-ateco']['@attributes']['c-attivita'];
					$info_categorie[$iCategorie]['attivita']=$array_categorie['classificazione-ateco']['@attributes']['attivita'];
					$iCategorie++;
				}
			}else {

				foreach ($array_categorie['classificazione-ateco'] as $key => $value) {
					if(!$this->in_array_r($value['@attributes']['c-attivita'], $info_categorie)) {
						$info_categorie[$iCategorie]['c-attivita']=$value['@attributes']['c-attivita'];
						$info_categorie[$iCategorie]['attivita']=$value['@attributes']['attivita'];
						$iCategorie++;
					}
				}
			}

			$array_loc=$visura['dati']['blocchi-impresa']['localizzazioni'];
			if (self::PRINT_VISURA_ATECO){
				print "<pre><h4>Array locazioni</h4>";
				print_r ($array_loc);
				print "</pre>";
			}
			$value=$array_loc['localizzazione'];
			if(count($array_loc)>0) {
				if(isSet($value['classificazione-ateco']['@attributes'])) {
					foreach ($value['classificazione-ateco'] as $key1 => $value1) {
						if(count($value['classificazione-ateco'])>1) {
							if(!$this->in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
								$info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
								$info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
								$iCategorie++;
							}
						}else {
							if(!$this->in_array_r($value1['c-attivita'], $info_categorie)) {
								$info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
								$info_categorie[$iCategorie]['attivita']=$value1['attivita'];
								$iCategorie++;
							}
						}
					}
				}
				if(isSet($value['classificazioni-ateco']['classificazione-ateco'])) {
						
					foreach ($value['classificazioni-ateco']['classificazione-ateco'] as $key1 => $value1) {

						if(count($value['classificazioni-ateco']['classificazione-ateco'])>1) {
							if(!$this->in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
								$info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
								$info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
								$iCategorie++;
							}
						}else {
							if(!$this->in_array_r($value1['c-attivita'], $info_categorie)) {
								$info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
								$info_categorie[$iCategorie]['attivita']=$value1['attivita'];
								$iCategorie++;
							}
						}
					}
				}
				foreach ($array_loc['localizzazione'] as $key => $value) {

					if(isSet($value['classificazione-ateco']['@attributes'])) {
						foreach ($value['classificazione-ateco'] as $key1 => $value1) {
							if(count($value['classificazione-ateco'])>1) {
								if(!$this->in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
									$info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
									$info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
									$iCategorie++;
								}
							}else {
								if(!$this->in_array_r($value1['c-attivita'], $info_categorie)) {
									$info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
									$info_categorie[$iCategorie]['attivita']=$value1['attivita'];
									$iCategorie++;
								}
							}
						}
					}
					if(isSet($value['classificazioni-ateco']['classificazione-ateco'])) {
						foreach ($value['classificazioni-ateco']['classificazione-ateco'] as $key1 => $value1) {

							if(count($value['classificazioni-ateco']['classificazione-ateco'])>1) {
								if(!$this->in_array_r($value1['@attributes']['c-attivita'], $info_categorie)) {
									$info_categorie[$iCategorie]['c-attivita']=$value1['@attributes']['c-attivita'];
									$info_categorie[$iCategorie]['attivita']=$value1['@attributes']['attivita'];
									$iCategorie++;
								}
							}else {
								if(!$this->in_array_r($value1['c-attivita'], $info_categorie)) {
									$info_categorie[$iCategorie]['c-attivita']=$value1['c-attivita'];
									$info_categorie[$iCategorie]['attivita']=$value1['attivita'];
									$iCategorie++;
								}
							}
						}
					}
				}

			}
				
			if (self::PRINT_VISURA_ATECO){
				print "<pre><h4>INFO Categorie</h4>";
				print_r ($info_categorie);
				print "</pre>";
			}
			
			foreach ($info_categorie as $key => $categoria){
				$atecoVisura[trim($categoria['c-attivita'])]=array();
				$atecoVisura[trim($categoria['c-attivita'])]['c-attivita'] = trim($categoria['c-attivita']);
				$atecoVisura[trim($categoria['c-attivita'])]['attivita'] = $categoria['attivita'];
			}

			if (self::PRINT_VISURA_ATECO){
				print "<pre><h4>ATECO da VISURA</h4>";
				print_r ($atecoVisura);
				print "</pre>";
			}
		}
		return $atecoVisura;
		
	}
	
	/**
	 * Funzione che estrae i codici ateco dalla visura
	 * @param array $visura
	 * @return array $atecoVisura
	 */
	public function estrazioneAtecoDaDB($impresaId){
		
		//$sqlAtecoImpresa ="select * from EXPO_TJ_Imprese_Ateco where IdImpresa='$impresaId' GROUP BY IdAteco ";
		$sqlAtecoImpresa = "select AtecoImpresa.Id as IdAssociazione, AtecoImpresa.IdImpresa,AtecoImpresa.IdAteco,Ateco.CodiceAteco,Ateco.IsPresente from EXPO_TJ_Imprese_Ateco as AtecoImpresa join EXPO_T_Ateco as Ateco on AtecoImpresa.IdAteco=Ateco.Id where IdImpresa='$impresaId' GROUP BY IdAteco";

		$allAteco= $this->db->GetRows($sqlAtecoImpresa);

		$atecoImpresa = array();
		$duplicato = 0;
		foreach ($allAteco as $ateco ){

			$singoloAteco = array ();
			$singoloAteco['IdAssociazione'] = $ateco['IdAssociazione'];
			$singoloAteco['IdImpresa'] = $ateco['IdImpresa'];
			$singoloAteco['IdAteco'] = $ateco['IdAteco'];
			$singoloAteco['CodiceAteco'] = trim($ateco['CodiceAteco']);
			$singoloAteco['IsPresente'] = $ateco['IsPresente'];

			if ($atecoImpresa[trim($ateco['CodiceAteco'])]){
				$duplicato++;
				$codiceAtecoDuplicato = trim($ateco['CodiceAteco'])."_$duplicato";
				$atecoImpresa[$codiceAtecoDuplicato] = $singoloAteco;
			}else{
			
				$atecoImpresa[trim($ateco['CodiceAteco'])] = $singoloAteco;
			}
		}
		
		return $atecoImpresa;  
	}
	
	
	/**
	 * Funzione che controlla la consistenza tra gli ateco della visura e quelli presenti su DB.
	 * Se c'� consistenza ritorno TRUE.
	 * Se inconistente ritorna due array: $atecoMancanti e $atecoEccedenti;
	 * 
	 * @param $atecoVisura
	 * @param $atecoDB
	 * 
	 * @return boolean || $atecoMancanti , $atecoEccedenti
	 */
	public function consistenzaAteco($atecoVisura,$atecoDB){
		$atecoMancanti = array();
		$atecoEccedenti = array();
		print "<h3>Ateco Mancanti</h3>";
		foreach ($atecoVisura as $keyVisura => $valueVisura){
			if (!$atecoDB[$keyVisura]['CodiceAteco']){
				print "<h4 style=\"color: red\">Ateco mancante: $keyVisura</h4>";
				$atecoMancanti[] = $keyVisura;
				$this->verificaPresenzaAtecoDB($keyVisura,true);
			}
		}

		print "<h3>Ateco Eccedeneti</h3>";
		foreach ($atecoDB as $keyDB => $valueDB){
				
			if (!$atecoVisura[$keyDB]){
				print "<h4 style=\"color: blue\">Ateco eccedenete: $keyDB</h4>";
				$atecoEccedenti[] = $keyDB;
			}
		}

		if  ( (count($atecoEccedenti) == 0 ) && (count ($atecoMancanti) ==0 ) ) {
			print "<h2 style=\"color: green\">Consistenza Ateco Visura e Ateco DB</h2>";
			return "true";
		}
		print "<h2 style=\"color: red\">Inconsistenza Ateco Visura e Ateco DB</h2>";
		
		return $ateco = array ("Mancanti" => $atecoMancanti, "Eccedenti" =>  $atecoEccedenti); 
	}
	
	
	/**
	 * Funzione che estrae le categorie associate a un codice ateco
	 */
	public function estraiCategorieFromCodiceAteco($ateco, $print =false){
		$categorie = array();
		
		$sqlIdCategorieCodiceAteco ="Select Id_Categoria From EXPO_TLK_Categorie Where Descrizione = '$ateco'";
		$idCategorieCodiceAteco = $this->db->GetRows($sqlIdCategorieCodiceAteco);
		
		foreach ($idCategorieCodiceAteco as $key => $idCategoriaCodiceAteco){
			
			$idEsploso = explode(".", $idCategoriaCodiceAteco['Id_Categoria']);
			$id_categoria = "$idEsploso[0].$idEsploso[1].$idEsploso[2]";
			//print "<h3>$id_categoria</h3>";
			$sqlIdCategoria ="Select Id, DescEstesa From EXPO_TLK_Categorie Where Id_Categoria = '$id_categoria'";
			$idCategoria = $this->db->GetRow($sqlIdCategoria);
			if ($id_categoria){
				$categoria= array();
				$categoria['Id'] = $idCategoria['Id'];
				$categoria['DescEstesa'] = $idCategoria['DescEstesa'];
				$categoria['Ateco'] = $ateco;
					
				$categorie[$idCategoria['Id']] = $categoria;
			}
		}
		if ($print){
			print "<pre>CATEGORIE per $ateco";
			print_r($categorie);
			print "</pre>";
			print "<br>".count($categorie);
		}
		return $categorie;
	}
	
	
	/**
	 * Funzione che estrae le categorie associate a un codice ateco
	 */
	public function estraiCategorieFromIdImpresa($impresaId){
		$categorie = array();
	
		$sqlIdCategorieImpresa ="SELECT * FROM EXPO_TJ_Imprese_Categorie WHERE IdImpresa = $impresaId ORDER BY IdCategoria,Id";
		$categorieImpresa = $this->db->GetRows($sqlIdCategorieImpresa);
		$duplicato = 0;
		foreach ($categorieImpresa as $key => $valueCategoria){
			
			if ($valueCategoria){
				$categoria= array();
				$categoria['IdAssociazione'] = $valueCategoria['Id'];
				$categoria['IdCategoria'] = $valueCategoria['IdCategoria'];
				$categoria['IdImpresa'] = $valueCategoria['IdImpresa'];
				$categoria['IsIscritta'] = $valueCategoria['IsIscritta'];
			}		
			if (is_array($categorie[$valueCategoria['IdCategoria']])){
				$duplicato++;
				$idCat = $valueCategoria['IdCategoria']."_$duplicato";
				$categorie[$idCat] = $categoria;
			}else{
				
			$categorie[$valueCategoria['IdCategoria']] = $categoria;
			}
		}
	
		if (!$categorie){
			print "<h3 style=\"color: red\">Nessuna Categoria associata all'impresa</h3>";
		}
		
		if (self::PRINT_CATEGORIE_DA_DB){
			print "<pre>";
			print_r ($categorie);
			print "</pre>";
		}
		
		return $categorie;
	}
	
	
	/**
	 * Funzione che estra le categorie associate ai codidi ateco della visura
	 * @param $atecoVisura
	 * 
	 * @return multitype:Ambigous <unknown, Ambigous <>>
	 */
	public function associazioneCategorieDaAtecoVisura($atecoVisura){
		
		$categorieByAtecoVisura = array();
		foreach ($atecoVisura as $ateco => $value){
			$categoria = $this->estraiCategorieFromCodiceAteco($ateco);
				
			foreach ($categoria as $key =>$valueCategoria){
				$categorieByAtecoVisura[$key] = $valueCategoria['DescEstesa'];
			}
		}
		if (self::PRINT_CATEGORIE_DA_VISURA){
			print "<pre>Categorie da visura<br>";
			print_r($categorieByAtecoVisura);
			print "</pre>";
		}
		return $categorieByAtecoVisura;
		
	}
	
	
	/**
	 * Funzione che controlla la consistenza Tra le imprese e le categorie
	 */
	public function consistenzaCategorie($impresaId,$categorieDaVisura){
		
		$categorieMancanti = array();
		$categorieEccedenti = array();
				
		//associazioni imprese categorie presenti nel db
		$categorieImpresaDB = $this->estraiCategorieFromIdImpresa($impresaId);
		
		/*
		print "<pre><h3>Categorie associate </h3>";
		print_r($categorieDaVisura);
		print "</pre>";
		*/
		
		
		//print "<h3>controllo categoria da ateco visure</h3>";
		foreach ($categorieDaVisura as $keyCategoriaVisura => $valueCategoriaVisura){
			
			if (!$categorieImpresaDB[$keyCategoriaVisura]['IdCategoria']){
				print "<h4 style=\"color: red\">Categoria mancante: $keyCategoriaVisura - $valueCategoriaVisura</h4>";
				$categorieMancanti[$keyCategoriaVisura] = $keyCategoriaVisura;
			}
		}
		
		if  (count($categorieMancanti) > 0 ) {
			print "<h2 style=\"color: red\">Categorie Mancanti: ". count ($categorieMancanti)."</h2>";
		}
		
		//print "<h3>controllo categoria eccedenti</h3>";
		foreach ($categorieImpresaDB as $keyCategoriaDB => $valueCategoriaImpresaDB){
			/*
			print "<pre>keyDB: $keyCategoriaDB<br>";
			print_r($valueCategoriaImpresaDB);
			print "</pre>";
			*/
			
			if (!$categorieDaVisura[$keyCategoriaDB]){
				$categoriaEccedente = array();
				$categoriaEccedente['IdCategoria'] = $valueCategoriaImpresaDB ['IdCategoria'];
				$categoriaEccedente['IdAssociazione'] = $valueCategoriaImpresaDB['IdAssociazione'];
				$categoriaEccedente['IsIscritta'] = $valueCategoriaImpresaDB['IsIscritta'];
				
				if (self::PRINT_CATEGORIA_ECCEDENTE){
					print "<pre><br>";
					print_r($categoriaEccedente);
					print "</pre>";
				}
				
				print "<h4 style=\"color: blue\">Categoria Eccedente: $keyCategoriaDB ----- IdAssocizione: ".$valueCategoriaImpresaDB['IdAssociazione']." ---- IsIscritta: ".$valueCategoriaImpresaDB['IsIscritta']." </h4>";
				$categorieEccedenti[$keyCategoriaDB] = $categoriaEccedente;
			}
		}
		if  (count($categorieEccedenti) > 0 ) {
			print "<h2 style=\"color: blue\">Categorie Eccedenti: ".count($categorieEccedenti)."</h2>";
		}
		
		if  ( (count($categorieEccedenti) == 0 ) && (count ($categorieMancanti) ==0 ) ) {
			print "<h2 style=\"color: green\">Consistenza Associazioni Categorie Imprese</h2>";
			return "true";
		}
		print "<h2 style=\"color: red\">Inconsistenza Associazioni Categorie Imprese</h2>";
		
		
		
		if (self::PRINT_CATEGORIE_ECCEDENTI){
			print "<pre>CATEGORIE ECCEDENTI<br>";
			print_r($categorieEccedenti);
			print "</pre>";
		}
		

		return $categorie = array ("Mancanti" => $categorieMancanti, "Eccedenti" =>  $categorieEccedenti);
	
	}
	
	public function estrazioneProdottiImpresa($impresaId){
		$sqlProdotti = "Select PRODOTTI.Id as IdProdotto,PRODOTTI_CATEGORIE.Id as IdAssociazione,PRODOTTI.IdImpresa,PRODOTTI_CATEGORIE.IdCategoria,PRODOTTI_CATEGORIE.IsIscritta
						From EXPO_T_Prodotti AS PRODOTTI join EXPO_TJ_Prodotti_Categorie AS PRODOTTI_CATEGORIE ON PRODOTTI.Id= PRODOTTI_CATEGORIE.IdProdotto
						Where IdImpresa = '$impresaId'";
		$prodottoDaQuery = $this->db->GetRows($sqlProdotti);
		
		$prodotti = array();
		foreach ($prodottoDaQuery as $key => $valueProdotto){
			
			$prodotto = array();
			$prodotto['IdProdotto']= $valueProdotto['IdProdotto'];
			$prodotto['IdAssociazione']= $valueProdotto['IdAssociazione'];
			$prodotto['IdImpresa']= $valueProdotto['IdImpresa'];
			$prodotto['IdCategoria']= $valueProdotto['IdCategoria'];
			$prodotto['IsIscritta']= $valueProdotto['IsIscritta'];
			
			$prodotti[$prodotto['IdProdotto']] = $prodotto;
			
		}
		
		
		if (self::PRINT_PRODOTTI_IMPRESA){
			print "<pre><h4>Prodotti</h4>";
			print_r ($prodotti);
			print "</pre>";
		}
		
		return $prodotti;
	}
	
	
	/**
	 * Funzione che controlla la consistenza dei prodotti
	 * 
	 * @param unknown $categorieDaVisura
	 * @param unknown $prodottiImpresa
	 * 
	 * @return array consistenza
	 */
	public function consistenzaProdotti($categorieDaVisura,$prodottiImpresa){
		$prodottiConsistenti = array();
		$prodottiInconsistenti = array();
		
		
		foreach ($prodottiImpresa as $idProdotto => $valueProdotto){
			if ($categorieDaVisura[$valueProdotto['IdCategoria']]){
				print "<h4 style=\"color: green\">Prodotto consistente: $idProdotto - Categoria: ".$valueProdotto['IdCategoria']." </h4>";
				$prodottiConsistenti[] = $idProdotto;
			}else{
				print "<h4 style=\"color: red\">Prodotto Inconsistente</h4>";
				$prodottiInconsistenti[] = $idProdotto;
			}
		}

		return array("Consistenti" => $prodottiConsistenti,"Inconsistenti" => $prodottiInconsistenti);
	}
	
	
	/**
	 * Funzione che inserisce le associazioni Ateco Impresa
	 * 
	 * @param $ateco
	 * @param $idImpresa
	 * @param $ESEGUI_INSERIMENTO_ASSOCIAZIONE_ATECO_IMPRESA
	 * @param $PRINT_SQL
	 * @return bool
	 */
	public function inserisciAssociazioneAtecoImpresaMancante($ateco,$idImpresa,$ESEGUI_INSERIMENTO_ASSOCIAZIONE_ATECO_IMPRESA = false,$PRINT_SQL = true){

		//cerco se esiste gia il codice ateco
		
		$sqlAteco="SELECT * from EXPO_T_Ateco where CodiceAteco='".$ateco."'";
		print $sqlAteco."<br>";
		$idAteco = $this->db->GetRow($sqlAteco,'Id');
		print $idAteco."<br>";
		if($idAteco > 0){
			

			//verifico se esiste gia la relazione tra ateco e impresa
			$qry="SELECT * from EXPO_TJ_Imprese_Ateco where IdImpresa='".$idImpresa."' and IdAteco='".$idAteco."'";
			if($this->db->NumRows($qry) == 0){
				/*$ultimo="SELECT MAX(Id) as massimo FROM EXPO_TJ_Imprese_Ateco ";
				$prossimo=$this->db->GetRow($ultimo, "massimo");
				$prossimo++;
				*/
				// inserisco relazione
				$insert="INSERT INTO EXPO_TJ_Imprese_Ateco (Id,IdImpresa,IdAteco)
						VALUES((SELECT @massimo:= COALESCE(MAX(TJA.Id),0)+1 as massimo FROM EXPO_TJ_Imprese_Ateco AS TJA),'".$idImpresa."','".$idAteco."')";
				if ($PRINT_SQL){
					print $insert."<br>";
				}
				if ($ESEGUI_INSERIMENTO_ASSOCIAZIONE_ATECO_IMPRESA){
					if ($this->db->Query($insert)){
						return true;
					}
				}
					
			}
		}

		return false;
	}

	/**
	 * Funzione che inserirsce i codici ateco mancanti nella tabella EXPO_T_Ateco
	 * @param $ateco
	 * @param boolean $ESEGUI_INSERIMENTO_ATECO
	 * @param boolean $printSql
	 * 
	 * return IdAteco
	 * 
	 */
	public function inserisciAtecoMancante($ateco,$ESEGUI_INSERIMENTO_ATECO = false,$PRINT_SQL = true){

		$idAteco = false;
		$cattivita=$ateco['c-attivita'];
		$attivita=$ateco['attivita'];
		
		
		//cerco se esiste gia il codice ateco
		$sqlAteco="SELECT * from EXPO_T_Ateco where CodiceAteco='".$cattivita."'";
		
		if($this->db->NumRows($sqlAteco)==0){
			
			$found = 'N';
			$sqlOccorrenze = "Select count(Id) AS Occorrenze FROM EXPO_TLK_Categorie WHERE Descrizione ='$cattivita'";
			$occorrenze = $this->db->GetRow($sqlOccorrenze,'Occorrenze');
			
			
			if ($occorrenze>0){
				$found = 'Y';
			}
			
			// se non esiste l'aggiungo
			/*$ultimo="SELECT @massimo:= COALESCE(MAX(TA.Id),0)+1 as massimo from EXPO_T_Ateco AS TA ";

			$prossimo=$this->db->GetRow($ultimo, "massimo");
			$prossimo++;
			$prossimoateco=$prossimo;*/
			$insert="INSERT INTO EXPO_T_Ateco (Id,CodiceAteco,Descrizione,IsPresente)
					VALUES((SELECT @massimo:= COALESCE(MAX(TA.Id),0)+1 as massimo from EXPO_T_Ateco AS TA),'".$cattivita."','".mysql_escape_string($attivita)."','".$found."')";
			if ($PRINT_SQL){
					print $insert."<br>";
			}
			if ($ESEGUI_INSERIMENTO_ATECO){
				if ($this->db->Query($insert)){
					$idAteco = $this->db->GetRow("SELECT @massimo as massimo","massimo",null,"id ateco inserito recupero dati ln 1038");
				}else{
					$idAteco = -1;
				}
			}
			
		}
		return $idAteco;
	}

	
	
	/**
	 * Funzione che inserisce associazioni Impresa Categoria
	 * 
	 * @param $idCategoria
	 * @param $idImpresa
	 * @param $ESEGUI_INSERIMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA
	 * @param $PRINT_SQL
	 * @return boolean
	 */
	public function inserisciAssociazioneImpresaCategoriaMancante($idCategoria,$idImpresa,$ESEGUI_INSERIMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA = false,$PRINT_SQL = true){
		


		//verifico se esiste gia la relazione tra ateco e impresa
		$qry="SELECT * from EXPO_TJ_Imprese_Categorie where IdImpresa='".$idImpresa."' and IdCategoria='".$idCategoria."'";
		if($this->db->NumRows($qry) == 0){
			/*$ultimo="SELECT @massimo:= COALESCE(MAX(TJC.Id),0)+1 as massimo FROM EXPO_TJ_Imprese_Categorie AS TJC";
			$prossimo=$this->db->GetRow($ultimo, "massimo");
			$prossimo++;
			*/
			// inserisco relazione
			$insert="INSERT INTO EXPO_TJ_Imprese_Categorie (Id,IdImpresa,IdCategoria,IsIscritta)
					VALUES((SELECT @massimo:= COALESCE(MAX(TJC.Id),0)+1 as massimo FROM EXPO_TJ_Imprese_Categorie AS TJC),'$idImpresa','$idCategoria','N')";
			if ($PRINT_SQL){
				print $insert."<br>";
			}
			if ($ESEGUI_INSERIMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA){
				if ($this->db->Query($insert)){
					return true;
				}
			}
				
		}

		return false;

		
	} 

	
	/**
	 * Funzione che inserisce associazioni Impresa Categoria
	 *
	 * @param $idCategoria
	 * @param $idImpresa
	 * @param $ESEGUI_CANCELLAMENTO_ASSOCIAZIONE_IMPRESA
	 * @param $PRINT_SQL
	 * @return boolean
	 */
	public function cancellaAssociazioneImpresaCategoriaEccedente($idAssociazione,$idCategoria,$idImpresa,$ESEGUI_CANCELLAMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA = false,$PRINT_SQL = true){
	
	
	
		//verifico se esiste gia la relazione tra ateco e impresa
		$qry="SELECT * from EXPO_TJ_Imprese_Categorie where Id ='$idAssociazione' and IdImpresa='$idImpresa' and IdCategoria= '$idCategoria'";
		print $qry."<br>";
		if($this->db->NumRows($qry) != 0){
			
	
			// inserisco relazione
			$delete="DELETE FROM EXPO_TJ_Imprese_Categorie WHERE Id ='$idAssociazione'";
			if ($PRINT_SQL){
				print $delete."<br>";
			}
			if ($ESEGUI_CANCELLAMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA){
				if ($this->db->Query($delete)){
					return true;
				}
			}
	
		}
	
		return false;
	
	
	}
	
	
	/**
	 * Funzione che inserisce le associazioni Ateco Impresa
	 *
	 * @param $ateco
	 * @param $idImpresa
	 * @param $ESEGUI_CANCELLAZIONE_ASSOCIAZIONE_ATECO_IMPRESA
	 * @param $PRINT_SQL
	 * @return bool
	 */
	public function cancellaAssociazioneAtecoImpresaEccedente($ateco,$idImpresa,$ESEGUI_CANCELLAZIONE_ASSOCIAZIONE_ATECO_IMPRESA = false, $PRINT_SQL = true){
	
		//cerco se esiste gia il codice ateco
	
		$sqlAteco="SELECT * from EXPO_T_Ateco where CodiceAteco='".$ateco."'";
		if ($PRINT_SQL){
			print $sqlAteco."<br>";
		}
		$idAteco = $this->db->GetRow($sqlAteco,'Id');
		if ($PRINT_SQL){
			print $idAteco."<br>";
		}
		if($idAteco > 0){
				
	
			//verifico se esiste gia la relazione tra ateco e impresa
			$qry="SELECT * from EXPO_TJ_Imprese_Ateco where IdImpresa='$idImpresa' and IdAteco='$idAteco'";
			if ($PRINT_SQL){
				PRINT $qry."<BR>";
			}
			if($this->db->NumRows($qry) != 0){
				
	
				// inserisco relazione
				$delete="DELETE FROM EXPO_TJ_Imprese_Ateco where IdImpresa='$idImpresa' and IdAteco='$idAteco'";
				if ($PRINT_SQL){
					print $delete."<br>";
				}
				if ($ESEGUI_CANCELLAZIONE_ASSOCIAZIONE_ATECO_IMPRESA){
					if ($this->db->Query($delete)){
						return true;
					}
				}
					
			}
		}
	
		return false;
	}
	
	
	public function scegliVisura($visuraParix,$visuraTelemaco) {
		
		if ($visuraParix['Visura'] && $visuraTelemaco['Visura']){
			
			if (strcmp($visuraParix['DataChiamata'], $visuraTelemaco['DataChiamata']) > 0 ){
				$this->countParix++;
				return $visuraParix;
			}else{
				$this->countTelemaco++;
				return $visuraTelemaco;
			}
			
		}else if ($visuraParix['Visura'] ){
			$this->countParix++;
			return $visuraParix;
		}
		else if($visuraTelemaco['Visura']){
			$this->countTelemaco++;
			return $visuraTelemaco;
			
		}
			
		
	}
	
	
	
	public function getCountParix(){
		return $this->countParix;
	}
	
	public function getCountTelemaco(){
		return $this->countTelemaco;
	}
	
	
	public function estraiOrdini(){
	
	
		$sqlOrdini = "SELECT Id,NomeOrdineProfessionale as Ordine FROM EXPO_Tlk_OrdiniProfessionali WHERE Id > 0";
		print$resultOrdini = $this->db->GetRows($sqlOrdini, null, null, "esrazione EXPO_Tlk_Ordini");
	
		return $resultOrdini;
	}
	
}  




//$DEBUG = true;

//LIMITAZIONE QUERY IMPRESE
$LIMIT_IMPRESE = 0;

//$ID_IMPRESA = '545 or T.Id =  582 or T.Id =  608 or T.Id =  757 or T.Id =  869 or T.Id =  870 or T.Id =  909 or T.Id =  947 or T.Id =  1004';
$ID_IMPRESA=419;

//COSTANTE PER LA STAMPA DEL NUMERO REA
$PRINT_NREA = TRUE;


//COSTANTE PER LA STAMPA DELL'ARRAY DELL'IMPRESA
$PRINT_IMPRESA = false;

//CONSTANTI RELATIVE ALL'UPDATE DELLA PARTITA IVA E DEL NUMERO REA;
$UPDATE_IVA_NREA = FALSE;
$EXEC_UPDATE_IVA_NREA = false;

//COSTANTE PER LA STAMPA DEGLI ATECO
$PRINT_ATECO_VISURA = false;
$PRINT_ATECO_DB = false;

//COSTANTE PER LA STAMPA DELLE IMPRESE CONSISTENTI PRIMA DI ESEGUIRE MODIFICHE
$UPDATE_ERANO_CONSISTENTI = true;

$PRINT_ERANO_CONSISTENTI_ARRAY = false;

$ATECO_PRESENTI = false;

$PRINT_CONSISTENZA_ATECO = false;
$PRINT_CONSISTENZA_CATEGORIE = false;


$PRINT_PRODOTTI_IMPRESA = false;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CONSTANTI DI INSERIMENTO 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

$INSERIRE_ATECO_MANCANTI = false;
$INSERIRE_ASSOCIAZIONI_ATECO_IMPRESE_MANCANTI = false;
$INSERIRE_ASSOCIAZIONI_IMPRESE_CATEGORIE_MANCANTI = false;

$ESEGUI_CANCELLAMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA = true;
$ESEGUI_CANCELLAMENTO_ASSOCIAZIONI_ATECO_IMPRESE_MANCANTI = true;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Fine CONSTANTI DI INSERIMENTO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


$dbImprese = new DataBase();


$recupero = new recuperoDati();

$impreseSchedePersona = $recupero->recuperoImpreseDaSchedePersone();

$totaleImprese = 0;

$totaleConfronti = 0;
$confronti = array ("Positivi" => 0 ,"Negativi"  => 0 );
$errorConfornti = array();

$professionisti = 0;

$totalUpdate = 0;
$updateSuccess = 0;
$updateError = 0;
$errorImpresa = array();


$associazioneAtecoDaInserire = 0;
$associazioneAtecoDaEliminare = 0;

$categorieDaInserire = 0;
$categorieDaEliminare = 0;


$confrontiProdotti = array ("Positivi" => 0 ,"Negativi"  => 0 );

$eranoConsistentiAteco = array ("Positivi" => 0 ,"Negativi"  => 0 );

$errorEranoConsistentiAteco = array();
$errorEranoInconsistentiAteco = array();

$eranoConsistentiCategorie = array ("Positivi" => 0 ,"Negativi"  => 0 );

$errorEranoConsistentiCategorie = array();
$errorEranoInconsistentiCategorie = array();


$atecoDaInserire = array(); 
$atecoInseriti = array();
$atecoPresente = array();

$categorieInserite = 0;
$categorieCancellate = 0;

$associazioniAtecoInseriti = 0;
$associazioniAtecoEliminate = 0;

$categorieInconsistentiIscrizione = array("True" => 0 ,"False" => 0 );

$elencoProdottiInconsinstenti = array();

$categorieInconsistentiIscritte = array();

$impreseCategorieInconsistentiIscritte = array();

//print "<pre>";
//print_r($elencoOrdini = $recupero->estraiOrdini());
//print "</pre>";

//estrazione di tutte le imprese da DataBase

if ( $LIMIT_IMPRESE > 0 ){
	$sqlTutteimprese="select T.Id AS Id,RagioneSociale,CodiceFiscale,PartitaIva,Pr,NRea,StatoRegistrazione,cciaa,Id_OrdineProfessionale,IsStudio from EXPO_T_Imprese AS T join EXPO_TJ_Imprese_OrdiniProfessionali AS TJ  on T.Id = TJ.Id_Impresa Where T.Id > 27 order by T.Id limit 0,$LIMIT_IMPRESE";
}else
	if ( $ID_IMPRESA > 0 ){

	$sqlTutteimprese="select T.Id AS Id,RagioneSociale,CodiceFiscale,PartitaIva,Pr,NRea,StatoRegistrazione,cciaa,Id_OrdineProfessionale,IsStudio from EXPO_T_Imprese AS T join EXPO_TJ_Imprese_OrdiniProfessionali AS TJ  on T.Id = TJ.Id_Impresa Where T.Id = $ID_IMPRESA order by T.Id";

}else{

	$sqlTutteimprese="select T.Id AS Id,RagioneSociale,CodiceFiscale,PartitaIva,Pr,NRea,StatoRegistrazione,cciaa,Id_OrdineProfessionale,IsStudio from EXPO_T_Imprese AS T join EXPO_TJ_Imprese_OrdiniProfessionali AS TJ  on T.Id = TJ.Id_Impresa Where T.Id > 27 order by T.Id";
}


$tutteImprese = $dbImprese->GetRows($sqlTutteimprese);


foreach ($tutteImprese as $impresa){
	//$totaleImprese++;
	
	$impresaCCIAA = $impresa['Pr'];
	if ($impresa['cciaa']){
		
		$impresaCCIAA = $impresa['cciaa'];
	
	}
	
	print "<h2>".$impresa['RagioneSociale']."</h2>";
	print "<span><b>Id: </b>".$impresa['Id']."</span><br>";
	print "<span><b>Codice Fiscale: </b>".$impresa['CodiceFiscale']."</span><br>";
	print "<span><b>Provincia: </b>".$impresa['Pr']."</span><br>";
	print "<span><b>CCIAA: </b>".$impresaCCIAA."</span><br>";
	print "<span><b>StatoRegistrazione: </b>".$impresa['StatoRegistrazione']."</span><br>";
	print "<span><b>Dipendenti: </b>".$impresa['NumeroDipendenti']."</span><br>";
	
	
	if ($PRINT_IMPRESA){
		print "<pre>";
		print_r($impresa);
		print "</pre>";
	}
	
	if ($impresa['NRea']){
		
		$NreaImpresa = $impresa['NRea'];
		
	}else{
		
		$NreaImpresa = $recupero->associazioneNrea($impresa['Id'], $impresa['CodiceFiscale'], $impreseSchedePersona);
	}
	
	if ($PRINT_NREA){
		print "<pre><h3>Nrea:</h3> ";
		print_r($NreaImpresa);
		print "</pre>";
	}
	
	
	if ($impresa['Id_OrdineProfessionale'] == 0){
		$totaleImprese++;
		$totaleConfronti++;
	$visuraParix = $recupero->cercaVisuraParix($impresaCCIAA, $NreaImpresa);
	
	$visuraTelemaco = $recupero->cercaVisura($impresaCCIAA, $NreaImpresa,$impresa['CodiceFiscale']);
	//$visura = $visuraTelemaco['Visura']; 
	 
	$visuraCache = $recupero->scegliVisura($visuraParix, $visuraTelemaco);
	$visura = $visuraCache['Visura'];
	
	$dataSources = $visuraCache['DataSurces'];
	//$recupero->stampaBoolean($visuraParix['Visura']);
	
	
	//VISURA TROVATA
	if ($visura){
		$confronti["Positivi"]++;
		
		/* --- start UPDATE DELLA PARTITA IVA E NREA SE $UPDATE_IVA_NREA = TRUE----*/
		/*
		if ($UPDATE_IVA_NREA){
			$partitaIva = $visura['dati']['blocchi-impresa']['info-sede']['partita-iva'];
			$reaVisura = $visura['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['n-rea'];
			$codiceFiscaleVisura = $visura['dati']['blocchi-impresa']['dati-identificativi']['@attributes']['c-fiscale'];

			print "Partita Iva: $partitaIva<br>";
			print "Nrea: $reaVisura<br><br>";
			$impresaId = $impresa['Id'];
			$sqlUpdateImpresa="UPDATE EXPO_T_Imprese SET PartitaIva='$partitaIva',Nrea='$reaVisura' WHERE Id = $impresaId and CodiceFiscale='$codiceFiscaleVisura'";
			print  $sqlUpdateImpresa."<br><br>";
			$totalUpdate++;
			if ($EXEC_UPDATE_IVA_NREA){
				if ($dbImprese->Query($sqlUpdateImpresa)){
					print "<span style=\"color:green\">Update con Successo</span><br>";
					$updateSuccess++;
				}else{
					print "<span style=\"color:red\">Update Negativo</span><br>";
					$updateError++;
					$errorImpresa[$impresa['Id']]= $impresa['CodiceFiscale'];
				}

			}
		}
		*/
		/* --- end UPDATE DELLA PARTITA IVA E NREA SE $UPDATE_IVA_NREA = TRUE ----*/
		
		
		print "-------------------------------------------<br>";
		$atecoVisura = $recupero->estrazioneAtecoDaVisura($visura,$dataSources);
		if ($PRINT_ATECO_VISURA){
			print "<pre><h3>ATECO DA VISURA</h3> ";
			print_r($atecoVisura);
			print "</pre>";
		}
		
		$categorieDaVisura = $recupero->associazioneCategorieDaAtecoVisura($atecoVisura);
		
		
		$atecoDB = $recupero->estrazioneAtecoDaDB($impresa['Id']);
		if ($PRINT_ATECO_DB){
			print "<pre><h3>ATECO DA DB</h3> ";
			print_r($atecoDB);
			print "</pre>";
		}
		
		$consistenzaAteco = $recupero->consistenzaAteco($atecoVisura, $atecoDB);
		
		if ($PRINT_CONSISTENZA_ATECO){
			print "<pre>";
			print_r($consistenzaAteco);
			print "</pre>";
		}
		
		foreach ($atecoVisura as $key => $value){
			if (!$recupero->verificaPresenzaAtecoDB($key)){
				$atecoDaInserire[$key] = $key;
				$idInserito = $recupero->inserisciAtecoMancante($value,$INSERIRE_ATECO_MANCANTI);
				if ($idInserito){
					$atecoInseriti[$key] = $idInserito;
				}
			}else if ($ATECO_PRESENTI){
				$atecoPresente[$key] = $key;
			}
		}
		print "-------------------------------------------<br>";
		if (is_array($consistenzaAteco)){
			$eranoConsistentiAteco["Negativi"]++;
			$errorEranoInconsistentiAteco[$impresa['Id']]= $impresa['CodiceFiscale'];
			
			$associazioneAtecoDaInserire += count($consistenzaAteco['Mancanti']);
			$associazioneAtecoDaEliminare += count($consistenzaAteco['Eccedenti']);
			
			
			foreach ($consistenzaAteco["Mancanti"] as $key => $value){
								
					if ($recupero->inserisciAssociazioneAtecoImpresaMancante($value, $impresa['Id'],$INSERIRE_ASSOCIAZIONI_ATECO_IMPRESE_MANCANTI)){
						$associazioniAtecoInseriti++;
					}
			}
			
		}else{
			$eranoConsistentiAteco["Positivi"]++;
			$errorEranoConsistentiAteco[$impresa['Id']]= $impresa['CodiceFiscale'];
		}
		
			
		
		//controllo consistenza categorie	
		$consistenzaCategorie = $recupero->consistenzaCategorie($impresa['Id'],$categorieDaVisura);

		
		if ($PRINT_CONSISTENZA_CATEGORIE){
			print "<pre>";
			print_r($consistenzaCategorie);
			print "</pre>";
		}
		$temp = array();
		foreach ($consistenzaCategorie['Eccedenti'] as $keyCategoria => $valueCategoria){
			
			if(strcmp($valueCategoria['IsIscritta'],'Y') == 0 ){
				$categorieInconsistentiIscrizione['True']++;
				$temp[] = $keyCategoria;
				$impreseCategorieInconsistentiIscritte[$impresa['CodiceFiscale']] = $temp; 
			} else if(strcmp($valueCategoria['IsIscritta'],'N') == 0 ){
				$categorieInconsistentiIscrizione['False']++;
			}
		}
		
		if (is_array($consistenzaCategorie)){
			$eranoConsistentiCategorie["Negativi"]++;
			$errorEranoInconsistentiCategorie[$impresa['Id']]= $impresa['CodiceFiscale'];
			
			$categorieDaInserire += count($consistenzaCategorie['Mancanti']);
			$categorieDaEliminare += count($consistenzaCategorie['Eccedenti']);
			
			foreach ($consistenzaCategorie["Mancanti"] as $key => $value){
			
				if ($recupero->inserisciAssociazioneImpresaCategoriaMancante($key, $impresa['Id'],$INSERIRE_ASSOCIAZIONI_IMPRESE_CATEGORIE_MANCANTI)){
					$categorieInserite++;
				}
			}
			
			foreach ($consistenzaCategorie["Eccedenti"] as $key => $value){
				
				$categorieInconsistentiIscritte[$key] = $consistenzaCategorie["Eccedenti"][$key];
				if ($recupero->cancellaAssociazioneImpresaCategoriaEccedente($value['IdAssociazione'], $value['IdCategoria'], $impresa['Id'],$ESEGUI_CANCELLAMENTO_ASSOCIAZIONE_CATEGORIA_IMPRESA)){
					$categorieCancellate++;
				}
			}
			
		}else{
			$eranoConsistentiCategorie["Positivi"]++;
			$errorEranoConsistentiCategorie[$impresa['Id']]= $impresa['CodiceFiscale'];
		}
		
		//eliminazione ateco eccedenti
		if (is_array($consistenzaAteco)){
			foreach ($consistenzaAteco["Eccedenti"] as $key => $value){
			
				if($recupero->cancellaAssociazioneAtecoImpresaEccedente($value, $impresa['Id'],$ESEGUI_CANCELLAMENTO_ASSOCIAZIONI_ATECO_IMPRESE_MANCANTI)){
					$associazioniAtecoEliminate++;
				}
			}
		}
		
		//print "MANCANTI:  $categorieDaInserire<br>";
		//print "ECCENDENTI:  $categorieDaEliminare<br>";
			
		$prodottiImpresa = $recupero-> estrazioneProdottiImpresa($impresa['Id']);
		
		if($PRINT_PRODOTTI_IMPRESA){
			print "<pre><h3>PRODOTTI IMPRESA</h3>";
			print_r($prodottiImpresa);
			print "</pre>";
		}
		
		if (count ($prodottiImpresa) >0){
			print "<h3>CONSITENZA PRODOTTI</h3> ";
			$consistenzaProdotti = $recupero->consistenzaProdotti($categorieDaVisura, $prodottiImpresa);
				
			$confrontiProdotti["Positivi"] += count ($consistenzaProdotti['Consistenti']);
			$confrontiProdotti["Negativi"] += count ($consistenzaProdotti['Inconsistenti']);

				foreach ($consistenzaProdotti['Inconsistenti'] as $key => $valueProdotto){
					$elencoProdottiInconsinstenti[] +=  $valueProdotto;
				}
		}
		
		
		
	// visura NON TROVATA
	}else{
		$confronti["Negativi"]++;
		$errorConfornti[$impresa['Id']] = $impresa['CodiceFiscale'];
	}
	
	}else{
		$professionisti++;
		print "<h3>Professionista</h3>";
	}	
	print "<br><br>******************************************************************<br>";
}
	print "******************************************************************<br><br>";


	
//	$recupero->estraiCategorieFromCodiceAteco('71.11',true);

	/*
	
	print "<br>";
	$recupero->estraiCategorieFromCodiceAteco('43.29.09',true);
	

	print "<br>";
	$recupero->estraiCategorieFromCodiceAteco('73.11',true);
	
*/
print "<br>Totale imprese: $totaleImprese<br>";

print "<br>Totale Confronti: $totaleConfronti<br>";
print "<span style=\"color:green\">Confronti con successo: ".$confronti["Positivi"]."</span><br>";
print "<span style=\"color:red\">Confronti falliti: ". $confronti["Negativi"]."</span><br>";
if ($confronti["Negativi"] > 0){
	print "<pre>";
	print_r($errorConfornti);
	print "</pre>";
}
print "<br>Totale Professionisti: $professionisti<br>";

print "<br>Visure da Telemaco: ".$recupero->getCountTelemaco()."<br>";
print "<br>Visure da Parix: ".$recupero->getCountParix()."<br>";

/* STAMPA ESECUZIONE UPDATE PARTITA E NUMERO REA SE $UPDATE_IVA_NREA =  TRUE; */
if ($UPDATE_IVA_NREA){
	print "<br>Totale Update: $totalUpdate<br>";
	print "<span style=\"color:green\">Update con successo: $updateSuccess</span><br>";
	print "<span style=\"color:red\">Update fallite: $updateError</span><br>";
	if ($updateError > 0){
		print "<pre>";
		print_r($errorImpresa);
		print "</pre>";
	}
}

/* STAMPA ESECUZIONE UPDATE PARTITA E NUMERO REA SE $UPDATE_IVA_NREA =  TRUE; */
if ($UPDATE_ERANO_CONSISTENTI){
	print "<h3>Consistenza ateco prima di eseguire Modifiche</h3>";
	print "<span style=\"color:green\">Erano Consistenti: ".$eranoConsistentiAteco["Positivi"]."</span><br>";
	
	if ($PRINT_ERANO_CONSISTENTI_ARRAY){
		if ($eranoConsistentiAteco["Positivi"] > 0){
			print "<pre>";
			print_r($errorEranoConsistentiAteco);
			print "</pre>";
		}
	}
	print "<span style=\"color:red\">Erano Inconsistenti: ".$eranoConsistentiAteco["Negativi"]."</span><br>";
	if ($eranoConsistentiAteco["Negativi"] > 0){
		print "<pre>";
		print_r($errorEranoInconsistentiAteco);
		print "</pre>";
	}
}


if ($ATECO_PRESENTI){
	print "<br>";
	print "AtecoPresente: ".count($atecoPresente)."<br>";
	if (count($atecoPresente) > 0){
		print "<pre>";
		print_r($atecoPresente);
		print "</pre>";
	}
}

print "<br>";
print "<h4>AtecoDaInserire: ".count($atecoDaInserire)."</h4>";
if (count($atecoDaInserire) > 0){
	print "<pre>";
	print_r($atecoDaInserire);
	print "</pre>";
}





print "<h4>Ateco inseriti nel DB EXPO_T_Ateco: ".count($atecoInseriti)."</h4>";
if (count($atecoInseriti) > 0){
	print "<pre>";
	print_r($atecoInseriti);
	print "</pre>";
}

	
print "<h3>Associazioni Ateco Imprese</h3>";
print "Associazione Ateco Impresa da inserire: $associazioneAtecoDaInserire<br>";
print "Associazione Ateco Impresa da eliminare: $associazioneAtecoDaEliminare<br>";

print "<br>";
print "Associazione Ateco Impresa inseriti: $associazioniAtecoInseriti<br>";
print "Associazione Ateco Impresa Eliminati: $associazioniAtecoEliminate<br>";


if ($UPDATE_ERANO_CONSISTENTI){
	print "<h3>Consistenza categorie prima di eseguire Modifiche</h3>";
	print "<span style=\"color:green\">Erano Consistenti: ".$eranoConsistentiCategorie["Positivi"]."</span><br>";

	if ($PRINT_ERANO_CONSISTENTI_ARRAY){
		if ($eranoConsistentiCategorie["Positivi"] > 0){
			print "<pre>";
			print_r($errorEranoConsistentiCategorie);
			print "</pre>";
		}
	}
	print "<span style=\"color:red\">Erano Inconsistenti: ".$eranoConsistentiCategorie["Negativi"]."</span><br>";
	if ($eranoConsistentiCategorie["Negativi"] > 0){
		print "<pre>";
		print_r($errorEranoInconsistentiCategorie);
		print "</pre>";
	}
}


print "<h3>Categorie</h3>";
print "Categorie da inserire: $categorieDaInserire<br>";
print "Categorie da eliminare: $categorieDaEliminare<br>";


print "<br>";
print "<span style=\"color:green\">Categorie da eliminare NON Iscritte: ".$categorieInconsistentiIscrizione['False']."</span><br>";
print "<span style=\"color:red\">Categorie da eliminare Iscritte: ".$categorieInconsistentiIscrizione['True']."</span><br>";

print "<h3>imprese categorie iscritte da cancelllare</h3>";
print "<pre>";
print_r($impreseCategorieInconsistentiIscritte);
print "</pre>";

print "<br>";
print "Categorie Inserite: $categorieInserite<br>";
print "Categorie Eliminate: $categorieCancellate<br>";



print "<h3>Prodotti</h3>";
print "<span style=\"color:green\">Prodotti Consistenti: ".$confrontiProdotti["Positivi"]."</span><br>";
print "<span style=\"color:red\">Prodotti Inconsistenti: ".$confrontiProdotti["Negativi"]."</span><br>";


print "<h4>Prodotti inconsistenti : ".count($elencoProdottiInconsinstenti)."</h4>";
if (count($elencoProdottiInconsinstenti) > 0){
	print "<pre>";
	print_r($elencoProdottiInconsinstenti);
	print "</pre>";
}







