<script language="JavaScript" type="text/javascript">
	$(document).keyup(function(e) {
		if (e.keyCode == 27) { chiudiUpload(); }   // esc
    });
</script>

<?php
$idImpresaAllow = ($_POST['idImp']);
$utypeArrayAllow = array(96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $impresaAllow){

require_once($PROGETTO . "/view/languages/" . $_POST['lang'] . ".inc");

$idImpresa = $_POST['idImp'];
$azione = $_POST['action'];
if (strcasecmp($azione, "VIEW") == 0) {
    ?>
    <div class="contentHeader">
        <div class="divClose">
            <a href="javascript:chiudiUpload()" >X</a>   
        </div>
    </div>
    <div >
         <h4><?php echo _TITOLO_MOD_IMG_; ?></h4>
        <form id="upload" action="/ajaxlib/requester/upload_file.inc" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idImp" id="idImp" value="<?php echo $idImpresa; ?>">
            <input type="hidden" name="lang" id="lang" value="<?php echo $_REQUEST['lang']; ?>">
            <input type="hidden" name="action" id="action" value="INS">
            <?php echo _TESTO_MOD_IMG_; ?><br><br>
            <input type="file" name="logoF" id="logoF"><br>
            <br>
            <input type="submit" value="<?php echo _SALVA_; ?>" />

        </form>
    </div>
    <div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
    </div>
    <br/>

    <div id="message"></div>

    <script>
        $(document).ready(function()
        {

            var options = {
                beforeSubmit: function()
                {

                    if ($("#logoF").val() == '') {

                        alert("Devi selezionare un' immagine");
                        return false;
                    } else {
                        $("#progress").show();
                        //clear everything

                        $("#bar").width('0%');
                        $("#message").html("");
                        $("#percent").html("0%");
                    }
                },
                uploadProgress: function(event, position, total, percentComplete)
                {

                    $("#bar").width(percentComplete + '%');
                    $("#percent").html(percentComplete + '%');

                },
                success: function()
                {
                    $("#bar").width('100%');
                    $("#percent").html('100%');

                },
                complete: function(response)
                {

                    if ((response.responseText.indexOf("Errore:") !== -1) == true) {
                        $("#message").html("<font color='red'>" + response.responseText + "</font>");
                    } else {
                        $("#logoImgSrc").attr('src', "files/" + response.responseText);
                        $("#message").html("<font color='green'>Caricamento effettuato correttamente</font>");
                    }

                },
                error: function()
                {
                    $("#message").html("<font color='red'> ERRORE nel caricamento del logo</font>");

                }

            };

            $("#upload").ajaxForm(options);
            $("#progress").hide();
        });

    </script>

    <?php
}

}
//}

else{
	?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudiUpload()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}


?>