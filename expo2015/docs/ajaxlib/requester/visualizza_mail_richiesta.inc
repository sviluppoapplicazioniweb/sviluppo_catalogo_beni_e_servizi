<?php
include "configuration.inc";
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/languages/" . $_REQUEST['lang'] . ".inc");
$db = new DataBase();
$idUtente = settaVar($_REQUEST, "idu", "");
$prodottiOk = array();
$prodottiNo = array();
$ind = 0;
while (strcmp(settaVar($_REQUEST, "richiesta" . $ind, ""), "") != 0) {
    $idProd = settaVar($_REQUEST, "richiesta" . $ind, "");

    $query = "SELECT Nome,Stato FROM EXPO_T_Bacheca AS T
        LEFT JOIN EXPO_T_Bacheca_Trattative AS T2 ON T.Id = T2.IdBacheca
        RIGHT JOIN EXPO_T_Prodotti AS T3 ON T.IdProdotto = T3.Id
        WHERE T.IdProdotto = '$idProd' AND T.IdPartecipante = '$idUtente'";

    $nomeProd = $db->GetRow($query, "Nome");
    $statoProd = $db->GetRow($query, "Stato");
    if (strcmp($nomeProd, "") != 0 && strcmp($statoProd, "C") != 0) {
        $prodottiNo[] = $nomeProd;
    } else {
        $prodottiOk[] = $idProd;
    }

    $ind++;
}


if (count($prodottiNo) > 0) {
    ?>
    <div>
        <?php
        echo _TRA_INCORSO_ . "<br>";
        echo "<ul>";
        foreach ($prodottiNo AS $value) {
            echo "<li>" . $value . "</li>";
        }
        echo "</ul>";
        ?>
    </div>
    <?php
}
if (count($prodottiOk) > 0) {
    ?>
    <form enctype="multipart/form-data" id="form_moduli" name="form_moduli"  >
        <?php
        $ind = 0;
        foreach ($prodottiOk AS $value) {
            ?>
            <input type = "hidden" id="richiesta<?php echo $ind; ?>" name="richiesta<?php echo $ind; ?>" value = "<?php echo $value; ?>"/>
            <?php
            $ind++;
        }
        ?>  
        <strong><?php echo _OGGETTO_; ?> :*</strong> 
        <input type = "text" id = "oggetto" name = "oggetto" size = "50" maxlength = "250" value = ""/>

        <table id="tabella_elenchi"  >

            <tr>
                <td colspan="3" style="text-align: left;border-top:1px solid #000;padding-top: 10px;"><strong><?php echo _MESSAGGIO_; ?>: *</strong></td>
            </tr>
            <tr >

                <td colspan="3" ><textarea rows="12" cols="70" id="messaggio" name="messaggio"></textarea></td>
            </tr>

        </table> 

        <a class="buttonsLista" href="javascript:invia('index.phtml?Id_VMenu=299&amp;azione=SEND&amp;option=MLT')"><?php echo _INVIA_; ?></a>

    </form>
<?php } ?>
<script language="JavaScript" type="text/javascript">
    function invia(destinazione) {
        var iform = document.form_moduli;
        if (validaCampi(iform)) {
            iform.method = "post";
             iform.setAttribute('action',destinazione);
            iform.submit();
        }

    }

    function validaCampi(iform) {
        var formOggetto = iform.oggetto.value;
        var formMessaggio = iform.messaggio.value;
        if (formOggetto === "" || formMessaggio === "") {
            alert("<?php echo _CAMPI_OB_; ?>");
            return false;
        }
        return true;
    }
</script>