<script language="JavaScript" type="text/javascript">
	$(document).keyup(function(e) {
		if (e.keyCode == 27) { chiudiUpload(); }   // esc
    });
</script>

<?php
$uidArrayAllow = array($_POST['idProf']);
$utypeArrayAllow = array(96);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $uidAllow){

require_once($PROGETTO . "/view/languages/" . $_POST['lang'] . ".inc");
$idProf = (int)$_POST['idProf'];
$idOrdine = (int)$_POST['idOrdine'];
$azione = $_POST['action'];

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();

$nomeAlboSQL = "SELECT NomeOrdineProfessionale FROM EXPO_Tlk_OrdiniProfessionali 
		WHERE Autocertificazione = 'Y' AND Id=$idOrdine"; 
$nomeAlbo = $db->GetRow($nomeAlboSQL,'NomeOrdineProfessionale');

function getFile($ordine,$idProf){
	$db = new DataBase();
	$sql = "SELECT Concat(Cognome,' ',Nome) as Persona FROM T_Anagrafica WHERE Id = $idProf";
	
	$rootDoc = selectRootDocPersona($db->GetRow($sql,"Persona")) . "/" . "$idProf";
	$dir = Config::ROOTFILES.$rootDoc;
	$dirAutocertificazioni = $dir."/autocertificazioni/";
	$dirDownload = Config::ROOTFILESHTML.$rootDoc."/autocertificazioni/";
	
	$arrayfiles=Array();
	if(file_exists($dirAutocertificazioni)){
		$handle = opendir($dirAutocertificazioni);
		while (false !== ($file = readdir($handle))) {
			if(is_file($dirAutocertificazioni.$file)){
				//array_push($this->arrayfiles,$file);
				$end = explode('.',$file);
				if ($end[0] == $ordine){
					return array("FILE" => $dirDownload.$file,"NOME" => $file);;
				}
			}
		}
		$handle = closedir($handle);
	}
	return false;
}

if ((strcasecmp($azione, "VIEW") == 0) || (strcasecmp($azione, "EDIT") == 0)) {
    ?>
    <div class="contentHeader">
        <div class="divClose">
            <a href="javascript:chiudiUpload()" >X</a>   
        </div>
    </div>
    <div >
         <h4>Carica il certificato di iscrizione all'albo <?php print $nomeAlbo ?></h4>
        <form id="upload" action="/ajaxlib/requester/upload_autocertificazione.inc" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idProf" id="idProf" value="<?php echo $idProf; ?>">
            <input type="hidden" name="idOrdine" id="idOrdine" value="<?php echo $idOrdine; ?>">
            <input type="hidden" name="action" id="action" value="INS">
            <?php echo _TESTO_AUTOCERTIFICAZIONE_; ?><br><br>
            <?php 
            if (strcasecmp($azione, "EDIT") == 0){
				$file = getFile($idOrdine,$idProf);
				print "Autocertificazione attalmente presentata: ";
				print '<a href="'.$file["FILE"].'">'.$file['NOME'].'</a>';
			}
			print "<br>";
            ?>
            <input type="file" name="autocertificazione" id="auto"><br>
            <br>
            <input style="float: right;"class="buttonRed" type="submit" value="<?php echo _SALVA_; ?>" />

            
            
        </form>
    </div>
    <div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
    </div>
    <br/>

    <div id="message"></div>

   <script>
   function doRedirect() {
	   location.href = "index.phtml?Id_VMenu=507";
	   }
   
        $(document).ready(function()
        {

            var options = {
                beforeSubmit: function()
                {
                    if ($("#auto").val() == '') {

                        alert("Si deve selezionare un file da caricare.");
                        return false;
                    } else {
                        $("#progress").show();
                        //clear everything

                        $("#bar").width('0%');
                        $("#message").html("");
                        $("#percent").html("0%");
                    }
                },
                uploadProgress: function(event, position, total, percentComplete)
                {

                    $("#bar").width(percentComplete + '%');
                    $("#percent").html(percentComplete + '%');
					
                },
                success: function()
                {
                    $("#bar").width('100%');
                    $("#percent").html('100%');
                    
                },
                complete: function(response)
                {

                    if ((response.responseText.indexOf("Errore:") !== -1) == true) {
                        $("#message").html("<font color='red'>" + response.responseText + "</font>");
                    } else {
                        $("#message").html("<font color='green'>Caricamento effettuato correttamente</font>");
                        window.setTimeout("doRedirect()", 0);
                    }

                },
                error: function()
                {
                    $("#message").html("<font color='red'> ERRORE nel caricamento del logo</font>");

                }

            };

            $("#upload").ajaxForm(options);
            $("#progress").hide();
        });

    </script>

    <?php
}else if (strcasecmp($azione, "EDIT") == 0) {
print "EDIT";

}else {
print "nulla";
}
}else{
	?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudiUpload()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}


?>