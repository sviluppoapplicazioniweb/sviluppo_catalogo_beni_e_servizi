<script language="JavaScript" type="text/javascript">
    $(function() {
        $("#tabs").tabs();
    });

    $(document).keyup(function(e) {
    	  
    	  if (e.keyCode == 27) { chiudi(); }   // esc
    	});
</script>


<?php

$idImpresaAllow = ($_POST['Id']);

$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $impresaAllow){

$Lang = $_REQUEST['lang'];



require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");



require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();
$IdImpresa = settaVar($_REQUEST, 'Id', '');

$ordine = settaVar($_REQUEST, 'Ordine', '');
$studio = settaVar($_REQUEST, 'IsStudio', '');

$rootDoc = selectRootDoc($db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale", null, "Ajax - partecipante_expo_t_imprese - root doc")) . "/";



//MODIFICA TAB IMPRESE
$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'";

$row = $db->GetRow($query, null, null, "Ajax - partecipante_expo_t_imprese - Modifica TAb Imprese");

$tipoCategora = "";
$query = "SELECT SUBSTRING(Id_Categoria,1,2) AS Id_Categoria
FROM EXPO_Tlk_Categorie AS T RIGHT JOIN EXPO_TJ_Imprese_Categorie AS T1 ON T.Id = T1.IdCategoria
WHERE (((T1.IdImpresa)='$IdImpresa') AND ((T.Id_Categoria) Like '01%') AND ((T1.IsIscritta)='Y')) OR (((T.Id_Categoria) Like '02%') AND ((T1.IsIscritta)='Y'))
GROUP BY SUBSTRING(Id_Categoria,1,2);";
$tipoCategora = $db->GetRows($query);

//RETI
$reti = FALSE;
$queryRete = "SELECT EXPO_TJ_Imprese_RetiImpresa.Id_Impresa, EXPO_T_RetiImpresa.NomeRete, EXPO_T_RetiImpresa.Id
FROM EXPO_T_RetiImpresa INNER JOIN EXPO_TJ_Imprese_RetiImpresa ON EXPO_T_RetiImpresa.Id = EXPO_TJ_Imprese_RetiImpresa.Id_Rete
WHERE EXPO_TJ_Imprese_RetiImpresa.Id_Impresa = $IdImpresa ORDER BY EXPO_T_RetiImpresa.NomeRete;";
if ($db->NumRows($queryRete) > 0) {
	$reti = TRUE;
}
?>
<div class="contentHeader">
    <div class="divClose">
        <a draggable="false" href="javascript:chiudi()" >X</a>   
		<a draggable="false" href="javascript:stampaBOXPDF('<?php echo $Lang;?>',<?php echo $IdImpresa;?>)">Print</a>
    </div>
    <div id="header-lang">
        <?php include_once $PROGETTO . "/view/lib/languages.inc"; ?>
    </div>
</div>
<div class="divBoxAnteprima">

    <?php
    $tabDettagli = checkIfDisplayableImpresa($IdImpresa, 'dettagli', $_POST);
    $tabDettagli = true;
    $tabQualita = checkIfDisplayableImpresa($IdImpresa, 'qualita', $_POST);
    $tabQualita = true;
    $tabAssociazioni = checkIfDisplayableImpresa($IdImpresa, 'associazioni', $_POST);
    $tabAssociazioni = true;
    ?>
    <div id="tabs">
        <ul id="ulmenu">
            <li><a draggable="false" href="#tabs-1"><?php echo _DATI_GENERALI_; ?></a></li>
            <li><a draggable="false" href="#tabs-5"><?php echo _CATEGORIE_MERCIOLOGICHE_; ?></a></li>
            <li><a draggable="false" href="#tabs-2"><?php echo _DETTAGLI_; ?></a></li>
            <li><a draggable="false" href="#tabs-3"><?php echo _QUALIT_T_; ?></a></li>
            <li><a draggable="false" href="#tabs-4"><?php echo _ASSOCIZIONI_RETI_; ?></a></li>
        </ul>
        <div id="tabs-1">

            <h4><?php 	if ( $ordine== 0){ 
						echo _TIT_1_;
					} else if (strcmp($studio,"Y")== 0){
						echo _TIT_1_STUDIO_;
					} else if (strcmp($studio,"N")== 0){
						echo _TIT_1_LP_;
					} 
			?></h4>
            <div class="containerDataCheck">
                <table width="100%">
                    <tr>
                        <td ><b><?php if ($ordine > 0){echo _RAGSOC_PROF_;}else {echo _RAGSOC_;} ?>:</b> <?php echo $row['RagioneSociale']; ?></td>
                        <td rowspan="5"><?php echo viewLogo($row['Logo'],$_POST['WebSiteURL']); ?></td>
                    </tr>
                    <?php if (strcmp($row['CapitaleSociale'], "0") != 0 && strcmp($row['CapitaleSociale'], "") != 0) { ?>
                        <tr>
                            <td colspan="1"><b><?php echo _CAPISOC_; ?>:</b> <?php echo $row['CapitaleSociale']; ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="1" valign="middle" colspan="2"><b><?php echo _SEDE_LEG_; ?>:</b> 
                            <?php echo $row['Indirizzo']; ?> <?php echo $row['Cap']; ?> - <?php echo $row['Citta']; ?> ( <?php echo $row['Pr']; ?> )
                        </td>
                    </tr> 

                    <tr>
                        <td colspan="1"><b><?php if ($ordine > 0){echo _N_DIP_ORDINI_;}else {echo _N_DIP_;} ?>:</b>  <?php echo $_POST['NumeroDipendenti']; ?></td>
                    </tr>
                    <tr>                   
                        <td colspan="1" style="padding-left: 10px;">
                            <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/descrizione", _DESCRIZIONE_FILE_); ?>
                        </td>
                    </tr>
                    <tr>                   
                        <td colspan="1" style="padding-left: 10px;">
                            <?php echo getAllegati('link', $_POST['descrizioneL']); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <?php
            if ($_POST['Telefono'] == '' and $_POST['Fax'] == '' and $_POST['WebSiteURL'] == '') {
                
            } else {
                ?>
                <h4><?php echo _TIT_2_; ?></h4>
                <div class="containerDataCheck">
                    <table width="100%">
                        <?php
                        if ($_POST['Telefono'] == '') {
                            
                        } else {
                            ?>
                            <tr>
                                <td ><b><?php echo _TEL_; ?>:</b> <?php echo $_POST['Telefono']; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        if ($_POST['Fax'] == '') {
                            
                        } else {
                            ?>
                            <tr>
                                <td ><b>Fax :</b> <?php echo $_POST['Fax']; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        if ($_POST['WebSiteURL'] == '') {
                            
                        } else {
                            ?>
                            <tr>
                                <td ><b>Site Web:</b> <?php if (strcasecmp($_POST['WebSiteURL'], "") != 0) { ?>
                                        <a href="<?php echo getLinkWeb($_POST['WebSiteURL']); ?>" target="_blank"><?php echo $_POST['WebSiteURL']; ?></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                        if ($_POST['Email'] == '') {
                            
                        } else {
                            ?>
                             <!--<tr>
                                <th >E-mail :</th>
                                <td ><?php echo $_POST['Email']; ?></td>
                            </tr>!-->
                            <?php
                        }
                        ?>


                    </table>
                </div>
                <?php
            }
            ?>
            <?php
            $fileExist = file_exists(Config::ROOTFILES . $rootDoc . $row['Id'] . "/bilanci.pdf");

            if ($_POST['UltimoFatturato'] == '' and $_POST['FatturatoUltimoEsercizio'] == '' and $fileExist == false) {
                
            } else {
                ?>
                <h4><?php echo _TIT_3_; ?></h4>
                <div class="containerDataCheck">
                    <table width="100%">

                        <tr>
                            <td ><b><?php echo _U_FATTURATO_; ?>:</b> <?php echo $_POST['UltimoFatturato']; ?>  &euro;</td>
                        </tr>

                        <?php
                        if ($_POST['FatturatoUltimoEsercizio'] != '') {
                            ?>
                            <tr>
                                <td ><b><?php echo _U_FATTURATO_ESERC; ?>:</b> <?php echo $_POST['FatturatoUltimoEsercizio']; ?>  &euro;</td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td>
                                <?php
                                echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/bilanci", _BILANCI_FILE_);
                                ?> 
                            </td>
                        </tr> 
                    </table>
                </div>
            <?php } ?>


            <h4><?php echo _DESCRIZIONE_ATT_; ?></h4>
            <div class="containerDataCheck">
                <table width="100%">

                    <tr>
                        <td >
                            <?php
                            $IdTipo = "I" . $IdImpresa;
//                                $testo = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = 'descrizione' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
//                                if (strcmp($testo, "") != 0)
//                                    echo html_entity_decode($testo);
//                                else
                            switch ($Lang) {
                                case 'IT': echo html_entity_decode($_POST['DescEstesa']);
                                    break;
                                case 'EN': echo html_entity_decode($_POST['DescEstesa_AL']);
                                    break;
                                case 'FR': echo html_entity_decode($_POST['DescEstesa_AL2']);
                                    break;
                            }
                            ?>
                        </td>
                    </tr>

                </table>
            </div>


        </div>
        
        <div id="tabs-5">
            <div id="listCategories">
                <h4><?php echo _C_MERC_; ?></h4>
                <ul class="divCategorie">
                    <?php
                    $query = "SELECT TJ.Id AS Id,DescEstesa,DescEstesa_AL,DescEstesa_AL2,T.FlagNote,T.Descrizione,T.Note_AL
                                FROM EXPO_TJ_Imprese_Categorie AS TJ
                                RIGHT JOIN EXPO_Tlk_Categorie AS T ON TJ.IdCategoria = T.Id
                                WHERE TJ.IdImpresa = '$IdImpresa'
                                 AND  TJ.IsIscritta = 'Y'
                                      ORDER BY DescEstesa";
                    foreach ($db->GetRows($query) AS $rows) {
						$infoNota = '';
						if ((strcmp ($rows['FlagNote'],'Y') == 0 ) &&( strcmp ($rows['Note_AL'],'_DEFAULT_') == 0 )){
								
							$nota = _DISCLAIMER_CATEGORIE_DEFAULT_PDMS_;
							/*if ((strcmp ($rows['Note'],'_DEFAULT_') != 0)){
								$nota = $rows['Note'];
							}
							*/	
						
							$infoNota = " <a href=\"#\" class=\"tooltip\" draggable=\"false\" >
                                        <img src=\"tmpl/expo2015/catalogo/images/info1.png\" name=\"iconaInfo\" />
                                        <span >
                                        ".$nota."    <br><br>
                                        </span>
                                    </a>";
						}	

                        switch ($Lang) {
                            case 'IT':
                                ?><li ><?php echo str_replace("->", ">", $rows ['DescEstesa']); echo $infoNota;?></li><?php
                                break;
                            case 'EN':
                                ?><li><?php echo str_replace("->", ">", $rows ['DescEstesa_AL']); echo $infoNota;?></li><?php
                                    break;
                                case 'FR':
                                    ?><li><?php echo str_replace("->", ">", $rows ['DescEstesa_AL2']); echo $infoNota;?></li><?php
                                    break;
                                default:
                                    break;
                            }
                        }
                        ?>

                </ul>


            </div>
        </div>
        
        
        <?php
        if ($tabDettagli == true) {
            ?>
            <div id="tabs-2" >
            	<?php if ($ordine == 0){ ?>
                <h4><?php echo _T_IMPRESA_; ?></h4>
                <div class="containerDataCheck">
                    <table width="100%">
                        <tr>
                            <td >
                                <?php echo getCheckImg($_POST['IsGeneralContractor']); ?> <?php echo _GENER_CONT_; ?>
							</td>
                        </tr>
                        <tr>
                            <td >
                                <?php echo getCheckImg($_POST['IsProduttore']); ?> <?php echo _PRODUTTORE_; ?>
                            </td>
                        </tr>    
                        <tr>
                            <td >        
                                <?php echo getCheckImg($_POST['IsDistributore']); ?> <?php echo _DISTRIBUTORE_; ?>
                       		</td>
                       </tr>
                       <tr>
                            <td >         
                                <?php echo getCheckImg($_POST['IsPrestatoreDiServizi']); ?> <?php echo _IsPrestatoreDiServizi_; ?>
                            </td>
                        </tr>
                    </table>           

                </div>
                <?php }?>
                <h4><?php if ( $ordine== 0){echo _SEDI_;} else if (strcmp($studio,"Y")== 0){echo _SEDI_STUDIO_;} else if (strcmp($studio,"N")== 0){echo _SEDI_LP_;} ?></h4>
                <div class="containerDataCheck">
                    <table width="95%">
                        <tr>
                            <td width="40%"> 
                                <?php echo getCheckImg($_POST['IsSediEstere']); ?> <?php echo _S_ESTERA_; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"> 
                                <?php echo getCheckImg($_POST['IsNetwork']); ?> <?php echo _NET_INTER_; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/internazionalizzazione", _INTERNAZIONALIZZAZIONE_FILE_); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                        
                                <?php echo getAllegati('link', $_POST['internazionalizzazioneL']); ?>
                            </td>
                        </tr>  
                    </table>
                </div>
                <h4><?php echo _REFERENZE_; ?></h4>
                <div class="containerDataCheck">
                    <table width="95%">
                        <tr>
                            <td>
                                <?php echo getCheckImg($_POST['partecipazioneExpo']); ?> <?php echo _PARTECIPZIONI_EXPO_; ?><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo getCheckImg($_POST['partecipazioniInternazionali']); ?> <?php echo _PAR_INTERNAZIONALI_; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br>
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/referenze", _REFERENZE_FILE_); ?>
                                <?php echo getAllegati('link', $_POST['referenzeL']); ?>
                            </td>  
                        </tr>
                    </table>
                </div>
                <?php
                $query = "SELECT Lingua,EXPO_TJ_Imprese_Lingue.Id AS IdL FROM EXPO_Tlk_Lingue,EXPO_TJ_Imprese_Lingue
          WHERE EXPO_TJ_Imprese_Lingue.IdImpresa = '$IdImpresa' AND
          EXPO_TJ_Imprese_Lingue.IdLingua = EXPO_Tlk_Lingue.Id ORDER BY Lingua";
                if ($db->NumRows($query) > 0) {
                    ?>
                    <h4><?php echo _CON_LINGUE_; ?></h4>

                    <?php if ($db->NumRows($query) > 0) { ?>
                        <ul class="ulLingue">
                            <?php foreach ($db->GetRows($query) AS $rows) { ?>
                                <li ><?php echo $rows ['Lingua']; ?> </li>
                            <?php } ?>
                        </ul>
                        <?php
                    } else {
                        echo _NO_LINGUE_;
                    }
                    ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php
        if ($tabQualita == true) {
            ?>
            <div id="tabs-3">
                <?php
//               if($_POST['aModello']!='Y' and $_POST['pMasetr']!='Y' and $_POST['pCertificazione']!='Y'){
//               }else{
                ?>
                <h4><?php 	if ( $ordine == 0){ 
							echo _QUALIT_;
						} else if (strcmp($studio,"Y")== 0){
							echo _QUALIT_STUDIO_;
						} else if (strcmp($studio,"N")== 0){
							echo _QUALIT_LP_;
						} ?></h4>
                <div class="containerDataCheck">
                    <table width="95%">
                        <tr>
                            <td>

                                <?php echo getCheckImg($_POST['aModello']); ?> <?php echo _A_MODELLO_; ?><br>
						 	</td>
                        </tr>
						<tr>
                            <td>
                                <?php
                                foreach ($tipoCategora as $key => $value) {
                                    if (in_array("01", $tipoCategora[$key])) {
                                        ?>
                                        <?php echo getCheckImg($_POST['IsMasterSpecialistico']); ?> <?php echo _P_MASTER_; ?><br>
                                        <?php
                                        if (strcmp($row['IsMasterSpecialistico'], "Y") == 0) {
                                            echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/master", _P_MASTER_FILE_);
                                        }
                                    }
                                }
                                ?>
							</td>
                        </tr>
						<tr>
                            <td>
                                <?php
                                foreach ($tipoCategora as $key => $value) {
                                    if (in_array("02", $tipoCategora[$key])) {
                                        ?>
                                        <?php echo getCheckImg($_POST['IsCertificazione']); ?> <?php echo _P_CERTIFICAZIONE_; ?><br>
                                        <?php
                                        if (strcmp($row['IsCertificazione'], "Y") == 0) {
                                            echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/pCertificazione", _P_CERTIFICAZIONE_FILE_);
                                        }
                                    }
                                }
                                ?>

                            </td>
                        </tr>
                    </table>
                </div>



                <?php
                /*    $query = "SELECT A.Descrizione,A.Sigla
                  FROM EXPO_TJ_Imprese_Accreditamenti as IA
                  join  EXPO_Tlk_Accreditamenti as A on IA.IdAccreditamento=A.Id
                  where IA.IdImpresa='" . $IdImpresa . "'
                  ORDER BY A.Descrizione";
                  $queryLing = "SELECT IDCert,T.Descrizione AS Descrizione,Descrizione_FR,Descrizione_EN
                  FROM EXPO_Tlk_Certificazioni AS T
                  JOIN EXPO_TJ_Imprese_Certificazione TJ ON T.Id = TJ.IdCertificazione
                  WHERE IdImpresa =  '$IdImpresa'
                  ORDER BY IDCert";
                 */

//
//                }else{
                ?>
                <h4><?php echo _CERTIFICAZIONI2_; ?></h4>
                <div class="containerDataCheck">
                    <table width="95%">
                        
                                <?php
                                $query = "SELECT A.Descrizione,A.Sigla
                                          FROM EXPO_TJ_Imprese_Accreditamenti as IA
                                          join  EXPO_Tlk_Accreditamenti as A on IA.IdAccreditamento=A.Id
                                          where IA.IdImpresa='" . $IdImpresa . "'
                                          ORDER BY A.Descrizione";
                                foreach ($db->GetRows($query) AS $rows) {
                                    ?>
                                 <tr>
                            		<td >
                                    <div style="float:none">
                                        <?php echo $rows ['Sigla']; ?>
                                        <a href="#" class="tooltip" draggable="false" >
                                            <img src="tmpl/expo2015/catalogo/images/info.png" name="iconaInfo" />
                                            <span>
                                                <img class="callout" draggable="false"  src="tmpl/expo2015/catalogo/images/callout.gif" />
                                                <?php echo $rows['Descrizione']; ?>
                                            </span>
                                        </a>
                                    </div>
									</td>
                            		</tr >


                                    <?php
                                }
                                $LangTab = "Descrizione";
                                if (strcmp($Lang, "IT") != 0)
                                    $LangTab .= "_" . $Lang;


                                $query = "SELECT * FROM EXPO_Tlk_Certificazioni AS C join EXPO_TJ_Certificazioni_OrdiniProfessionali AS TJ ON C.Id = TJ.Id_Certificazione WHERE TJ.Id_OrdineProfessionale = $ordine ORDER BY C.IDCert";
                                //$query = "SELECT * FROM EXPO_Tlk_Certificazioni ORDER BY IDCert";
                                foreach ($db->GetRows($query) AS $rows) {
                                    $query_check = "SELECT *
                                          FROM EXPO_TJ_Imprese_Certificazione
                                          where IdImpresa='" . $IdImpresa . "'
                                              and IdCertificazione='" . $rows['Id'] . "'";

                                    $checked = "N";
                                    if ($db->NumRows($query_check) > 0) {
                                        $checked = 'Y';
                                    }
                                    ?>
                                <tr>
                                <td>    
                                    <div style="float:none">
                                        <?php echo getCheckImg($checked); ?>   <?php echo $rows ['IDCert']; ?>
                                        <a href="#" class="tooltip" draggable="false" >
                                            <img src="tmpl/expo2015/catalogo/images/info.png" name="iconaInfo" />
                                            <span>
                                                <img class="callout" draggable="false"  src="tmpl/expo2015/catalogo/images/callout.gif" />
                                                <?php echo $rows[$LangTab]; ?><br><br>
                                            </span>
                                        </a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/certificazioni", _CERTIFICAZIONI_FILE_); ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php
//                }
//                $linguaOn=true;
//                 if($Lang=='IT'){
//                        if($_POST['qualita']=='');
//                        $linguaOn=false;
//                 }
//                 if($Lang=='EN'){
//                        if($_POST['qualitaEN']=='');
//                        $linguaOn=false;
//                 }
//
//
//                if($Lang=='FR'){
//                        if($_POST['qualitaFR']=='');
//                        $linguaOn=false;
//                 }
                $_POST['qualitaL'] = str_replace("http://", "", $_POST['qualitaL']);

                if ($_POST['qualita'] != '') {
//
//                 }else{
                    ?>
                    <h4><?php echo _GREEN_; ?></h4>
                    <div class="containerDataCheck">
                        <table width="95%">
                            <tr>
                                <td >
                                    <?php
                                    if ($Lang == 'IT')
                                        echo html_entity_decode($_POST['qualita']);

                                    if ($Lang == 'EN')
                                        echo html_entity_decode($_POST['qualitaEN']);

                                    if ($Lang == 'FR')
                                        echo html_entity_decode($_POST['qualitaFR']);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <br>
                                    <?php echo getAllegatiFiles('file', $rootDoc . $row['Id'] . "/qualita", _QUALITA_FILE_); ?><br> 
                                    <?php echo getAllegati('link', $_POST['qualitaL']); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        <?php } ?>
        <?php if ($tabAssociazioni == true) { ?>
            <div id="tabs-4">
                <?php
//                $_POST['RetiImpresaLink'] = str_replace("http://", "", $_POST['RetiImpresaLink']);
//                if($_POST['retiImpresa']!='Y' and $_POST['RetiImpresaLink']==''){
//
//                }else{
                ?>
                <h4><?php if ( $ordine== 0){echo _APP_RETE_IMP_;} else if ( $ordine > 0){echo _APP_RETE_IMP_PROF_;}?></h4>
                <div class="containerDataCheck">
                <table width="95%">
                    <tr>
                        <td  >

                            <?php
                            if ($reti) {
                                echo "<ul>";
                                foreach ($db->GetRows($queryRete) AS $rows) {
                                    echo '<li><a draggable="false" href="javascript:previewReteR(\''.$rows['NomeRete'].'\')" >' . $rows['NomeRete'] . '</a></li>';
                        }
                        echo "</ul>";
                    } else {
                        echo _NO_RETI_;
                    }
                    ?>

                    </td>
                    </tr>
                </table>
            </div>

                <?php
//                }
                $query = "SELECT EXPO_TJ_Imprese_Associazioni.Id AS Id,EXPO_Tlk_Associazioni.Denominazione AS Denominazione FROM EXPO_Tlk_Associazioni,EXPO_TJ_Imprese_Associazioni
                                    WHERE EXPO_TJ_Imprese_Associazioni.IdImpresa = '$IdImpresa' AND
                                      EXPO_TJ_Imprese_Associazioni.IdAssociazione = EXPO_Tlk_Associazioni.id ORDER BY EXPO_TJ_Imprese_Associazioni.Id";
                if (1 == 2) {
                    ?>
                    <h4><?php echo _APP_ASS_; ?></h4>
                    <div class="containerDataCheck">
                        <table width="95%">
                            <tr>
                                <td >
                                    <?php
                                    foreach ($db->GetRows($query) AS $rows) {
                                        ?>
                                        <div class="divLingue"><?php echo $rows ['Denominazione']; ?> </div>

                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>

<?php }
//}

else{
	?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudi()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>