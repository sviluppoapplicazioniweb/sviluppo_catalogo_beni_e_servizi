<script type="text/javascript" src="/jquery/js/tsc/tabs.js"></script>
<script type="text/javascript" src="/jquery/js/tsc/accordion_toggle.js"></script>
<?php
$uidArrayAllow = array(3);
$utypeArrayAllow = array(95);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $uidAllow){

require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();

$tag = "";
$idNonTest = 27;
$campo = $_POST['campo'];
$tab = $_POST['tab'];
$ord = $_POST['ord'];
if (strcmp($tab, "formeGiuridiche") == 0) {
//FORME GIURIDICHE
    $arrayFG = array();
    $query = "SELECT * FROM EXPO_Tlk_Forme_Giuridiche ORDER BY Descrizione ";
    foreach ($db->GetRows($query) AS $rows) {
        $array = array();
        $desc = $rows['Descrizione'];
        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and FormaGiuridica = '" . mysql_escape_string(htmlentities($desc)) . "' ORDER BY RagioneSociale";
        $array['descrizione'] = $desc;
        $array['iscritti'] = $db->NumRows($queryImprese);
        $arrayImp = array();
        foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
            $arrayImp[] = $rowsImprese ['RagioneSociale'] . "<br>";
        }
        $array['imprese'] = $arrayImp;
        $arrayFG[] = $array;
    }

    if (strcmp($ord, "ASC") == 0) {
        $ordina = SORT_ASC;
    } else {
        $ordina = SORT_DESC;
    }

    $ordina = array();
    foreach ($arrayFG as $key => $row) {
        $ordina[$key] = $row[$campo];
    }
    $tipoOrdinamento = SORT_DESC;
    if (strcmp($ord, "ASC") == 0) {
        $tipoOrdinamento = SORT_ASC;
    }

    array_multisort($ordina, $tipoOrdinamento, $arrayFG);


    $tag .= '<dl class="tsc_accordion2" style="width:100%;margin-left: -10px" >';
    foreach ($arrayFG as $array) {
        $tag .= '<dt >' . $array['descrizione'] . '<span style="float:right;">' . $array['iscritti'] . '</span></dt>';
        $tag .= '<dd >
        <p>';
        foreach ($array['imprese'] AS $rowsImprese) {
            if (strcmp($rowsImprese, "") != 0)
                $tag .= $rowsImprese . "<br>";
        }
        $tag .= '    </p>
    </dd>';
    }
    $tag .='</dl>';
} elseif (strcmp($tab, "retiImprese") == 0) {
    $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and IsRetiImpresa = 'Y' ORDER BY RagioneSociale $ord";
    foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
        $tag .= $rowsImprese['RagioneSociale'] . "<br>";
    }
} elseif (strcmp($tab, "province") == 0) {
    $arrayPr = array();
//PROVINCIA
    $query = "SELECT DISTINCT Descrizione,Pr FROM EXPO_T_Imprese AS T JOIN Tlk_Province AS T2 "
            . "ON T.Pr = T2.Sigla"
            . " WHERE T.Id > $idNonTest ORDER BY Pr";
    foreach ($db->GetRows($query) AS $rows) {
        $array = array();
        $array['province'] = $rows['Descrizione'];
        $pr = $rows['Pr'];
        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and Pr = '$pr' ORDER BY RagioneSociale";
        $array['iscritti'] = $db->NumRows($queryImprese);

        $arrayImp = array();
        foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
            $arrayImp[] = $rowsImprese ['RagioneSociale'] . "<br>";
        }
        $array['imprese'] = $arrayImp;
        $arrayPr[] = $array;
    }


    $ordina = array();
    foreach ($arrayPr as $key => $row) {
        $ordina[$key] = $row[$campo];
    }
    $tipoOrdinamento = SORT_DESC;
    if (strcmp($ord, "ASC") == 0) {
        $tipoOrdinamento = SORT_ASC;
    }

    array_multisort($ordina, $tipoOrdinamento, $arrayPr);


    $tag .= '<dl class="tsc_accordion2" style="width:100%;margin-left: -10px" >';
    foreach ($arrayPr as $array) {
        $tag .= '<dt >' . $array['province'] . '<span style="float:right;">' . $array['iscritti'] . '</span></dt>';
        $tag .= '<dd >
        <p>';
        foreach ($array['imprese'] AS $rowsImprese) {
            if (strcmp($rowsImprese, "") != 0)
                $tag .= $rowsImprese . "<br>";
        }
        $tag .= '    </p>
    </dd>';
    }
    $tag .='</dl>';
} elseif (strcmp($tab, "iscrizioni") == 0) {
    
    if (isset($_POST['inizio']) && strcmp($_POST['inizio'], "") != 0 && isset($_POST['fine']) && strcmp($_POST['fine'], "") != 0) {
        $dataInizio = date_format(date_create($_POST['inizio']), "Y-m-d");
        $dataFine = date_format(date_create($_POST['fine']), "Y-m-d");
    } elseif (isset($_POST['periodo']) && strcmp($_POST['periodo'], "") != 0) {
        $dataInizio = $_POST['periodo'];
        $dataFine = date("Y-m-d");
    } else {
        $dataInizio = "2014-01-01";
        $dataFine = date("Y-m-d");
    }

    list($annoInizio, $meseInizio, $giornoInizio) = explode("-", $dataInizio);
    list($annoFine, $meseFine, $giornoFine) = explode("-", $dataFine);

    $arrayIn = array();
//NUMERO DI IMPRESE X MESE
    $arrayMesi = array("01" => "Gennaio", "02" => "Febbraio", "03" => "Marzo", "04" => "Aprile", "05" => "Maggio", "06" => "Giugno", "07" => "Luglio", "08" => "Agosto", "09" => "Settembre", "10" => "Ottobre", "11" => "Novembre", "12" => "Dicembre");
    for ($anno = $annoInizio; $anno <= $annoFine; $anno ++) {
        $meseFineCiclo = 12;
        $meseInizioCiclo = 1;
        if ($anno == $annoInizio) {
            $meseInizioCiclo = $meseInizio;
        }

        if ($anno == $annoFine) {
            $meseFineCiclo = $meseFine;
        }

        for ($i = $meseInizioCiclo; $i <= $meseFineCiclo; $i++) {

            $mese = sprintf("%02d", $i);
            $array = array();
            
            $data = $anno . " - " . $arrayMesi[$mese];
            $dataDB = $anno . "-" . $mese;
            
            $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and DataRegistrazione LIKE '$dataDB%' ORDER BY RagioneSociale";
            $array['iscritti'] = $db->NumRows($queryImprese);
            $array['data'] = $data;


            $arrayImp = array();
            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                $arrayImp[] = $rowsImprese ['RagioneSociale'] . "<br>";
            }
            $array['imprese'] = $arrayImp;
            $arrayIn[] = $array;
        }
    }

    $ordina = array();
    foreach ($arrayIn as $key => $row) {
        $ordina[$key] = $row[$campo];
    }
    $tipoOrdinamento = SORT_DESC;
    if (strcmp($ord, "ASC") == 0) {
        $tipoOrdinamento = SORT_ASC;
    }

    array_multisort($ordina, $tipoOrdinamento, $arrayIn);


    $tag .= '<dl class="tsc_accordion2" style="width:100%;margin-left: -10px" >';
    foreach ($arrayIn as $array) {
        $tag .= '<dt >' . $array['data'] . '<span style="float:right;">' . $array['iscritti'] . '</span></dt>';
        $tag .= '<dd >
        <p>';
        foreach ($array['imprese'] AS $rowsImprese) {
            if (strcmp($rowsImprese, "") != 0)
                $tag .= $rowsImprese . "<br>";
        }
        $tag .= '    </p>
    </dd>';
    }
    $tag .='</dl>';
}


echo $tag;

}
?>