<script language="JavaScript" type="text/javascript">
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi(); }   // esc
  	});
</script>


<?php
//include "configuration.inc";
$idImpresaAllow = ($_POST['idimp']);
$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $impresaAllow){

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
$Lang = $_POST['lang'];
$IdImpresa = $_POST['idimp'];
$NRete = $_POST['NRete'];
$action = $_POST['action'];
$rootDoc = selectRootDocReti($NRete) . "/";

require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
$db = new DataBase();

$IdRete = 0;
if ($action == "UPD"){
	if ($NRete != 'Nome Mancante'){
		$rowR = $db->GetRow("SELECT * FROM EXPO_T_RetiImpresa WHERE NomeRete = '$NRete'",null,null,"T_Reti ajax ln.35");
		$IdRete = $rowR['Id'];
	} else {
		$qry = "SELECT EXPO_T_RetiImpresa.Id, EXPO_T_RetiImpresa.NomeRete, EXPO_TJ_Imprese_RetiImpresa.Id_Impresa
		FROM EXPO_T_RetiImpresa INNER JOIN EXPO_TJ_Imprese_RetiImpresa ON EXPO_T_RetiImpresa.Id = EXPO_TJ_Imprese_RetiImpresa.Id_Rete
		WHERE EXPO_T_RetiImpresa.NomeRete='Nome Mancante' AND EXPO_TJ_Imprese_RetiImpresa.Id_Impresa=$IdImpresa";		
		$rowR = $db->GetRow($qry,'Id',null,"T_Reti ajax ln.41");
		$IdRete = $rowR['Id'];
		$rowR = $db->GetRow("SELECT * FROM EXPO_T_RetiImpresa WHERE Id = '$IdRete'",null,null,"T_Reti ajax ln.43");
	}
	
	$IsGR = $db->GetRow("SELECT * FROM EXPO_TJ_Imprese_RetiImpresa WHERE Id_Rete = $IdRete AND Id_Impresa = $IdImpresa",'IsCapogruppo',null,null,"T_Reti ajax ln.46");
	/*echo "IDRete: ".$IdRete;
	echo "IsGR :".$IsGR;
	echo "Qry: ".$qry;
	exit;*/
}

$IsAdmin = 'N';
$qry = "SELECT min(Id) as Id FROM EXPO_TJ_Imprese_RetiImpresa where Id_Rete=$IdRete";
$idMin = $db->GetRow($qry,'Id',null,'partecipante_EXPO_T_RetiImprese.inc Ln 55');
$IdTJImp = $db->GetRow("SELECT * FROM EXPO_TJ_Imprese_RetiImpresa WHERE Id_Rete = $IdRete AND Id_Impresa = $IdImpresa",'Id',null,"T_Reti ajax ln.56");
if ($idMin == $IdTJImp){
	$IsAdmin = 'Y';
}
?>
<script language="javascript">
$(document).ready(function() {
	//collapsible management
	$.cookie('navReti', null);
	$('.collapsibleR').collapsible({
		defaultOpen: '',
		cookieName: 'navReti',
		speed: 'slow'
	});

    $("input:file").change(function() {
		//alert('Prova');
	    var fileName = $(this).val();

	    fileName = fileName.split('\\')
	    $("#testofilename" + $(this).attr('name')).html(fileName[fileName.length - 1]);
    });

    $('textarea[maxlength]').keyup(function() {
        var max = parseInt($(this).attr('maxlength'));
        if ($(this).val().length > max) {
            $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
        }

        $(this).parent().find('.charsRemaining').html('' + (max - $(this).val().length) + ' caratteri rimanenti');
    });
});
</script>
<?php 

//$idPage = $_GET['Id_VMenu'];
if ($action == "ADD"){
	//Aggiunge Rete
	$formP.= '<h4>Nuova Rete</h4>
			<div class="containerData">
				<table width="100%">
					<tbody>
		<tr><td colspan="2"><strong>Nome Rete: </strong><input type="text" id="NomeRete" name="NomeRete" size="35" value="'.$NRete.'"></td></tr>';
	$formDescrReti = '<tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2"><strong>Descrizione Rete</strong></td></tr>
						<tr><td id="descrizioneMultiLinguaR">';
						$query = "SELECT Id,Nome,Lang,Defoult FROM EXPO_T_Lingue_Sito ORDER BY Id";
						foreach ($db->GetRows($query,null,"T_Reti ajax ln.102") AS $rows) {
                                if ($rows['Defoult'] == 1) {
                                	$formDescrReti .= '<div class="collapsibleR" id="section'.$rows['Id'].'">
								<span></span>'.constant($rows['Nome']).'</div>
							<div class="container">
								<div class="content">
									<textarea rows="10" cols="35" id="descrizioneR"
										onkeyup="contacaratteriR(\'\')" maxlength="1000"
										name="descrizioneR"></textarea>
									<p id="charsRemainingR">max 1000 caratteri disponibili</p>
								</div>
							</div>';
                                } else {
                                    $l = $rows['Lang'];
                                    	
                                    $formDescrReti .='<div class="collapsibleR" id="section'.$rows['Id'].'">
								<span></span>'.constant($rows['Nome']).'</div>
							<div class="container">
								<div class="content">
									<textarea rows="10" cols="35" maxlength="1000"
										onkeyup="contacaratteriR(\''.$l.'\')"
										id="descrizioneR'.$l.'"
										name="descrizioneR'.$rows['Lang'].'"></textarea>
									<p id="charsRemainingR'.$l.'">max 1000 caratteri
										disponibili</p>
								</div>
							</div>';
                                }
                            }
                            $formDescrReti .= '</td>
					</tr>';
    $formP .= $formDescrReti;                         
	$formP .= '<tr><td colspan="2"><br> '. _CONTATTO_RETE_ .' <input
							type="text" id="ContattoRete" name="ContattoRete" size="30"
							maxlength="500" /></td></tr>
			<tr><td>'. _ALLEGA_DOC_ .' <a href="#" class="tooltip"
							draggable="false"> <img
								src="tmpl/expo2015/catalogo/images/info.png" name="iconaInfo" />
								<span> <img class="callout" draggable="false"
									src="tmpl/expo2015/catalogo/images/callout.gif" />'. _NOTE_DESCRETE_.'
							</span>
						</a></td>
                        <td align="left">'.getFileInput('descrizR', 'descriz', $rootDoc . $IdRete, $IdRete).'</td></tr>
            <tr><td colspan="2"><br> '. _ALLEGA_LINK_ .' <input
							type="text" id="linkRete" name="linkRete" size="30"
							maxlength="500" /></td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2"><input	draggable="false" type="button"
			onclick="javascript:salvaRete(document.form_moduli, \'index.phtml?Id_VMenu=301&amp;azione=ADR\')"
			value="Salva Rete" /></td></tr>
			</tbody></table></div>';
} elseif ($action == "UPD"){
	//Recupera lista Imprese collegate ad una rete selezionata
	$qryLsImp = "SELECT EXPO_TJ_Imprese_RetiImpresa.Id_Impresa, EXPO_TJ_Imprese_RetiImpresa.Id_Rete, EXPO_TJ_Imprese_RetiImpresa.IsCapogruppo, EXPO_T_Imprese.RagioneSociale
	FROM EXPO_T_Imprese INNER JOIN EXPO_TJ_Imprese_RetiImpresa ON EXPO_T_Imprese.Id = EXPO_TJ_Imprese_RetiImpresa.Id_Impresa
	WHERE Id_Rete = $IdRete AND Id_Impresa <> $IdImpresa";
	//echo "Qry :".$qryLsImp;
	$divListaImp = '';
	if ($db->NumRows($qryLsImp,null,"T_Reti ajax ln.160")>0 && $NRete != 'Nome Mancante'){
		$divListaImp = '<tr><td id="ImpreseXRete" colspan="2">';
		foreach ($db->GetRows($qryLsImp,null,"T_Reti ajax ln.162") AS $rowsLsImp) {
			$divListaImp .= '<div class="divImpReti">'.$rowsLsImp['RagioneSociale'];
			if ($IsAdmin == 'Y'){
				$divListaImp .= '<img src="/images/cancel.png" name="imgDel"
									style="cursor: pointer"
									onclick="gestioneImpXReti(\'DEL\', \''.$rowsLsImp['Id_Impresa'].'\', \''.$rowsLsImp['Id_Rete'].'\', \''.$IdImpresa.'\')">';
			}
			$divListaImp .=	'</div>';
		}
		$divListaImp .= '</td></tr><tr><td colspan="2">&nbsp;</td></tr>';
	}
	
	$trCercaImpXRete = '';
	$trCercaImpXRete .= '<tr><td colspan="2">&nbsp;</td></tr>';
	$trCercaImpXRete .= '<tr><td colspan="2"><strong>Altre Imprese nella Rete</strong></td></tr>';
	/*$trCercaImpXRete .= '<tr><td>';
	
	$autocompleteSql = "SELECT EXPO_T_Imprese.RagioneSociale FROM EXPO_T_Imprese LEFT JOIN (SELECT Id, Id_Impresa, Id_Rete FROM EXPO_TJ_Imprese_RetiImpresa WHERE Id_Rete=$IdRete) 
						AS Tr ON EXPO_T_Imprese.Id = Tr.Id_Impresa WHERE Tr.Id_Impresa Is Null GROUP BY EXPO_T_Imprese.RagioneSociale ORDER BY 
						EXPO_T_Imprese.RagioneSociale";
	$autocompleteResult = $db->GetRows($autocompleteSql);
	
	$trCercaImpXRete .= '<script>' . "\n";
	$trCercaImpXRete .= '  $(function() {' . "\n";
	$trCercaImpXRete .= '    var availableTags = [' . "\n";
	$i=1;
	foreach ($autocompleteResult as $element) {
		if ($i == 1)
			$trCercaImpXRete .=  "'".str_replace("'", "\'", html_entity_decode($element['RagioneSociale']))."'";
		else
			$trCercaImpXRete .=  ",'" . str_replace("'", "\'", html_entity_decode($element['RagioneSociale']))."'";
		$i++;
	}
	$trCercaImpXRete .=  '        ];' . "\n";
	
	$trCercaImpXRete .=  '$( "#searcCodeI" ).autocomplete({
	appendTo: "#risultatiI",
    open: function() {
        var position = $("#risultatiI").position(),
            left = position.left, top = position.top;
	
        $("#risultatiI > ul").css({left: left + 20 + "px",
                                top: top + 4 + "px" });
	
    },
	source: function ( request, response ) {
		var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
		response(
				$.grep(
						availableTags, function( item ){
							return matcher.test( item );
						}
						)
				);
	}
}); ';
	$trCercaImpXRete .=  '  });' . "\n";
	$trCercaImpXRete .=  '  </script>';
	
	$trCercaImpXRete .=  '<input id="searcCodeI" type="search" name="code" size="35">
							<img width="18" onclick="gestioneImpXReti(\'ADD\', \'\', \''.$IdRete.'\', \''.$IdImpresa.'\');" style="cursor: pointer; padding-left:10px" name="imgMod" 
								title="Aggiungi questa impresa alla Rete" src="/images/expo2015/gray-add-to-list-icon.png">
							<div id="risultatiI"></div>
							
							</td>
						</tr>';*/
	
	//Modifica dati della Rete
	$formP.= '<h4>Modifica Rete: '.$NRete.'</h4>
			<div class="containerData">
				<input type="hidden" id="IdRete" name="IdRete" value="'.$IdRete.'">';
				if ($NRete != 'Nome Mancante'){
					$formP.= '<input type="hidden" id="NomeRete" name="NomeRete" value="'.$NRete.'">';
				} 
				$formP.= '<table width="100%">
					<tbody>';
				if ($NRete == 'Nome Mancante'){
					$formP.= '<tr><td colspan="2"><strong> '. _NOME_RETE_ .' </strong>&nbsp;<input type="text" id="NomeRete" name="NomeRete" value="'.$NRete.'">
						</td></tr>';
				}
				$formP .= '<tr><td colspan="2"><input type="checkbox" id="isCapogruppo" name="isCapogruppo" value="Y" '.getCheck($IsGR).'>';
					
                $formP .='<label for="isCapogruppo" ><strong>'. _R_CAPOGR_.'</strong></label>
                 <a href="#" class="tooltip" draggable="false" >
                     <img src="tmpl/expo2015/catalogo/images/info.png" name="iconaInfo" />
						<span>
                        	<img class="callout" draggable="false"  src="tmpl/expo2015/catalogo/images/callout.gif" />
                                            '. _NOTE_CAPOGR_.'
                                        </span>
                                    </a>
                            </td>
                        </tr>';
		//<tr><td colspan="2"><strong>Nome Rete: '.$NRete.'</strong></td></tr>';
$formDescrReti = '<tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2"><strong>Descrizione Rete</strong></td></tr>
						<tr><td id="descrizioneMultiLinguaR">';
$query = "SELECT Id,Nome,Lang,Defoult FROM EXPO_T_Lingue_Sito ORDER BY Id";
foreach ($db->GetRows($query,null,"T_Reti ajax ln.258") AS $rows) {
	if ($rows['Defoult'] == 1) {
		$formDescrReti .= '<div class="collapsibleR" id="section'.$rows['Id'].'">
								<span></span>'.constant($rows['Nome']).'</div>
							<div class="container">
								<div class="content">
									<textarea rows="10" cols="35" id="descrizioneR"
										onkeyup="contacaratteriR(\'\')" maxlength="1000"
										name="descrizioneR">'.html_entity_decode($rowR['Descrizione']).'</textarea>
									<p id="charsRemainingR">max 1000 caratteri disponibili</p>
								</div>
							</div>';
	} else {
		$l = $rows['Lang'];
		if ($l == "EN")
			$descr = html_entity_decode($rowR['Descrizione_AL']);
		else 
			$descr = html_entity_decode($rowR['Descrizione_AL2']);
		
		$formDescrReti .='<div class="collapsibleR" id="section'.$rows['Id'].'">
								<span></span>'.constant($rows['Nome']).'</div>
							<div class="container">
								<div class="content">
									<textarea rows="10" cols="35" maxlength="1000"
										onkeyup="contacaratteriR(\''.$l.'\')"
										id="descrizioneR'.$l.'"
										name="descrizioneR'.$rows['Lang'].'">'.$descr.'</textarea>
									<p id="charsRemainingR'.$l.'">max 1000 caratteri
										disponibili</p>
								</div>
							</div>';
	}
}
$formDescrReti .= '</td>
					</tr>';
$formP .= $formDescrReti;
$formP .= $trCercaImpXRete;
$formP .= $divListaImp;
$formP .= '	<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td colspan="2"><br><strong> '. _CONTATTO_RETE_ .' </strong><input
							type="text" id="ContattoRete" name="ContattoRete" size="30"
							maxlength="500" value="'.$rowR['Contatto'].'"/></td></tr>
            <tr><td>'. _ALLEGA_DOC_ .' <a href="#" class="tooltip"
							draggable="false"> <img
								src="tmpl/expo2015/catalogo/images/info.png" name="iconaInfo" />
								<span> <img class="callout" draggable="false"
									src="tmpl/expo2015/catalogo/images/callout.gif" />'. _NOTE_DESCRETE_.'
							</span>
						</a></td>
                        <td align="left">'.getFileInput('descrizR', 'descriz', $rootDoc . $IdRete, $IdRete).'</td></tr>
			<tr><td colspan="2"><br> '. _ALLEGA_LINK_ .' <input
							type="text" id="linkRete" name="linkRete" size="30"
							maxlength="500" value="'.$rowR['SitoWeb'].'"/></td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2"><input	draggable="false" type="button"
			onclick="javascript:salvaRete(document.form_moduli, \'index.phtml?Id_VMenu=301&amp;azione=UPR\')"
			value="Salva Rete" /></td></tr>
			</tbody></table></div>';
}
echo $formP;
}
//}
else{
?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudi()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>