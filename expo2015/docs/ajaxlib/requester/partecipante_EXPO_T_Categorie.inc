<?php
$idImpresaAllow = ($_POST['Id']);
$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $impresaAllow){

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
$Lang = $_POST['lang'];
$IdImpresa = $_POST['Id'];
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
$db = new DataBase();
?>

<script language="javascript">
    $(document).ready(function() {
        //collapsible management
        $.cookie('navCat', null);
        $('.collapsibleCat').collapsible({
            defaultOpen: '',
            cookieName: 'navCat',
            speed: 'slow'
        });
    });
</script>


<?php
$query = "SELECT Id,
                         Descrizione,
                         Stato_Record,
                         Id_Categoria
                    FROM EXPO_Tlk_Categorie
                   WHERE LENGTH(Id_Categoria) = 2";
foreach ($db->GetRows($query) AS $rows) {
    ?>
    <div class="alberoCategorie">
        <div class="collapsibleCat" id="section<?php echo $rows['Id']; ?>">
            <span></span><?php
            if (strcmp($rows['Id_Categoria'], '02') == 0) {
                ?>

                <?php echo html_entity_decode($rows['Descrizione']); ?>



                <?php
            } else {
                echo html_entity_decode($rows['Descrizione']);
            }
            ?>
        </div>
        <div class="container">
            <div class="content">
                <?php
                $query2 = "SELECT Id,
                                       Descrizione,
                                       Stato_Record,
                                       Id_Categoria
                                  FROM EXPO_Tlk_Categorie
                                 WHERE SUBSTRING(Id_Categoria,1,2) = '" . $rows['Id_Categoria'] . "'" .
                        "AND LENGTH(Id_Categoria) = 5";
                foreach ($db->GetRows($query2) AS $rows2) {
                    ?>
                    <div class="alberoCategorie">
                        <div class="collapsibleCat" id="section<?php echo $rows2['Id']; ?>"><span></span><?php echo html_entity_decode($rows2['Descrizione']); ?></div>
                        <div class="container">  
                            <div class="content">
                                <?php
                                $query3 = "SELECT EXPO_Tlk_Categorie.Id,
                                                                  Descrizione,
                                                                  Stato_Record,
                                                                  Id_Categoria,FlagNote,Note,
                                                                  COALESCE(EXPO_TJ_Imprese_Categorie.Id,-1) AS IdJoin
                                                             FROM EXPO_Tlk_Categorie LEFT OUTER JOIN EXPO_TJ_Imprese_Categorie 
                                                                                                  ON EXPO_Tlk_Categorie.Id = EXPO_TJ_Imprese_Categorie.IdCategoria
                                                                                                 AND EXPO_TJ_Imprese_Categorie.IdImpresa = " . $IdImpresa . "                                                                                      
                                                     WHERE SUBSTRING(Id_Categoria,1,5) = '" . $rows2['Id_Categoria'] . "'" .
                                        "AND LENGTH(Id_Categoria) = 8
                                                           order by Id_Categoria ";


                                foreach ($db->GetRows($query3,null,"Ajax - partecipante_expo_t_categorie - line 79") AS $rows3) {
									$infoNota = '';
									if (strcmp ($rows3['FlagNote'],'Y') == 0) {

										$nota = _DISCLAIMER_CATEGORIE_DEFAULT_;
										if ((strcmp ($rows3['Note'],'_DEFAULT_') != 0)){
											$nota = $rows3['Note'];
										}
											
										$infoNota = '<a href="#" class="tooltip" draggable="false">
									        					<img src="tmpl/expo2015/catalogo/images/info1.png" name="iconaInfo" />
																<span> <img class="callout" draggable="false" src="tmpl/expo2015/catalogo/images/callout.gif" />'
												. $nota .
												'</span>
															</a>';
									}		
							

                                    $query4 = "SELECT Id,
                                                                     IsIscritta,
                                                                     IdCategoria
                                                                FROM EXPO_TJ_Imprese_Categorie
                                                                
                                                               WHERE IdImpresa = '" . $IdImpresa . "'
                                                                 AND IdCategoria = " . $rows3['Id'] . "
                                                                      ";

                                    $rows4 = $db->GetRow($query4);
                                    ?>
                                    <p >
                                        <input type="checkbox" id="categoria<?php echo $rows3['Id']; ?>" name="categoria<?php echo $rows3['Id']; ?>" value="Y" <?php echo getCheck($rows4['IsIscritta']); ?> <?php echo getCheckCategorie($rows3['Stato_Record'], $rows3['IdJoin']); ?>  onclick="javascript:controllaCategoria(this, '<?php echo $rows4['Id']; ?>', '<?php echo $IdImpresa; ?>', '<?php echo $rows4['IdCategoria']; ?>', '<?php echo $Lang ?>')">
                                          <!--<input type="checkbox" id="categoria<?php echo $rows3['Id']; ?>" name="categoria<?php echo $rows3['Id']; ?>"/>-->
                                        <label   for="categoria<?php echo $rows3['Id']; ?>" <?php echo getColorNameCategorie($rows3['Stato_Record'], $rows3['IdJoin']); ?> ><?php echo $rows3['Descrizione']; echo $infoNota;?></label>
                                    </p>
                                    <?php
                                }
                                ?> 
                            </div>        
                        </div>                                                            
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>  
    </div>
<?php } 

}
?>
