<?php

include "configuration.inc";
include 'autpathAjax.inc';
require_once($PROGETTO . "/view/languages/" . $_REQUEST['lang'] . ".inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();

$idImp = $_REQUEST['idImp'];
$idPar = $_REQUEST['idPar'];

$options = '<option value="">'._SELEZIONA_.'</option>';
$query = "SELECT Id,Nome
            FROM EXPO_T_Prodotti 
            WHERE IdImpresa = '$idImp'
            ORDER BY Nome";
foreach ($db->GetRows($query) AS $rows) {
    $idProd = $rows['Id'];
    
     $query = "SELECT Nome,Stato FROM EXPO_T_Bacheca AS T
        LEFT JOIN EXPO_T_Bacheca_Trattative AS T2 ON T.Id = T2.IdBacheca
        RIGHT JOIN EXPO_T_Prodotti AS T3 ON T.IdProdotto = T3.Id
        WHERE T.IdProdotto = '$idProd' AND T.IdPartecipante = '$idPar'";

    $nomeProd = $db->GetRow($query, "Nome");
    $statoProd = $db->GetRow($query, "Stato");
    if (strcmp($nomeProd, "") == 0 || strcmp($statoProd, "C") == 0) {
        $options .= '<option value="'.$idProd.'">'.$rows['Nome'].'</option>';
    }
    
    
}

echo $options;
?>