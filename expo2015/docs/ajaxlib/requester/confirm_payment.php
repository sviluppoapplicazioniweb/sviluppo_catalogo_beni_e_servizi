<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 class XML2Array {

    private static $xml = null;
	private static $encoding = 'UTF-8';

    /**
     * Initialize the root XML node [optional]
     * @param $version
     * @param $encoding
     * @param $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DOMDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
		self::$encoding = $encoding;
    }

    /**
     * Convert an XML to Array
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DOMDocument
     */
    public static function &createArray($input_xml) {
        $xml = self::getXMLRoot();
		if(is_string($input_xml)) {
			$parsed = @$xml->loadXML($input_xml);
			if(!$parsed) {
                            return false;
//				throw new Exception('[XML2Array] Error parsing the XML string.');
			}
		} else {
			if(get_class($input_xml) != 'DOMDocument') {
				return false;
//                                throw new Exception('[XML2Array] The input XML object should be of type: DOMDocument.');
			}
			$xml = self::$xml = $input_xml;
		}
		$array[$xml->documentElement->tagName] = self::convert($xml->documentElement);
        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $array;
    }

    /**
     * Convert an Array to XML
     * @param mixed $node - XML as a string or as an object of DOMDocument
     * @return mixed
     */
    private static function &convert($node) {
		$output = array();

		switch ($node->nodeType) {
			case XML_CDATA_SECTION_NODE:
				$output['@cdata'] = trim($node->textContent);
				break;

			case XML_TEXT_NODE:
				$output = trim($node->textContent);
				break;

			case XML_ELEMENT_NODE:

				// for each child node, call the covert function recursively
				for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
					$child = $node->childNodes->item($i);
					$v = self::convert($child);
					if(isset($child->tagName)) {
						$t = $child->tagName;

						// assume more nodes of same kind are coming
						if(!isset($output[$t])) {
							$output[$t] = array();
						}
						$output[$t][] = $v;
					} else {
						//check if it is not an empty text node
						if($v !== '') {
							$output = $v;
						}
					}
				}

				if(is_array($output)) {
					// if only one node of its kind, assign it directly instead if array($value);
					foreach ($output as $t => $v) {
						if(is_array($v) && count($v)==1) {
							$output[$t] = $v[0];
						}
					}
					if(empty($output)) {
						//for empty nodes
						$output = '';
					}
				}

				// loop through the attributes and collect them
				if($node->attributes->length) {
					$a = array();
					foreach($node->attributes as $attrName => $attrNode) {
						$a[$attrName] = (string) $attrNode->value;
					}
					// if its an leaf node, store the value in @value instead of directly storing it.
					if(!is_array($output)) {
						$output = array('@value' => $output);
					}
					$output['@attributes'] = $a;
				}
				break;
		}
		return $output;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }
}
//
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$utypeArrayAllow = array(96);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();
//set POST variables
$url="https://test.monetaonline.it/monetaweb/payment/2/xml"; //url test
//$url = 'https://www.monetaonline.it/monetaweb/payment/2/xml'; // url produzione

$description="Pagamento EXPO2015";
$customField="";
$amount="50";
$fields = array(
    'authorizationcode' => urlencode($_POST['authorizationcode']),
    'paymentid' => urlencode($_POST['paymentid']),
    'merchantorderid' => urlencode($_POST['merchantorderid'])

);
$fields_string='';
//url-ify the data for the POST
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
$fields_string.='operationType=confirm&currencyCode=978&id='.$idCliente."&password=".$passCliente;
rtrim($fields_string, '&');

//open connection
$ch = curl_init();
 
//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch) or die (curl_error($ch));
 
$array = XML2Array::createArray($result);


if(isSet($array['response']['result']) and $array['response']['result']=='APPROVED'){
$responsecode=$array['response']['responsecode'];
$sql="INSERT into EXPO_TJ_Imprese_Pagamenti
    (Id,IdImpresa,Result,MerchantOrderId,PaymentId,Description,Authorizationcode,DataRichiesta)
    values((SELECT COALESCE(MAX(A.Id),0)+1 as id FROM EXPO_TJ_Imprese_Pagamenti as A),
    '".$_POST['IdImpresa']."',
    '".$array['response']['result']."',
    '".$array['response']['merchantorderid']."',
    '".$array['response']['paymentid']."',
    '".$array['response']['description']."',
    '".$array['response']['authorizationcode']."',
    '".date("Y-m-d H:i",time())."' 
        )";
$db->Query($sql);
$html="ok";
$html="
        <table>
            <tr  class='success'>
                <td>Stato</td>
                <td> ".$array['response']['result']." </td>
            </tr>
            <tr  class='success'>
                <td>Codice autorizzazione</td>
                <td id='authorizationcode'> ".$array['response']['authorizationcode']." </td>
            </tr>
            <tr  class='success'>
                <td>Codice pagamento</td>
                <td id='paymentid'> ".$array['response']['paymentid']." </td>
            </tr>
            <tr  class='success'>
                <td>Codice transazione</td>
                <td id='merchantorderid'>".$array['response']['merchantorderid']."</td>
            </tr>
            <tr  class='success'>
                <td></td>
                <td><span onclick='confermaPagamento()'>Conferma</span></td>
            </tr>

        </table>
        ";
//$html="
//        <table>
//            <tr  class='success'>
//                <td>Stato</td>
//                <td> ".$array['response']['result']." </td>
//            </tr>
//            <tr  class='success'>
//                <td>Codice autorizzazione</td>
//                <td> ".$array['response']['authorizationcode']." </td>
//            </tr>
//            <tr  class='success'>
//                <td>Codice pagamento</td>
//                <td> ".$array['response']['paymentid']." </td>
//            </tr>
//            <tr  class='success'>
//                <td>Codice transazione</td>
//                <td> ".$array['response']['merchantorderid']." </td>
//            </tr>
//            <tr  class='success'>
//                <td>Descrizione</td>
//                <td> ".$array['response']['description']." </td>
//            </tr>
//
//        </table>
//        ";
}else{
    $sql="INSERT into EXPO_TJ_Imprese_Pagamenti
    (Id,IdImpresa,Result,MerchantOrderId,PaymentId,Description,Authorizationcode,DataRichiesta)
    values((SELECT COALESCE(MAX(A.Id),0)+1 as id FROM EXPO_TJ_Imprese_Pagamenti as A),
    '".$_POST['IdImpresa']."',
    '".$array['error']['errorcode']."',
    '',
    '',
    '".$array['error']['errormessage']."',
    '',
    '".date("Y-m-d H:i",time())."'
        )";
    $db->Query($sql);
    $html="
        <table>
            <tr  class='error'>
                <td>Codice errore</td>
                <td> ".$array['error']['errorcode']." </td>
            </tr>
            <tr  class='error'>
                <td>Messaggio errore</td>
                <td> ".$array['error']['errormessage']." </td>
            </tr>
        </table>
        ";
}

//close connection
curl_close($ch);
ob_start;ob_clean;
echo $html;
exit;

}
//}
?>
