<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$idImpresaAllow = ($_POST['Id']);
$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $impresaAllow){

	
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();
$Lang = $_POST['lang'];

require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
$IdImpresa = settaVar($_POST, 'Id', '');
$query = "SELECT * FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'";
$row = $db->GetRow($query, null, null,"Ajax - partecipante _Expo_t_pagamento - line 16");


$rootDoc = selectRootDoc($db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale", null, "Ajax - partecipante Expo_t_Pagamento - Root doc")) . "/";

ob_start();
ob_clean();
 ?>

<script type="text/javascript" src="/jquery/js/paymentJs.js"></script>
<input type="hidden" id="IdImpresa" value="<?php echo $IdImpresa;?>">
<div class="divClose">
        <input type="button" onclick="chiudi()" id="close" value="X <?php echo _CHIUDI_; ?>" />
    </div>
<h1>Pagamento</h1>
<table width="100%">
    <tr>
        <td width="50%">
            <table>
                <tr>
                    <td>
                        Numero Carta
                    </td>
                    <td>
                        <input type="text" id="numeroCarta" maxlength="16">
                    </td>
                </tr>
                <tr>
                    <td>
                        Scadenza carta
                    </td>
                    <td>
                        <select id="meseScadenzaCarta">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select> /
                        <select id="annoScadenzaCarta">
                            <?php
                            $j=date('Y');
                            for($i=date('Y');$i<$j+10;$i++){
                                ?>
                                     <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                            }
                            ?>


                        </select>

                    </td>
                </tr>
                <tr>
                    <td>
                        Codice cvv2
                    </td>
                    <td>
                        <input type="text" id="cvv2Carta" maxlength="3" size="2">
                    </td>
                </tr>
                <tr>
                    <td>
                        Intestata a
                    </td>
                    <td>
                        <input type="text" id="intestatarioCarta">
                    </td>
                </tr>
                <tr>
                    <td>
                        E-Mail intestatario
                    </td>
                    <td>
                        <input type="text" id="intestatarioEmail">
                    </td>
                </tr>
                
            </table>
        </td>
        <td> <p  id="paga" style="cursor: pointer"onclick="sendInfoPosVirtuale()">Paga</p>
            <div id="paymentDiv">

                <img style="display: none" id="loadingPay" src="tmpl/expo2015/catalogo/images/ajax-loader.gif">
             
            </div>
        </td>
    </tr>
                        
</table>
 
<?php }
//}?>