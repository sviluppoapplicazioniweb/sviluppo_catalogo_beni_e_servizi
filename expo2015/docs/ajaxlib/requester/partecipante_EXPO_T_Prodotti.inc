<script language="JavaScript" type="text/javascript">
    $(function() {
        $("#prducts").tabs();
    });
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi(); }   // esc
  	});
</script>
<?php
//include "configuration.inc";

$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow){

$Lang = $_REQUEST['lang'];
$LangApp = "";
if (strcmp($Lang, "IT") != 0) {
    $LangApp = $Lang;
}

require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();

//$view = settaVar($_REQUEST, "bview", "");
$IdProdotto = $_REQUEST['Id'];
$queryPrd = "SELECT * FROM EXPO_T_Prodotti WHERE Id = $IdProdotto";

$IdImpresa = $db->GetRow($queryPrd, 'IdImpresa');
$IdTipo = "P" . $IdProdotto;

$ordine = settaVar($_REQUEST, 'Ordine', '');
$studio = settaVar($_REQUEST, 'IsStudio', '');

$rootDoc = selectRootDoc($db->GetRow("SELECT RagioneSociale FROM EXPO_T_Imprese WHERE Id = '$IdImpresa'", "RagioneSociale")) . "/";

$tag = "Nome" . $LangApp;
$testoNome = $_POST[$tag];

/*
  $testoNome = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
  if (strcmp($testoNome, "") == 0)
  $testoNome = $db->GetRow($queryPrd, 'Nome');
 */
$tag = "descrizione" . $LangApp;
$testoDescrizione = $_POST[$tag];
/*
  $testoDescrizione = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
  if (strcmp($testoDescrizione, "") == 0)
  $testoDescrizione = $db->GetRow($queryPrd, 'Descrizione');
 */
$tag = "certificazione" . $LangApp;
$testoCertificazione = $_POST[$tag];
/*
  $testoCertificazione = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
  if (strcmp($testoCertificazione, "") == 0)
  $testoCertificazione = $db->GetRow($queryPrd, 'Certificazione');
 */

$tag = "aspetti" . $LangApp;
$testoAspetti = $_POST[$tag];
/*
  $testoAspetti = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
  if (strcmp($testoAspetti, "") == 0)
  $testoAspetti = $db->GetRow($queryPrd, 'Aspetti');
 */


$isSiexpo = $_POST['isSiexpo'];
//$isSiexpo = $db->GetRow($queryPrd, 'IsSiexpo');

$tag = "brevetti" . $LangApp;
$testoBrevetti = $_POST[$tag];
/*
  $testoBrevetti = $db->GetRow("SELECT Testo FROM EXPO_T_Traduzioni WHERE Tag = '$tag' AND IdTipo = '$IdTipo' AND Lang = '$Lang'", "Testo");
  if (strcmp($testoBrevetti, "") == 0)
  $testoBrevetti = $db->GetRow($queryPrd, 'Brevetti');
 */

$descrizione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/prodotto", _PRODOTTO_FILE_), getAllegati('link', $_POST['prodottoL']));
$certificazione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/certificazione", _CERTIFICAZIONE_FILE_), getAllegati('link', $_POST['certificazioneL']));
$aspetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/aspetti", _ASPETTI_FILE_), getAllegati('link', $_POST['aspettiL']));
$brevetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/brevetti", _BREVETTI_FILE_), getAllegati('link', $_POST['brevettiL']));
/*
  $descrizione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/prodotto", _PRODOTTO_FILE_), getAllegati('link', $db->GetRow($queryPrd, 'ProdottoLink')));
  $certificazione = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/certificazione", _CERTIFICAZIONE_FILE_), getAllegati('link', $db->GetRow($queryPrd, 'CertificazioneLink')));
  $aspetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/aspetti", _ASPETTI_FILE_), getAllegati('link', $db->GetRow($queryPrd, 'AspettiLink')));
  $brevetti = array(getAllegatiFiles('file', $rootDoc . $IdImpresa . "/prodotti/" . $IdProdotto . "/brevetti", _BREVETTI_FILE_), getAllegati('link', $db->GetRow($queryPrd, 'BrevettiLink')));
 */
?>
<div class="contentHeader">
    <div class="divClose">
        <a draggable="false" href="javascript:chiudi()" >X</a>   
    </div>
    <div id="header-lang">
        <?php include_once $PROGETTO . "/view/lib/languages_prod.inc"; ?>
    </div>
</div>
<div class="divBoxAnteprima">

    <div id="prducts">
        <ul>
            <li><a draggable="false" href="#prducts-1"><?php echo _DATI_GENERALI_T_; ?></a></li>
            <li><a draggable="false" href="#prducts-2"><?php echo _C_MERC_T_; ?></a></li>
            <?php if (viewTabProd($testoCertificazione, $certificazione)) { ?><li><a href="#prducts-3"><?php echo _CERTIFICAZIONI_T_; ?></a></li><?php } ?>
            <?php if (viewTabProd($testoAspetti, $aspetti)) { ?><li><a draggable="false" href="#prducts-4"><?php echo _ASPETTI_T_; ?></a></li><?php } ?>
            <?php if ($ordine == 0) {?>
            <?php if (viewTabProd($testoBrevetti, $brevetti)) { ?><li><a draggable="false" href="#prducts-5"><?php echo _BREVETTI_T_; ?></a></li><?php } ?>
            <?php }?>
        </ul>
        <div id="prducts-1" style="width: 100%;">
            <h4><?php echo _NOME_; ?>:</h4> 
            <?php echo $testoNome; ?>

            <?php if (viewTabProd($testoDescrizione, $descrizione)) { ?>
                <h4><?php echo _DESCRIZIONE_; ?>:</h4>
                <?php echo $testoDescrizione; ?>

                <br><br>
                <?php echo $descrizione[0]; ?>
                <?php echo $descrizione[1]; ?>
            <?php } ?>
        </div>
        <div id="prducts-2" style="width: 100%;">
            <h4><?php echo _C_MERC_; ?></h4>
            <ul>  
                <?php
//                $query = "SELECT T.Id AS Id, T.DescEstesa AS DescEstesa,DescEstesa_AL as DescEstesa_AL
//            ,DescEstesa_AL2 as DescEstesa_AL2
//                                  FROM EXPO_Tlk_Categorie AS T
//                                  RIGHT JOIN EXPO_TJ_Prodotti_Categorie AS TJ ON T.Id = TJ.IdCategoria
//                                  AND TJ.IsIscritta =  'Y'
//                                AND TJ.IdProdotto = '$IdProdotto'
//                                WHERE T.Id IS NOT NULL 
//                                ORDER BY DescEstesa";
                $query = "SELECT T.Id AS Id, T.DescEstesa AS DescEstesa,DescEstesa_AL as DescEstesa_AL,T.FlagNote,T.Note,T.Note_AL
                        ,DescEstesa_AL2 as DescEstesa_AL2
                                  FROM EXPO_TJ_Prodotti_Categorie AS TJ
                                  LEFT JOIN EXPO_Tlk_Categorie AS T ON T.Id = TJ.IdCategoria
                                 
                                WHERE T.Id IS NOT NULL  AND TJ.IsIscritta =  'Y'
                                AND TJ.IdProdotto = '$IdProdotto'
                                ORDER BY DescEstesa";
                foreach ($db->GetRows($query) AS $rows) {

					$infoNota = '';
					if ((strcmp ($rows['FlagNote'],'Y') == 0 ) &&( strcmp ($rows['Note_AL'],'_DEFAULT_') == 0 )){
					
						$nota = _DISCLAIMER_CATEGORIE_DEFAULT_PDMS_;
						/*if ((strcmp ($rows['Note'],'_DEFAULT_') != 0)){
							$nota = $rows['Note'];
						}*/
					
						
						$infoNota = " <a href=\"#\" class=\"tooltip\" draggable=\"false\" >
                                        <img src=\"tmpl/expo2015/catalogo/images/info1.png\" name=\"iconaInfo\" />
                                        <span >
                                        ".$nota."    <br><br>
                                        </span>
                                    </a>";
					}

                    switch ($Lang) {
                        case 'IT':
                            ?><li><?php echo $rows ['DescEstesa']; echo $infoNota;?></li><?php
                            break;
                        case 'EN':
                            ?><li><?php echo $rows ['DescEstesa_AL']; echo $infoNota;?></li><?php
                                break;
                            case 'FR':
                                ?><li><?php echo $rows ['DescEstesa_AL2']; echo $infoNota;?></li><?php
                                break;
                            default:
                                break;
                        }
                        ?>


                    <?php
                }
                ?>
            </ul>
        </div>
        <?php if (viewTabProd($testoCertificazione, $certificazione)) { ?>
            <div id="prducts-3" style="width: 100%;">
                <h4><?php echo _CERTIFICAZIONI_; ?></h4>
                <?php echo descrizioneDefoult($testoCertificazione, $certificazione[0], $certificazione[1]); ?>
                <br><br>
                <?php echo $certificazione[0]; ?>
                <?php echo $certificazione[1]; ?>

            </div>
        <?php } ?>
        <?php if (viewTabProd($testoAspetti, $aspetti)) { ?>
            <div id="prducts-4" style="width: 100%;">
                <h4><?php echo _ASPETTI_; ?></h4>
                <?php echo descrizioneDefoult($testoAspetti, $aspetti[0], $aspetti[1]); ?>
                <br><br>
                <?php echo $aspetti[0]; ?>
                <?php echo $aspetti[1]; ?>
                <?php if ($ordine == 0){?>
                <br>
                <?php echo getCheckImg($isSiexpo); ?> <?php echo _SIEXPO_NOTE_1_ ?>   <img draggable="false" src="/images/expo2015/siexpo.jpg"  style='vertical-align: middle;margin-left: 5px;width: 100px'/>
                <?php }?>
            </div>
        <?php } ?>
        <?php if ($ordine == 0) {?>
        <?php if (viewTabProd($testoBrevetti, $brevetti)) { ?>
            <div id="prducts-5" style="width: 100%;">
                <h4><?php echo _BREVETTI_; ?></h4>
                <?php echo descrizioneDefoult($testoBrevetti, $brevetti[0], $brevetti[1]); ?>
                <br><br>
                <?php echo $brevetti[0]; ?>
                <?php echo $brevetti[1]; ?>
            </div>
        <?php } 
               }?>

    </div>
</div>

<?php }
//}

else{
	?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudi()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>