<script language="JavaScript" type="text/javascript">
     $(document).keyup(function(e) {
    	  
    	  if (e.keyCode == 27) { chiudi(); }   // esc
    	});
</script>



<?php
//$idImpresaAllow = ($_POST['Id']);
$utypeArrayAllow = array(95);
$uidArrayAllow = array(3);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';

//if ($urlAllow){

if ($utypeAllow && $uidAllow){

require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/lib/db.parix.class.php");

$db = new DataBase();


if (strcmp($_POST['azione'], "UPD") == 0 || strcmp($_POST['azione'], "NEW") == 0) {

    $Id_Categoria = settaVar($_POST, "Id_Categoria", "");
    $Id = settaVar($_POST, "Id", "");
    $abilitato = settaVar($_POST, "abilitato", "0");
    $descrzioneALL = settaVar($_POST, "descrizioneALL", "1");

    $query = "SELECT * FROM EXPO_Tlk_Categorie WHERE Id = '$Id'";
	
    $dataQuery = $db->GetRow($query,null,null,"estrazione dati gestione categorie ajax Ln.18");
    
    $descrizione = $dataQuery["Descrizione"];
    $descrizioneEN = $dataQuery["Descrizione_AL"];
    $descrizioneFR = $dataQuery["Descrizione_AL2"];
    $abilitatoDB = $dataQuery["Stato_Record"];
    ?>
    <div class="divClose">
        <input type="button" onclick="chiudi()" id="close" value="Chiudi" />   
    </div>
    <h1>Gestione Categorie merceologiche</h1>
    <form id="form_moduli" name="form_moduli">
        <input type="hidden" name="Id" id="Id" value="<?php echo $Id; ?>"/>
        <input type="hidden" name="Id_Categoria" id="Id_Categoria" value="<?php echo $Id_Categoria; ?>"/>
        <br><br><br>
        <div class="box2form">
            <div class="form_label">
                <label for="Descrizione">Descrizione</label>
                *
            </div>
            <div>
                <input type="text" id="Descrizione" name="Descrizione" size="100" maxlength="255" value="<?php echo $descrizione; ?>"/>
            </div>
            <br style="clear: left;"/>
        </div>
        <?php if (strcmp($descrzioneALL, "0") != 0) { ?>
            <div class="box2form">
                <div class="form_label">
                    <label for="Descrizione_AL">Descrizione (Inglese)</label>
                    *
                </div>
                <div>
                    <input type="text" id="Descrizione_AL" name="Descrizione_AL" size="100" maxlength="255" value="<?php echo $descrizioneEN; ?>"/>
                </div>
                <br style="clear: left;"/>
            </div>
            <div class="box2form">
                <div class="form_label">
                    <label for="Descrizione_AL2">Descrizione (Francese)</label>
                    *
                </div>
                <div>
                    <input type="text" id="Descrizione_AL2" name="Descrizione_AL2" size="100" maxlength="255" value="<?php echo $descrizioneFR; ?>"/>
                </div>
                <br style="clear: left;"/>
            </div>
        <?php } ?>
        <?php if (strcmp($abilitato, "1") == 0) { ?>
            <div class="box2form">
                <div class="form_label">
                    <label for="abilitato">Abilitato ?</label>
                    *
                </div>
                <div>
                    SI <input type="radio" name="abilitato" value="Y" <?php echo getCheck($abilitatoDB); ?> /> 
                    NO  <input type="radio" name="abilitato" value="N" <?php echo getCheckNo($abilitatoDB); ?>/>
                </div>
                <br style="clear: left;"/>
            </div>
        <?php } ?>
    </form>
    <input type="button" onclick="salva()" id="close" value="SALVA" /> 
    <?php
} elseif (strcmp($_POST['azione'], "INS") == 0) {
	$Id_CategoriaLevel = settaVar($_POST, "Id_Categoria", "");
	$IdLevel = settaVar($_POST, "Id", "");
	$DescEstesa = "";
	$DescEstesa_AL = "";
	$DescEstesa_AL2 = "";
	$DescrizioneLevel = $_POST['Descrizione'];
	$Descrizione_ALLevel = $_POST['Descrizione_AL'];
	$Descrizione_AL2Level = $_POST['Descrizione_AL2'];
	$abilitato = settaVar($_POST, "abilitato", "NULL");

	$descSplit = explode(".", $DescrizioneLevel);
	$lenDescSplit = count($descSplit);
	$atecoDaInserire= array();

	if (strcmp($descSplit[$lenDescSplit-1],"*") == 0){
		$dbParix = new DataBaseParix();
		//$dbParix->SelectServer(Config::DB_PARIX_SERVER, Config::DB_PARIX_USER, Config::DB_PARIX_PASSWORD);
		 
		$atecoSearch="";
		for ($i = 0; $i < $lenDescSplit-2; $i++){
			$atecoSearch .= $descSplit[$i].".";
		}
		$atecoSearch .= $descSplit[$i];
		 
		$estraiAteco= "SELECT catp as Ateco FROM ateco7 WHERE catp like '$atecoSearch%' ORDER BY catp";
		$atecoDaInserire = $dbParix->GetRows($estraiAteco,Config::DB_PARIX_MI_NAME,"estrazione codici ateco da parix, gestione categorie ajax Ln.106");
	}

	$numAteco = count($atecoDaInserire);
	if ($numAteco == 0) {
	 $temp= array();
	$temp['Ateco']= $Descrizione;
	$atecoDaInserire []= $temp;
	} 

	require_once($PROGETTO . "/view/lib/GestioneCategorie.php");
	$gc = new GestioneCategorie();

	foreach ($atecoDaInserire as $key => $ateco){
		$Id_Categoria = $Id_CategoriaLevel;
		$Id = $IdLevel;
		$Descrizione = $ateco['Ateco'];
		$DescEstesa_AL = $Descrizione_ALLevel;
		$DescEstesa_AL2 = $Descrizione_AL2Level;

		if (strcasecmp($abilitato,"undefined")==0){
			$abilitato = "N";
		}

		if (strcasecmp($Descrizione_AL,"undefined")==0){
			$Descrizione_AL = "$Descrizione";
		}

		if (strcasecmp($Descrizione_AL2,"undefined")==0){
			$Descrizione_AL2 = "$Descrizione";
		}



		if (strcmp($Id, "") == 0 && strcmp($Id_Categoria, "") == 0) {
			$DescEstesa = $Descrizione;
			$DescEstesa_AL = $Descrizione_AL;
			$DescEstesa_AL2 = $Descrizione_AL2;
			//FREDDO
			//CONTROLLO SE LA CATEGORIA HA RELAZIONE CON IMPRESE
			//SE C'E' E NON PRESENTE SI AGGIUNGER CAMPO IN IMPRESE CATEGORIE LIVELLO 3
			$Id = $db->GetRow("SELECT MAX(Id) AS Id FROM EXPO_Tlk_Categorie", "Id",null,null,"gestione categorie ajax Ln.101") + 1;
			$Id_Categoria = $gc->getIdCategoria("00");
			$query = "INSERT INTO EXPO_Tlk_Categorie (Id) VALUES('$Id')";
			$db->Query($query,null,"gestione categorie ajax Ln.117");
		} else if (strcmp($Id, "") == 0 && strcmp($Id_Categoria, "") != 0) {
			$Id = $db->GetRow("SELECT MAX(Id) AS Id FROM EXPO_Tlk_Categorie", "Id",null,null,"gestione categorie ajax Ln.119") + 1;
			$query = "INSERT INTO EXPO_Tlk_Categorie (Id) VALUES('$Id')";
			$db->Query($query,null,"gestione categorie ajax Ln.108");

			$arrayDescEstesa = $gc->getDescEstesa($Id_Categoria, "I");
			$DescEstesa = $arrayDescEstesa["DescEstesa"] . "->" . $Descrizione;
			$DescEstesa_AL = $arrayDescEstesa["DescEstesa_AL"] . "->" . $Descrizione_AL;
			$DescEstesa_AL2 = $arrayDescEstesa["DescEstesa_AL2"] . "->" . $Descrizione_AL2;
			$Id_Categoria = $Id_Categoria . "." . $gc->getIdCategoria($Id_Categoria);
		} else {

        if (strlen($Id_Categoria) > 2) {
            $arrayDescEstesa = $gc->getDescEstesa($Id_Categoria, "M");
            $DescEstesa = $arrayDescEstesa["DescEstesa"] . "->" . $Descrizione;
            $DescEstesa_AL = $arrayDescEstesa["DescEstesa_AL"] . "->" . $Descrizione_AL;
            $DescEstesa_AL2 = $arrayDescEstesa["DescEstesa_AL2"] . "->" . $Descrizione_AL2;
        } else {
            $DescEstesa = $Descrizione;
            $DescEstesa_AL = $Descrizione_AL;
            $DescEstesa_AL2 = $Descrizione_AL2;
        }
        $gc->modificaDescEstesa($Id_Categoria, array($Descrizione, $Descrizione_AL, $Descrizione_AL2));
    }

    $gc->codAtecoImpresa($Id_Categoria);

    $query = "UPDATE EXPO_Tlk_Categorie SET Id_Categoria = '$Id_Categoria', DescEstesa = '$DescEstesa' , DescEstesa_Al= '$DescEstesa_AL', DescEstesa_Al2 = '$DescEstesa_AL2',Descrizione='$Descrizione',Descrizione_AL = '$Descrizione_AL',Descrizione_AL2 = '$Descrizione_AL2', Stato_Record = '$abilitato' WHERE Id = '$Id' ";

    $db->Query($query,null,"gestione categorie ajax Ln.146");


    if (strlen($Id_Categoria) > 8) {

        $query = "SELECT A.Id,E.IdImpresa
				FROM EXPO_Tlk_Categorie AS A INNER JOIN EXPO_Tlk_Categorie AS B ON A.Id_Categoria = SUBSTRING(B.Id_Categoria,1,8) AND LENGTH(B.Id_Categoria) = 11
				INNER JOIN EXPO_T_Ateco AS D ON TRIM(B.Descrizione) = TRIM(D.CodiceAteco)
				INNER JOIN EXPO_TJ_Imprese_Ateco AS E ON E.IdAteco = D.Id AND 0 = (SELECT COUNT(*) AS Count2 FROM EXPO_TJ_Imprese_Categorie WHERE IdImpresa = E.IdImpresa AND IdCategoria = A.Id)
				LEFT JOIN EXPO_TJ_Imprese_Categorie AS C ON A.Id = C.IdCategoria AND E.IdImpresa = C.IdImpresa
				WHERE B.Id_Categoria IN ('" . $Id_Categoria . "') AND C.IdImpresa IS NULL";

        $IdMax = $db->GetRow("SELECT MAX(Id) AS IdMax FROM EXPO_TJ_Imprese_Categorie", "IdMax",null,"gestione categorie ajax Ln.158") + 1;
        foreach ($db->GetRows($query,null,"gestione categorie ajax Ln.146") AS $rows) {
            $db->Query("INSERT INTO EXPO_TJ_Imprese_Categorie VALUES (" . $IdMax . "," . $rows['IdImpresa'] . "," . $rows['Id'] . ",'N')",null,"gestione categorie ajax Ln.147");
            $IdMax ++;
        }
    }
	}
} elseif (strcmp($_POST['azione'], "UPDCAT") == 0) {

    /*require_once($PROGETTO . "/view/lib/GestioneCategorie.php");
    $gc = new GestioneCategorie();
    $val = (array) @json_decode($_POST['val'], 1);

    $gc->ordinaLista($val);*/

	
//Aggiorna le Descrizioni estese
include "$PROGETTO/view/lib/UpdateTreeCategories.php";	

} elseif (strcmp($_POST['azione'], "DEL") == 0) {
    require_once($PROGETTO . "/view/lib/GestioneCategorie.php");
    $gc = new GestioneCategorie();

    $Id_Categoria = settaVar($_POST, "Id_Categoria", "");
    $Id = settaVar($_POST, "Id", "");
    $r = $gc->cancellaCategoria($Id, $Id_Categoria);
    if (strcmp($r, "") != 0) {
        echo $r;
//bottone elimina senza controlli
        //bottone esci
    } else {
        ob_start();
        ob_clean();
        echo "0";
        exit();
    }
}
}
//}

else{
	?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudi()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>