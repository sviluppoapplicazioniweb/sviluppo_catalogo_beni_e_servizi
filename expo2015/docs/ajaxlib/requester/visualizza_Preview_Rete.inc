<script language="JavaScript" type="text/javascript">
    $(document).keyup(function(e) {
  	  
  	  if (e.keyCode == 27) { chiudi(); }   // esc
  	});
</script>

<?php
//include "configuration.inc";


$utypeArrayAllow = array(94,96,97);
//$idVMenuAllow = array(301);
include_once "configuration.inc";
include 'autpathAjax.inc';


//if ($urlAllow){

if ($utypeAllow){


$Lang = $_POST['lang'];

require_once($PROGETTO . "/view/lib/db.class.php");
require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
require_once($PROGETTO . "/view/lib/functions.inc");

$db = new DataBase();

//Cerca la rete nella sua anagrafica
if ($_POST['Id'] > 0){
	$IdRete = $_POST['Id'];
	$query = "SELECT * FROM EXPO_T_RetiImpresa WHERE Id = $IdRete";
	$row = $db->GetRow($query,null,null,"visualizza_Preview_Rete.inc ajax ln.35");
	$NRete = $row['NomeRete'];
} elseif (strlen($_POST['rete'])){
	$NRete = $_POST['rete'];
	$query = "SELECT * FROM EXPO_T_RetiImpresa WHERE NomeRete = '" . mysql_escape_string(htmlspecialchars($NRete)) . "'";
	$row = $db->GetRow($query,null,null,"visualizza_Preview_Rete.inc ajax ln.40");
	$IdRete = $row['Id'];
}
$rootDoc = selectRootDocReti($NRete) . "/";
//Recupera le imprese iscritte ad una Rete
$qryLsImp = "SELECT EXPO_TJ_Imprese_RetiImpresa.Id_Impresa, EXPO_TJ_Imprese_RetiImpresa.Id_Rete, EXPO_TJ_Imprese_RetiImpresa.IsCapogruppo, EXPO_T_Imprese.RagioneSociale
FROM EXPO_T_Imprese INNER JOIN EXPO_TJ_Imprese_RetiImpresa ON EXPO_T_Imprese.Id = EXPO_TJ_Imprese_RetiImpresa.Id_Impresa
WHERE Id_Rete = $IdRete";

//echo $qryLsImp;
//$rowsLsImp = $db->GetRows($qryLsImp);
//print_r($rowsLsImp);
//exit;

$divListaImp = '<tr><td id="ImpreseXRete" colspan="2">'._IMPRESE_RETE_;
foreach ($db->GetRows($qryLsImp,null,"visualizza_Preview_Rete.inc ajax ln.55") AS $rowsLsImp) {
	if ($rowsLsImp['IsCapogruppo']=='Y'){
		$divListaImp .= '<div class="divImpRetiCapog" title="'._ANT_IMP_CAPOG_.'">'.$rowsLsImp['RagioneSociale'].'</div>';
	} else {
		$divListaImp .= '<div class="divImpReti">'.$rowsLsImp['RagioneSociale'].'</div>';
	}
}
$divListaImp .= '</td></tr>';
		
?>

<div class="contentHeader">
    <div class="divClose">
        <a draggable="false" href="javascript:chiudi()" >X</a>   
    </div>
    <div id="header-lang">
        <?php include_once $PROGETTO . "/view/lib/languages_Reti.inc"; ?>
    </div>
</div>
<div class="divBoxAnteprima">
	
    <div class="containerDataCheck">
    	<table width="100%">
    		<tr><td><h4><?php echo _ANT_NOME_RETE_ .$row['NomeRete'] ?></h4></td></tr>
    		<tr>
            	<td><strong><?php echo _DESCR_RETE_ ?></strong>
        		<?php                           
                switch ($Lang) {
                	case 'IT': echo html_entity_decode($row['Descrizione']);
                    	break;
                    case 'EN': echo html_entity_decode($row['Descrizione_AL']);
                    	break;
                    case 'FR': echo html_entity_decode($row['Descrizione_AL2']);
                    	break;
               	}
                ?>
               	</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><?php echo $divListaImp; ?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td><strong><?php echo _ANT_CONTATTO_RETE ?> </strong> 
            	<?php 
            		echo html_entity_decode($row['Contatto']);
            	?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
            	<td><?php echo getAllegatiFiles('file', $rootDoc . $IdRete . "/descriz", _ALLEGA_DOC_); ?></td>
           </tr>
           <tr>
            	<td><?php echo getAllegati('link', $row['SitoWeb']); ?></td>
           </tr>
        </table>
    </div>
</div>

<?php }
//}

else{
?>
<div class="contentHeader">
	<div class="divClose">
		<a draggable="false" href="javascript:chiudi()">X</a>
	</div>
</div>

	<div class="error" style="text-align: center; font-size: 18px;">
		<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
	</div>

<?php 
}

?>