<script language="JavaScript" type="text/javascript">
    $(function() {
        $("#tabs").tabs();
    });

    $(document).keyup(function(e) {
    	  
    	  if (e.keyCode == 27) { chiudi(); }   // esc
    	});
</script>

<?php
$utypeArrayAllow = array(94,95,96,97);
include "configuration.inc";
$Lang = $_REQUEST['lang'];

require_once($PROGETTO . "/view/languages/" . $Lang . ".inc");
require_once($PROGETTO . "/view/lib/functions.inc");
require_once($PROGETTO . "/view/lib/db.class.php");

$db = new DataBase();
include 'autpathAjax.inc';
if ($utypeAllow){
$Id = settaVar($_REQUEST, 'Id', '');

$query = "SELECT * FROM EXPO_T_Bacheca WHERE id = '$Id'";

$resultQuery = $db->GetRow($query,null,null,"visualizza_trattativa.inc ajax Ln.18");
$IdPartecipante = $resultQuery['IdPartecipante'];
$IdNazione = $resultQuery['Id_Nazione'];
$IdImpresa = $resultQuery['IdImpresa'];
$oggetto = $resultQuery['Oggetto'];

if ($IdPartecipante > 0){
	$anagraficaPartecipante = $db->GetRow('SELECT * FROM T_Anagrafica WHERE id = ' . $IdPartecipante, 'RagSoc',null,"visualizza_EXPO_T_Bacheca_Trattative_Trattative.inc Ln.26");
} else {
	$anagraficaPartecipante = $db->GetRow('SELECT * FROM EXPO_Tlk_Nazioni WHERE Id_Nazione = ' . $IdNazione, 'Nome_Nazione',null,"visualizza_EXPO_T_Bacheca_Trattative_Trattative.inc Ln.28");
}
//$IdProdotto = $db->GetRow($query, 'IdProdotto');


$IdMessaggio = $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$Id'", 'IdMessaggio');
?>
<div class="divClose">
    <input style="float: right;" type="button" onclick="chiudi()" id="close" value="X <?php echo _CHIUDI_; ?>" />   
</div>
<h2><?php echo _VISUALIZZA_TRATTATIVA_; ?></h2>
<br>
<br>
<table > 
    <tr>

        <td style="width:20%; min-width: 190px;" class="textLineBlu"><?php echo _NOME_PARTECIPANTE_; ?>:</td>
        <td ><?php echo $anagraficaPartecipante ?></td>
    </tr>
    <tr>
        <td class="textLineBlu"><?php echo _FORNITORE_; ?>:</td>
        <td ><?php echo $db->GetRow('SELECT * FROM EXPO_T_Imprese WHERE id = ' . $IdImpresa, 'RagioneSociale', null ,"visualizza trattativa"); ?></td>
    </tr>
	<tr>
        <td class="textLineBlu"><?php echo _OGGETTO_TRATTATIVA_; ?>:</td>
		<td><?php echo $oggetto;?></td>
	</tr>    
    <tr>
        <td><span class="textLineBlu"><?php echo _VALORE_TRATTATIVA_; ?>:<?php if (strcmp($azione, "CLS") == 0) { ?>*<?php } ?></span></td>
        <td >
            <?php echo number_format($db->GetRow("SELECT * FROM EXPO_T_Bacheca_Trattative WHERE IdBacheca = '$Id'", 'Valore'), 2, ',', '.'); ?>
            &euro;
        </td> 
        <?php
        $query = "SELECT DISTINCT Nome
                                  FROM EXPO_T_Prodotti AS T
                                  RIGHT JOIN EXPO_TJ_Bacheca_Prodotti AS TJ ON T.Id = TJ.IdProdotto
                                WHERE TJ.IdBacheca = '$Id'
                                ORDER BY Nome";
        if ($db->NumRows($query) > 0) {
            ?>
            <td ><span class="textLineBlu"><?php echo _PRODOTTO_; ?>:</td> 
            <td >
                <ul>  
                    <?php
                    print_r($db->GetRows($query));
                    
                    foreach ($db->GetRows($query) AS $rows) {
                        ?>
                        <li><?php echo $rows ['Nome']; ?></li>
                    <?php } ?>
                </ul>
            </td>     

        <?php } ?>
    </tr>
    <?php if (strcmp($IdMessaggio, "") != 0 && strcmp($IdMessaggio, "0") != 0) { ?>

        <tr>
            <td colspan="4" style="border-bottom: 1px solid #26ACE2;">
                <span class="textLineBlu"><?php echo _MOTIVAZIONE_TRATTATIVA_; ?></span>
                
            </td>        
        </tr>
        <tr>
            <td colspan="4" style="border-bottom: 1px solid #26ACE2;">
                <br>
            <?php echo $db->GetRow("SELECT * FROM EXPO_T_Bacheca_Messaggi WHERE Id = '$IdMessaggio'", 'Testo'); ?>    
                <br><br>
            </td>        
        </tr>
    <?php } ?>
     <tr>
            <td>
            <?php $queryProdotto = "SELECT BA.Id AS Id 
									FROM EXPO_T_Bacheca AS B JOIN 
									     EXPO_T_Bacheca_Messaggi AS BM ON B.Id=BM.IdBacheca JOIN 
									     EXPO_T_Bacheca_Allegati AS BA ON BM.Id = BA.Id
									WHERE Trattativa_Impresa = 'Y' AND B.Id = $Id
									GROUP BY IdBacheca";
				$idFile = $db->GetRow($queryProdotto,'Id',null,'Visualizza trattative EXPO ln.163');
				if ($idFile >0 ){
	?>
		
			<a href="ajaxlib/requester/viewFile.inc?Id=<?php echo $idFile; ?>" target="_blank"><?php echo _VISUALIZZA_ . " " . _ALLEGATO_; ?> ></a>
            <?php }?>
            
            </td>
        </tr>

</table>

<?php 
}else{
	?>
<style>
.error {
	color: #e55;
}

div.error {
	border: 1px solid #d77;
}

div.error,tr.error {
	background: #fcc;
	color: #200;
	padding: 2px;
}
</style>
<div class="divClose">
    <input style="float: right;" type="button" onclick="chiudi()" id="close" value="X <?php echo _CHIUDI_; ?>" />   
</div>

<div class="error" style="text-align: center; font-size: 18px;">
	<p class="error">NON SEI ABILITATO ALLA VISUALIZZAZIONE</p>
</div>

<?php 
}

?>