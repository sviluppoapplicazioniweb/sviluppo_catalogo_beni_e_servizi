<?php

include "progetto.inc";
include "lib/URLClass.inc";
include $PROGETTO . "/prepend.php3";

header("Content-Type: text/xml");
header("Pragma: no-cache");

$db_news = new DB_CedCamCMS;
$sitemapURL = new URLClass();

$SQL = "SELECT Id, UCASE(Titolo) as Titolo, substring( Data_Modifica, 1, 10 ) as DataMod"; //MZ250211
$SQL.=" FROM T_Menu";
$SQL.=" WHERE Id_Priorita = 0 AND Id_TipoUtente = 0 AND TipiUtente LIKE '|' AND Gruppi LIKE '|' AND MasterEditor = 0 AND ((Id_Menu<'80' AND Id_Menu>'01') OR Id_Menu='00') ";

if ($db_news->query($SQL)) {
    $i = 1;
    $dataeora = "";
    $items = "";
    
    while ($db_news->next_record()) {
        $titolourl = $sitemapURL->furl($db_news->f('Titolo')); //MZ280211

        $items.="       <url>\n";
//	$items.="          <loc>".$HOMEURL."index.phtml?Id_VMenu=".$db_news->f('Id')."</loc>\n"; //MZ250211
	$items.="          <loc>".$HOMEURL.$titolourl."/".$db_news->f('Id')."</loc>\n"; //MZ250211
        $items.="          <lastmod>" . $db_news->f('DataMod') . "</lastmod>\n";
//	$items.="          <changefreq>monthly</changefreq>\n";
//	$items.="          <priority>0.8</priority>\n";
        $items.="       </url>\n";

        $i++;
    }
}
$codice_html = file_get_contents($PATHDOCS . "tmpl/sitemap.xml");
echo preg_replace("/\{%(\w+)%\}/e", "\$\\1", $codice_html);
?>