<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <script src="/js/jquery-1.7.min.js" type="text/javascript"></script>
    <script src="/js/jquery.reveal.js" type="text/javascript"></script>
    <script src="/js/jquery.min.js"type="text/javascript"></script>
    <script src="jquery.custom.min.js" type="text/javascript"></script>
    <link href="css/jquery.custom.css" rel="stylesheet" type="text/css" />
    <title>jQuery dialog</title>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#finestra').dialog({
            modal: true,
            buttons: {
                "Chiudi": function() {
                    $( this ).dialog( "close" );
                    }
                }
            });
        });
<?php
 unset($_SESSION['display']);
 unset($_SESSION['messaggio']);
 unset($_SESSION['GoURL']);
 ?>
    </script>
</head>
<body>
 <div id="finestra" title="Titolo della finestra">
     <p>Premi su "chiudi" per <strong> chiudere la  finestra</strong> o su "scrivi ciao mondo" per scrivere nella  pagina principale la stringa "Ciao mondo"</p>
</div>
<p id="box"></p>
</body>
</html>