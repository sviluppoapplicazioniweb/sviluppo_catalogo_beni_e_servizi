<?php

error_reporting(0);
$pathroot="/internet/datiExpo/include/";
require_once ($pathroot."configuration.inc");

/*--------- Libraries for sendFile -------*/
include $pathroot.'/send_mail.inc';
include_once $pathroot.'/lib/PHPMailer/class.smtp.php';
include_once $pathroot.'/htmlparser.inc';

 class  funzioniUtili {
	
	/**
	 * Function that create a file
	 * @param $dir
	 * @param $fileName
	 * @param $fileText
	 */
	public function createFile($dir,$fileName,$fileText){
	
		$localFile= "$dir/$fileName";
		
		$file = fopen($localFile, "w");
	
		fwrite($file, $fileText);
	
		fclose($file);
	
	}
	
	
	/**
	 * Function that send file to email
	 */
	public function sendEmailFile($subject,$message,$toAddress,$file){
	
	send_mail_html_file($toAddress, "noreply.catalogue@expo2015.org", $subject , $message, $file);
	}
	
	
	public  function lastBlank($string){
		$lastBlankIndex=-1;
		for ($i=0; $i < strlen($string); $i++){
	
			if 	($string{$i}== " "){
				$lastBlankIndex = $i;
			}
		}
	
		return $lastBlankIndex;
	}
	
}