
function emailCheck(emailStr) {

    var emailPat = /^(.+)@(.+)$/;
    var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars = "[^\\s" + specialChars + "]";
    var quotedUser = "(\"[^\"]*\")";
    var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom = validChars + "+";
    var word = "(" + atom + "|" + quotedUser + ")";
    var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
    var matchArray = emailStr.match(emailPat);

    if (matchArray == null) {
        return false;

    }

    var user = matchArray[1];
    var domain = matchArray[2];

    if (user.match(userPat) == null) {
        return false;

    }

    var IPArray = domain.match(ipDomainPat);

    if (IPArray != null) {

        for (var i = 1; i <= 4; i++) {

            if (IPArray[i] > 255) {
                return false;

            }

        }

        return true;

    }

    var domainArray = domain.match(domainPat);

    if (domainArray == null) {
        return false;

    }

    var atomPat = new RegExp(atom, "g");
    var domArr = domain.match(atomPat);
    var len = domArr.length;

    if (domArr[domArr.length - 1].length < 2 ||
            domArr[domArr.length - 1].length > 6) {
        return false;

    }

    if (len < 2) {
        return false;
    }

    return true;

}



function valida(iform, stato) {


    if (stato == 0) {
        var formNome = $("#Nome").val();
        var formCognome = $("#Cognome").val();
        var formEmail = $("#Email").val();
        var formEmailC = $("#EmailC").val();
        var formEmailAlternativa = $("#EmailAlternativa").val();
        var formEmailCAlternativa = $("#EmailCAlternativa").val();
        if (formNome == "" || formCognome == "" || formEmail == "" || formEmailC == "" ) {
            alert(_CAMPI_OB_);
            return false;
        } else if (!emailCheck(formEmail) || !emailCheck(formEmailC)) {
            alert(_EMAIL_ER_);
            return false;
        }  else if (formEmail != formEmailC) {
            alert(_EMAIL_CON_ER_);
            return false;
        } else if (formEmailAlternativa != formEmailCAlternativa) {
            alert(_EMAIL_CON_ER_);
            return false;
        } else if (formEmailAlternativa != "") {
        	if (!emailCheck(formEmailAlternativa) || !emailCheck(formEmailCAlternativa)) {
                alert(_EMAIL_ER_);
                return false;
            }
        }
    } else {

        var formPassword = $("#Password").val();

        if (formPassword == "") {
            alert( _CAMPI_OB_);
            return false;
        }

    }

    return true;
}
