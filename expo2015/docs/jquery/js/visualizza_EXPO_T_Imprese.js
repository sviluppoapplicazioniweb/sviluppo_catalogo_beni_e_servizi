document.writeln("<script type='text/javascript' src='/jquery/jquery.currency.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse_cookie.js'></script>");


var arrayLingue = null;
$(function() {
    var par1 = 'lang=' + $("#Lang").val() + '&Id=' + $("#IdImp").val();


    infoError($("#IdImp").val(), "visualizza_EXPO_T_Impresa", "", "Lista Categorie Scheda Impresa");

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_Categorie.inc",
        data: par1,
        success: function(response) {

            $("#listadellecategorie").html(response);

        }
    });

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/get_lista_categorie.php",
        data: par1,
        success: function(response) {

            $("#listdellecategorie").html(response);
            $("#listdellecategorieAnteprima").html(response);
        }
    });
});
$(document).ready(function() {

	$("#titoloBarra").html($("#titoloBarraAppend").val());
    $("input:file").change(function() {
        var fileName = $(this).val();

        fileName = fileName.split('\\');
        $("#testofilename" + $(this).attr('name')).html(fileName[fileName.length - 1]);
    });


    //    $('html,body').animate({scrollTop: $(this).offset().top}, 500);
    $('body, html').animate({
        scrollTop: 0
    }, 'slow');

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/get_caratteristiche_impresa.php",
        data: 'lang=' + $("#Lang").val() + '&impresaID=' + $("#IdImp").val(),
        success: function(response) {

            $("#div_caratteristiche_impresa").html(response);

        }
    });
    $("#fax").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43)
            return false;
        return true;
    });
    $("#tel").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43)
            return false;
        return true;
    });


    arrayLingue = $("#listaLingueSito").val().split('|');

    $('textarea[maxlength]').keyup(function() {
        var max = parseInt($(this).attr('maxlength'));
        if ($(this).val().length > max) {
            $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
        }

        $(this).parent().find('.charsRemaining').html('' + (max - $(this).val().length) + ' caratteri rimanenti');
    });

    $("#ultimoFatturato").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });

    $("#FatturatoUltimoEsercizio").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });

    $("#ultimoFatturato").blur(function() {
        $(this).currency({
            region: '',
            thousands: '.',
            decimal: ',',
            decimals: 2
        });
    });
    $("#FatturatoUltimoEsercizio").blur(function() {
        $(this).currency({
            region: '',
            thousands: '.',
            decimal: ',',
            decimals: 2
        });
    });
    $.cookie('nav', null);

    $('.collapsible').collapsible({
        defaultOpen: '',
        cookieName: 'nav',
        speed: 'slow'
    });
    //    $('.collapsibleCat').collapsible({
    //        defaultOpen: '',
    //        cookieName: 'navCat',
    //        speed: 'slow'
    //    });
    $("input[name$='L']").focus(function() {
        if ($(this).val() == "http://") {
            $(this).val("");
            $(this).css("color", "#000000");
        }
    });
    $("input[name$='L']").blur(function() {
        if ($(this).val() == "") {
            $(this).val("http://");
            $(this).css("color", "#999999");
        }
    });

    $("input[name$='L']").each(function() {
        if ($(this).val() == "http://") {
            $(this).css("color", "#999999");
        }
    });

    $("#webSite").focus(function() {
        if ($("#webSite").val() == "http://") {
            $("#webSite").val("");
            $("#webSite").css("color", "#000000");
        }
    });
    $("#webSite").blur(function() {
        if ($("#webSite").val() == "") {
            $("#webSite").val("http://");
            $("#webSite").css("color", "#999999");
        }
    });

    if ($("#webSite").val() == "http://") {
        $(this).css("color", "#999999");
    }


});

$(function() {
    if ($("#StatoRegistrazione").val() == "4") {
        var tag = 'input[type="text"],textarea,input[type="checkbox"],input[type="file"],select';
        $(tag).attr('disabled', 'disabled');
        $(tag).css("cursor", "default");
        $('a[name="link_form"]').css("cursor", "default");
        $("[id^='charsRemaining']").hide();
        $("[name='imgDel']").hide();
        $("[name='imgDelR']").hide();
        $("[name='imgModR']").hide();
        $(".icondelete").hide();
        $('a[name="link_aggiungi"]').css("background", "#666666");
        //$('a[name="link_form"]').css("background", "#666666");
        $('a[name="link_logo"]').css("cursor", "default");
        $('a[name="link_logo"]').css("background", "#666666");
        $('.upload-file-container_edit').attr("class", 'upload-file-container');
        $('.CategoriaCancel').hide();
        $('#listadellecategorie').hide();
        $('#gestiscicategoria').hide();
        $('a[name="link_form"]').click(function() {
            return false;
        });
        $('a[name="link_logo"]').click(function() {
            return false;
        });
    }

    $("#companies").tabs();

});



function contacaratteri(id) {
    var max = parseInt($("#descrizione" + id).attr('maxlength'));

    if ($("#descrizione" + id).val().length > max) {
        $("#descrizione" + id).val($("#descrizione" + id).val().substr(0, $("#descrizione" + id).attr('maxlength')));
    }
    $("#charsRemaining" + id).html('' + (max - $("#descrizione" + id).val().length) + ' caratteri rimanenti');
}


function contacaratteriG(id) {
    var max = parseInt($("#qualita" + id).attr('maxlength'));

    if ($("#qualita" + id).val().length > max) {
        $("#qualita" + id).val($("#qualita" + id).val().substr(0, $("#qualita" + id).attr('maxlength')));
    }
    $("#charsRemainingG" + id).html('' + (max - $("#qualita" + id).val().length) + ' caratteri rimanenti');
}



function contacaratteriR(id) {
    var max = parseInt($("#descrizioneR" + id).attr('maxlength'));

    if ($("#descrizioneR" + id).val().length > max) {
        $("#descrizioneR" + id).val($("#descrizioneR" + id).val().substr(0, $("#descrizioneR" + id).attr('maxlength')));
    }
    $("#charsRemainingR" + id).html('' + (max - $("#descrizioneR" + id).val().length) + ' caratteri rimanenti');
}


function checkCertificazione(element, idCert, idImpresa) {
	var par = null;
    if (element.checked) {
        par = "idImpresa=" + idImpresa + "&idCert=" + idCert + "&action=add";

    } else {
        par = "idImpresa=" + idImpresa + "&idCert=" + idCert + "&action=delete";
    }

    $.ajax({
        type: "GET",
        async: false,
        url: "ajaxlib/requester/controllo_prodotto_certificazioni.inc",
        data: par,
        success: function(response) {

            // alert(response);
        }
    });

}

function controlloCampiOb(idCampo) {

    for (var i = 0; i < arrayLingue.length; i++) {
        if (document.getElementById(idCampo + arrayLingue[i]).value == "") {
            return false;
        }
    }
    return true;
}

function controlloCampiTr(idCampo) {
    var er = 0;
    for (var i = 0; i < arrayLingue.length; i++) {

        if (document.getElementById(idCampo + arrayLingue[i]).value != "") {
            er++;
        }
    }

    if (er > 0 && er < arrayLingue.length)
        return false;

    return true;
}


function valida(iform) {

    var formEmail = $("#mail").val();
    var testoMsgHeader = _VALIDAZIONE_HEADER_;
    var testoMsgQualita = _CAMPI_OB_QUALITA_;
    var testoMsgEmail = _CAMPI_OB_EMAIL_;
    var testoMsgEmailEr = _EMAIL_ER_;
    var testoMsgDescrizione = _CAMPI_OB_DESCRIZIONE_;
    var testoMsg = "";
    
    if (!controlloCampiTr("qualita"))
        testoMsg = testoMsgQualita;

    if (!controlloCampiOb("descrizione"))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgDescrizione;

    if (formEmail == "")
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgEmail;

    
    if (!emailCheck(formEmail))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgEmailEr;

    if (testoMsg != '') {

        testoMsg = testoMsgHeader + "\n" + testoMsg;

        alert(testoMsg);
        return false;
    }
    else {
        return true;
    }
}

function validaRete(iform) {
	
	var NomeRete = $("#NomeRete").val();
    var DescIT = $("#descrizioneR").val();
    var DescEN = $("#descrizioneREN").val();
    var DescFR = $("#descrizioneRFR").val();
    var testoMsg = "";
    var testoMsgHeader = "Attenzione! Non è possibile aggiornare la Scheda Rete: ";
    
    if (NomeRete.length == 0)
    	testoMsg = "Nome Rete: campo obbligatorio \n";
    
    if (NomeRete == 'Nome Mancante')
    	testoMsg += "Nome Rete non valido \n";
    
    if (DescIT.length == 0 || DescEN.length == 0 || DescFR.length == 0)
    	testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + "Descrizione Rete: campo obbligatorio nelle tre lingue.";
    
    /*var testoMsgHeader = _VALIDAZIONE_HEADER_;
    var testoMsgQualita = _CAMPI_OB_QUALITA_;
    var testoMsgEmail = _CAMPI_OB_EMAIL_;
    var testoMsgEmailEr = _EMAIL_ER_;
    var testoMsgDescrizione = _CAMPI_OB_DESCRIZIONE_;
    

    if (!controlloCampiTr("qualita"))
        testoMsg = testoMsgQualita;

    if (!controlloCampiOb("descrizione"))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgDescrizione;

    if (formEmail == "")
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgEmail;

    
    if (!emailCheck(formEmail))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgEmailEr;*/

    if (testoMsg != '') {

        testoMsg = testoMsgHeader + "\n" + testoMsg;

        alert(testoMsg);
        return false;
    }
    else {
        return true;
    }
    
}

function emailCheck(emailStr) {

    var emailPat = /^(.+)@(.+)$/;
    var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars = "[^\\s" + specialChars + "]";
    var quotedUser = "(\"[^\"]*\")";
    var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom = validChars + "+";
    var word = "(" + atom + "|" + quotedUser + ")";
    var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
    var matchArray = emailStr.match(emailPat);

    
    if (matchArray == null) {
        return false;

    }

    var user = matchArray[1];
    var domain = matchArray[2];

    if (user.match(userPat) == null) {
        return false;

    }

    var IPArray = domain.match(ipDomainPat);

    if (IPArray != null) {

        for (var i = 1; i <= 4; i++) {

            if (IPArray[i] > 255) {
                return false;

            }

        }

        return true;

    }

    var domainArray = domain.match(domainPat);

    if (domainArray == null) {
        return false;

    }

    var atomPat = new RegExp(atom, "g");
    var domArr = domain.match(atomPat);
    var len = domArr.length;

    if (domArr[domArr.length - 1].length < 2 ||
            domArr[domArr.length - 1].length > 6) {
        return false;

    }

    if (len < 2) {
        return false;
    }

    return true;

}

function gestioneAssocizioni(iAction, impresa, associazione) {
    var par = 'lang=' + $("#InfoLang").html() + '&action=' + iAction + '&idImp=' + impresa + '&idAss=';

    if (iAction == "DEL") {
        par += associazione;
    } else if (iAction == "UPD") {
        var selObj = document.getElementById("associazioni");
        var selIndex = selObj.selectedIndex;
        par += selObj.options[selIndex].value;
    }

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/visualizza_Imprese_gestione_assicizioni.inc",
        data: par,
        success: function(response) {

            var list = response.split("::::");
            $("#listAssocizioni").html(list[0]);
            $("#associazioni").html(list[1]);
        }
    });
}


function gestioneLingue(iAction, impresa, lingua) {

    var idLingua = "";
    if (iAction == "DEL") {
        idLingua = lingua;
    } else if (iAction == "UPD") {
        var selObj = document.getElementById("lingue");
        var selIndex = selObj.selectedIndex;
        idLingua = selObj.options[selIndex].value;
    }
    if (idLingua != "") {
        var par = 'lang=' + $("#InfoLang").html() + '&action=' + iAction + '&idImp=' + impresa + '&idLg=' + idLingua;
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/visualizza_Imprese_gestione_lingue.inc",
            data: par,
            success: function(response) {
                var list = response.split("::::");
                $("#listaLingue").html(list[0]);
                $("#lingue").html(list[1]);
            }
        });
    }
}

function gestioneReti(iAction, impresa, rete) {
	
	//alert('entrato reti');
    var idRete = "";
    if (iAction == "DEL") {
        if (confirm("Sei sicuro di volerti cancellare dalla Rete?")) {
        	idRete = rete;
        }
    } else if (iAction == "UPD") {
        var selObj = document.getElementById("retiImprese");
        var selIndex = selObj.selectedIndex;
        idRete = selObj.options[selIndex].value;
    }
    
    //alert('Rete id'+idRete);
    if (idRete != "" && idRete > 0) {
        var par = 'lang=' + $("#InfoLang").html() + '&action=' + iAction + '&idImp=' + impresa + '&idNt=' + idRete;
       //alert('param'+par);
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/visualizza_Imprese_gestione_reti.inc",
            data: par,
            success: function(response) {
                //alert('response'+response);
            	var list = response.split("::::");
                //alert('lista'+list[1]);
                $("#listareti").html(list[0]);
                $("#retiImprese").html(list[1]);
            }
        });
    }
}

function gestioneImpXReti(iAction, impresa, rete, idimp) {
	
	//alert('entrato gestioneImpXReti');
	//var idRete = "";
	if (iAction == "ADD") {
        impresa = $("#searcCodeI").val();
        $("#searcCodeI").val("");
        //alert('impresa ' + impresa);
        //exit;
        //var selIndex = selObj.selectedIndex;
    }
    
    //alert('Impresa id '+impresa);
    //exit;
    //if (idRete != "" && idRete > 0) {
        var par = 'lang=' + $("#InfoLang").html() + '&action=' + iAction + '&idImp=' + idimp + 
        	'&idNt=' + rete + '&idimpDel=' + impresa;
       //alert('param'+par);
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/visualizza_Imprese_X_Reti.inc",
            data: par,
            success: function(response) {
                //alert('response'+response);
            	var list = response.split("::::");
                //alert('lista'+list[1]);
                $("#ImpreseXRete").html(list[0]);
                $("#retiImprese").html(list[1]);
            }
        });
    //}
}

function previewReteR(nameR){
	if (nameR.length > 0){
		var par = 'lang=' + $("#InfoLang").html() + '&rete=' + nameR;
		
		$.ajax({
	    	type: "POST",
	        async: false,
	        url: "ajaxlib/requester/visualizza_Preview_Rete.inc",
	        data: par,
	        success: function(response) {
	            $("#box_popup").html(response);
	            document.getElementById("coprente").style.display = 'block';
	        }
	    });
	}
}

function previewRete(nameR){
	var nomeRete = $("#searcCode").val();
	var avalTags = $("#lsReti").val().split(",");
	//alert(avalTags.join('\n'));
	
	if (nomeRete.length > 0){
		var a = avalTags.indexOf(nomeRete);
		
		if (a > -1){
			//alert('Rete trovata');
			//Carica l'anteprima della Rete
			var par = 'lang=' + $("#InfoLang").html() + '&rete=' + nomeRete;
		    //alert('param'+par);
		    $.ajax({
		    	type: "POST",
		        async: false,
		        url: "ajaxlib/requester/visualizza_Preview_Rete.inc",
		        data: par,
		        success: function(response) {
		            $("#box_popup").html(response);
		            document.getElementById("coprente").style.display = 'block';
		        }
		    });
		}
		
		
	} else {
		alert('Inserisci il nome di una Rete');
	}
		
}

function changeRete(idimp, NRete, opt){
	var nomeRete = '';
	//cerca il nome della rete tra quelli in lista e se non lo trova assume che sia nuova.
	if (opt != 'N')
		nomeRete = $("#searcCode").val();
	else
		nomeRete = NRete;
		
	//alert ('Rete ' + nomeRete);
	var avalTags = $("#lsReti").val().split(",");
	//alert(avalTags.join('\n'));
	var a = avalTags.indexOf(nomeRete);
	
	//alert('res ' + a);
	if (a > -1 || nomeRete == 'Nome Mancante'){
		//alert('Entrato');
		//Modifica una rete esistente gi� abbinata all'impresa
		if (nomeRete != "" && opt =='N'){
			//alert('Modifica rete:'+idRete);
			//visualizzaBOXReti(idimp, idRete,'UPD');
			creaFormReti($("#InfoLang").html(), idimp, nomeRete,'UPD');
		} else {
			//Rete esiste cerca quindi di aggiungerla
			
		    var par = 'lang=' + $("#InfoLang").html() + '&action=UPD&idImp=' + idimp + '&NRete=' + nomeRete;
		    //alert('param'+par);
		    $.ajax({
		    	type: "POST",
		        async: false,
		        url: "ajaxlib/requester/visualizza_Imprese_gestione_reti.inc",
		        data: par,
		        success: function(response) {
		        //alert('response'+response);
		        var list = response.split("::::");
		        //alert('lista'+list[1]);
		        $("#listareti").html(list[0]);
		        $("#retiImprese").html(list[1]);
		        }
		    });  
		}
	} else {
		//Nuova Rete
		
		var r=confirm("Nuova Rete d'imprese! Vuoi crearla?");
		if (r==true)
		  {
			//alert('Lang '+ $("#InfoLang").html());
			creaFormReti($("#InfoLang").html(),idimp, nomeRete,'ADD');
		  }
	}
	
}

function updateImg(azione, idImp) {

    if (azione === "INS") {

        var oMyForm = new FormData();
        oMyForm.append("idImp", idImp);
        oMyForm.append("action", azione);
        oMyForm.append("lang", $("#InfoLang").html());
        oMyForm.append("logoF", document.getElementById("logoF").files[0]);
        $.ajax({
            url: "ajaxlib/requester/impresa_update_logo.inc",
            type: "POST",
            processData: false,
            contentType: false,
            data: oMyForm,
            success: function(response) {
                document.getElementById("coprente").style.display = 'none';
                x = $.parseJSON(response);

                if (x.error !== '') {
                    alert(x.msg);
                } else {
                    $("#logoImg").html('<img src="' + x.msg + '" draggable="false"/><br><?php echo _C_LOGO_; ?>');
                }

            }
        });

    } else {
        var par = 'idImp=' + idImp + '&action=' + azione + '&lang=' + $("#InfoLang").html();
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/impresa_update_logo.inc",
            data: par,
            success: function(response) {
                $("#box_uploadfile").html(response);
                //document.getElementById("box_popup").innerHTML = response;
                document.getElementById("coprente_upload").style.display = 'block';

            }
        });
    }


}
function visualizzaPagamento(idTable) {
    var par = 'lang=' + $("#InfoLang").html() + '&Id=' + idTable;


    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_Pagamento.php",
        data: par,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }
    });
}
function visualizzaBOX(idTable) {
    var par = 'lang=' + $("#InfoLang").html() + '&Id=' + idTable
    		+ "&Ordine=" + $("#Ordine").val()
    		+ "&IsStudio=" + $("#IsStudio").val()
    		+ "&NumeroDipendenti=" + $("#NumeroDipendenti").val()
            + "&descrizioneL=" + $("#descrizioneL").val()
            + "&Telefono=" + $("#tel").val()
            + "&Fax=" + $("#fax").val()
            + "&Email=" + $("#mail").val()
            + "&WebSiteURL=" + $("#webSite").val()
            + "&UltimoFatturato=" + $("#ultimoFatturato").val()
            + "&FatturatoUltimoEsercizio=" + $("#FatturatoUltimoEsercizio").val()
            + "&DescrizioneAttivita=" + $("#descrizione").val()
            + "&DescEstesa=" + $("#descrizione").val()
            + "&DescEstesa_AL=" + $("#descrizioneEN").val()
            + "&DescEstesa_AL2=" + $("#descrizioneFR").val()

            + "&internazionalizzazioneL=" + $("#internazionalizzazioneL").val()
            + "&referenzeL=" + $("#referenzeL").val()
            + "&qualitaL=" + $("#qualitaL").val()
            + "&qualita=" + $("#qualita").val()
            + "&qualitaEN=" + $("#qualitaEN").val()
            + "&qualitaFR=" + $("#qualitaFR").val()
            + "&retiImpresaL=" + $("#retiImpresaL").val()
            //            + "&IsGeneralContractor=" + $("#gereralContractor:checked").val()
            //            + "&IsProduttore=" + $("#produttore:checked").val()
            //            + "&IsDistributore=" + $("#distributore:checked").val()
            //            + "&IsPrestatoreDiServizi=" + $("#IsPrestatoreDiServizi:checked").val()
            //            + "&IsSediEstere=" + $("#sedEstera:checked").val()
            //            + "&IsNetwork=" + $("#netetwork:checked").val()
            //            + "&partecipazioneExpo=" + $("#partecipazioneExpo:checked").val()
            //
            //            + "&partecipazioniInternazionali=" + $("#partecipazioniInternazionali:checked").val()
            //            + "&aModello=" + $("#aModello:checked").val()
            //            + "&IsMasterSpecialistico=" + $("#IsMasterSpecialistico:checked").val()
            //            + "&pMasetr=" + $("#pMasetr:checked").val()
            //            + "&pCertificazione=" + $("#pCertificazione:checked").val()
            //            + "&retiImpresa=" + $("#retiImpresa:checked").val()
            + "&aModello=" + $("#aModello:checked").val()
            + "&IsMasterSpecialistico=" + $("#pMasetr:checked").val()
            + "&IsCertificazione=" + $("#pCertificazione:checked").val()
            + "&IsGeneralContractor=" + $("#gereralContractor:checked").val()

            + "&IsProduttore=" + $("#produttore:checked").val()
            + "&IsDistributore=" + $("#distributore:checked").val()
            + "&IsPrestatoreDiServizi=" + $("#IsPrestatoreDiServizi:checked").val()
            + "&IsSediEstere=" + $("#sedEstera:checked").val()

            + "&IsNetwork=" + $("#netetwork:checked").val()
            + "&partecipazioneExpo=" + $("#partecipazioneExpo:checked").val()
            + "&partecipazioniInternazionali=" + $("#partecipazioniInternazionali:checked").val()


            ;

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc",
        data: par,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }
    });
}

function creaFormReti(lang, idImpresa, NRete, action) {
	//alert('Form Reti');
    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_RetiImprese.inc",
        data: "lang=" + lang + "&NRete=" + NRete + "&idimp=" + idImpresa + "&action=" + action,
        success: function(response) {
        	$("#formDatiRete").html(response);
        }
    });
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';
    //document.getElementById("box_popup").innerHTML = '';
    $("#box_popup").html('');
}

function chiudiUpload() {

    //    $("#coprente_upload").hide();
    document.getElementById("coprente_upload").style.display = 'none';
    $("#box_uploadfile").html('');
}

function deleteFile(filePath, myId, rootfiles, idimpresa) {
    if (confirm("Stai deselezionando una scelta abbinata ad un allegato, se procedi l'allegato verrà cancellato. Procedo?")) {


        var par = 'filePath=' + filePath + "&rootFile=" + rootfiles + "&nameinput=" + myId + "&idimpresa=" + idimpresa;

        $.ajax({
            type: "GET",
            async: false,
            url: "ajaxlib/requester/deleteFile.inc",
            data: par,
            success: function(response) {

                if (response == '0')
//                    if(myId=='masterF'){
//
//                        $("#pMasetr").removeAttr("checked");
//                    }else if(myId=='pCertificazione'){
//                        $("#pCertificazione").removeAttr("checked");
//                    }

                    $("#" + myId + "Delete").hide();
                $("#" + myId + "Upload").show();
            }
        });

    }
}
function controllaCategoriaCancel(myId, idImp, idcat, lang) {
    var par = 'categioriaID=' + myId + '&impresaID=' + idImp + "&idcat=" + idcat;
    if (confirm("Vuoi cancellare questa categoria?")) {

        if (myId === 'SEL') {
            var listaId = "";
            $("input[id^='categoriaCheckbox']:checked").each(function() {
                listaId += $(this).val() + "|"; 
            });
            par += "&listaId=" + listaId;
        }
        $.cookie('navCat', null);
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/controllo_prodotto_categoria.inc",
            data: par,
            success: function(response) {

                if (myId == 'ALL' || myId == 'SEL') {
                    var r = response.split("|");

                    if (r[1] != "") {
                        var arrayMyId = r[1].split(",");

                        infoError($("#IdImp").val(), "visualizza_EXPO_T_Impresa", "controllaCategoriaCancel1", "Lista Categorie Scheda Impresa");

                        var par1 = 'lang=' + $("#Lang").val() + '&Id=' + $("#IdImp").val();
                        $("#listadellecategorie").html('');
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: "ajaxlib/requester/partecipante_EXPO_T_Categorie.inc",
                            data: par1,
                            success: function(response) {

                                $("#listadellecategorie").html(response);
                            }
                        });
                        for (var i = 0; i < arrayMyId.length; i++) {
                            $("#categoriaMerc" + arrayMyId[i]).remove();
                        }

                    }
                    if (r[0] != "" ) {         
                        alert(_PRODOTTO_CATEGORIA_ + "\n\n" + r[0]);
                    }
                    
                } else {
                    if (response != "") {
                        alert(_PRODOTTO_CATEGORIA_ + "\n\n" + response);

                    } else {

                        infoError($("#IdImp").val(), "visualizza_EXPO_T_Impresa", "controllaCategoriaCancel2", "Lista Categorie Scheda Impresa");
                        var par1 = 'lang=' + $("#Lang").val() + '&Id=' + $("#IdImp").val();
                        $("#listadellecategorie").html('');
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: "ajaxlib/requester/partecipante_EXPO_T_Categorie.inc",
                            data: par1,
                            success: function(response) {

                                $("#listadellecategorie").html(response);
                            }
                        });
                        $("#categoriaMerc" + myId).remove();

                    }
                }

            }
        });
    }

}

function controllaCategoria(myElemen, myId, idImp, idcat, lang) {

    if (!myElemen.checked) {
        var par = 'categioriaID=' + myId + '&impresaID=' + idImp + "&idcat=" + idcat;
        if (confirm('Vuoi cancellare la categoria?')) {
            $.ajax({
                type: "POST",
                async: false,
                url: "ajaxlib/requester/controllo_prodotto_categoria.inc",
                data: par,
                success: function(response) {

                    if (response != "") {
                        alert(_PRODOTTO_CATEGORIA_ + "\n\n" + response);
                        myElemen.checked = true;
                    }
                }
            });
        }
    } else {
        var par1 = 'categioriaID=' + myId + '&impresaID=' + idImp + "&idcat=" + idcat;

        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/aggiornamento_categoria_azienda.inc",
            data: par1,
            success: function(response) {

                if (response != '') {
                    alert(response);
                    myElemen.checked = false;
                }

            }
        });
    }
    var par2 = 'lang=' + $("#Lang").val() + '&Id=' + $("#IdImp").val();
    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/get_lista_categorie.php",
        data: par2,
        success: function(response) {

            $("#listdellecategorie").html(response);

        }
    });

}

/*
 function visualizzaCategorie(idTable, lang) {
 
 var par = 'lang=' + lang + '&Id=' + idTable;
 
 
 $.ajax({
 type: "POST",
 async: false,
 url: "ajaxlib/requester/partecipante_EXPO_T_Categorie.inc",
 data: par,
 success: function(response) {
 
 $("#box_popup").html(response);
 $("#box_popup").html(response);
 document.getElementById("coprente").style.display = 'block';
 }
 });
 }
 */
function cancellaAzienda(idImpresa) {
    var par = 'Id=' + idImpresa;

    if (confirm("Sei sicuro di voler cancellare l'azienda?")) {
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/cancella_Impresa.inc",
            data: par,
            success: function(response) {

                window.location.replace('index.phtml?Id_VMenu=307');

            }
        });
    } else {
        return false;
    }

}

function salvaImpresa(iform, destinazione, stato) {
    //var iform = document.form_moduli;
    var iform = $("#form_moduli");
    destinazione = String(destinazione);

    if (valida(iform, stato)) {
        iform.attr('method', 'post');
        iform.attr('action', destinazione + "#companies-" + $("#menuTab").val());
        iform.submit();


    }

}

function salvaRete(iform, destinazione, stato) {
    //var iform = document.form_moduli;
    //alert('Entrato');
	var iform = $("#form_moduli");
    destinazione = String(destinazione);

    if (validaRete(iform, stato)) {
        iform.attr('method', 'post');
        iform.attr('action', destinazione + "#companies-4");
        iform.submit();
    }

}

$(document).ready(function() {

	

	$("#visualizza").hover(
		function () {
	        //mostra sottomenu
			$("input#password").prop('type', 'text');
     
		}, 
		function () {
			//nascondi sottomenu
			$("input#password").prop('type', 'password');       
		}

		
	);
});

function occhio() {

	if ($("input#password").val().length >0){
		$("#visualizza").show();
	} else {
		$("#visualizza").hide();
	}
}


