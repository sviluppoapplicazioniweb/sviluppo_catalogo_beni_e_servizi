
var divOld = "";
var imgOld = "";
function mostraDivScorrevole(IdTag) {
    var divId = "#divScorrevole" + IdTag;
    var imgId = "#imgTag" + IdTag;
    $(divOld).hide('fast');
    $(imgOld).attr("src", "images/admin/piu.gif");
    if (divOld != divId) {
        $(divId).css({"text-align": "left"});
        $(divId).animate({"height": "toggle"}, {duration: 1000});
        $(imgId).attr("src", "images/admin/meno.gif");
        divOld = divId;
        imgOld = imgId;
    } else {
        divOld = "";
        imgOld = "";
    }

}


function valida(iform) {

    var formDescrizione = $("#messaggio").val();


    if (formDescrizione === "") {
        alert(_CAMPI_OB_);
        return false;
    }
    else {
        return true;
    }
}

function showMail(idTag){
        $(idTag).toggle();
}