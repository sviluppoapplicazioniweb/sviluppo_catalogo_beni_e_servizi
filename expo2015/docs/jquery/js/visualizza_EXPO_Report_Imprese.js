$(document).ready(function() {


    $("#dataInizio").datepicker({
        showOn: "button",
        buttonImage: "/images/calendar.gif",
        buttonImageOnly: true
    });
    $("#dataFine").datepicker({
        showOn: "button",
        buttonImage: "/images/calendar.gif",
        buttonImageOnly: true
    });
    $("#dataFine").change(function() {

        if ($("#dataInizio").val() !== "") {
            if ((new Date($("#dataInizio").val()).getTime() > new Date($("#dataFine").val()).getTime())) {
                $("#dataFine").val("");
                alert("Selezionare una data successiva alla prima.");
            }
        } else {
            $("#dataFine").val("");
            alert("Selezionare prima data inizio.");
        }
    });
});
function invia(tipo) {
    
    if (tipo === "periodo") {
        $("#dataInizio").val("");
        $("#dataFine").val("");
    } else {
        $("#periodo").val("");
    }

    var par = "tab=iscrizioni&inizio=" + $("#dataInizio").val() + "&fine=" + $("#dataFine").val() + "&periodo=" + $("#periodo").val();

    $.ajax({
        type: "POST",
        async: false,
        dataType: 'html',
        url: "ajaxlib/requester/EXPO_Report_Impresa.inc",
        data: par,
        success: function(response) {
            $("#iscrizioni").html(response);
        }
    });
}


var tabAppo = "";
var o = "";
function ordina(tipo, idTag) {
    if (tabAppo !== idTag) {
        tabAppo = idTag;
        o = "ASC";
    } else {
        if (o !== "ASC") {
            o = "ASC";
        } else {
            o = "DESC";
        }
    }
    var par = "campo=" + tipo + "&ord=" + o + "&tab=" + idTag;
    if ($('#dataInizio').val() !== "" && $('#dataFine').val() !== "") {
        par += "&inizio=" + $('#dataInizio').val() + "&fine=" + $('#dataFine').val();
    } else if ($('#periodo').val() !== "") {
        par += "&periodo=" + $('#periodo').val();
    }
    $.ajax({
        type: "POST",
        async: false,
        dataType: 'html',
        url: "ajaxlib/requester/EXPO_Report_Impresa.inc",
        data: par,
        success: function(response) {
            $("#" + idTag).html(response);
        }
    });
}

     