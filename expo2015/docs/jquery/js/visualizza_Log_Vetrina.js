document.writeln("<script type='text/javascript' src='/jquery/jquery.currency.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse_cookie.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/jquery.tablesorter.js'></script>");

$(document).ready(function() {
    //collapsible management
    $.cookie('nav', null);
    $('.collapsible').collapsible({
        defaultOpen: '',
        cookieName: 'nav',
        speed: 'slow'
    });

    $("#dataInizio").datepicker({
        showOn: "button",
        buttonImage: "/images/calendar.gif",
        buttonImageOnly: true
    });
    $("#dataFine").datepicker({
        showOn: "button",
        buttonImage: "/images/calendar.gif",
        buttonImageOnly: true
    });
    $("#dataFine").change(function() {

        if ($("#dataInizio").val() !== "") {
            if ((new Date($("#dataInizio").val()).getTime() > new Date($("#dataFine").val()).getTime())) {
                $("#dataFine").val("");
                alert("Selezionare una data successiva alla prima.");
            }
        } else {
            $("#dataFine").val("");
            alert("Selezionare prima data inizio.");
        }
    });
    // call the tablesorter plugin 
    $("table[name^='tabella']").tablesorter();


    $("#data").click(function() {
        $("#periodo").val("");
        $("#form_filtro").attr("action", "")
                .attr("method", "post");
        $("#form_filtro").submit();
    });
    
    $("#data2").click(function() {
         $("#dataInizio").val("");
        $("#dataFine").val("");
        $("#form_filtro").attr("action", "")
                .attr("method", "post");
        $("#form_filtro").submit();
    });
    
    $("#csv").click(function() {

        $("#form_filtro").attr("action", $("#urlCSV").val())
                .attr("method", "post");
        $("#form_filtro").submit();
    });

});
