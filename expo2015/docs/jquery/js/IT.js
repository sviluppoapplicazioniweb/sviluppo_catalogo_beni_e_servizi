var _CAMPI_OB_ = "I parametri contrassegnati con l'asterisco sono obbligatori.";
var _CAMPI_PAS_NO_ = "ATTENZIONE!! Hai inserito una password non conforme ai criteri di sicurezza. Sono valide le password che hanno una lunghezza compresa tra gli 8 e i 16 caratteri e contengono almeno una lettera e un numero.";
var _CAMPI_PAS_UG_ = "Password non corrisponde a quella di conferma";
var _EMAIL_ER_ = "Campo E-Mail non valido";
var _EMAIL_CON_ER_ = "E-Mail non corrispondono";
var _CANCELLA_PROD_ = "Sei sicuro di voler cancellare il Prodotto/Servizio?  La cancellazione sarà irreversibile e eliminerà tutti i documenti precedentemente caricati.";
var _VALIDAZIONE_PROD_HEADER_ = "Attenzione! Non è possibile aggiornare la Scheda Prodotto: ";
var _PRODOTTO_CATEGORIA_ = "La categoria che stai deselezionando risulta associata ai seguenti prodotti:";

var _CAMPI_OB_NOME_ = "- Il Nome del prodotto è obbligatorio;";
var _CAMPI_OB_DESCRIZIONE_PRODOTTO_ = "- La descrizione del prodotto è obbligatoria;";
var _CAMPI_OB_CERTIFICAZIONE_PRODOTTO_ = "- La descrizione riguardante le certificazioni e\o riconoscimenti del Prodotto deve essere presente nelle tre lingue previste;";
var _CAMPI_OB_ASPETTI_PRODOTTO_ = "- La descrizione riguardante gli aspetti di innovazione e sostenibilit&agrave; del Prodotto deve essere presente nelle tre lingue previste;";
var _CAMPI_OB_BREVETTI_PRODOTTO_ = "- La descrizione riguardante i brevetti del Prodotto deve essere presente nelle tre lingue previste;";
var _CAMPI_OB_C_MERC_PRODOTTO_ = "- Il prodotto deve essere associato ad almeno una Categoria Merceologica";
var _MOD_PRODOTTO_ = "Attenzione:\nLa seguente funzionalità pone il Prodotto in modalità modifica. Lo stesso rimarrà pertanto invisibile ai Partecipanti fino all'azione di '*Salva & pubblica*'.\nConfermi?";
var _MOD_IMPRESA_ = "Attenzione:\nLa seguente funzionalità pone l'Impresa in modalità modifica. La stessa rimarrà pertanto invisibile ai Partecipanti fino all'azione di '*Salva & pubblica*'.\nConfermi?";

var _VALIDAZIONE_HEADER_ = "Attenzione! Non è possibile pubblicare la Scheda Impresa: \n";
var _CAMPI_OB_QUALITA_ = "- La descrizione riguardante l\'attenzione al tema della sostenibilit&agrave; Green e Sociali deve essere presente nelle tre lingue previste;";
var _CAMPI_OB_EMAIL_ = "- Il campo E-Mail è obbligatorio;";
var _CAMPI_OB_DESCRIZIONE_ = "- La Descrizione dell\'attività dell\'Impresa è obbligatoria;";
var _ALERT_HOME_ = "Stai abbandonando il Catalogo Fornitori per essere reindirizzato sulla home page di Expo 2015.\nConfermi?";

var _ALERT_SMARTCARD_ = "Attenzione se questo computer non è configurato correttamente, proseguendo il sistema potrebbe non funzionare.\nPer una corretta configurazione fai riferimento alla guida presente in homepage al punto 1.\n\nContinuare?";