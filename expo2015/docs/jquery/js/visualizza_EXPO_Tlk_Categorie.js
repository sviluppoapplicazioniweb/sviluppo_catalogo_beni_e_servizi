function popup_categorie(pagina)
{
    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/gestione_EXPO_Tlk_Categorie.inc",
        data: pagina,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});
}
function popup_cancella(pagina) {
    if (confirm("Verranno cancellate anche eventuali sotto categoria.\nConfermi l'operazione?")) {
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/gestione_EXPO_Tlk_Categorie.inc",
            data: pagina,
            success: function(response) {
                if (response.trim() == "0") {
                    document.getElementById("coprente").style.display = 'none';
                    $("#box_popup").html('');
                    window.location.replace(window.location.href);
                } else {
                    $("#box_popup").html(response);
                    document.getElementById("coprente").style.display = 'block';
                }
            }});
    }
}
function ordina() {
    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/gestione_EXPO_Tlk_Categorie.inc",
        data: "azione=UPDCAT&val=" + $('#nestable-output').val(),
        success: function(response) {
            window.location.replace(window.location.href);
        }});
}
function salva() {
    var formDescrizione = $("#Descrizione").val();
    var formDescrizione_AL = $("#Descrizione_AL").val();
    var formDescrizione_AL2 = $("#Descrizione_AL2").val();
    var formAbilitato = $("input[name='abilitato']:checked").val();

    if (formDescrizione === ""
            || (formDescrizione_AL != undefined && formDescrizione_AL === "")
            || (formDescrizione_AL2 != undefined && formDescrizione_AL2 === "")
            || ($("input[name='abilitato']").length > 0 && formAbilitato == undefined)) {
        alert(_CAMPI_OB_);
        return false;
    }

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/gestione_EXPO_Tlk_Categorie.inc",
        data: "azione=INS&Id=" + $("#Id").val() + "&abilitato=" + formAbilitato + "&Id_Categoria=" + $("#Id_Categoria").val() + "&Descrizione=" + formDescrizione + "&Descrizione_AL=" + formDescrizione_AL + "&Descrizione_AL2=" + formDescrizione_AL2,
        success: function(response) {
            window.location.replace(window.location.href);
        }
    });
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';
    $("#box_popup").html('');
}

$(document).ready(function() {
    $('#nestable3').nestable('collapseAll');
});