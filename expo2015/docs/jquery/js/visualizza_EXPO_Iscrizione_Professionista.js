/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {

    $("#titoloBarra").html($("#titoloBarraAppend").val());
    
    $("[name=alboProfessionista]").change(function() {
    	if ($("[name=alboProfessionista]").data("type") >= "4") {
    		$("#avanti").show();
    		
    	}/*if ($("[name=alboProfessionista]").data("type") == "0") {
    		$("#avanti").hide();
    	}else{
    		$("#avanti").hide();
    	}*/
    	
    });

    
    $("input[name='IdQuestionario']").change(function() {
    	
    	var altro = $("#IdQuestionarioAltro").val();
    	var idQuestionario = $("input[name='IdQuestionario']").filter(':checked').val();	
    	//alert('ciao  '+$("input[name='IdQuestionario']").filter(':checked').val());
    	
    	if (idQuestionario == altro){
    		$("#noteAltro").show();
    	}else{
    		$("#noteAltro").hide();
    	}
    	
    });
   
    
    
    $("[name=CodiceFiscale]").change(function() {
    	
    	//alert('ciao ');
    	if (!(validaCodiceFiscale($("[name=CodiceFiscale]").val()) || (ControllaPIVA($("[name=CodiceFiscale]").val())))){
    		alert('Il codice Fiscale non è formalmente corretto');
    	}
    });
   
    $("[name=PartitaIVA]").change(function() {
    	
    	//alert('ciao');
    	if (!(ControllaPIVA($("[name=PartitaIVA]").val()))){
    		alert('La Partita IVA non è formalmente corretta');
    	}
    });
    
    
$("[name=CodFiscale]").change(function() {
    	
    	//alert('ciao ');
    	if (!(validaCodiceFiscale($("[name=CodFiscale]").val()) || (ControllaPIVA($("[name=CodFiscale]").val())))){
    		alert('Il codice Fiscale non è formalmente corretto');
    	}
    });
   
    $("[name=ParIVA]").change(function() {
    	
    	//alert('ciao');
    	if (!(ControllaPIVA($("[name=ParIVA]").val()))){
    		alert('La Partita IVA non è formalmente corretta');
    	}
    });
    
    
    $("[name=professionista]").change(function() {
    		$("#avanti").show();
    });
    
    $("input[name='tipoPagamentoRadio']").change(function() {

        if ($("input[name='tipoPagamentoRadio']").filter(':checked').val() == 1) {
            creaFormPagamentoMyBank($("#cfimpresa").val(), $("#fasciadifatturato").val(),$("#uid").val(),$("#OrdineProfessionista").val());
            $("#formPagamento").html("");
        } else {
            creaFormPagamento($("#cfimpresa").val(), $("#fasciadifatturato").val(),$("#uid").val(),$("#OrdineProfessionista").val());
            $("#formPagamentoMyBank").html("");
        }
    });

    $("#datiFatturazione").hide();
    $("input[name='sedeLegaleRadio']").change(function() {
        if ($("input[name='sedeLegaleRadio']").filter(':checked').val() == 1) {
            $("#datiFatturazione").toggle();
        } else {
            $("#datiFatturazione").hide();
        }
    });
    
    $("input:file").change(function() {
        var fileName = $(this).val();

        fileName = fileName.split('\\');
        $("#testofilename" + $(this).attr('name')).html(fileName[fileName.length - 1]);
    });

});

function creaFormPagamento(cfimpresa, fascia,uid,ordineProfessionista) {
	
    if (fascia > 0) {
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/payment_system.php?action=normal",
            data: 'fascia=' + fascia + "&impresa=" + cfimpresa + "&uid=" + uid + '&ordineProfessionista=' + ordineProfessionista,
            success: function(response) {

                if (response == 'ok') {
                    window.location.replace("index.phtml?Id_VMenu=507&azione=PAY&cfimpresa=" + cfimpresa + '&ordineProfessionista=' + ordineProfessionista + "");
                    return;
                } else {
                    $("#formPagamento").html(response);
                }


            }
        });
    } else {

        $("#formPagamento").html('');
    }

}

function creaFormPagamentoMyBank(cfimpresa, fascia,uid,ordineProfessionista) {
	
    if (fascia > 0) {

        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/payment_system.php?action=mybank",
            data: 'fascia=' + fascia + "&impresa=" + cfimpresa + "&uid=" + uid + '&ordineProfessionista=' + ordineProfessionista,
            success: function(response) {
                if (response == 'ok') {
                    window.location.replace("index.phtml?Id_VMenu=507&azione=PAYMYBANK&cfimpresa=" + cfimpresa + '&ordineProfessionista=' + ordineProfessionista + "");
                    return;
                } else {
                    $("#formPagamentoMyBank").html(response);
                }


            }
        });
    } else {

        $("#formPagamento").html('');
    }

}

function emailCheck(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

function valida(iform, stato) {

    var formImpresa = $("#professionista").val();
    var formFasciaFatturato = $("#fasciafatturato").val();

    if (formImpresa != undefined && formImpresa == "") {
        alert(_CAMPI_OB_);
        return false;
    } else if (formFasciaFatturato !== undefined) {

    	var formEmailFatturazione = $("#EmailFatturazione").val();
    	
    	var formDenominazione = $("#RagioneSociale").val();
        var formCodFiscale = $("#CodiceFiscale").val();
        var formParIVA = $("#PartitaIVA").val();
        
        var formVia = $("#Indirizzo").val();
        var formCAP = $("#CAP").val();
        var formPRV = $("#Provincia").val();
        var formComune = $("#Comune").val();
        
        if (formDenominazione == "" || formCodFiscale == "" || formVia == "" || formParIVA == "" || formCAP == "" || formPRV == "" || formComune == "") {
             alert(_CAMPI_OB_ +"sda");
             return false;
        } else if (formEmailFatturazione == "") {
            alert(_CAMPI_OB_ + " - Mail Fatturazione");
            return false;
        } /*else
        	if (emailCheck(formEmailFatturazione) == false) {
        		alert("L'indirizzo E-mail è errato.");
        		f.e_mail.focus();
        		return (false);
        	} */else if (formFasciaFatturato == "0") {
        		alert(_CAMPI_OB_ + " - Fascia di Fatturato");
        		return false;
        	} else if ($("input[name='sedeLegaleRadio']").is(':checked') === false) {
            alert(_CAMPI_OB_);
            return false;
        } else if ($("input[name='sedeLegaleRadio']").filter(':checked').val() == 1) {
            var formDenominazione = $("#Denominazione").val();
            var formCodFiscale = $("#CodFiscale").val();
            var formVia = $("#Via").val();
            var formParIVA = $("#ParIVA").val();
            var formCAP = $("#CAP").val();
            var formPRV = $("#PRV").val();
            var formComune = $("#Comune").val();
            if (formDenominazione == "" || formCodFiscale == "" || formVia == "" || formParIVA == "" || formCAP == "" || formPRV == "" || formComune == "") {
                alert(_CAMPI_OB_);
                return false;
            }
        }
    }


    return true;
}

function startTimeout() {

    var cfimpresa = $("#cfimpresa").val();
    var ordineProfessionista = $("#OrdineProfessionista").val();
    var fascia = $("#fasciadifatturato").val();
    var securitytoken = $("#securitytoken").val();
    var paymentid = $("#paymentid").val();
    var orderid = $("#orderid").val();
    var amount = $("#amount").val();
    var description = $("#description").val();
    var optype = $("#optype").val();
    //alert('optype '+optype);
    
    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/start_timeout.php",
        data: 'optype=' + optype + '&description=' + description + '&amount=' + amount + '&orderid=' + orderid + '&fascia=' + fascia + "&impresa=" + cfimpresa + "&securitytoken=" + securitytoken + "&paymentid=" + paymentid + '&ordineProfessionista=' + ordineProfessionista,
        success: function(response) {

        }
    });
}

function compileSelect(par, valueSelect,tag) {
    var formTag = tag;
	$.ajax({
        type: "POST",
        async: false,
        dataType: 'json',
        url: "ajaxlib/requester/select_provincia_comune.inc",
        data: 'par=' + par + "&valueSelect=" + valueSelect,
        success: function(response) {
            $("#" + formTag).html(response.html);

        }
    });
}

function autocertificazione(azione, idProf,idOrdine) {

    if (azione === "INS") {

      /*  var oMyForm = new FormData();
        oMyForm.append("idProf", idProf);
        oMyForm.append("idOrdine", idOrdine);
        oMyForm.append("action", azione);
        oMyForm.append("lang", $("#InfoLang").html());
        oMyForm.append("logoF", document.getElementById("logoF").files[0]);
        $.ajax({
            url: "ajaxlib/requester/upload_autocertificazione.inc",
            type: "POST",
            processData: false,
            contentType: false,
            data: oMyForm,
            success: function(response) {
                document.getElementById("coprente").style.display = 'none';
                x = $.parseJSON(response);

                if (x.error !== '') {
                    alert(x.msg);
                } else {
                    $("#logoImg").html('<img src="' + x.msg + '" draggable="false"/><br><?php echo _C_LOGO_; ?>');
                }

            }
        });
*/
    	alert ("ins");
    } else {
        var par = 'idProf=' + idProf +'&idOrdine=' + idOrdine + '&action=' + azione + '&lang=' + $("#InfoLang").html();
        $.ajax({
            type: "POST",
            async: false,
            url: "ajaxlib/requester/persona_upload_autocertificazione.inc",
            data: par,
            success: function(response) {
                $("#box_uploadfile").html(response);
                //document.getElementById("box_popup").innerHTML = response;
                document.getElementById("coprente_upload").style.display = 'block';

            }
        });
    }

}

function chiudiUpload() {

    //    $("#coprente_upload").hide();
    document.getElementById("coprente_upload").style.display = 'none';
    $("#box_uploadfile").html('');
}

//
//function creaFormPagamentoMyBank(cfimpresa, fascia) {
//
//    if (fascia > 0) {
//        $.ajax({
//            type: "POST",
//            async: false,
//            url: "ajaxlib/requester/payment_system.php?action=mybank",
//            data: 'fascia=' + fascia + "&impresa=" + cfimpresa,
//            success: function(response) {
//
//                if (response == 'ok') {
//                    window.location.replace("index.phtml?Id_VMenu=507&azione=PAY&cfimpresa=" + cfimpresa + "");
//                    return;
//                } else {
//                    $("#formPagamentoMyBank").html(response)
//                }
//
//
//            }
//        });
//    } else {
//
//        $("#formPagamento").html('');
//    }
//
//}
