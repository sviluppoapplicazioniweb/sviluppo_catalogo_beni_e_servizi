
function valida(iform) {

    var formNome = $("#Nome").val();
    var formCognome = $("#Cognome").val();
    var formTelefono = $("#Telefono").val();
    //var formFax = $("#Fax").val();
    var formEmail = $("#E_Mail").val();

    if (formNome == "" || formCognome == "" || formTelefono == "" || formEmail == "") {
        alert(_CAMPI_OB_);

        return false;
    } else if (!emailCheck(formEmail)) {
        alert(_EMAIL_ER_);
        return false;
    }

    return true;

}


$(document).ready(function() {

    $("#Fax").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43)
            return false;
        return true;
    });
    $("#Telefono").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43)
            return false;
        return true;
    });
});

function emailCheck(emailStr) {
 
    var emailPat = /^(.+)@(.+)$/;
    var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars = "[^\\s" + specialChars + "]";
    var quotedUser = "(\"[^\"]*\")";
    var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom = validChars + "+";
    var word = "(" + atom + "|" + quotedUser + ")";
    var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
    var matchArray = emailStr.match(emailPat);

    if (matchArray == null) {
        return false;

    }

    var user = matchArray[1];
    var domain = matchArray[2];

    if (user.match(userPat) == null) {
        return false;

    }

    var IPArray = domain.match(ipDomainPat);

    if (IPArray != null) {

        for (var i = 1; i <= 4; i++) {

            if (IPArray[i] > 255) {
                return false;

            }

        }

        return true;

    }

    var domainArray = domain.match(domainPat);

    if (domainArray == null) {
        return false;

    }

    var atomPat = new RegExp(atom, "g");
    var domArr = domain.match(atomPat);
    var len = domArr.length;

    if (domArr[domArr.length - 1].length < 2 ||
            domArr[domArr.length - 1].length > 6) {
        return false;

    }

    if (len < 2) {
        return false;
    }

    return true;

}


