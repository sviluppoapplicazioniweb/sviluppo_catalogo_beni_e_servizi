document.writeln("<script type='text/javascript' src='/jquery/jquery.currency.js'></script>");


function valida(iform, stato) {

    if (stato == 0) {
        var formValore = $("#valore").val();
        if (formValore === "") {
            alert(_CAMPI_OB_);
            return false;
        }
    } else if (stato == 2) {
        var formTesto =  $("#testo").val();;
        if (formTesto === "") {
            alert(_CAMPI_OB_);
            return false;
        }

    }
    return true;
}

function visualizzaBOX(tipo, idTable) {
    var par = 'lang=' + $("#InfoLang").html() + '&Id=' + idTable + "&bview=NO";

    var url = '';

    if (tipo === "PRT") {
        url = "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc";
    } else {
        url = "ajaxlib/requester/partecipante_EXPO_T_Imprese.inc";
    }


    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});
}


function visualizza(idTrat) {
    var par = 'lang=' + $("#InfoLang").html() + '&Id=' + idTrat;
    var url = "ajaxlib/requester/visualizza_Trattativa.inc";
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: par,
        success: function(response) {
                $("#box_popup").html(response);
                document.getElementById("coprente").style.display = 'block';
        }});
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';
}

$(document).ready(function() {
    $("#valore").keypress(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });
    $("#valore").blur(function() {
        $(this).currency({region: '', thousands: '.', decimal: ',', decimals: 2});
    });

});