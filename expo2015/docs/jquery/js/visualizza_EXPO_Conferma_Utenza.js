
function valida(iform) {
	var pattern_email=/((?=.*\d)(?=.*[a-zA-Z])).{8,16}/;
	var pattern_email_NoSpecialChar=/[@%;]/;
    var formPassword = $("#Password").val();
    var formRep_Password = $("#Rep_Password").val();

    if (formPassword == "" || formRep_Password == "") {
        alert(_CAMPI_OB_);
        return false;
    } else if (formPassword.length < 8 || formPassword.length > 16) {
        alert(_CAMPI_PAS_NO_);
        return false;
    } else if (formPassword != formRep_Password) {
        alert(_CAMPI_PAS_NO_);
        return false;
    } else if(pattern_email.test(formPassword) && !pattern_email_NoSpecialChar.test(formPassword)){//
			return true;
		}else{
			alert(_CAMPI_PAS_NO_);
			return false;
		  }
    alert('OK');
    return true;
}



