
var arrayLingue = null;

$(document).ready(function() {
    arrayLingue = $("#listaLingue").val().split('|');
    //collapsible management
    $.cookie('nav', null);
    $('.collapsible').collapsible({
        defaultOpen: '',
        cookieName: 'nav',
        speed: 'slow'
    });

    $("input[name$='L']").focus(function() {
        if ($(this).val() == "http://") {
            $(this).val("");
            $(this).css("color", "#000000");
        }
    });
    $("input[name$='L']").blur(function() {
        if ($(this).val() == "") {
            $(this).val("http://");
            $(this).css("color", "#999999");
        }
    });

    $("input[name$='L']").each(function() {
        if ($(this).val() == "http://") {
            $(this).css("color", "#999999");
        }
    });


    $("input:file").change(function() {
        var fileName = $(this).val();

        fileName = fileName.split('\\')
        $("#testofilename" + $(this).attr('name')).html(fileName[fileName.length - 1]);
    });

});

$(function() {
    $("#tabs").tabs();

    if (document.getElementById("StatoProdotto").value == "A") {
        var tag = 'input[type="text"],textarea,input[type="checkbox"],input[type="file"],select';
        $(tag).attr('disabled', 'disabled');
        $(tag).css("cursor", "default");
        $("[id^='charsRemaining']").hide();
        $('.upload-file-container_edit').attr("class", 'upload-file-container');
        $('a[name="link_form"]').css("cursor", "default");
        //$('a[name="link_form"]').css("background", "#666666");
        $('a[name="link_form"]').click(function() {
            return false;
        });
    }
});

function controlloCampiOb(idCampo) {

    for (var i = 0; i < arrayLingue.length; i++) {
        if (document.getElementById(idCampo + arrayLingue[i]).value == "") {
            return false;
        }
    }
    return true;
}

function controlloCampiTr(idCampo) {
    var er = 0;
    
    for (var i = 0; i < arrayLingue.length; i++) {
        if (document.getElementById(idCampo + arrayLingue[i]).value != "") {
            er++;
        }
    }
    if (er > 0 && er < arrayLingue.length)
        return false;

    return true;
}


function valida(iform) {

	var formOrdine = $("#Ordine").val();
    
	
    var testoMsgHeader = _VALIDAZIONE_PROD_HEADER_;
    var testoMsgNomeProdotto = _CAMPI_OB_NOME_;
    var testoMsgDescrizioneProdotto = _CAMPI_OB_DESCRIZIONE_PRODOTTO_;
    var testoMsgCertificazioneProdotto = _CAMPI_OB_CERTIFICAZIONE_PRODOTTO_;
    var testoMsgAspettiProdotto = _CAMPI_OB_ASPETTI_PRODOTTO_;
    var testoMsgBrevettiProdotto = _CAMPI_OB_BREVETTI_PRODOTTO_;
    var testoMsgCategorieProdotto = _CAMPI_OB_C_MERC_PRODOTTO_;

    var testoMsg = "";

    if (!controlloCampiOb("Nome"))
        testoMsg = testoMsgNomeProdotto;

    if (!controlloCampiOb("descrizione"))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgDescrizioneProdotto;

    if (!controlloCampiTr("certificazione"))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgCertificazioneProdotto;

    if (!controlloCampiTr("aspetti"))
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgAspettiProdotto;

    if (formOrdine == 0){
	    if (!controlloCampiTr("brevetti"))
	        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgBrevettiProdotto;
	}
    if (!$("input[type=checkbox]:checked").val())
        testoMsg = testoMsg + ((testoMsg != "") ? "\n" : "") + testoMsgCategorieProdotto;

    if (testoMsg != '') {

        testoMsg = testoMsgHeader + "\n" + testoMsg;

        alert(testoMsg);
        return false;

    } else {

        return true;

    }

}


function visualizzaBOX(idTable) {
	
    var par = 'lang=' + $("#InfoLang").html() + '&Id=' + idTable
		    + "&Ordine=" + $("#Ordine").val()
			+ "&IsStudio=" + $("#IsStudio").val()
            + "&Nome=" + $("#Nome").val()
            + "&NomeEN=" + $("#NomeEN").val()
            + "&NomeFR=" + $("#NomeFR").val()

            + "&descrizione=" + $("#descrizione").val()
            + "&descrizioneEN=" + $("#descrizioneEN").val()
            + "&descrizioneFR=" + $("#descrizioneFR").val()
            + "&prodottoL=" + $("#prodottoL").val()

            + "&certificazione=" + $("#certificazione").val()
            + "&certificazioneEN=" + $("#certificazioneEN").val()
            + "&certificazioneFR=" + $("#certificazioneFR").val()
            + "&certificazioneL=" + $("#certificazioneL").val()

            + "&aspetti=" + $("#aspetti").val()
            + "&aspettiEN=" + $("#aspettiEN").val()
            + "&aspettiFR=" + $("#aspettiFR").val()
            + "&aspettiL=" + $("#aspettiL").val()
            + "&isSiexpo=" + $("#isSiexpo:checked").val()

            + "&brevetti=" + $("#brevetti").val()
            + "&brevettiEN=" + $("#brevettiEN").val()
            + "&brevettiFR=" + $("#brevettiFR").val()
            + "&brevettiL=" + $("#brevettiL").val();

    $.ajax({
        type: "POST",
        async: false,
        url: "ajaxlib/requester/partecipante_EXPO_T_Prodotti.inc",
        data: par,
        success: function(response) {
            $("#box_popup").html(response);
            document.getElementById("coprente").style.display = 'block';
        }});
}

function chiudi() {
    document.getElementById("coprente").style.display = 'none';
    document.getElementById("box_popup").innerHTML = '';
}

function deleteFile(filePath, myId, rootfiles, idimpresa) {
    if (confirm("Stai deselezionando una scelta abbinata ad un allegato, se procedi l'allegato verrà cancellato. Procedo?")) {


        var par = 'filePath=' + filePath + "&rootFile=" + rootfiles + "&nameinput=" + myId + "&idimpresa=" + idimpresa;

        $.ajax({
            type: "GET",
            async: false,
            url: "ajaxlib/requester/deleteFile.inc",
            data: par,
            success: function(response) {

                if (response == '0')
//                    if(myId=='masterF'){
//
//                        $("#pMasetr").removeAttr("checked");
//                    }else if(myId=='pCertificazione'){
//                        $("#pCertificazione").removeAttr("checked");
//                    }

                    $("#" + myId + "Delete").hide();
                $("#" + myId + "Upload").show();
            }
        });

    }
}

function contacaratteri(id) {
    var max = parseInt($("#" + id).attr('maxlength'));

    if ($("#" + id).val().length > max) {
        $("#" + id).val($("#" + id).val().substr(0, $("#" + id).attr('maxlength')));
    }
    $("#charsRemaining" + id).html('' + (max - $("#" + id).val().length) + ' caratteri rimanenti');
}

function cancella(iform, destinazione) {

    if (confirm(_CANCELLA_PROD_)) {


        iform.method = "post";
        iform.action = destinazione;
        iform.submit();
    }


}

document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse.js'></script>");
document.writeln("<script type='text/javascript' src='/jquery/collapse/jquery.collapse_cookie.js'></script>");


