<?php
mb_internal_encoding("UTF-8");
header_remove('x-powered-by');
header('X-Frame-Options: deny');
header('X-XSS-Protection: 1; mode=block');
header('X-Content-Type-Options: nosniff');
header('X-Permitted-Cross-Domain-Policies:master-only');


session_start();
if (strcmp($_POST["submit"],"LOGOUT")==0){
	if ($_SESSION["sessionid"]==session_id()){
		session_destroy();
		header ("reportIscrizioneLogin.php");
	}
}

if ($_SESSION["sessionid"]!=session_id()){
    session_destroy();
    header ("Location: reportIscrizioneLogin.php");    
}
else{
	
?>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="refresh" content="3600">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Report Iscrizioni</title>



<script type="text/javascript" src="/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/jquery/ui/jquery-ui.js"></script>


<style type="text/css">
body {
	background: url(/tmpl/expo2015/catalogo/images/bg.gif) repeat-y scroll 0
		0 transparent;
	color: #333333;
	padding: 0;
	text-align: center;
	margin: 0;
	height: 100%;
	font-family: Helvetica;
	font-size: 12px;
}

div#container {
	max-width: 940px;
	margin: 0 auto;
	text-align: left;
	margin-bottom: 10px;
	display: block;
	min-height: 100%;
	height: auto;
	height: 100%;
}

div#content {
	width: 90%;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10px;
	margin-bottom: 10px;
}

h1{
	text-align: center;
	color: #00aeef;
}

h2 {
	text-align: center;
	
}

table {
	border-top-right-radius: 8px;
	border-top-left-radius: 8px;
	margin: 2px;
	padding: 3px 5px;
	border: #00aeef;
	background-color: #00aeef;
	width: 100%;
	margin-bottom: 30px;
	text-align: center;
}

thead {
	border-top-right-radius: 8px;
	border-top-left-radius: 8px;
	margin: 2px;
	padding: 3px 5px;
	color: #fff;
	text-align: center;
	font-weight: bold;
	background-color: #00aeef;
}

table td {
	padding: 5px;
}

.total {
	background-color: #0050D1;
	color: white;
	padding: 5px;
}

.today,.beforeYesterday,.yesterday,.regione,.accessi  {
	width: 35%;
	background-color: white;
	padding: 5px;
}

.categorie td {
	width: 80%;
	background-color: white;
	padding: 5px;
	
}

.Nord,.Centro,.Sud {
	width: 35%;
	background-color: white;
	padding: 5px;
}

.table td {
	width: 10%;
}

.questionariTesto {
	text-align: left;
	background-color: white;
	padding: 5px;
}

.questionariNumRisposte{
	
	background-color: white;
	padding: 5px;
	width: 15%;
}
.id {
	font-size: 10px;
}

.accessi td {
	width: auto;
	font-size: 13px;
	padding: 5px;
}


#tabs ul {


}

#tabs li {
display:inline-block;
padding: 12px 3px;
}
#tabs a {
margin-bottom: 10px;
padding: 10px;

color: white;
background-color: #00aeef;
text-decoration: none;
text-transform: uppercase;
font-weight: bold;
}

#tabs a.sel{
padding: 10px;


color: white;
background-color: blue;
text-decoration: underline;
text-transform: uppercase;
font-weight: bold;
}
</style>

<script type="text/javascript">

$(document).ready(function() {
	
	for (var i = 2; i < 7; i++){ 
		$("#tab"+i).hide();
	}
});

	function showTab(tab){
		$("a").removeClass("sel");
		$("a[name='tab"+tab+"']").addClass("sel");
		$("#tab"+tab).show();
		for (var i = 1; i < 7; i++){ 
			if (tab != i){
			$("#tab"+i).hide();
			}
		}	
	}		
	

</script>
</head>

<body>
	
			<form style="float: right; display: inline;" name="login" id="form" method="post" action="reportIscrizione.php">
			    
			    	<button  type="submit" name="submit" value="LOGOUT"/>Logout</button>
			    
			</form>
	<div id="container">
		<div id="content">
			<h1>Reportistica Iscrizioni</h1>
			 <ul id="tabs">
		        <li><a href="javascript:showTab(1)" name="tab1" >Imprese Iscritte</a></li>
		        <li><a href="javascript:showTab(2)" name="tab2" >Persone Iscritte</a></li>
		        <li><a href="javascript:showTab(3)" name="tab3" >Imprese Iscritte per province</a></li>
		        <li><a href="javascript:showTab(4)" name="tab4" >Categorie Merceologiche</a></li>
		        <li><a href="javascript:showTab(5)" name="tab5" >Vetrina</a></li>
		        <li><a href="javascript:showTab(6)" name="tab6" >Questionari</a></li>
    		</ul>
    		
			<?php
			include "reportIscrizione.class.php";
			$report = new ReportIscrizione();
			
			?>
			<div id="tab1" >
			<?php 
				$report->impreseIscritte(true); 
				$report->professionistiIscritti(true);
				$report->impreseIscrittePerMacroRegioni(true);
				
				$regioni = $report->impreseIscrittePerRegioni(true);
				
				$report->mappaRegioni($regioni);
			?>
			</div>
			
			<div id="tab2">
			<?php 
				$report->legaliRappresentantiIscritti(true); 
				$report->delegatiIscritti(true);
				$report->delegatiIscrittiRead(true);
				?>
			</div>

		
			<div id="tab3">
			<?php
								
				$report->impreseIscrittePerProvinceMacroAree(true);
				$report->mappaLombardia();
			?>
			</div>
			
			<div id="tab4">
			<?php
				$report->macroCategorie();	
				$report->categorie();
			?>
			</div>
				
			<div id="tab5">
			<?php
				$report->logAccessiVetrina();
				$report->accessiUtenze();
			?>
			</div>
			
			<div id="tab6">
			<?php
				$report->questionari();
				
			?>
			</div>
				
			
		</div>
	</div>

</body>
</html>
<?php 

}

?>