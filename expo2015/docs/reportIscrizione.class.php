<?php
include "configuration.inc";
require_once($PROGETTO . "/view/lib/db.class.php");



class ReportIscrizione{
	private $db;
	private $today;
	private $yesterday;
	private $tomorrow;
	private $beforeYesterday;

	const HOUR = 24;
	const IDIMPRESE = 27;
	const IDANAGRAFICA = 167;

	const BEFOREYESTERDAY = "beforeYesterday";
	const YESTERDAY = "yesterday";
	const TODAY = "today";
	const TOTAL = "total";
	
	/*const INTFIRMARE 	= 2;
	const INTPAGARE 	= 3;
	const INTISCRITTE 	= 4;
	const INTONLINE 	= 5;
*/
	const FIRMARE 	= "2";
	const PAGARE 	= "3";
	const ISCRITTE 	= "4";
	const ONLINE 	= "5";

	CONST ATTIVI = "attivi";
	CONST INATTIVI = "inattivi";
	
	CONST FASCIECOLORI = 5;
	private $fascieRegioniStats  = array(0,0.05,0.15,0.95,1);
	private $fascieProvinceStats = array(0,0.05,0.15,0.95,1);
	
	private $aree = array(
			1=> Array("Nome" =>"Nord", "Area"=> array(1 => 1, 2 =>2)), 
			3=> Array("Nome" =>"Centro", "Area"=> array(1 => 3)),
			4=> Array("Nome" =>"Sud", "Area"=> array(1 => 4, 2 =>5))
			);
			
	private $macroAree = array(
			1=> Array("Nome" =>"Nord-Ovest","Area"=> array(1 => 1)),
			2=> Array("Nome" =>"Nord-Est",	"Area"=> array(1 => 2)),
			3=> Array("Nome" =>"Centro", 	"Area"=> array(1 => 3)),
			4=> Array("Nome" =>"Sud", 		"Area"=> array(1 => 4)),
			5=> Array("Nome" =>"Isole", 	"Area"=> array(1 => 5))
	);
		
	
	private $regioni = array(
			"01" => array ("Id" => "01", "Area" => 1, "Descrizione" => "Piemonte" ),
			"02" => array ("Id" => "02", "Area" => 1, "Descrizione" => "Valle d'Aosta" ),
			"03" => array ("Id" => "03", "Area" => 1, "Descrizione" => "Lombardia" ),
			"04" => array ("Id" => "04", "Area" => 2, "Descrizione" => "Trentino-Alto Adige" ), 
			"05" => array ("Id" => "05", "Area" => 2, "Descrizione" => "Veneto" ),
			"06" => array ("Id" => "06", "Area" => 2, "Descrizione" => "Friuli-Venezia Giulia" ),
			"07" => array ("Id" => "07", "Area" => 1, "Descrizione" => "Liguria" ),
			"08" => array ("Id" => "08", "Area" => 2, "Descrizione" => "Emilia-Romagna" ),
			"09" => array ("Id" => "09", "Area" => 3, "Descrizione" => "Toscana" ),
			"10" => array ("Id" => "10", "Area" => 3, "Descrizione" => "Umbria" ),
			"11" => array ("Id" => "11", "Area" => 3, "Descrizione" => "Marche" ),
			"12" => array ("Id" => "12", "Area" => 3, "Descrizione" => "Lazio" ),
			"13" => array ("Id" => "13", "Area" => 4, "Descrizione" => "Abruzzo" ),
			"14" => array ("Id" => "14", "Area" => 4, "Descrizione" => "Molise" ),
			"15" => array ("Id" => "15", "Area" => 4, "Descrizione" => "Campania" ),
			"16" => array ("Id" => "16", "Area" => 4, "Descrizione" => "Puglia" ),
			"17" => array ("Id" => "17", "Area" => 4, "Descrizione" => "Basilicata" ),
			"18" => array ("Id" => "18", "Area" => 4, "Descrizione" => "Calabria" ),
			"19" => array ("Id" => "19", "Area" => 5, "Descrizione" => "Sicilia" ),
			"20" => array ("Id" => "20", "Area" => 5, "Descrizione" => "Sardegna" ),
			
			);
	
	
	
	public function __construct(){
		$this->db = new DataBase();
			
		$delay = self::HOUR *3600;
		$beforeYesterdayTime = time() - (2*$delay);
		$yesterdayTime = time() - $delay;
		$tomorrowTime = time() + $delay;

		$this->beforeYesterday = date("Y-m-d" , $beforeYesterdayTime);
		$this->yesterday = date("Y-m-d" , $yesterdayTime);
		$this->today = date("Y-m-d" , time());
		$this->tomorrow = date("Y-m-d" , $tomorrowTime);
				
		
		//$this->beforeYesterday = "2014-01-13";
		//$this->yesterday = "2014-01-14";
		//$this->today = "2014-01-15";
		//$this->tomorrow = "2014-01-16";
	}

	
	/**
	 * Funzione che stampa il trend positivo o negativo
	 * @param int $old
	 * @param int $new
	 */
	public function trend ($old , $new){
		
		$dif = $new - $old;
		if ($dif> 0){
			$color = "green";
		} else{
			$color = "red";
		}
		print $new;
		print "<span style=\" color :$color\"> ($dif) </span>";
	}
	
	/**
	 * Funzione che stampa il trend positivo o negativo
	 * @param int $old
	 * @param int $new
	 */
	public function percent ($value , $total,$color="red"){
	
		//$color = "red";
		if ($total>0){
		$perCent = round($value / $total *100,2);
		}
		print "<span style=\" color :$color\"> ($perCent %) </span>";
	}
	
	private function setVisite($visiteDB, $default = 0){
		if ($visiteDB)
			return  $visiteDB;
		
		return $default;
		
	}
	
	/**
	 * Funzione che estrae dal database le imprese iscritte prima di ieri, ieri, oggi e il totale.
	 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
	 * Array (
	 * [beforeYesterday] 	=> Array ( [firmare] =>  [pagare] =>  [iscritte] =>  [onLine] =>  )
	 * [yesterday] 			=> Array ( [firmare] =>  [pagare] =>  [iscritte] =>  [onLine] =>  )
	 * [today] 				=> Array ( [firmare] =>  [pagare] =>  [iscritte] =>  [onLine] =>  )
	 * [total] 				=> Array ( [firmare] =>  [pagare] =>  [iscritte] =>  [onLine] =>  )
	 * )
	 *
	 * @param boolean $print
	 * @return Array come definito sopra
	 *
	 */
	public function impreseIscritte($print = false){

		$totalResult=array(
				self::BEFOREYESTERDAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
				self::YESTERDAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
				self::TODAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
				self::TOTAL=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0)
		);

		$id = self::IDIMPRESE;
		$firmare ='StatoRegistrazione = '. self::FIRMARE ;
		$pagare ='StatoRegistrazione = ' . self::PAGARE ;
		$onLine = 'StatoRegistrazione = ' . self::ONLINE ;
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		$nonIscritte ='(StatoRegistrazione = '. self::FIRMARE .' or StatoRegistrazione = ' . self::PAGARE . ')';
		

		/* --------------- Start estrazione dati da database  imprese non  iscritte------------------*/
		
		
			
		$sqlFirmareBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $firmare and DataRegistrazione < '$this->yesterday' ";
		$sqlFirmareYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $firmare and DataRegistrazione = '$this->yesterday' ";
		$sqlFirmareToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $firmare and DataRegistrazione = '$this->today'		";
		
		$firmareBeforeYesterdayResult = $this->db->GetRow($sqlFirmareBeforeYesterday);
		
		
		$firmareYesterdayResult = $this->db->GetRow($sqlFirmareYesterday);
		$firmareTodayResult = $this->db->GetRow($sqlFirmareToday);
		
		
		$sqlPagareBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $pagare and DataRegistrazione < '$this->yesterday' ";
		$sqlPagareYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $pagare and DataRegistrazione = '$this->yesterday' ";
		$sqlPagareToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $pagare and DataRegistrazione = '$this->today'		";
		
		$pagareBeforeYesterdayResult = $this->db->GetRow($sqlPagareBeforeYesterday);
		$pagareYesterdayResult = $this->db->GetRow($sqlPagareYesterday);
		$pagareTodayResult = $this->db->GetRow($sqlPagareToday);
		
		
		$totalResult[self::BEFOREYESTERDAY][self::FIRMARE] += $firmareBeforeYesterdayResult[0];
		$totalResult[self::BEFOREYESTERDAY][self::PAGARE] 	+= $pagareBeforeYesterdayResult[0];
		
		$totalResult[self::YESTERDAY][self::FIRMARE] 	+= $firmareYesterdayResult[0];
		$totalResult[self::YESTERDAY][self::PAGARE]		+= $pagareYesterdayResult[0];
		
		$totalResult[self::TODAY][self::FIRMARE]	+= $firmareTodayResult[0];
		$totalResult[self::TODAY][self::PAGARE] 	+= $pagareTodayResult[0];
		
		
		
		/* --------------- End estrazione dati da database  imprese non iscritte------------------*/


		/* --------------- Start estrazione dati da database  imprese iscritte------------------*/


			
		$sqlIscritteBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione < '$this->yesterday' ";
		$sqlIscritteYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione = '$this->yesterday' ";
		$sqlIscritteToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione = '$this->today'		";

		$iscritteBeforeYesterdayResult = $this->db->GetRow($sqlIscritteBeforeYesterday);
		
		
		$iscritteYesterdayResult = $this->db->GetRow($sqlIscritteYesterday);
		$iscritteTodayResult = $this->db->GetRow($sqlIscritteToday);

		
		$sqlOnLineBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $onLine and DataRegistrazione < '$this->yesterday' ";
		$sqlOnLineYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $onLine and DataRegistrazione = '$this->yesterday' ";
		$sqlOnLineToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese Where Id > $id and $onLine and DataRegistrazione = '$this->today'		";
		
		$onLineBeforeYesterdayResult = $this->db->GetRow($sqlOnLineBeforeYesterday);
		$onLineYesterdayResult = $this->db->GetRow($sqlOnLineYesterday);
		$onLineTodayResult = $this->db->GetRow($sqlOnLineToday);
		
		
		$totalResult[self::BEFOREYESTERDAY][self::ISCRITTE] += $iscritteBeforeYesterdayResult[0];
		$totalResult[self::BEFOREYESTERDAY][self::ONLINE] 	+= $onLineBeforeYesterdayResult[0];

		$totalResult[self::YESTERDAY][self::ISCRITTE] 	+= $iscritteYesterdayResult[0];
		$totalResult[self::YESTERDAY][self::ONLINE]		+= $onLineYesterdayResult[0];

		$totalResult[self::TODAY][self::ISCRITTE]	+= $iscritteTodayResult[0];
		$totalResult[self::TODAY][self::ONLINE] 	+= $onLineTodayResult[0];

		/* --------------- End estrazione dati da database  imprese iscritte------------------*/

		
		
		/*---- Calcolo risultati totali  ----------*/

		$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::BEFOREYESTERDAY][self::FIRMARE];
		$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::YESTERDAY][self::FIRMARE];
		$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::TODAY][self::FIRMARE];

		$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::BEFOREYESTERDAY][self::PAGARE];
		$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::YESTERDAY][self::PAGARE];
		$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::TODAY][self::PAGARE];

		$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::BEFOREYESTERDAY][self::ISCRITTE];
		$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::YESTERDAY][self::ISCRITTE];
		$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::TODAY][self::ISCRITTE];

		$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::BEFOREYESTERDAY][self::ONLINE];
		
		$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::YESTERDAY][self::ONLINE];
		$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::TODAY][self::ONLINE];




		if ($print){
			/*------ stampa risultati-------------------*/

			?>
			<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
			<h2>Imprese/Professionisti Iscritti</h2>

			<table class="table">
			
				<thead class="table-head">
					<tr>
						<td>Firmare</td>
						<td>Pagare</td>
						<td>Iscritte</td>
						<td>ON Line</td>
						<td>Data</td>
					</tr>
				</thead>
				<tbody>
					<tr class="beforeYesterday">
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::PAGARE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ONLINE];?></td>
						<td>Before Yesterday</td>
					</tr>
					<tr class="yesterday">
						<td><?php print $totalResult[self::YESTERDAY][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::PAGARE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::ONLINE];?></td>
						<td>Yesterday</td>
					</tr>
					<tr class="today">
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::FIRMARE], $totalResult[self::TODAY][self::FIRMARE]);?></td>
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::PAGARE], $totalResult[self::TODAY][self::PAGARE]);?></td>
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::ISCRITTE], $totalResult[self::TODAY][self::ISCRITTE]);?></td>
						<td><?php print $totalResult[self::TODAY][self::ONLINE];?></td>
						<td>Today</td>
					</tr>
					<tr class="total">
						<td><?php print $totalResult[self::TOTAL][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::PAGARE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::ONLINE]; ?></td>
						<td>Totale</td>
					</tr>
				</tbody>
			</table>
<?php 
		}
		return $totalResult;
	}

	
	/**
	 * Funzione che estrae le imprese iscritte tra il $dataInizio e il $dataFine
	 * @param (YYYY-DD-GG) $dataInizio
	 * @param (YYYY-DD-GG) $dataFine
	 * @return $numeroImpreseIscritte
	 */
	public function estrazioneImpreseIscritteBetweenDate($dataInizio,$dataFine){
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		$id = self::IDIMPRESE;
		
		$sqlImpreseIscritte = "Select count(StatoRegistrazione) as Count From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione BETWEEN  '$dataInizio' AND '$dataFine'		";
		//print $sqlLegaleBetweenDate;
		$numeroImpreseIscritte =$this->db->GetRow($sqlImpreseIscritte,'Count');
	
		
		return $numeroImpreseIscritte;
	
	}
	
	/**
	* Funzione che estrae le imprese iscritte nel mese di $mese e $anno
	* @param MM $mese
	* @param YYYY $anno
	* @return $numeroImpreseIscritte
	*/
	public function numeroImpreseIscritteMeseDayByDay($mese,$anno){
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		$id = self::IDIMPRESE;
		
		$sqlIscritteMensili = "Select count(StatoRegistrazione) as Count ,DataRegistrazione as Data From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione Like '$anno-$mese%' Group By DataRegistrazione Order by DataRegistrazione";
		//$sqlImpreseIscritte = "Select count(StatoRegistrazione) as Count From EXPO_T_Imprese Where Id > $id and $iscritte and DataRegistrazione BETWEEN  '$dataInizio' AND '$dataFine'		";
		//print $sqlLegaleBetweenDate;
		$numeroImpreseIscritte =$this->db->GetRows($sqlIscritteMensili);
	
		return $numeroImpreseIscritte;
	}
	
	
	
	
	/**
	 * Funzione che estrae dal database i legali rappresentanti iscritti prima di ieri, ieri, oggi e il totale.
	 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
	 * Array (
	 * [beforeYesterday] 	=> Array ( [attivi] =>  [inattivi] => )
	 * [yesterday] 			=> Array ( [attivi] =>  [inattivi] => )
	 * [today] 				=> Array ( [attivi] =>  [inattivi] => )
	 * [total] 				=> Array ( [attivi] =>  [inattivi] => )
	 * )
	 *
	 * @param boolean $print
	 * @return Array come definito sopra
	 *
	 */
	public function legaliRappresentantiIscritti($print = false){

		$totalResult=array(
		self::BEFOREYESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
		self::YESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
		self::TODAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
		self::TOTAL=>array(self::ATTIVI=>0,self::INATTIVI=>0)
		);

		$id = self::IDANAGRAFICA;
		
		/*
		$sqlBeforeYesterday = "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione  < '$this->yesterday' and Codice_Fiscale != '' Group by Stato_Record";
		$sqlYesterday 		= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today' 		and Codice_Fiscale != '' Group by Stato_Record";
		$sqlToday 			= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow'		and Codice_Fiscale != '' Group by Stato_Record";
		*/
		
		$sqlBeforeYesterday = "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 96 AND A. Id > $id and Data_Registrazione  < '$this->yesterday' Group by Stato_Record";
		$sqlYesterday 		= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 96 AND A. Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today'  Group by Stato_Record";
		$sqlToday 			= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 96 AND A. Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow' Group by Stato_Record";
		
		//print $sqlBeforeYesterday;
		$beforeYesterdayResult = $this->db->GetRows($sqlBeforeYesterday);
		$yesterdayResult = $this->db->GetRows($sqlYesterday);
		$todayResult = $this->db->GetRows($sqlToday);
		
		$totalResult[self::BEFOREYESTERDAY][self::ATTIVI]	+= $beforeYesterdayResult[0][0];
		$totalResult[self::BEFOREYESTERDAY][self::INATTIVI] += $beforeYesterdayResult[1][0];
		
		$totalResult[self::YESTERDAY][self::ATTIVI] 	+= $yesterdayResult[0][0];
		$totalResult[self::YESTERDAY][self::INATTIVI]	+= $yesterdayResult[1][0];
		
		$totalResult[self::TODAY][self::ATTIVI]		+= $todayResult[0][0];
		$totalResult[self::TODAY][self::INATTIVI] 	+= $todayResult[1][0];
		

		/*---- Calcolo risultati totali  ----------*/

		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];
		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::YESTERDAY][self::ATTIVI];
		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::TODAY][self::ATTIVI];

		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];
		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::YESTERDAY][self::INATTIVI];
		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::TODAY][self::INATTIVI];
		
		if ($print){
		?>
		<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
				<h2>Legali Rappresentati</h2>
				<table class="table">
				
					<thead class="table-head">
						<tr>
							<td>Attivi</td>
							<td>Inattivi</td>
							<td>Data</td>
						</tr>
					</thead>
					<tbody>
					<tr class="beforeYesterday">
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];?></td>
						<td>Before Yesterday</td>
					</tr>
					<tr class="yesterday">
						<td><?php print $totalResult[self::YESTERDAY][self::ATTIVI];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::INATTIVI];?></td>
						<td>Yesterday</td>
					</tr>
					<tr class="today">
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::ATTIVI],$totalResult[self::TODAY][self::ATTIVI]);?></td>
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::INATTIVI],$totalResult[self::TODAY][self::INATTIVI]);?></td>
						<td>Today</td>
					</tr>
					<tr class="total">
						<td><?php print $totalResult[self::TOTAL][self::ATTIVI];?></td>
						<td><?php print $totalResult[self::TOTAL][self::INATTIVI];?></td>
						<td>Totale</td>
					</tr>
				</tbody>
			</table>
			<?php 
		}
			
		return $totalResult;
	}
	
	/**
	 * Funzione che estrae i legali rappresentanti attivi iscritti tra il $dataInizio e il $dataFine 
	 * @param (YYYY-DD-GG) $dataInizio
	 * @param (YYYY-DD-GG) $dataFine
	 * @return numero legali rappresentanti
	 */
	public function estrazioneLegaliRappresentantiBetweenDate($dataInizio,$dataFine){
	
		$statoLegaleRappresentatante ="A";
		$idAnagrafica = self::IDANAGRAFICA;
		
		$sqlLegaleBetweenDate="Select count(Stato_Record) as Count ,Stato_Record From t_anagrafica join t_utente on t_anagrafica.Id = t_utente.Id
							Where t_anagrafica.Id > $idAnagrafica and t_anagrafica.Stato_Record ='$statoLegaleRappresentatante' and t_anagrafica.Data_Registrazione  BETWEEN  '$dataInizio' AND '$dataFine' and t_utente.Id_TipoUtente = '96'";
	//print $sqlLegaleBetweenDate;
		$legaleBetweenDate =$this->db->GetRow($sqlLegaleBetweenDate,'Count');
		
		$numLegaleRappresentanti = $legaleBetweenDate;
		
		
		return $legaleBetweenDate;
		
	}
	
	
	/**
	 * Funzione che estrae i legali rappresentanti attivi iscritti tra il $dataInizio e il $dataFine
	 * @param MM $mese
	 * @param YYYY $anno
	 * @return numero legali rappresentanti mensili- giornaliero
	 */
	public function numeroLegaliRappresentantiMeseDayByDay($mese,$anno){
	
		$statoLegaleRappresentatante ="A";
		$idAnagrafica = self::IDANAGRAFICA;
	
		$sqlLegaleMese = "Select count(Stato_Record) as Count,  YEAR(Data_Registrazione)as Year , MONTH(Data_Registrazione)as Month , DAY(Data_Registrazione)as Day  From t_anagrafica join t_utente on t_anagrafica.Id = t_utente.Id
		Where t_anagrafica.Id > $idAnagrafica and t_anagrafica.Stato_Record ='$statoLegaleRappresentatante' and  t_utente.Id_TipoUtente = '96' and t_anagrafica.Data_Registrazione Like '$anno-$mese%' Group By DAY(t_anagrafica.Data_Registrazione) Order by t_anagrafica.Data_Registrazione ";
		//print $sqlLegaleBetweenDate;
		$legaleMese =$this->db->GetRows($sqlLegaleMese);
	
		
		return $legaleMese;
	
	}
	
	
	
	/**
	 * Funzione che estrae dal database i delegati iscritti prima di ieri, ieri, oggi e il totale.
	 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
	 * Array (
	 * [beforeYesterday] 	=> Array ( [attivi] =>  [inattivi] => )
	 * [yesterday] 			=> Array ( [attivi] =>  [inattivi] => )
	 * [today] 				=> Array ( [attivi] =>  [inattivi] => )
	 * [total] 				=> Array ( [attivi] =>  [inattivi] => )
	 * )
	 *
	 * @param boolean $print
	 * @return Array come definito sopra
	 *
	 */
	public function delegatiIscritti($print = false){
	
		$totalResult=array(
				self::BEFOREYESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
				self::YESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
				self::TODAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
				self::TOTAL=>array(self::ATTIVI=>0,self::INATTIVI=>0)
		);
	
		$id = self::IDANAGRAFICA;
	
		/*
			$sqlBeforeYesterday = "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione  < '$this->yesterday' and Codice_Fiscale != '' Group by Stato_Record";
		$sqlYesterday 		= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today' 		and Codice_Fiscale != '' Group by Stato_Record";
		$sqlToday 			= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow'		and Codice_Fiscale != '' Group by Stato_Record";
		*/
	
		$sqlBeforeYesterday = "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 97 AND A. Id > $id and Data_Registrazione  < '$this->yesterday' Group by Stato_Record";
		$sqlYesterday 		= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 97 AND A. Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today'  Group by Stato_Record";
		$sqlToday 			= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 97 AND A. Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow' Group by Stato_Record";
	
		//print $sqlBeforeYesterday;
		$beforeYesterdayResult = $this->db->GetRows($sqlBeforeYesterday);
		$yesterdayResult = $this->db->GetRows($sqlYesterday);
		$todayResult = $this->db->GetRows($sqlToday);
	
		$totalResult[self::BEFOREYESTERDAY][self::ATTIVI]	+= $beforeYesterdayResult[0][0];
		$totalResult[self::BEFOREYESTERDAY][self::INATTIVI] += $beforeYesterdayResult[1][0];
	
		$totalResult[self::YESTERDAY][self::ATTIVI] 	+= $yesterdayResult[0][0];
		$totalResult[self::YESTERDAY][self::INATTIVI]	+= $yesterdayResult[1][0];
	
		$totalResult[self::TODAY][self::ATTIVI]		+= $todayResult[0][0];
		$totalResult[self::TODAY][self::INATTIVI] 	+= $todayResult[1][0];
	
	
		/*---- Calcolo risultati totali  ----------*/
	
		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];
		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::YESTERDAY][self::ATTIVI];
		$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::TODAY][self::ATTIVI];
	
		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];
		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::YESTERDAY][self::INATTIVI];
		$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::TODAY][self::INATTIVI];
	
		if ($print){
			?>
			<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
					<h2>Delegati</h2>
					<table class="table">
					
						<thead class="table-head">
							<tr>
								<td>Attivi</td>
								<td>Inattivi</td>
								<td>Data</td>
							</tr>
						</thead>
						<tbody>
						<tr class="beforeYesterday">
							<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];?></td>
							<td><?php print $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];?></td>
							<td>Before Yesterday</td>
						</tr>
						<tr class="yesterday">
							<td><?php print $totalResult[self::YESTERDAY][self::ATTIVI];?></td>
							<td><?php print $totalResult[self::YESTERDAY][self::INATTIVI];?></td>
							<td>Yesterday</td>
						</tr>
						<tr class="today">
							<td><?php $this->trend($totalResult[self::YESTERDAY][self::ATTIVI],$totalResult[self::TODAY][self::ATTIVI]);?></td>
							<td><?php $this->trend($totalResult[self::YESTERDAY][self::INATTIVI],$totalResult[self::TODAY][self::INATTIVI]);?></td>
							<td>Today</td>
						</tr>
						<tr class="total">
							<td><?php print $totalResult[self::TOTAL][self::ATTIVI];?></td>
							<td><?php print $totalResult[self::TOTAL][self::INATTIVI];?></td>
							<td>Totale</td>
						</tr>
					</tbody>
				</table>
				<?php 
			}
				
			return $totalResult;
		}
	
	
		/**
		 * Funzione che estrae dal database i delegati in sola lettura iscritti prima di ieri, ieri, oggi e il totale.
		 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
		 * Array (
		 * [beforeYesterday] 	=> Array ( [attivi] =>  [inattivi] => )
		 * [yesterday] 			=> Array ( [attivi] =>  [inattivi] => )
		 * [today] 				=> Array ( [attivi] =>  [inattivi] => )
		 * [total] 				=> Array ( [attivi] =>  [inattivi] => )
		 * )
		 *
		 * @param boolean $print
		 * @return Array come definito sopra
		 *
		 */
		public function delegatiIscrittiRead($print = false){
		
			$totalResult=array(
					self::BEFOREYESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
					self::YESTERDAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
					self::TODAY=>array(self::ATTIVI=>0,self::INATTIVI=>0),
					self::TOTAL=>array(self::ATTIVI=>0,self::INATTIVI=>0)
			);
		
			$id = self::IDANAGRAFICA;
		
			/*
			 $sqlBeforeYesterday = "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione  < '$this->yesterday' and Codice_Fiscale != '' Group by Stato_Record";
			$sqlYesterday 		= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today' 		and Codice_Fiscale != '' Group by Stato_Record";
			$sqlToday 			= "Select count(Stato_Record) From t_anagrafica Where Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow'		and Codice_Fiscale != '' Group by Stato_Record";
			*/
		
			$sqlBeforeYesterday = "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 94 AND A. Id > $id and Data_Registrazione  < '$this->yesterday' Group by Stato_Record";
			$sqlYesterday 		= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 94 AND A. Id > $id and Data_Registrazione >= '$this->yesterday' and Data_Registrazione < '$this->today'  Group by Stato_Record";
			$sqlToday 			= "Select count(Stato_Record) from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id where U.Id_TipoUtente = 94 AND A. Id > $id and Data_Registrazione >= '$this->today' 	and Data_Registrazione < '$this->tomorrow' Group by Stato_Record";
		
			//print $sqlBeforeYesterday;
			$beforeYesterdayResult = $this->db->GetRows($sqlBeforeYesterday);
			$yesterdayResult = $this->db->GetRows($sqlYesterday);
			$todayResult = $this->db->GetRows($sqlToday);
		
			$totalResult[self::BEFOREYESTERDAY][self::ATTIVI]	+= $beforeYesterdayResult[0][0];
			$totalResult[self::BEFOREYESTERDAY][self::INATTIVI] += $beforeYesterdayResult[1][0];
		
			$totalResult[self::YESTERDAY][self::ATTIVI] 	+= $yesterdayResult[0][0];
			$totalResult[self::YESTERDAY][self::INATTIVI]	+= $yesterdayResult[1][0];
		
			$totalResult[self::TODAY][self::ATTIVI]		+= $todayResult[0][0];
			$totalResult[self::TODAY][self::INATTIVI] 	+= $todayResult[1][0];
		
		
			/*---- Calcolo risultati totali  ----------*/
		
			$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];
			$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::YESTERDAY][self::ATTIVI];
			$totalResult[self::TOTAL][self::ATTIVI] += $totalResult[self::TODAY][self::ATTIVI];
		
			$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];
			$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::YESTERDAY][self::INATTIVI];
			$totalResult[self::TOTAL][self::INATTIVI] += $totalResult[self::TODAY][self::INATTIVI];
		
			if ($print){
				?>
					<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
							<h2>Delegati in sola lettura</h2>
							<table class="table">
							
								<thead class="table-head">
									<tr>
										<td>Attivi</td>
										<td>Inattivi</td>
										<td>Data</td>
									</tr>
								</thead>
								<tbody>
								<tr class="beforeYesterday">
									<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ATTIVI];?></td>
									<td><?php print $totalResult[self::BEFOREYESTERDAY][self::INATTIVI];?></td>
									<td>Before Yesterday</td>
								</tr>
								<tr class="yesterday">
									<td><?php print $totalResult[self::YESTERDAY][self::ATTIVI];?></td>
									<td><?php print $totalResult[self::YESTERDAY][self::INATTIVI];?></td>
									<td>Yesterday</td>
								</tr>
								<tr class="today">
									<td><?php $this->trend($totalResult[self::YESTERDAY][self::ATTIVI],$totalResult[self::TODAY][self::ATTIVI]);?></td>
									<td><?php $this->trend($totalResult[self::YESTERDAY][self::INATTIVI],$totalResult[self::TODAY][self::INATTIVI]);?></td>
									<td>Today</td>
								</tr>
								<tr class="total">
									<td><?php print $totalResult[self::TOTAL][self::ATTIVI];?></td>
									<td><?php print $totalResult[self::TOTAL][self::INATTIVI];?></td>
									<td>Totale</td>
								</tr>
							</tbody>
						</table>
						<?php 
					}
						
					return $totalResult;
				}	
		
	
	
	/**
	 * Funzione che estrae dal database le imprese iscritte prima di ieri, ieri, oggi e il totale per macroRegioni.
	 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
	 *
	 * @param boolean $print
	 * @return Array 
	 *
	 */
	public function impreseIscrittePerMacroRegioni($print = false){
		$totalResult=array(
			1 => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			2 => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			3 => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			4 => array(self::ISCRITTE => 0, self::ONLINE => 0 ),	
			5 => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
		 );


		$id = self::IDIMPRESE;
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		
		/* --------------- Start estrazione dati da database imprese non iscritte------------------*/

		$sqlIscritteRegione 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where I.Id > 27 and (StatoRegistrazione = 4 or StatoRegistrazione = 5) Group By Codice_Regione,StatoRegistrazione";

		$iscritteRegione = $this->db->GetRows($sqlIscritteRegione);

		
		foreach ($iscritteRegione as $regione){
		
			$idResult = $this->regioni[$regione["IdRegione"]]["Area"];
			
			$totalResult [$idResult][$regione["StatoRegistrazione"]] += $regione["CountStato"];
			if ($regione["StatoRegistrazione"] == self::ONLINE){
				$totalResult [$idResult][self::ISCRITTE] += $regione["CountStato"];
			}
		
		}
		
		/* --------------- End estrazione dati da database  imprese non iscritte------------------*/

		
		foreach ($this->macroAree as $subArea){
			$totalResult[self::TOTAL][self::ISCRITTE] 	+= $totalResult[$subArea["Area"][1]][self::ISCRITTE];
			$totalResult[self::TOTAL][self::ONLINE] 	+= $totalResult[$subArea["Area"][1]][self::ONLINE];
			}
				
				
			
			if ($print){
				/*------ stampa risultati-------------------*/
			
				?>
			<!--  <pre><?php //print_r ($totalResult);?></pre><br> -->
			<h2>Imprese Iscritte Per Macro Regioni</h2>
			
			<table class="table">
			
				<thead class="table-head">
					<tr>
						<td>Iscritte</td>
						<td>ON Line</td>
						<td>Macro Regione</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($this->aree as $area){
					//print"<br><pre>";print_r($area);print"</pre><br>";
						$iscritte = 0;
						$onLine = 0;
						?>
						<tr class="<?php print $area["Nome"];?>">
						
							<?php foreach ($area["Area"] as $subSubArea){
										
										$iscritte += $totalResult[$subSubArea][self::ISCRITTE];
										$onLine += $totalResult[$subSubArea][self::ONLINE];
									}
							
								?>
							<td><?php print $iscritte; $this->percent($iscritte, $totalResult[self::TOTAL][self::ISCRITTE])?></td>
							<td><?php print $onLine; $this->percent($onLine, $totalResult[self::TOTAL][self::ONLINE])?></td>
							<td><?php print$area["Nome"];?></td>
						</tr>
						
					<?php } 
					
					?>
					<tr class="total">
						<td><?php print $totalResult[self::TOTAL][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::ONLINE];?></td>
						<td>Totale</td>
					</tr>
				</tbody>
			</table>
			<?php 
		}
		return $totalResult;
		
	}

	
	/**
	 * Funzione che estrae dal database le imprese iscritte Regioni.
	 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
	 *
	 * @param boolean $print
	 * @return Array 
	 *
	 */
	public function impreseIscrittePerRegioni($print = false){
		
		$regionResult = array();
		$totalResult=array(
				self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
		);
	
	
		$id = self::IDIMPRESE;
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
	
		/* --------------- Start estrazione dati da database imprese non iscritte------------------*/
	
		$sqlIscritteRegione 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where I.Id > $id and $iscritte Group By Codice_Regione,StatoRegistrazione";
	
		$iscritteRegione = $this->db->GetRows($sqlIscritteRegione);
	
		
		foreach ($iscritteRegione as $regione){
		
			$idResult = $this->regioni[$regione["IdRegione"]]["Id"];
		
			$regionResult [$idResult][self::ISCRITTE] 	= 0;
			$regionResult [$idResult][self::ONLINE] 	= 0;
				
		}
		
		foreach ($iscritteRegione as $regione){
		
			$idResult = $this->regioni[$regione["IdRegione"]]["Id"];
		
			$regionResult [$idResult][$regione["StatoRegistrazione"]] += $regione["CountStato"];
			if ($regione["StatoRegistrazione"] == self::ONLINE){
				$regionResult [$idResult][self::ISCRITTE] += $regione["CountStato"];
			}
		
		}
	
		
	
		/* --------------- End estrazione dati da database  imprese non iscritte------------------*/
	
	
		foreach ($this->regioni as $regione){
			$totalResult[self::TOTAL][self::ISCRITTE] 	+= $regionResult[$regione["Id"]][self::ISCRITTE];
			$totalResult[self::TOTAL][self::ONLINE] 	+= $regionResult[$regione["Id"]][self::ONLINE];
		}
	
	
			
		if ($print){
			/*------ stampa risultati-------------------*/
			
			?>
				<!-- <pre><?php //print_r ($regionResult);?></pre><br> -->
				<h2>Imprese Iscritte Per Regioni</h2>
				
				<table class="table">
				
					<thead class="table-head">
						<tr>
							<td>Iscritte</td>
							<td>ON Line</td>
							<td>Regione (<?php print count($regionResult) ?>)</td>
						</tr>
					</thead>
					<tbody>
						
						
							<?php foreach ($regionResult as $value => $regione){	?>
							<tr class="regione">
							
								
								
								<td><?php print $regione[self::ISCRITTE]; $this->percent($regione[self::ISCRITTE], $totalResult[self::TOTAL][self::ISCRITTE])?></td>
								<td><?php print $regione[self::ONLINE]; $this->percent($regione[self::ONLINE], $totalResult[self::TOTAL][self::ONLINE])?></td>
								<td><?php print $this->regioni[$value]["Descrizione"];?></td>
								
							</tr>
							
						<?php } 
						
						?>
						<tr class="total">
							<td><?php print $totalResult[self::TOTAL][self::ISCRITTE];?></td>
							<td><?php print $totalResult[self::TOTAL][self::ONLINE];?></td>
							<td>Totale</td>
						</tr>
					</tbody>
				</table>
				
				<?php 
			}
			return array("Regioni"=>$regionResult,"Total" =>$totalResult);
			
		}

		/**
		 * Funzione che estrae dal database le imprese iscritte per province
		 * Se si vuole stampare i dati estratti in tabella passare come parametro true altrimenti non passare nessun parametro.
		 * 
		 *
		 * @param boolean $print
		 * @return array
		 *
		 */
		public function impreseIscrittePerProvinceRegioni($print = false){
		
			$provinceResult = array();
			$provinceRegionResult = array();
			$totalRegionResult=array(
					self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			);
			$totalResult=array(
					self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
			);			
		
		
			$id = self::IDIMPRESE;
			$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		
			/* --------------- Start estrazione dati da database imprese non iscritte------------------*/
		
			$sqlIscritteProvince 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione, P.Sigla,P.Descrizione,P.Id as IdProvincia From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where I.Id > 27 and (StatoRegistrazione = 4 or StatoRegistrazione = 5) Group By P.Sigla,P.Codice_Regione,I.StatoRegistrazione Order By P.Codice_Regione";
		
			$iscritteProvince = $this->db->GetRows($sqlIscritteProvince);
		
		
		foreach ($iscritteProvince as $provincia){
		
				$idResult = $provincia["Descrizione"];
			
				$provinceRegionResult [$idResult][self::ISCRITTE] 	= 0;
				$provinceRegionResult [$idResult][self::ONLINE] 	= 0;
			
			}
			
			foreach ($iscritteProvince as $provincia){
		
				$idResult = $provincia["Descrizione"];
		
				$provinceRegionResult [$idResult][$provincia["StatoRegistrazione"]] += $provincia["CountStato"];
				if ($provincia["StatoRegistrazione"] == self::ONLINE){
					$provinceRegionResult [$idResult][self::ISCRITTE] += $provincia["CountStato"];
				}
				$provinceResult[$provincia['IdRegione']][$idResult] = $provinceRegionResult [$idResult]; 
			}
		
			/* --------------- End estrazione dati da database  imprese non iscritte------------------*/
		
		
			foreach ($provinceResult as $idRegion => $provinceRegion){
					$totalRegionResult[self::TOTAL][self::ISCRITTE] 	=0;
					$totalRegionResult[self::TOTAL][self::ONLINE] 	=0;
				foreach ($provinceRegion as $nomePr =>$provincia){
					$totalRegionResult[self::TOTAL][self::ISCRITTE] 	+= $provinceRegionResult[$nomePr][self::ISCRITTE];
					$totalRegionResult[self::TOTAL][self::ONLINE] 	+= $provinceRegionResult[$nomePr][self::ONLINE];
				}
				$totalResult[$idRegion] = $totalRegionResult[self::TOTAL];
			}
		
		
				
			if ($print){
				/*------ stampa risultati-------------------*/
				
				?>
						<!-- <pre><?php //print_r ($iscritteProvince);?></pre><br> -->
						<!-- <pre><?php //print_r ($provinceRegionResult);?></pre><br> -->
						<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
						<div style=" width: 100%; display: inline-block; ">
						<h2>Imprese Iscritte Per Province</h2>
						<?php
						foreach ($provinceResult As $keyRegion => $region){
						?>
						
						<h2><?php print $this->regioni[$keyRegion]['Descrizione']?></h2>
						
						<table class="table">
						
							<thead class="table-head">
								<tr>
									<td>Iscritte</td>
									<td>ON Line</td>
									<td>Province (<?php print count($region) ?>)</td>
								</tr>
							</thead>
							<tbody>
								
								
									<?php foreach ($region as $value => $provincia){	?>
									<tr class="regione">
									
										
										
										<td><?php print $provincia[self::ISCRITTE]; $this->percent($provincia[self::ISCRITTE], $totalResult[$keyRegion][self::ISCRITTE])?></td>
										<td><?php print $provincia[self::ONLINE]; $this->percent($provincia[self::ONLINE], $totalResult[$keyRegion][self::ONLINE])?></td>
										<td><?php print utf8_decode($value);?></td>
										
									</tr>
									
								<?php } 
								
								?>
								<tr class="total">
									<td><?php print $totalResult[$keyRegion][self::ISCRITTE];?></td>
									<td><?php print $totalResult[$keyRegion][self::ONLINE];?></td>
									<td>Totale</td>
								</tr>
							</tbody>
						</table>
					
						<?php 
					}
					}
					?>	</div><?php 
					
					
					return $totalResult;
					
				}		

				public function impreseIscrittePerProvinceMacroAree($print = false){
				
					$provinceResult = array();
					$provinceRegionResult = array();
					$totalRegionResult=array(
							self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
					);
					$totalResult=array(
							self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
					);
				
				
					$id = self::IDIMPRESE;
					$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
				
					/* --------------- Start estrazione dati da database imprese non iscritte------------------*/
				
					$sqlIscritteProvince 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione, P.Sigla,P.Descrizione,P.Id as IdProvincia From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where I.Id > 27 and (StatoRegistrazione = 4 or StatoRegistrazione = 5) 
					Group By P.Sigla,P.Codice_Regione,I.StatoRegistrazione Order By P.Codice_Regione";
				
					$iscritteProvince = $this->db->GetRows($sqlIscritteProvince);
				
				
					foreach ($iscritteProvince as $provincia){
				
						$idResult = $provincia["Descrizione"];
							
						$provinceRegionResult [$idResult][self::ISCRITTE] 	= 0;
						$provinceRegionResult [$idResult][self::ONLINE] 	= 0;
							
					}
						
					foreach ($iscritteProvince as $provincia){
				
						$idResult = $provincia["Descrizione"];
				
						$provinceRegionResult [$idResult][$provincia["StatoRegistrazione"]] += $provincia["CountStato"];
						if ($provincia["StatoRegistrazione"] == self::ONLINE){
							$provinceRegionResult [$idResult][self::ISCRITTE] += $provincia["CountStato"];
						}
						$provinceResult[$this->regioni[$provincia['IdRegione']]['Area']][$idResult] = $provinceRegionResult [$idResult];
					}
				
					/* --------------- End estrazione dati da database  imprese non iscritte------------------*/
				
				
					foreach ($provinceResult as $idRegion => $provinceRegion){
							$totalRegionResult[self::TOTAL][self::ISCRITTE] 	=0;
							$totalRegionResult[self::TOTAL][self::ONLINE] 	=0;
						foreach ($provinceRegion as $nomePr =>$provincia){
							$totalRegionResult[self::TOTAL][self::ISCRITTE] 	+= $provinceRegionResult[$nomePr][self::ISCRITTE];
							$totalRegionResult[self::TOTAL][self::ONLINE] 	+= $provinceRegionResult[$nomePr][self::ONLINE];
						}
						$totalResult[$idRegion] = $totalRegionResult[self::TOTAL];
					}
				
				
				
					if ($print){
						/*------ stampa risultati-------------------*/
				
						?>
										<!-- <pre><?php //print_r ($iscritteProvince);?></pre><br>-->
										<!-- <pre><?php //print_r ($provinceRegionResult);?></pre><br>-->
										<!-- <pre><?php //print_r ($provinceResult);?></pre><br>-->
										<div style=" width: 100%; display: inline-block; ">
										<h2>Imprese Iscritte Per Province</h2>
										<?php
										foreach ($provinceResult As $keyRegion => $region){
										?>
										
										<h2><?php print $this->macroAree[$keyRegion]['Nome']?></h2>
										
										<table class="table">
										
											<thead class="table-head">
												<tr>
													<td>Iscritte</td>
													<td>ON Line</td>
													<td>Province (<?php print count($region) ?>)</td>
												</tr>
											</thead>
											<tbody>
												
												
													<?php foreach ($region as $value => $provincia){	?>
													<tr class="regione">
													
														
														
														<td><?php print $provincia[self::ISCRITTE]; $this->percent($provincia[self::ISCRITTE], $totalResult[$keyRegion][self::ISCRITTE])?></td>
														<td><?php print $provincia[self::ONLINE]; $this->percent($provincia[self::ONLINE], $totalResult[$keyRegion][self::ONLINE])?></td>
														<td><?php print utf8_decode($value);?></td>
														
													</tr>
													
												<?php } 
												
												?>
												<tr class="total">
													<td><?php print $totalResult[$keyRegion][self::ISCRITTE];?></td>
													<td><?php print $totalResult[$keyRegion][self::ONLINE];?></td>
													<td>Totale</td>
												</tr>
											</tbody>
										</table>
									
										<?php 
									}
									}
									?>	</div><?php 
									
									
									return $totalResult;
									
								}		
				
						
				
				public function impreseIscrittePerProvince($print = false){
				
					$provinceResult = array();
					$totalResult=array(
							self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
					);
				
				
					$id = self::IDIMPRESE;
					$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
				
					/* --------------- Start estrazione dati da database imprese non iscritte------------------*/
				
					$sqlIscritteProvince 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione, P.Sigla,P.Descrizione,P.Id as IdProvincia From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where I.Id > 27 and (StatoRegistrazione = 4 or StatoRegistrazione = 5) Group By P.Sigla,P.Codice_Regione,I.StatoRegistrazione Order By P.Codice_Regione";
				
					$iscritteProvince = $this->db->GetRows($sqlIscritteProvince);
				
				
					foreach ($iscritteProvince as $provincia){
				
						$idResult = $provincia["Descrizione"];
							
						$provinceResult [$idResult][self::ISCRITTE] 	= 0;
						$provinceResult [$idResult][self::ONLINE] 	= 0;
							
					}
						
					foreach ($iscritteProvince as $provincia){
				
						$idResult = $provincia["Descrizione"];
				
						$provinceResult [$idResult][$provincia["StatoRegistrazione"]] += $provincia["CountStato"];
						if ($provincia["StatoRegistrazione"] == self::ONLINE){
							$provinceResult [$idResult][self::ISCRITTE] += $provincia["CountStato"];
						}
				
					}
				
					/* --------------- End estrazione dati da database  imprese non iscritte------------------*/
				
				
					foreach ($provinceResult as $nomePr =>$provincia){
						$totalResult[self::TOTAL][self::ISCRITTE] 	+= $provinceResult[$nomePr][self::ISCRITTE];
						$totalResult[self::TOTAL][self::ONLINE] 	+= $provinceResult[$nomePr][self::ONLINE];
					}
				
				
				
					if ($print){
						/*------ stampa risultati-------------------*/
				
						?>
										<!-- <pre><?php //print_r ($iscritteProvince);?></pre><br> -->
										<!-- <pre><?php //print_r ($provinceResult);?></pre><br> -->
										<div style=" width: 100%; display: inline-block; ">
										<h2>Imprese Iscritte Per Province</h2>
										
										<table class="table">
										
											<thead class="table-head">
												<tr>
													<td>Iscritte</td>
													<td>ON Line</td>
													<td>Province (<?php print count($provinceResult) ?>)</td>
												</tr>
											</thead>
											<tbody>
												
												
													<?php foreach ($provinceResult as $value => $provincia){	?>
													<tr class="regione">
													
														
														
														<td><?php print $provincia[self::ISCRITTE]; $this->percent($provincia[self::ISCRITTE], $totalResult[self::TOTAL][self::ISCRITTE])?></td>
														<td><?php print $provincia[self::ONLINE]; $this->percent($provincia[self::ONLINE], $totalResult[self::TOTAL][self::ONLINE])?></td>
														<td><?php print utf8_decode($value);?></td>
														
													</tr>
													<?php if ($this->regioni[IdRegione]['Area'] == 03){?>
													<tr><td></td><tr>
								<?php }?>
												<?php } 
												
												?>
												<tr class="total">
													<td><?php print $totalResult[self::TOTAL][self::ISCRITTE];?></td>
													<td><?php print $totalResult[self::TOTAL][self::ONLINE];?></td>
													<td>Totale</td>
												</tr>
											</tbody>
										</table>
										</div>
										<?php 
									}
									return $totalResult;
									
								}		
					
	
	public function mappaRegioni(array $regioniResult){
	
	$regionResult = $regioniResult["Regioni"];
	$totalRegioniResul = $regioniResult["Total"]; 

	$defaultBaseColor = "#FFFFA8";
	$defaultGradiente = "1";
	
	$fullColor= 255;
	$FASCIECOLORI = count($this->fascieRegioniStats )-1;
	
	$stepColor = $fullColor/$FASCIECOLORI;
	
	
	//print $rgb."<br>";
	
	
	$gradientColor = array(
			"0" => $defaultBaseColor,
			//"1" => "#E6E6FF",
			//"2" => "#ADADFF",
			//"3" => "#9A9AFF",
			//"4" => "#7F7FFF",
			//"$FASCIECOLORI" => "#0000FF",
			
	);
	
	for ($i = 1; $i<$FASCIECOLORI; $i++){
		$red = $green = $fullColor - (($stepColor * $i/$FASCIECOLORI)*$i);
		$blue = $fullColor;
		$rgb =  "#".dechex($red).dechex($green).dechex($blue);
		$gradientColor[$i] = $rgb;
		
	}
	$gradientColor[$FASCIECOLORI] = "#0000FF";
	
	
	
	$regioniMap = array(
		"01" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M100,58L102,66L102,69L98,71L92,70L84,74L81,74L81,74L82,76L80,83L77,83L75,85L71,85L68,86L71,90L71,93L79,101L76,102L76,104L73,108L75,115L86,122L92,120L93,123L94,123L96,122L102,123L104,116L108,108L112,110L112,109L117,108L118,107L122,109L125,105L127,104L130,106L133,107L133,106L133,104L133,102L127,94L124,90L122,91L119,91L116,81L119,79L121,81L123,78L124,78L122,74L118,64L119,62L119,59L122,56L122,53L122,53L119,52L115,46L115,40L113,41L112,43L109,46L107,46L108,49L104,56L101,57L100,58Z"),
		"02" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M81,74L84,74L92,70L98,71L102,69L102,66L100,58L100,58L93,56L89,58L84,59L78,60L74,61L74,65L78,72L80,74L81,74Z"),
		"03" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M122,53L122,56L119,59L119,62L118,64L122,74L124,78L123,78L121,81L119,79L116,81L119,91L122,91L124,90L127,94L133,102L133,104L134,104L137,97L136,96L139,89L144,88L145,90L149,90L153,89L155,92L158,91L165,96L172,94L172,95L175,96L180,94L183,95L188,95L189,95L189,94L182,89L180,90L173,82L169,73L173,65L174,63L174,63L172,63L167,64L166,57L168,47L165,38L165,38L160,37L159,35L154,41L157,43L157,49L155,50L152,44L147,46L143,47L140,44L140,39L138,39L135,42L136,46L129,55L130,61L128,63L127,62L124,56L124,54L122,53L122,53Z"),
		"04" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M165,38L168,47L166,57L167,64L172,63L174,63L174,63L175,64L175,67L178,68L183,65L183,64L190,57L195,58L196,54L200,53L201,50L198,45L200,41L199,40L203,38L207,35L212,35L214,33L214,33L209,29L209,27L206,22L207,21L201,21L198,22L191,22L190,22L184,23L180,26L178,30L175,30L170,27L166,27L163,34L165,38L165,38Z"),
		"05" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M214,33L212,35L207,35L203,38L199,40L200,41L198,45L201,50L200,53L196,54L195,58L190,57L183,64L183,65L178,68L175,67L175,64L174,63L173,65L169,73L173,82L180,90L182,89L189,94L189,95L189,95L192,96L194,97L200,93L210,95L211,98L213,100L213,100L216,97L216,93L211,89L211,85L209,86L207,83L216,72L216,75L213,77L223,73L227,70L230,70L230,69L228,65L223,63L220,64L217,62L213,55L215,53L212,49L212,47L214,45L216,41L221,38L221,34L221,34L219,34L218,34L215,33L214,33Z"),
		"06" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M221,34L221,38L216,41L214,45L212,47L212,49L215,53L213,55L217,62L220,64L223,63L228,65L230,69L230,69L232,67L230,68L233,65L238,67L237,68L244,65L247,69L250,70L247,66L242,63L243,58L243,52L243,51L236,46L244,40L244,38L236,37L234,37L223,35L222,35L221,34Z"),
		"07" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M94,124L92,126L88,131L89,134L90,134L100,132L105,129L109,122L112,121L114,117L121,112L131,116L133,117L134,116L137,118L140,119L143,123L148,126L148,124L153,127L153,127L152,123L149,122L149,119L146,117L145,116L145,115L143,113L139,113L140,109L138,107L133,106L133,106L133,107L130,106L127,104L125,105L122,109L118,107L117,108L112,109L112,110L108,108L104,116L102,123L96,122L94,123L94,124Z"),
		"08" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M188,95L183,95L180,94L175,96L172,95L172,94L165,96L158,91L155,92L153,89L149,90L145,90L144,88L139,89L136,96L137,97L134,104L133,104L133,106L133,106L138,107L140,109L139,113L143,113L145,115L145,116L145,115L151,112L159,119L162,119L167,124L169,123L176,126L184,125L183,123L189,120L191,123L195,124L196,127L197,133L199,135L206,137L206,137L207,136L207,134L208,131L214,130L214,129L215,129L216,129L216,130L216,131L218,132L218,133L223,129L222,129L214,120L212,116L210,105L210,102L213,100L213,100L211,98L210,95L200,93L194,97L192,96L189,95L189,95L188,95Z"),
		"09" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M163,171L164,170L155,172L156,173L160,173L163,174L163,171Z M145,116L146,117L149,119L149,122L152,123L153,127L154,127L157,130L159,135L160,144L166,155L166,159L165,166L170,166L172,167L171,171L177,173L181,180L182,184L180,184L183,187L185,185L189,186L190,186L191,184L194,182L193,179L196,179L199,173L198,171L200,170L201,169L202,168L203,162L202,161L203,158L205,155L208,155L209,153L206,150L208,144L209,142L209,141L212,137L210,137L207,137L206,137L206,137L199,135L197,133L196,127L195,124L191,123L189,120L183,123L184,125L176,126L169,123L167,124L162,119L159,119L151,112L145,115L145,116Z"),
		"10" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M235,172L236,169L231,167L230,168L228,165L227,161L226,156L225,154L223,148L220,148L217,146L213,144L213,142L209,142L209,142L208,144L206,150L209,153L208,155L205,155L203,158L202,161L203,162L202,168L201,169L202,171L203,173L203,175L205,176L208,175L216,185L218,186L223,183L225,180L235,176L235,173L235,173L235,172Z"),
		"11" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M253,163L250,155L245,143L236,138L228,132L223,129L223,129L218,133L218,132L216,131L216,131L216,132L214,131L214,130L214,130L208,131L207,134L207,136L206,137L207,137L210,137L212,137L209,141L209,142L209,142L213,142L213,144L217,146L220,148L223,148L225,154L226,156L227,161L228,165L230,168L231,167L236,169L235,172L235,173L237,173L237,173L237,173L238,173L239,174L241,174L244,170L254,166L253,163Z"),
		"12" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M200,170L198,171L199,173L196,179L193,179L194,182L191,184L190,186L191,187L196,191L200,198L204,199L208,203L212,209L216,212L221,218L229,221L234,226L247,225L251,225L251,226L254,224L255,220L257,217L257,217L257,217L258,215L258,212L256,210L256,209L255,208L244,206L241,205L241,202L233,198L232,198L231,195L233,193L237,194L240,192L237,189L235,186L234,182L235,178L240,178L240,176L239,175L239,174L238,173L237,173L237,173L237,173L235,173L235,173L235,176L225,180L223,183L218,186L216,185L208,175L205,176L203,175L203,173L202,171L201,169L200,170Z M216,203L217,203L217,203L216,203Z"),
		"13" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M277,195L262,181L258,176L255,170L254,166L244,170L241,174L239,174L239,175L240,176L240,178L235,178L234,182L235,186L237,189L240,192L237,194L233,193L231,195L232,198L233,198L241,202L241,205L244,206L255,208L256,209L257,209L262,206L261,205L270,206L271,205L274,201L277,195L277,195Z"),
		"14" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M287,199L282,197L280,196L278,195L277,195L274,201L271,205L270,206L261,205L262,206L257,209L256,209L256,210L258,212L258,215L257,217L257,217L257,217L260,220L261,216L266,217L274,220L283,217L284,216L283,214L283,210L284,211L288,208L287,206L287,199L287,199Z"),
		"15" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M257,217L257,217L255,220L254,224L251,226L252,227L255,230L261,240L264,241L265,239L267,240L272,244L268,249L272,246L274,247L279,244L283,248L286,254L284,260L290,262L294,266L298,268L301,265L304,265L304,266L307,257L299,247L299,244L296,242L295,237L301,231L300,230L297,229L292,229L290,227L292,225L291,223L289,222L284,216L284,216L283,217L274,220L266,217L261,216L260,220L257,217L257,217Z"),
		"16" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M338,249L341,247L344,247L347,248L346,250L354,253L363,253L368,259L369,263L370,266L378,269L380,261L382,257L379,252L373,246L368,243L364,239L362,239L356,236L352,235L347,231L340,227L327,223L325,222L313,217L312,216L308,212L309,209L315,203L314,198L311,198L304,199L288,200L287,199L287,206L288,208L284,211L283,210L283,214L284,216L284,216L289,222L291,223L292,225L290,227L292,229L297,229L300,230L300,230L301,228L308,227L313,230L312,233L319,236L324,241L326,239L329,239L332,240L333,249L337,251L338,249Z"),
		"17" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M300,230L301,231L295,237L296,242L299,244L299,247L307,257L304,266L304,266L305,267L306,268L307,270L307,270L310,267L314,267L316,270L320,269L325,262L326,262L331,261L332,260L333,259L334,256L335,255L337,251L333,249L332,240L329,239L326,239L324,241L319,236L312,233L313,230L308,227L301,228L300,230L300,230Z"),
		"18" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M308,271L309,278L314,285L316,290L318,299L320,303L322,304L322,307L321,311L320,312L312,314L314,317L314,321L311,328L309,329L308,339L311,341L320,340L322,332L326,327L330,324L333,321L331,312L333,308L339,304L342,303L346,303L348,301L347,297L347,293L345,285L338,280L330,278L328,274L329,270L331,267L331,262L331,261L326,262L325,262L320,269L316,270L314,267L310,267L307,270L308,271Z"),
		"19" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M307,329L301,330L299,331L296,331L295,332L292,334L287,333L281,335L276,338L274,338L270,339L260,339L258,340L252,340L249,337L245,337L243,334L237,334L232,340L229,338L227,335L223,339L222,339L218,348L219,351L222,353L225,357L230,356L236,359L239,359L244,363L247,366L251,367L254,369L260,372L265,371L271,374L280,383L286,385L287,384L294,386L296,386L295,381L296,377L299,374L300,371L298,370L297,364L296,364L293,358L295,354L297,346L304,335L307,329Z"),
		"20" => array ("Gradiente"=> $defaultGradiente, "ColorArea" => $defaultBaseColor, "MapArea" => "M105,301L107,306L107,301L105,301Z M104,301L104,298L102,299L104,301Z M131,226L129,226L125,230L120,236L111,240L106,239L103,235L103,238L101,243L101,249L104,248L107,254L107,256L110,259L110,273L111,277L108,278L108,284L107,287L106,292L107,295L106,297L109,302L113,309L115,308L120,309L122,306L124,304L123,302L124,296L131,298L135,301L136,301L140,295L140,294L141,289L140,288L142,280L142,272L143,267L141,265L141,261L146,251L144,249L143,241L144,240L138,237L140,235L142,235L138,234L140,230L137,230L136,228L131,226Z")
);


$max = 0;
foreach ( $this->regioni as $id => $regione ) {

	if ($regionResult[$regione["Id"]][self::ISCRITTE] > $max){
		$max = $regionResult[$regione["Id"]][self::ISCRITTE];
	}
}




foreach ( $this->regioni as $id => $regione ) {
	$perCent = 0;
	if ($max> 0) {
		$perCent = $regionResult[$regione["Id"]][self::ISCRITTE] / $max;
	}
	$color = 0;
	for ($j = 1; $j< count($this->fascieRegioniStats ); $j++){
		if ($perCent >$this->fascieRegioniStats [$j-1]  && $perCent <= $this->fascieRegioniStats [$j]){
			$color = $j;
			
		}
		
	}

/*
		if ($perCent >$this->fascieRegioniStats [0]  && $perCent <= $this->fascieRegioniStats [1]){
			$color = 1;
		}else 
		if ($perCent > $this->fascieRegioniStats [1] && $perCent <= $this->fascieRegioniStats [2] ){
			$color = 2;
		}else
		if ($perCent > $this->fascieRegioniStats [2] && $perCent <= $this->fascieRegioniStats [4] ){
			$color = 3;
		}/*else
		if ($perCent > 0.6 && $perCent <= 0.8 ){
			$color = 4;
		}else
		if ($perCent > 0.8 && $perCent <= 1 ){
			$color = 5;
		}*/

	//print "$id<br>";
	//print "$perCent<br>";
	//print "$color<br>";
	$regioniMap [$id] ["ColorArea"] =  $gradientColor[$color];
}

//print"<pre>";
//print_r($gradientColor);
//print"</pre>";

?>

<div style=" width: 100%;">


<svg  height="451px"
	 transform="translate(-50,0)"
	style="display:inline-block; stroke-linejoin: round; stroke: black; fill: white;"
	version="1.1" viewBox="50 0 " width="400px"
	xmlns="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink"> <defs>
<style type="text/css">
<!
[
CDATA
[path
{
fill-rule:
evenodd;
}
#context path {
	fill: yellow;
	stroke: #bbb;
	width: 60%;
}

]]>
}
</style>

</defs> <metadata> <views> <view h="451" padding="0.06" w="450">

<bbox h="156.02" w="122.15" x="953.92" y="960.83" />
<llbbox lat0="-45" lat1="90" lon0="-180" lon1="180" /></view></views></metadata>

<g id="context">
<path
		d="M208,385L205,382L206,386L208,385Z M306,330L307,329L299,331L297,331L293,334L289,333L282,334L280,337L270,339L263,339L253,340L248,337L244,336L244,334L238,334L235,338L230,339L226,335L221,339L218,348L221,353L226,356L232,356L235,358L239,359L245,364L251,367L257,371L260,372L267,372L273,376L278,382L285,385L289,384L295,385L297,377L300,374L296,367L294,361L294,358L297,346L306,330Z M108,303L105,303L107,306L108,303Z M141,239L140,235L138,231L132,227L131,225L127,229L124,230L120,236L113,239L110,240L106,239L104,237L102,242L102,246L109,259L109,265L107,272L111,275L110,279L109,278L107,286L107,298L113,308L119,308L123,304L124,297L130,298L136,301L138,300L140,293L142,273L141,261L146,253L143,241L141,239Z M162,169L161,170L155,171L155,172L161,172L162,169Z M165,27L164,31L164,38L163,38L157,36L156,36L155,41L156,44L157,49L155,49L151,44L145,47L141,45L140,42L137,39L136,40L135,47L129,56L129,62L128,63L123,57L125,54L120,53L115,46L116,42L114,41L110,44L105,54L99,59L94,57L83,59L81,58L80,58L78,60L74,61L74,65L76,66L79,73L82,76L80,82L74,85L69,86L71,93L77,97L78,101L73,108L76,116L86,122L92,121L93,124L88,131L88,135L94,134L102,131L104,130L109,122L116,115L122,113L126,113L132,117L135,117L146,125L154,128L157,130L159,134L161,146L164,151L166,162L165,165L171,166L173,171L176,173L182,181L181,185L182,186L186,185L191,187L195,189L199,197L206,202L210,206L221,218L227,220L234,226L241,224L246,226L255,229L260,238L261,241L267,240L271,243L268,248L279,245L281,246L286,255L284,257L285,260L295,265L303,266L306,268L308,272L311,280L316,290L319,301L322,304L322,310L318,311L313,316L314,321L312,327L309,329L308,332L308,337L310,340L320,340L325,328L332,322L333,319L332,311L333,308L337,305L342,303L346,303L348,300L347,295L346,287L342,283L337,280L331,279L329,275L329,271L332,261L335,254L338,249L341,247L345,247L347,250L353,253L364,253L371,265L375,268L378,268L381,258L380,254L376,249L368,243L365,240L352,234L346,230L341,227L326,222L311,216L308,213L308,209L315,202L315,199L309,198L294,200L288,199L280,196L271,189L261,180L257,174L254,168L251,157L248,149L237,139L227,131L221,128L216,124L214,120L211,113L210,103L215,98L217,94L213,91L210,85L209,84L210,77L216,73L214,76L222,73L229,70L233,65L239,67L243,65L247,70L246,71L250,70L242,57L240,58L240,55L243,50L238,49L237,45L244,40L244,38L239,37L231,37L214,33L212,32L206,24L207,19L197,22L184,23L182,24L179,29L174,30L171,27L166,27L165,27Z M214,131L215,129L216,132L214,131Z "
		data-iso="ITA" />
</g>

<g fill="" fill-opacity="1" id="regions" >

<?php 
foreach ($this->regioni as $id => $regione){

?>

<path fill="<?php print $regioniMap[$id]["ColorArea"];?>"
		fill-opacity="<?php print $regioniMap[$id]["Gradiente"];?>"
		d="<?php print $regioniMap[$id]["MapArea"];?>"
		data-fips="IT<?php print $id;?>" data-fips-="" data-iso3="ITA"
		data-name="<?php print $regione["Descrizione"];?>"
		 />


<?php
}
?>
</g>

</svg>


<div style="width: 45%; float: right; display: inline-block; ">
<table style="width: 35%; float: right; ">
<thead >
<tr>
<td></td>
<td>Iscritti</td>

</tr>
</thead>
<tbody>

<?php /*
$intervall = $max/self::FASCIECOLORI;
foreach ($gradientColor as $idColor => $colorRow){



<?php
*/
for ($j = 1; $j< count($this->fascieRegioniStats ); $j++){
	?>
<tr>
<td style="background-color: <?php print $gradientColor[$j];?>" >
</td>
<td style="background-color: white;">
<?php 
	print (round($this->fascieRegioniStats [$j-1]*$max,0)+1)."-";

	print round($this->fascieRegioniStats [$j]*$max,0);
 				
	


/*
$num = $intervall * $idColor;
if ($num>0){
if ($num -$intervall>0){
$prev = $num -$intervall;
}else {
$prev = 1;
}
print $prev." - ";
}  
print $intervall * $idColor;*/ 
?>
</td>
</tr>
<?php 
}
?>
</tbody>
</table>
</div>
</div>
<?php 




		
		
		
	
}



public function mappaLombardia(){
	$defaultBaseColor = "#FFFFA8";
$defaultGradiente = "1";

$fullColor= 255;
$FASCIECOLORI = count($this->fascieProvinceStats )-1;

$stepColor = $fullColor/$FASCIECOLORI;


//print $rgb."<br>";


$gradientColor = array(
		"0" => $defaultBaseColor,
		//"1" => "#E6E6FF",
		//"2" => "#ADADFF",
		//"3" => "#9A9AFF",
		//"4" => "#7F7FFF",
		//"$FASCIECOLORI" => "#0000FF",
			
);

for ($i = 1; $i<$FASCIECOLORI; $i++){
	$red = $green = $fullColor - (($stepColor * $i/$FASCIECOLORI)*$i);
	$blue = $fullColor;
	$rgb =  "#".dechex($red).dechex($green).dechex($blue);
	$gradientColor[$i] = $rgb;

}
$gradientColor[$FASCIECOLORI] = "#0000FF";




	$provinceResult = array();
	$provinceResult = array(
			"Bergamo" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Brescia" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Como" 		=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Cremona" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Lecco" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Lodi" 		=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Mantova" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Milano" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Monza_e_della_Brianza" => array ($ISCRITTE => 0, $ONLINE => 0),
			"Pavia" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Sondrio" 	=> array ($ISCRITTE => 0, $ONLINE => 0),
			"Varese" 	=> array ($ISCRITTE => 0, $ONLINE => 0)
	);
	
	$totalResult=array(
			self::TOTAL => array(self::ISCRITTE => 0, self::ONLINE => 0 ),
	);
	
	
	$id = self::IDIMPRESE;
	$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
	
	/* --------------- Start estrazione dati da database imprese non iscritte------------------*/
	
	$sqlIscritteProvince 	= "Select count(StatoRegistrazione) as CountStato ,StatoRegistrazione, P.Codice_Regione as IdRegione, P.Sigla,P.Descrizione,P.Id as IdProvincia From EXPO_T_Imprese as I join Tlk_Province as P on I.Pr = P.Sigla Where P.Codice_Regione = 03 and I.Id > 27 and (StatoRegistrazione = 4 or StatoRegistrazione = 5) Group By P.Sigla,P.Codice_Regione,I.StatoRegistrazione Order By P.Codice_Regione";
	
	$iscritteProvince = $this->db->GetRows($sqlIscritteProvince);
	
	foreach ($iscritteProvince as $provincia){
	
		$idResult = str_replace(" ", "_", $provincia["Descrizione"]);
			
		$provinceResult [$idResult][self::ISCRITTE] 	= 0;
		$provinceResult [$idResult][self::ONLINE] 	= 0;
			
	}
		
	foreach ($iscritteProvince as $provincia){
	
		$idResult = str_replace(" ", "_", $provincia["Descrizione"]);
	
		$provinceResult [$idResult][$provincia["StatoRegistrazione"]] += $provincia["CountStato"];
		if ($provincia["StatoRegistrazione"] == self::ONLINE){
			$provinceResult [$idResult][self::ISCRITTE] += $provincia["CountStato"];
		}
	
	}

	$max = 0;
	foreach ( $provinceResult as $nome => $provincia ) {
	
		if ($provinceResult[$nome][self::ISCRITTE] > $max){
			$max = $provinceResult[$nome][self::ISCRITTE];
		}
	}
	
	
foreach ( $provinceResult as $nome => $provincia ) {
	//print "<br>".$provincia[self::ISCRITTE]."<br>";
	//print_r ($provincia)."<br>";
	//print $max."<br>";
	$perCent = 0;
	if ($max> 0) {
		$perCent = $provincia[self::ISCRITTE] / $max;

	}
	$color = 0;
	for ($j =1; $j <count($this->fascieProvinceStats ); $j++){
		//print $j."<br>";
		if ($perCent > $this->fascieProvinceStats [$j-1]  && $perCent <= $this->fascieProvinceStats [$j] ){
				
			$color = $j;

			//print "ciao<br>";
		}
	}
	
	$provinceColor[$color] .= "#$nome,";

}

?>
<style>

<?php 


for ($i=0; $i < count($this->fascieProvinceStats ); $i++){
//foreach ($provinceLombardiaMap as $nome => $provincia){
//print "#$nome";
if (strlen($provinceColor[$i])){
print substr($provinceColor[$i],0, strlen($provinceColor[$i])-1);

?>
 {
	fill: <?php print $gradientColor[$i];?>;
	fill-opacity: 1;
	stroke: #000000;

}
<?php
}
}
?>

</style>
<?php 
include 'lombardia.svg';
?>
<div style="width: 45%; float: right; display: inline-block; ">
<table style="width: 35%; float: right; ">
<thead >
<tr>
<td></td>
<td>Iscritti</td>

</tr>
</thead>
<tbody>
<?php 

for ($j = 1; $j< count($this->fascieProvinceStats ); $j++){
	?>
<tr>
<td style="background-color: <?php print $gradientColor[$j];?>" >
</td>
<td style="background-color: white;">
<?php 
	print (round($this->fascieProvinceStats [$j-1]*$max,0)+1)."-";

	print round($this->fascieProvinceStats [$j]*$max,0);
 				
	


/*
$num = $intervall * $idColor;
if ($num>0){
if ($num -$intervall>0){
$prev = $num -$intervall;
}else {
$prev = 1;
}
print $prev." - ";
}  
print $intervall * $idColor;*/ 
?>
</td>
</tr>
<?php 
}
?>
</tbody>
</table>
</div>

<?php 
}


public function categorie(){
?>
<h2>Categorie Merceologiche</h2>
<?php 
$stringSqlVisiteCategorie = "SELECT  I.IdCategoria, count(I.IdCategoria) AS Visite
FROM (EXPO_Log_Visite_Vetrina AS Log join EXPO_TJ_Imprese_Categorie as I ON Log.Id_Impresa = I.IdImpresa )join EXPO_Tlk_Categorie as C on I.IdCategoria = C.Id
WHERE Log.Id_Impresa > 27 AND Log.Id_Prodotto = 0  AND I.IsIscritta = 'Y'
GROUP BY I.IdCategoria";
$sqlVisiteCategorie = $this->db->GetRows($stringSqlVisiteCategorie);


$stringSqlVisiteCategorieProdotti = "SELECT  I.IdCategoria, count(I.IdCategoria) AS Visite
FROM (EXPO_Log_Visite_Vetrina AS Log join EXPO_TJ_Prodotti_Categorie as I ON Log.Id_Prodotto = I.IdProdotto )join EXPO_Tlk_Categorie as C on I.IdCategoria = C.Id
WHERE Log.Id_Impresa > 27 AND Log.Id_Prodotto <> 0 AND I.IsIscritta = 'Y'
GROUP BY I.IdCategoria";
$sqlVisiteCategorieProdotti = $this->db->GetRows($stringSqlVisiteCategorieProdotti);



/*
print "<pre>";
print_r ($sqlVisiteCategorie);
print "</pre>";
*/
$visiteCategorie = array();

foreach ($sqlVisiteCategorie as $key => $value){
	$visiteCategorie[$value['IdCategoria']] = $value['Visite'];
}

foreach (sqlVisiteCategorieProdotti as $key => $value){
	$visiteCategorie[$value['IdCategoria']] += $value['Visite'];
}
/*
print "<pre>";
print_r ($visiteCategorie);
print "</pre>";
*/

for ($cat = 1; $cat < 5 ; $cat ++){

$sqlNomeMacroCategoria = "SELECT DISTINCT C.Descrizione As Descrizione 
FROM EXPO_Tlk_Categorie as C
WHERE C.Id_Categoria like '0$cat'";
$nomeMacroCategoria = $this->db->GetRow($sqlNomeMacroCategoria,"Descrizione"); 

?>
<table class="table">
<thead class="table-head">
<tr>
<th width="60%"><?php print $nomeMacroCategoria;?></th>
<th>Imprese Abbinate</th>
<th>Visite</th>
</tr>
</thead>
<tbody>
<?php 

$query = "SELECT DISTINCT C.Id As IdCategoria, C.Descrizione As Descrizione 
FROM EXPO_Tlk_Categorie as C 
WHERE C.Id_Categoria like '0$cat%' And  LENGTH(C.Id_Categoria)=8 ORDER BY C.Id_Categoria
		";


//$query = "SELECT DISTINCT I.IdCategoria, C.Descrizione As Descrizione FROM EXPO_TJ_Imprese_Categorie as I join EXPO_Tlk_Categorie as C on I.IdCategoria = C.Id WHERE IdImpresa > 27  AND C.Id_Categoria like  '0$cat%' ORDER BY C.Id_Categoria";

foreach ($this->db->GetRows($query) AS $rows) {

	$category = $rows;
	$queryImprese = "SELECT DISTINCT(I.Id) As Id, I.RagioneSociale as RagioneSociale FROM EXPO_T_Imprese AS I join EXPO_TJ_Imprese_Categorie AS IC on I.Id=IC.IdImpresa WHERE I.Id > 27 and IC.IdCategoria = '" . $category['IdCategoria'] . "'and IC.IsIscritta='Y' ORDER BY RagioneSociale";
	
	$numImprese = $this->db->NumRows($queryImprese);
	?><tr class="categorie">
                    <?php
                    echo "<td>".$category['Descrizione']."</td>";
                    print "<td>" . $numImprese . "</td>";
                    ?>
                <td  >
                <?php print $this->setVisite($visiteCategorie[$category['IdCategoria']]);?>
                </td>    
          </tr>          
<?php 
                    
   	}

   	?></tbody></table><?php 
}
}





public function macroCategorie(){
	?>
<h2>Categorie Merceologiche</h2>


<table class="table">
<thead class="table-head">
<tr>
<th> Categorie</th>
<th >Imprese Abbinate</th>
</tr>
</thead>
<tbody>
<?php 


for ($cat = 1; $cat < 5 ; $cat ++){

$sqlNomeMacroCategoria = "SELECT DISTINCT C.Descrizione As Descrizione 
FROM EXPO_Tlk_Categorie as C
WHERE C.Id_Categoria like '0$cat'";

$nomeMacroCategoria = $this->db->GetRow($sqlNomeMacroCategoria,"Descrizione"); 


$query = "SELECT DISTINCT (I.IdImpresa) FROM EXPO_TJ_Imprese_Categorie as I join EXPO_Tlk_Categorie as C on I.IdCategoria = C.Id WHERE IdImpresa > 27 AND IsIscritta = 'Y' AND C.Id_Categoria like  '0$cat%'  ORDER BY C.Id_Categoria";


	$numImprese = $this->db->NumRows($query);
	?><tr class="categorie">
                    <?php
                    echo "<td>".$nomeMacroCategoria."</td>";
                    print "<td width=\"10%\">" . $numImprese . "</td>";
                    ?>
                    
          </tr>          
<?php 
                    
   	
}
   	?></tbody></table><?php 

}


public function logAccessiVetrina(){





$sqlAccessiBeforeYesterday = "	SELECT count(data) AS Accessi, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and data < '$this->yesterday'";

$sqlAccessiYesterday 		= "	SELECT count(data) AS Accessi, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and DATE_FORMAT(data,'%Y-%m-%d') = '$this->yesterday'";

$sqlAccessiToday 			= "	SELECT count(data) AS Accessi, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and DATE_FORMAT(data,'%Y-%m-%d') = '$this->today'";
/*
print "<br>";
print $sqlAccessiBeforeYesterday."<br>";
print "<br>";
print "<br>";
print $sqlAccessiYesterday."<br>";
print "<br>";
print "<br>";
print $sqlAccessiToday."<br>";
*/


$accessiBeforeYesterday = $this->db->GetRow($sqlAccessiBeforeYesterday,'Accessi');
$accessiYesterday = $this->db->GetRow($sqlAccessiYesterday,'Accessi');
$accessiToday = $this->db->GetRow($sqlAccessiToday,'Accessi');

$sqlUtentiBeforeYesterday = "	SELECT count(distinct(parametri)) AS Utenti, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and data < '$this->yesterday'";

$sqlUtentiYesterday 		= "	SELECT count(distinct(parametri)) AS Utenti, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and DATE_FORMAT(data,'%Y-%m-%d') = '$this->yesterday'";
	
$sqlUtentiToday 			= "	SELECT count(distinct(parametri)) AS Utenti, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468' and DATE_FORMAT(data,'%Y-%m-%d') = '$this->today'";

$sqlUtentiTotal 			= "	SELECT count(distinct(parametri)) AS Utenti, DATE_FORMAT(data,'%Y-%m-%d') as Data
								FROM EXPO_T_Vetrina_Logs
								WHERE azione = 'Login PDMS2' AND response = 'ok' AND parametri <> 'idPartecipante=1468'";


$utentiBeforeYesterday = $this->db->GetRow($sqlUtentiBeforeYesterday,'Utenti');
$utentiYesterday = $this->db->GetRow($sqlUtentiYesterday,'Utenti');
$utentiToday = $this->db->GetRow($sqlUtentiToday,'Utenti');
$utentiTotal = $this->db->GetRow($sqlUtentiTotal,'Utenti');


?>

<h2>Accessi Vetrina</h2>

			<table class="table">
			
				<thead class="table-head">
					<tr>
						<td>Accessi</td>
						<td>Utenti</td>
						<td>Data</td>
					</tr>
				</thead>
				<tbody>
					<tr class="beforeYesterday">
						<td><?php print $this->setVisite($accessiBeforeYesterday);?></td>
						<td><?php print $this->setVisite($utentiBeforeYesterday);?></td>
						<td>Before Yesterday</td>
					</tr>
					<tr class="yesterday">
						<td><?php print $this->setVisite($accessiYesterday);?></td>
						<td><?php print $this->setVisite($utentiYesterday);?></td>
						<td>Yesterday</td>
					</tr>
					<tr class="today">
						<td><?php print $this->setVisite($accessiToday);?></td>
						<td><?php print $this->setVisite($utentiToday);?></td>
						<td>Today</td>
					</tr>
					<tr class="total">
						<td><?php print ($accessiBeforeYesterday + $accessiYesterday + $accessiToday);?></td>
						<td><?php print ($utentiTotal);?></td>
						<td>Totale</td>
					</tr>
				</tbody>
			</table>


<?php 


}


public function accessiUtenze(){
//$sql = "SELECT count(*) as Accessi, parametri,DATE_FORMAT(data,'%Y-%m-%d') as Data FROM EXPO_T_Vetrina_Logs WHERE azione = 'Login PDMS2' group by parametri";
$sql = "SELECT parametri,DATE_FORMAT(data,'%Y-%m-%d %H:%i') as Data FROM EXPO_T_Vetrina_Logs WHERE azione = 'Login PDMS2'  ORDER BY data Desc";
$accessiUtenze = $this->db->GetRows($sql);

/*
print "<pre>";
print_r($accessiUtenze);
print "</pre>";
*/

$accessi = array();

foreach ($accessiUtenze as $key => $value){
$accesso = array();

$id = explode ("=",$value['parametri']);
$id= $id[1];
/*
print "<pre>";
print_r($value);
print "</pre>";
*/

$accessi [$id]['Accessi'] ++;
if (! $accessi [$id]['UltimoAccesso']){
	$accessi [$id]['UltimoAccesso'] = $value['Data'];
}

}
/*
print "<pre>";
print_r($accessi);
print "</pre>";
*/



?>

<h2>Accessi Vetrina per utenze</h2>

			<table class="tableAccessi">
			
				<thead class="table-head">
					<tr>
						<td>Id</td>
						<td>Partecipante</td>
						<td>Paese</td>
						<td>E_Mail</td>
						<!-- <td>Login</td>-->
						<td>Ultimo Accesso</td>
						<td>Accessi</td>
					</tr>
				</thead>
				<tbody>
<?php 
$totale = 0;
$sonda = null;
$utentiTest = array();
foreach ($accessi AS $uid => $accessiUtente){

	$sqlUtente = "	select A.Id as Id, Nome, Cognome,E_Mail, RagSoc, Sigla_Nazione,Data_Registrazione,Login
					from T_Anagrafica AS A join T_Utente AS U  on A.Id = U.Id
					where U.Id_TipoUtente = 98 and A.Id = $uid";
	

	$utente = $this->db->GetRow($sqlUtente,null,null,'report iscrizione class ln 1016');
	
	//if (is_array($utente)){

	if ($utente['Id'] == 1468){
		$sonda = $utente;
	}else if ( (strpos($utente['E_Mail'], "@expo2015.org")) || (strpos($utente['E_Mail'], "@digicamere.it"))){
	$utentiTest[$uid] = $utente;
	} else{
	/*
	print "$uid<pre>";
	print_r($utente);
	print "</pre>";
	*/
	$totale += $accessiUtente['Accessi'];
?>
					<tr class="accessi">
						<td><span class="id"><?php echo $uid;?></span></td>
						<td><?php echo $utente['Nome'].' '.$utente['Cognome'];?></td>
						<td><?php echo $utente['RagSoc'];?></td>
						<td><?php echo $utente['E_Mail'];?></td>
						<!-- <td><?php //echo $utente['Login'];?></td>-->
						<td><?php echo $accessiUtente['UltimoAccesso'] ;?></td>
						<td><?php echo $accessiUtente['Accessi'] ?></td>
					</tr>
					
<?php }
		
	}
	//}?>					
					<tr class="total">
						<td colspan="5">Totale</td>
						<td><?php print $totale;?></td>
					</tr>
				</tbody>
			</table>


<h2>Accessi Vetrina per utenze di Test</h2>

			<table class="tableAccessi">
			
				<thead class="table-head">
					<tr>
						<td>Id</td>
						<td>Partecipante</td>
						<td>Paese</td>
						<td>E_Mail</td>
						<!-- <td>Login</td>-->
						<td>Ultimo Accesso</td>
						<td>Accessi</td>
					</tr>
				</thead>
				<tbody>
<?php 
$totaleTest = 0;

foreach ($utentiTest AS $uid => $utenteTest){

	/*
	print "$uid<pre>";
	print_r($utenteTest);
	print "</pre>";
	*/
	$totaleTest += $accessi[$uid]['Accessi'];
?>
					<tr class="accessi">
						<td><span class="id"><?php echo $uid;?></span></td>
						<td><?php echo $utenteTest['Nome'].' '.$utenteTest['Cognome'];?></td>
						<td><?php echo $utenteTest['RagSoc'];?></td>
						<td><?php echo $utenteTest['E_Mail'];?></td>
						<!-- <td><?php //echo $utenteTest['Login'];?></td>-->
						<td><?php echo $accessi[$uid]['UltimoAccesso'] ;?></td>
						<td><?php echo $accessi[$uid]['Accessi'] ?></td>
					</tr>
					
<?php }
		?>					
					<tr class="total">
						<td colspan="5">Totale</td>
						<td><?php print $totaleTest;?></td>
					</tr>
				</tbody>
			</table>


<?php 

	
	
	
	
}




public function professionistiIscritti($print = false){

	$totalResult=array(
			self::BEFOREYESTERDAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
			self::YESTERDAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
			self::TODAY=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0),
			self::TOTAL=>array(self::FIRMARE=>0,self::PAGARE=>0,self::ISCRITTE=>0,self::ONLINE=>0)
	);

	$id = self::IDIMPRESE;
	$firmare ='StatoRegistrazione = '. self::FIRMARE ;
	$pagare ='StatoRegistrazione = ' . self::PAGARE ;
	$onLine = 'StatoRegistrazione = ' . self::ONLINE ;
	$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
	$nonIscritte ='(StatoRegistrazione = '. self::FIRMARE .' or StatoRegistrazione = ' . self::PAGARE . ')';


	/* --------------- Start estrazione dati da database  imprese non  iscritte------------------*/


		
	$sqlFirmareBeforeYesterday = "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa
	 								Where I.Id > $id and $firmare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione < '$this->yesterday' ";
	
	$sqlFirmareYesterday 		= "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa 
									Where I.Id > $id and $firmare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->yesterday' ";
	
	$sqlFirmareToday 			= "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa 
									Where I.Id > $id and $firmare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->today'		";

	$firmareBeforeYesterdayResult = $this->db->GetRow($sqlFirmareBeforeYesterday);


	$firmareYesterdayResult = $this->db->GetRow($sqlFirmareYesterday);
	$firmareTodayResult = $this->db->GetRow($sqlFirmareToday);


	$sqlPagareBeforeYesterday = "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa 
									Where I.Id > $id and $pagare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione < '$this->yesterday' ";
	
	$sqlPagareYesterday 		= "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa 
									Where I.Id > $id and $pagare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->yesterday' ";
	
	$sqlPagareToday 			= "	Select count(StatoRegistrazione) 
									From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa 
									Where I.Id > $id and $pagare and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->today'		";

	$pagareBeforeYesterdayResult = $this->db->GetRow($sqlPagareBeforeYesterday);
	$pagareYesterdayResult = $this->db->GetRow($sqlPagareYesterday);
	$pagareTodayResult = $this->db->GetRow($sqlPagareToday);


	$totalResult[self::BEFOREYESTERDAY][self::FIRMARE] += $firmareBeforeYesterdayResult[0];
	$totalResult[self::BEFOREYESTERDAY][self::PAGARE] 	+= $pagareBeforeYesterdayResult[0];

	$totalResult[self::YESTERDAY][self::FIRMARE] 	+= $firmareYesterdayResult[0];
	$totalResult[self::YESTERDAY][self::PAGARE]		+= $pagareYesterdayResult[0];

	$totalResult[self::TODAY][self::FIRMARE]	+= $firmareTodayResult[0];
	$totalResult[self::TODAY][self::PAGARE] 	+= $pagareTodayResult[0];



	/* --------------- End estrazione dati da database  imprese non iscritte------------------*/


	/* --------------- Start estrazione dati da database  imprese iscritte------------------*/


		
	$sqlIscritteBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $iscritte and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione < '$this->yesterday' ";
	$sqlIscritteYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $iscritte and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->yesterday' ";
	$sqlIscritteToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $iscritte and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->today'		";
	
	$iscritteBeforeYesterdayResult = $this->db->GetRow($sqlIscritteBeforeYesterday);


	$iscritteYesterdayResult = $this->db->GetRow($sqlIscritteYesterday);
	$iscritteTodayResult = $this->db->GetRow($sqlIscritteToday);


	$sqlOnLineBeforeYesterday = "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $onLine and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione < '$this->yesterday' ";
	$sqlOnLineYesterday 		= "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $onLine and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->yesterday' ";
	$sqlOnLineToday 			= "Select count(StatoRegistrazione) From EXPO_T_Imprese As I join EXPO_TJ_Imprese_OrdiniProfessionali as TJ on I.Id = TJ.Id_Impresa Where I.Id > $id and $onLine and TJ.Id_OrdineProfessionale > 0 and DataRegistrazione = '$this->today'		";

	$onLineBeforeYesterdayResult = $this->db->GetRow($sqlOnLineBeforeYesterday);
	$onLineYesterdayResult = $this->db->GetRow($sqlOnLineYesterday);
	$onLineTodayResult = $this->db->GetRow($sqlOnLineToday);


	$totalResult[self::BEFOREYESTERDAY][self::ISCRITTE] += $iscritteBeforeYesterdayResult[0];
	$totalResult[self::BEFOREYESTERDAY][self::ONLINE] 	+= $onLineBeforeYesterdayResult[0];

	$totalResult[self::YESTERDAY][self::ISCRITTE] 	+= $iscritteYesterdayResult[0];
	$totalResult[self::YESTERDAY][self::ONLINE]		+= $onLineYesterdayResult[0];

	$totalResult[self::TODAY][self::ISCRITTE]	+= $iscritteTodayResult[0];
	$totalResult[self::TODAY][self::ONLINE] 	+= $onLineTodayResult[0];

	/* --------------- End estrazione dati da database  imprese iscritte------------------*/



	/*---- Calcolo risultati totali  ----------*/

	$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::BEFOREYESTERDAY][self::FIRMARE];
	$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::YESTERDAY][self::FIRMARE];
	$totalResult[self::TOTAL][self::FIRMARE] += $totalResult[self::TODAY][self::FIRMARE];

	$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::BEFOREYESTERDAY][self::PAGARE];
	$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::YESTERDAY][self::PAGARE];
	$totalResult[self::TOTAL][self::PAGARE] += $totalResult[self::TODAY][self::PAGARE];

	$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::BEFOREYESTERDAY][self::ISCRITTE];
	$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::YESTERDAY][self::ISCRITTE];
	$totalResult[self::TOTAL][self::ISCRITTE] += $totalResult[self::TODAY][self::ISCRITTE];

	$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::BEFOREYESTERDAY][self::ONLINE];

	$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::YESTERDAY][self::ONLINE];
	$totalResult[self::TOTAL][self::ONLINE] += $totalResult[self::TODAY][self::ONLINE];




	if ($print){
		/*------ stampa risultati-------------------*/

		?>
			<!-- <pre><?php //print_r ($totalResult);?></pre><br> -->
			<h2>Professionisti Iscritti</h2>

			<table class="table">
			
				<thead class="table-head">
					<tr>
						<td>Firmare</td>
						<td>Pagare</td>
						<td>Iscritte</td>
						<td>ON Line</td>
						<td>Data</td>
					</tr>
				</thead>
				<tbody>
					<tr class="beforeYesterday">
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::PAGARE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::BEFOREYESTERDAY][self::ONLINE];?></td>
						<td>Before Yesterday</td>
					</tr>
					<tr class="yesterday">
						<td><?php print $totalResult[self::YESTERDAY][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::PAGARE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::YESTERDAY][self::ONLINE];?></td>
						<td>Yesterday</td>
					</tr>
					<tr class="today">
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::FIRMARE], $totalResult[self::TODAY][self::FIRMARE]);?></td>
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::PAGARE], $totalResult[self::TODAY][self::PAGARE]);?></td>
						<td><?php $this->trend($totalResult[self::YESTERDAY][self::ISCRITTE], $totalResult[self::TODAY][self::ISCRITTE]);?></td>
						<td><?php print $totalResult[self::TODAY][self::ONLINE];?></td>
						<td>Today</td>
					</tr>
					<tr class="total">
						<td><?php print $totalResult[self::TOTAL][self::FIRMARE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::PAGARE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::ISCRITTE];?></td>
						<td><?php print $totalResult[self::TOTAL][self::ONLINE]; ?></td>
						<td>Totale</td>
					</tr>
				</tbody>
			</table>
<?php 
		}
		return $totalResult;
	}

public function questionari(){

	$questionariSql = "SELECT NQuestionario,Testo FROM EXPO_Tlk_Questionari WHERE Tipo='D'";
	
	$questionari = $this->db->GetRows($questionariSql,null,"Estrazione Questionari");

	foreach ($questionari as $key => $value){
		$Nquestionario = $value['NQuestionario'];
		$Testo = $value['Testo'];

	
		?>
		<table >
				
			<thead class="table-head">
				<tr>
					<td colspan ="2"><?php print $Testo;?></td>
					
				</tr>
			</thead>
	<?php 
	
		$sqlRisposte = "SELECT Id,Testo,OrdineRisposta FROM EXPO_Tlk_Questionari WHERE Tipo='R' AND NQuestionario = $Nquestionario  ORDER BY OrdineRisposta";
		$totaleRisposte = 0;
		$risposte = $this->db->GetRows($sqlRisposte,null,"Risposte questionari report");
		
		$risposteQuestionario = array ();
		
		foreach ($risposte as $key => $risposta){
			
			$idQuestionario = $risposta["Id"];
			$numRispostaSql = "SELECT count(Id) as NumRisposte, IdQuestionario
							FROM   EXPO_TJ_Imprese_Questionari WHERE IdQuestionario = $idQuestionario";
			$numRisposta = $this->db->GetRow($numRispostaSql,"NumRisposte",null, "estrazione Numero per risposta questionari report");
			$totaleRisposte += $numRisposta;
			
			
			
			$risposteQuestionario[$risposta['OrdineRisposta']]['Testo'] = $risposta["Testo"];
			
			$risposteQuestionario[$risposta['OrdineRisposta']]['Numero'] = $numRisposta;
			
		}	
		
			
		$end = count($risposteQuestionario);
		for ($i = 1; $i < $end; $i++){
			
		?>
			<tr>
				<td class="questionariTesto">    
				<?php echo $risposteQuestionario[$i]['Testo']; ?>
				</td>
				<td class="questionariNumRisposte">    
				<?php echo $risposteQuestionario[$i]['Numero']; echo " ".$this->percent($risposteQuestionario[$i]['Numero'], $totaleRisposte);  ?>
				</td>
			</tr>   
			<?php
		}
		

		?>
			<tr>
				<td class="questionariTesto">    
				<?php echo $risposteQuestionario[0]['Testo']; ?>
				</td>
				<td class="questionariNumRisposte">    
				<?php echo $risposteQuestionario[0]['Numero']; echo " ".$this->percent($risposteQuestionario[0]['Numero'], $totaleRisposte);  ?>
				</td>
			</tr>   
			<?php
		
	}
	?>
	
		<tr class="total">
				<td >Totale</td>
				<td> <?php echo $totaleRisposte;  ?></td>
			</tr>
		</table>
	<?php 
	
}






}
?>


