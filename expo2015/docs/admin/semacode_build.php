<?php
/**
 * Build a semacode from given URL parameters
 *
 * Parameters:
 *
 * text - the text to encode (limited charset)
 * type - the wanted outoput: svg, png or table
 * size - for png type only: width of the image
 */

include "progetto.inc";
//include $PROGETTO."/prepend.php3";
include $PROGETTO."/semacode/semacode.inc";

/*
$text = $_REQUEST['text'];
$type = $_REQUEST['type'];
$size = (int) $_REQUEST['size'];
*/

$text = base64_decode($imglink);
//$text = $imglink;
$type = "png";
$size = 160;
if(!$size) $size = 160;

// strip non-supported chars (is this correct?)
$text = preg_replace('/[^\w!\"#$%&\'()*+,\-\.\/:;<=>?@[\\_\]\[{\|}~\r*]+ /','', $text);

// encode
$semacode = new Semacode();

if($type == 'png'){
    $semacode->sendPNG($text,$size);
}elseif($type == 'svg'){
    header('Content-Type: image/svg');
    echo $semacode->asSVG($text);
}else{
    echo $semacode->asHTMLTable($text);
}
?>
