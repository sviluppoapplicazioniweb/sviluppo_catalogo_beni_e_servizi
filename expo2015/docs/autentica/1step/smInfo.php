<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<title>Innovery SpA</title>
<link rel="stylesheet" href="style.css" media="all"/>

<object id="pdlccard"
		classid="CLSID:57F9FC08-EEAF-49a8-AE3B-843A564DCC19"
		codebase="pdlc.cab#Version=3,0,0,0"
		type="application/x-oleobject">
</object>
<object id="pdlcxml"
	classid="CLSID:E04BEB79-E633-4a40-8084-8A873B9008AB"
	codebase="pdlc.cab#Version=3,0,0,0"
	type="application/x-oleobject">
</object>

<script type="text/vbscript" src="pdlc.vbs">
</script>

<script type="text/javascript" src="base64.js">
</script>
<SCRIPT TYPE="text/JavaScript">
function getPersonalData(zform) {	
		//var l = SignBasket.SignMessageLen("ATRURL=https://fgs.innovery.it/IBSAc1.ini",20);
		
		//if (l>0) {
			var datiXmlUsr;
			//datiXmlUsr = pdlccard.GetAsnBerFileContent("3F002F02");
		    zform.ATRcarta.value=pdlccard.CardAtr;
		    zform.CNSManufacturer.value=pdlccard.CNSManufacturer;		    
		    zform.CNSType.value=pdlccard.CNSType;	
		    zform.CardReaderName.value=pdlccard.CardReaderName;	
		    zform.SISSCertificateIssuer.value=pdlccard.SISSCertificateIssuer;		
		    zform.CryptographicProvider.value=pdlccard.CryptographicProvider;	
		    datiXmlUsrASN1 = pdlccard.GetAsnBerFileContent("EF.NKAF");
		    datiXmlUsr=pdlcxml.ASN1BERAsXML(datiXmlUsrASN1);	
		        
		    //zform.BinaryFileContentAsBase64.value=Base64.decode(pdlccard.GetBinaryFileContentAsBase64("EF.NKAF"));		
		    zform.BinaryFileContent.value=datiXmlUsr;
		    
		    //datiXmlUsr = asnparser.getASN1BERAsXML(scardAccess.getAsnBerFileContent("EF.NKAF"));
	    	    		    		    
		//} else {
		//    alert("Error in prepare sign!");
		//}
	}
</SCRIPT>

</head>

<body>
<?php
	//print_r($_SERVER);
?>
<!-- Richiamo componenti Lombardia Informatica -->

<img src="company_logo.png" width="212" height="40" alt="Company Logo" border="0" />
<hr class="lineaOrizzontale" />
<div class="title">Verifica Client Authentication : CNS-CRS</div>
<table border="0" cellpadding="1" cellspacing="1" class="mainTab">
	<tr>
		<td class="label">Variable Name</td><td class="label">Description</td><td class="label">Value</td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_VERIFY</td><td class="label">NONE, SUCCESS, GENEROUS or FAILED:reason</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_VERIFY'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_M_SERIAL</td><td class="label">The serial of the client certificate</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_M_SERIAL'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN</td><td class="label" rowspan="14" valign="top">Subject DN in client's certificate</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_C</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_C'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_ST</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_ST'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_L</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_L'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_O</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_O'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_OU</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_OU'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_CN</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_CN'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_T</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_T'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_I</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_I'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_G</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_G'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_S</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_S'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_D</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_D'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_UID</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_UID'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_S_DN_Email</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_S_DN_Email'] ?></td>
	</tr>
	<tr>
		<td class="label">SSL_CLIENT_CERT</td><td class="label">PEM-encoded client certificate</td><td class="value"><?php echo $_SERVER['SSL_CLIENT_CERT'] ?></td>
	</tr>
</table>
<form name="userData" action="smInfo.php" method="post">
	<table border="0" cellpadding="1" cellspacing="1" class="mainTab" width="100%">
		<tr>
			<td colspan="2">Per la lettura dei file memorizzati su CRS &egrave; necessario esplicitare il proprio consenso all'accesso ai dati...</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type='button' name='sb2' value='Leggi Dati Utente' onClick='javascript:getPersonalData(document.userData);'></input>
			</td>
		</tr>
		<tr>
			<td style="width: 91px">ATR</td><td>
			<input type='text' name='ATRcarta' value='' style="width:95%" ></input></td>
		</tr>
		<tr>
			<td style="width: 91px">CNSManufacturer</td><td><input type='text' name='CNSManufacturer' value='' style="width95:100%" ></input></td>
		</tr>	
		<tr>
			<td style="width: 91px">CNSType</td><td><input type='text' name='CNSType' value='' style="width:95%" ></input></td>
		</tr>
		<tr>
			<td style="width: 91px">CardReaderName</td><td><input type='text' name='CardReaderName' value='' style="width:95%" ></input></td>
		</tr>
		<tr>
			<td style="width: 91px">SISSCertificateIssuer</td><td><input type='text' name='SISSCertificateIssuer' value='' style="width:95%" ></input></td>
		</tr>
		<tr>
			<td style="width: 91px">CryptographicProvider</td><td><input type='text' name='CryptographicProvider' value='' style="width:95%" ></input></td>
		</tr>	
		<tr>
			<td style="width: 91px" valign="top">BinaryFileContent</td><td><textarea rows="10" cols="80" name='BinaryFileContent' ></textarea></td>
		</tr>										
	</table>
</form>
</body>

</html>
