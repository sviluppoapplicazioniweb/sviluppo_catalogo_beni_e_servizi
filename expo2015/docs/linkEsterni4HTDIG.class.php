<?php

include "configuration.inc";
include $PROGETTO . "/prepend.php3";
include "$PROGETTO/view/lib/db.class.php";


class linkEsterni4HTDIG{
	private $db;
	private $id = ' > 27';
	
	public function __construct(){

		$this->db = new DataBase();
		
	}
	
	/**
	 * Funzione che estrae i link relativi ai WebSiteURL delle imprese
	 * 
	 * return $string "WebSiteURL".$rows['Id']."]$url}"
	 */
	public function getWebSiteURL(){
		
		$webSiteString = '';
		
		foreach ($this->db->GetRows("Select Id,WebSiteURL From EXPO_T_Imprese where Id $this->id and  StatoRegistrazione > 3 and ( WebSiteURL <> '' and WebSiteURL <> 'http://' ) ORDER BY Id") AS $rows) {
			$url = str_replace("http://", "", $rows['WebSiteURL']);

			if (!(strpos($url, ".pdf"))){
				//$webSiteString .= "WebSiteURL]WebSiteURL".$rows['Id']."]$url}";
				$webSiteString .= "WebSiteURL]WebSiteURL".$rows['Id']."]$url}";
				
			}
		}
		
		return $webSiteString; 
		
	}
	
	/**
	 * Funzione che estrae i link InternazionalizzazioneLink imprese
	 *
	 * return $string "InternazionalizzazioneLink".$rows['Id']."]$url}"
	 */
	public function getInternazionalizzazioneLink(){
	
		$internazionalizzazioneLinkString = '';


		foreach ($this->db->GetRows("Select Id,InternazionalizzazioneLink From EXPO_T_Imprese where Id $this->id and  StatoRegistrazione > 3 and InternazionalizzazioneLink <> '' and InternazionalizzazioneLink <> 'http://' ORDER BY Id") AS $rows) {
			$url = str_replace("http://", "", $rows['InternazionalizzazioneLink']);

			if (!(strpos($url, ".pdf"))){
				$internazionalizzazioneLinkString .= "InternazionalizzazioneLink]InternazionalizzazioneLink".$rows['Id']."]$url}";
			}
		}
		
		return $internazionalizzazioneLinkString;
	}
	
	
	/**
	 * Funzione che estrae i link ReferenzeLink imprese
	 *
	 * return $string "ReferenzeLink".$rows['Id']."]$url}"
	 */
	public function getReferenzeLink(){
	
		$referenzeLinkString = '';

		foreach ($this->db->GetRows("Select Id,ReferenzeLink From EXPO_T_Imprese where Id $this->id and  StatoRegistrazione > 3 and ReferenzeLink <> '' and ReferenzeLink <> 'http://' ORDER BY Id") AS $rows) {
			$url = str_replace("http://", "", $rows['ReferenzeLink']);
			
			if (!(strpos($url, ".pdf"))){
				$referenzeLinkString .= "ReferenzeLink]ReferenzeLink".$rows['Id']."]$url}";
			}
		}
		
		return $referenzeLinkString;
	}
	
	/**
	 * Funzione che estrae i link Qualita_GreenLink  imprese
	 *
	 * return $string "Qualita_GreenLink".$rows['Id']."]$url}"
	 */
	public function getQualita_GreenLink(){
	
		$qualita_GreenLinkString = '';

		foreach ($this->db->GetRows("Select Id,Qualita_GreenLink From EXPO_T_Imprese where Id $this->id and  StatoRegistrazione > 3 and Qualita_GreenLink <> '' and Qualita_GreenLink <> 'http://' ORDER BY Id") AS $rows) {
			$url = (str_replace("http://", "", $rows['Qualita_GreenLink']));

			if (!(strpos($url, ".pdf"))){
				$qualita_GreenLinkString .= "Qualita_GreenLink]Qualita_GreenLink".$rows['Id']."]$url}";
			}
			
		}
	
		return $qualita_GreenLinkString;
	}
	
	
	/**
	 * Funzione che estrae i link RetiImpresaLink  imprese
	 *
	 * return $string "RetiImpresaLink".$rows['Id']."]$url}"
	 */
	public function getRetiImpresaLink(){
	
		$retiImpresaLinkString = '';

		foreach ($this->db->GetRows("Select Id,RetiImpresaLink From EXPO_T_Imprese where Id $this->id and  StatoRegistrazione > 3 and RetiImpresaLink <> '' and RetiImpresaLink <> 'http://' ORDER BY Id") AS $rows) {
			$url = str_replace("http://", "", $rows['RetiImpresaLink']);
			
			if (!(strpos($url, ".pdf"))){
				$retiImpresaLinkString .= "RetiImpresaLink]RetiImpresaLink".$rows['Id']."]$url}";
			}

		}
		
		return $retiImpresaLinkString;
	}
	

	/**
	 * Funzione che estrae i link PRODOTTI-ProdottoLink prodotti 
	 *
	 * return $string "PRODOTTI-ProdottoLink".$rows['Id']."]$url}"
	 */
	public function getProdottiProdottoLink(){
	
		$prodottoLinkString = '';
	
		foreach ($this->db->GetRows("SELECT Id,ProdottoLink FROM EXPO_T_Prodotti WHERE IdImpresa $this->id and ProdottoLink <> '' and ProdottoLink  <> 'http://' ORDER BY Id") AS $rows) {
			$url = (str_replace("http://", "", $rows['ProdottoLink']));
			if (!(strpos($url, ".pdf"))){
				$prodottoLinkString .= "PRODOTTI-ProdottoLink]PRODOTTI-ProdottoLink".$rows['Id']."]$url}";
			}
		
		}
		
		return $retiImpresaLinkString;
	}
		

	/**
	 * Funzione che estrae i link CertificazioneLink prodotti
	 *
	 * return $string "PRODOTTI-CertificazioneLink".$rows['Id']."]$url}"
	 */
	public function getProdottiCertificazioneLink(){

		$certificazioneLinkString = '';

		foreach ($this->db->GetRows("SELECT Id,CertificazioneLink FROM EXPO_T_Prodotti WHERE IdImpresa $this->id and CertificazioneLink <> '' and CertificazioneLink  <> 'http://' ORDER BY Id") AS $rows) {
			$url = (str_replace("http://", "", $rows['CertificazioneLink']));
			if (!(strpos($url, ".pdf"))){
				$certificazioneLinkString .= "PRODOTTI-CertificazioneLink]PRODOTTI-CertificazioneLink".$rows['Id']."]$url}";
			}

		}

		return $certificazioneLinkString;
	}

	/**
	 * Funzione che estrae i link AspettiLink prodotti
	 *
	 * return $string "PRODOTTI-AspettiLink".$rows['Id']."]$url}"
	 */
	public function getProdottiAspettiLink(){

		$aspettiLinkString = '';

		foreach ($this->db->GetRows("SELECT Id,AspettiLink FROM EXPO_T_Prodotti WHERE IdImpresa $this->id and AspettiLink <> '' and AspettiLink  <> 'http://' ORDER BY Id") AS $rows) {
			$url = (str_replace("http://", "", $rows['AspettiLink']));
			if (!(strpos($url, ".pdf"))){
				$aspettiLinkString .= "PRODOTTI-AspettiLink]PRODOTTI-AspettiLink".$rows['Id']."]$url}";
			}
		}
		
		return $aspettiLinkString;
	}
	

	/**
	 * Funzione che estrae i link BrevettiLink prodotti
	 *
	 * return $string "PRODOTTI-AspettiLink".$rows['Id']."]$url}"
	 */
	public function getProdottiBrevettiLink(){
	
		$brevettiLinkString = '';
	
		foreach ($this->db->GetRows("SELECT Id,BrevettiLink FROM EXPO_T_Prodotti WHERE IdImpresa $this->id and BrevettiLink <> '' and BrevettiLink  <> 'http://' ORDER BY Id") AS $rows) {
			$url = (str_replace("http://", "", $rows['BrevettiLink']));
			if (!(strpos($url, ".pdf"))){
				$brevettiLinkString .= "PRODOTTI-BrevettiLink]PRODOTTI-BrevettiLink".$rows['Id']."]$url}";
			}

		}
	
		return $brevettiLinkString;
	}
	
	/**
	 * Funziona che estrae tutti i link e ritorna un array
	 * @return array $esterni
	 */
	public function  getAllLink(){
		$link = '';
		
		$link .= $this->getWebSiteURL();
		$link .= $this->getInternazionalizzazioneLink();
		$link .= $this->getReferenzeLink();
		$link .= $this->getQualita_GreenLink();
		$link .= $this->getRetiImpresaLink();
		$link .= $this->getProdottiProdottoLink();
		$link .= $this->getProdottiCertificazioneLink();
		$link .= $this->getProdottiAspettiLink();
		$link .= $this->getProdottiBrevettiLink();
		
		
		$link = substr($link, 0, -6);
		
		$esterni = explode("}",$link);
		return $esterni;
	}
		
	
}