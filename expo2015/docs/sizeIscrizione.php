<?php

include "configuration.inc";

function foldersize($dirname) {	
   // se la cartella non esiste o non ha i permessi di lettura, ritorno false
   if (!is_dir($dirname) || !is_readable($dirname))
       return false;

   // immagazzino la cartella nell'array
   $dirname_stack[] = $dirname;
   $size = 0;
	
   do {
// assegno a $dirname il primo elemento di $dirname_stack e lo tolgo dall'array
       $dirname = array_shift($dirname_stack);
       $handle = opendir($dirname);


// scorro tutti i file presenti
       while (false !== ($file = readdir($handle))) {
           if ($file != '.' && $file != '..' && is_readable($dirname . DIRECTORY_SEPARATOR . $file)) {
               if (is_dir($dirname . DIRECTORY_SEPARATOR . $file)) {
   // inserisco nell'array il nome e l'indirizzo del file
                   $dirname_stack[] = $dirname . DIRECTORY_SEPARATOR . $file;
               }
// aggiunge la dimensione di ogni file
               $size += filesize($dirname . DIRECTORY_SEPARATOR . $file);
           }
       }
// chiude la directory
       closedir($handle);
   } while (count($dirname_stack) > 0);

   return $size;
}



/** richiamo della funzione: stampa della dimensione della cartella file **/
$totalSize= 0;

for ($index = 1; $index <5; $index++){
	//print $index."<br>";
	
	$dir = Config::ROOTFILES."aziende$index/";
	
	$totalSize +=$mb_occupati=foldersize("$dir")/(1024*1024);
	print "aziende$index: ". ceil(($mb_occupati)). "MB<br><br>";
}

print "Totale spazio: ".ceil(($totalSize)). "MB";
//echo ceil(($mb_occupati))." MB";


?>