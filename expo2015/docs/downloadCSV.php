<?php

$tab = $_REQUEST['tab'];

include "configuration.inc";
require_once($PROGETTO . "/view/lib/db.class.php");
$db = new DataBase();

$arrayTitolo = array();
$arrayDati = array();
$nomeFile = $tab . "_" . date("Ymd") . ".csv";
$idNonTest = 27;


if (strcmp($tab, "formeGiuridiche") == 0) {
    $arrayTitolo = array('Imprese', 'Iscritti');
    $query = "SELECT * FROM EXPO_Tlk_Forme_Giuridiche ORDER BY Descrizione ";
    foreach ($db->GetRows($query) AS $rows) {
        $array = array();
        $desc = $rows['Descrizione'];
        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and FormaGiuridica = '" . mysql_escape_string(htmlentities($desc)) . "' ORDER BY RagioneSociale";
        $array['descrizione'] = $desc;
        $array['iscritti'] = $db->NumRows($queryImprese);
        $arrayDati[] = $array;
    }
} elseif (strcmp($tab, "retiImprese") == 0) {
    $arrayTitolo = array('Imprese');
    $array = array();
    $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and IsRetiImpresa = 'Y' ORDER BY RagioneSociale";
    foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
        $array[] = html_entity_decode($rowsImprese['RagioneSociale']);
        $arrayDati[] = $array;
    }
} elseif (strcmp($tab, "province") == 0) {
    $arrayTitolo = array('Province', 'Iscritti');
    $query = "SELECT DISTINCT Descrizione,Pr FROM EXPO_T_Imprese AS T JOIN Tlk_Province AS T2 "
            . "ON T.Pr = T2.Sigla"
            . " WHERE T.Id > $idNonTest ORDER BY Pr";
    foreach ($db->GetRows($query) AS $rows) {
        $array = array();
        $array['province'] = $rows['Descrizione'];
        $pr = $rows['Pr'];
        $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and Pr = '$pr' ORDER BY RagioneSociale";
        $array['iscritti'] = $db->NumRows($queryImprese);
        $arrayDati[] = $array;
    }
} elseif (strcmp($tab, "iscrizioni") == 0) {
    $arrayTitolo = array('Anno', 'Mese', 'Imprese');
    if (isset($_POST['dataInizio']) && strcmp($_POST['dataInizio'], "") != 0 && isset($_POST['dataFine']) && strcmp($_POST['dataFine'], "") != 0) {
        $dataInizio = date_format(date_create($_POST['dataInizio']), "Y-m-d");
        $dataFine = date_format(date_create($_POST['dataFine']), "Y-m-d");
    } elseif (isset($_POST['periodo']) && strcmp($_POST['periodo'], "") != 0) {
        $dataInizio = $_POST['periodo'];
        $dataFine = date("Y-m-d");
    } else {
        $dataInizio = "2014-01-01";
        $dataFine = date("Y-m-d");
    }

    list($annoInizio, $meseInizio, $giornoInizio) = explode("-", $dataInizio);
    list($annoFine, $meseFine, $giornoFine) = explode("-", $dataFine);

//NUMERO DI IMPRESE X MESE
    $arrayMesi = array("01" => "Gennaio", "02" => "Febbraio", "03" => "Marzo", "04" => "Aprile", "05" => "Maggio", "06" => "Giugno", "07" => "Luglio", "08" => "Agosto", "09" => "Settembre", "10" => "Ottobre", "11" => "Novembre", "12" => "Dicembre");
    for ($anno = $annoInizio; $anno <= $annoFine; $anno ++) {
        $meseFineCiclo = 12;
        $meseInizioCiclo = 1;
        if ($anno == $annoInizio) {
            $meseInizioCiclo = $meseInizio;
        }

        if ($anno == $annoFine) {
            $meseFineCiclo = $meseFine;
        }

        for ($i = $meseInizioCiclo; $i <= $meseFineCiclo; $i++) {

            $mese = sprintf("%02d", $i);
            $array = array();
            $dataDB = $anno . "-" . $mese;

            $queryImprese = "SELECT * FROM EXPO_T_Imprese WHERE Id > $idNonTest and DataRegistrazione LIKE '$dataDB%' ORDER BY RagioneSociale";
            foreach ($db->GetRows($queryImprese) AS $rowsImprese) {
                $array['anno'] = $anno;
                $array['mese'] = $arrayMesi[$mese];
                $array['imprese'] = html_entity_decode($rowsImprese ['RagioneSociale']);
                $arrayDati[] = $array;
            }
        }
    }
} elseif (strcmp($tab, "login") == 0 || strcmp($tab, "visiteImprese") == 0 || strcmp($tab, "visiteProdotti") == 0 || strcmp($tab, "trattative") == 0 || strcmp($tab, "trattativeMacroCategorie") == 0) {
    if (isset($_POST['dataInizio']) && strcmp($_POST['dataInizio'], "") != 0 && isset($_POST['dataFine']) && strcmp($_POST['dataFine'], "") != 0) {
        $dataInizioInput = $_POST['dataInizio'];
        $dataFineInput = $_POST['dataFine'];
        $dataInizio = date_format(date_create($_POST['dataInizio']), "Y-m-d");
        $dataFine = date_format(date_create($_POST['dataFine']), "Y-m-d");
    } elseif (isset($_POST['periodo']) && strcmp($_POST['periodo'], "") != 0) {
        $dataInizio = $_POST['periodo'];
        $dataPeriodo = $_POST['periodo'];
        $dataFine = date("Y-m-d");
    } else {
        $dataInizio = "2014-01-01";
        $dataFine = date("Y-m-d");
    }

    require_once($PROGETTO . "/view/lib/getReport.class.php");
    $r = new GetReport();
    if(strcmp($tab, "login") == 0 || strcmp($tab, "visiteImprese") == 0 || strcmp($tab, "visiteProdotti") == 0) {
        $arrayLogs = $r->getReportVetrina($dataInizio, $dataFine);
    } else {
        $arrayLogs = $r->getReportTrattative($dataInizio, $dataFine);
    }
    


    if (strcmp($tab, "login") == 0) {
        $arrayTitolo = array('Anno', 'Mese', 'Paese', 'Visite');

        foreach ($arrayLogs AS $arrayLog) {
            if ($arrayLog['totaliLoginMese'] > 0) {
                foreach ($arrayLog['login'] AS $nome => $visite) {
                    $array = array();
                    $array['anno'] = $arrayLog['anno'];
                    $array['mese'] = $arrayLog['mese'];
                    $array['paese'] = $nome;
                    $array['visite'] = $visite;
                    $arrayDati[] = $array;
                }
            }
        }
    } elseif (strcmp($tab, "visiteImprese") == 0) {
        $arrayTitolo = array('Anno', 'Mese', 'Imprese', 'Paese', 'Visite');

        foreach ($arrayLogs AS $arrayLog) {
            if ($arrayLog['totaliVisiteMeseImprese'] > 0) {
                foreach ($arrayLog['imprese'] AS $idImpresa => $arrayImprese) {
                    $arrayListaPesi = $arrayLog['imprese'][$idImpresa]['paesi'];
                    ksort($arrayListaPesi);
                    foreach ($arrayListaPesi AS $nome => $visite) {
                        $array = array();
                        $array['anno'] = $arrayLog['anno'];
                        $array['mese'] = $arrayLog['mese'];
                        $array['imprese'] = html_entity_decode($arrayLog['imprese'][$idImpresa]['nome']);
                        $array['paese'] = $nome;
                        $array['visite'] = $visite;
                        $arrayDati[] = $array;
                    }
                }
            }
        }
    } elseif (strcmp($tab, "visiteProdotti") == 0) {
        $arrayTitolo = array('Anno', 'Mese', 'Prodotti', 'Imprese', 'Paese', 'Visite');

        foreach ($arrayLogs AS $arrayLog) {
            if ($arrayLog['totaliVisiteMeseProdotti'] > 0) {
                foreach ($arrayLog['prodotti'] AS $idProdotto => $arrayImprese) {
                    $nomeImpresa = $arrayLog['prodotti'][$idProdotto]['impresa'];
                    $nomeProdotto = $arrayLog['prodotti'][$idProdotto]['nome'];
                    $arrayListaPesi = $arrayLog['prodotti'][$idProdotto]['paesi'];
                    ksort($arrayListaPesi);
                    foreach ($arrayListaPesi AS $nome => $visite) {
                        $array = array();
                        $array['anno'] = $arrayLog['anno'];
                        $array['mese'] = $arrayLog['mese'];
                        $array['prodotti'] = html_entity_decode($arrayLog['prodotti'][$idProdotto]['nome']);
                        $array['imprese'] = html_entity_decode($arrayLog['prodotti'][$idProdotto]['impresa']);
                        $array['paese'] = $nome;
                        $array['visite'] = $visite;
                        $arrayDati[] = $array;
                    }
                }
            }
        }
    } elseif (strcmp($tab, "trattative") == 0) {
        $arrayTitolo = array('Anno', 'Mese', 'Paese', 'Chiuse');
        foreach ($arrayLogs AS $arrayLog) {
            if ($arrayLog['totaliMese'] > 0) {
                foreach ($arrayLog['trattative'] AS $nome => $visite) {
                    $array = array();
                    $array['anno'] = $arrayLog['anno'];
                    $array['mese'] = $arrayLog['mese'];
                    $array['paese'] = $nome;
                    $array['visite'] = $visite;
                    $arrayDati[] = $array;
                }
            }
        }
    } elseif (strcmp($tab, "trattativeMacroCategorie") == 0) {
        $arrayTitolo = array('Anno', 'Mese', 'Categoria', 'Paese', 'Chiuse');
        foreach ($arrayLogs AS $arrayLog) {
            if ($arrayLog['totaliMeseCategorie'] > 0) {
                foreach ($arrayLog['categorie'] AS $categoria => $arrayCategorie) {
                    $arrayListaPesi = $arrayCategorie['paese'];
                    ksort($arrayListaPesi);
                    foreach ($arrayListaPesi AS $nome => $visite) {
                        $array = array();
                        $array['anno'] = $arrayLog['anno'];
                        $array['mese'] = $arrayLog['mese'];
                        $array['categoria'] = html_entity_decode($categoria);
                        $array['paese'] = $nome;
                        $array['visite'] = $visite;
                        $arrayDati[] = $array;
                    }
                }
            }
        }
    }
}


// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $nomeFile);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, $arrayTitolo, ";");


// loop over the rows, outputting them
foreach ($arrayDati AS $row)
    fputcsv($output, $row, ";");
?>
