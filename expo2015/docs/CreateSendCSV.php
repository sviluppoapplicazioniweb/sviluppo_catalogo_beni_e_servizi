<?php
error_reporting(-1);
$pathroot="/internet/datiExpo/include/";
require_once ($pathroot."configuration.inc");
//include_once $pathroot.$PROGETTO."/prepend.php3";
//print $pathroot."$PROGETTO/view/lib/db.class.php";
include $pathroot."$PROGETTO/view/lib/db.class.php";


/*--------- Libraries for sendFile -------*/
include $pathroot."lib/phpsec/Net/SFTP.php";
include $pathroot."lib/phpsec/Math/BigInteger.php";
include $pathroot."lib/phpsec/Crypt/Hash.php";
include $pathroot."lib/phpsec/Crypt/Random.php";
include $pathroot."lib/phpsec/Crypt/RC4.php";

/*--------- Libraries for sendFile -------*/
/*include $pathroot.'/send_mail.inc';
include_once $pathroot.'/lib/PHPMailer/class.smtp.php';
include_once $pathroot.'/htmlparser.inc';*/


/**
 * Class that Create And Send File CSV -Fatturazione Expo
 * @author fabio.bertoletti
 *
 */
class CreateCSV{

	private $host = "sftp.expo2015.org";
	private $login = "DIGICAMERE";
	private $password = "@F(jryGu";
	
	private $localPath;
	private $remotePath;
	
	private $localFile;
	private $remoteFile;
	
	const ISCRITTE 	= "4";
	const ONLINE 	= "5";
	
	public function __construct($fileName){

		$this->localPath = Config::ROOTFILESIMAGE."fatture_Accenture/";
		$this-> remotePath = "/DIGICAMERE/";
		
		$this -> localFile = $this->localPath.$fileName;
		$this -> remoteFile = $this->remotePath.$fileName;
		
	}

	/**
	 * Function that return valueFatturazione if is not Null else return valueAnagrafica
	 *
	 * @param $valueFatturazione
	 * @param $valueAnagrafica
	 */
	private function restituisciValore($valueFatturazione,$valueAnagrafica){
		if ($valueFatturazione==null){
			return $valueAnagrafica;
		}
		return $valueFatturazione;
	}

	
	/**
	 * Function that return the last Blank position into $string else return -1
	 * 
	 * @param string $string
	 * @return int $lastBlankIndex
	 */
	private function lastBlank($string){
		$lastBlankIndex=-1;
		for ($i=0; $i < strlen($string); $i++){
				
			if 	($string{$i}== " "){
				$lastBlankIndex = $i;
			}
		}

		return $lastBlankIndex;
	}


	/**
	 * Function that delete character of the $string after $length position 
	 * @param string $string
	 * @param int $lenght
	 * @return cut $string 
	 */
	private function cutByLenght($string,$lenght) {

		if (strlen($string)> $lenght){
				
			$result = substr($string, 0, $lenght-1);
			return $result;
		}
		return $string;
	}


	/**
	 * Function that split $value in more rows if lenght($value) is longer of $firstLenght
	 * @param string $value
	 * @param int $numField
	 * @param int $firstLenght
	 * @param int $lenghtSecond
	 */
	private function splitByLenght($string,$numField, $firstLenght,$secondLenght=null){

		$string =str_replace("&quot;", "", $string); 
		
		/*---- Start Inizialize Result ---------*/
		$result = array();
		for ($num = 0; $num < $numField; $num++ ){
			$result[]="";
		}

		/*---- End Inizialize Result ---------*/

		if ($secondLenght==null){
			$secondLenght=$firstLenght;
		}
		if (strlen($string)> $firstLenght){
			$lastBlank = $this->lastBlank(substr($string, 0, $firstLenght-1 ));
			$result[0] = substr($string, 0, $lastBlank);
			$string = substr($string, $lastBlank);
				
		}else{
			$result[0] = $string;
			return $result;
		}


		for ($i=1; $i < $numField; $i++){
				
			if (strlen($string) > $secondLenght){

				$lastBlank = $this->lastBlank(substr($string, 0, $secondLenght-1 ));
				$result[$i] = substr($string, 0, $lastBlank);
				$string = substr($string, $lastBlank);
			}else{

				$result[$i] = $string;
				return $result;
			}
		}

		return $result;

	}

	/**
	 * Function that create the information of file CSV.
	 * Returns false if there are no values into query 
	 * otherwise returns cvs text.
	 * 
	 * @param string $date (YYYY-MM-DD)
	 * @return string | boolean
	 */
	
	
	function createTextCSV($date="2013-01-01"){
		$tracciatoAnagrafica = array("Codice Cliente","Nome 1","Nome 2","Name 3","Name 4","Via","Via 2","Via 3","Via 4","Via 5","Cap","Localit�","Paese","Regione","Indirizzo Mail","Partita IVA 1","Partita IVA","IVA","Fatturato annuo","Divisa","Anno","Conto riconciliazione","Gruppo cash management","Chiave di classificazione");
		$tracciatoFatturazione = array("Nome 1","Nome 2","Name 3","Name 4","Via","Via 2","Via 3","Via 4","Via 5","Cap","Localit�","Paese","Regione","Indirizzo Mail","Partita IVA 1","Partita IVA","IVA");

		$MARKER = ';';

		$db = new DataBase();
		$iscritte ='(StatoRegistrazione = '. self::ISCRITTE .' or StatoRegistrazione = ' . self::ONLINE . ')';
		
		$sql = "SELECT A.Id,A.RagioneSociale,A.CodiceFiscale,A.PartitaIva,A.FasciaFatturato,A.Cap,A.Citta,A.Pr,A.Indirizzo,F.Denominazione,F.CodFiscale,F.PIVA,F.Via,F.CAP,F.PRV,F.Comune,A.EmailFatturazione FROM EXPO_T_Imprese as A LEFT OUTER JOIN expo_t_imprese_dati_fatturazione as F on A.Id = F.Id_Impresa LEFT OUTER JOIN EXPO_T_EXT_Lista_Imprese AS EXTIMP ON A.CodiceFiscale=EXTIMP.CodiceFiscale WHERE DataRegistrazione='$date' and $iscritte and IscrizioneSenzaPagamento = 'N'";
		$datiForCSV = $db->GetRows($sql);
		if (!$datiForCSV){
			return false;
		}
		
		$csv = '';
		/*---------- Create Head Anagrafica ---------------*/

		foreach($tracciatoAnagrafica as $field){
			$fieldHeadAnagrafica[] = $field;

		}
		$headAnagraficaCSV = '';
		$headAnagraficaCSV .= implode($MARKER, $fieldHeadAnagrafica);

		/*---------- End Create Head Anagrafica ---------------*/

		/*---------- Start Create Head Fatturazione ---------------*/

		foreach($tracciatoFatturazione as $field){
			$fieldHeadFatturazione[] = $field;

		}
		$headFatturazioneCSV = '';
		$headFatturazioneCSV .= implode($MARKER, $fieldHeadFatturazione);

		/*---------- End Create Head Fatturazione ---------------*/

		$headCSV = "$headAnagraficaCSV$MARKER$headFatturazioneCSV";
		$csv .="$headCSV\n";


		foreach ($datiForCSV as $impresa){

			reset($tracciatoAnagrafica);
			reset($tracciatoFatturazione);

			
			$idNum = (string)($impresa["Id"] + '0006000000');
			
			$tracciatoAnagrafica["Codice Cliente"] = "000".$idNum;
			
			$nomeAnagrafica = $this->splitByLenght($impresa["RagioneSociale"], 4, 40);

			$tracciatoAnagrafica["Nome 1"] = $nomeAnagrafica[0];
			$tracciatoAnagrafica["Nome 2"] = $nomeAnagrafica[1];
			$tracciatoAnagrafica["Name 3"] = $nomeAnagrafica[2];
			$tracciatoAnagrafica["Name 4"] = $nomeAnagrafica[3];
				

			$viaAnagrafica = $this->splitByLenght($impresa["Indirizzo"],5 , 60, 40);
				
			$tracciatoAnagrafica["Via"] = $viaAnagrafica[0];
			$tracciatoAnagrafica["Via 2"] = $viaAnagrafica[1];
			$tracciatoAnagrafica["Via 3"] = $viaAnagrafica[2];
			$tracciatoAnagrafica["Via 4"] = $viaAnagrafica[3];
			$tracciatoAnagrafica["Via 5"] = $viaAnagrafica[4];
				
			$tracciatoAnagrafica["Cap"] = $this->cutByLenght($impresa["Cap"],10);				
			$tracciatoAnagrafica["Localit�"] = $this->cutByLenght($impresa["Citta"], 40);
			$tracciatoAnagrafica["Paese"] = "IT";
			$tracciatoAnagrafica["Regione"] = $this->cutByLenght($impresa["Pr"],3);
			$tracciatoAnagrafica["Indirizzo Mail"] = $this->cutByLenght($impresa["EmailFatturazione"],132);
			$tracciatoAnagrafica["Partita IVA 1"] = $this->cutByLenght($impresa["CodiceFiscale"],16);
			$tracciatoAnagrafica["Partita IVA"] = $this->cutByLenght($impresa["PartitaIva"],11);
			
			
			$tracciatoAnagrafica["IVA"] = ""; // SAP
			$tracciatoAnagrafica["Fatturato annuo"] = $this->cutByLenght($impresa["FasciaFatturato"],20);
			$tracciatoAnagrafica["Divisa"] = ""; // SAP
			$tracciatoAnagrafica["Anno"] = ""; // SAP
			$tracciatoAnagrafica["Conto riconciliazione"] = ""; // SAP
			$tracciatoAnagrafica["Gruppo cash management"] = ""; // SAP
			$tracciatoAnagrafica["Chiave di classificazione"] = ""; // SAP

			$csvAnagrafica="";
			for ($index=0; $index < count($tracciatoAnagrafica)/2-1; $index++ ){
				$csvAnagrafica .= $tracciatoAnagrafica[$tracciatoAnagrafica[$index]].$MARKER;
			}
			$csvAnagrafica .= $tracciatoAnagrafica[$tracciatoAnagrafica[$index]];
				
				

			/*----- End Dati Anagrafica ------*/
			 	

			/*----- Start Dati Fatturazione ------*/
				
			$nomeFatturazione = $this->splitByLenght($this->restituisciValore($impresa["Denominazione"],""), 4, 40);

			$tracciatoFatturazione["Nome 1"] = $nomeFatturazione[0];
			$tracciatoFatturazione["Nome 2"] = $nomeFatturazione[1];
			$tracciatoFatturazione["Name 3"] = $nomeFatturazione[2];
			$tracciatoFatturazione["Name 4"] = $nomeFatturazione[3];
				
			$viaFatturazione = $this->splitByLenght($this->restituisciValore($impresa["Via"],""),5 , 60, 40);
				
			$tracciatoFatturazione["Via"] = $viaFatturazione[0];
			$tracciatoFatturazione["Via 2"] = $viaFatturazione[1];
			$tracciatoFatturazione["Via 3"] = $viaFatturazione[2];
			$tracciatoFatturazione["Via 4"] = $viaFatturazione[3];
			$tracciatoFatturazione["Via 5"] = $viaFatturazione[4];
			
			
			$tracciatoFatturazione["Cap"] = $this->cutByLenght($this->restituisciValore($impresa["CAP"],""),10);
			$tracciatoFatturazione["Localit�"] = $this->cutByLenght($this->restituisciValore($impresa["Comune"],""), 40);
			
			$tracciatoFatturazione["Paese"] = "";
			if ($impresa["Comune"] != null){
				$tracciatoFatturazione["Paese"] = "IT";
				$tracciatoFatturazione["Indirizzo Mail"] = $this->cutByLenght($impresa["EmailFatturazione"],132);
			}
			$tracciatoFatturazione["Regione"] = $this->cutByLenght($this->restituisciValore($impresa["PRV"],""),3);
			
			$tracciatoFatturazione["Partita IVA 1"] = $this->cutByLenght($this->restituisciValore($impresa["CodFiscale"],""),16);
			$tracciatoFatturazione["Partita IVA"] = $this->cutByLenght($this->restituisciValore($impresa["PIVA"],""),11);
			$tracciatoFatturazione["IVA"] = ""; //SAP


			$csvFatturazione="";
			for ($index=0; $index < count($tracciatoFatturazione)/2-1; $index++ ){
				$csvFatturazione .= $tracciatoFatturazione[$tracciatoFatturazione[$index]].$MARKER;
			}
			$csvFatturazione .= $tracciatoFatturazione[$tracciatoFatturazione[$index]];

			/*----- Start Dati Fatturazione ------*/


			$csv .= "$csvAnagrafica$MARKER$csvFatturazione\n";


		}

		return html_entity_decode($csv);

	}
	
	/**
	 * Function that create the FileCSV
	 * @param $fileText
	 */	
	public function createFileCSV($fileText){
		
		
		$fileCSV = fopen($this->localFile, "w");
		
		fwrite($fileCSV, $fileText);
		
		fclose($fileCSV);
		
	}
	
	
	/**
	 * Function that send fileCSV to SFTP server
	 */
	public function sendFileCSV($fileText) {
		
		$sftp= New Net_SFTP("$this->host");
		
		$sftp->login("$this->login","$this->password");
		
		$sftp->put("$this->remoteFile", $fileText);
		
		$sftp-> disconnect();
		
	}
	
	/**
	 * Function that send fileCSV to email
	 */
	/*public function sendEmailFileCSV($to_address,$date){
		
		$subject = "Dati Fatturazione Catalogo Expo del: $date";
		$message = "In allegato file CSV contenente i Dati di fatturazione delle imprese iscritte il: $date"; 
		
		send_mail_html_file($to_address, "noreply.catalogue@expo2015.org", $subject , $message, $this->localFile);
	}*/
}

$hour = 24;

$delay = $hour*3600;

$yesterdayTime = time()-$delay;

$todayFile = date("dmY",time());

$todayQuery = date("Y-m-d",$yesterdayTime);

$fileName = "IN_$todayFile.csv";


$createCSV = new CreateCSV($fileName);


$fileText = $createCSV ->createTextCSV($todayQuery);


if ($fileText){

	$createCSV->createFileCSV($fileText);
	$createCSV -> sendFileCSV($fileText);
	//$createCSV->sendEmailFileCSV("fabio.bertoletti.stage@digicamere.it", $todayFile);

}
?>