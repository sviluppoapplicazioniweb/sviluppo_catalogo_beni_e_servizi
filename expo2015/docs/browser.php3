<?php
// --------------------------------------------------------------------
// Browser Detection Version 1.0     :     Written by daddyBoy 2001

// Place the script within your common templates folder and place the following tag at
// the top of any pages you wish to use the script for browser detection and redirection.

// include "your.common.directory.here/browser.php";

// To use the debugging feature instert one of the following includes instead of the above one.

// Debugging For Your IP Address Only :
// if ($REMOTE_ADDR == "your.ip.address.here") $DEBUG_SW = true;
// include "your.common.directory.here/browser.php";

// Debugging For Several IP Addresses ( Unlimited Address Allowed ):

// if ($REMOTE_ADDR == "your.ip.address.here") $DEBUG_SW = true;
// if ($REMOTE_ADDR == "your.ip.address.here") $DEBUG_SW = true;
// if ($REMOTE_ADDR == "your.ip.address.here") $DEBUG_SW = true;
// include "your.common.directory.here/browser.php";

// Debugging Mode For All IPs:

$DEBUG_SW = true;
// include "your.common.directory.here/browser.php";
// --------------------------------------------------------------------


// ====================================================================
// global variables
// ====================================================================
$BROWSER [name]        = "Unknown";
$BROWSER [major]    = 0;
$BROWSER [system]    = "Other";

$BROWSER [has_frames]    = 0;
$BROWSER [has_jscript]    = 0;
$BROWSER [has_css]    = 0;
$BROWSER [has_dhtml]    = 0;

// ====================================================================
// initialization code
// ====================================================================
if ($DEBUG_SW) echo "<!-- USER_AGENT: $HTTP_USER_AGENT -->\n";

if ($name = strstr ($HTTP_USER_AGENT, "WebTV")) {
    $BROWSER [name] = "WebTV";
    list ($BROWSER [version]) = split ('[ ;]', substr ($name, 6));
    list ($BROWSER [major], $BROWSER [minor]) = split ('\.', $BROWSER [version]);

} elseif ($name = strstr ($HTTP_USER_AGENT, "MSIE")) {
    $BROWSER [name] = "Explorer";
    list ($BROWSER [version]) = split ('[ ;]', substr ($name, 5));
    list ($BROWSER [major], $BROWSER [minor]) = split ('\.', $BROWSER [version]);

    if ($BROWSER [major] >= 3) {
        $BROWSER [has_jscript]  = 1;
        $BROWSER [has_frames]   = 1;
        $BROWSER [has_css]      = 1;
    }

    if ($BROWSER [major] >= 4) {
        $BROWSER [has_dhtml]    = 1;
    }

    if ($BROWSER [major] >= 5) {
        $BROWSER [has_dhtml]    = 1;
    }

} elseif ($name = strstr ($HTTP_USER_AGENT, "Opera")) {
    $BROWSER [name] = "Opera";
    list ($BROWSER [version]) = split ('[ ;]', substr ($name, 6));
    list ($BROWSER [major], $BROWSER [minor]) = split ('\.', $BROWSER [version]);

    if ($BROWSER [major] >= 3) {
        $BROWSER [has_jscript]  = 1;
        $BROWSER [has_frames]   = 1;
    }

} elseif ($name = strstr ($HTTP_USER_AGENT, "Mozilla")) {
    $BROWSER [name] = "Netscape";
    list ($BROWSER [version]) = split ('[ ;]', substr ($name, 8));
    list ($BROWSER [major], $BROWSER [minor]) = split ('\.', $BROWSER [version]);

    if ($BROWSER [major] >= 3) {
        $BROWSER [has_jscript]  = 1;
        $BROWSER [has_frames]   = 1;
    }

    if ($BROWSER [major] >= 4) {
        $BROWSER [has_css]      = 1;
        $BROWSER [has_dhtml]    = 0;
    }

    if ($BROWSER [major] >= 6) {
        $BROWSER [has_dhtml]    = 1;
    }

} elseif ($name = strstr ($HTTP_USER_AGENT, "Amaya")) {
    $BROWSER [name] = "Amaya";
    list ($BROWSER [version]) = split ('[ ;]', substr ($name, 6));
    list ($BROWSER [major], $BROWSER [minor]) = split ('\.', $BROWSER [version]);

    $BROWSER [has_css]          = 1;
}

// ======== determine browser system ========
if (eregi ('win', $HTTP_USER_AGENT))    { $BROWSER [system] = "Windows"; }
if (eregi ('mac', $HTTP_USER_AGENT))    { $BROWSER [system] = "Macintosh"; }
if (eregi ('x11', $HTTP_USER_AGENT))    { $BROWSER [system] = "Unix"; }

if ($DEBUG_SW) {
    echo "<!-- Browser_Fields ['name'] = " . $BROWSER [name]        . " -->\n";
    echo "<!-- Browser_Fields ['version'] = " . $BROWSER [major]        . " -->\n";
    echo "<!-- Browser_Fields ['minor'] = " . $BROWSER [minor]        . " -->\n";
    echo "<!-- Browser_Fields ['system'] = " . $BROWSER [system]        . " -->\n";
    echo "<!-- Browser_Fields ['has_frames'] = " . $BROWSER [has_frames]    . " -->\n";
    echo "<!-- Browser_Fields ['has_jscript'] = " . $BROWSER [has_jscript] . " -->\n";
    echo "<!-- Browser_Fields ['has_css'] = " . $BROWSER [has_css]        . " -->\n";
    echo "<!-- Browser_Fields ['has_dhtml'] = " . $BROWSER [has_dhtml]    . " -->\n";

}
?>
