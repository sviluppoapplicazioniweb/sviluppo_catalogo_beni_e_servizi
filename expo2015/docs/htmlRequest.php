<?php
//$lines = file_get_contents('http://www.expo2015.org/');

function remove_javascript($java){
	
	return preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}

function remove_noscript($java){
	
	return preg_replace('~<\s*\bnoscript\b[^>]*>(.*?)<\s*\/\s*noscript\s*>~is', '', $java);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}

function remove_head($html){

	return preg_replace('~<\s*\bhead\b[^>]*>(.*?)<\s*\/\s*head\s*>~is', '', $html);
	//return preg_replace('<script\b[^>]*>(.*?)</script>', "", $java);

}


function htmlButTags($string) {
	 
	$pattern = '<([a-zA-Z0-9\. "\'_\/-=;\(\)?&#%]+)>';
	preg_match_all ('/' . $pattern . '/', $string, $tagMatches, PREG_SET_ORDER);
	$textMatches = preg_split ('/' . $pattern . '/', $string);
	 
	foreach ($textMatches as $key => $value) {
		$textMatches [$key] = htmlentities ($value, ENT_QUOTES, 'UTF-8');
	}
	 
	for ($i = 0; $i < count ($textMatches); $i ++) {
		$textMatches [$i] = $textMatches [$i] . $tagMatches [$i] [0];
	}
	 
	return implode ($textMatches);
	 
}


$ctx = stream_context_create(array(
		'http' => array(
				'timeout' => 5
		)
)
);



$lines = file_get_contents('http://test.digicamere.it/htmlRequest.php', 0, $ctx);
//$lines = file_get_contents('http://www.isopan.it/assets/pdf/14_10_2011_Bookphoto.pdf', 0, $ctx);
//$lines = file_get_contents('https://docs.google.com/a/digicamere.it/file/d/0B8NH5tOIuXwXTDdTWEZzMy1qRlU/edit?pli=1', 0, $ctx);
//$lines = file_get_contents('http://www.parrocchiavenegono.it/download/foglioinformativo_2014-04-12.pdf', 0, $ctx);


print remove_noscript(remove_javascript(remove_head($lines)));
//print html_entity_decode(htmlButTags(remove_javascript($lines)));
//print $lines;

 ?>